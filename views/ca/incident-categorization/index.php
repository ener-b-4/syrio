<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
//use yii\grid\GridView;
use app\components\helpers\DataGrid\CustomGridView;
use app\components\helpers\DataGrid\funDataGridClass;

use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ca\IncidentCategorizationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Reported incidents');
$this->params['breadcrumbs'][] = $this->title;
?>

<?php


/*
=====================================================================================
*/
$title_h_l =  'SyRIO ' . \Yii::t('app', 'Export');
$title_h_c =  $this->title; //'Custom C';
$title_h_r = 'Generated' . ': ' . date("D, d-M-Y g:i a T");

$ourPdfHeader = funDataGridClass::getOurPdfHeader($title_h_l, $title_h_c, $title_h_r);

$title_f = 'Reported incidents';
$ourPdfFooter = funDataGridClass::getOurPdfFooter($title_f);
$exportConfig = funDataGridClass::getExportConfiguration($ourPdfHeader, $ourPdfFooter);

$heading = Yii::t('app', 'Reported incidents');

/*
=====================================================================================
*/

include 'grid_parts/columns.php';
include 'grid_parts/grid_toolbar.php';


$beforeHeaderColumns = [
    ['content'=>ucfirst(\Yii::t('app', 'assessment')), 'options'=>['colspan'=>2, 'class'=>'text-center warning']], 
    ['content'=>\Yii::t('app', 'Reporting'), 'options'=>['colspan'=>2, 'class'=>'text-center warning']], 
    ['content'=>\Yii::t('app/crf', 'Event'), 'options'=>['colspan'=>4, 'class'=>'text-center warning']], 
];

?>

<div class="incident-categorization-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class='sy_pad_bottom_36'>
        <?= \Yii::t('app', 'The following is the list of all the off-shore events in your jurisdiction, reported by the Operator/Owners according to {evt1} and {evt2}.', [
            'evt1' => Html::a(\Yii::t('app', 'Directive 2013/30/EU'), 'http://eur-lex.europa.eu/legal-content/EN/ALL/?uri=CELEX:32013L0030'),
            'evt2' => Html::a(\Yii::t('app', 'Regulation (EU) No 1112/2014 of 13 October 2014'), 'http://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32014R1112'),
        ]) ?>
    </div>

    
    <div class='row'>
        <div class='col-lg-12'>

<?php    

Pjax::begin();

    //GridView::widget([
    echo CustomGridView::widget([
      'dataProvider' => $dataProvider,
      'filterModel' => $searchModel,
      'autoXlFormat' => false,
      'pjax' => false,
        
        'beforeHeader'=>[
            [
                'columns'=>$beforeHeaderColumns,
                'options'=>['class'=>'skip-export'] // remove this row from export
            ]
        ],
        
      'rowOptions' => function($model) {
        
//        return $model->del_status == app\models\events\EventDeclaration::STATUS_DELETED ? ['class' => 'danger'] : [];
//        /* asta cade - pun coloana separata */
//        if ($model->ca_status == EventDeclaration::INCIDENT_REPORT_REJECTED) {
//          return ['class' => 'danger'];
//        }
//        else if ($model->ca_status == EventDeclaration::INCIDENT_REPORT_ACCEPTED) {
//          return ['class' => 'success'];
//        }
      },
      'columns' => $columns,
      'toolbar' => $grid_toolbar,
              
      // set export properties
      'export' => [
        'icon' => 'share-square-o',
        'encoding' => 'utf-8',
        'fontAwesome' => true,
        'target' => CustomGridView::TARGET_SELF,   //target: string, the target for submitting the export form, which will trigger the download of the exported file. 
        'showConfirmAlert' => TRUE,
        'header' => '<li role="presentation" class="dropdown-header">' . \Yii::t('app', 'Grid Export') . '</li>'
        //'header' => 'Export Page Data',
      ],

      //(start) Author: cavesje   Date: 14.08.2015  - extra configuration PDF export
      'exportConfig'     => $exportConfig,
      //(end) Author: cavesje   Date: 14.08.2015  - extra configuration PDF export
              
      'panel' => [
        //'type' => GridView::TYPE_PRIMARY,
        'type' => CustomGridView::TYPE_PRIMARY,
        'heading' => $heading,
         
        /*  
        'before' => '<div class="col-sm-2">'
                . Html::a('<span class="glyphicon glyphicon-trash"></span> '
                        . '<span class="badge">' . \app\models\events\drafts\EventDraft::getNumberOfDeleted($_event->id) . '</span>',
                        $trashUrl
                        , [
                            'class' => 'btn btn-danger',
                            'title' => Yii::t('app', 'Deleted drafts. Click to manage.')])
                . '</div>',
         * 
         */
      ],

      'persistResize' => false,
              
]);   //end widget

Pjax::end();      
?>                  
            
        </div>
    </div>    

</div>
