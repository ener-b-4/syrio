<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ca\IncidentCategorization */
/* @var $form yii\widgets\ActiveForm */
/* @var $promptMessage string (may be null) */


$draft = $model->draft;
$event = $draft->event;
$dt = new DateTime($event->event_date_time);

if ($model->hasErrors())
{
    echo "<div class='alert alert-danger' role='alert'>";
    echo '<h3>'. \Yii::t('app', 'The Assessment could not be finalized!').'</h3>';
    echo '<p>' . \Yii::t('app', 'Review the information below and try again.').'</p>';
    echo '<ul>';
    
    foreach($model->errors as $key => $errors)
    {
        foreach($errors as $error)
        {
           echo "<li>" . $error . '</li>';
        }
        //echo "<br/>" . $value;
    }
    echo '</ul>';
    echo "</div>";
    //echo '<pre>';
    //echo var_dump($model->errors);
    //echo '</pre>';
}
else if (\Yii::$app->session->hasFlash('msg'))
{
    if (Yii::$app->session->getFlash('msg')=='signed')
    {
        $s='<div class="alert alert-success" role="alert">'
                . 'You have successfully signed the Incident Report.<br/>'
                . 'You may now proceed to <a href="#"> submit the report </a> to the Competent Authority .'
                . 'or go back to the <a href="#">Incident reports</a> list.'
                . '</div>';
        echo $s;
    }
    
}
?>

<div id='def'>
    <h1><?= \Yii::$app->user->identity->isCaUser ? '' : $model->draft->event->event_name ?></h1>

    <!-- render the event details -->
    <h3><?= Yii::t('app/crf', 'Event date and time') ?></h3>
    (a) <?= Yii::t('app/crf', 'Event date') ?>: <?= date_format($dt, 'd-m-Y') ?>
    <br/>
    (b) <?= Yii::t('app/crf', 'Event time') ?>: <?= date_format($dt, 'H:i:s') ?>


    <h3><?= Yii::t('app/crf', 'Details of the location and of the person reporting the event') ?></h3>
    
    <table class='sy_simple-table'>
        <tr>
            <td><?= $event->attributeLabels()['operator'] ?></td>
            <td><?= $event->operator ?></td>
        </tr>
        <tr>
            <td><?= $event->attributeLabels()['installation_name_type'] ?></td>
            <td><?= $event->installation_name_type ?></td>
        </tr>
        <tr>
            <td><?= $event->attributeLabels()['field_name'] ?></td>
            <td><?= $event->field_name ?></td>
        </tr>
        <tr style='min-height: 32px; max-height: 32px; height: 32px;'>
            <td colspan='2'></td>
        </tr>
        <tr>
            <td><?= $event->attributeLabels()['raporteur_name'] ?></td>
            <td><?= $event->raporteur_name ?></td>
        </tr>
        <tr>
            <td><?= $event->attributeLabels()['raporteur_role'] ?></td>
            <td><?= $event->raporteur_role ?></td>
        </tr>
        <tr style='min-height: 32px; max-height: 32px; height: 32px;'>
            <td colspan='2'></td>
        </tr>
        <tr>
            <td colspan='2'><strong><?= Yii::t('app/crf','Contact details') ?></strong></td>
        </tr>
        <tr>
            <td><?= $event->attributeLabels()['contact_tel'] ?></td>
            <td><?= $event->contact_tel ?></td>
        </tr>
        <tr>
            <td><?= $event->attributeLabels()['contact_email'] ?></td>
            <td><?= $event->contact_email ?></td>
        </tr>
    </table>
    

    <h3><?= Yii::t('app/crf', 'Event categorization') ?></h3>
    <p><em><?= Yii::t('app/crf', 'What type of event is being reported?') ?></em></p>
    <?= $this->render('/events/drafts/event-draft/view_all', ['model'=>$draft]) ?>
    
<?php $form = ActiveForm::begin(['id'=>'ca_form']); ?>

    <?php include 'updateSections/sectionA.php';?>
    <?php include 'updateSections/sectionB.php';?>
    <?php include 'updateSections/sectionC.php';?>

    <?php include 'updateSections/sectionD.php';?>
    <?php include 'updateSections/sectionE.php';?>
    <?php include 'updateSections/sectionF.php';?>
    <?php include 'updateSections/sectionG.php';?>
    <?php include 'updateSections/sectionH.php';?>
    
    <?php include 'updateSections/sectionI.php';?>
    <?php include 'updateSections/sectionJ.php';?>

    <?= Html::hiddenInput('hi_draft', '0', ['id' => 'hi_draft']) ?>
    
</div>

        <div class="form-group" style="padding-top:36px;">
            
            <?php
            
            if (isset($promptMessage) && trim($promptMessage)!='') {
                echo Html::submitButton(Yii::t('app', 'Save draft'), [
                'form' => 'ca_form',
                'class' => 'btn btn-primary',
                'id' => 'btn-draft',

                'prompt' => $promptMessage
                
                //'click' => 'draft'
                ]);
            } else {
                echo Html::submitButton(Yii::t('app', 'Save draft'), [
                'form' => 'ca_form',
                'class' => 'btn btn-primary',
                'id' => 'btn-draft',
                ]);
            }
            
            ?>
            
            <?= Html::submitButton(Yii::t('app', 'Finalize'), [
                'form' => 'ca_form',
                'class' => 'btn btn-success'
                ]) ?>
        </div>

<?php ActiveForm::end(); ?>

</div>


<?php
$js = <<< EOF

$(function() {
        //alert('here');
        $('#btn-draft').click(function(event) {
            event.stopPropagation();
            event.preventDefault();

        var prompt = $ (this).attr("prompt");
        
        if (typeof prompt !== "undefined" && prompt !== "") {
            var r = confirm(prompt);
            if (r==true) {
                var \$in = $('#hi_draft').val(1);
                //alert(\$in.val());
                $('#ca_form').submit();
            }   //end if confirm
        } else {
                var \$in = $('#hi_draft').val(1);
                //alert(\$in.val());
                $('#ca_form').submit();
        }  //end if prompt
        });
});
        
EOF;

$this->registerJs($js, yii\web\View::POS_END);

?>