<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

if ($draft->is_a)
{
    $title = Yii::t('app/crf', 'Unintended release of oil, gas or other hazardous substances, whether or not ignited');
    $ssection = 'A';
    
    echo '<div class="col-lg-12 text-center sy_rep-head-1">'
            . '<h3>' . Yii::t('app/crf', 'Section {evt}', ['evt' => $ssection]) . '</h3>'
            . '<strong>'
            . $title
            . '</strong>'
            . '</div>';

//    $s = '<h4>'
//            . 'A1. <strong>Was there a release of hydrocarbon substances?</strong>'
//            . '<span style="padding-left:12px;">    Yes</span>'
//            . Html::radio("r1", $draft->sectionA->a1->a1)
//            . '<span style="padding-left:12px;">    No</span>'
//            . Html::radio("r2", !$draft->sectionA->a1->a1)
//            . '</h4>';
    //echo $s;
    
    echo $this->render('/events/drafts/sections/subsections/a1/view', ['model'=>$draft->sectionA->a1, 'stripped'=>2]);
//    echo $this->render('/events/drafts/sections/subsections/a1/view', ['model'=>$draft->sectionA->a1, 'stripped'=>2]);

    
    echo $this->render('/events/drafts/sections/subsections/a2/view', ['model'=>$draft->sectionA->a2, 'stripped'=>2]);

    echo $this->render('/events/drafts/sections/subsections/a3/view', ['model'=>$draft->sectionA->a3, 'stripped'=>2]);

    echo $this->render('/events/drafts/sections/subsections/a4/view', ['model'=>$draft->sectionA->a4, 'stripped'=>2]);
    

?>

<div class="well col-lg-12">

    <table style='width: 100%'>
        <tbody>
            <tr>
                <td class="sy_crf_para">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <strong><?= Yii::t('app/crf', 'The competent authority shall further complete this section') ?></strong>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <?= \Yii::t('app/crf', 'Is this considered to be a major incident?') ?><br/>
                    <ul class="list-unstyled">
                        <li class="sy_vert_top"><?= Html::activeRadio($model, 'is_major_a', ['id'=>'is_major_a_y', 'label'=>\Yii::t('app', 'Yes'), 'value'=>1, 'uncheck'=>null]) ?>
                        <li class="sy_vert_top"><?= Html::activeRadio($model, 'is_major_a', ['id'=>'is_major_a_n', 'label'=>\Yii::t('app', 'No') , 'value'=>0, 'uncheck'=>null]) ?>
                    </ul>
                    </div>
                </td>
            </tr>
            <tr>
                <td style='width:100%;'>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <?= \Yii::t('app/crf', 'Give justification'); ?>
                        <br/>
                        <?= $form->field($model, 'major_a_just')->textarea(['class'=>'sy_fill_textarea form-control', 'rows'=>25])->label('') ?>                      
                    </div>
                </td>
            </tr>            
        </tbody>
    </table>

<?php

}
?>



</div>




