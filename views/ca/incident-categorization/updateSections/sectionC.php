<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

if ($draft->is_c)
{
    $title = Yii::t('app/crf', 'Failure of a safety and environmental critical element');
    $ssection = 'C';
    
    echo '<div class="col-lg-12 text-center sy_rep-head-1">'
            . '<h3>' . Yii::t('app/crf', 'Section {evt}', ['evt' => $ssection]) . '</h3>'
            . '<strong>'
            . $title
            . '</strong>'
            . '</div>';

    echo $this->render('/events/drafts/sections/subsections/c1/view', ['model'=>$draft->sectionC->c1, 'stripped'=>2]);
    echo $this->render('/events/drafts/sections/subsections/c2/view', [
        'model'=>$model->draft->sectionC->c2, 
        'stripped'=>2, 
        'section2_1'=>$model->draft->sectionC->c2->sC2_1,
        'section2_2'=>$model->draft->sectionC->c2->sC2_2]);
    echo $this->render('/events/drafts/sections/subsections/c3/view', ['model'=>$draft->sectionC->c3, 'stripped'=>2]);
    echo $this->renderAjax('/events/drafts/sections/subsections/c4/view', ['model'=>$draft->sectionC->c4, 'stripped'=>2]);


?>

<div class="well col-lg-12">

        <table style='width: 100%'>
        <tbody>
            <tr>
                <td class="sy_crf_para">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <strong><?= Yii::t('app/crf', 'The competent authority shall further complete this section') ?></strong>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <?= \Yii::t('app/crf', 'Is this considered to be a major incident?') ?><br/>
                    <ul class="list-unstyled">
                        <li class="sy_vert_top"><?= Html::activeRadio($model, 'is_major_c', ['id'=>'is_major_c_y', 'label'=>\Yii::t('app', 'Yes'), 'value'=>1, 'uncheck'=>null]) ?>
                        <li class="sy_vert_top"><?= Html::activeRadio($model, 'is_major_c', ['id'=>'is_major_c_n', 'label'=>\Yii::t('app', 'No') , 'value'=>0, 'uncheck'=>null]) ?>
                    </ul>
                    </div>
                </td>
            </tr>
            <tr>
                <td style='width:100%;'>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <?= \Yii::t('app/crf', 'Give justification'); ?>
                        <br/>
                        <?= $form->field($model, 'major_c_just')->textarea(['class'=>'sy_fill_textarea form-control', 'rows'=>20])->label('') ?>                      
                    </div>
                </td>
            </tr>            
        </tbody>
    </table>

    

</div>

<?php
}
?>

