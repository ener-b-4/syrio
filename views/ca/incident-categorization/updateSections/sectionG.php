<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

if ($draft->is_g)
{
    $title = Yii::t('app/crf', 'Any fatal accident to be reported under the requirements of Directive 92/91/EEC');
    $ssection = 'G';
    
    echo '<div class="col-lg-12 text-center sy_rep-head-1">'
            . '<h3>' . Yii::t('app/crf', 'Section {evt}', ['evt' => $ssection]) . '</h3>'
            . '<strong>'
            . $title
            . '</strong>'
            . '</div>';

?>

<div style="padding-top: 64px" class='text-muted'>
    <p>
        <?= Yii::t('app', 'Page intentionally left blank') ?>
    </p>
</div>

<div class="well col-lg-12">

    <table style="width: 100%">
        <tbody>
            <tr>
                <td class="sy_crf_para">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <strong><?= Yii::t('app/crf', 'The competent authority shall further complete this section') ?></strong>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <?= \Yii::t('app/crf', 'Is this considered to be a major incident?') ?><br/>
                    <ul class="list-unstyled">
                        <li class="sy_vert_top"><?= Html::activeRadio($model, 'is_major_g', ['id'=>'is_major_g_y', 'label'=>\Yii::t('app', 'Yes'), 'value'=>1, 'uncheck'=>null]) ?>
                        <li class="sy_vert_top"><?= Html::activeRadio($model, 'is_major_g', ['id'=>'is_major_g_n', 'label'=>\Yii::t('app', 'No') , 'value'=>0, 'uncheck'=>null]) ?>
                    </ul>
                    </div>
                </td>
            </tr>
            <tr>
                <td style='width:100%;'>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <?= \Yii::t('app/crf', 'Give justification'); ?>
                        <br/>
                        <?= $form->field($model, 'major_g_just')->textarea(['class'=>'sy_fill_textarea form-control', 'rows'=>20])->label('') ?>                      
                    </div>
                </td>
            </tr>            
        </tbody>
    </table>

    

</div>

<?php
}
?>

