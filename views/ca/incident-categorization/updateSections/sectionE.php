<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

if ($draft->is_e)
{
    $title = Yii::t('app/crf', 'Vessels on collision course and actual vessel collisions with an offshore installation');
    $ssection = 'E';
    
    echo '<div class="col-lg-12 text-center sy_rep-head-1">'
            . '<h3>' . Yii::t('app/crf', 'Section {evt}', ['evt' => $ssection]) . '</h3>'
            . '<strong>'
            . $title
            . '</strong>'
            . '</div>';

    echo $this->render('/events/drafts/sections/subsections/e1/view', ['model'=>$draft->sectionE->e1, 'stripped'=>2]);
    echo $this->render('/events/drafts/sections/subsections/e2/view', ['model'=>$draft->sectionE->e2, 'stripped'=>2]);
    echo $this->render('/events/drafts/sections/subsections/e3/view', ['model'=>$draft->sectionE->e3, 'stripped'=>2]);
    echo $this->render('/events/drafts/sections/subsections/e4/view', ['model'=>$draft->sectionE->e4, 'stripped'=>2]);


?>

<div class="well col-lg-12">

    <table style="width: 100%">
        <tbody>
            <tr>
                <td class="sy_crf_para">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <strong><?= Yii::t('app/crf', 'The competent authority shall further complete this section') ?></strong>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <?= \Yii::t('app/crf', 'Is this considered to be a major incident?') ?><br/>
                    <ul class="list-unstyled">
                        <li class="sy_vert_top"><?= Html::activeRadio($model, 'is_major_e', ['id'=>'is_major_e_y', 'label'=>\Yii::t('app', 'Yes'), 'value'=>1, 'uncheck'=>null]) ?>
                        <li class="sy_vert_top"><?= Html::activeRadio($model, 'is_major_e', ['id'=>'is_major_e_n', 'label'=>\Yii::t('app', 'No') , 'value'=>0, 'uncheck'=>null]) ?>
                    </ul>
                    </div>
                </td>
            </tr>
            <tr>
                <td style='width:100%;'>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <?= \Yii::t('app/crf', 'Give justification'); ?>
                        <br/>
                        <?= $form->field($model, 'major_e_just')->textarea(['class'=>'sy_fill_textarea form-control', 'rows'=>20])->label('') ?>                      
                    </div>
                </td>
            </tr>            
        </tbody>
    </table>

    

</div>

<?php
}
?>

