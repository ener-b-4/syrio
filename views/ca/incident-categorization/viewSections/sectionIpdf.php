<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

if ($draft->is_i)
{
    echo '<div class="col-lg-12 text-center sy_rep-head-1">'
            . '<h3>Section I</h3>'
            . '<strong>'
            . 'ANY EVACUATION OF PERSONNEL'
            . '</strong>'
            . '</div>';

    //echo $this->render('/events/drafts/sections/subsections/i1/view', ['model'=>$draft->sectionI->i1, 'stripped'=>1]);
    //echo $this->render('/events/drafts/sections/subsections/i2/view', ['model'=>$draft->sectionI->i2, 'stripped'=>1]);
    echo $this->render('/events/drafts/sections/subsections/i3/view_pdf', ['model'=>$draft->sectionI->i3, 'stripped'=>1]);
    //echo $this->render('/events/drafts/sections/subsections/i4/view', ['model'=>$draft->sectionI->i4, 'stripped'=>1]);


?>

        <table>
        <tbody>
            <tr>
                <td class="sy_crf_para">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <strong><?= Yii::t('app/crf', 'The competent authority shall further complete this section') ?></strong>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <p>
                            <?= Yii::t('app/crf', 'Is this considered to be a major incident?') ?>
                        </p>
                    <ul class="list-unstyled">
                        <li class="sy_vert_top"><?= Html::activeRadio($model, 'is_major_i', ['id'=>'is_major_i_y', 'label'=>\Yii::t('app', 'Yes'), 'value'=>1, 'uncheck'=>null]) ?>
                        <li class="sy_vert_top"><?= Html::activeRadio($model, 'is_major_i', ['id'=>'is_major_i_n', 'label'=>\Yii::t('app', 'No') , 'value'=>0, 'uncheck'=>null]) ?>
                    </ul>
                    </div>
                </td>
            </tr>
            <tr>
                <td style='width:100%;'>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <?= \Yii::t('app/crf', 'Give justification') ?>
                        <br/>
                        <div class="sy_pdf_text_area">
                            <?php
                                $init_text = $model->major_i_just;
                                //echo '<pre>';
                                //echo var_dump($model);
                                //echo '</pre>';
                                //die();
                                $parsed_text = nl2br($init_text);
                                echo $parsed_text;
                            ?>
                        </div>
                    </div>
                </td>
            </tr>            
        </tbody>
    </table>


<?php
}
?>

