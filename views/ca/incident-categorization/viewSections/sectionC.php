<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

if ($draft->is_c)
{
    $title = Yii::t('app/crf', 'Failure of a safety and environmental critical element');
    $ssection = 'C';
    
    echo '<div class="col-lg-12 text-center sy_rep-head-1">'
            . '<h3>' . Yii::t('app/crf', 'Section {evt}', ['evt' => $ssection]) . '</h3>'
            . '<strong>'
            . $title
            . '</strong>'
            . '</div>';

    echo $this->render('/events/drafts/sections/subsections/c1/review', ['model'=>$draft->sectionC->c1, 'from_ca'=>1, 'event_id'=>$draft->event->id]);
    echo $this->render('/events/drafts/sections/subsections/c2/review', [
        'model'=>$model->draft->sectionC->c2, 
        'stripped'=>1, 
        'section2_1'=>$model->draft->sectionC->c2->sC2_1,
        'section2_2'=>$model->draft->sectionC->c2->sC2_2]);
    echo $this->render('/events/drafts/sections/subsections/c3/review', ['model'=>$draft->sectionC->c3, 'stripped'=>1]);
    echo $this->render('/events/drafts/sections/subsections/c4/review', ['model'=>$draft->sectionC->c4, 'stripped'=>1]);


?>

<div class="well col-lg-12">

        <table>
        <tbody>
            <tr>
                <td class="sy_crf_para">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <strong><?= Yii::t('app/crf', 'The competent authority shall further complete this section') ?></strong>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <p>
                            <?= Yii::t('app/crf', 'Is this considered to be a major incident?') ?>
                        </p>
                        <ul class="list-unstyled">
                            <li class="sy_vert_top"><?= Html::activeRadio($model, 'is_major_c', ['id'=>'is_major_c_y', 'label'=>\Yii::t('app', 'Yes'), 'value'=>1, 'uncheck'=>null, 'disabled' => 'disabled']) ?>
                            <li class="sy_vert_top"><?= Html::activeRadio($model, 'is_major_c', ['id'=>'is_major_c_n', 'label'=>\Yii::t('app', 'No') , 'value'=>0, 'uncheck'=>null, 'disabled' => 'disabled']) ?>
                        </ul>
                    </div>
                </td>
            </tr>
            <tr>
                <td style='width:100%;'>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <br/>
                        <p>
                            <?= \Yii::t('app/crf', 'Give justification') ?>
                        </p>
                        <p>
                            <?= nl2br(\app\models\shared\NullValueFormatter::Format($model->major_c_just)) ?>
                        </p>
                    </div>
                </td>
            </tr>            
        </tbody>
    </table>

    

</div>

<?php
}
?>

