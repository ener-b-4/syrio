<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="sy_pdf_text">

<?php
if ($draft->is_e)
{
    echo '<div class="col-lg-12 text-center sy_rep-head-1">'
            . '<h3>Section E</h3>'
            . '<strong>'
            . Yii::t('app/crf', 'VESSELS ON COLLISION COURSE AND ACTUAL VESSEL COLLISIONS WITH AN OFFSHORE INSTALLATION')
            . '</strong>'
            . '</div>';

    echo $this->render('/events/drafts/sections/subsections/e1/view', ['model'=>$draft->sectionE->e1, 'stripped'=>1]);
    echo $this->render('/events/drafts/sections/subsections/e2/view', ['model'=>$draft->sectionE->e2, 'stripped'=>1]);
    echo $this->render('/events/drafts/sections/subsections/e3/view', ['model'=>$draft->sectionE->e3, 'stripped'=>1]);
    echo $this->render('/events/drafts/sections/subsections/e4/view', ['model'=>$draft->sectionE->e4, 'stripped'=>1]);


?>

<div class="well col-lg-12">

        <table>
        <tbody>
            <tr>
                <td class="sy_crf_para">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <strong><?= Yii::t('app/crf', 'The competent authority shall further complete this section') ?></strong>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <p>
                            <?= Yii::t('app/crf', 'Is this considered to be a major incident?') ?>
                        </p>
                    <ul class="list-unstyled">
                        <li class="sy_vert_top"><?= Html::activeRadio($model, 'is_major_e', ['id'=>'is_major_e_y', 'label'=>\Yii::t('app', 'Yes'), 'value'=>1, 'uncheck'=>null]) ?>
                        <li class="sy_vert_top"><?= Html::activeRadio($model, 'is_major_e', ['id'=>'is_major_e_n', 'label'=>\Yii::t('app', 'No') , 'value'=>0, 'uncheck'=>null]) ?>
                    </ul>
                    </div>
                </td>
            </tr>
            <tr>
                <td style='width:100%;'>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <?= \Yii::t('app/crf', 'Give justification') ?>
                        <br/>
                        <?= Html::textarea('major_e_just', $model->major_e_just, ['disabled'=>'disabled', 'class'=>'sy_fill_textarea form-control']) ?>
                    </div>
                </td>
            </tr>            
        </tbody>
    </table>

    

</div>

<?php
}
?>

</div>