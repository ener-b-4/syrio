<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\widgets\ValidationReportWidget;


/* @var $this yii\web\View */
/* @var $model app\models\ca\IncidentCategorization */

$this->title = \Yii::t('app', 'Reject') . ' ' . $model->draft->event->operator . ' / ' . $model->draft->event->event_date_time;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'MA Assessment'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

if ($model->hasErrors())
{
    echo ValidationReportWidget::widget([
        'title' => \Yii::t('app','Reject incident report'),
        'errors_message' => \Yii::t('app','Justification must be provided when rejecting an incident report.'),
        'errors'=>$model->errors,
    ]);
}                            

?>

<div class="incident-categorization-view">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <?php $form = ActiveForm::begin(); ?>    

    <?= Html::label($model->attributeLabels()['rejection_desc']) ?>
    <div class='sy_pad_top_18'>
        <?= \Yii::t('app', 'Please provide justification for rejecting the incident report.') ?>
    </div>
    <div class='sy_pad_top_18 sy_pad_bottom_18 text-danger'>
        <?= \Yii::t('app', 'Your justification will be sent to the Operator/Owner who submitted the report.') ?>
    </div>
    <?= Html::activeTextarea($model, 'rejection_desc', [
        'class' => 'sy_fill_textarea sy_narative-text form-control',
        'placeholder' => Yii::t('app', 'enter text...'),
    ]) ?>

    <div class="form-group" style="padding-top:36px;">
        <?= Html::submitButton(Yii::t('app/commands', 'Submit'), [
            'class' => 'btn btn-primary'
            ]) ?>
    </div>
    
    <?php $form = ActiveForm::end(); ?>    
    
</div>