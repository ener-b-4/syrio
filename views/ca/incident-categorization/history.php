<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;

/**
 * view file for event reporting history
 *
 * @author vamanbo
 */

/* @var $this yii\web\View */
/* @var $searchModel app\models\ReportingHistorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $evt_id integer */
/* @var $evt_name string */
/* @var $incident_cat_id integer the id of the Incident Categorization */

$this->title = \Yii::t('app', 'Reporting history');

$this->params['breadcrumbs'][] = 
	[
		'label' => \Yii::t('app', 'Reported incidents'),
		'url' => \Yii::$app->urlManager->createUrl(['/ca/incident-categorization']),
		//'template' =>"<li>{link}</li>
	];

$this->params['breadcrumbs'][] = 
	[
		'label' => $evt_name,
		//'url' => \Yii::$app->urlManager->createUrl(['/management/']),
		//'template' =>"<li>{link}</li>
	];
//$this->params['breadcrumbs'] = $breadcrumbs;

$this->params['breadcrumbs'][] = $this->title;


?>


<div class="row">
    <div class="col-lg-12">
        <?php include Yii::getAlias('@app') . '/views/shared_parts/message_snippet.php'; ?>
    </div>
    
    <div class="col-lg-12">
        <h1><?= Html::encode($this->title) ?></h1>
        <div class="lead"><?= Html::encode(\Yii::t('app/crf', 'Event Name') . ': ' . $evt_name) ?></div>
        <?php

        echo $this->render('@app/views/reporting-history/history_details_ca', [
            'searchModel'=>$searchModel,
            'dataProvider'=>$dataProvider,
            'evt_id' => $evt_id,
            'evt_name' => $evt_name,
            'incident_cat_id' => $incident_cat_id
        ])

        ?>
    </div>
</div>


