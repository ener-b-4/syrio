<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ca\IncidentCategorization */
/* @var $promptMessage string (may be null) */

$this->title = Yii::t('app', 'Assessment of {modelClass}', [
    'modelClass' => Yii::t('app', 'Incident Report'),
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Incident reports'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Major Accident assessment');
?>
<div class="incident-categorization-update">

    <h1><?= Html::encode($this->title) . ': ' . $model->name ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'promptMessage' => isset($promptMessage) ? $promptMessage : null
    ]) ?>

</div>

<?php
$JS = <<< EOF

// 2024-05-16 HOTFIX - enable categorization radiobuttons
$("input[id^='is_major_']").each(function() {
    $ (this).attr('disabled', null);    
});            

EOF;

$this->registerJs($JS, yii\web\View::POS_READY);

?>