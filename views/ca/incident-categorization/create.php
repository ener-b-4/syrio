<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ca\IncidentCategorization */

$this->title = Yii::t('app', 'Create {evt}', [
    'modelClass' => 'Incident Categorization',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Incident Categorizations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="incident-categorization-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
