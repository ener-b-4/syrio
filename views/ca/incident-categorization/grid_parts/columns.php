<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use kartik\grid\GridView;
use app\models\events\EventDeclaration;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ca\IncidentCategorizationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$columns = [
    
    [
        'class' => kartik\grid\SerialColumn::className(),
        'hidden' => true,
        'hiddenFromExport' => false
    ],
    
    [
        'class' => kartik\grid\DataColumn::className(),
        'attribute' => 'name',
        'hidden' => true,
        'hiddenFromExport' => false,
        'label' => \Yii::t('app', 'Name')
    ],
    
    [
        'class' => \kartik\grid\DataColumn::className(),
        'content' => function($model, $key, $index, $column) {
            return Html::a('<span class="fa fa-history"></span>', ['history', 'id'=>$key], [
                
            ]);
        },
        'hiddenFromExport' => true
    ],    
    
    [
        'label' => \Yii::t('app', 'MA / CPF'),
        
        'attribute' => 'is_done',
        'class' => \app\utilities\CaBoolDataColumn::className(),
        
        'width' => '180px;',
        
        'filterType' => GridView::FILTER_SELECT2,   // '\kartik\widgets\Select2',
        'filter' => [
            \Yii::t('app', 'Major Accident') =>[0 => \Yii::t('app', 'unassessed'), 1 => strtoupper(\Yii::t('app', 'major')), 2 => strtoupper(\Yii::t('app', 'non-major')), 3 => strtoupper(\Yii::t('app', 'rejected'))],
            \Yii::t('app', 'CPF Section 4') =>[-20 => \Yii::t('app', 'not required'), -10 => \Yii::t('app', 'not available'), 20 => strtoupper(\Yii::t('app', 'unassessed')), 10 => strtoupper(\Yii::t('app', 'assessed'))]            
            ],
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear'=>true],
        ],
        'filterInputOptions' => ['placeholder'=>'MA / CPF'],
        
        'hAlign' => GridView::ALIGN_CENTER,
        
        'hidden' => false,
        'hiddenFromExport' => true
    ],
    
    /* Export-only fields */
    [
        'label' => Yii::t('app', 'MA Assessment'),
        
        'class' => \app\utilities\CaTextBoolDataColumn::className(),
        'attribute' => 'is_done',
        
        'hidden' => true,
        'hiddenFromExport' => false,
        'filter' => false,
        'mergeHeader' => true
    ],
    [
        'label' => Yii::t('app', 'CPF Assessment'),
        
        'class' => \app\utilities\CaTextBoolDataColumn::className(),
        'attribute' => 'cpf_assessed',
        
        'hidden' => true,
        'hiddenFromExport' => false,
        
        'filter' => false,
        'mergeHeader' => true
    ],
    /* End export-only fields */
    
    
    [
        'class' => \app\utilities\CaRegModDataColumn::className(),
        'attribute' => 'deadline_status',
        'label' => \Yii::t('app', 'Deadline'),
        'width' => '100px;',
        'filterType' => \kartik\grid\GridView::FILTER_SELECT2,
        'filter' => [
            '5' => Yii::t('app', 'in due time'),
            '4' => Yii::t('app', 'late'),
            ],
        'filterWidgetOptions'=>[
                'pluginOptions'=>['allowClear'=>true],
            ],
        'filterInputOptions'=>['placeholder'=>Yii::t('app', 'Status')],
    ],

    [
        'attribute' => 'op_submitted_at',
        'format' => ['date', 'php: Y-m-d H:i:s'],
        'label' => ucfirst(\Yii::t('app', 'submitted')),
        'width' => '210px;',
          'filterType' => \yii\widgets\MaskedInput::className(),
          'filterWidgetOptions' => [
              'mask'=>'(9999)|*-(99)|*-(99)|*',
              //'mask'=>'(9999)|*-(99)|*-(99)|* (99)|*:(99)|*:(99)|*',
              'definitions'=>[
                  '*'=>[
                      'validator'=>'\*',
                      'cardinality'=>1,
                  ]
              ],
          ],
        'filterInputOptions' => ['placeholder'=>'Y-m-d', 'class'=>'form-control'],
    ],
                
                
                
    [
        'class' => \kartik\grid\DataColumn::className(),
        'attribute' => 'event_date_time',
        'format' => ['date', 'php:Y-m-d H:i:s'],
        'label' => \Yii::t('app/crf', 'Event date/time'),
        'width' => '110px;',
        
        'content' => function($model, $key, $index, $column) {
            $s = '<span style="">'. $model->event->event_date_time . '</span>';
            return $s; //->operator->name;    
        },
                
          'filterType' => \yii\widgets\MaskedInput::className(),
          'filterWidgetOptions' => [
              'mask'=>'(9999)|*-(99)|*-(99)|*',
              //'mask'=>'(9999)|*-(99)|*-(99)|* (99)|*:(99)|*:(99)|*',
              'definitions'=>[
                  '*'=>[
                      'validator'=>'\*',
                      'cardinality'=>1,
                  ]
              ],
          ],
        'filterInputOptions' => ['placeholder'=>'Y-m-d', 'class'=>'form-control'],
    ],
    
    [
        'class' => \kartik\grid\DataColumn::className(),
        'attribute' => 'operator',
        
        'label' => ucfirst(\Yii::t('app', 'Operator')),
        'content' => function($model, $key, $index, $column) {
            $url = \Yii::$app->urlManager->createUrl([
                'management/operators-owners-organization/view', 'id'=>$model->event->organization->id
            ]);
            $s = Html::a($model->event->organization->organization_acronym, $url);
    
            return $s; //->operator->name;    
        },
        'filterInputOptions' => ['placeholder'=>\Yii::t('app', 'acronym'), 'class'=>'form-control'],
                
        'hidden' => false,
        'hiddenFromExport' => true,
    ],

    [
        'class' => \kartik\grid\DataColumn::className(),
        'attribute' => 'operator',
        
        'label' => ucfirst(\Yii::t('app', 'Operator')),
        'content' => function($model, $key, $index, $column) {
            $s = $model->event->organization->organization_acronym;
    
            return $s; //->operator->name;    
        },
        'format' => 'text',
                
        'hidden' => true,
        'hiddenFromExport' => false,
        
        'filter' => false,
        'mergeHeader' => true
                
    ],
                
                
    [
        'class' => \kartik\grid\DataColumn::className(),
        'attribute' => 'installation',
        //'format' => ['date', 'php:Y-m-d h:i:s'],
        
        'label' => ucfirst(\Yii::t('app', 'installation')) . '/' . ucfirst(\Yii::t('app', 'type')),
        'content' => function($model, $key, $index, $column) {
        //http://syrio.avb:8080/web/index.php?r=management%2Finstallations%2Fview&id=IN0000000001&operator_id=OO1442250179
            $url = \Yii::$app->urlManager->createUrl([
                'management/installations/view', 
                'id'=>$model->event->installation->id,
                'operator_id'=>$model->event->organization->id
            ]);
            $s = Html::a($model->event->installation_name_type, $url);
    
            return $s; //->operator->name;    
        },
        
        'hidden' => false,
        'hiddenFromExport' => true,
                
        'filterInputOptions' => ['placeholder'=>\Yii::t('app', 'name or type'), 'class'=>'form-control'],
    ],

    [
        'class' => \kartik\grid\DataColumn::className(),
        'attribute' => 'installation',
        
        'label' => ucfirst(Yii::t('app', 'installation')),
        'content' => function($model, $key, $index, $column) {
        //http://syrio.avb:8080/web/index.php?r=management%2Finstallations%2Fview&id=IN0000000001&operator_id=OO1442250179
            $s = $model->event->installation_name_type;
    
            return $s; 
        },
                
        'format' => 'text',
                
        'hidden' => true,
        'hiddenFromExport' => false,
        
        'filter' => false,
        'mergeHeader' => true
    ],

                
                
                
    [
        'class' => app\utilities\EventTypesDataColumn::className(),
        'attribute' => 'event',
        'label' => \Yii::t('app/crf', 'Event categorization'),
        //'content' => function($model, $key, $index, $column) {
        //    return var_dump($model->event->report);
        //}
          'filterType' => \yii\widgets\MaskedInput::className(),
          'filterWidgetOptions' => [
              'mask'=>'a[\,a][\,a][\,a][\,a][\,a][\,a][\,a][\,a][\,a]',
              //'mask'=>'(9999)|*-(99)|*-(99)|* (99)|*:(99)|*:(99)|*',
              'definitions'=>[
                  ','=>[
                      'validator'=>'\,',
                      'cardinality'=>1,
                  ]
              ],
          ],
        'filterInputOptions' => ['placeholder'=>\Yii::t('app', 'comma-delimited letters (a to j)'), 'class'=>'form-control'],

        'hidden' => false,
        'hiddenFromExport' => true
    ],
    
    /* Export-only fields */
    [
        'label' => Yii::t('app/crf', 'Event categorization'),
        
        'class' => app\utilities\EventTypesDataColumnExport::className(),
        'attribute' => 'event',
        
        'hidden' => true,
        'hiddenFromExport' => false,
        
        'filter' => false,
        'mergeHeader' => true
       
    ],
                
                
    [
        'class' => \kartik\grid\ActionColumn::className(),
        'header' => ucfirst(Yii::t('app', 'actions')),
        'template' => '{view} {delete}',
        'vAlign' => 'top',
    ]
];

?>

