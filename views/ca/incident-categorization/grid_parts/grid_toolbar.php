<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;

/*
Grid toolbar section ================================================================
*/
$grid_toolbar = [
        [
            'content'=>
                Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], [
                    'class' => 'btn btn-default', 
                    'title' => Yii::t('app', 'Reset filters')
                ]),
            'options' => ['class' => 'btn-group']
        ],
        '{export}',
        //'{toggle-data}'
    ];   //end grid toolbar



?>
 
 