<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ca\IncidentCategorization */


$draft = $model->draft;
$event = $draft->event;
$dt = new DateTime($event->event_date_time);

/* use this on any view to include the actionmessage logic and widget */
//include Yii::getAlias('@app') . '\views\shared_parts\message_snippet.php';
include Yii::getAlias('@app') . '/views/shared_parts/message_snippet.php';

?>

<div class="incident-categorization-view">

    <h1><?= Html::encode($model->name) ?></h1>
    <h3><?= ucfirst(Yii::t('app', 'submitted')) . ': ' . $model->op_submitted_at ?><br/>
    <?= Yii::t('app', 'by') . ': ' . $model->draft->event->raporteur_name ?> <em>(<?=  $model->draft->event->raporteur_role  ?>)</em></h3>
    <p>
        <?php
        
        $buttons = [];
        
        if (\Yii::$app->user->can('ca_assessor')) {
            if ($model->draft->ca_status == \app\models\events\EventDeclaration::INCIDENT_REPORT_REJECTED) {
                $message = new app\models\common\ActionMessage();
                $message->SetWarning(
                        Yii::t('app', 'This report has been rejected with the following justification:'),
                        $model->draft->ca_rejection_desc);
                
                echo \app\widgets\ActionMessageWidget::widget(['actionMessage' => $message,]);
                
                $buttons[] = Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                        'method' => 'post',
                    ],
                ]);
                
            }
            else if ($model->draft->ca_status == \app\models\events\EventDeclaration::INCIDENT_REPORT_REPORTED &&
                    isset($model->draft->ca_rejection_desc) && strlen($model->draft->ca_rejection_desc) > 0) {
                $message = new app\models\common\ActionMessage();
                $message->SetWarning(
                        Yii::t('app', 'This report has been re-submitted. Previous rejection justification:'),
                        $model->draft->ca_rejection_desc);
                
                echo \app\widgets\ActionMessageWidget::widget(['actionMessage' => $message,]) . ' ';
                
                $buttons[] = Html::a(Yii::t('app', 'Assess'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']);
                $buttons[] = Html::a(Yii::t('app', 'Reject'), ['reject', 'id' => $model->id], ['class' => 'btn btn-danger']);
//                $s = '<ul class="list list-inline">'
//                        . '<li>'
//                        . Html::a(Yii::t('app', 'Assess'. ' '), ['update', 'id' => $model->id], ['class' => 'btn btn-primary'])
//                        . '</li>'
//                        . '<li>'
//                        . Html::a(Yii::t('app', 'Reject'), ['reject', 'id' => $model->id], ['class' => 'btn btn-danger'])
//                        . '</li>'
//                        . '</ul>';
//                
//                echo $s;
                
            }
            else if (($model->draft->ca_status == \app\models\events\EventDeclaration::INCIDENT_REPORT_ACCEPTED && $model->is_done == 1)) {
                //do nothing
                $buttons[] = Html::a(Yii::t('app', 'Re-assess'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']);
            }
            else if (($model->draft->ca_status == \app\models\events\EventDeclaration::INCIDENT_REPORT_ACCEPTED && $model->is_done != 1)) {
                //do nothing
                $buttons[] = Html::a(Yii::t('app', 'Continue assessment'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']);
            } else
            {
                $buttons[] = Html::a(Yii::t('app', 'Assess'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']);
                $buttons[] = Html::a(Yii::t('app', 'Reject'), ['reject', 'id' => $model->id], ['class' => 'btn btn-danger']);
                //echo Html::a(Yii::t('app', 'Assess') . ' ', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) . ' ';
                //echo Html::a(Yii::t('app', 'Reject'), ['reject', 'id' => $model->id], ['class' => 'btn btn-danger']);
            }
            
            //draw the toolbar
            $s = '<ul class="list list-inline">';
            foreach($buttons as $button) {
                $s .= '<li>'.$button.'</li>';
            }
            $s.='</li>';
            
            echo $s;
            
        } else if (\Yii::$app->user->can('ca_user')) {
            //ca_user
            if ($model->draft->ca_status == \app\models\events\EventDeclaration::INCIDENT_REPORT_REJECTED) {
                $message = new app\models\common\ActionMessage();
                $message->SetWarning(
                        Yii::t('app', 'This report has been rejected with the following justification:'),
                        $model->draft->ca_rejection_desc);
                
                echo \app\widgets\ActionMessageWidget::widget(['actionMessage' => $message,]);
            }
            else if ($model->draft->ca_status == \app\models\events\EventDeclaration::INCIDENT_REPORT_REPORTED &&
                    isset($model->draft->ca_rejection_desc) && strlen($model->draft->ca_rejection_desc) > 0) {
                $message = new app\models\common\ActionMessage();
                $message->SetWarning(
                        Yii::t('app', 'This report has been re-submitted. Previous rejection justification:'),
                        $model->draft->ca_rejection_desc);
                
                echo \app\widgets\ActionMessageWidget::widget(['actionMessage' => $message,]);
            }
        }
        
        ?>

        <?php 
          // Html::a(Yii::t('app', 'pdf'), ['pdf', 'id' => $model->id], ['class' => 'btn btn-success'])
          //(start) Bug 8 - Add button of "Generate PDF" in Incident Categorisation View -->
          // modified by vamanbo 20150910 - simpler authentication
          //$roles = Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId());
          //var_dump($roles);
          
          $user = \Yii::$app->user->identity;
          
          if ($user->isCaUser && $user->status == \app\models\User::STATUS_ACTIVE)
          {
            echo Html::a(Yii::t('app', '<i class="fa fa-file-pdf-o"></i> PDF'), 
              ['pdfgenerator/default/index', 'id' => $model->draft_id], 
              ['class' => 'btn btn-default']); 
          };
          //die();
          //(end) Bug 8 - Add button of "Generate PDF" in Incident Categorisation View  -->
        ?>
        
    </p>

<div id='def'>
    <h1><?= $user->isCaUser ? '' : $model->name ?></h1>

    <h3><?= Yii::t('app/crf', 'Event date and time') ?></h3>
    (a) <?= Yii::t('app/crf', 'Event date') ?>: <?= date_format($dt, 'd-m-Y') ?>
    <br/>
    (b) <?= Yii::t('app/crf', 'Event time') ?>: <?= date_format($dt, 'H:i:s') ?>


    <h3><?= Yii::t('app/crf', 'Details of the location and of the person reporting the event') ?></h3>

    <table class='sy_simple-table'>
        <tr>
            <td><?= $event->attributeLabels()['operator'] ?></td>
            <td><?= $event->operator ?></td>
        </tr>
        <tr>
            <td><?= $event->attributeLabels()['installation_name_type'] ?></td>
            <td><?= $event->installation_name_type ?></td>
        </tr>
        <tr>
            <td><?= $event->attributeLabels()['field_name'] ?></td>
            <td><?= $event->field_name ?></td>
        </tr>
        <tr style='min-height: 32px; max-height: 32px; height: 32px;'>
            <td colspan='2'></td>
        </tr>
        <tr>
            <td><?= $event->attributeLabels()['raporteur_name'] ?></td>
            <td><?= $event->raporteur_name ?></td>
        </tr>
        <tr>
            <td><?= $event->attributeLabels()['raporteur_role'] ?></td>
            <td><?= $event->raporteur_role ?></td>
        </tr>
        <tr style='min-height: 32px; max-height: 32px; height: 32px;'>
            <td colspan='2'></td>
        </tr>
        <tr>
            <td colspan='2'><strong><?= Yii::t('app/crf','Contact details') ?></strong></td>
        </tr>
        <tr>
            <td><?= $event->attributeLabels()['contact_tel'] ?></td>
            <td><?= $event->contact_tel ?></td>
        </tr>
        <tr>
            <td><?= $event->attributeLabels()['contact_email'] ?></td>
            <td><?= $event->contact_email ?></td>
        </tr>
    </table>

    <br/>
    <h3><?= Yii::t('app/crf', 'Event categorization') ?></h3>
    <p><em><?= Yii::t('app/crf', 'What type of event is being reported?') ?></em></p>
    <?= $this->render('/events/drafts/event-draft/view_all', ['model'=>$draft]) ?>
    
    <div style="margin-bottom: 44px;">
        <?php include 'viewSections/sectionA.php';?>
    </div>

    <div style="margin-bottom: 44px;">
        <?php include 'viewSections/sectionB.php';?>
    </div>
    
    <div style="margin-bottom: 44px;">
        <?php include 'viewSections/sectionC.php';?>
    </div>

    <div style="margin-bottom: 44px;">
        <?php include 'viewSections/sectionD.php';?>
    </div>

    <div style="margin-bottom: 44px;">
        <?php include 'viewSections/sectionE.php';?>
    </div>

    <div style="margin-bottom: 44px;">
        <?php include 'viewSections/sectionF.php';?>
    </div>

    <div style="margin-bottom: 44px;">
        <?php include 'viewSections/sectionG.php';?>
    </div>

    <div style="margin-bottom: 44px;">
        <?php include 'viewSections/sectionH.php';?>
    </div>
    
    <div style="margin-bottom: 44px;">
        <?php include 'viewSections/sectionI.php';?>
    </div>
    
    <div>
        <?php include 'viewSections/sectionJ.php';?>
    </div>
    
</div>

</div>
</div>

<?php

//put these at the end to override the sections' settings
$this->title = $model->draft->event->operator . ' / ' . $model->draft->event->event_date_time;
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Reported incidents'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;


?>