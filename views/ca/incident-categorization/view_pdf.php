<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\widgets\DetailView;

$draft = $model->draft;
$event = $draft->event;
$dt = new DateTime($event->event_date_time);

?>

<div class="incident-categorization-view">


    <div id='def'>
        <h1><?= $model->draft->event->event_name ?></h1>

        <!-- render the event details -->
        <h3>Event date and time</h3>
        (a) Event date: <?= date_format($dt, 'd-m-Y') ?>
        <br/>
        (b) Event time: <?= date_format($dt, 'H:i:s') ?>


        <h3>Details of the location and person reporting the event</h3>
        <?= DetailView::widget([
            'model' => $event,
            'attributes' => [
                'operator',
                'installation_name_type',
                'field_name',
                'raporteur_name',
                'raporteur_role',
                'contact_tel',
                'contact_email:email',
            ],
        ]) ?>

        <h3>Event categorization</h3>
        <p><em>What type of event is being reported</em></p>
        <?= $this->render('/events/drafts/event-draft/view_all', ['model'=>$draft]) ?>

        <div style="margin-bottom: 44px;">
            <?php include 'viewSections/sectionIpdf.php';?>
        </div>

        <p><em><strong><?= \Yii::t('app/crf', 'END OF THE REPORT') ?></strong></em></p>


    </div>

</div>

