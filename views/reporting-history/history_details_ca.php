<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ReportingHistorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $evt_id integer the id of the Event */
/* @var $incident_cat_id integer the id of the Incident Categorization */

require_once 'gridview_settings/sys_admin/_definition_history_details.php';

$url = \Yii::$app->urlManager->createUrl(['/ca/incident-categorization/clear-history', 'id'=>$incident_cat_id]);

?>
<div class="reporting-history-index">
    <div class='row'>
        
        <?php if (isset($message)) {
            ?>
        
        <div class='col-lg-12'>
            <div class='alert alert-warning' role='alert'>
                <?= Html::encode($message) ?>
            </div>
        </div>
        
        <?php
        }
        
        
        if ($dataProvider->count !== 0 && (\Yii::$app->user->can('ca_admin') || \Yii::$app->user->can('sys_admin'))) {
        ?>
        
        <div class='col-lg-12' id='firstrow_<?= $evt_id ?>'>
            <div class='btn-group'>
                <?= Html::a(\Yii::t('app', 'Clear'), $url, [
                    'id' => 'btn-clear-all',
                    'evt_id' => $evt_id,
                    'class' => 'btn btn-danger',
                    'data-confirm' => \Yii::t('app', 'Clear history?'),
                    'data-method' => 'post'
                ]) ?>
            </div>
        </div>
        <?php 
        
        } ?>
        <div class='col-lg-12' id='secondrow_<?= $evt_id ?>'>
            <?= GridView::widget([
                'id' => 'grid_' . $evt_id,
                
                'pjax' => true,
                'dataProvider' => $dataProvider,
                'filterModel' => null,
                
                'condensed' => true,
                
                'columns' => $columns,
                
                'showPageSummary' => false,
                
                'rowOptions' => function($model, $key, $index, $grid) {
                    //the model is History
                }
                
            ]); ?>
        </div>
        
    </div>
    
    
</div>

<?php

//require 'gridview_settings/sys_admin/_scripts.php';

?>