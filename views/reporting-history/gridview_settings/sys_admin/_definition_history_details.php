<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use kartik\grid\GridView;
use app\assets\SyrioGlyphsAsset;
use app\models\ReportingHistory;

/**
 * Description of _definition
 *
 * @author vamanbo
 */


SyrioGlyphsAsset::register($this);

$columns = [
    [
        'class' => kartik\grid\SerialColumn::className(),
        'vAlign' => 'top'
    ],
    
    [
        'class' => kartik\grid\DataColumn::className(),
        'format' => 'html',
        'hAlign' => 'center',
        'width' => '36px',
        'content' => function ($model, $key, $index, $column) {
            switch ($model->event) {
                case ReportingHistory::EVENT_REGISTERED:
                case ReportingHistory::EVENT_FINALIZED:
                case ReportingHistory::EVENT_SIGNED;
                case ReportingHistory::EVENT_REOPENED:
                case ReportingHistory::EVENT_SUBMITTED:
                case ReportingHistory::EVENT_SECTION_FINALIZED:
                case ReportingHistory::EVENT_SECTION_REOPENED:
                case ReportingHistory::EVENT_DRAFT_CREATED:
                case ReportingHistory::EVENT_DRAFT_DELETED:
                case ReportingHistory::EVENT_DRAFT_RESTORED:
                    return '<span class="icon-syrio_glyphs"></span>';
                    break;
                case ReportingHistory::EVENT_REJECTED:
                case ReportingHistory::EVENT_ACCEPTED:
                case ReportingHistory::EVENT_ASSESSED:
                case ReportingHistory::EVENT_ASSESSED_FOR_CPF:
                case ReportingHistory::EVENT_CPF_REOPENED :
                case ReportingHistory::EVENT_CPF_RESUMED :
                case ReportingHistory::EVENT_CPF_SUSPENDED :
                case ReportingHistory::EVENT_MA_REOPENED :
                    return '<span class="fa fa-university"></span>';
                    break;
                default:
                    return '<span class="fa fa-question"></span>';
            }
    
        }
    ],
    
    [
        'class' => kartik\grid\DataColumn::className(),
        'attribute' => 'evt_id',
        'label' => \Yii::t('app', 'Date/Time'),
        'format' => ['date', 'php:D, Y-m-d H:i:s']
    ],

    //event id column
    //event id column => as date time
    //[
    //    'class' => kartik\grid\DataColumn::className(),
    //    'attribute' => 'edt',
    //    'value' => 'offshoreIncident.event_date_time',
    //    'label' => \Yii::t('app/crf', 'Event date/time'),
        //'format' => ['date', 'php:Y-m-d h:i:s']
    //],
    
    //event type column
    [
        'class' => kartik\grid\DataColumn::className(),
        'attribute' => 'event',
        'label' => \Yii::t('app', 'Action'),
        'value' => function ($model, $key, $index, $column) {
            try {
                return \app\models\ReportingHistory::getLogDescription()[$model->event];
            } catch (Exception $ex) {
                return YII_DEBUG ? 'unknown event type. please add it to getLogDescription.' : 'unknown event type. please contact system admin.';
                //return YII_DEBUG ? 'unknown event type. please add it to getLogDescription.' . '<pre>'. var_dump($ex) . '</pre>' : 'unknown event type. please contact system admin.';
            }
        }
    ],
            
    [
        'class' => kartik\grid\DataColumn::className(),
        'attribute' => 'description',
        'format' => 'html',
        'value' => function ($model, $key, $index, $column) {
            try {
                $desc_arr = \yii\helpers\Json::decode($model->description);
                
                $s = '<ul class="list list-unstyled">';
                foreach($desc_arr as $key=>$value) {
                    $s .= '<li>'
                            . '<span class="text-info">' . \Yii::t('app', $key) . ': </span>'
                            . nl2br(\yii\helpers\Html::encode($value)) 
                            . '</li>';
                }
                
                $s.='</ul>';
                
                return $s;
            } catch (Exception $ex) {
                //return YII_DEBUG ? 'unable to parse message.' . '<pre>'. var_dump($ex) . '</pre>' : 'unknown message format. please contact system admin.';
                return YII_DEBUG ? 'unable to parse message.' : 'unknown message format. please contact system admin.';                
            }
        }
    ]
    
];