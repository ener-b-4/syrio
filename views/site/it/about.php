<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;

/* @var $this yii\web\View */
$this->title = 'Chi siamo';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="lead sy_pad_top_36">
        La Piattaforma
    </div>
    <p>
        <strong>SyRIO</strong> <em> (<b>Sy</b>stem for <b>R</b>eporting the <b>I</b>ncidents in <b>O</b>ffshore Oil&Gas Activities)</em> è una piattaforma web progettato principalmente per assistere le offshore Operatori/Proprietari e le Autorità Competenti
        rispetto delle disposizioni in materia di segnalazione degli incidenti in mare aperto, come previsto dalla direttiva 2013/30/EU (la direttiva Offshore) e del regolamento 1112/2014.
    </p>

    <p>
        <strong>SyRIO</strong> fornisce l'ambiente protetto che consente una comunicazione sicura e sonora degli incidenti da parte degli Operatori/Proprietari per l'Autorità Competente, in seguito il modello di rendicontazione proposta dal Common Reporting Format (CRF).
    </p>
    
    <p>
        La piattaforma è stata sviluppata presso il <strong>European Commission's Joint Research Centre - Direzione per l'energia, i trasporti e il clima</strong> (<?= Html::a('EC JRC - ETC', 'https://ec.europa.eu/jrc/en') ?> ).
    </p>

    <div class='lead sy_pad_top_36'>
        Hosting & Versione
    </div>
    <p>
        La piattaforma corrente è ospitata dalla Autorità Competente di
        <span class='text-info'>
            <?= Html::encode(app\modules\management\models\Country::findOne(['iso2'=>\Yii::$app->params['competentAuthorityIso2']])->short_name) ?>
        </span> 
        (<em class='text-info'>
            <?= \Yii::$app->params['competentAuthority'] ?>
        </em>) 
        
        <?= Html::a('<span class="glyphicon glyphicon-envelope"></span>', \Yii::$app->urlManager->createUrl(['//site/contact'])) ?>
        
    </p>
    
    <p>
        <b>Versione: </b> <?= Yii::$app->params['version'] ?> <em>build </em> <?= Yii::$app->params['build'] ?>
    </p>
</div>
