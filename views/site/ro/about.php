<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;

/* @var $this yii\web\View */
$this->title = 'Despre';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="lead sy_pad_top_36">
        Platforma
    </div>
    <p>
        <strong>SyRIO</strong> <em> (<b>Sy</b>stem for <b>R</b>eporting the <b>I</b>ncidents in <b>O</b>ffshore Oil&Gas Activities)</em> este o platformă WEB care are ca scop asistarea atât a Operatorilor/Proprietarilor cât și a Autorităților Competente 
        în îndeplinrea sarcinilor legate de raportarea incidentelor offshore prevăzute în Directiva 2013/30/UE.
    </p>

    <p>
        <strong>SyRIO</strong> oferă mediul securizat ce permite operatorilor/proprietarilor transmiterea în condiții de siguranță și confidențialitate a rapoartelor de incident către Autoritatea competentă, utilizând formatul comun de raportare (Common Reporting Format - CRF).
    </p>
    
    <p>
        Platforma a fost dezvoltată de către <strong>Centrul Comun de Cercetare al Comisiei Europene - Directoratul pentru energie, transport și climă</strong> (<?= Html::a('EC JRC - ETC', 'https://ec.europa.eu/jrc/en') ?> )
    </p>

    <div class='lead sy_pad_top_36'>
        Găzduire și Versiune
    </div>
    <p>
        Platforma de față este găzduită de Autoritatea Competentă a
        <span class='text-info'>
            <?= Html::encode(app\modules\management\models\Country::findOne(['iso2'=>\Yii::$app->params['competentAuthorityIso2']])->short_name) ?>
        </span> 
        (<em class='text-info'>
            <?= \Yii::$app->params['competentAuthority'] ?>
        </em>) 
        
        <?= Html::a('<span class="glyphicon glyphicon-envelope"></span>', \Yii::$app->urlManager->createUrl(['//site/contact'])) ?>
        
    </p>
    
    <p>
        <b>Versiunea: </b> <?= Yii::$app->params['version'] ?> <em>build </em> <?= Yii::$app->params['build'] ?>
    </p>
</div>
