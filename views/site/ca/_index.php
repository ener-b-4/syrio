<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use app\components\helpers\DataGrid\CustomGridView;


/* @var $this yii\web\View */
$this->title = 'SyRIO (CA)';

$incidentsStat = Yii::$app->user->getIdentity()->caIncidents;



$year = date("Y");
$incidents2015 = Yii::$app->user->getIdentity()->getCaIncidentsInYear($year);

//echo '<pre>';
//echo var_dump($incidents2015);
//echo '</pre>';
//die();
?>


    <div class="container" style="padding-top:6px;">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <h3><?= \Yii::t('app', 'Statistics') ?></h3>
                <p>
                    <span class="text-info"><?= Yii::t('app', 'Incidents recorded in {evt}:', ['evt'=>$year]) ?>  </span> <?= count($incidents2015) ?><br/>
                    <span class="text-info"><?= Yii::t('app', 'of which classified as Major Incidents:', ['evt'=>$year]) . ' ' ?>  </span> <?= Yii::$app->user->getIdentity()->major ?><br/>
                </p>
                
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="lead">
                            <?= \Yii::t('app', 'Incident reports') ?>
                        </div>
                        <p><?= Html::a(ucfirst(Yii::t('app', 'view {evt}', ['evt' => Yii::t('app', 'incident reports')])), ['/ca/incident-categorization'], [
                            'class' => 'btn btn-success btn-block']); ?>
                        </p>
                        <p class="text-muted">
                            <?= Html::encode(\Yii::t('app', 'Displays the list of the incidents reported as required by Directive 2013/30/EU.')) ?>
                        </p>
                    </div>
                </div>
                
                <?php if (\Yii::$app->user->can('ca_user')) { ?>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="lead">
                            <?= \Yii::t('app', 'Annual report') ?>
                        </div>
                        <p class="text-muted">
                            <em>
                                <?= Html::encode(\Yii::t('app', 'Tools for drafting the CA Annual Report to be submitted to the Commission, as required by Directive 2013/30/EU.')) ?>
                            </em>
                        </p>
                        
                        <p><?php echo Html::a(ucfirst(Yii::t('app', 'annual report preparation')), ['/cpfbuilder/cpf-crf-assessments'], [
                            'class' => 'btn btn-warning btn-block']); ?></p>
                        <p class="text-muted">
                            <?= Html::encode(\Yii::t('app', 'Assessment of the submitted incidents, case by case, in terms of Section 4 of the Common Publication Format.')) ?>
                            <br/>
                            <br/>
                        </p>
                        <p><?php echo Html::a(ucfirst(Yii::t('app', 'annual report drafting')), ['/cpfbuilder/cpf-sessions'], [
                            'class' => 'btn btn-primary btn-block']); ?></p>
                        <p class="text-muted">
                            <?= Html::encode(\Yii::t('app', 'The tools for drafting the CA Annual Report. Export the Annual report as XML.')) ?>
                        </p>
                        <p class="text-warning">
                            <?= Html::encode(\Yii::t('app', 'Please make sure that all the reported incidents have been assessed in terms of CPF Section 4 (i.e. Annual report preparation is done and up-to-date).')) ?>
                        </p>
                    </div>
                </div>
                <?php } //end if can generate the annual report ?>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12 text-left">
                
                <h2>
                    Tasks
                </h2>
                
                
                <?php
                    $unassessed=count($incidentsStat);
                    if ($unassessed == 0)
                    {
                        $s='<span class="glyphicon glyphicon-ok" style="color: #090; margin-right: 6px;"></span>'
                                . 'There are no incidents to assess as MA.';
                        echo '<h3>' . $s . '</h3>';
                    }
                    else
                    {
                        $s='<span class="glyphicon glyphicon-flag sy_alert_color" style="margin-right: 6px;"></span>'
                                . 'There are '
                                . $unassessed
                                . ' incidents pending for being assessed.';
                        echo '<h3>' . $s . '</h3>';
                        
//echo '<pre>';
//echo var_dump($incidentsStat);
//echo '</pre>';
                        
                        
                        $s='<table>';
                        $ss='';
                        
                        foreach($incidentsStat as $incident)
                        {
                            $url=Yii::$app->getUrlManager()->createUrl(['ca/incident-categorization/view', 'id'=>$incident->id]);
                            $inst_name = isset($incident->draft->event->installation_name_type) ? ($incident->draft->event->installation_name_type . ' / ') : '';
                                $ss .= '<tr>'
                                . '<td>'
                                        . '<a href="' . $url . '" class="btn btn-link">display>> </a>'
                                . '</td>'
                                . '<td>'
                                        . $incident->draft->event->operator 
                                        . ' / '
                                        . $inst_name
                                        . $incident->draft->event->event_date_time
                                . '</td>'
                                . '</tr>';
                        }
                        $s.= $ss;
                        $s.= '</table>';
                        echo $s;
                    }
//                    echo Html::a('View list', ['launch-ca'], ['class' => 'btn btn-success']);
                ?>
                
            </div>
        </div>

        

    </div>