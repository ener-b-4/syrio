<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

/**
 * Description of build_menu_items
 *
 * @author vamanbo
 */

    $items = [
            ['label' => ucfirst(Yii::t('app', 'home')), 'url' => ['/site/index'], 'options' => ['encodeLabels'=>false]],
            ['label' => ucfirst(Yii::t('app', 'requests')), 'url' => ['/site/requests'], 'options' => ['encodeLabels'=>false]]
        ];

    if (!Yii::$app->user->isGuest)
    {
        if (Yii::$app->user->identity->isOperatorUser || Yii::$app->user->identity->isInstallationUser)
        {
            $items[] = ['label' => ucfirst(Yii::t('app', 'incidents')), 'items' => [
                ['label' => ucfirst(Yii::t('app', 'hub')), 'url' => ['/events/event-declaration']],
                ['label' => ucfirst(Yii::t('app', 'register new {evt}', [
                    'evt'=>\Yii::t('app', 'incident')])), 'url' => ['/events/event-declaration/create']],
            ]];
        }
    }

    if (Yii::$app->user->can('sys_admin'))
    {
        /*
        $items[] = ['label' => Yii::t('app', 'Competent Authorities'), 'items' => [
            ['label' => Yii::t('app', 'Manage'), 'url' => ['/ca/competent-authority/index']],
            ['label' => Yii::t('app', 'Registration requests'), 'url' => ['requests/ca-org-register']],
        ]];
         * 
         */
        $items[] = ['label' => 'RBAC Management', 'url' => ['/site/rbac'], 'items' => [
                    ['label' => ucfirst(\Yii::t('app','roles')), 'url' => ['/site/roles', 'tag' => 'roles']],
                    ['label' => ucfirst(\Yii::t('app','permissions')), 'url' => ['/site/permissions', 'tag' => 'permissions']],
                    ['label' => ucfirst(\Yii::t('app','rules')), 'url' => ['/site/rules', 'tag' => 'rules']],
                    ['label' => ucfirst(\Yii::t('app','assignments')), 'url' => ['/site/assignments', 'tag' => 'assignments']],
                   ]];
        $items[] = ['label' => ucfirst(Yii::t('app', 'units')), 'url' => ['/units/']];
    }

    if (Yii::$app->user->can('organization-list'))
    {
        $items[] = ['label' => ucfirst(Yii::t('app', 'admin')), 'items' => [
                    ['label' => Yii::t('app', 'Management Hub'), 'url' => ['/management/']],
                    '<li role="presentation" class="dropdown-header">' . Yii::t('app', 'Users') . '</li>',
                    ['label' => ucfirst(Yii::t('app', 'manage users')), 'url' => ['/admin/users/user/index']],
                    [
                        'label' => ucfirst(Yii::t('app', 'register user')), 'url' => ['/admin/users/user/create-user']
                    ],
                    '<li role="presentation" class="dropdown-header">' . ucfirst(Yii::t('app', 'organizations')) . '</li>',
                    ['label' => Yii::t('app', 'Manage Organizations'), 'url' => ['/management/organization/index']],
                    //['label' => 'Rules', 'url' => ['/site/rules', 'tag' => 'rules']],
                    //['label' => 'Assignments', 'url' => ['/site/assignments', 'tag' => 'assignments']],
                   ]];
    }

    if (!Yii::$app->user->isGuest)
    {
        foreach (Yii::$app->user->identity->getRoles() as $role)
        {
            $s = $role . '<br/>';
        }
    }
    else {
        $s = '';
    }

    
    $userCard = '
    <div style="background-color: #080808; padding:6px; margin-top:-6px; margin-bottom:6px; color: lightgray">
        <small class="text-info">Logged in as:</small><br/>'
        . $s .
    '</div>';
    
    
    Yii::$app->user->isGuest ?
        $items[] = ['label' => ucfirst(Yii::t('app', 'login')), 'url' => ['/site/login']] :
        $items[] = [
          '<li role="presentation" class="dropdown-header">' . ucfirst(Yii::t('app', 'users')) . '</li>',
          'label'=>Yii::$app->user->identity->username, 'items' => [
                $userCard,
                ['label' => ucfirst(Yii::t('app', 'logout')), 
                    'url' => ['/site/logout'],
                    'linkOptions' => ['data-method' => 'post']
                    ],
                '<li role="separator" class="divider"></li>',
                ['label' => ucfirst(Yii::t('app', 'account')), 
                    'url' => ['/admin/users/user/account'],
                    //'linkOptions' => ['data-method' => 'get']
                    ],
          ],  
        ];
    
    
