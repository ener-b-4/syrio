<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

$this->title = Yii::t('app', 'Contact Developers');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-contact">
    <h1><?= Html::encode($this->title) ?></h1>

    <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>

    <div class="alert alert-success">
        <?= Html::encode(\Yii::t('app', 'Your message has been sent to the developers.')) ?>
    </div>

    <?php else: ?>

    <p>
        <?= Html::encode(Yii::t('app', 'Use this form to contact SyRIO developers. Please provide any inquiries and / or report any bugs encountered. Thank you.')) ?>
        
    </p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>
                <?= $form->field($model, 'name')->hiddenInput()->label('', ['class' => 'hidden']) ?>
                <?= $form->field($model, 'email')->hiddenInput()->label('', ['class' => 'hidden']) ?>
                <?= $form->field($model, 'name')->textInput(['disabled'=>'disabled']) ?>
                <?= $form->field($model, 'email')->textInput(['disabled'=>'disabled']) ?>
                <?= $form->field($model, 'subject') ?>
                <?= $form->field($model, 'body')->textArea(['rows' => 6]) ?>
                <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                    'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
                ]) ?>
                <div class="form-group">
                    <?= Html::submitButton(Yii::t('app/commands', 'Submit'), ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>

    <?php endif; ?>
</div>
