<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */


/**
 * Description of restricted-area
 *
 * @author vamanbo
 */

/* @var $this yii\web\View */
/* @var $code integer */

use yii\helpers\Html;

?>

<?php

switch ($code)
{
    case app\components\Errors::RESTRICTED_OPERATION_ERROR:
        $loggedinTitle = Yii::t('app/error', 'Restricted operation');

        $loggedinMessage = Yii::t('app/error', 'You do not have enough credentials to perform this operation.');
        $loggedinMessage .= '<br/>';
        $loggedinMessage .= '<br/>';
        $loggedinMessage .= Yii::t('app/error', 'Please login with a different account; or');
        $loggedinMessage .= '<br/>';
        $loggedinMessage .= Yii::t('app/error', 'Contact the SyRIO administrator to request elevating your rights.');

        $guestMessage = Yii::t('app/error', 'Please login first.');
        
        break;
    default:
        $loggedinTitle = Yii::t('app/error', 'Restricted area');

        $loggedinMessage = Yii::t('app/error', 'You do not have enough credentials to access this section.');
        $loggedinMessage .= '<br/>';
        $loggedinMessage .= '<br/>';
        $loggedinMessage .= Yii::t('app/error', 'Please login with a different account; or');
        $loggedinMessage .= '<br/>';
        $loggedinMessage .= Yii::t('app/error', 'Contact the SyRIO administrator to request elevating your rights.');

        $guestMessage = Yii::t('app/error', 'Please login first.');
        break;
}


?>
<div class="errors-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="alert alert-danger" role="alert">
                <h2><?= $loggedinTitle ?></h2>
                <div>
                    <?= Yii::$app->user->isGuest ? $guestMessage : $loggedinMessage ?>
                </div>
            </div>
        </div>
    </div>
</div>

