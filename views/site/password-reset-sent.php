<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;

/**
 * Password reset request confirmation
 *
 * @author vamanbo
 */

/* @var $this \yii\web\View */
/* @var $model \app\models\forms\PasswordReset */



/* use this on any view to include the actionmessage logic and widget */
//include Yii::getAlias('@app') . '\views\shared_parts\message_snippet.php';
include Yii::getAlias('@app') . '/views/shared_parts/message_snippet.php';

if ($model->hasErrors())
{
    echo ValidationReportWidget::widget([
        'title' => \Yii::t('app','Change {evt}', ['evt'=>\Yii::t('app', 'password')]),
        'errors_message' => \Yii::t('app','Unable to change your username! Please review the information below and try again.'),
        'errors'=>$model->errors,
        'no_errors_message' => \Yii::t('app','Your username has been successfully modified.', []),
    ]);
}                            

$title = Yii::t('app', 'Reset password');

?>

<h1><?= Yii::t('app', Html::encode($title)) ?></h1>    


<div class='sy_pad_top_36'>
    <div class="col-lg-12">
        
        <p>
            An email has been sent to <?= Html::encode($model->email) ?> with the instructions for reseting your password.
        </p>
        <p>
            Check your email regularly (including the spam).
        </p>
        
        <div class="alert alert-warning" role="alert">
            <?= Yii::t('app', 'Please note that the link is valid for 24 hours') ?>
        </div>
    </div>
</div>
