<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

/* @var $this yii\web\View */
$this->title = 'SyRIO';


//use app\models\widgets\ActionMessageWidget;

use app\components\ActionMessageWidget;

            //$actionMsg = Yii::$app->session->getFlash('msg');
            //echo 'actionMessage: ' . var_dump($actionMsg);
    
            $session = Yii::$app->session;
            //$session_id = $session->id;
            $session_msg = $session->get('finished_action_result');     //$session_msg is of type ActionMessage
            //echo 'session id: ' . $session_id;
?>
<div class="site-index">

    <?= isset($session_msg) ? ActionMessageWidget::widget(['actionMessage' => $session_msg,]) : '' ?>
    
    <?php
        $url = Yii::getAlias('@web').'/css/images/syrio_black.png';
    ?>
        <img class='bg' src=<?=$url?> />

        
    <div class="jumbotron" style='background-color: rgba(255,255,255,0.85)'>
        <h1>SyRIO</h1>

        <p class="lead"><?= Yii::t('app', 'Please log-in to start using the application') . '.' ?></p>

    </div>

    <div class="body-content">
    </div>
</div>

<?php 
    $this->registerCss("body { background-image: none; }");
?>
