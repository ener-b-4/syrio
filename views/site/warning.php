<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use app\components\Errors;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $code integer */

$this->title = ucfirst(Yii::t('app', 'warning'));
?>
<div class="site-warning">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="alert alert-warning">
        <?= Errors::WarnText($code) ?>
    </div>

</div>
