<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use app\components\ActionMessageWidget;

/* @var $this yii\web\View */
$this->title = 'SyRIO';
?>

<?php
    /* use this on any view to include the actionmessage logic and widget */
    //include Yii::getAlias('@app') . '\views\shared_parts\message_snippet.php';
    include Yii::getAlias('@app') . '/views/shared_parts/message_snippet.php';
?>

<div class="site-index">

<?php

    if (Yii::$app->user->isGuest)
    {
        echo $this->render('_indexGuest', []);
    }
    else
    {
        $user = Yii::$app->user->identity->getName();
        $user_roles = Yii::$app->getAuthManager()->getRolesByUser(Yii::$app->user->id);

        if (isset($user_roles) && (key_exists('op_user', $user_roles) || key_exists('op_raporteur', $user_roles)))
        {
            echo $this->render('operators/_index', []);
        }
        elseif (isset($user_roles) && (key_exists('inst_user', $user_roles) || key_exists('inst_rapporteur', $user_roles)))
        {
            echo $this->render('installations/_index', []);
        }
        elseif (isset($user_roles) && (key_exists('ca_admin', $user_roles) || key_exists('ca_assessor', $user_roles) || key_exists('ca_user', $user_roles)))
        {
            echo $this->render('ca/_index', []);
        }
    }

?>
</div>
