<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;

/* @var $this yii\web\View */
$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="lead sy_pad_top_36">
        The Platform
    </div>
    <p>
        <strong>SyRIO</strong> <em> (<b>Sy</b>stem for <b>R</b>eporting the <b>I</b>ncidents in <b>O</b>ffshore Oil&Gas Activities)</em> is a web-platform primarily designed to assist the offshore Operators/Owners and the Competent Authorities in 
        complying with the provisions on reporting the offshore incidents, as provided by Directive 2013/30/EU (the Offshore Directive) and Regulation 1112/2014.
    </p>

    <p>
        <strong>SyRIO</strong> provides the secured environment that allows a safe and sound communication of the incidents from the Operators/Owners to the Competent Authority, following the reporting template given by the Common Reporting Format (CRF).
    </p>
    
    <p>
        The platform has been developed at the <strong>European Commission's Joint Research Centre - Directorate for Energy, Transport and Climate</strong> (<?= Html::a('EC JRC - ETC', 'https://ec.europa.eu/jrc/en') ?> ) premises.
    </p>

    <div class='lead sy_pad_top_36'>
        Hosting & Version
    </div>
    <p>
        The current platform is hosted by the Competent Authority of 
        <span class='text-info'>
            <?= Html::encode(app\modules\management\models\Country::findOne(['iso2'=>\Yii::$app->params['competentAuthorityIso2']])->short_name) ?>
        </span> 
        (<em class='text-info'>
            <?= \Yii::$app->params['competentAuthority'] ?>
        </em>) 
        
        <?= Html::a('<span class="glyphicon glyphicon-envelope"></span>', \Yii::$app->urlManager->createUrl(['//site/contact'])) ?>
        
    </p>
    
    <p>
        <b>Version: </b> <?= Yii::$app->params['version'] ?> <em>build </em> <?= Yii::$app->params['build'] ?>
    </p>
</div>
