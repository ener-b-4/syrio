<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use app\widgets\OperatorsReportingStatusWidget;

/* @var $this yii\web\View */
$this->title = 'SyRIO (operators)';
//$incidentsStat = Yii::$app->user->getIdentity()->organizationIncidents;
?>


    <div class="container" style="padding-top:6px;">
        
        <div class="row">
            <div class="col-md-4 col-sm-6 col-xs-12" style="margin-left: -15px;">
                <?= OperatorsReportingStatusWidget::widget() ?>
            </div>
            <div class="col-md-8 col-sm-6 col-xs-12">
                <h2><?= Yii::t('app','Events management') ?></h2>
                <div>
                    <p><?= Yii::t('app', 'Operations in this section allows you to control the events registered by your organization.') ?></p>
                </div>

                <?= Html::a(Yii::t('app', 'Open Incidents Hub'), ['/events/event-declaration'], ['class' => 'btn btn-success']) ?>
                
            </div>
        </div>

    </div>

