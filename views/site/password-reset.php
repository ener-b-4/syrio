<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use kartik\form\ActiveForm;
use app\models\forms\PasswordReset;
use yii\helpers\Html;
use app\widgets\ValidationReportWidget;

/**
 * Description of account
 *
 * @author vamanbo
 */

/* @var $this \yii\web\View */
/* @var $model app\models\forms\PasswordReset */
/* @var $form kartik\form\ActiveForm */



/* use this on any view to include the actionmessage logic and widget */
//include Yii::getAlias('@app') . '\views\shared_parts\message_snippet.php';
include Yii::getAlias('@app') . '/views/shared_parts/message_snippet.php';

if ($model->hasErrors())
{
    echo ValidationReportWidget::widget([
        'title' => \Yii::t('app','Change {evt}', ['evt'=>\Yii::t('app', 'password')]),
        'errors_message' => \Yii::t('app','Unable to submit your request! Please review the information below and try again.'),
        'errors'=>$model->errors,
        'no_errors_message' => \Yii::t('app','Your username has been successfully modified.', []),
    ]);
}                            

$title = Yii::t('app', 'Reset password');

?>

<h1><?= Yii::t('app', Html::encode($title)) ?></h1>    


<div class='sy_pad_top_36'>
    <div class="col-lg-12">
        
        <?php $form = ActiveForm::begin([
            //'enableAjaxValidation' => false,
            //'enableClientValidation' => false
        ]); ?>

        <?= $form->field($model, 'username', [
        ])->textInput()
                ->hint(Yii::t('app', 'The username you used for logging in to SyRIO'), [
        ]) ?>

        <?= $form->field($model, 'email', [
        ])->textInput()
                ->hint(Yii::t('app', 'The email provided when registered.'), [
        ]) ?>
        
        <div class='sy_pad_top_18'>
            <?=  $form->field($model, 'captcha', [
            ])->widget(\yii\captcha\Captcha::classname(), [
                // configure additional widget properties here
                'captchaAction'=>'site/captcha'
            ]) ?>
        </div>        

        <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-4 form-group" >
                <?= Html::submitButton(Yii::t('app', 'Submit'), [
                    'class' => 'btn btn-primary'
                    ]) ?>
            </div>
        </div>

    <?php
    ActiveForm::end();
    ?>
        
    </div>
</div>

