<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\models\common\ActionMessage;
use app\widgets\ActionMessageWidget;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

$this->title = ucfirst(\Yii::t('app', 'Login'));
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <?php
        /* use this on any view to include the actionmessage logic and widget */
        //include Yii::getAlias('@app') . '\views\shared_parts\message_snippet.php';
        include Yii::getAlias('@app') . '/views/shared_parts/message_snippet.php';
    ?>    

    <h1><?= Html::encode($this->title) ?></h1>

    <p><?= Yii::t('app', 'Please fill out the following fields to login') ?></p>

    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'options' => ['class' => 'form-horizontal'],
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]); ?>

    <?= $form->field($model, 'username') ?>

    <?= $form->field($model, 'password')->passwordInput()->label(ucfirst($model->attributeLabels()['password'])) ?>

    <div class="form-group">
        <div class="col-lg-offset-1 col-lg-11">
            <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button', 'onclick' => 'return encryptData();' ]) ?>
        </div>
    </div>

    <div class="form-group">
    <div class="col-lg-offset-1 col-lg-11">
        <?php $url = \yii\helpers\Url::to(['site/reset-password']) ?>
        <?= Html::a(Yii::t('app', 'Forget password?'), $url, []) ?>
    </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script type="text/javascript">
  function encryptData(){
    var key = CryptoJS.enc.Hex.parse('bcb04b7e103a0cd8b54763051cef08bc55abe029fdebae5e1d417e2ffb2a00a3');
    var iv  = CryptoJS.enc.Hex.parse('101112131415161718191a1b1c1d1e1f');
    var password = document.getElementById('loginform-password').value;
    
    var encrypted = CryptoJS.AES.encrypt(password, key, { iv: iv });

    var password_base64 = encrypted.ciphertext.toString(CryptoJS.enc.Base64); 
    //return password_base64; 
    //alert(password_base64);
    $('#loginform-password').val(password_base64);
  };

</script>
