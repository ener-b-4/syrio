<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
//use app\models\User;
//use app\models\forms\NewPassword;
use yii\captcha\Captcha;


/**
 * Form displayed at first-login
 *
 * @author bogdanV
 */

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\forms\NewPassword */

?>

<h1>
    <?= Html::encode(Yii::t('app', 'Reset password')) ?>
</h1>

<p><?= Html::encode(Yii::t('app', 'Please provide a new password in the form below.')) ?></p>

<div class='panel panel-info col-lg-5 col-md-5 col-sm-8 col-xs-12'>
    <div class='panel-body'>
        <?php
            $form = ActiveForm::begin(
                    [
                        'method' => 'post',
                        'action' => ['site/set-new-password']
                    ])
        ?>
        
        <?= $form->field($model, 'user_id')->hiddenInput()->label(null, ['class' => 'hidden']) ?>
        <?= $form->field($model, 'token')->textInput()->label(null, ['class' => 'hidden']) ?>
        <?= $form->field($model, 'new_password')->passwordInput()->label(Yii::t('app', 'New password')); ?>
        <?= $form->field($model, 'password_repeat')->passwordInput()->label(Yii::t('app', 'Repeat password')) ?>
        
        <?= $form->field($model, 'captcha')->widget(Captcha::className())->label('') ?>
        
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary', 'name' => 'submit-button', 'onclick'=>'return encryptData();']) ?>
        </div>
        
        <?php
            ActiveForm::end();
        ?>
    </div>
</div>

<script type="text/javascript">
  function encryptData(){
    var key = CryptoJS.enc.Hex.parse('bcb04b7e103a0cd8b54763051cef08bc55abe029fdebae5e1d417e2ffb2a00a3');
    var iv  = CryptoJS.enc.Hex.parse('101112131415161718191a1b1c1d1e1f');
    var password = document.getElementById('newpassword-new_password').value;
    var password_repeat = document.getElementById('newpassword-password_repeat').value;
    
    var encrypted = CryptoJS.AES.encrypt(password, key, { iv: iv });
    var encrypted_repeat = CryptoJS.AES.encrypt(password_repeat, key, { iv: iv });

    var password_base64 = encrypted.ciphertext.toString(CryptoJS.enc.Base64); 
    var password_base64_repeat = encrypted_repeat.ciphertext.toString(CryptoJS.enc.Base64); 
    //return password_base64; 
    //alert(password_base64);
    $('#newpassword-new_password').val(password_base64);
    $('#newpassword-password_repeat').val(password_base64_repeat);
  };

</script>