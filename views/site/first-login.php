<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\models\User;
use app\models\admin\users\UserFirstLoginBehavior;
use app\models\admin\users\FirstLoginForm;
use yii\captcha\Captcha;


/**
 * Form displayed at first-login
 *
 * @author bogdanV
 */

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model common\models\FirstLoginForm */

//get the loggedin user
$user = User::findOne(Yii::$app->user->id);
//attach UserFirstBehavior to the user
$user->attachBehavior('firstLogin' , new UserFirstLoginBehavior());

if (!$user->firstLogin)
{
    echo 'you cannot change the password from here';
    die();
}

$model = new FirstLoginForm();
?>

<h1>
    <?= Html::encode(Yii::t('app', 'Finalize registration')) ?>
</h1>

<p><?= Html::encode(Yii::t('app', 'Welcome to SyRIO. As this is the first time you log in, please provide a password at your convenience to activate your account.')) ?></p>

<div class='panel panel-info col-lg-5 col-md-5 col-sm-8 col-xs-12'>
    <div class='panel-body'>
        <?php
            $form = ActiveForm::begin(
                    [
                        'method' => 'post',
                        'action' => ['site/first-login']
                    ])
        ?>
        
        <?= $form->field($model, 'new_password')->passwordInput()->label(Yii::t('app', 'New password')); ?>
        <?= $form->field($model, 'new_password_repeat')->passwordInput()->label(Yii::t('app', 'Repeat password')) ?>
        
        <?= $form->field($model, 'captcha')->widget(Captcha::className())->label('') ?>
        
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Activate account'), ['class' => 'btn btn-primary', 'name' => 'submit-button']) ?>
        </div>
        
        <?php
            ActiveForm::end();
        ?>
    </div>
</div>