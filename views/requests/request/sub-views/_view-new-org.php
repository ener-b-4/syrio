<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use app\models\forms\NewOrganizationRequest;

/* @var $this yii\web\View */
/* @var $model \app\models\requests\Request */

$lis = '';
$content = json_decode($model->content, true);

$attributeLabels = NewOrganizationRequest::staticAttributeLabels();

foreach ($content as $key=>$value)
{
    if (!NewOrganizationRequest::SkipOnReport($key))
    {
        $lis .= '<li>'
                . '<span class = "text-info">'
                . $attributeLabels[$key] . ' '
                . '</span>';
        if (!is_array($value))
        {
            $lis.= '<span>'
                . Html::encode($value)
                . '</span>';
        }
        else
        {
            $lis.= '<span>'
                . Html::encode(implode(', ', $value)) 
                . '</span>';
        }
    }
}
?>

<ul class='list-unstyled'>
<?= $lis ?>
</ul>

