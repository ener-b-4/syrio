<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\requests\RequestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = ucfirst(Yii::t('app', 'requests'));
$this->params['breadcrumbs'][] = $this->title;
?>

<div class='container request-index' id='message-container'>
    <div class='row' id='message-row'>
        <div class='col-lg-12'>
            <?php
                /* use this on any view to include the actionmessage logic and widget */
                //include Yii::getAlias('@app') . '\views\shared_parts\message_snippet.php';
                include Yii::getAlias('@app') . '/views/shared_parts/message_snippet.php';        
            ?>
        </div>
    </div>    
    
    <h2><?= Html::encode($this->title) ?></h2>
    <div class="row">
        <div class="col-lg-12 text-muted">
            <?= Yii::t('app', 'Please select one of the request types in the categories below.') ?>
        </div>
    </div>
    
    <div class="container">
        <div class="row"> 
            <div class="row sy_pad_top_36"> 
                <div class="col-sm-3 col-xs-12"> <!-- start organizations panel -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-warning">
                                <div class="panel-heading text-warning">
                                    <span class="lead">
                                        <?= Yii::t('app', 'Organizations') ?>
                                    </span>
                                </div>
                                <div class="panel-body">
                                    <small class="text-muted">
                                        <?= Yii::t('app', 'Use these forms for requesting the registration of your organization in SyRIO.') ?>
                                    </small>
                                </div>
                                <ul class="list-group">
                                    <li class="list-group-item">
                                        <?php $url = Yii::$app->urlManager->createUrl(['/requests/request/new_organization', 'type'=>  \app\models\requests\RequestTypes::NEW_CA_ORG]) ?>
                                        <?= Html::a(Yii::t('app', 'Register new {evt} Organization', ['evt'=>\Yii::t('app', 'Competent Authority')]) , $url) ?>
                                    </li>
                                    <li class="list-group-item">
                                        <?php $url = Yii::$app->urlManager->createUrl(['/requests/request/new_organization', 'type'=> \app\models\requests\RequestTypes::NEW_OO_ORG]) ?>
                                        <?= Html::a(Yii::t('app', 'Register new {evt} Organization', ['evt'=>\Yii::t('app', 'Operators / Owners')]) , $url) ?>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div> <!-- end Organizations panel -->
                <div class="col-sm-3 col-xs-12"> <!-- start installations panel -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-danger">
                                <div class="panel-heading text-danger">
                                    <span class="lead">
                                        <?= ucfirst(Yii::t('app', 'installations')) ?>
                                    </span>
                                </div>
                                <div class="panel-body">
                                    <small class="text-muted">
                                        <?= Yii::t('app', 'Use these forms for requesting the registration of installation in SyRIO.') ?>
                                    </small>
                                </div>
                                <ul class="list-group">
                                    <li class="list-group-item">
                                        <?php $url = Yii::$app->urlManager->createUrl('/requests/request/new_installation') ?>
                                        <?= Html::a(ucfirst(Yii::t('app', 'register new {evt}', ['evt'=>\Yii::t('app', 'installation')])) , $url) ?>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div> <!-- end Installations panel -->
                <div class="col-sm-3 col-xs-12"> <!-- start users panel -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-info">
                                <div class="panel-heading text-info">
                                    <span class="lead">
                                        <?= ucfirst(Yii::t('app', 'users')) ?>
                                    </span>
                                </div>
                                <div class="panel-body">
                                    <small class="text-muted">
                                        <?= Yii::t('app', 'Use these forms for registering to SyRIO.') ?>
                                    </small>
                                </div>
                                <ul class="list-group">
                                    <li class="list-group-item">
                                        <?php $url = Yii::$app->urlManager->createUrl('/requests/request/user_new_competent_authority') ?>
                                        <?= Html::a(Yii::t('app', 'Register as {evt} user', ['evt'=>\Yii::t('app', 'Competent Authority')]) , $url) ?>
                                    </li>
                                    <li class="list-group-item">
                                        <?php $url = Yii::$app->urlManager->createUrl(['/requests/request/user_new_operator']) ?>
                                        <?= Html::a(Yii::t('app', 'Register as {evt} user', ['evt'=>\Yii::t('app', 'Operators / Owners')]) , $url) ?>
                                    </li>
                                    <li class="list-group-item">
                                        <?php $url = Yii::$app->urlManager->createUrl('/requests/request/user_new_installation') ?>
                                        <?= Html::a(Yii::t('app', 'Register as {evt} user', ['evt'=>\Yii::t('app', 'installation')]) , $url) ?>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div> <!-- end users panel -->
                
            </div> <!-- end row -->
        </div>
        <div class="row">
            <div class="col-lg-12">

                <div class="form-group">
                    <?php
                    $url=\Yii::$app->urlManager->createUrl(['/requests/request/list']);
                    $num = app\models\requests\Request::find()
                            ->where(['status' => 0])
                            ->orWhere(['status' => null])->count();
                    
                    if (Yii::$app->user->can('process_request'))
                    {                        
                        echo Html::a('View requests <span class="badge" style="padding-left:6px">' . $num . '</span>', $url, [
                            'class'=>"btn btn-success",
                        ]);
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>

    
    <h2><?= Html::encode(\Yii::t('app', 'Contact')) ?></h2>
    <div class="row">
        <div class="col-lg-12 text-muted">
            <?= Yii::t('app', 'Use one of the options below to send a free text message.') ?>
        </div>
        <div class="col-lg-12">
            <ul class="list-unstyled">
                <li>
                    <?= Html::a(\Yii::t('app', 'SyRIO administrators'), \Yii::$app->urlManager->createUrl(['//site/contact'])) ?>
                </li>
                <?php 
                if (\Yii::$app->user->can('sys_admin')) {
                ?>
                <li>
                    <?= Html::a(\Yii::t('app', 'SyRIO Developers'), \Yii::$app->urlManager->createUrl(['//site/contact', 'devs'=>true])) ?>
                </li>
                <?php
                }
                ?>
            </ul>
        </div>
    </div>

    
    
    
</div> <!-- end top-level container -->

