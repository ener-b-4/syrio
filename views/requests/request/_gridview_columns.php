<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use app\components\helpers\DataGrid\CustomGridView;

$columns = [

    [
        'class' => kartik\grid\DataColumn::className(),
        'label' => '#',
        'attribute' => 'id'
    ],
    
    [
        'class' => \kartik\grid\DataColumn::className(),
        'attribute' => 'id',
        'label'=>ucfirst(\Yii::t('app', 'submitted')),
        'content' => function($model) {
            $date = new DateTime();
            $date->setTimestamp($model->id);
            return $date->format('Y-m-d H:i:s');
        },
    ],

                [
                    'class' => \kartik\grid\DataColumn::className(),
                    'label' => ucfirst(\Yii::t('app', 'type')),
                    'attribute' => 'request_type',
                    'content' => function($model) {
                        return $model->requestType->description;
                        //return $model->status;
                    },
                    'filterType' => \kartik\grid\GridView::FILTER_SELECT2,
                    'filter' => yii\helpers\ArrayHelper::map(\app\models\requests\RequestTypes::find()->all(), 'description', 'description'),
                    'filterWidgetOptions'=>[
                            'pluginOptions'=>['allowClear'=>true],
                        ],
                    'filterInputOptions'=>['prompt'=>Yii::t('app', 'type')],
                    
                ],
                
                
                [
                    'class' => \kartik\grid\DataColumn::className(),
                    'attribute' => 'status',
                    'label' => \Yii::t('app', 'Status'),
                    'content' => function($model) {
                        $opt = [
                            '0'=>strtoupper(\Yii::t('app', 'open')), 
                            '10'=>strtoupper(\Yii::t('app', 'accepted')), 
                            '20'=>strtoupper(\Yii::t('app', 'rejected'))];
                        return isset($model->status) ? $opt[$model->status] : strtoupper(\Yii::t('app', 'open'));
                        //return $model->status;
                    },
                    'filterType' => \kartik\grid\GridView::FILTER_SELECT2,
                    'filter' => [
                        '-1' => '('.\Yii::t('app', 'all').')',
                        '0'=>strtoupper(\Yii::t('app', 'open')), 
                        '10'=>strtoupper(\Yii::t('app', 'accepted')), 
                        '20'=>strtoupper(\Yii::t('app', 'rejected'))],
                    'filterWidgetOptions'=>[
                            'pluginOptions'=>['allowClear'=>true],
                        ],
                    'filterInputOptions'=>['prompt'=>lcfirst(Yii::t('app', 'Status'))],
                    
                ],
                            
                            [
                                'header' => ucfirst(Yii::t('app', 'actions')),
                                'class' => \kartik\grid\ActionColumn::classname(),
                                'template' => '{view} {delete}'
                            ]
];