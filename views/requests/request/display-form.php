<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use app\models\requests\RequestTypes;

/* @var $this \yii\web\View */
/* @var $model mixed any type of form object */
/* @var $request_type integer */
/* @var $extra mixed any extra things depending on the request type */


//check the type of the request and set the form content accordingly
$formName='';
switch ($request_type)
{
    case RequestTypes::NEW_OO_USER:
        $formName = '_form-new-oo-user';
        $extra_param_name = 'roles';
        $firstBread = 'users';
        $secondBread = Yii::t('app', 'User registration form') . ' (' . Yii::t('app', 'Operators / Owners') . ')';
        break;
    case RequestTypes::NEW_CA_USER:
        $formName = '_form-new-ca-user';
        $extra_param_name = 'roles';
        $firstBread = 'users';
        $secondBread = Yii::t('app', 'User registration form') . ' ( ' . Yii::t('app', 'Competent Authority') . ')';
        break;
    case RequestTypes::NEW_INST_USER:
        $formName = '_form-new-inst-user';
        $extra_param_name = 'roles';
        $firstBread = 'users';
        $secondBread = Yii::t('app', 'User registration form') . ' ( ' . ucfirst(Yii::t('app', 'installation')) . ')';
        break;
    case RequestTypes::NEW_CA_ORG:
        $formName = '_form-new-organization';
        $extra_param_name = 'type';
        $extra = 'ca';
        $firstBread = 'Organizations';
        $secondBread = \Yii::t('app', 'Register new {evt} Organization', ['evt' => \Yii::t('app', 'Competent Authority')]);
        break;
    case RequestTypes::NEW_OO_ORG:
        $formName = '_form-new-organization';
        $extra_param_name = 'type';
        $extra = 'oo';
        $firstBread = 'Organizations';
        $secondBread = \Yii::t('app', 'Register new {evt} Organization', ['evt' => \Yii::t('app', 'Operators / Owners')]);
        break;
    case RequestTypes::NEW_INST:
        $formName = '_form-new-installation';
        $extra_param_name = 'extra';
        $firstBread = 'installations';
        $secondBread = Yii::t('app', 'Installation registration form');
        break;
}

/* breadcrumbs section */
use yii\helpers\Url;
$breadcrumbs = [
    [
        'label' => ucfirst(Yii::t('app', 'requests')),
        'url' => Url::to(['/requests/request']),
        //'template' =>"<li>{link}</li>
    ],
    [
        'label' => ucfirst(Yii::t('app', $firstBread)),
        //'url' => Url::to(['/admin/users/user']),
        //'template' =>"<li>{link}</li>
    ],
    [
        'label' => $secondBread,
        //'url' => Url::to(['/admin/users/user/create-user']),
        //'template' =>"<li>{link}</li>
    ],
];
$this->params['breadcrumbs'] = $breadcrumbs;

?>
<div class="container">
    <div class='row' id='message-row'>
        <div class='row'>
            <div class='col-lg-12'>
                <?php echo count($model->errors) != 0 ? \app\widgets\ValidationReportWidget::widget(['errors'=>$model->errors]) : '' ?>
            </div>
            <?php
                /* use this on any view to include the actionmessage logic and widget */
                //include Yii::getAlias('@app') . '\views\shared_parts\message_snippet.php';
                include Yii::getAlias('@app') . '/views/shared_parts/message_snippet.php';        
            ?>
        </div>
    </div>
    
    <?php
    $session_msg = Yii::$app->session->get('finished_action_result');     //$session_msg is of type ActionMessage
        if (!isset($session_msg))
        {    
    ?>
    <div class="row">
        <div>
            <?= strlen($formName) != 0 ? 
                $this->render('forms/' . $formName, ['model' => $model, $extra_param_name => $extra]):
                '<div "class = col-lg-12 alert alert-danger" role="alert">'
                . Yii::t('app', 'unknown request')
                . '</div>' ?>
        </div>
    </div>
    <?php
        };
    ?>
</div>