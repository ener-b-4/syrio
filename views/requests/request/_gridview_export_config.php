<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use app\components\helpers\DataGrid\CustomGridView;
use yii\helpers\Html;
          // set export properties
$exportConfig = [
            'fontAwesome' => true,
            //'target' => GridView::TARGET_BLANK,   //target: string, the target for submitting the export form, which will trigger the download of the exported file. 
            'target' => CustomGridView::TARGET_BLANK,   //target: string, the target for submitting the export form, which will trigger the download of the exported file. 
            'showConfirmAlert' => false,
            'header' => 'Export Page Data',       //'Export Page Data' : Title of Menu Export
          ];
