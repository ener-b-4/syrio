<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use app\components\helpers\DataGrid\CustomGridView;


/* @var $this yii\web\View */
/* @var $searchModel app\models\requests\RequestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = Yii::t('app', 'Requests list');
$this->params['breadcrumbs'][] = $this->title;


include '_gridview_toolbar.php';
include '_gridview_columns.php';
include_once '_gridview_export_config.php';

?>
    <div class='container request-index' id='message-container'>
        <div class='row' id='message-row'>
            <div class='row'>
                <?php
                    /* use this on any view to include the actionmessage logic and widget */
                    //include Yii::getAlias('@app') . '\views\shared_parts\message_snippet.php';
                    include Yii::getAlias('@app') . '/views/shared_parts/message_snippet.php';        
                ?>
            </div>
        </div>    
    </div>
    
    <div class="container">
       
        <div class="row" id="core_list">

            <div class='col-lg-12'>
                <h2><?= Html::encode($this->title) ?></h2>
            </div>
            
            <div class="col-lg-12">
                <?= CustomGridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'pjax' => true,

                    'rowOptions' => function($model) {
                    },

                    'columns' => $columns,  // $grid_columns,
                    'toolbar' => $grid_toolbar,

                    'responsive'=>true,
                    'hover'=>true,

                  // set export properties
                  'export' => [
                    'icon' => 'share-square-o',
                    'encoding' => 'utf-8',
                    'fontAwesome' => true,
                    'target' => CustomGridView::TARGET_SELF,   //target: string, the target for submitting the export form, which will trigger the download of the exported file. 
                    'showConfirmAlert' => TRUE,
                    'header' => '<li role="presentation" class="dropdown-header">' . \Yii::t('app', 'Grid Export') . '</li>'
                    //'header' => 'Export Page Data',
                  ],

                    'panel' => [
                            'type' => CustomGridView::TYPE_PRIMARY,
                            'heading' => '<span class="icon-syrio_glyphs"></span>' . ucfirst(Yii::t('app', 'requests')),
                            'before'=>''
                            ,     //end before
                            'after'=>'',            
                        ],                        

                ]); ?>

            </div>
        </div>
    </div>
