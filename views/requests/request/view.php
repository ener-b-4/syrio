<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use app\models\requests\RequestTypes;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \app\models\requests\Request */

$this->title = \Yii::t('app', 'Req. #') . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Requests'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$modelAttr = $model->attributeLabels();

$contentViewName='';
switch ($model->request_type)
{
    case RequestTypes::NEW_OO_USER:
        $contentViewName = '_view-new-oo-user';
        $subtitle = \Yii::t('app', '{evt} user', ['evt' => \Yii::t('app', 'Operators/Owners')]);
        break;
    case RequestTypes::NEW_CA_USER:
        $contentViewName = '_view-new-ca-user';
        $subtitle = \Yii::t('app', '{evt} user', ['evt' => \Yii::t('app', 'Competent Authority')]);
        break;
    case RequestTypes::NEW_INST_USER:
        $contentViewName = '_view-new-inst-user';
        $subtitle = \Yii::t('app', '{evt} user', ['evt' => ucfirst(Yii::t('app', 'installation'))]);
        break;
    case RequestTypes::NEW_CA_ORG:
        $contentViewName = '_view-new-org';
        $subtitle = \Yii::t('app', '{evt} Organization', ['evt' => \Yii::t('app', 'Competent Authority')]);
        break;
    case RequestTypes::NEW_OO_ORG:
        $contentViewName = '_view-new-org';
        $subtitle = \Yii::t('app', '{evt} Organization', ['evt' => \Yii::t('app', 'Operator/Owner')]);
        break;
    case RequestTypes::NEW_INST:
        $contentViewName = '_view-new-installation';
        $subtitle = \Yii::t('app', '{evt}', ['evt' => ucfirst(Yii::t('app', 'installation'))]);
        break;
}

/* create the status text */
$status_text = '';
switch ($model->status)
{
    case \app\models\requests\Request::STATUS_OPEN:
        $status_text=Yii::t('app', 'OPEN');
        $panel_class = 'panel-default';
        break;
    case \app\models\requests\Request::STATUS_ACCEPTED_CLOSED:
        $panel_class = 'panel-success';
        
        $user = app\models\User::find($model->processed_by_id)->one();
        if (!$user)
        {
            $status_text .= '<span class=glyphicon glyphicon-exclamation text-alert></span> '
            . Yii::t('app', 'user is not a current Syrio user');
        }
        $userLink = Html::a($user->first_names . ' ' . $user->last_name, 
                \Yii::$app->urlManager->createAbsoluteUrl(['/admin/users/user/view', 'id'=>$user->id]));
        $processDate = date('Y-m-d h:i', $model->processed_at);
        
        $status_text = strtoupper(Yii::t('app', 'closed')) . ' / ' . Yii::t('app', 'ACCEPTED by {evt1} at {evt2}',
                ['evt1' => $userLink, 'evt2'=>$processDate]);
        break;
    case \app\models\requests\Request::STATUS_ACCEPTED_REJECTED:
        $panel_class = 'panel-danger';
        
        $user = app\models\User::find($model->processed_by_id)->one();
        if (!$user)
        {
            $userlink = '<span class=glyphicon glyphicon-exclamation text-alert></span> '
            . Yii::t('app', 'user is not a current Syrio user');
        }
        else
        {
            $userLink = Html::a($user->first_names . ' ' . $user->last_name, 
                    \Yii::$app->urlManager->createAbsoluteUrl(['/admin/users/user/view', 'id'=>$user->id]));
        }
        $processDate = date('Y-m-d h:i', $model->processed_at);
        
        $status_text = strtoupper(Yii::t('app', 'closed')) . ' / ' . Yii::t('app', 'REJECTED by {evt1} at {evt2}',
                ['evt1' => $userLink, 'evt2'=>$processDate]);
        
        //$status_text = $userLink . ' ' . $model->processed_at;
        break;
}


?>
<div class="request-view">

    <div class='container' id='message-container'>
        <div class='row' id='message-row'>
            <div class='row'>
                <div class='col-lg-12'>
                    <?php echo count($model->errors) != 0 ? \app\widgets\ValidationReportWidget::widget(['errors'=>$model->errors]) : '' ?>
                </div>
                <?php
                    /* use this on any view to include the actionmessage logic and widget */
                    //include Yii::getAlias('@app') . '\views\shared_parts\message_snippet.php';
                    include Yii::getAlias('@app') . '/views/shared_parts/message_snippet.php';        
                ?>
            </div>
        </div>    
    </div>
    
    <div class='container'>
        <div class='row'>
            <div class='col-md-8 col-md-push-2'>
                <div class='panel <?= $panel_class ?>'>
                    <div class='panel-heading'>
                        <span class='lead'><?= Yii::t('app', '{evt} request', [
                            'evt'=>Yii::t('app', $subtitle)
                        ]) ?></span>
                        <span class="pull-right"><?= ' #' . $model->id ?></span>
                    </div>
                    <div class='panel-body' style='padding-top: 0px;'>

                        <div class='row'>
                            <div id='details-pane' class='col-lg-12' style='padding-top:15px; padding-bottom: 0; border-bottom: 1px solid #ddd; background-color: #DCE7E8'>
                                <small>
                                <ul class='list-unstyled'>
                                    <li>
                                        <?= $modelAttr['id'] .  ': ' . date('Y-m-d', $model->id) ?>
                                    </li>
                                    <li>
                                        <?= $modelAttr['request_type'] .  ': ' . Yii::t('app', $model->requestType->description) ?>
                                    </li>
                                    <li>
                                        <div class='sy_pad_top_6'>
                                            <span class=''>From: </span>
                                        <?php
                                            if ($model->from == \app\models\requests\Request::ANONYMOUS_REQUEST)
                                            {
                                                echo Yii::t('app', 'anonymous');
                                            }
                                            else
                                            {
                                                $user = app\models\User::find($model->from)->one();
                                                if (!$user)
                                                {
                                                    echo '<span class=glyphicon glyphicon-exclamation text-alert></span> '
                                                    . Yii::t('app', 'sender is not a current Syrio user');
                                                }
                                                echo Html::a($user->first_names . ' ' . $user->last_name, 
                                                        \Yii::$app->urlManager->createAbsoluteUrl(['/admin/users/user/view', 'id'=>$user->id]));
                                            } 
                                        ?>
                                        </div>
                                    </li>
                                    <li class='sy_pad_top_6'>
                                        <?= \Yii::t('app', 'Status') . ': ' ?>
                                        <?= $status_text ?>
                                    </li>
                                </ul>
                                </small>
                            </div>
                            <div id='request-content-pane' class='col-lg-12' style='padding-top:15px'>
                                <?= $this->render('sub-views/' . $contentViewName, ['model'=>$model]) ?>
                            </div>
                        </div>
                    </div> <!-- end panel body -->
                    <div class='panel-footer'>
                        <?= $this->render('forms/_assess_form', ['model' => $model]) ?>
                    </div>
                </div> <!-- end form panel -->
            </div>
        </div>
    </div>

</div>
