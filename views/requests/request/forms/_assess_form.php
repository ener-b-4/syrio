<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use app\models\requests\RequestTypes;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \app\models\requests\Request */

/* create the possible status array */
$stats = [
    \app\models\requests\Request::STATUS_ACCEPTED_CLOSED => Yii::t('app', 'Accept'),
    \app\models\requests\Request::STATUS_ACCEPTED_REJECTED => Yii::t('app', 'Reject'),    
]
?>

<div>
    <?php if ($model->status == \app\models\requests\Request::STATUS_OPEN) {
        $form = ActiveForm::begin();
        ?>
    <div class='row'>
        <div class='col-sm-2'>
            <?= $form->field($model, 'status')->radioList($stats, [])->label(\Yii::t('app', 'Actions')) ?>
        </div>
        <div class='col-sm-10'>
            <?= $form->field($model, 'justification')->textarea(['class'=>'form-control'])
                ->hint(\Yii::t('app', 'Please provide justifiction.')) ?>
            
            <?= $form->field($model, 'send_justification')->checkbox()->hint(\Yii::t('app', 'Check this if you want an email that includes justification to be sent to the requester')) ?>
        </div>
        <div class='hidden-xs hidden-lg hidden-md hidden-sm hidden-print'>
            <?= $form->field($model, 'id')->hiddenInput() ?>
            <?= $form->field($model, 'request_type')->hiddenInput() ?>
            <?= $form->field($model, 'from')->hiddenInput() ?>
            <?= $form->field($model, 'content')->hiddenInput() ?>
            <?= $form->field($model, 'processed_by_id')->hiddenInput() ?>
            <?= $form->field($model, 'processed_at')->hiddenInput() ?>
        </div>
    </div>
    <div class='row'>
        <div class='col-lg-12'>
           <?php
                $url = \Yii::$app->urlManager->createUrl(['/requests/request/view', 'id'=>$model->id]);
                echo Html::a(\Yii::t('app/commands', 'Submit'), $url, [
                    'class'=>'btn btn-default',
                    'confirm' => Yii::t('app', 'Are you sure?'),
                    'data-method' => 'POST'
                    ]);
            ?>
        </div>
    </div>

    <?php $form->end(); }
    else    //the request could be reopened
    {
    ?>
    <div class='row'>
        <div class='col-xs-6'>
            <?php
                $url = \Yii::$app->urlManager->createUrl(['/requests/request/reopen', 'id'=>$model->id]);
                echo Html::a(\Yii::t('app', 'Reopen'), $url, [
                    'class'=>'btn btn-default',
                    'confirm' => Yii::t('app', 'Are you sure you want to reopen this request this item? An email will be send to the requester.')
                    ]);
            ?>
        </div>
        <div class='col-xs-6 text-right'>
            <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]) ?>            
        </div>
    </div>    
    <?php
    }
    ?>
</div>