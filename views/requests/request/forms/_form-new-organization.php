<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\web\View;
use kartik\form\ActiveForm;

/* @var $model \app\models\forms\UserRegistrationCa */
/* @var $type string ca/oo */

$helpBlock = 'Fill-in this form to submit a {evt} Organization registration request.';

if ($type == 'ca')
{
    $helpBlock2 = 'Once submitted, your request will be assessed by the system administrators. You will be informed by email once your request is processed.';
    $evt = \Yii::t('app', 'Competent Authority');
}
elseif ($type == 'oo')
{
    $helpBlock2 = 'Once submitted, your request will be assessed by the system administrators. You will be informed by email once your request is processed.';
    $evt = \Yii::t('app', 'Operators / Owners');
}
else
{
    echo 'unknown organization type';
    die();
}

?>

<div class='container'>
    <div class='row'>
        <div class='col-lg-12'>
            <?php echo count($model->errors) != 0 ? \app\widgets\ValidationReportWidget::widget(['errors'=>$model->errors]) : '' ?>
        </div>
    </div>
    
    
    <div class='row'>
        <div class='col-md-8 col-md-push-2'>
            <div class='panel panel-default'>
                <div class='panel-heading'>
                    <span class='lead'><?= Yii::t('app', 'Organization registration form') ?></span>                    
                </div>
                <div class='panel-body'>
                    <p class='text-muted'>
                        <?= Yii::t('app', $helpBlock, ['evt'=>$evt]) ?>
                    </p>
                    <p class='text-muted'>
                        <?= Yii::t('app', $helpBlock2) ?>
                    </p>
                    
                    <div class='row sy_pad_top_18'>
                        <div class='col-lg-12'>
                        <!-- the form section -->
                        <?php
                        $form = ActiveForm::begin([
                            'type' => ActiveForm::TYPE_HORIZONTAL,
                            'formConfig' => [
                                'labelSpan' => 3,
                                'deviceSize' => ActiveForm::SIZE_SMALL
                            ]
                        ]) 
                        ?>

                        <div class='row'>
                            <div class='col-lg-12 text-info text-right'>
                                <h3><?= Yii::t('app', 'General') ?><br/>
                                    <small><span class='text-muted'><?= \Yii::t('app', 'Information about your organization') ?></span></small>
                                </h3>
                            </div>
                        </div>
                        <?= $form->field($model, 'organization_name')->textInput()->hint(\Yii::t('app', 'The full name of the organization')) ?>
                        <?= $form->field($model, 'organization_acronym')->textInput()->hint(\Yii::t('app', 'The acronym of the organization')) ?>

                        <div class='row'>
                            <div class='col-lg-12 text-info text-right'>
                                <h3><?= ucfirst(Yii::t('app', 'address')) ?><br/>
                                    <small><span class='text-muted'><?= \Yii::t('app', 'Organization address') ?></span></small>
                                </h3>
                            </div>
                        </div>

                        <?= $form->field($model, 'street1')->textInput() ?>
                        <?= $form->field($model, 'street2')->textInput() ?>

                        <?= $form->field($model, 'city')->textInput() ?>
                        <?= $form->field($model, 'county')->textInput() ?>
                        <?= $form->field($model, 'zip')->textInput() ?>
                        <?= $form->field($model, 'country')->textInput() ?>

                        <div class='row'>
                            <div class='col-lg-12 text-info text-right'>
                                <h3><?= Yii::t('app', 'Contact') ?><br/>
                                    <small><span class='text-muted'><?= \Yii::t('app/crf', 'Contact details') ?></span></small>
                                </h3>
                            </div>
                        </div>

                        <?= $form->field($model, 'organization_phone')->textInput() ?>
                        <?= $form->field($model, 'organization_phone2')->textInput() ?>
                        <?= $form->field($model, 'organization_fax')->textInput() ?>
                        <?= $form->field($model, 'organization_email')->textInput() ?>

                        <div class='row'>
                            <div class='col-lg-12 text-info text-right'>
                                <h3><?= Yii::t('app', 'Requester details') ?><br/>
                                    <small>
                                        <span class='text-muted'>
                                            <?= \Yii::t('app', 'State your personal details below.') ?>
                                            <br/>
                                            <span class="text-warning">
                                                <?= \Yii::t('app', 'Please be adviced that the information below might be used with the Organization to be registered for verification purposes.') ?>
                                            </span>
                                            <br/>
                                            <?= \Yii::t('app', 'The person responsible for (being informed about) the request status is you.') ?>
                                        </span>
                                    </small>
                                </h3>
                            </div>
                        </div>
                        
                        <?= $form->field($model, 'first_name')->textInput() ?>
                        <?= $form->field($model, 'second_name')->textInput() ?>
                        <?= $form->field($model, 'email')->textInput() ?>
                        <?= $form->field($model, 'email_repeat')->textInput() ?>

                        <?= $form->field($model, 'phone')->textInput() ?>

                        <?= $form->field($model, 'organization')->textInput()->hint(\Yii::t('app', 'Either the full name or the acronym of your organization')) ?>
                        <?= $form->field($model, 'role_in_organization')->textInput()->hint(\Yii::t('app', 'Your role within the organization')) ?>                                               
                        
                        <div class='row'>
                            <div class='col-lg-12 text-info text-right'>
                                <h3><?= Yii::t('app', 'Are you human?') ?><br/>
                                    <small><span class='text-muted'><?= \Yii::t('app', 'Please provide the verification text from the image') ?></span></small>
                                </h3>
                            </div>
                        </div>
                        <?= $form->field($model, 'captcha')->widget(\yii\captcha\Captcha::className())->label('') ?>
                        
                        </div>
                    </div>
                </div>
                <div class='panel-footer'  style='padding-bottom: 0px;'>
                    <div class='row'>
                        <div class='col-sm-10 col-xs-7'>
                            <small><em><?php //Yii::t('app','all fields are compulsory') ?></em></small>
                        </div>
                        <div class="form-group col-xs-12 pull-right text-right">
                            <?= Html::submitButton(\Yii::t('app/commands', 'Submit'), ['class' => 'btn btn-primary']) ?>
                        </div>
                    </div>
                        
                    <?php $form->end() ?>
                </div>
            </div>
        </div>
    </div>
</div>