<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

//Possible value for 'module_name' : 
//  EVENT_SECTIONS  => EVENT DRAFT VIEW PAGES

if ($module_name === 'EVENT_SECTIONS') {
  $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Events management'), 'url' => ['events/event-declaration/index']];
  $this->params['breadcrumbs'][] = ['label' => $modelEvent->event_name, 'url' => ['events/event-declaration/view', 'id'=>$modelEvent->id]];
  $this->params['breadcrumbs'][] = ['label' => $model->session_desc, 'url' => ['events/drafts/event-draft/view', 'id'=>$model->id]];
  $this->params['breadcrumbs'][] = $title_page;
}

if ($module_name === 'EVENT_SECTION_PDF') {
  $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Registered events'), 'url' => ['/events/event-declaration/index']];
  $this->params['breadcrumbs'][] = ['label' => $model->event_name, 'url' => ['/events/event-declaration/view', 'id'=>$model->event_id]];
  $this->params['breadcrumbs'][] = ['label' => $model->session_desc, 'url' => ['/events/drafts/event-draft/view', 'id'=>$model->id]];
  $this->params['breadcrumbs'][] = $title_page;
}

if ($module_name === 'ADD_SUBSECTION') {
  $this->params['breadcrumbs'][] = $title_subsection;
}

