<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

    /* @var $action string */
?>

<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="alert alert-warning text-center" role="alert">
                <h2><?= \Yii::t('app','Under construction') ?></h2>
                <p class='text-warning text-center'>
                    <?php
                        if (YII_ENV_DEV) {
                            echo \Yii::t('app', 'The \'{evt}\' action is not implemented yet', [
                                'evt'=>$action
                            ]);                            
                        }
                        else {
                            echo \Yii::t('app', 'This section is still under construction.');
                        }
                    ?>
                </p>
            </div>
        </div>
    </div>
</div>