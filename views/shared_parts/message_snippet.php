<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

//use Yii;
use \app\widgets\ActionMessageWidget;

    $session_msg = Yii::$app->session->get('finished_action_result');     //$session_msg is of type ActionMessage
    if (isset($session_msg))
    {
        echo ActionMessageWidget::widget(['actionMessage' => $session_msg,]);
        //disable the next line if you want the message to persist
        unset(\Yii::$app->session['finished_action_result']);
    }

?>


