<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\cpf\CpfSection4 */

?>

        <div class="row">
            <div class="col-lg-12" class="sy_section_h1_black">
                <div class="sy_section_h1_black" style="width: 100%; align:center;">
                    <?= Yii::t('app/cpf', 'Section {id}', ['id'=>'4']) ?>
                </div>
                <div class="sy_section_h1_black">
                    <?= Yii::t('app/cpf', 'INCIDENT DATA AND PERFORMANCE OF OFFSHORE OPERATIONS') ?>
                </div>
            </div>  <!-- end row -->
            
            <div class="col-lg-12">
                <table>
                    <tbody>
                        <tr>
                            <td class="sy_section_h2_black">4.1.</td>
                            <td class="sy_section_h2_black">
                                <?= Yii::t('app/cpf', 'Incident data') ?>
                            </td>
                            <td></td>
                        </tr>
                        <tr class='sy_crf_inferred'>
                            <td colspan = 2>
                                <span class='sy_pad_right_6'>
                                    <?= Yii::t('app/cpf', 'Number of reportable events pursuant to Annex IX' . ': ') ?>
                                </span>
                            </td>
                            <td>
                                <?php
                                    echo Html::encode($model->s4_1_n_events_total);
                                ?>
                            </td>
                        </tr>
                        <tr class='sy_crf_inferred'>
                            <td colspan = 2>
                                <?= Yii::t('app/cpf', 'of which identified as major accidents' . ': ') ?>
                            </td>
                            <td>
                                <?php
                                    echo Html::encode($model->s4_1_n_major_accidents);
                                ?>
                            </td>
                        </tr>
                    </tbody>
                </table>    
            </div> <!-- end 4.1. -->

        </div>  <!-- end row-->
