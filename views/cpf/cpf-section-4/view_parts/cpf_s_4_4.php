<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\cpf\CpfSection4 */

?>

<div class='row'>
    <div class='col-lg-12'>
        <table style='margin-top:32px;'>
            <tbody>
                <tr>
                    <td class="sy_section_h2_black">4.4.</td>
                    <td class="sy_section_h2_black">
                        <?= Yii::t('app/cpf', 'Failures of Safety and Environmental Critical Elements') ?>
                    </td>
                    <td></td>
                </tr>
            </tbody>
        </table>

        <table class='sy_cpf_table_4'>
            <thead>
                <tr>
                    <td colspan='2' class=''><?= Yii::t('app/cpf', 'SECE') ?></td>
                    <td class='sy_last_cell'><?= Yii::t('app/cpf', 'Number related to major accidents') ?></td>
                </tr>
            </thead>
            <tbody>
                <tr class='sy_crf_inferred'>
                    <td>(a)</td>
                    <td>
                        <?= $model->attributeLabels()['s4_4_nsis'] ?>
                    </td>
                    <td class='sy_num_cell sy_last_cell'>
                        <?= $model->s4_4_nsis ?>
                    </td>
                </tr>
                <tr class='sy_crf_inferred'>
                    <td>(b)</td>
                    <td>
                        <?= $model->attributeLabels()['s4_4_npcs'] ?>
                    </td>
                    <td class='sy_num_cell sy_last_cell'>
                        <?= $model->s4_4_npcs ?>
                    </td>
                </tr>
                <tr class='sy_crf_inferred'>
                    <td>(c)</td>
                    <td>
                        <?= $model->attributeLabels()['s4_4_nics'] ?>
                    </td>
                    <td class='sy_num_cell sy_last_cell'>
                        <?= $model->s4_4_nics ?>
                    </td>
                </tr>
                <tr class='sy_crf_inferred'>
                    <td>(d)</td>
                    <td>
                        <?= $model->attributeLabels()['s4_4_nds'] ?>
                    </td>
                    <td class='sy_num_cell sy_last_cell'>
                        <?= $model->s4_4_nds ?>
                    </td>
                </tr>
                <tr class='sy_crf_inferred'>
                    <td>(e)</td>
                    <td>
                        <?= $model->attributeLabels()['s4_4_npcrs'] ?>
                    </td>
                    <td class='sy_num_cell sy_last_cell'>
                        <?= $model->s4_4_npcrs ?>
                    </td>
                </tr>
                <tr class='sy_crf_inferred'>
                    <td>(f)</td>
                    <td>
                        <?= $model->attributeLabels()['s4_4_nps'] ?>
                    </td>
                    <td class='sy_num_cell sy_last_cell'>
                        <?= $model->s4_4_nps ?>
                    </td>
                </tr>
                <tr class='sy_crf_inferred'>
                    <td>(g)</td>
                    <td>
                        <?= $model->attributeLabels()['s4_4_nsds'] ?>
                    </td>
                    <td class='sy_num_cell sy_last_cell'>
                        <?= $model->s4_4_nsds ?>
                    </td>
                </tr>
                <tr class='sy_crf_inferred'>
                    <td>(h)</td>
                    <td>
                        <?= $model->attributeLabels()['s4_4_nnavaids'] ?>
                    </td>
                    <td class='sy_num_cell sy_last_cell'>
                        <?= $model->s4_4_nnavaids ?>
                    </td>
                </tr>
                <tr class='sy_crf_inferred'>
                    <td>(i)</td>
                    <td>
                        <?= $model->attributeLabels()['s4_4_roteq'] ?>
                    </td>
                    <td class='sy_num_cell sy_last_cell'>
                        <?= $model->s4_4_roteq ?>
                    </td>
                </tr>
                <tr class='sy_crf_inferred'>
                    <td>(j)</td>
                    <td>
                        <?= $model->attributeLabels()['s4_4_eere'] ?>
                    </td>
                    <td class='sy_num_cell sy_last_cell'>
                        <?= $model->s4_4_eere ?>
                    </td>
                </tr>
                <tr class='sy_crf_inferred'>
                    <td>(k)</td>
                    <td>
                        <?= $model->attributeLabels()['s4_4_ncoms'] ?>
                    </td>
                    <td class='sy_num_cell sy_last_cell'>
                        <?= $model->s4_4_ncoms ?>
                    </td>
                </tr>
                <tr class='sy_crf_inferred'>
                    <td>(l)</td>
                    <td>
                        <?= $model->attributeLabels()['s4_4_nother'] ?>
                    </td>
                    <td class='sy_num_cell sy_last_cell'>
                        <?= $model->s4_4_nother ?>
                    </td>
                </tr>
            </tbody>
        </table>
        
    </div>
</div>
