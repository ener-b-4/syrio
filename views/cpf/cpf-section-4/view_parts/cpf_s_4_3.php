<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\cpf\CpfSection4 */

?>

<div class='row'>
    <div class='col-lg-12'>
        <table style='margin-top:32px;'>
            <tbody>
                <tr>
                    <td class="sy_section_h2_black">4.3.</td>
                    <td class="sy_section_h2_black">
                        <?= Yii::t('app/cpf', 'Total number of fatalities and injuries') ?>
                        <sup>(**)</sup>
                    </td>
                    <td></td>
                </tr>
            </tbody>
        </table>

        <table class='sy_cpf_table_4'>
            <thead>
                <tr>
                    <td><?= '' ?></td>
                    <td class='sy_reg_cell'><?= Yii::t('app/cpf', 'Number') ?></td>
                    <td><?= Yii::t('app/cpf', 'Normalized value') ?></td>
                </tr>
            </thead>
            <tbody>
                <tr class=''>
                    <td>
                        <?= $model->attributeLabels()['s4_3_fatalities'] ?>
                    </td>
                    <td class='sy_reg_cell sy_num_cell'>
                        <?= $model->s4_3_fatalities ?>
                    </td>
                    <td>-</td>
                </tr>
                <tr class=''>
                    <td>
                        <?= $model->attributeLabels()['s4_3_sinj'] ?>
                    </td>
                    <td class='sy_reg_cell sy_num_cell'>
                        <?= $model->s4_3_sinj ?>
                    </td>
                    <td>-</td>
                </tr>
                <tr class=''>
                    <td>
                        <?= $model->attributeLabels()['s4_3_inj'] ?>
                    </td>
                    <td class='sy_reg_cell sy_num_cell'>
                        <?= $model->s4_3_inj ?>
                    </td>
                    <td>-</td>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="3">
                        <small>
                            <span>(<sup>**</sup>)</span>
                            <?= Yii::t('app/cpf', 'A total number as reported pursuant to 92/91/EC') ?>
                        </small>
                    </td>
                </tr>
            </tfoot>
        </table>
        
    </div>
</div>
