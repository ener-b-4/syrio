<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\cpf\CpfSection4 */

?>

        <div class='row'>
            <!-- start 4.2 -->
            <div class="col-lg-12">
                <table style='margin-top:32px;'>
                    <tbody>
                        <tr>
                            <td class="sy_section_h2_black">4.2.</td>
                            <td class="sy_section_h2_black">
                                <?= Yii::t('app/cpf', 'Annex IX Incident Categories') ?>
                            </td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
                
                <table class='sy_cpf_table_4'>
                    <thead>
                        <tr>
                            <td colspan='2'><?= Yii::t('app/cpf', 'Annex IX categories') ?></td>
                            <td class='sy_reg_cell'><?= Yii::t('app/cpf', 'Number of events') ?></td>
                            <td><?= Yii::t('app/cpf', 'Number of normalized events') ?></td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class='sy_crf_inferred'>
                            <td class='sy_col_1'><strong>(a)</strong></td>
                            <td class='sy_col_2'>
                                <strong><?= $model->attributeLabels()['s4_2_a_total'] ?></strong>
                            </td>
                            <td class='sy_reg_cell sy_num_cell'>
                                <?= $model->s4_2_a_total ?>
                            </td>
                            <td>-</td>
                        </tr>
                        <tr class='sy_crf_inferred'>
                            <td class='sy_col_1'></td>
                            <td class='sy_col_2'>
                                <?= $model->attributeLabels()['s4_2_a_if'] ?>
                            </td>
                            <td class='sy_reg_cell sy_num_cell'>
                                <?= $model->s4_2_a_if ?>
                            </td>
                            <td>-</td>
                        </tr>
                        <tr class='sy_crf_inferred'>
                            <td class='sy_col_1'></td>
                            <td class='sy_col_2'>
                                <?= $model->attributeLabels()['s4_2_a_ix'] ?>
                            </td>
                            <td class='sy_reg_cell sy_num_cell'>
                                <?= $model->s4_2_a_ix ?>
                            </td>
                            <td>-</td>
                        </tr>
                        <tr class='sy_crf_inferred'>
                            <td class='sy_col_1'></td>
                            <td class='sy_col_2'>
                                <?= $model->attributeLabels()['s4_2_a_nig'] ?>
                            </td>
                            <td class='sy_reg_cell sy_num_cell'>
                                <?= $model->s4_2_a_nig ?>
                            </td>
                            <td>-</td>
                        </tr>
                        <tr class='sy_crf_inferred'>
                            <td class='sy_col_1'></td>
                            <td class='sy_col_2'>
                                <?= $model->attributeLabels()['s4_2_a_nio'] ?>
                            </td>
                            <td class='sy_reg_cell sy_num_cell'>
                                <?= $model->s4_2_a_nio ?>
                            </td>
                            <td>-</td>
                        </tr>
                        <tr class='sy_crf_inferred'>
                            <td class='sy_col_1'></td>
                            <td class='sy_col_2'>
                                <?= $model->attributeLabels()['s4_2_a_haz'] ?>
                            </td>
                            <td class='sy_reg_cell sy_num_cell'>
                                <?= $model->s4_2_a_haz ?>
                            </td>
                            <td>-</td>
                        </tr>
                        
                        <!-- 4_2_b -->
                        <tr class='sy_crf_inferred'>
                            <td class='sy_col_1'><strong>(b)</strong></td>
                            <td class='sy_col_2'>
                                <strong><?= $model->attributeLabels()['s4_2_b_total'] ?></strong>
                            </td>
                            <td class='sy_reg_cell sy_num_cell'>
                                <strong><?= $model->s4_2_b_total ?></strong>
                            </td>
                            <td>-</td>
                        </tr>
                        <tr class=''>
                            <td class='sy_col_1'></td>
                            <td class='sy_col_2'>
                                <?= $model->attributeLabels()['s4_2_b_bo'] ?>
                            </td>
                            <td class='sy_reg_cell sy_num_cell'>
                                <?= $model->s4_2_b_bo ?>
                            </td>
                            <td>-</td>
                        </tr>
                        <tr class='sy_crf_inferred'>
                            <td class='sy_col_1'></td>
                            <td class='sy_col_2'>
                                <?= $model->attributeLabels()['s4_2_b_bda'] ?>
                            </td>
                            <td class='sy_reg_cell sy_num_cell'>
                                <?= $model->s4_2_b_bda ?>
                            </td>
                            <td>-</td>
                        </tr>
                        <tr class='sy_crf_inferred'>
                            <td class='sy_col_1'></td>
                            <td class='sy_col_2'>
                                <?= $model->attributeLabels()['s4_2_b_wbf'] ?>
                            </td>
                            <td class='sy_reg_cell sy_num_cell'>
                                <?= $model->s4_2_b_wbf ?>
                            </td>
                            <td>-</td>
                        </tr>
                        <!-- 4_2_c -->
                        <tr class='sy_crf_inferred'>
                            <td class='sy_col_1'><strong>(c)</strong></td>
                            <td class='sy_col_2'>
                                <strong><?= $model->attributeLabels()['s4_2_c_total'] ?></strong>
                            </td>
                            <td class='sy_reg_cell sy_num_cell'>
                                <strong><?= $model->s4_2_c_total ?></strong>
                            </td>
                            <td>-</td>
                        </tr>
                        <!-- 4_2_d -->
                        <tr class='sy_crf_inferred'>
                            <td class='sy_col_1'><strong>(d)</strong></td>
                            <td class='sy_col_2'>
                                <strong><?= $model->attributeLabels()['s4_2_d_total'] ?></strong>
                            </td>
                            <td class='sy_reg_cell sy_num_cell'>
                                <strong><?= $model->s4_2_d_total ?></strong>
                            </td>
                            <td>-</td>
                        </tr>
                        <tr class=''>
                            <td class='sy_col_1'></td>
                            <td class='sy_col_2'>
                                <?= $model->attributeLabels()['s4_2_d_si'] ?>
                            </td>
                            <td class='sy_reg_cell sy_num_cell'>
                                <?= $model->s4_2_d_si ?>
                            </td>
                            <td>-</td>
                        </tr>
                        <tr class=''>
                            <td class='sy_col_1'></td>
                            <td class='sy_col_2'>
                                <?= $model->attributeLabels()['s4_2_d_sb'] ?>
                            </td>
                            <td class='sy_reg_cell sy_num_cell'>
                                <?= $model->s4_2_d_sb ?>
                            </td>
                            <td>-</td>
                        </tr>
                        <tr class=''>
                            <td class='sy_col_1'></td>
                            <td class='sy_col_2'>
                                <?= $model->attributeLabels()['s4_2_d_sk'] ?>
                            </td>
                            <td class='sy_reg_cell sy_num_cell'>
                                <?= $model->s4_2_d_sk ?>
                            </td>
                            <td>-</td>
                        </tr>
                        <!-- 4_2_e -->
                        <tr class='sy_crf_inferred'>
                            <td class='sy_col_1'><strong>(e)</strong></td>
                            <td class='sy_col_2'>
                                <strong><?= $model->attributeLabels()['s4_2_e_total'] ?></strong>
                            </td>
                            <td class='sy_reg_cell sy_num_cell'>
                                <strong><?= $model->s4_2_e_total ?></strong>
                            </td>
                            <td>-</td>
                        </tr>
                        <!-- 4_2_f -->
                        <tr class='sy_crf_inferred'>
                            <td class='sy_col_1'><strong>(f)</strong></td>
                            <td class='sy_col_2'>
                                <strong><?= $model->attributeLabels()['s4_2_f_total'] ?></strong>
                            </td>
                            <td class='sy_reg_cell sy_num_cell'>
                                <strong><?= $model->s4_2_f_total ?></strong>
                            </td>
                            <td>-</td>
                        </tr>
                        <!-- 4_2_g -->
                        <tr class='sy_crf_inferred'>
                            <td class='sy_col_1'><strong>(g)</strong></td>
                            <td class='sy_col_2'>
                                <strong><?= $model->attributeLabels()['s4_2_g_total'] ?></strong>
                            </td>
                            <td class='sy_reg_cell sy_num_cell'>
                                <strong><?= $model->s4_2_g_total ?></strong>
                            </td>
                            <td>-</td>
                        </tr>
                        <!-- 4_2_h -->
                        <tr class='sy_crf_inferred'>
                            <td class='sy_col_1'><strong>(h)</strong></td>
                            <td class='sy_col_2'>
                                <strong><?= $model->attributeLabels()['s4_2_h_total'] ?></strong>
                            </td>
                            <td class='sy_reg_cell sy_num_cell'>
                                <strong><?= $model->s4_2_h_total ?></strong>
                            </td>
                            <td>-</td>
                        </tr>
                        <!-- 4_2_i -->
                        <tr class='sy_crf_inferred'>
                            <td class='sy_col_1'><strong>(i)</strong></td>
                            <td class='sy_col_2'>
                                <strong><?= $model->attributeLabels()['s4_2_i_total'] ?></strong>
                            </td>
                            <td class='sy_reg_cell sy_num_cell'>
                                <strong><?= $model->s4_2_i_total ?></strong>
                            </td>
                            <td>-</td>
                        </tr>
                        <!-- 4_2_j -->
                        <tr class='sy_crf_inferred'>
                            <td class='sy_col_1'><strong>(j)</strong></td>
                            <td class='sy_col_2'>
                                <strong><?= $model->attributeLabels()['s4_2_j_total'] ?></strong>
                            </td>
                            <td class='sy_reg_cell sy_num_cell'>
                                <strong><?= $model->s4_2_j_total ?></strong>
                            </td>
                            <td>-</td>
                        </tr>
                        
                    </tbody>
                </table>
            </div>
        </div>  <!-- end row 4.2. -->
