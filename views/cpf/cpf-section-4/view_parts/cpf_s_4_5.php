<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\cpf\CpfSection4 */

?>

<div class='row'>
    <div class='col-lg-12'>
        <table style='margin-top:32px;'>
            <tbody>
                <tr>
                    <td class="sy_section_h2_black">4.5.</td>
                    <td class="sy_section_h2_black">
                        <?= Yii::t('app/cpf', 'Direct and Underlying causes of major incidents') ?>
                    </td>
                    <td></td>
                </tr>
            </tbody>
        </table>

        <table class='sy_cpf_table_4'>
            <thead>
                <tr>
                    <!-- left -->
                    <td colspan='2' class=''><?= Yii::t('app/cpf', 'Causes') ?></td>
                    <td class='sy_last_cell'><?= Yii::t('app/cpf', 'Number of incidents') ?></td>

                    <!-- right -->
                    <td colspan='2' class=''><?= Yii::t('app/cpf', 'Causes') ?></td>
                    <td class='sy_last_cell'><?= Yii::t('app/cpf', 'Number of incidents') ?></td>
                </tr>
            </thead>
            <tbody>
                <tr class=''>
                    <!-- left -->
                    <td><strong>(a)</strong></td>
                    <td>
                        <strong><?= $model->attributeLabels()['s4_5_a_total'] ?></strong>
                    </td>
                    <td class='sy_num_cell sy_reg_cell'>
                        <strong><?= $model->s4_5_a_total ?></strong>
                    </td>

                    <!-- right -->
                    <td><strong>(c)</strong></td>
                    <td>
                        <strong><?= $model->attributeLabels()['s4_5_c_total'] ?></strong>
                    </td>
                    <td class='sy_num_cell sy_last_cell'>
                        <strong><?= $model->s4_5_c_total ?></strong>
                    </td>
                </tr>
                
                <tr class=''>
                    <!-- left -->
                    <td></td>
                    <td class='sy_cpf_inferred'>
                        <em><?= $model->attributeLabels()['s4_5_a_df'] ?></em>
                    </td>
                    <td class='sy_num_cell sy_reg_cell sy_cpf_inferred'>
                        <?= $model->s4_5_a_df ?>
                    </td>

                    <!-- right -->
                    <td></td>
                    <td>
                        <em><?= $model->attributeLabels()['s4_5_c_inq__rap'] ?></em>
                    </td>
                    <td class='sy_num_cell sy_last_cell'>
                        <?= $model->s4_5_c_inq__rap ?>
                    </td>
                </tr>
                
                <tr class=''>
                    <!-- left -->
                    <td></td>
                    <td class='sy_cpf_inferred'>
                        <em><?= $model->attributeLabels()['s4_5_a_co__int'] ?></em>
                    </td>
                    <td class='sy_num_cell sy_reg_cell sy_cpf_inferred'>
                        <?= $model->s4_5_a_co__int ?>
                    </td>

                    <!-- right -->
                    <td></td>
                    <td>
                        <em><?= $model->attributeLabels()['s4_5_c_inq__instp'] ?></em>
                    </td>
                    <td class='sy_num_cell sy_last_cell'>
                        <?= $model->s4_5_c_inq__instp ?>
                    </td>
                </tr>

                <tr class=''>
                    <!-- left -->
                    <td></td>
                    <td class='sy_cpf_inferred'>
                        <em><?= $model->attributeLabels()['s4_5_a_co__ext'] ?></em>
                    </td>
                    <td class='sy_num_cell sy_reg_cell sy_cpf_inferred'>
                        <?= $model->s4_5_a_co__ext ?>
                    </td>

                    <!-- right -->
                    <td></td>
                    <td class='sy_cpf_inferred'>
                        <em><?= $model->attributeLabels()['s4_5_c_nc__prc'] ?></em>
                    </td>
                    <td class='sy_num_cell sy_last_cell sy_cpf_inferred'>
                        <?= $model->s4_5_c_nc__prc ?>
                    </td>
                </tr>

                <tr class=''>
                    <!-- left -->
                    <td></td>
                    <td class='sy_cpf_inferred'>
                        <em><?= $model->attributeLabels()['s4_5_a_mf__f'] ?></em>
                    </td>
                    <td class='sy_num_cell sy_reg_cell sy_cpf_inferred'>
                        <?= $model->s4_5_a_mf__f ?>
                    </td>

                    <!-- right -->
                    <td></td>
                    <td class='sy_cpf_inferred'>
                        <em><?= $model->attributeLabels()['s4_5_c_nc__ptw'] ?></em>
                    </td>
                    <td class='sy_num_cell sy_last_cell sy_cpf_inferred'>
                        <?= $model->s4_5_c_nc__ptw ?>
                    </td>
                </tr>

                <tr class=''>
                    <!-- left -->
                    <td></td>
                    <td class='sy_cpf_inferred'>
                        <em><?= $model->attributeLabels()['s4_5_a_mf__wo'] ?></em>
                    </td>
                    <td class='sy_num_cell sy_reg_cell sy_cpf_inferred'>
                        <?= $model->s4_5_a_mf__wo ?>
                    </td>

                    <!-- right -->
                    <td></td>
                    <td>
                        <em><?= $model->attributeLabels()['s4_5_c_inq__com'] ?></em>
                    </td>
                    <td class='sy_num_cell sy_last_cell'>
                        <?= $model->s4_5_c_inq__com ?>
                    </td>
                </tr>

                <tr class=''>
                    <!-- left -->
                    <td></td>
                    <td class='sy_cpf_inferred'>
                        <em><?= $model->attributeLabels()['s4_5_a_mf__dm'] ?></em>
                    </td>
                    <td class='sy_num_cell sy_reg_cell sy_cpf_inferred'>
                        <?= $model->s4_5_a_mf__dm ?>
                    </td>

                    <!-- right -->
                    <td></td>
                    <td>
                        <em><?= $model->attributeLabels()['s4_5_c_inq__pc'] ?></em>
                    </td>
                    <td class='sy_num_cell sy_last_cell'>
                        <?= $model->s4_5_c_inq__pc ?>
                    </td>
                </tr>

                <tr class=''>
                    <!-- left -->
                    <td></td>
                    <td class='sy_cpf_inferred'>
                        <em><?= $model->attributeLabels()['s4_5_a_mf__vh'] ?></em>
                    </td>
                    <td class='sy_num_cell sy_reg_cell sy_cpf_inferred'>
                        <?= $model->s4_5_a_mf__vh ?>
                    </td>

                    <!-- right -->
                    <td></td>
                    <td>
                        <em><?= $model->attributeLabels()['s4_5_c_inq__sup'] ?></em>
                    </td>
                    <td class='sy_num_cell sy_last_cell'>
                        <?= $model->s4_5_c_inq__sup ?>
                    </td>
                </tr>
                
                <tr class=''>
                    <!-- left -->
                    <td></td>
                    <td class='sy_cpf_inferred'>
                        <em><?= $model->attributeLabels()['s4_5_a_if'] ?></em>
                    </td>
                    <td class='sy_num_cell sy_reg_cell sy_cpf_inferred'>
                        <?= $model->s4_5_a_if ?>
                    </td>

                    <!-- right -->
                    <td></td>
                    <td>
                        <em><?= $model->attributeLabels()['s4_5_c_inq__safelead'] ?></em>
                    </td>
                    <td class='sy_num_cell sy_last_cell'>
                        <?= $model->s4_5_c_inq__safelead ?>
                    </td>
                </tr>

                <tr class=''>
                    <!-- left -->
                    <td></td>
                    <td class='sy_cpf_inferred'>
                        <em><?= $model->attributeLabels()['s4_5_a_csf'] ?></em>
                    </td>
                    <td class='sy_num_cell sy_reg_cell sy_cpf_inferred'>
                        <?= $model->s4_5_a_csf ?>
                    </td>

                    <!-- right -->
                    <td></td>
                    <td class='sy_cpf_inferred'>
                        <em><?= $model->attributeLabels()['s4_5_c_other'] ?></em>
                    </td>
                    <td class='sy_num_cell sy_last_cell sy_cpf_inferred'>
                        <?= $model->s4_5_c_other ?>
                    </td>
                </tr>

                <tr class=''>
                    <!-- left -->
                    <td></td>
                    <td class='sy_cpf_inferred'>
                        <em><?= $model->attributeLabels()['s4_5_a_other'] ?></em>
                    </td>
                    <td class='sy_num_cell sy_reg_cell sy_cpf_inferred'>
                        <?= $model->s4_5_a_other ?>
                    </td>

                    <!-- right -->
                    <td></td>
                    <td>
                    </td>
                    <td class='sy_num_cell sy_last_cell'>
                    </td>
                </tr>


                <!-- NEXT SECTIONS -->
                
                <tr class=''>
                    <!-- left -->
                    <td><strong>(c)</strong></td>
                    <td>
                        <strong><?= $model->attributeLabels()['s4_5_b_total'] ?></strong>
                    </td>
                    <td class='sy_num_cell sy_reg_cell'>
                        <strong><?= $model->s4_5_a_total ?></strong>
                    </td>

                    <!-- right -->
                    <td><strong>(d)</strong></td>
                    <td>
                        <strong><?= $model->attributeLabels()['s4_5_d_total'] ?></strong>
                    </td>
                    <td class='sy_num_cell sy_last_cell'>
                        <strong><?= $model->s4_5_c_total ?></strong>
                    </td>
                </tr>
                
                <tr class=''>
                    <!-- left -->
                    <td></td>
                    <td class='sy_cpf_inferred'>
                        <em><?= $model->attributeLabels()['s4_5_b_err__op'] ?></em>
                    </td>
                    <td class='sy_num_cell sy_reg_cell sy_cpf_inferred'>
                        <?= $model->s4_5_b_err__op ?>
                    </td>

                    <!-- right -->
                    <td></td>
                    <td>
                        <em><?= $model->attributeLabels()['s4_5_d_exc__design__wind'] ?></em>
                    </td>
                    <td class='sy_num_cell sy_last_cell'>
                        <?= $model->s4_5_d_exc__design__wind ?>
                    </td>
                </tr>
                
                <tr class=''>
                    <!-- left -->
                    <td></td>
                    <td class='sy_cpf_inferred'>
                        <em><?= $model->attributeLabels()['s4_5_b_err__mnt'] ?></em>
                    </td>
                    <td class='sy_num_cell sy_reg_cell sy_cpf_inferred'>
                        <?= $model->s4_5_b_err__mnt ?>
                    </td>

                    <!-- right -->
                    <td></td>
                    <td>
                        <em><?= $model->attributeLabels()['s4_5_d_exc__design__wave'] ?></em>
                    </td>
                    <td class='sy_num_cell sy_last_cell'>
                        <?= $model->s4_5_d_exc__design__wave ?>
                    </td>
                </tr>

                <tr class=''>
                    <!-- left -->
                    <td></td>
                    <td class='sy_cpf_inferred'>
                        <em><?= $model->attributeLabels()['s4_5_b_err__tst'] ?></em>
                    </td>
                    <td class='sy_num_cell sy_reg_cell sy_cpf_inferred'>
                        <?= $model->s4_5_b_err__tst ?>
                    </td>

                    <!-- right -->
                    <td></td>
                    <td>
                        <em><?= $model->attributeLabels()['s4_5_d_exc__design__lowvis'] ?></em>
                    </td>
                    <td class='sy_num_cell sy_last_cell'>
                        <?= $model->s4_5_d_exc__design__lowvis ?>
                    </td>
                </tr>

                <tr class=''>
                    <!-- left -->
                    <td></td>
                    <td class='sy_cpf_inferred'>
                        <em><?= $model->attributeLabels()['s4_5_b_err__insp'] ?></em>
                    </td>
                    <td class='sy_num_cell sy_reg_cell sy_cpf_inferred'>
                        <?= $model->s4_5_b_err__insp ?>
                    </td>

                    <!-- right -->
                    <td></td>
                    <td>
                        <em><?= $model->attributeLabels()['s4_5_d_ice__icebergs'] ?></em>
                    </td>
                    <td class='sy_num_cell sy_last_cell'>
                        <?= $model->s4_5_d_ice__icebergs ?>
                    </td>
                </tr>

                <tr class=''>
                    <!-- left -->
                    <td></td>
                    <td>
                        <em><?= $model->attributeLabels()['s4_5_b_err__dsgn'] ?></em>
                    </td>
                    <td class='sy_num_cell sy_reg_cell'>
                        <?= $model->s4_5_b_err__dsgn ?>
                    </td>

                    <!-- right -->
                    <td></td>
                    <td>
                        <em><?= $model->attributeLabels()['s4_5_d_other'] ?></em>
                    </td>
                    <td class='sy_num_cell sy_last_cell'>
                        <?= $model->s4_5_d_other ?>
                    </td>
                </tr>

                <tr class=''>
                    <!-- left -->
                    <td></td>
                    <td>
                        <em><?= $model->attributeLabels()['s4_5_b_other'] ?></em>
                    </td>
                    <td class='sy_num_cell sy_reg_cell'>
                        <?= $model->s4_5_b_other ?>
                    </td>

                    <!-- right -->
                    <td></td>
                    <td>
                    </td>
                    <td class='sy_num_cell sy_last_cell'>
                    </td>
                </tr>
                
            </tbody>
        </table>
        
    </div>
</div>
