<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\cpf\CpfSection4 */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cpf-section4-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'year')->textInput() ?>

    <?= $form->field($model, 's4_1_n_events_total')->textInput() ?>

    <?= $form->field($model, 's4_1_n_major_accidents')->textInput() ?>

    <?= $form->field($model, 's4_2_a_total')->textInput() ?>

    <?= $form->field($model, 's4_2_a_if')->textInput() ?>

    <?= $form->field($model, 's4_2_a_ix')->textInput() ?>

    <?= $form->field($model, 's4_2_a_nig')->textInput() ?>

    <?= $form->field($model, 's4_2_a_nio')->textInput() ?>

    <?= $form->field($model, 's4_2_a_haz')->textInput() ?>

    <?= $form->field($model, 's4_2_b_total')->textInput() ?>

    <?= $form->field($model, 's4_2_b_bo')->textInput() ?>

    <?= $form->field($model, 's4_2_b_bda')->textInput() ?>

    <?= $form->field($model, 's4_2_b_wbf')->textInput() ?>

    <?= $form->field($model, 's4_2_c_total')->textInput() ?>

    <?= $form->field($model, 's4_2_d_total')->textInput() ?>

    <?= $form->field($model, 's4_2_d_si')->textInput() ?>

    <?= $form->field($model, 's4_2_d_sb')->textInput() ?>

    <?= $form->field($model, 's4_2_d_sk')->textInput() ?>

    <?= $form->field($model, 's4_2_e_total')->textInput() ?>

    <?= $form->field($model, 's4_2_f_total')->textInput() ?>

    <?= $form->field($model, 's4_2_g_total')->textInput() ?>

    <?= $form->field($model, 's4_2_h_total')->textInput() ?>

    <?= $form->field($model, 's4_2_i_total')->textInput() ?>

    <?= $form->field($model, 's4_2_j_total')->textInput() ?>

    <?= $form->field($model, 's4_3_total')->textInput() ?>

    <?= $form->field($model, 's4_3_fatalities')->textInput() ?>

    <?= $form->field($model, 's4_3_inj')->textInput() ?>

    <?= $form->field($model, 's4_3_sinj')->textInput() ?>

    <?= $form->field($model, 's4_4_nsis')->textInput() ?>

    <?= $form->field($model, 's4_4_npcs')->textInput() ?>

    <?= $form->field($model, 's4_4_nics')->textInput() ?>

    <?= $form->field($model, 's4_4_nds')->textInput() ?>

    <?= $form->field($model, 's4_4_npcrs')->textInput() ?>

    <?= $form->field($model, 's4_4_nps')->textInput() ?>

    <?= $form->field($model, 's4_4_nsds')->textInput() ?>

    <?= $form->field($model, 's4_4_nnavaids')->textInput() ?>

    <?= $form->field($model, 's4_4_roteq')->textInput() ?>

    <?= $form->field($model, 's4_4_eere')->textInput() ?>

    <?= $form->field($model, 's4_4_ncoms')->textInput() ?>

    <?= $form->field($model, 's4_4_nother')->textInput() ?>

    <?= $form->field($model, 's4_5_a_total')->textInput() ?>

    <?= $form->field($model, 's4_5_a_df')->textInput() ?>

    <?= $form->field($model, 's4_5_a_co__int')->textInput() ?>

    <?= $form->field($model, 's4_5_a_co__ext')->textInput() ?>

    <?= $form->field($model, 's4_5_a_mf__f')->textInput() ?>

    <?= $form->field($model, 's4_5_a_mf__wo')->textInput() ?>

    <?= $form->field($model, 's4_5_a_mf__dm')->textInput() ?>

    <?= $form->field($model, 's4_5_a_mf__vh')->textInput() ?>

    <?= $form->field($model, 's4_5_a_if')->textInput() ?>

    <?= $form->field($model, 's4_5_a_csf')->textInput() ?>

    <?= $form->field($model, 's4_5_a_other')->textInput() ?>

    <?= $form->field($model, 's4_5_b_total')->textInput() ?>

    <?= $form->field($model, 's4_5_b_err__op')->textInput() ?>

    <?= $form->field($model, 's4_5_b_err__mnt')->textInput() ?>

    <?= $form->field($model, 's4_5_b_err__tst')->textInput() ?>

    <?= $form->field($model, 's4_5_b_err__insp')->textInput() ?>

    <?= $form->field($model, 's4_5_b_err__dsgn')->textInput() ?>

    <?= $form->field($model, 's4_5_b_other')->textInput() ?>

    <?= $form->field($model, 's4_5_c_total')->textInput() ?>

    <?= $form->field($model, 's4_5_c_inq__rap')->textInput() ?>

    <?= $form->field($model, 's4_5_c_inq__instp')->textInput() ?>

    <?= $form->field($model, 's4_5_c_nc__prc')->textInput() ?>

    <?= $form->field($model, 's4_5_c_nc__ptw')->textInput() ?>

    <?= $form->field($model, 's4_5_c_inq__com')->textInput() ?>

    <?= $form->field($model, 's4_5_c_inq__pc')->textInput() ?>

    <?= $form->field($model, 's4_5_c_inq__sup')->textInput() ?>

    <?= $form->field($model, 's4_5_c_inq__safelead')->textInput() ?>

    <?= $form->field($model, 's4_5_c_other')->textInput() ?>

    <?= $form->field($model, 's4_5_d_total')->textInput() ?>

    <?= $form->field($model, 's4_5_d_exc__design__wind')->textInput() ?>

    <?= $form->field($model, 's4_5_d_exc__design__wave')->textInput() ?>

    <?= $form->field($model, 's4_5_d_exc__design__lowvis')->textInput() ?>

    <?= $form->field($model, 's4_5_d_ice__icebergs')->textInput() ?>

    <?= $form->field($model, 's4_5_d_other')->textInput() ?>

    <?= $form->field($model, 's4_6')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app\cpf', 'Create') : Yii::t('app\cpf', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
