<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\cpf\CpfSection4 */

$this->title = $model->year;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app\cpf', 'Cpf Section4s'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cpf-section4-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="container">
    
        <?= $this->render('view_parts/cpf_s_4_1', ['model'=>$model]) ?>
        <?= $this->render('view_parts/cpf_s_4_2', ['model'=>$model]) ?>
        <?= $this->render('view_parts/cpf_s_4_3', ['model'=>$model]) ?>
        <?= $this->render('view_parts/cpf_s_4_4', ['model'=>$model]) ?>
        <?= $this->render('view_parts/cpf_s_4_5', ['model'=>$model]) ?>
        
    
    </div>

</div>
