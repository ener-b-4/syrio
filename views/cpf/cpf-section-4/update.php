<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\cpf\CpfSection4 */

$this->title = Yii::t('app/cpf', 'Update {modelClass}: ', [
    'modelClass' => 'Cpf Section4',
]) . ' ' . $model->year;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/cpf', 'Cpf Section4s'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->year, 'url' => ['view', 'id' => $model->year]];
$this->params['breadcrumbs'][] = Yii::t('app/cpf', 'Update');
?>
<div class="cpf-section4-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
