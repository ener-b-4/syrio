<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\assets\FontAwesomeAsset;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
FontAwesomeAsset::register($this);

/* setup the interface depending on type of user */
$head_title = 'SyRIO (' . Yii::$app->params['competentAuthority'] . ')';

?>

<?php

$wrap_values = 60;  //the default value for margin and padding for the wrap
        //&& array_key_exists(Yii::$app->getRequest()->getCookies()->getValue('_lang'), Yii::$app->params['languages']))

if(!key_exists('SyRIOCookieAgreed', $_COOKIE)) {
    $wrap_values += 0; //39;
}

$wrap_values_string = (string)$wrap_values . 'px';
?>


<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    
<!-- Begin Cookie Consent plugin by Silktide - http://silktide.com/cookieconsent -->
<!--
<script type="text/javascript">
    window.cookieconsent_options = {"message":"This website uses cookies to ensure you get the best experience on our website","dismiss":"Got it!","learnMore":"More info","link":"http://my_cookie_policy","theme":"dark-floating"};
</script>

<script type="text/javascript" src="//s3.amazonaws.com/cc.silktide.com/cookieconsent.latest.min.js"></script>
-->
<!-- End Cookie Consent plugin -->
    
    
</head>
<body>

<?php $this->beginBody() ?>
    <?php echo "<div class='wrap' style=' margin-bottom:-$wrap_values_string; padding-bottom: $wrap_values_string'>" ?>
        <?= app\widgets\cookies\Cookies::widget() ?>
        <?php
            $items = [
                    ['label' => ucfirst(Yii::t('app', 'home')), 'url' => ['/site/index'], 'options' => ['encodeLabels'=>false]],
                ];

            if (!Yii::$app->user->isGuest)
            {
                if (Yii::$app->user->identity->isOperatorUser || Yii::$app->user->identity->isInstallationUser)
                {
                    $items[] = ['label' => ucfirst(Yii::t('app', 'incidents')), 'items' => [
                        ['label' => ucfirst(Yii::t('app', 'incidents hub')), 'url' => ['/events/event-declaration']],
                        ['label' => Yii::t('app', 'Register {evt}', ['evt'=>Yii::t('app', 'new event')]), 'url' => ['/events/event-declaration/create']],
                    ]];
                }
                
                $items[] = ['label' => ucfirst(Yii::t('app', 'Management')), 'url' => ['/management/']];
                if (Yii::$app->user->can('sys_admin'))
                {
                    /*
                    $items[] = ['label' => Yii::t('app', 'Competent Authorities'), 'items' => [
                        ['label' => Yii::t('app', 'Manage'), 'url' => ['/ca/competent-authority/index']],
                        ['label' => Yii::t('app', 'Registration requests'), 'url' => ['requests/ca-org-register']],
                    ]];
                     * 
                     */
                    $items[] = ['label' => 'RBAC Management', 'url' => ['/site/rbac'], 'items' => [
                                ['label' => 'Roles', 'url' => ['/site/roles', 'tag' => 'roles']],
                                ['label' => 'Permissions', 'url' => ['/site/permissions', 'tag' => 'permissions']],
                                ['label' => 'Rules', 'url' => ['/site/rules', 'tag' => 'rules']],
                                ['label' => 'Assignments', 'url' => ['/site/assignments', 'tag' => 'assignments']],
                               ]];
                }
                
                $items[] = ['label' => ucfirst(Yii::t('app', 'requests')), 'url' => ['/site/requests'], 'options' => ['encodeLabels'=>false]];
                
                foreach (Yii::$app->user->identity->getRoles() as $role)
                {
                    $s = $role . '<br/>';
                }
            }
            else {
                $items[] = ['label' => ucfirst(Yii::t('app', 'requests')), 'url' => ['/site/requests'], 'options' => ['encodeLabels'=>false]];
                $s = '';
            }
            
            ?>
        
        
            
                 
            
            <?php
            
            Yii::$app->user->isGuest ?
                $items[] = ['label' => Yii::t('app', 'Login'), 'url' => ['/site/login']] :
                $items[] = [
                  '<li role="presentation" class="dropdown-header">' . ucfirst(Yii::t('app', 'users')) . '</li>',
                  'label'=>Yii::$app->user->identity->username, 'items' => [
                        ['label'=>app\components\helpers\AdminHelper::buildUserRoles(Yii::$app->user->id)],
                        ['label' => Yii::t('app', 'Logout'), 
                            'url' => ['/site/logout'],
                            'linkOptions' => ['data-method' => 'post']
                            ],
                        '<li role="separator" class="divider"></li>',
                        ['label' => Yii::t('app', 'Account'), 
                            'url' => ['/admin/users/user/account'],
                            //'linkOptions' => ['data-method' => 'get']
                            ],
                  ],  
                ];
                
            NavBar::begin([
                'id'=>'syrio-navbar',                
                'brandLabel' => $head_title,
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top',
                ],
            ]);
            echo Nav::widget([
                'options' => [
                    'class' => 'navbar-nav navbar-right',
                    'id'=>'syrio-navbar-list',
                    ],
                'items' => $items,
                'encodeLabels'=>false
            ]);
            NavBar::end();
        ?>

        <div class="container">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= $content ?>
        </div>
    
    </div>


    <footer class="footer">
        <div class="container">
            <div class='row'>
                <div class='col-xs-5'>
                    <ul class="list-inline">
                        <li>&copy; <?= 2016 ?></li>
                        <li><?= app\widgets\LanguageSelectorWidget::widget() ?></li>
                    </ul>
                </div>
                
                <div class='col-xs-7 pull-right' style='text-align: right'>
                    <ul class='list-inline'>
                        <li>
                            <?= Html::a(\Yii::t('app', 'About'), ['//site/about']) ?>
                        </li>
                        <li>
                            <?= Html::a(\Yii::t('app', 'Contact'), ['//site/contact']) ?>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
    
<?php $this->endBody() ?>
</body>
<?php
$js = <<< 'SCRIPT'
/* To initialize BS3 tooltips set this below */
    $('body').tooltip({selector: '[data-toggle="tooltip"]'});
/* To initialize BS3 popovers set this below */
    $('body').popover({selector: '[data-toggle="popover"]'});

$(window).on('load', function(){
    var w1_width = ($('#syrio-navbar-list').width());
    var w0_collapse_width = $('#syrio-navbar-collapse').width();
    
    if (!w1_width || w1_width==0) {
        w1_width = 50;
        $('.navbar-brand').width($(window).width()- 100);
    } else {
        $('.navbar-brand').width(w0_collapse_width - w1_width - 10);
    }

});

$(window).on('resize', function(){
    var w1_width = ($('#syrio-navbar-list').width());
    var w0_collapse_width = $('#syrio-navbar-collapse').width();
    
    if (!w1_width || w1_width==0) {
        w1_width = 50;
        $('.navbar-brand').width($(window).width()- 100);
    } else {
        $('.navbar-brand').width(w0_collapse_width - w1_width-10);
    }

});

SCRIPT;
// Register tooltip/popover initialization javascript
$this->registerJs($js, yii\web\View::POS_READY);
?>
</html>
<?php $this->endPage() ?>
