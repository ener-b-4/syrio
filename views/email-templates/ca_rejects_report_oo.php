<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

/**
 * This renders the notification message to the OO rapporteur when an incident report is REJECTED.
 * 
 * @author bogdanV
 */

/* @var $model app\models\ca\IncidentCategorization */
/* @var $details mixed array */

//use app\models\forms\CaNewUserRequestForm;

?>

<h1>Incident report <span style="color: #FF000A">REJECTED</span></h1>
<p>This is an automatic message. Please do not reply.</p>
<div>
    <p>
        Your report for incident <b><?= $details['name'] ?></b> has been REJECTED the competent authority 
        (<?= strtoupper(\Yii::$app->params['competentAuthorityIso2']) ?>) with the following justification:
    </p>
    <p></p>
    <p>
        <span style ="color: #007fff">
            <i>
                <?= nl2br($model->rejection_desc) ?>
            </i>
        </span>
    </p>
        
    <table class="def">
            <tr>
                <td>Date:</td>
                <td><?= $details['date'] ?></td>
            </tr>
    </table>
    <p>=====================================</p>
    <p></p>
</div>
