<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use \app\models\requests\Request;
/**
 * This renders the user request registration confirmation message to the user
 * 
 * @author bogdanV
 */

/* @var $model \app\models\requests\Request */

switch ($model->status)
{
	case Request::STATUS_ACCEPTED_REJECTED:
		$status_text = 'REJECTED, with the following justification:';
		break;
	case Request::STATUS_ACCEPTED_CLOSED:
		$status_text = 'ACCEPTED';
		break;
}

if (isset($model->justification) && trim($model->justification) !=='')
{
	$justification = $model->justification;
}
else
{
	$justification = '(no other message)';
}
?>

<h1>User registration request - processed</h1>
<p>This is an automatic message. Please do not reply.</p>
<div>
	<p>Your request has been <?= $status_text ?></p>
	<p></p>
        <p><em><?= nl2br($justification) ?></em></p>
</div>
