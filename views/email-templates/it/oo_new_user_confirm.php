<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

/**
 * This renders the user registration confirmation message to the newly created user
 * 
 * @author bogdanV
 */

/* @var $model app\admin\users\User */
/* @var $tempPassword string the temporary password */

//use app\models\forms\CaNewUserRequestForm;

$reqUrl = Yii::$app->getUrlManager()->createAbsoluteUrl('site/login');

?>

<h1>Richiesta di registrazione per l'utente</h1>
<p>Questo è un messaggio automatico . Si prega di non rispondere .</p>
<div>
    <p>Il tuo account utente Operatore/Proprietario è stato creato.</p>
    <p></p>
    <p>Si prega <a href='<?=$reqUrl?>'> di effettuare il login</a> in SyRIO con le seguenti credenziali:</p>
    <p></p>
    <table>
            <tr>
                <td>Nome utente: </td>
                <td><?= $model->username ?></td>
            </tr>
            <tr>
                <td>Password: </td>
                <td><?= $tempPassword ?></td>
            </tr>
    </table>

    <p>===========================</p>
    <p></p>
</div>
