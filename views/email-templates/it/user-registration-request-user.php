<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

/**
 * This renders the user request registration confirmation message to the user
 * 
 * @author bogdanV
 */

/* @var $model \app\models\requests\Request\ */

?>

<h1>Richiesta di registrazione per l'utente</h1>
<p>Questo è un messaggio automatico . Si prega di non rispondere .</p>
<div>
    <p>La richiesta è stata presentata e verrà processato a breve.</p>
    <p></p>
    <p>Sarete informati sullo stato della vostra richiesta . Si prega di controllare la posta elettronica ( compresa la cartella spam) regolarmente .</p>
    <p></p>
    <p>Grazie.</p>
</div>
