<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use \app\models\requests\Request;
/**
 * This renders the user request registration confirmation message to the user
 * 
 * @author bogdanV
 */

/* @var $model \app\models\requests\Request */

?>

<h1>Richiesta di registrazione per l'utente - riaperto</h1>
<p>Questo è un messaggio automatico . Si prega di non rispondere .</p>

<div>
    <p>La tua richiesta è stata riaperta.</p>
    <p></p>
    <p>
    <p>Si prega di controllare la posta elettronica per ulteriori sviluppi.</p>
    <p>Grazie.</p>

</div>
