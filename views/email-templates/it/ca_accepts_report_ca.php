<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

/**
 * This renders the notification message to the CA administrators when an incident report is ACCEPTED.
 * 
 * @author bogdanV
 */

/* @var $model app\models\ca\IncidentCategorization */
/* @var $details mixed array */

//use app\models\forms\CaNewUserRequestForm;

if ($model->is_major == 1) {
    $back_color = '#f5ad66';
    $ma_text = 'INCIDENTE GRAVE';
} else {
    $back_color = '#9D9';
    $ma_text = 'INCIDENTE';
}

?>

<h1>Relazione dell'Incidente <span style="color: #00ff00">ACCETTATA</span></h1>
<p>Questo è un messaggio automatico. Si prega di non rispondere.</p>
<div>
    <p>
        La Relazione dell'Incidente <b><?= $model->name ?></b> è stata ACCETTATA.
    </p>
    <p></p>
    <div style='padding:12px; background-color: <?= $back_color ?>'>
        L'evento è stato classificato come <?= $ma_text ?>
    </div>
        
    <table class="def">
            <tr>
                <td>Nome del valutatore:</td>
                <td><?= $details['assessor_name'] ?></td>
            </tr>
            <tr>
                <td>Data:</td>
                <td><?= $details['date'] ?></td>
            </tr>
    </table>
    
    <p>
    </p>
    
    <h2>Dettagli dell'incidente: </h2>
    
    <table class="def">
            <tr>
                <td>Data/Ora dell'evento:</td>
                <td><?= date_format(new DateTime($model->event->event_date_time), 'd M Y H:i:s') ?></td>
            </tr>
            <tr>
                <td>Operatore/proprietario:</td>
                <td><?= $model->event->organization->organization_name ?><?= ' (' . $model->event->organization->organization_acronym . ')' ?></td>
            </tr>
            <tr>
                <td>Impianto:</td>
                <td><?= $details['installation'] ?></td>
            </tr>
            <tr>
                <td>Nome/codice di campo:</td>
                <td><?= $model->event->field_name ?></td>
            </tr>
            <tr>
                <td colspan="2" style="padding-top: 1.3em"><b>Categorizzazione dell'evento:</b></td>
            </tr>

            <?php 
            foreach($details['event_types'] as $event_type_string) {
            ?>
            <tr>
                <td colspan="2">
                    <?= $event_type_string ?>
                </td>
            </tr>
            <?php
            }
            ?>
            
            
            <tr>
                <td colspan="2" style="padding-top: 1.3em"><b>Comunicato da: </b></td>
            </tr>
            <tr>
                <td>Nome della persona che comunica l'evento: </td>
                <td><?= $model->event->raporteur_name ?></td>
            </tr>
            <tr>
                <td>Functione della persona che comunica l'evento: </td>
                <td><?= $model->event->raporteur_role ?></td>
            </tr>


            <tr>
                <td colspan="2" style="padding-top: 1.3em"><b>Recapiti:</b></td>
            </tr>
            <tr>
                <td>Numero di telefono:</td>
                <td><?= $model->event->raporteur->phone ?></td>
            </tr>
            <tr>
                <td>Indirizzo e-mail: </td>
                <td><?= $model->event->raporteur->email ?></td>
            </tr>
    </table>
    
    <p>=====================================</p>
    <p></p>
</div>
