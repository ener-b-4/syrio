<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

/**
 * This renders the message to the CA administrators for registering a new user in their Organization
 * 
 * @author bogdanV
 */

/* @var $model app\models\events\drafts\EventDraft */
/* @var $details mixed array */

//use app\models\forms\CaNewUserRequestForm;

?>

<h1>Incidente ripresentato</h1>
<p>Questo è un messaggio automatico . Si prega di non rispondere .</p>
<div>
    <p>L'incidente <?= $details['title'] ?> è stato rinviato.</p>
    <p></p>
    <h2>Stato della relazione:</h2>
    <p>
        <b>Data di invio: </b>
        <?= ' ' . $details['date_of_submission'] ?>
        <br/>
        <b>Scadenza prevista: </b>
        <?= ' ' . $details['deadline'] ?>
        <i>(del primo invio)</i>
        <br/>
        <b>Intervallo della scadenza: </b>
        <?= ' ' . $details['deadline_interval'] ?>
        <i>(del primo invio)</i>
        <br/>
        <b>Stato della relazione: </b>
        <?= ' ' . $details['late'] ?>
        <?= ' (' . $details['to_deadline'] . ')'?>
        <i>(del primo invio della scadenza)</i>
    </p>
    
    <h2>Giustificazione per il rifiuto: </h2>
    <p>
        <i>
            Il rapporto è stato precedentemente respinto con la seguente motivazione:
        </i>
    </p>
    <p style="color: blue">
        <?= nl2br(\yii\helpers\Html::encode($details['justification'])) ?>
    </p>
    
    <h2>Dettagli dell'incidente:  </h2>
    
    <table class="def">
            <tr>
                <td>Data/ora evento:</td>
                <td><?= date_format(new DateTime($model->event->event_date_time), 'd M Y H:i:s') ?></td>
            </tr>
            <tr>
                <td>Operatore/proprietario:</td>
                <td><?= $model->event->organization->organization_name ?><?= ' (' . $model->event->organization->organization_acronym . ')' ?></td>
            </tr>
            <tr>
                <td>Nome dell'installazione:</td>
                <td><?= $details['installation'] ?></td>
            </tr>
            <tr>
                <td>Nome/codice di campo :</td>
                <td><?= $model->event->field_name ?></td>
            </tr>
            <tr>
                <td colspan="2" style="padding-top: 1.3em"><b>Categorizzazione dell'evento:</b></td>
            </tr>

            <?php 
            foreach($details['event_types'] as $event_type_string) {
            ?>
            <tr>
                <td colspan="2">
                    <?= $event_type_string ?>
                </td>
            </tr>
            <?php
            }
            ?>
            
            
            <tr>
                <td colspan="2" style="padding-top: 1.3em"><b>Communicato da:</b></td>
            </tr>
            <tr>
                <td>Nome della persona che comunica l'evento: </td>
                <td><?= $model->event->raporteur_name ?></td>
            </tr>
            <tr>
                <td>Funzione della persona che comunica l’evento: </td>
                <td><?= $model->event->raporteur_role ?></td>
            </tr>


            <tr>
                <td colspan="2" style="padding-top: 1.3em"><b>Recapiti:</b></td>
            </tr>
            <tr>
                <td>Numero di telefono:</td>
                <td><?= $model->event->raporteur->phone ?></td>
            </tr>
            <tr>
                <td>Indirizzo e-mail: </td>
                <td><?= $model->event->raporteur->email ?></td>
            </tr>
    </table>

    <p>===========================</p>
    <p></p>
</div>
