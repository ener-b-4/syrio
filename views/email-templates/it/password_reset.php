<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

/**
 * This renders the message for password reset request
 * 
 * @author bogdanV
 */

/* @var $url string the url to reset password*/

?>

<h1><?= Yii::t('app', 'Password reset request') ?></h1>
<p><?= Yii::t('app', 'This is an automatic message. Please do not reply.') ?></p>
<div>
    <p>Reimpostazione della password è stata richiesta.</p>
    <p></p>
    <p>Si prega di utilizzare <a href='<?=$url?>'>questo link</a> e seguire le istruzioni.</p>
	
    <p></p>
    <p>NOTA: il link è valido per 24 ore dalla ricezione di questo messaggio.</p>

    <p>===========================</p>
    <p></p>
</div>
