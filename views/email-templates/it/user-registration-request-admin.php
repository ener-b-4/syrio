<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;

/**
 * This renders the user request registration confirmation message to the user
 * 
 * @author bogdanV
 */

/* @var $model \app\models\requests\Request\ */
$reqUrl = Yii::$app->getUrlManager()->createAbsoluteUrl(['requests/request/view', 'id'=>$model->id]);

?>

<h1>Richiesta di registrazione per l'utente</h1>
<p>Questo è un messaggio automatico . Si prega di non rispondere .</p>
<div>
    <p>
        Hai una richiesta d'attesa . Si può vedere la richiesta <?= Html::a('here', $reqUrl) ?>
    </p>
    <p></p>
    <p>'Si prega di notare che è necessario effettuare l'accesso per visualizzare la richiesta.'</p>
</div>
