<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

/**
 * This renders the notification message to the OO rapporteur when an incident report is REJECTED.
 * 
 * @author bogdanV
 */

/* @var $model app\models\ca\IncidentCategorization */
/* @var $details mixed array */

//use app\models\forms\CaNewUserRequestForm;

?>

<h1>Relazione dell'Incidente <span style="color: #FF000A">RIFIUTATA</span></h1>
<p>Questo è un messaggio automatico. Si prega di non rispondere.</p>
<div>
    <p>
        La tua relazione per l'incidente <b><?= $details['name'] ?></b> è stata RIFIUTATA da parte dell'autorità competente (<?= strtoupper(\Yii::$app->params['competentAuthorityIso2']) ?>) con la seguente motivazione:
    </p>
    <p></p>
    <p>
        <span style ="color: #007fff">
            <i>
                <?= nl2br($model->rejection_desc) ?>
            </i>
        </span>
    </p>
        
    <table class="def">
            <tr>
                <td>Data:</td>
                <td><?= $details['date'] ?></td>
            </tr>
    </table>
    <p>=====================================</p>
    <p></p>
</div>
