<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

/**
 * This renders the message for password reset request
 * 
 * @author bogdanV
 */

/* @var $url string the url to reset password*/

?>

<h1><?= Yii::t('app', 'Password reset request') ?></h1>
<p><?= Yii::t('app', 'This is an automatic message. Please do not reply.') ?></p>
<div>
    <p>A password reset has been requested.</p>
    <p></p>
    <p>Please use <a href='<?=$url?>'>this link</a> and follow the instructions.</p>
    <p></p>
    <p>NOTE: the link is valid for 24 hours from receiving this message.</p>

    <p>===========================</p>
    <p></p>
</div>
