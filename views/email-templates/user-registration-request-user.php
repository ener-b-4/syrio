<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

/**
 * This renders the user request registration confirmation message to the user
 * 
 * @author bogdanV
 */

/* @var $model \app\models\requests\Request\ */

?>

<h1>User registration request</h1>
<p>This is an automatic message. Please do not reply.</p>
<div>
    <p>Your request has been submitted and will be processed shortly.</p>
    <p></p>
    <p>You will be informed about the status of your request. Please check your email (including spam folder) regularly.</p>
    <p></p>
    <p>Thank you.</p>
</div>
