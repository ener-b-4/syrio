<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use \app\models\requests\Request;
/**
 * This renders the user request registration confirmation message to the user
 * 
 * @author bogdanV
 */

/* @var $model \app\models\requests\Request */

?>

<h1>User request - re-opened</h1>
<p>This is an automatic message. Please do not reply.</p>
<div>
    <p>Your request #<?=$model->id?> has been re-opened.</p>
    <p></p>
    
    <p>Please check your email (including spam folder) regularly for further notifications.</p>
    <p></p>
    <p>Thank you.</p>

</div>
