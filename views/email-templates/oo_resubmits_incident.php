<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

/**
 * This renders the message to the CA administrators for registering a new user in their Organization
 * 
 * @author bogdanV
 */

/* @var $model app\models\events\drafts\EventDraft */
/* @var $details mixed array */

//use app\models\forms\CaNewUserRequestForm;

?>

<h1>Offshore incident resubmitted</h1>
<p>This is an automatic message. Please do not reply.</p>
<div>
    <p>Incident <?= $details['title'] ?> has been resubmitted.</p>
    <p></p>
    <h2>Reporting status:</h2>
    <p>
        <b>Date of submission: </b>
        <?= ' ' . $details['date_of_submission'] ?>
        <br/>
        <b>Expected deadline: </b>
        <?= ' ' . $details['deadline'] ?>
        <i>(of the first submission)</i>
        <br/>
        <b>Deadline interval: </b>
        <?= ' ' . $details['deadline_interval'] ?>
        <i>(of the first submission)</i>
        <br/>
        <b>Reporting status: </b>
        <?= ' ' . $details['late'] ?>
        <?= ' (' . $details['to_deadline'] . ')'?>
        <i>(from the first submission deadline)</i>
    </p>
    
    <h2>Rejection justification: </h2>
    <p>
        <i>
            The report has been previously rejected with the following justification:
        </i>
    </p>
    <p style="color: blue">
        <?= nl2br(\yii\helpers\Html::encode($details['justification'])) ?>
    </p>
    
    <h2>Incident details: </h2>
    
    <table class="def">
            <tr>
                <td>Event date/time:</td>
                <td><?= date_format(new DateTime($model->event->event_date_time), 'd M Y H:i:s') ?></td>
            </tr>
            <tr>
                <td>Operator/owner:</td>
                <td><?= $model->event->organization->organization_name ?><?= ' (' . $model->event->organization->organization_acronym . ')' ?></td>
            </tr>
            <tr>
                <td>Installation:</td>
                <td><?= $details['installation'] ?></td>
            </tr>
            <tr>
                <td>Field name/code:</td>
                <td><?= $model->event->field_name ?></td>
            </tr>
            <tr>
                <td colspan="2" style="padding-top: 1.3em"><b>Event categorization:</b></td>
            </tr>

            <?php 
            foreach($details['event_types'] as $event_type_string) {
            ?>
            <tr>
                <td colspan="2">
                    <?= $event_type_string ?>
                </td>
            </tr>
            <?php
            }
            ?>
            
            
            <tr>
                <td colspan="2" style="padding-top: 1.3em"><b>Reported by: </b></td>
            </tr>
            <tr>
                <td>Name of the reporting person: </td>
                <td><?= $model->event->raporteur_name ?></td>
            </tr>
            <tr>
                <td>Role of the reporting person: </td>
                <td><?= $model->event->raporteur_role ?></td>
            </tr>


            <tr>
                <td colspan="2" style="padding-top: 1.3em"><b>Contact details:</b></td>
            </tr>
            <tr>
                <td>Telephone number:</td>
                <td><?= $model->event->raporteur->phone ?></td>
            </tr>
            <tr>
                <td>E-mail address: </td>
                <td><?= $model->event->raporteur->email ?></td>
            </tr>
    </table>

    <p>===========================</p>
    <p></p>
</div>
