<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

/**
 * This renders the message to the CA administrators for registering a new user in their Organization
 * 
 * @author bogdanV
 */

/* @var $model app\admin\users\User */
/* @var $tempPassword string the temporary password */

//use app\models\forms\CaNewUserRequestForm;

$reqUrl = Yii::$app->getUrlManager()->createAbsoluteUrl('site/login');

?>

<h1>User registration request</h1>
<p>This is an automatic message. Please do not reply.</p>
<div>
    <p>Your Competent Authority user account has been created.</p>
    <p></p>
    <p>Please <a href='<?=$reqUrl?>'>login</a> to SyRIO with the following credentials:</p>
    <p></p>
    <table>
            <tr>
                <td>username: </td>
                <td><?= $model->username ?></td>
            </tr>
            <tr>
                <td>password: </td>
                <td><?= $tempPassword ?></td>
            </tr>
    </table>

    <p>===========================</p>
    <p></p>
</div>
