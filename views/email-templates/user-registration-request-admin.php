<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;

/**
 * This renders the user request registration confirmation message to the user
 * 
 * @author bogdanV
 */

/* @var $model \app\models\requests\Request\ */
$reqUrl = Yii::$app->getUrlManager()->createAbsoluteUrl(['requests/request/view', 'id'=>$model->id]);

?>

<h1>User registration request</h1>
<p>This is an automatic message. Please do not reply.</p>
<div>
    <p>
        You have a request waiting. You may see the request <?= Html::a('here', $reqUrl) ?>
    </p>
    <p></p>
    <p>'Please note that you must be logged in to view the request.'</p>
</div>
