<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

/**
 * This renders the notification message to the OO rapporteur when an incident report is REJECTED.
 * 
 * @author bogdanV
 */

/* @var $model app\models\ca\IncidentCategorization */
/* @var $details mixed array */

//use app\models\forms\CaNewUserRequestForm;

?>

<h1>Incident report <span style="color: #00ff00">ACCEPTED</span></h1>
<p>This is an automatic message. Please do not reply.</p>
<div>
    <p>
        Your report for incident <b><?= $details['name'] ?></b> has been ACCEPTED by the competent authority 
        (<?= strtoupper(\Yii::$app->params['competentAuthorityIso2']) ?>).
    </p>
    <p></p>
    <p>
        All your reporting obligations (acc. to Article 23 of Directive 2013/30/EU) have been met.
    </p>
        
    <table class="def">
            <tr>
                <td>Date:</td>
                <td><?= $details['date'] ?></td>
            </tr>
    </table>
    <p>=====================================</p>
    <p></p>
</div>
