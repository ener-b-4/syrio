<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

/**
 * This renders the notification message to the OO rapporteur when an incident report is REJECTED.
 * 
 * @author bogdanV
 */

/* @var $model app\models\ca\IncidentCategorization */
/* @var $details mixed array */

//use app\models\forms\CaNewUserRequestForm;

?>

<h1>Raport de incident <span style="color: #FF000A">RESPINS</span></h1>
<p>Acesta este un mesaj generat automat. Vă rugăm nu trimiteți răspuns.</p>
<div>
    <p>
        Raportul de incident <b><?= $details['name'] ?></b> a fost RESPINS de către autoritatea competentă 
        (<?= strtoupper(\Yii::$app->params['competentAuthorityIso2']) ?>) cu următoarea justificare:
    </p>
    <p></p>
    <p>
        <span style ="color: #007fff">
            <i>
                <?= nl2br($model->rejection_desc) ?>
            </i>
        </span>
    </p>
        
    <table class="def">
            <tr>
                <td>Data și ora:</td>
                <td><?= $details['date'] ?></td>
            </tr>
    </table>
    <p>=====================================</p>
    <p></p>
</div>
