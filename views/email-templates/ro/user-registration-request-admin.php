<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;

/**
 * This renders the user request registration confirmation message to the user
 * 
 * @author bogdanV
 */

/* @var $model \app\models\requests\Request\ */
$reqUrl = Yii::$app->getUrlManager()->createAbsoluteUrl(['requests/request/view', 'id'=>$model->id]);
?>

<h1>Solicitare înregistrare utilizator</h1>
<p>Acesta este un mesaj generat automat. Vă rugăm nu trimiteți răspuns.</p>
<div>
    <p>
        O nouă cerere a fost înregistrată.
        Pentru a vizualiza cererea apăsați <?= Html::a('aici', $reqUrl) ?>.
    </p>
    <p></p>
    <p>Vizualizarea cererii necesită să fiți autentificat.</p>
</div>
