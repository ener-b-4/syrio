<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use \app\models\requests\Request;
/**
 * This renders the user request registration confirmation message to the user
 * 
 * @author bogdanV
 */

/* @var $model \app\models\requests\Request */

switch ($model->status)
{
	case Request::STATUS_ACCEPTED_REJECTED:
		$status_text = 'RESPINSĂ, cu următoarea justificare:';
		break;
	case Request::STATUS_ACCEPTED_CLOSED:
		$status_text = 'ACCEPTATĂ';
		break;
}

if (isset($model->justification) && trim($model->justification) !=='')
{
	$justification = $model->justification;
}
else
{
	$justification = '(nici un al mesaj)';
}
?>

<h1>Solicitare înregistrare utilizator - procesată</h1>
<p>Acesta este un mesaj generat automat. Vă rugăm nu trimiteți răspuns.</p>
<div>
	<p>Solicitarea dumneavoastră a fost <?= $status_text ?></p>
	<p></p>
        <p><em><?= nl2br($justification) ?></em></p>
</div>
