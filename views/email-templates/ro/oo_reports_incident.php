<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

/**
 * This renders the message to the CA administrators for registering a new user in their Organization
 * 
 * @author bogdanV
 */

/* @var $model app\models\events\drafts\EventDraft */
/* @var $details mixed array */

//use app\models\forms\CaNewUserRequestForm;

?>

<h1>Incident offshore nou raportat</h1>
<p>Acesta este un mesaj generat automat. Vă rugăm nu trimiteți răspuns.</p>
<div>
    <p>Un nou incident a fost raportat.</p>
    <p></p>
    <h2>Status raportare:</h2>
    <p>
        <b>Data raportării: </b>
        <?= ' ' . $details['date_of_submission'] ?>
        <br/>
        <b>Termen limită: </b>
        <?= ' ' . $details['deadline'] ?>
        <br/>
        <b>Interval termen limită: </b>
        <?= ' ' . $details['deadline_interval'] ?>
        <br/>
        <b>Status raportare: </b>
        <?= ' ' . $details['late'] ?>
        <?= ' (' . $details['to_deadline'] . ')'?>
    </p>
    
    <h2>Detalii incident: </h2>
    
    <table class="def">
            <tr>
                <td>Data și ora evenimentului:</td>
                <td><?= date_format(new DateTime($model->event->event_date_time), 'd M Y H:i:s') ?></td>
            </tr>
            <tr>
                <td>Operator/proprietar:</td>
                <td><?= $model->event->organization->organization_name ?><?= ' (' . $model->event->organization->organization_acronym . ')' ?></td>
            </tr>
            <tr>
                <td>Nume instalație:</td>
                <td><?= $details['installation'] ?></td>
            </tr>
            <tr>
                <td>Denumire câmp/cod:</td>
                <td><?= $model->event->field_name ?></td>
            </tr>
            <tr>
                <td colspan="2" style="padding-top: 1.3em"><b>Clasificare eveniment:</b></td>
            </tr>

            <?php 
            foreach($details['event_types'] as $event_type_string) {
            ?>
            <tr>
                <td colspan="2">
                    <?= $event_type_string ?>
                </td>
            </tr>
            <?php
            }
            ?>
            
            
            <tr>
                <td colspan="2" style="padding-top: 1.3em"><b>Raportat de: </b></td>
            </tr>
            <tr>
                <td>Numele persoanei care raportează: </td>
                <td><?= $model->event->raporteur_name ?></td>
            </tr>
            <tr>
                <td>Funcția persoanei care raportează: </td>
                <td><?= $model->event->raporteur_role ?></td>
            </tr>


            <tr>
                <td colspan="2" style="padding-top: 1.3em"><b>Date de contact:</b></td>
            </tr>
            <tr>
                <td>Număr de telefon:</td>
                <td><?= $model->event->raporteur->phone ?></td>
            </tr>
            <tr>
                <td>Adresa de e-mail: </td>
                <td><?= $model->event->raporteur->email ?></td>
            </tr>
    </table>

    <p>===========================</p>
    <p></p>
</div>
