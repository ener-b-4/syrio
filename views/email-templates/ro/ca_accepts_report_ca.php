<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

/**
 * This renders the notification message to the CA administrators when an incident report is ACCEPTED.
 * 
 * @author bogdanV
 */

/* @var $model app\models\ca\IncidentCategorization */
/* @var $details mixed array */

//use app\models\forms\CaNewUserRequestForm;

if ($model->is_major == 1) {
    $back_color = '#f5ad66';
    $ma_text = 'ACCIDENT MAJOR';
} else {
    $back_color = '#9D9';
    $ma_text = 'INCIDENT';
}

?>

<h1>Raport de incident <span style="color: #00ff00">ACCEPTAT</span></h1>
<p>Acesta este un mesaj generat automat. Vă rugăm nu trimiteți răspuns.</p>
<div>
    <p>
        Raportul de incident <b><?= $model->name ?></b> a fost ACCEPTAT.
    </p>
    <p></p>
    <div style='padding:12px; background-color: <?= $back_color ?>'>
        Evenimentul a fost clasificat ca <?= $ma_text ?>
    </div>
        
    <table class="def">
            <tr>
                <td>Nume evaluator:</td>
                <td><?= $details['assessor_name'] ?></td>
            </tr>
            <tr>
                <td>Data și ora:</td>
                <td><?= $details['date'] ?></td>
            </tr>
    </table>
    
    <p>
    </p>
    
    <h2>Detalii incident: </h2>
    
    <table class="def">
            <tr>
                <td>Data și ora evenimentului:</td>
                <td><?= date_format(new DateTime($model->event->event_date_time), 'd M Y H:i:s') ?></td>
            </tr>
            <tr>
                <td>Operator/proprietar:</td>
                <td><?= $model->event->organization->organization_name ?><?= ' (' . $model->event->organization->organization_acronym . ')' ?></td>
            </tr>
            <tr>
                <td>Nume instalație:</td>
                <td><?= $details['installation'] ?></td>
            </tr>
            <tr>
                <td>Denumire câmp/cod:</td>
                <td><?= $model->event->field_name ?></td>
            </tr>
            <tr>
                <td colspan="2" style="padding-top: 1.3em"><b>Clasificare eveniment:</b></td>
            </tr>

            <?php 
            foreach($details['event_types'] as $event_type_string) {
            ?>
            <tr>
                <td colspan="2">
                    <?= $event_type_string ?>
                </td>
            </tr>
            <?php
            }
            ?>
            
            
            <tr>
                <td colspan="2" style="padding-top: 1.3em"><b>Raportat de: </b></td>
            </tr>
            <tr>
                <td>Numele persoanei care raportează: </td>
                <td><?= $model->event->raporteur_name ?></td>
            </tr>
            <tr>
                <td>Funcția persoanei care raportează: </td>
                <td><?= $model->event->raporteur_role ?></td>
            </tr>


            <tr>
                <td colspan="2" style="padding-top: 1.3em"><b>Date de contact:</b></td>
            </tr>
            <tr>
                <td>Număr de telefon:</td>
                <td><?= $model->event->raporteur->phone ?></td>
            </tr>
            <tr>
                <td>Adresa de e-mail: </td>
                <td><?= $model->event->raporteur->email ?></td>
            </tr>
    </table>
    
    <p>=====================================</p>
    <p></p>
</div>
