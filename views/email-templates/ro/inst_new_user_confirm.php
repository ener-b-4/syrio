<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

/**
 * This renders the user registration confirmation message to the newly created user
 * 
 * @author bogdanV
 */

/* @var $model app\admin\users\User */
/* @var $tempPassword string the temporary password */

//use app\models\forms\CaNewUserRequestForm;

$reqUrl = Yii::$app->getUrlManager()->createAbsoluteUrl('site/login');

?>

<h1>Solicitare înregistrare utilizator</h1>
<p>Acesta este un mesaj generat automat. Vă rugăm nu trimiteți răspuns.</p>
<div>
    <p>Contul dumneavoastră de tip utilizator Instalație a fost creat.</p>
    <p></p>
    <p>Vă rugăm să vă <a href='<?=$reqUrl?>'>autentificați</a> utilizând datele următoare:</p>
    <p></p>
    <table>
            <tr>
                <td>nume utilizator: </td>
                <td><?= $model->username ?></td>
            </tr>
            <tr>
                <td>parola: </td>
                <td><?= $tempPassword ?></td>
            </tr>
    </table>

    <p>===========================</p>
    <p></p>
</div>
