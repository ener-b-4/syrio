<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

/**
 * This renders the user request registration confirmation message to the user
 * 
 * @author bogdanV
 */

/* @var $model \app\models\requests\Request\ */

?>

<h1>Solicitare înregistrare utilizator</h1>
<p>Acesta este un mesaj generat automat. Vă rugăm nu trimiteți răspuns.</p>
<div>
    
    <p>Cererea dumneavoastră a fost transmisă și va fi procesată în cel mai scurt timp.</p>
    <p></p>
    <p>Veți fi informat despre stadiul procesării cererii dumneavoastră. Vă rugăm să vă verificați poșta electronică (inclusiv directorul spam) în mod frecvent.</p>
    <p></p>
    <p>Mulțumim.</p>
</div>
