<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;

use app\components\helpers\DataGrid\CustomGridView;
use app\components\helpers\DataGrid\funDataGridClass;

use app\models\events\EventDeclaration;
use app\widgets\ActionMessageWidget;
use app\assets\FontAwesomeAsset;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\rebuilt\EventDraftSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $evtID integer the ID of the event */

FontAwesomeAsset::register($this);
//$this->registerAssetBundle(FontAwesomeAsset::className());

$_event = EventDeclaration::findOne(['id' => $evtID]);

$breadcrumbs = [
    [
        'label' => Yii::t('app', 'Registered events'),
        'url' => Url::to(['/events/event-declaration/index']),
        //'template' =>"<li>{link}</li>
    ],
    [
        'label' => $_event->event_name,
        'url' => Url::to(['/events/event-declaration/view', 'id'=>$_event->id]),
        //'template' =>"<li>{link}</li>
    ],
    [
        'label' => Yii::t('app', 'Reporting'),
        'url' => Url::to(['/events/drafts/event-draft/index', 'evtID'=>$_event->id]),
    ],
    [
        'label' => Yii::t('app', 'Trash'),
        //'template' =>"<li>{link}</li>
    ],
];

$this->params['breadcrumbs'] = $breadcrumbs;
$this->title = Yii::t('app', 'Deleted drafts');
//$this->params['breadcrumbs'][] = [];

$pathPDF = 'events/drafts/event-draft/view-pdf';

$heading = Yii::t('app', 'Deleted drafts');

//var_dump(get_included_files());
//if ( ! class_exists('CustomGridView')) die('There is no hope!'  ); 

?>


<div class="event-draft-index">

    <?php
        //check if flash is set
        if (Yii::$app->session->hasFlash('msg'))
        {
            $msg = Yii::$app->session->getFlash('msg');
            if (strpos($msg,'[err]'))
            {
                //failure
                //get rid of the [s]
                $msg =  str_replace('[err]', '', $msg);
                $msg =  str_replace('\n', '<br/>', $msg);
                
                echo "<div class='alert alert-danger'>$msg</div>";
            }
            else
            {
                //success
                $msg =  str_replace('\n', '<br/>', $msg);
                echo "<div class='alert alert-success'>$msg</div>";
            }
        }
    ?>
    
    <?php
    /* use this on any view to include the actionmessage logic and widget */
    //include Yii::getAlias('@app') . '\views\shared_parts\message_snippet.php';
	include Yii::getAlias('@app') . '/views/shared_parts/message_snippet.php';
    ?>

    <h1><?= Html::encode($this->title) . ': ' . $_event->event_name ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php
      
    //(start) Author: cavesje   Date: 14.08.2015  - Header and Footer options for PDF format
    //TODO CONSTANT VARIABLE (MAYBE $title_h_l and $title_h_r)
    $title_h_l =  'SyRIO ' . \Yii::t('app', 'Export'); //'Custom L';
    $title_h_c =  $this->title; //'Custom C';
    $title_h_r = 'Generated' . ': ' . date("D, d-M-Y g:i a T");
    $ourPdfHeader = funDataGridClass::getOurPdfHeader($title_h_l, $title_h_c, $title_h_r);

    $title_f = '';
    $ourPdfFooter = funDataGridClass::getOurPdfFooter($title_f);
    //(end) Author: cavesje   Date: 14.08.2015  - Header and Footer options for PDF format
    
    //(start) Author: cavesje   Date: 14.08.2015  - Merge exportConfig of kartik\grid\GridView with CustomDataGrid
    $exportConfig = funDataGridClass::getExportConfiguration($ourPdfHeader, $ourPdfFooter);
    //(end) Author: cavesje   Date: 14.08.2015  - Merge exportConfig of kartik\grid\GridView with CustomDataGrid

    
    $trashUrl = Yii::$app->urlManager->createAbsoluteUrl(['events/drafts/event-draft/trash', 'evtID'=>$_event->id]);

    //GridView::widget([
    echo CustomGridView::widget([
      'dataProvider' => $dataProvider,
      'filterModel' => $searchModel,
      'pjax' => false,
      'rowOptions' => function($model) {
        return $model->del_status == app\models\events\EventDeclaration::STATUS_DELETED ? ['class' => 'danger'] : [];
        /* asta cade - pun coloana separata */
        if ($model->ca_status == EventDeclaration::INCIDENT_REPORT_REJECTED) {
          return ['class' => 'danger'];
        }
        else if ($model->ca_status == EventDeclaration::INCIDENT_REPORT_ACCEPTED) {
          return ['class' => 'success'];
        }
      },
      'columns' => [
        //['class' => 'kartik\grid\SerialColumn'],
        //Column : Status of Report
        [
          'class' => 'kartik\grid\DataColumn',
            'hiddenFromExport' => true,
          'attribute' => 'status',
          'label' => '',
          'width' => '43px',
          'vAlign' => 'middle',
          'hAlign' => 'center',
          'filterType' => null,
          'content' => function($model, $key, $index, $column) {
            //$model->refreshFinalizeStatus();
            if ($model->status == EventDeclaration::INCIDENT_REPORT_FINALIZED_PARTIALLY) {
              return '<i class="fa fa-star-half-o fa-lg" title="' . \Yii::t('app', 'PARTIALLY FINALIZED') . '"></i>';
            }
            else if ($model->status == EventDeclaration::INCIDENT_REPORT_FINALIZED) {
              return '<i class="fa fa-star fa-lg" title="'. \Yii::t('app', 'FINALIZED. READY TO BE SIGNED') . '"></i>';
            }
            else if ($model->status == EventDeclaration::INCIDENT_REPORT_SIGNED) {
              return '<i class="fa fa-check-square-o fa-lg" title="' . \Yii::t('app', 'SIGNED. Can be submitted.') . '"></i>';
            }
            else {
              return '<i class="fa fa-star-o fa-lg" title="' . ucfirst(\Yii::t('app', 'draft')) . '"></i>';
            }
          },
        ],
    //Column : Status of Report for Exporting
    [
      'class' => 'kartik\grid\DataColumn',
      'attribute' => 'status',
      'label' => \Yii::t('app', 'Drafting status'),
      'vAlign' => 'middle',
      'hAlign' => 'center',
      'hidden' => true,  
      'content' => function($model, $key, $index, $column) {
        //$model->refreshFinalizeStatus();
        if ($model->status == EventDeclaration::INCIDENT_REPORT_FINALIZED_PARTIALLY) {
          $s = \Yii::t('app', "PARTIALLY FINALIZED");
          return $s;
        }
        else if ($model->status == EventDeclaration::INCIDENT_REPORT_FINALIZED) {
          $s = \Yii::t('app', "FINALIZED. READY TO BE SIGNED");
          return $s;
        }
        else if ($model->status == EventDeclaration::INCIDENT_REPORT_SIGNED) {
          $s = \Yii::t('app', "SIGNED. Can be submitted.");
          return $s;
        }
        else {
          $s = ucfirst(\Yii::t('app', "draft"));
          return $s;
        }
      },
    ],
                  
        //Column : SUBMITTED or NOT to CA
        [
          'class' => 'kartik\grid\DataColumn',
            'hiddenFromExport' => true,
          'attribute' => 'ca_status',
          'label' => '',
          'width' => '43px',
          'vAlign' => 'middle',
          'hAlign' => 'center',
          'content' => function($model, $key, $index, $column) {
            //$model->refreshFinalizeStatus();
            if ($model->ca_status == EventDeclaration::INCIDENT_REPORT_REPORTED) {
              $s = '<i class="fa fa-arrow-right fa-sm" title="' . \Yii::t('app', 'REPORTED TO THE CA. PENDING FOR ACCEPTANCE.') . '"></i>'
                  . '<i class="fa fa-building-o fa-sm" title="' . \Yii::t('app', 'REPORTED TO THE CA. PENDING FOR ACCEPTANCE.') . '"></i>';
              return $s;
            }
            else if ($model->ca_status == EventDeclaration::INCIDENT_REPORT_REJECTED) {
              return '<i class="fa fa-exclamation-triangle fa-lg sy_error" title="' . \Yii::t('app', 'REJECTED BY THE CA. CHECK THE REASON THEN RESUBMIT.') . '"></i>';
            }
            else if ($model->ca_status == EventDeclaration::INCIDENT_REPORT_ACCEPTED) {
              return '<i class="fa fa-check fa-lg warning" title="' . \Yii::t('app', 'ACCEPTED AND ASSESSED BY THE CA.') . '"></i>';
            }
            else {
              return '<i class="fa fa-ellipsis-h fa-lg" title="' . \Yii::t('app', 'NOT SUBMITTED.') . '"></i>';
            };
          },
        ],
                  
    //Column : SUBMITTED or NOT to CA (column for export only)
    [
      'class' => 'kartik\grid\DataColumn',
      'attribute' => 'ca_status',
      'label' => \Yii::t('app', 'Reporting status'),

      'hidden' => true,  

      'vAlign' => 'middle',
      'hAlign' => 'center',
      'content' => function($model, $key, $index, $column) {
        //$model->refreshFinalizeStatus();
        if ($model->ca_status == EventDeclaration::INCIDENT_REPORT_REPORTED) {
          $s = \Yii::t('app', "REPORTED TO THE CA. PENDING FOR ACCEPTANCE.");
          return $s;
        }
        else if ($model->ca_status == EventDeclaration::INCIDENT_REPORT_REJECTED) {
          $s = \Yii::t('app', "REJECTED BY THE CA. CHECK THE REASON THEN RESUBMIT.");
          return $s;
        }
        else if ($model->ca_status == EventDeclaration::INCIDENT_REPORT_ACCEPTED) {
          $s = \Yii::t('app', "ACCEPTED AND ASSESSED BY THE CA.");
          return $s;
        }
        else {
          $s = \Yii::t('app', "NOT SUBMITTED.");
          return $s;
        };
      },
    ],
                  
        //Column : function to expand row
        [
          'class' => 'kartik\grid\ExpandRowColumn',
          'width' => '36px;',
            'hiddenFromExport' => true,
          'value' => function($model, $key, $index, $column) {
            //return GridView::ROW_COLLAPSED;
            return CustomGridView::ROW_COLLAPSED;
          },
          'detail' => function($model, $key, $index, $column) {
            return Yii::$app->controller->renderPartial('_draft-details', ['model' => $model]);
          }
        ],
        //Column : Created AT
        [
          'class' => 'kartik\grid\DataColumn',
          'attribute' => 'created_at',
          'vAlign' => 'middle',
          'width' => '155px',
        'content' => function($model, $key, $index, $column) {
            return \date('Y-m-d H:i:s', $model->created_at);
        }
            
        ],
        //Column : DESCRIPTION
        //start of event types column and description
        [
          'class' => 'kartik\grid\DataColumn',
          'attribute' => 'session_desc',
          'vAlign' => 'middle',
        ],
                
        [
          'class' => 'kartik\grid\DataColumn',
            'hiddenFromExport' => true,
          'attribute' => \Yii::t('app', 'Classification'),
          'vAlign' => 'middle',
          'content' => function($model, $key, $index, $column) {
            $s = '';
            //http://localhost:8080/SyRIO_DEV_post_brussels/web/index.php/events/drafts/event-draft/view?id=40
            foreach ($model->eventTypes as $key => $type) {
              $view_section = 'view-section-' . strtolower($type);

              //(start) Author : cavesje  Date : 20150724 - Open the view of entire SECTION
              //$root = Yii::$app->getUrlManager()->createAbsoluteUrl(['/events/drafts/event-draft/' . $view_section, 'id' => $model->id, 'v' => 1]);
              $root = Yii::$app->getUrlManager()->createAbsoluteUrl(['/events/drafts/event-draft/' . $view_section, 'id' => $model->id, 'v' => 0]);
              //(end) Author : cavesje  Date : 20150724 - Open the view of entire SECTION

              //set the background
              $the_section = 'section' . strtoupper($type);
              $appended = '';
              $appended_text = '';

              if ($type == 'G' || $type == 'H') {
                $appended = '<span class="glyphicon glyphicon-ok" style="padding-left:3px;"></span>';
                $appended_text = Yii::t('app', 'Finalized and Signed by default.');
                $s .= '<span class="sy_event-span">'
                    //(start) Author : cavesje  Date : 20150728 - Link page of details 
                    //. '<a href = "#" title = "' . $appended_text . '">'
                    . '<a href = "' . $root . '" title = "' . $appended_text . '">'
                    //(end) Author : cavesje  Date : 20150728 - Link page of details 
                    . $type
                    . $appended
                    . '</a>'
                    . '</span>';
              }
              else {
                switch ($model->$the_section->status) {
                  case EventDeclaration::INCIDENT_REPORT_LOCKED:
                    $appended = '<i class="fa fa-lock fa-lg" style="padding-left:3px;"></i>';
                    $appended_text = Yii::t('app', 'Locked. Someone is currently working on it.');
                    break;
                  case EventDeclaration::INCIDENT_REPORT_SIGNED:
                    $appended = '<span class="glyphicon glyphicon-check" style="padding-left:3px;"></span>';
                    $appended_text = ucfirst(Yii::t('app', 'signed'));
                    break;
                  case EventDeclaration::INCIDENT_REPORT_FINALIZED:
                    $appended = '<i class="fa fa-star-o fa-large" style="padding-left:3px;"></i>';
                    $appended_text = Yii::t('app', 'Finalized. Ready to be signed.');
                    break;
                }

                $s .= '<span class="sy_event-span">'
                    . '<a href = "' . $root . '" title = "' . $appended_text . '">'
                    . $type
                    . $appended
                    . '</a>'
                    . '</span>';
              }
            }
            return $s;
          }
        ], //end of event types column and description
        //Column : Event Type [A,B,C,D,E,F,G,H,I,J]
                
//start of event types column and description for export
    [
      'class' => 'kartik\grid\DataColumn',
      'attribute' => \Yii::t('app', 'Classification'),
      'hidden' => true, 
      'vAlign' => 'middle',
      'content' => function($model, $key, $index, $column) {
        $types = [];
        foreach(range('a','j') as $letter) {
            $s = '';
            $attr = 'is_'.$letter;
            $section = 'section' . strtoupper($letter);
            if ($model->$attr) {
                $s .= strtoupper($letter);
                if ($letter === 'g' || $letter === 'h') {
                    $s.= ' (' . Yii::t('app', 'Finalized and Signed by default.') . ')';
                } else {
                    if (isset($model->$section)) {
                        switch ($model->$section->status) {
                          case EventDeclaration::INCIDENT_REPORT_LOCKED:
                            $s.= ' (' . ucfirst(strtolower(Yii::t('app', 'LOCKED'))) . ')';
                            break;
                          case EventDeclaration::INCIDENT_REPORT_SIGNED:
                            $s.= ' (' . ucfirst(Yii::t('app', 'signed')) . ')';
                            break;
                          case EventDeclaration::INCIDENT_REPORT_FINALIZED:
                            $s.= ' (' . ucfirst(Yii::t('app', 'finalized')) . ')';
                            break;
                        }
                    }
                }
                $types[] = $s;
            }
        }   //end foreach
        $s = implode(', ', $types);
        return $s;
      }
    ],                
        [
          'class' => \kartik\grid\ActionColumn::className(),
            'hiddenFromExport' => true,
          'buttons' => [
            'purge' => function ($url, $model, $key) {
                    /* @var $model \app\models\events\drafts\EventDraft */
                    
                    //return $model->del_status;

                    if (!Yii::$app->user->can('incident_draft_purge', ['draft'=>$model]))
                    {
                        return '';
                    }
                    $msg = Yii::t('app', 'Are you sure you want to PERMANENTLY delete {data}?', [
                        'data'=>$model->session_desc]);
                    $msg .= ' ';
                    $msg .= Yii::t('app', 'Operation CAN NOT be undone.');
                    return $model->del_status == 0 ? 
                            Html::a('<i class="glyphicon glyphicon-remove"></i>', $url, [
                                'title' => Yii::t('app/commands', 'Purge'),
                                'data-confirm' => $msg,
                                'data-method' => 'POST']) : '';
                },
            'reactivate' => function ($url, $model, $key) {
                    /* @var $model \app\models\events\drafts\EventDraft */

                    if (!Yii::$app->user->can('incident_draft_delete', ['draft'=>$model]) || $model->del_status != \app\models\events\EventDeclaration::STATUS_DELETED)
                    {
                        return '';
                    }

                    return $model->del_status == 0 ? 
                            Html::a('<i class="glyphicon glyphicon-repeat"></i>', $url, [
                                'title' => Yii::t('app', 'Undo delete'), 
                                'data-method'=>'POST']) : '';
                },
            //(start) Author : cavesje  Date : 20150617 - Add new button for GeneratePDF
            //button-PDF                          
            'pdf' => function ($url, $model, $key) {
                $s = '<span class="glyphicon glyphicon-print text-success" style="padding-left:6px;"></span> <small>PDF</small>';

              return Html::a($s, $url, [
                        'class' => 'btn sy_padding_clear',
                      ]);
            }
            //(end) Author : cavesje  Date : 20150617 - Add new button for GeneratePDF
          ],
          'template' => '{view} {purge} {reactivate} {pdf}'
        ],
      ],
      //END COLUMNS
          
      // set your toolbar
      'toolbar' => [
        ['content' =>
          Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['trash', 'evtID'=>$evtID], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => Yii::t('app', 'Reset Grid')])
        ],
        '{export}',
        '{toggleData}',
      ],
                      
      // set export properties
      'export' => [
        'icon' => 'share-square-o',
        'encoding' => 'utf-8',
        'fontAwesome' => true,
        'target' => CustomGridView::TARGET_SELF,   //target: string, the target for submitting the export form, which will trigger the download of the exported file. 
        'showConfirmAlert' => TRUE,
        'header' => '<li role="presentation" class="dropdown-header">' . \Yii::t('app', 'Grid Export') . '</li>'
        //'header' => 'Export Page Data',
      ],
              
      //(start) Author: cavesje   Date: 14.08.2015  - extra configuration PDF export
      'exportConfig'     => $exportConfig,
      //(end) Author: cavesje   Date: 14.08.2015  - extra configuration PDF export
              
      'panel' => [
        //'type' => GridView::TYPE_PRIMARY,
        'type' => CustomGridView::TYPE_DANGER,
        'heading' => $heading,
        'before' => '',
      ],

      'persistResize' => false,
    ]);

  ?>

</div>
