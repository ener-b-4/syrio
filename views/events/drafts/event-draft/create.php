<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\rebuilt\EventDraft */

$this->title = Yii::t('app', 'Create {evt}', [
    'evt' => Yii::t('app/crf', 'report draft'),
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Events management'), 'url' => ['events/event-declaration/index']];
$this->params['breadcrumbs'][] = ['label' => $model->event->event_name, 'url' => ['events/event-declaration/display-drafts', 'id'=>$model->event->id]];
$this->params['breadcrumbs'][] = $this->title 


?>
<div class="event-draft-create">

    <h1><?= \Yii::t('app', '{evt1} for {evt2}', [
        'evt1' => $this->title,
        'evt2' => $model->event->event_name
    ]) ?></h1>
    
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
