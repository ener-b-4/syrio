<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\ArrayHelper;
use yii\helpers\Html;

use kartik\grid\GridView;
use app\components\helpers\DataGrid\CustomGridView;

use app\models\AppConstants;
use app\models\events\EventDeclaration;

/*
 * 
 * ACTION COLUMNS
 * 
 * 
 */    

$actionColumn = [
          'class' => \kartik\grid\ActionColumn::className(),
          'hiddenFromExport' => true,
          
          'header' => ucfirst(\Yii::t('app', 'actions')),
          'width' => 124,
    
          'buttons' => [
            //button-update
            'update' => function ($url, $model, $key) {
              //return $model->status === 'editable' ? Html::a('Update', $url) : '';
            },
            //button-delete
            'delete' => function ($url, $model, $key) {
              $delete_s = Yii::t('app', 'Delete');

              if (!Yii::$app->user->can('incident_draft_delete', ['draft'=>$model]))
              {
                  return '';
              }
              else
              {
                  if ($model->del_status == EventDeclaration::STATUS_ACTIVE)
                  {
                    $msg = Yii::t('app', 'Are you sure you want to delete {data}?', [
                        'data'=>$model->session_desc]);
                    $msg .= ' ';
                    $msg .= Yii::t('app', 'Operation CAN be undone.');
                      
                      return Html::a('<span class=" glyphicon glyphicon-trash" title="' . $delete_s . '"></span>', $url, [
                         'data-method' => 'post',
                          'data-confirm' => $msg,
                          'title' => Yii::t('app', 'Remove')
                      ]);
                  }
              }

              return '';
            },
                    
            'purge' => function ($url, $model, $key) {
                    /* @var $model \app\models\events\drafts\EventDraft */
                    
                    //return $model->del_status;

                    if (!Yii::$app->user->can('incident_draft_purge', ['draft'=>$model]))
                    {
                        return '';
                    }
                    $msg = Yii::t('app', 'Are you sure you want to PERMANENTLY delete {data}?', [
                        'data'=>$model->session_desc]);
                    $msg .= ' ';
                    $msg .= Yii::t('app', 'Operation CAN NOT be undone.');
                    return $model->del_status == 0 ? 
                            Html::a('<i class="glyphicon glyphicon-remove"></i>', $url, [
                                'title' => Yii::t('app/commands', 'Purge'),
                                'data-confirm' => $msg,
                                'data-method' => 'POST']) : '';
                },
            'reactivate' => function ($url, $model, $key) {
                    /* @var $model \app\models\events\drafts\EventDraft */

                    if (!Yii::$app->user->can('incident_draft_delete', ['draft'=>$model]) || $model->del_status != \app\models\events\EventDeclaration::STATUS_DELETED)
                    {
                        return '';
                    }

                    return $model->del_status == 0 ? 
                            Html::a('<i class="glyphicon glyphicon-repeat"></i>', $url, [
                                'title' => Yii::t('app', 'Undo delete'), 
                                'data-method'=>'POST']) : '';
                },
                    
            //button-unlock-all                        
            'unlock-all' => function ($url, $model, $key) {
                if ((Yii::$app->user->can('op_raporteur') || Yii::$app->user->can('inst_rapporteur'))) {
                  return $model->getStatus() == EventDeclaration::INCIDENT_REPORT_LOCKED ? Html::a('<i class="fa fa-unlock fa-lg"></i> <small>' . Yii::t('app', 'Unlock') .  '</small>', $url, [
                        'data-confirm' => Yii::t('app', 'Force unlock?'),
                        'class' => 'btn sy_padding_clear',
                        'data-method' => 'post',
                        'data-pjax' => '0'
                      ]) : '';
                } else {
                    return '';
                }
            },
            //button-reopen                                                
            'reopen' => function ($url, $model, $key) {
                if (Yii::$app->user->can('incident_draft_reopen', ['draft' => $model])) {
                      return Html::a('<span class="glyphicon glyphicon-hand-down text-warning" style=""></span> <small>'
                            . Yii::t('app', 'Re-open')
                            . '</small>', $url, [
                          'class' => 'btn sy_padding_clear',
                          'data' => [
                            'confirm' => Yii::t('app', 'Are you sure you want to re-open this item?'),
                          ],
                        ]);
                }
            },
            //button-sign                           
            'sign' => function ($url, $model, $key) {
              if (Yii::$app->user->can('incident_draft_sign', ['draft' => $model])) {
                return Html::a('<span class="glyphicon glyphicon-check text-success" style=""></span> <small>'
                      . Yii::t('app', 'Sign')
                      . '</small>', $url, [
                                            'class' => 'btn sy_padding_clear',
                                            'data' => [
                                                'confirm' => Yii::t('app', 'You are about to sign the incident report. This is the last step before submitting to the CA.'),
                                            ]
                                    ]);
              }
            },
            //button-submit-confirmation                           
            'submit-confirmation' => function ($url, $model, $key) {
              if (Yii::$app->user->can('incident_submit', ['draft'=>$model])) {
                  return $model->status == EventDeclaration::INCIDENT_REPORT_SIGNED ?
                      Html::a('<span class="glyphicon glyphicon-send text-success" style=""></span> <small>' . Yii::t('app/commands', 'Submit') .  '</small>', $url, [
                        'class' => 'btn sy_padding_clear',
                          'title' => 'Sends the current draft to the Competent Authority'
                      ]) : '';
              }
            },
                'pdf' => function ($url, $model, $key) {
                    $txt = '<span class="glyphicon glyphicon-print text-success" style=""></span>';
                    $txt .= ' ' . '<small>' . Yii::t('app', 'PDF') . '</small>';
                    
                  return Html::a($txt, $url, [
                            'class' => 'btn sy_padding_clear',
                          ]);
            }
          ],
          'template' => '{view} {purge} {reactivate} {delete} {pdf} {unlock-all} {reopen} {sign} {submit-confirmation} '
];
        
$columns = [
    //Column : Status of Report
    [
      'class' => 'kartik\grid\DataColumn',
      'attribute' => 'status',
      'label' => '',
      'width' => '43px',

      'hiddenFromExport' => true,  

      'vAlign' => 'middle',
      'hAlign' => 'center',
      'filterType' => null,
      'content' => function($model, $key, $index, $column) {
        //$model->refreshFinalizeStatus();
        if ($model->status == EventDeclaration::INCIDENT_REPORT_FINALIZED_PARTIALLY) {
          return '<i class="fa fa-star-half-o fa-lg" title="' . \Yii::t('app', 'PARTIALLY FINALIZED') . '"></i>';
        }
        else if ($model->status == EventDeclaration::INCIDENT_REPORT_FINALIZED) {
          return '<i class="fa fa-star fa-lg" title="'. \Yii::t('app', 'FINALIZED. READY TO BE SIGNED') . '"></i>';
        }
        else if ($model->status == EventDeclaration::INCIDENT_REPORT_SIGNED) {
          return '<i class="fa fa-check-square-o fa-lg" title="' . \Yii::t('app', 'SIGNED. Can be submitted.') . '"></i>';
        }
        else {
          return '<i class="fa fa-star-o fa-lg" title="' . ucfirst(\Yii::t('app', 'draft')) . '"></i>';
        }
      },
    ],
];

$columns = ArrayHelper::merge($columns, [
    //Column : Status of Report for Exporting
    [
      'class' => 'kartik\grid\DataColumn',
      'attribute' => 'status',
      'label' => \Yii::t('app', 'Drafting status'),
      'vAlign' => 'middle',
      'hAlign' => 'center',
      'hidden' => true,  
      'content' => function($model, $key, $index, $column) {
        //$model->refreshFinalizeStatus();
        if ($model->status == EventDeclaration::INCIDENT_REPORT_FINALIZED_PARTIALLY) {
          $s = \Yii::t('app', "PARTIALLY FINALIZED");
          return $s;
        }
        else if ($model->status == EventDeclaration::INCIDENT_REPORT_FINALIZED) {
          $s = \Yii::t('app', "FINALIZED. READY TO BE SIGNED");
          return $s;
        }
        else if ($model->status == EventDeclaration::INCIDENT_REPORT_SIGNED) {
          $s = \Yii::t('app', "SIGNED. Can be submitted.");
          return $s;
        }
        else {
          $s = ucfirst(\Yii::t('app', "draft"));
          return $s;
        }
      },
    ]
]);                  
          
$columns = ArrayHelper::merge($columns, [                 
    //Column : SUBMITTED or NOT to CA
    [
      'class' => 'kartik\grid\DataColumn',
      'attribute' => 'ca_status',
      'label' => '',
      'width' => '43px',
      'vAlign' => 'middle',
      'hAlign' => 'center',
      'hiddenFromExport' => true,  
      'content' => function($model, $key, $index, $column) {
        //$model->refreshFinalizeStatus();
        if ($model->ca_status == EventDeclaration::INCIDENT_REPORT_REPORTED) {
          $s = '<i class="fa fa-arrow-right fa-sm" title="' . \Yii::t('app', 'REPORTED TO THE CA. PENDING FOR ACCEPTANCE.') . '"></i>'
              . '<i class="fa fa-building-o fa-sm" title="' . \Yii::t('app', 'REPORTED TO THE CA. PENDING FOR ACCEPTANCE.') . '"></i>';
          return $s;
        }
        else if ($model->ca_status == EventDeclaration::INCIDENT_REPORT_REJECTED) {
          return '<i class="fa fa-exclamation-triangle fa-lg sy_error" title="' . \Yii::t('app', 'REJECTED BY THE CA. CHECK THE REASON THEN RESUBMIT.') . '"></i>';
        }
        else if ($model->ca_status == EventDeclaration::INCIDENT_REPORT_ACCEPTED) {
          return '<i class="fa fa-check fa-lg warning" title="' . \Yii::t('app', 'ACCEPTED AND ASSESSED BY THE CA.') . '"></i>';
        }
        else {
          return '<i class="fa fa-ellipsis-h fa-lg" title="' . \Yii::t('app', 'NOT SUBMITTED.') . '"></i>';
        };
      },
    ]
]);                  
              
$columns = ArrayHelper::merge($columns, [ 
    //Column : SUBMITTED or NOT to CA (column for export only)
    [
      'class' => 'kartik\grid\DataColumn',
      'attribute' => 'ca_status',
      'label' => \Yii::t('app', 'Reporting status'),

      'hidden' => true,  

      'vAlign' => 'middle',
      'hAlign' => 'center',
      'content' => function($model, $key, $index, $column) {
        //$model->refreshFinalizeStatus();
        if ($model->ca_status == EventDeclaration::INCIDENT_REPORT_REPORTED) {
          $s = \Yii::t('app', "REPORTED TO THE CA. PENDING FOR ACCEPTANCE.");
          return $s;
        }
        else if ($model->ca_status == EventDeclaration::INCIDENT_REPORT_REJECTED) {
          $s = \Yii::t('app', "REJECTED BY THE CA. CHECK THE REASON THEN RESUBMIT.");
          return $s;
        }
        else if ($model->ca_status == EventDeclaration::INCIDENT_REPORT_ACCEPTED) {
          $s = \Yii::t('app', "ACCEPTED AND ASSESSED BY THE CA.");
          return $s;
        }
        else {
          $s = \Yii::t('app', "NOT SUBMITTED.");
          return $s;
        };
      },
    ],
]);                  
                  
$columns = ArrayHelper::merge($columns, [          
    //Column : function to expand row
    [
      'class' => 'kartik\grid\ExpandRowColumn',
      'width' => '36px;',
      //'hidden' => true,
      'hiddenFromExport' => true,
      'value' => function($model, $key, $index, $column) {
        //return GridView::ROW_COLLAPSED;
        return CustomGridView::ROW_COLLAPSED;
      },
      'detail' => function($model, $key, $index, $column) {
        return Yii::$app->controller->renderPartial('_draft-details', ['model' => $model]);
      }
    ]
]);                  
                  
$columns = ArrayHelper::merge($columns, [  
    //Column : Created AT
    [
      'class' => 'kartik\grid\DataColumn',
      'attribute' => 'created_at',
      'vAlign' => 'middle',
      'width' => '155px',
        'content' => function($model, $key, $index, $column) {
            return \date('Y-m-d H:i:s', $model->created_at);
        }
    ]
]);                  
                  
$columns = ArrayHelper::merge($columns, [  
    //Column : DESCRIPTION
    //start of event types column and description
    [
      'class' => 'kartik\grid\DataColumn',
      'attribute' => 'session_desc',
      'vAlign' => 'middle',
    ]
]);                  
                  
$columns = ArrayHelper::merge($columns, [      
    [
      'class' => 'kartik\grid\DataColumn',
      'attribute' => \Yii::t('app', 'Classification'),
      'vAlign' => 'middle',
        'hiddenFromExport' => true,
      'content' => function($model, $key, $index, $column) {
        $s = '';
        //http://localhost:8080/SyRIO_DEV_post_brussels/web/index.php/events/drafts/event-draft/view?id=40
        foreach ($model->eventTypes as $key => $type) {

          $view_section = 'view-section-' . strtolower($type);
          $root = Yii::$app->getUrlManager()->createAbsoluteUrl(['/events/drafts/event-draft/' . $view_section, 'id' => $model->id, 'v' => 0]);
          //set the background
          $the_section = 'section' . strtoupper($type);
          $appended = '';
          $appended_text = '';

          if ($type == 'G' || $type == 'H') {
            $appended = '<span class="glyphicon glyphicon-ok" style="padding-left:3px;"></span>';
            $appended_text = Yii::t('app', 'Finalized and Signed by default.');
            $s .= '<span class="sy_event-span">'
                //(start) Author : cavesje  Date : 20150728 - Link page of details 
                //. '<a href = "#" title = "' . $appended_text . '">'
                . '<a href = "' . $root . '" title = "' . $appended_text . '">'
                //(end) Author : cavesje  Date : 20150728 - Link page of details 
                . $type
                . $appended
                . '</a>'
                . '</span>';
          }
          else {
              if (isset($model->$the_section)) {
                switch ($model->$the_section->status) {
                  case EventDeclaration::INCIDENT_REPORT_LOCKED:
                    $appended = '<i class="fa fa-lock fa-lg" style="padding-left:3px;"></i>';
                    $appended_text = Yii::t('app', 'Locked. Someone is currently working on it.');
                    break;
                  case EventDeclaration::INCIDENT_REPORT_SIGNED:
                    $appended = '<span class="glyphicon glyphicon-ok" style="padding-left:3px;"></span>';
                    $appended_text = ucfirst(Yii::t('app', 'signed'));
                    break;
                  case EventDeclaration::INCIDENT_REPORT_FINALIZED:
                    $appended = '<i class="fa fa-star-o fa-large" style="padding-left:3px;"></i>';
                    $appended_text = ucfirst(Yii::t('app', 'finalized'));
                    break;
                }
              }

            $s .= '<span class="sy_event-span">'
                . '<a href = "' . $root . '" title = "' . $appended_text . '">'
                . $type
                . $appended
                . '</a>'
                . '</span>';
          }
        }
        return $s;
      }
    ] //end of event types column and description
]);                  
                  
$columns = ArrayHelper::merge($columns, [ 
    //start of event types column and description for export
    [
      'class' => 'kartik\grid\DataColumn',
      'attribute' => \Yii::t('app', 'Classification'),
      'hidden' => true, 
      'vAlign' => 'middle',
      'content' => function($model, $key, $index, $column) {
        $types = [];
        foreach(range('a','j') as $letter) {
            $s = '';
            $attr = 'is_'.$letter;
            $section = 'section' . strtoupper($letter);
            if ($model->$attr) {
                $s .= strtoupper($letter);
                if ($letter === 'g' || $letter === 'h') {
                    $s.= ' (' . Yii::t('app', 'Finalized and Signed by default.') . ')';
                } else {
                    if (isset($model->$section)) {
                        switch ($model->$section->status) {
                          case EventDeclaration::INCIDENT_REPORT_LOCKED:
                            $s.= ' (' . ucfirst(strtolower(Yii::t('app', 'LOCKED'))) . ')';
                            break;
                          case EventDeclaration::INCIDENT_REPORT_SIGNED:
                            $s.= ' (' . ucfirst(Yii::t('app', 'signed')) . ')';
                            break;
                          case EventDeclaration::INCIDENT_REPORT_FINALIZED:
                            $s.= ' (' . ucfirst(Yii::t('app', 'finalized')) . ')';
                            break;
                        }
                    }
                }
                $types[] = $s;
            }
        }   //end foreach
        $s = implode(', ', $types);
        return $s;
      }
    ],
    
    /* here insert the first action column */
    $actionColumn,       
]);          

$toolbar_content = '';
$toolbar_content .= Html::a('<i class="glyphicon glyphicon-plus"></i>', 
    ['create', 'evtId' => $evtID], 
    [
        'class' => 'btn btn-success',
        'title' => Yii::t('app', 'Create {evt}', ['evt' => \Yii::t('app/crf', 'report draft'),]),
    ]);
//$toolbar_content .= Html::button('<i class="glyphicon glyphicon-plus"></i>', ['type'=>'button', 'title'=>Yii::t('kvgrid', 'Add Book'), 'class'=>'btn btn-success', 'onclick'=>'alert("This will launch the book creation form.\n\nDisabled for this demo!");']);
//$toolbar_content .= Html::button('<i class="glyphicon glyphicon-plus"></i>', [
//        'type'=>'button',
//        'title'=>Yii::t('app', 'Create'), 'class'=>'btn btn-success',
//        'onclick'=>'alert("This will launch the book creation form.\n\nDisabled for this demo!");']) 
//$toolbar_content .= Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['grid-demo'], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => Yii::t('app', 'Reset Grid')]);
$toolbar_content .= Html::a('<i class="glyphicon glyphicon-repeat"></i>', 
            ['index', 'evtID' => $evtID], 
            [
                'class' => 'btn btn-default', 
                'title' => Yii::t('app', 'Reset Grid')
            ]);
