<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\jui\Accordion;

    if ($model->is_a == 1)
    {
        
        //check if can repoen
        if (Yii::$app->user->can('incident_draft_section_reopen_operators', ['section' => $model->sectionA])) {
            $url = Yii::$app->urlManager->createUrl(['//events/drafts/event-draft/reopen-section', 'id'=>$model->id, 'section'=>'a']);
            $reopen = Html::a('<i class="fa fa-hand-o-up"></i> ' . Yii::t('app', 'reopen'), $url, [
                
            ]);
        } else {
            $reopen = '';
        }
        
?>

<div class="row" style="padding-top: 16px">
    <div class="col-md-10 col-sm-9">
        <div class="col-lg-12">
            <!-- the main part goes here -->
            <h4>
                <?= Html::checkbox('isA', $model->is_a, ['disabled'=>true]) ?>
                <strong style="padding-left: 10px; padding-right: 10px">A</strong> 
                <?= Yii::t('app/crf', 'Unintended release of oil, gas or other hazardous substances, whether or not ignited') ?>
            </h4>
        </div>
        <div class="col-lg-12">
            <!-- the description goes here -->
            <?php
                    $selGuidance1 = '<ol>';
                    $selGuidance1 .= '   <li>';
                    $selGuidance1 .= \Yii::t('app/crf', 'Any unintentional release of ignited gas or oil on or from an offshore installation;');
                    $selGuidance1 .= '   </li>';
                    $selGuidance1 .= '   <li>';
                    $selGuidance1 .=        \Yii::t('app/crf', 'The unintentional release on or from an offshore installation of:');
                    $selGuidance1 .= '       <ol type="a">';
                    $selGuidance1 .= '          <li>';
                    $selGuidance1 .=                \Yii::t('app/crf', 'not ignited natural gas or evaporated associated gas if mass released >= 1 kg');
                    $selGuidance1 .= '          </li>';
                    $selGuidance1 .= '          <li>';
                    $selGuidance1 .=                \Yii::t('app/crf', 'not ignited liquid of petroleum hydrocarbon if mass released >= 60 kg');
                    $selGuidance1 .= '          </li>';
                    $selGuidance1 .= '       </ol>';
                    $selGuidance1 .= '   </li>';
                    $selGuidance1 .= '   <li>';
                    $selGuidance1 .= \Yii::t('app/crf', 'The unintentional	release	or escape of any hazardous substance, '
    . 'for which the major accident risk has been assessed in the report on major hazards, '
    . 'on or from an offshore installation, including wells and returns of drilling additives.');
                    $selGuidance1 .= '   </li>';
                    $selGuidance1 .= '</ol>';


                    echo Accordion::widget([
                        'items' => [
                            [
                                'header' => Yii::t('app', 'Selection guidance'),
                                'headerOptions' => ['tag' => 'h3'],
                                'content' => $selGuidance1,
                                'options' => ['tag' => 'div'],
                            ],
                        ],
                        'options' => ['tag' => 'div'],
                        'itemOptions' => ['tag' => 'div'],
                        'headerOptions' => ['tag' => 'h3'],
                        'clientOptions' => ['collapsible' => true, 'active'=>false],
                    ]);                        

            ?>                            
        </div>
    </div>
    <div class="col-md-2 col-sm-3 text-right" style="padding-top: 12px;">
        <div class="row">
            <div class="col-lg-12">
                <!-- the commands go here -->
                <?= 
                    //Html::a(Yii::t('app', 'Goto {evt}', ['evt'=>Yii::t('app', 'Section {evt}', ['evt'=>'A'])]), ['view-section-a', 'id' => $model->id, 'v' => 1 ], ['class' => 'btn btn-primary']) 
                    Html::a(Yii::t('app', 'Goto {evt}', ['evt'=>Yii::t('app/crf', 'Section {evt}', ['evt'=>'A'])]), ['view-section-a', 'id' => $model->id, 'v' => 0 ], ['class' => 'btn btn-primary']) 
                ?>
            </div>
            <div class="col-lg-12">
                    <?php
                    $section = $model->sectionA;
                    $stat_text = '';
                    $stat_css = '';
                    $stat_details = '';
                                
                    switch ($section->status) {
                        case app\models\events\EventDeclaration::INCIDENT_REPORT_DRAFT:
                            $stat_text = \Yii::t('app', 'draft');
                            $stat_css = 'label-info';
                            break;
                        case app\models\events\EventDeclaration::INCIDENT_REPORT_LOCKED:
                            $stat_text = \Yii::t('app', 'LOCKED');
                            $stat_css = 'label-danger';
                            $user = app\models\User::find(['id'=>$section->locked_by])->one();
                            if (isset($user))
                            {
                                $stat_details = \Yii::t('app', 'by {evt1} since {evt2}', [
                                    'evt1' => $user->full_name,
                                    'evt2' => '<br/>' . date('Y-m-d H:i:s', $section->locked_at)
                                ]);
                            }
                            break;
                        case app\models\events\EventDeclaration::INCIDENT_REPORT_FINALIZED:
                            $stat_text = strtoupper(\Yii::t('app', 'finalized'));
                            $stat_css = 'label-success';
                            break;
                        case app\models\events\EventDeclaration::INCIDENT_REPORT_SIGNED:
                            $stat_text = strtoupper(\Yii::t('app', 'signed'));
                            $stat_css = 'label-success';
                            break;
                    }
                    ?>
                    <span class="label <?= $stat_css ?>">
                        <?= $stat_text ?>
                    </span>
                    <div>
                        <small>
                            <?= $stat_details; ?>
                        </small>
                    </div>
                    <div>
                        <small>
                            <?= $reopen; ?>
                        </small>
                    </div>
            </div>
        </div>
    </div>
</div>

<?php
    }
?>