<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\jui\Accordion;

    if ($model->is_d == 1)
    {
        //check if can repoen
        if (Yii::$app->user->can('incident_draft_section_reopen_operators', ['section' => $model->sectionD])) {
            $url = Yii::$app->urlManager->createUrl(['//events/drafts/event-draft/reopen-section', 'id'=>$model->id, 'section'=>'d']);
            $reopen = Html::a('<i class="fa fa-hand-o-up"></i> ' . Yii::t('app', 'reopen'), $url, [
                
            ]);
        } else {
            $reopen = '';
        }
        
?>

<div class="row" style="padding-top: 16px">
    <div class="col-md-10 col-sm-9">
        <div class="col-lg-12">
            <!-- the main part goes here -->
            <h4>
                <?= Html::checkbox('isD', $model->is_d, ['disabled'=>true]) ?>
                <strong style="padding-left: 10px; padding-right: 10px">D</strong> 
                <?= ucfirst(\Yii::t('app/crf', 'significant loss of structural integrity, or loss of protection against the effects of fire or explosion, or loss of station keeping in relation to a mobile installation')) ?>
            </h4>
        </div>
        <div class="col-lg-12">
            <!-- the description goes here -->
                <?php

                    $selGuidance3 = '<ol class="list-unstyled">';
                    $selGuidance3 .= '   <li>';
                    $selGuidance3 .=        \Yii::t('app/crf', 'Any detected condition that reduces the designed structural integrity of the installation, including stability, buoyancy and station keeping, to the extent that it requires '
                            . 'immediate remedial action.');
                    $selGuidance3 .= '   </li>';
                    $selGuidance3 .= '</ol>';


                    echo Accordion::widget([
                        'items' => [
                            [
                                'header' => Yii::t('app', 'Selection guidance'),
                                'headerOptions' => ['tag' => 'h3'],
                                'content' => $selGuidance3,
                                'options' => ['tag' => 'div'],
                            ],
                        ],
                        'options' => ['tag' => 'div'],
                        'itemOptions' => ['tag' => 'div'],
                        'headerOptions' => ['tag' => 'h3'],
                        'clientOptions' => ['collapsible' => true, 'active'=>false],
                    ]);                        
                ?>                            
            
        </div>
    </div>
    
        <div class="col-md-2 col-sm-3 text-right" style="padding-top: 12px;">
                    <div class="row">
                        <div class="col-lg-12">
        <!-- the commands go here -->
        <?= 
          //Html::a(Yii::t('app', 'Goto {evt}', ['evt'=>Yii::t('app', 'Section {evt}', ['evt'=>'D'])]), ['view-section-d', 'id' => $model->id, 'v' => 1 ], ['class' => 'btn btn-primary']) 
          Html::a(Yii::t('app', 'Goto {evt}', ['evt'=>Yii::t('app/crf', 'Section {evt}', ['evt'=>'D'])]), ['view-section-d', 'id' => $model->id, 'v' => 0 ], ['class' => 'btn btn-primary']) 
        ?>
                        </div>
                        <div class="col-lg-12">
                                <?php
                                $section = $model->sectionD;
                                $stat_text = '';
                                $stat_css = '';
                                $stat_details = '';

                                
                                switch ($section->status) {
                                    case app\models\events\EventDeclaration::INCIDENT_REPORT_DRAFT:
                                        $stat_text = \Yii::t('app', 'draft');
                                        $stat_css = 'label-info';
                                        break;
                                    case app\models\events\EventDeclaration::INCIDENT_REPORT_LOCKED:
                                        $stat_text = \Yii::t('app', 'LOCKED');
                                        $stat_css = 'label-danger';
                                        $user = app\models\User::find(['id'=>$section->locked_by])->one();
                                        if (isset($user))
                                        {
                                            $stat_details = \Yii::t('app', 'by {evt1} since {evt2}', [
                                                'evt1' => $user->full_name,
                                                'evt2' => '<br/>' . date('Y-m-d H:i:s', $section->locked_at)
                                            ]);
                                        }
                                        
                                        break;
                                    case app\models\events\EventDeclaration::INCIDENT_REPORT_FINALIZED:
                                        $stat_text = strtoupper(\Yii::t('app', 'finalized'));
                                        $stat_css = 'label-success';
                                        break;
                                    case app\models\events\EventDeclaration::INCIDENT_REPORT_SIGNED:
                                        $stat_text = strtoupper(\Yii::t('app', 'signed'));
                                        $stat_css = 'label-success';
                                        break;
                                }
                                ?>

                                <span class="label <?= $stat_css ?>">
                                    <?= $stat_text ?>
                                </span>
                                <div>
                                    <small>
                                        <?= $stat_details; ?>
                                    </small>
                                </div>
                                <div>
                                    <small>
                                        <?= $reopen; ?>
                                    </small>
                                </div>
                                
                        </div>
                    </div>
        
    </div>
</div>

<!-- end of hasC -->
<?php
    }
?>