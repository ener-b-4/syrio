<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\jui\Accordion;

    if ($model->is_b == 1)
    {
        
        //check if can repoen
        if (Yii::$app->user->can('incident_draft_section_reopen_operators', ['section' => $model->sectionB])) {
            $url = Yii::$app->urlManager->createUrl(['//events/drafts/event-draft/reopen-section', 'id'=>$model->id, 'section'=>'b']);
            $reopen = Html::a('<i class="fa fa-hand-o-up"></i> ' . Yii::t('app', 'reopen'), $url, [
                
            ]);
        } else {
            $reopen = '';
        }
        
        
?>


                <div class="row" style="padding-top: 16px">
                    <div class="col-md-10 col-sm-9">
                        <div class="col-lg-12">
                                <h4>
                                <?= Html::checkbox('isB', $model->is_b, ['disabled'=>true]) ?>
                                <strong style="padding-left: 10px; padding-right: 10px">B</strong> 
                                <?= \Yii::t('app/crf', 'Loss of well control requiring actuation of well control equipment, or failure of a well barrier requiring its replacement or repair') ?>
                                </h4>
                        </div>
                        <div class="col-lg-12">
                                <?php

                    $selGuidance2 = '<ol>';
                    $selGuidance2 .= '   <li>';
                    $selGuidance2 .=        \Yii::t('app/crf', 'Any blowout, regardless of the duration;');
                    $selGuidance2 .= '   </li>';
                    $selGuidance2 .= '   <li>';
                    $selGuidance2 .=        \Yii::t('app/crf', 'The coming into operation of a blowout prevention or diverter system to control flow of well-fluids;');
                    $selGuidance2 .= '   </li>';
                    $selGuidance2 .= '   <li>';
                    $selGuidance2 .=        \Yii::t('app/crf', 'The mechanical failure of any part of a well, whose purpose is to prevent or limit the effect of the unintentional release of fluids from a well or a reservoir being drawn on by a well, or whose failure would cause or contribute to such a release');
                    $selGuidance2 .= '   </li>';
                    $selGuidance2 .= '   <li>';
                    $selGuidance2 .=        \Yii::t('app/crf', 'The taking of precautionary measures additional to any already contained in the original drilling programme where a planned minimum separation distance between adjacent wells was not maintained.');
                    $selGuidance2 .= '   </li>';
                    $selGuidance2 .= '</ol>';


                    echo Accordion::widget([
                        'items' => [
                            [
                                'header' => Yii::t('app', 'Selection guidance'),
                                'headerOptions' => ['tag' => 'h3'],
                                'content' => $selGuidance2,
                                'options' => ['tag' => 'div'],
                            ],
                        ],
                        'options' => ['tag' => 'div'],
                        'itemOptions' => ['tag' => 'div'],
                        'headerOptions' => ['tag' => 'h3'],
                        'clientOptions' => ['collapsible' => true, 'active'=>false],
                    ]);                        
                                ?>  
                        </div>

                    </div>
                    
                    <div class="col-md-2 col-sm-3 text-right" style="padding-top: 12px;">
                        <div class="row">
                            <div class="col-lg-12">
                              <?= 
                                //Html::a(Yii::t('app', 'Goto {evt}', ['evt'=>Yii::t('app', 'Section {evt}', ['evt'=>'B'])]), ['view-section-b', 'id' => $model->id, 'v' => 1 ], ['class' => 'btn btn-primary']) 
                                Html::a(Yii::t('app', 'Goto {evt}', ['evt'=>Yii::t('app/crf', 'Section {evt}', ['evt'=>'B'])]), ['view-section-b', 'id' => $model->id, 'v' => 0 ], ['class' => 'btn btn-primary']) 
                              ?>
                            </div>
                            <div class="col-lg-12">
                                <?php
                                $section = $model->sectionB;
                                $stat_text = '';
                                $stat_css = '';
                                
                                $stat_details = '';
                                
                                switch ($section->status) {
                                    case app\models\events\EventDeclaration::INCIDENT_REPORT_DRAFT:
                                        $stat_text = \Yii::t('app', 'draft');
                                        $stat_css = 'label-info';
                                        break;
                                    case app\models\events\EventDeclaration::INCIDENT_REPORT_LOCKED:
                                        $stat_text = \Yii::t('app', 'LOCKED');
                                        $stat_css = 'label-danger';
                                            $user = app\models\User::find(['id'=>$section->locked_by])->one();
                                            if (isset($user))
                                            {
                                                $stat_details = \Yii::t('app', 'by {evt1} since {evt2}', [
                                                    'evt1' => $user->full_name,
                                                    'evt2' => '<br/>' . date('Y-m-d H:i:s', $section->locked_at)
                                                ]);
                                            }
                                        break;
                                    case app\models\events\EventDeclaration::INCIDENT_REPORT_FINALIZED:
                                        $stat_text = strtoupper(\Yii::t('app', 'finalized'));
                                        $stat_css = 'label-success';
                                        break;
                                    case app\models\events\EventDeclaration::INCIDENT_REPORT_SIGNED:
                                        $stat_text = strtoupper(\Yii::t('app', 'signed'));
                                        $stat_css = 'label-success';
                                        break;
                                }
                                ?>
                            
                                <span class="label <?= $stat_css ?>">
                                    <?= $stat_text ?>
                                </span>
                                <div>
                                    <small>
                                        <?= $stat_details; ?>
                                    </small>
                                </div>
                                <div>
                                    <small>
                                        <?= $reopen; ?>
                                    </small>
                                </div>
                          
                            
                            </div>
                        </div>
                    </div>

            </div> <!-- end of hasB -->
<?php
    }
?>