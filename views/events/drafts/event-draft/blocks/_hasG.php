<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\jui\Accordion;

    if ($model->is_g == 1)
    {
?>

<div class="row" style="padding-top: 16px">
    <div class="col-md-10 col-sm-9">
        <div class="col-lg-12">
            <!-- the main part goes here -->
            <h4>
                <?= Html::checkbox('isG', $model->is_g, ['disabled'=>true]) ?>
                <strong style="padding-left: 10px; padding-right: 10px">G</strong> 
                    <?= \Yii::t('app/crf', 'Any fatal accident to be reported under the requirements of Directive 92/91/EEC') ?>
            </h4>
        </div>
        <div class="col-lg-12">
            <!-- the description goes here -->
        </div>
    </div>
    <div class="col-md-2 col-sm-3 text-right" style="padding-top: 12px;">
                    <div class="row">
                        <div class="col-lg-12">
        <!-- the commands go here -->
        <?= 
          Html::a(Yii::t('app', 'Goto {evt}', ['evt'=>Yii::t('app/crf', 'Section {evt}', ['evt'=>'G'])]), ['view-section-g', 'id' => $model->id, 'v' => 1 ], ['class' => 'btn btn-primary']) 
        ?>
                        </div>
                        <div class="col-lg-12">
                                <?php
                                $section = $model->sectionG;
                                $stat_text = strtoupper(\Yii::t('app', 'finalized'));
                                $stat_css = 'label-success';
                                $stat_details = '';

                                ?>
                                
                    <span class="label <?= $stat_css ?>">
                        <?= $stat_text ?>
                    </span>
                    <div>
                        <small>
                            <?= $stat_details; ?>
                        </small>
                    </div>

                        </div>
                    </div>
        
    </div>
</div>

<!-- end of hasG -->
<?php
    }
?>