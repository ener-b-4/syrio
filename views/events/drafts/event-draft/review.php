<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\web\JqueryAsset;
use yii\jui\Accordion;
use \yii\widgets\ActiveForm;
use yii\helpers\Url;

use app\models\User;
use app\models\events\EventDeclaration;
use app\widgets\ValidationReportWidget;

/* @var $this yii\web\View */
/* @var $model app\models\events\drafts\EventDraft */
/* @var bool $display_status */
/* @var string $task */


$event = $model->event;
$dt = new DateTime($event->event_date_time);

$success_signed='You have successfully signed the Incident Report.<br/>'
        . 'You may now proceed to <a href="#"> submit the report </a> to the Competent Authority .'
        . 'or go back to the <a href="#">Incident reports</a> list.';

if (isset($display_status))
{
    if (isset($task))
    {
        switch ($task)
        {
            case 'finalize':
                echo ValidationReportWidget::widget([
                    'title' => \Yii::t('app','Finalizing Report'),
                    'errors_message' => \Yii::t('app','The incident report could not be finalized! Please review the information below and try again.'),
                    'errors'=>$model->errors,
                    'no_errors_message' => \Yii::t('app','The incident report has been finalized. You may now proceed with signing the report.'),
                ]);
                break;
            case 'sign':
                echo ValidationReportWidget::widget([
                    'title' => \Yii::t('app','Finalizing Report'),
                    'errors_message' => \Yii::t('app','The incident report could not be signed! Please review the information below and try again.'),
                    'errors'=>$model->errors,
                    'no_errors_message' => $success_signed,
                ]);
                break;
        }
    }
}

?>

<div class="row">
    <div class="col-lg-12">
        <?php include Yii::getAlias('@app') . '/views/shared_parts/message_snippet.php'; ?>
    </div>
</div>

<?php 
//(start) Author: vamanbo   Date:15/09/2015     On: adding the pdf button (enhancement)
//(edit) Author: vamanbo    Date:23/09/2016     On: integrate the new pdf module
echo Html::a(Yii::t('app', '<i class="fa fa-file-pdf-o"></i> PDF'), 
['pdfgenerator/default/index', 'id' => $model->id ], 
['class' => 'btn btn-default']); 
//(end) Author: vamanbo   Date:15/09/2015     On: adding the pdf button (enhancement)
?>

<h1><?= $model->event->event_name ?></h1>

<div class="sy_block_v_spacer">
    <!-- render the event details -->
    <h3><?= Yii::t('app/crf' , 'Event date and time') ?></h3>
    (a) <?= Yii::t('app/crf' , 'Event date') ?>: <?= date_format($dt, 'd-m-Y') ?>
    <br/>
    (b) <?= Yii::t('app/crf' , 'Event time') ?>: <?= date_format($dt, 'H:i:s') ?>


    <h3>Details of the location and person reporting the event</h3>
    
    <table class='sy_simple-table'>
        <tr>
            <td><?= $event->attributeLabels()['operator'] ?></td>
            <td><?= $event->operator ?></td>
        </tr>
        <tr>
            <td><?= $event->attributeLabels()['installation_name_type'] ?></td>
            <td><?= $event->installation_name_type ?></td>
        </tr>
        <tr>
            <td><?= $event->attributeLabels()['field_name'] . ' ' . \Yii::t('app/crf', '(if relevant)') ?></td>
            <td><?= $event->field_name ?></td>
        </tr>
        <tr style='min-height: 32px; max-height: 32px; height: 32px;'>
            <td colspan='2'></td>
        </tr>
        <tr>
            <td><?= $event->attributeLabels()['raporteur_name'] ?></td>
            <td><?= $event->raporteur_name ?></td>
        </tr>
        <tr>
            <td><?= $event->attributeLabels()['raporteur_role'] ?></td>
            <td><?= $event->raporteur_role ?></td>
        </tr>
        <tr style='min-height: 32px; max-height: 32px; height: 32px;'>
            <td colspan='2'></td>
        </tr>
        <tr>
            <td colspan='2'><strong><?= Yii::t('app/crf','Contact details') ?></strong></td>
        </tr>
        <tr>
            <td><?= $event->attributeLabels()['contact_tel'] ?></td>
            <td><?= $event->contact_tel ?></td>
        </tr>
        <tr>
            <td><?= $event->attributeLabels()['contact_email'] ?></td>
            <td><?= $event->contact_email ?></td>
        </tr>
    </table>
</div>

<div class="sy_block_v_spacer">
    <h3><?= \Yii::t('app/crf', 'Event categorization') ?></h3>
    <p><em><strong><?= Yii::t('app/crf', 'What type of event is being reported?') ?></strong><?= ' ' . Yii::t('app/crf', '(More than one option might be chosen)')?></em></p>

    <?= $this->render('view_all', ['model'=>$model]) ?>    
</div>




<?php

if ($model->is_a)
{
    echo '<div class="sy_block_v_spacer" id="a_block">';
    echo '<div class="col-lg-12 text-center sy_pad_bottom_36"><h3>' . \Yii::t('app/crf', 'Section {evt}', ['evt'=>'A']) . '</h3>'
            . '<strong><h4>' . \Yii::t('app/crf', 'Unintended release of oil, gas or other hazardous substances, whether or not ignited') . '</h4></strong>'
            . '</div>';

    echo $this->render('/events/drafts/sections/subsections/a1/view_all', ['model'=>$model->sectionA->a1, 'stripped'=>1]);

    echo $this->render('/events/drafts/sections/subsections/a2/review', ['model'=>$model->sectionA->a2, 'stripped'=>1]);

    echo $this->render('/events/drafts/sections/subsections/a3/review', ['model'=>$model->sectionA->a3, 'stripped'=>1]);

    echo $this->render('/events/drafts/sections/subsections/a4/review', ['model'=>$model->sectionA->a4, 'stripped'=>1]);
    
    echo '</div>';  //block-spacer
}




if ($model->is_b)
{
    echo '<div class="sy_block_v_spacer">';
    echo '    <a id="b_block" href="#"></a>';
    
    echo '<div class="col-lg-12 text-center sy_pad_bottom_36">'
            . '<h3>' . \Yii::t('app/crf', 'Section {evt}', ['evt'=>'B']) . '</h3>'
            . '<strong><h4>' . \app\models\crf\CrfHelper::SectionsTitle()['B'] .  '</h4></strong>'
            . '</div>';


    echo $this->render('/events/drafts/sections/subsections/b1/review_2', ['model'=>$model->sectionB->b1,'event_id'=>$model->event_id, 'section_name'=>'B.']);
//    echo $this->render('/events/drafts/sections/subsections/b1/review', ['model'=>$model->sectionB->b1, 'event_id'=>$model->event_id, 'section_name'=>'b1', 'stripped'=>1]);
    echo $this->render('/events/drafts/sections/subsections/b2/review', ['model'=>$model->sectionB->b2, 'stripped'=>1]);
    echo $this->render('/events/drafts/sections/subsections/b3/review', ['model'=>$model->sectionB->b3, 'stripped'=>1]);
    echo $this->render('/events/drafts/sections/subsections/b4/review', ['model'=>$model->sectionB->b4, 'stripped'=>1]);

    echo '</div>';  //block spacer
}


if ($model->is_c)
{
    echo '<div class="sy_block_v_spacer">';
    echo '    <a id="c_block" href="#"></a>';
    
    echo '<div class="col-lg-12 text-center sy_pad_bottom_36">'
            . '<h3>' . \Yii::t('app/crf', 'Section {evt}', ['evt'=>'C']) . '</h3>'
            . '<strong><h4>' . \app\models\crf\CrfHelper::SectionsTitle()['C'] .  '</h4></strong>'
            . '</div>';


    echo $this->render('/events/drafts/sections/subsections/c1/review_2', ['model'=>$model->sectionC->c1, 'stripped'=>1]);
    echo $this->render('/events/drafts/sections/subsections/c2/review', [
        'model'=>$model->sectionC->c2, 
        'stripped'=>1, 
        'section2_1'=>$model->sectionC->c2->sC2_1,
        'section2_2'=>$model->sectionC->c2->sC2_2]);
    echo $this->render('/events/drafts/sections/subsections/c3/review', ['model'=>$model->sectionC->c3, 'stripped'=>1]);
    echo $this->render('/events/drafts/sections/subsections/c4/review', ['model'=>$model->sectionC->c4, 'stripped'=>1]);

    echo '</div>';
}

if ($model->is_d)
{
    echo '<div class="sy_block_v_spacer">';
    echo '    <a id="d_block" href="#"></a>';
    
    echo '<div class="col-lg-12 text-center sy_pad_bottom_36">'
            . '<h3>' . \Yii::t('app/crf', 'Section {evt}', ['evt'=>'D']) . '</h3>'
            . '<strong><h4>' . \app\models\crf\CrfHelper::SectionsTitle()['D'] .  '</h4></strong>'
            . '</div>';

    echo '<div class="sy_block_v_spacer">';

    echo $this->render('/events/drafts/sections/subsections/d1/review_2', ['model'=>$model->sectionD->d1, 'stripped'=>1]);
    echo $this->render('/events/drafts/sections/subsections/d2/review', ['model'=>$model->sectionD->d2, 'stripped'=>1]);
    echo $this->render('/events/drafts/sections/subsections/d3/review', ['model'=>$model->sectionD->d3, 'stripped'=>1]);
    echo $this->render('/events/drafts/sections/subsections/d4/review', ['model'=>$model->sectionD->d4, 'stripped'=>1]);

    echo '</div>';

    echo '</div>';
}

if ($model->is_e)
{
    echo '<div class="sy_block_v_spacer">';
    echo '    <a id="e_block" href="#"></a>';
    
    echo '<div class="col-lg-12 text-center sy_pad_bottom_36">'
            . '<h3>' . \Yii::t('app/crf', 'Section {evt}', ['evt'=>'E']) . '</h3>'
            . '<strong><h4>' . \app\models\crf\CrfHelper::SectionsTitle()['E'] .  '</h4></strong>'
            . '</div>';


    echo $this->render('/events/drafts/sections/subsections/e1/review_2', ['model'=>$model->sectionE->e1, 'stripped'=>1]);
    echo $this->render('/events/drafts/sections/subsections/e2/review', ['model'=>$model->sectionE->e2, 'stripped'=>1]);
    echo $this->render('/events/drafts/sections/subsections/e3/review', ['model'=>$model->sectionE->e3, 'stripped'=>1]);
    echo $this->render('/events/drafts/sections/subsections/e4/review', ['model'=>$model->sectionE->e4, 'stripped'=>1]);
    
    echo '</div>';
}

if ($model->is_f)
{
    echo '<div class="sy_block_v_spacer">';
    echo '    <a id="f_block" href="#"></a>';
    
    echo '<div class="col-lg-12 text-center sy_pad_bottom_36">'
            . '<h3>' . \Yii::t('app/crf', 'Section {evt}', ['evt'=>'F']) . '</h3>'
            . '<strong><h4>' . \app\models\crf\CrfHelper::SectionsTitle()['F'] .  '</h4></strong>'
            . '</div>';


    echo '<div class="col-lg-12" style="margin-top:16px; margin-bottom:16px">'
    . 'Hellicopter incidents are reported under CAA regulations. If a helicopter accident occurs in relation to Directive 2013/30/EU, section F must be completed.'
    . '</div>';
    
    echo $this->render('/events/drafts/sections/subsections/f1/review_2', ['model'=>$model->sectionF->f1, 'stripped'=>1]);
    echo $this->render('/events/drafts/sections/subsections/f2/review', ['model'=>$model->sectionF->f2, 'stripped'=>1]);
    echo $this->render('/events/drafts/sections/subsections/f3/review', ['model'=>$model->sectionF->f3, 'stripped'=>1]);
    echo $this->render('/events/drafts/sections/subsections/f4/review', ['model'=>$model->sectionF->f4, 'stripped'=>1]);

    echo '</div>';
}

if ($model->is_g)
{
    echo '<div class="sy_block_v_spacer">';
    echo '    <a id="g_block" href="#"></a>';
    
    echo '<div class="col-lg-12 sy_pad_top_36 text-center">'
            . '<h3>' . \Yii::t('app/crf', 'Section {evt}', ['evt'=>'G']) . '</h3>'
            . '<strong><h4>' . \app\models\crf\CrfHelper::SectionsTitle()['G'] .  '</h4></strong>'
            . '</div>';


    echo '<div class="col-lg-12 sy_pad_top_36" style="margin-top:16px; margin-bottom:16px">'
    . \Yii::t('app', 'Intentionally left blank')
    . '</div>';    
    
    echo '</div>';
}

if ($model->is_h)
{
    echo '<div class="sy_block_v_spacer sy_pad_top_36">';
    echo '    <a id="h_block" href="#"></a>';
    
    echo '<div class="col-lg-12 text-center">'
            . '<h3>' . \Yii::t('app/crf', 'Section {evt}', ['evt'=>'H']) . '</h3>'
            . '<strong><h4>' . \app\models\crf\CrfHelper::SectionsTitle()['H'] .  '</h4></strong>'
            . '</div>';


    echo '<div class="col-lg-12 sy_pad_top_36" style="margin-top:16px; margin-bottom:16px">'
    . \Yii::t('app', 'Intentionally left blank')
    . '</div>';    
    
    echo '</div>';
}

if ($model->is_i)
{
    echo '<div class="sy_block_v_spacer sy_pad_top_36">';
    echo '    <a id="i_block" href="#"></a>';
    
    echo '<div class="col-lg-12 text-center">'
            . '<h3>' . \Yii::t('app/crf', 'Section {evt}', ['evt'=>'I']) . '</h3>'
            . '<strong><h4>' . \app\models\crf\CrfHelper::SectionsTitle()['I'] .  '</h4></strong>'
            . '</div>';


    echo $this->render('/events/drafts/sections/subsections/i1/review_2', ['model'=>$model->sectionI->i1, 'stripped'=>1]);
    echo $this->render('/events/drafts/sections/subsections/i2/review', ['model'=>$model->sectionI->i2, 'stripped'=>1]);
    echo $this->render('/events/drafts/sections/subsections/i3/review', ['model'=>$model->sectionI->i3, 'stripped'=>1]);
    echo $this->render('/events/drafts/sections/subsections/i4/review', ['model'=>$model->sectionI->i4, 'stripped'=>1]);

    echo '</div>';
}

if ($model->is_j)
{
    echo '<div class="sy_block_v_spacer" >';
    echo '    <a id="j_block" href="#"></a>';
    
    echo '<div class="col-lg-12 text-center">'
            . '<h3>' . \Yii::t('app/crf', 'Section {evt}', ['evt'=>'J']) . '</h3>'
            . '<strong><h4>' . \app\models\crf\CrfHelper::SectionsTitle()['J'] .  '</h4></strong>'
            . '</div>';


    echo $this->render('/events/drafts/sections/subsections/j1/review_2', ['model'=>$model->sectionJ->j1, 'stripped'=>1]);
    echo $this->render('/events/drafts/sections/subsections/j2/review', ['model'=>$model->sectionJ->j2, 'stripped'=>1]);
    echo $this->render('/events/drafts/sections/subsections/j3/review', ['model'=>$model->sectionJ->j3, 'stripped'=>1]);
    echo $this->render('/events/drafts/sections/subsections/j4/review', ['model'=>$model->sectionJ->j4, 'stripped'=>1]);

    echo '</div>';
}

?>

<?php
if (!(isset($model->event->report)) ||
($model->event->report->id == $model->id && $model->event->status==EventDeclaration::INCIDENT_REPORT_REJECTED))
{
    if ($model->status <= EventDeclaration::INCIDENT_REPORT_DRAFT)
    {
        echo Html::a(Yii::t('app', 'Finalize {evt}', ['evt'=>\Yii::t('app', 'Incident Report')]), ['finalize', 'id' => $model->id], ['class' => 'btn btn-primary']);
    }
    else if ($model->status == EventDeclaration::INCIDENT_REPORT_FINALIZED)
    {
        echo Html::a(Yii::t('app', 'Sign Incident Report'), ['sign', 'id' => $model->id], ['class' => 'btn btn-primary']);
    }
    else if ($model->status == EventDeclaration::INCIDENT_REPORT_SIGNED && ( is_null($model->ca_status) || $model->ca_status== EventDeclaration::INCIDENT_REPORT_REJECTED) )
    {
        echo Html::a(Yii::t('app', 'Submit Incident Report to the CA'), ['submit-confirmation', 'id' => $model->id], ['class' => 'btn btn-success']);
    }
}

?>

<?php

$this->title = $model->session_desc;

$_event = EventDeclaration::findOne(['id' => $model->event_id]);
$breadcrumbs = [
    [
        'label' => Yii::t('app', 'Registered events'),
        'url' => Url::to(['/events/event-declaration/index']),
        //'template' =>"<li>{link}</li>
    ],
    [
        'label' => $_event->event_name,
        'url' => Url::to(['/events/event-declaration/view', 'id'=>$_event->id]),
        //'template' =>"<li>{link}</li>
    ],
    [
        'label' => Yii::t('app', 'Reporting'),
        'url' => Url::to(['/events/drafts/event-draft/index', 'evtID'=>$_event->id]),
    ],
    [
        'label' => $model->session_desc,
        'url' => Url::to(['/events/drafts/event-draft/view', 'id'=>$model->id]),
    ],
    [
        'label' => 'Review',
        //'url' => Url::to(['/events/drafts/event-draft/view', 'id'=>$model->id]),
    ],
    
];

$this->params['breadcrumbs'] = $breadcrumbs;



?>


<?php // $this->render('/events/event-declaration/view', ['model'=>$event]) ?>    

