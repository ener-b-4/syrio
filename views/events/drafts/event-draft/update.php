<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\rebuilt\EventDraft */

$this->title = ucfirst(Yii::t('app', 'modify {evt}', [
    'evt' => strtolower(Yii::t('app/crf', 'Event categorization')),
]));

//Author : cavesje  Date : 20150617 - Missing the parameter $evtID
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Event Drafts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Events management'), 'url' => ['events/event-declaration/index']];
$this->params['breadcrumbs'][] = ['label' => $model->event->event_name, 'url' => ['events/event-declaration/display-drafts', 'id'=>$model->event->id]];
$this->params['breadcrumbs'][] = $this->title 
?>
<div class="event-draft-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

$script = <<< JS
$(window).bind('beforeunload', function() {
        if (typeof proceed === 'undefined')
        {
            _msg = 'Please only use Update and Cancel buttons at the bottom of this page to navigate away!';
            return _msg;
        }
        });
        
        //alert('here');
JS;

$this->registerJs($script, \yii\web\View::POS_READY);
// where $position can be View::POS_READY (the default), 

?>
