<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;

//(start) Author: cavesje   Date: 14.08.2015  - Declaration of CustomDataGrid
use app\components\helpers\DataGrid\CustomGridView;
use app\components\helpers\DataGrid\funDataGridClass;
//(end) Author: cavesje   Date: 14.08.2015  - Declaration of CustomDataGrid

use app\models\events\EventDeclaration;
use app\widgets\ActionMessageWidget;
use app\assets\FontAwesomeAsset;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\rebuilt\EventDraftSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $evtID integer the ID of the event */

FontAwesomeAsset::register($this);
//$this->registerAssetBundle(FontAwesomeAsset::className());

$_event = EventDeclaration::findOne(['id' => $evtID]);

$breadcrumbs = [
	[
		'label' => Yii::t('app', 'Registered events'),
		'url' => Url::to(['/events/event-declaration/index']),
		//'template' =>"<li>{link}</li>
	],
	[
		'label' => $_event->event_name,
		'url' => Url::to(['/events/event-declaration/view', 'id'=>$_event->id]),
		//'template' =>"<li>{link}</li>
	],
	[
		'label' => Yii::t('app', 'Reporting'),
		//'template' =>"<li>{link}</li>
	],
];

$this->params['breadcrumbs'] = $breadcrumbs;
$this->title = Yii::t('app', 'Event reporting');
//$this->params['breadcrumbs'][] = [];

$pathReopenDraft = 'events/drafts/event-draft/reopen';
$pathSignDraft = 'events/drafts/event-draft/finalize';
$pathDel = 'events/drafts/event-draft/delete';
$pathUnlockDraft = 'events/drafts/event-draft/unlock-all';
$pathCreate = Yii::$app->getUrlManager()->createUrl(['create', 'evtId' => $evtID]);
//Author : cavesje  Date : 20150617 - Add new button for GeneratePDF
$pathPDF = 'events/drafts/event-draft/view-pdf';

$heading = Yii::t('app', 'Report drafts list');

//var_dump(get_included_files());
//if ( ! class_exists('CustomGridView')) die('There is no hope!'  ); 

?>


<div class="event-draft-index">

	<?php
		//check if flash is set
		if (Yii::$app->session->hasFlash('msg'))
		{
			$msg = Yii::$app->session->getFlash('msg');
			if (strpos($msg,'[err]'))
			{
				//failure
				//get rid of the [s]
				$msg =  str_replace('[err]', '', $msg);
				$msg =  str_replace('\n', '<br/>', $msg);
				
				echo "<div class='alert alert-danger'>$msg</div>";
			}
			else
			{
				//success
				$msg =  str_replace('\n', '<br/>', $msg);
				echo "<div class='alert alert-success'>$msg</div>";
			}
		}
	?>
	
	
	<?php
	/* use this on any view to include the actionmessage logic and widget */
	//include Yii::getAlias('@app') . '\views\shared_parts\message_snippet.php';
	include Yii::getAlias('@app') . '/views/shared_parts/message_snippet.php';
	?>

	<h1><?= Html::encode($this->title) . ': ' . $_event->event_name ?></h1>
	<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

	<p>
		<?=
		Html::a(Yii::t('app', 'Create {modelClass}', [
			  'modelClass' => 'Event Draft',
			]), ['create', 'evtId' => $evtID], ['class' => 'btn btn-success hidden'])
		?>
	</p>

	<?php
	  
	//(start) Author: cavesje   Date: 14.08.2015  - Header and Footer options for PDF format
	//TODO CONSTANT VARIABLE (MAYBE $title_h_l and $title_h_r)
        $title_h_l =  'SyRIO ' . \Yii::t('app', 'Export');
	$title_h_c =  $this->title; //'Custom C';
	$title_h_r = 'Generated' . ': ' . date("D, d-M-Y g:i a T");
	$ourPdfHeader = funDataGridClass::getOurPdfHeader($title_h_l, $title_h_c, $title_h_r);

	$title_f = '';
	$ourPdfFooter = funDataGridClass::getOurPdfFooter($title_f);
	//(end) Author: cavesje   Date: 14.08.2015  - Header and Footer options for PDF format
	
	//(start) Author: cavesje   Date: 14.08.2015  - Merge exportConfig of kartik\grid\GridView with CustomDataGrid
	$exportConfig = funDataGridClass::getExportConfiguration($ourPdfHeader, $ourPdfFooter);
	//(end) Author: cavesje   Date: 14.08.2015  - Merge exportConfig of kartik\grid\GridView with CustomDataGrid

	
	$trashUrl = Yii::$app->urlManager->createAbsoluteUrl(['events/drafts/event-draft/trash', 'evtID'=>$_event->id]);

	//GridView::widget([
	echo CustomGridView::widget([
	  'dataProvider' => $dataProvider,
	  'filterModel' => $searchModel,
	  'pjax' => false,
	  'rowOptions' => function($model) {
		return $model->del_status == app\models\events\EventDeclaration::STATUS_DELETED ? ['class' => 'danger'] : [];
		/* asta cade - pun coloana separata */
		if ($model->ca_status == EventDeclaration::INCIDENT_REPORT_REJECTED) {
		  return ['class' => 'danger'];
		}
		else if ($model->ca_status == EventDeclaration::INCIDENT_REPORT_ACCEPTED) {
		  return ['class' => 'success'];
		}
	  },
	  'columns' => [
		//['class' => 'kartik\grid\SerialColumn'],
		//Column : Status of Report
		[
		  'class' => 'kartik\grid\DataColumn',
		  'attribute' => 'status',
		  'label' => '',
		  'width' => '43px',
		  'vAlign' => 'middle',
		  'hAlign' => 'center',
		  'filterType' => null,
		  'content' => function($model, $key, $index, $column) {
			//$model->refreshFinalizeStatus();
			if ($model->status == EventDeclaration::INCIDENT_REPORT_FINALIZED_PARTIALLY) {
			  return '<i class="fa fa-star-half-o fa-lg" title="PARTIALLY FINALIZED"></i>';
			}
			else if ($model->status == EventDeclaration::INCIDENT_REPORT_FINALIZED) {
			  return '<i class="fa fa-star fa-lg" title="FINALIZED. READY TO BE SIGNED"></i>';
			}
			else if ($model->status == EventDeclaration::INCIDENT_REPORT_SIGNED) {
			  return '<i class="fa fa-check-square-o fa-lg" title="SIGNED. Can be submitted."></i>';
			}
			else {
			  return '<i class="fa fa-star-o fa-lg" title="DRAFT"></i>';
			}
		  },
		],
		//Column : SUBMITTED or NOT to CA
		[
		  'class' => 'kartik\grid\DataColumn',
		  'attribute' => 'ca_status',
		  'label' => '',
		  'width' => '43px',
		  'vAlign' => 'middle',
		  'hAlign' => 'center',
		  'content' => function($model, $key, $index, $column) {
			//$model->refreshFinalizeStatus();
			if ($model->ca_status == EventDeclaration::INCIDENT_REPORT_REPORTED) {
			  $s = '<i class="fa fa-arrow-right fa-sm" title="REPORTED TO THE CA. PENDING FOR ACCEPTANCE."></i>'
				  . '<i class="fa fa-building-o fa-sm" title="REPORTED TO THE CA. PENDING FOR ACCEPTANCE."></i>';
			  return $s;
			}
			else if ($model->ca_status == EventDeclaration::INCIDENT_REPORT_REJECTED) {
			  return '<i class="fa fa-exclamation-triangle fa-lg sy_error" title="REJECTED BY THE CA. CHECK THE REASON THEN RESUBMIT."></i>';
			}
			else if ($model->ca_status == EventDeclaration::INCIDENT_REPORT_ACCEPTED) {
			  return '<i class="fa fa-check fa-lg warning" title="ACCEPTED AND ASSESSED BY THE CA."></i>';
			}
			else {
			  return '<i class="fa fa-ellipsis-h fa-lg" title="NOT SUBMITTED."></i>';
			};
		  },
		],
		//Column : function to expand row
		[
		  'class' => 'kartik\grid\ExpandRowColumn',
		  'width' => '36px;',
		  'value' => function($model, $key, $index, $column) {
			//return GridView::ROW_COLLAPSED;
			return CustomGridView::ROW_COLLAPSED;
		  },
		  'detail' => function($model, $key, $index, $column) {
			return Yii::$app->controller->renderPartial('_draft-details', ['model' => $model]);
		  }
		],
		//Column : Created AT
		[
		  'class' => 'kartik\grid\DataColumn',
		  'attribute' => 'created_at',
		  'vAlign' => 'middle',
		  'width' => '155px'
		],
		//Column : DESCRIPTION
		//start of event types column and description
		'session_desc',
		[
		  'class' => 'kartik\grid\DataColumn',
		  'attribute' => 'Event Type',
		  'vAlign' => 'middle',
		  'content' => function($model, $key, $index, $column) {
			$s = '';
			//http://localhost:8080/SyRIO_DEV_post_brussels/web/index.php/events/drafts/event-draft/view?id=40
			foreach ($model->eventTypes as $key => $type) {
			  $view_section = 'view-section-' . strtolower($type);

			  //(start) Author : cavesje  Date : 20150724 - Open the view of entire SECTION
			  //$root = Yii::$app->getUrlManager()->createAbsoluteUrl(['/events/drafts/event-draft/' . $view_section, 'id' => $model->id, 'v' => 1]);
			  $root = Yii::$app->getUrlManager()->createAbsoluteUrl(['/events/drafts/event-draft/' . $view_section, 'id' => $model->id, 'v' => 0]);
			  //(end) Author : cavesje  Date : 20150724 - Open the view of entire SECTION

			  //set the background
			  $the_section = 'section' . strtoupper($type);
			  $appended = '';
			  $appended_text = '';

			  if ($type == 'G' || $type == 'H') {
				$appended = '<span class="glyphicon glyphicon-ok" style="padding-left:3px;"></span>';
				$appended_text = Yii::t('app', 'Finalized. Ready to be signed.');
				$s .= '<span class="sy_event-span">'
					//(start) Author : cavesje  Date : 20150728 - Link page of details 
					//. '<a href = "#" title = "' . $appended_text . '">'
					. '<a href = "' . $root . '" title = "' . $appended_text . '">'
					//(end) Author : cavesje  Date : 20150728 - Link page of details 
					. $type
					. $appended
					. '</a>'
					. '</span>';
			  }
			  else {
				switch ($model->$the_section->status) {
				  case EventDeclaration::INCIDENT_REPORT_LOCKED:
					$appended = '<i class="fa fa-lock fa-lg" style="padding-left:3px;"></i>';
					$appended_text = Yii::t('app', 'Locked. Someone is currently working on it.');
					break;
				  case EventDeclaration::INCIDENT_REPORT_SIGNED:
					$appended = '<span class="glyphicon glyphicon-check" style="padding-left:3px;"></span>';
					$appended_text = ucfirst(Yii::t('app', 'signed'));
					break;
				  case EventDeclaration::INCIDENT_REPORT_FINALIZED:
					$appended = '<i class="fa fa-star-o fa-large" style="padding-left:3px;"></i>';
					$appended_text = Yii::t('app', 'Finalized. Ready to be signed.');
					break;
				}

				$s .= '<span class="sy_event-span">'
					. '<a href = "' . $root . '" title = "' . $appended_text . '">'
					. $type
					. $appended
					. '</a>'
					. '</span>';
			  }
			}
			return $s;
		  }
		], //end of event types column and description
		//Column : Event Type [A,B,C,D,E,F,G,H,I,J]
		[
		  'class' => 'yii\grid\ActionColumn',
		  'buttons' => [
			//button-update
			'update' => function ($url, $model, $key) {
			  //return $model->status === 'editable' ? Html::a('Update', $url) : '';
			},
			//button-delete
			'delete' => function ($url, $model, $key) {
			  $delete_s = Yii::t('app', 'Delete');

			  
			  if (!\Yii::$app->user->can('incident_draft_delete', ['draft'=>$model]))
			  {
				  return '';
			  }
			  else
			  {
				  return '';

				  if ($model->del_status == EventDeclaration::STATUS_ACTIVE)
				  {
					$msg = Yii::t('app', 'Are you sure you want to delete {data}?', [
						'data'=>$model->session_desc]);
					$msg .= ' ';
					$msg .= Yii::t('app', 'Operation CAN be undone.');
					  
					  return Html::a('<span class=" glyphicon glyphicon-trash" title="' . $delete_s . '"></span>', $url, [
						 'data-method' => 'post',
						  'data-confirm' => $msg,
						  'title' => Yii::t('app', 'Remove')
					  ]);
				  }
			  }

			  return '';
			},
					
			'purge' => function ($url, $model, $key) {
					/* @var $model \app\models\events\drafts\EventDraft */
					
					//return $model->del_status;

					if (!Yii::$app->user->can('incident_draft_purge', ['draft'=>$model]))
					{
						return '';
					}
					$msg = Yii::t('app', 'Are you sure you want to PERMANENTLY delete {data}?', [
						'data'=>$model->session_desc]);
					$msg .= ' ';
					$msg .= Yii::t('app', 'Operation CAN NOT be undone.');
					return $model->del_status == 0 ? 
							Html::a('<i class="glyphicon glyphicon-remove"></i>', $url, [
								'title' => Yii::t('app/commands', 'Purge'),
								'data-confirm' => $msg,
								'data-method' => 'POST']) : '';
				},
			'reactivate' => function ($url, $model, $key) {
					/* @var $model \app\models\events\drafts\EventDraft */

					if (!Yii::$app->user->can('incident_draft_delete', ['draft'=>$model]) || $model->del_status != \app\models\events\EventDeclaration::STATUS_DELETED)
					{
						return '';
					}

					return $model->del_status == 0 ? 
							Html::a('<i class="glyphicon glyphicon-repeat"></i>', $url, [
								'title' => Yii::t('app', 'Undo delete'), 
								'data-method'=>'POST']) : '';
				},
					
			//button-unlock-all						
			'unlock-all' => function ($url, $model, $key) {

$drawIt = false;
foreach (range('a','j') as $letter) {
   $attr = 'is_'.$letter;
   if ($model->$attr && ($letter!=='g' || $letter!=='h')) {
	   $sectionAttr = 'section'.strtoupper($letter);

//$sect = $model->$sectionAttr;
//echo '<pre>';
//echo var_dump($sect);
//echo '</pre>';
//die();
//$status = $sect->status;
//	   if ($status === EventDeclaration::INCIDENT_REPORT_LOCKED) 

	   if ($model->$sectionAttr->status === EventDeclaration::INCIDENT_REPORT_LOCKED) 
	   { $drawIt = true; break; }
   }
}

				  return $drawIt ? Html::a('<i class="fa fa-unlock fa-lg"></i>', $url, [
						'data-confirm' => 'Are you sure?',
						'data-method' => 'post',
						'data-pjax' => '0',
						'title' => 'force unlock'
					  ]) : '';
			},
			//button-reopen												
			'reopen' => function ($url, $model, $key) {
				if (Yii::$app->user->can('incident_draft_reopen', ['draft' => $model])) {
					  return Html::a('<span class="glyphicon glyphicon-hand-down text-warning" style="padding-left:6px;"></span> <small>'
							. Yii::t('app', 'Re-open')
							. '</small>', $url, [
						  'class' => 'btn sy_padding_clear',
						  'data' => [
							'confirm' => Yii::t('app', 'Are you sure you want to re-open this item?'),
						  ],
						]);
				}
			},
			//button-sign						   
			'sign' => function ($url, $model, $key) {
			  if (Yii::$app->user->can('incident_draft_sign', ['draft' => $model])) {
				return Html::a('<span class="glyphicon glyphicon-check text-success" style="padding-left:6px;"></span> <small>'
					  . Yii::t('app', 'Sign')
					  . '</small>', $url, [
											'class' => 'btn sy_padding_clear',
											'data' => [
												'confirm' => Yii::t('app', 'You are about to sign the incident report. This is the last step before submitting to the CA.'),
											]
									]);
			  }
			},
			//button-submit-confirmation						   
			'submit-confirmation' => function ($url, $model, $key) {
			  if (Yii::$app->user->can('incident_submit', ['draft'=>$model])) {
				  return $model->status == EventDeclaration::INCIDENT_REPORT_SIGNED ?
					  Html::a(Yii::t('app', '<span class="glyphicon glyphicon-send text-success" style="padding-left:6px;"></span> <small>' . Yii::t('app', 'Submit') .  '</small>'), $url, [
						'class' => 'btn sy_padding_clear',
						  'title' => 'Sends the current draft to the Competent Authority'
					  ]) : '';
			  }
			},
			//(start) Author : cavesje  Date : 20150617 - Add new button for GeneratePDF
			//button-PDF						  
			'pdf' => function ($url, $model, $key) {
                            $s = '<span class="glyphicon glyphicon-print text-success" style="padding-left:6px;"></span> <small>PDF</small>';
			  return Html::a($s, $url, [
						'class' => 'btn sy_padding_clear',
					  ]);
			}
			//(end) Author : cavesje  Date : 20150617 - Add new button for GeneratePDF
		  ],
		  'template' => '{view} {purge} {reactivate} {delete} {pdf} {unlock-all} {reopen} {sign} {submit-confirmation} '
		],
	  ],
	  //END COLUMNS
		  
	  // set your toolbar
	  'toolbar' => [
		['content' =>
		  Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create', 'evtId' => $evtID], [
			'class' => 'btn btn-success',
			'title' => Yii::t('app', 'Create {modelClass}', [
			  'modelClass' => 'Event Draft',
			]),
		  ]),
		  //Html::button('<i class="glyphicon glyphicon-plus"></i>', ['type'=>'button', 'title'=>Yii::t('kvgrid', 'Add Book'), 'class'=>'btn btn-success', 'onclick'=>'alert("This will launch the book creation form.\n\nDisabled for this demo!");']) . ' '.

		  /*
			Html::button('<i class="glyphicon glyphicon-plus"></i>', [
			'type'=>'button',
			'title'=>Yii::t('app', 'Create'), 'class'=>'btn btn-success',
			'onclick'=>'alert("This will launch the book creation form.\n\nDisabled for this demo!");']) . ' '.
		   *
		   */
		  Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['grid-demo'], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => Yii::t('app', 'Reset Grid')])
		],
		'{export}',
		'{toggleData}',
	  ],
					  
          // set export properties
          'export' => [
            'icon' => 'share-square-o',
            'encoding' => 'utf-8',
            'fontAwesome' => true,
            'target' => CustomGridView::TARGET_SELF,   //target: string, the target for submitting the export form, which will trigger the download of the exported file. 
            'showConfirmAlert' => TRUE,
            'header' => '<li role="presentation" class="dropdown-header">' . \Yii::t('app', 'Grid Export') . '</li>'
            //'header' => 'Export Page Data',
          ],
			  
	  //(start) Author: cavesje   Date: 14.08.2015  - extra configuration PDF export
	  'exportConfig'	 => $exportConfig,
	  //(end) Author: cavesje   Date: 14.08.2015  - extra configuration PDF export
			  
	  'panel' => [
		//'type' => GridView::TYPE_PRIMARY,
		'type' => CustomGridView::TYPE_PRIMARY,
		'heading' => $heading,
		'before' => '<div class="col-sm-2">'
				. Html::a('<span class="glyphicon glyphicon-trash"></span> '
						. '<span class="badge">' . \app\models\events\drafts\EventDraft::getNumberOfDeleted($_event->id) . '</span>',
						$trashUrl
						, [
							'class' => 'btn btn-danger',
							'title' => Yii::t('app', 'Deleted drafts. Click to manage.')])
				. '</div>',
	  ],

	  'persistResize' => false,
	]);

  ?>

</div>
