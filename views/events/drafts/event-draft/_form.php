<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\Accordion;
use app\models\User;


//get user's details anf fill in the model
$users = new User();
$user=$users->findOne($model->created_by);
if (is_object($user))
{
    $uname = $user->name;
}
else
{
    $uname = 'user_id ' . $model->created_by . 'not found';
}

$user=$users->findOne($model->modified_by);
if (is_object($user))
{
    $modifname = $user->name;
}
else
{
    $modifname = 'user_id ' . $model->modified_by . ' not found';
}



/* @var $this yii\web\View */
/* @var $model app\models\rebuilt\EventDraft */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="event-draft-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-lg-9">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h4>Draft name</h4>            
                </div>
                <div class="panel-body">
                    <?= $form->field($model, 'session_desc')->textInput(['maxlength' => 45])
                            ->hint(Yii::t('app', 'Please provide a short description of this draft.')) ?>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <small>
                        <?php
                            if ($model->id != 0)
                            {
                        ?>

                        <p>
                            <?= ucfirst(\Yii::t('app', 'created at')) . ': ' . $model->created_at ?>
                            <br/>
                            <?= \Yii::t('app', 'by') . ': ' . $uname ?>
                        </p>
                        <p>
                            <?= ucfirst(\Yii::t('app', 'modified at')) . ': ' . $model->modified_at ?>
                            <br/>
                            <?= \Yii::t('app', 'by') . ': ' . $modifname ?>
                        </p>

                        
                        <?php
                            }
                            else
                            {
                        ?>
                        <?php
                            }
                        ?>
                    </small>
                </div>
            </div>
        </div>
    </div>
    
    
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h4><?= Yii::t('app/crf', 'Event categorization') ?></h4>
            <small><?= Yii::t('app/crf', 'According to Annex IX of Directive 2013/30/EU') ?></small>
        </div>
        <div class="panel-body">
            <p><strong><?= Yii::t('app/crf', 'What type of event is being reported?') ?></strong></p>
            <span class="text-muted"><em><?= Yii::t('app', 'More than one option might be chosen') ?></em></span>
            
            <div>
                <h4 style="padding-top: 16px">
                    <?= Html::activeCheckbox($model, 'is_a', ['value'=>1, 'uncheck'=>0, 'label'=>'']) ?>
                    <strong style="padding-left: 10px; padding-right: 10px">A</strong> 
                    <?= Yii::t('app/crf', 'Unintended release of oil, gas or other hazardous substances, whether or not ignited') ?>
                </h4>
                <?php

                    $selGuidance1 = '<ol>';
                    $selGuidance1 .= '   <li>';
                    $selGuidance1 .= \Yii::t('app/crf', 'Any unintentional release of ignited gas or oil on or from an offshore installation;');
                    $selGuidance1 .= '   </li>';
                    $selGuidance1 .= '   <li>';
                    $selGuidance1 .=        \Yii::t('app/crf', 'The unintentional release on or from an offshore installation of:');
                    $selGuidance1 .= '       <ol type="a">';
                    $selGuidance1 .= '          <li>';
                    $selGuidance1 .=                \Yii::t('app/crf', 'not ignited natural gas or evaporated associated gas if mass released >= 1 kg');
                    $selGuidance1 .= '          </li>';
                    $selGuidance1 .= '          <li>';
                    $selGuidance1 .=                \Yii::t('app/crf', 'not ignited liquid of petroleum hydrocarbon if mass released >= 60 kg');
                    $selGuidance1 .= '          </li>';
                    $selGuidance1 .= '       </ol>';
                    $selGuidance1 .= '   </li>';
                    $selGuidance1 .= '   <li>';
                    $selGuidance1 .= \Yii::t('app/crf', 'The unintentional	release	or escape of any hazardous substance, '
    . 'for which the major accident risk has been assessed in the report on major hazards, '
    . 'on or from an offshore installation, including wells and returns of drilling additives.');
                    $selGuidance1 .= '   </li>';
                    $selGuidance1 .= '</ol>';


                    echo Accordion::widget([
                        'items' => [
                            [
                                'header' => Yii::t('app', 'Selection guidance'),
                                'headerOptions' => ['tag' => 'h3'],
                                'content' => $selGuidance1,
                                'options' => ['tag' => 'div'],
                            ],
                        ],
                        'options' => ['tag' => 'div'],
                        'itemOptions' => ['tag' => 'div'],
                        'headerOptions' => ['tag' => 'h3'],
                        'clientOptions' => ['collapsible' => true, 'active'=>false],
                    ]);                        
                ?>            
            </div> <!-- end of A block -->


            <div>
                <h4 style="padding-top: 16px">
                    <?= Html::activeCheckbox($model, 'is_b', ['value'=>1, 'uncheck'=>0, 'label'=>'']) ?>
                    <strong style="padding-left: 10px; padding-right: 10px">B</strong> 
                    <?= \Yii::t('app/crf', 'Loss of well control requiring actuation of well control equipment, or failure of a well barrier requiring its replacement or repair') ?>
                </h4>
                
                <?php

                    $selGuidance2 = '<ol>';
                    $selGuidance2 .= '   <li>';
                    $selGuidance2 .=        \Yii::t('app/crf', 'Any blowout, regardless of the duration;');
                    $selGuidance2 .= '   </li>';
                    $selGuidance2 .= '   <li>';
                    $selGuidance2 .=        \Yii::t('app/crf', 'The coming into operation of a blowout prevention or diverter system to control flow of well-fluids;');
                    $selGuidance2 .= '   </li>';
                    $selGuidance2 .= '   <li>';
                    $selGuidance2 .=        \Yii::t('app/crf', 'The mechanical failure of any part of a well, whose purpose is to prevent or limit the effect of the unintentional release of fluids from a well or a reservoir being drawn on by a well, or whose failure would cause or contribute to such a release');
                    $selGuidance2 .= '   </li>';
                    $selGuidance2 .= '   <li>';
                    $selGuidance2 .=        \Yii::t('app/crf', 'The taking of precautionary measures additional to any already contained in the original drilling programme where a planned minimum separation distance between adjacent wells was not maintained.');
                    $selGuidance2 .= '   </li>';
                    $selGuidance2 .= '</ol>';


                    echo Accordion::widget([
                        'items' => [
                            [
                                'header' => Yii::t('app', 'Selection guidance'),
                                'headerOptions' => ['tag' => 'h3'],
                                'content' => $selGuidance2,
                                'options' => ['tag' => 'div'],
                            ],
                        ],
                        'options' => ['tag' => 'div'],
                        'itemOptions' => ['tag' => 'div'],
                        'headerOptions' => ['tag' => 'h3'],
                        'clientOptions' => ['collapsible' => true, 'active'=>false],
                    ]);                        
                ?>            
            </div> <!-- end of B block -->

            <div>
                <h4 style="padding-top: 16px">
                    <?= Html::activeCheckbox($model, 'is_c', ['value'=>1, 'uncheck'=>0, 'label'=>'']) ?>
                    <strong style="padding-left: 10px; padding-right: 10px">C</strong> 
                    <?= \Yii::t('app/crf', 'Failure of a safety and environmental critical element') ?>
                </h4>
                
                <?php

                    $selGuidance3 = '<ol>';
                    $selGuidance3 .= '   <li>';
                    $selGuidance3 .=        \Yii::t('app/crf', 'Any loss or non-availability of a SECE requiring immediate remedial action.');
                    $selGuidance3 .= '   </li>';
                    $selGuidance3 .= '</ol>';


                    echo Accordion::widget([
                        'items' => [
                            [
                                'header' => Yii::t('app', 'Selection guidance'),
                                'headerOptions' => ['tag' => 'h3'],
                                'content' => $selGuidance3,
                                'options' => ['tag' => 'div'],
                            ],
                        ],
                        'options' => ['tag' => 'div'],
                        'itemOptions' => ['tag' => 'div'],
                        'headerOptions' => ['tag' => 'h3'],
                        'clientOptions' => ['collapsible' => true, 'active'=>false],
                    ]);                        
                ?>            
            </div> <!-- end of C block -->

            <div>
                <h4 style="padding-top: 16px">
                    <?= Html::activeCheckbox($model, 'is_d', ['value'=>1, 'uncheck'=>0, 'label'=>'']) ?>
                    <strong style="padding-left: 10px; padding-right: 10px">D</strong> 
                    <?= ucfirst(\Yii::t('app/crf', 'significant loss of structural integrity, or loss of protection against the effects of fire or explosion, or loss of station keeping in relation to a mobile installation')) ?>
                </h4>
                <?php

                    $selGuidance3 = '<ol class="list-unstyled">';
                    $selGuidance3 .= '   <li>';
                    $selGuidance3 .=        \Yii::t('app/crf', 'Any detected condition that reduces the designed structural integrity of the installation, including stability, buoyancy and station keeping, to the extent that it requires '
                            . 'immediate remedial action.');
                    $selGuidance3 .= '   </li>';
                    $selGuidance3 .= '</ol>';


                    echo Accordion::widget([
                        'items' => [
                            [
                                'header' => Yii::t('app', 'Selection guidance'),
                                'headerOptions' => ['tag' => 'h3'],
                                'content' => $selGuidance3,
                                'options' => ['tag' => 'div'],
                            ],
                        ],
                        'options' => ['tag' => 'div'],
                        'itemOptions' => ['tag' => 'div'],
                        'headerOptions' => ['tag' => 'h3'],
                        'clientOptions' => ['collapsible' => true, 'active'=>false],
                    ]);                        
                ?>            
            </div> <!-- end of D block -->            
        
            <div>
                <h4 style="padding-top: 16px">
                    <?= Html::activeCheckbox($model, 'is_e', ['value'=>1, 'uncheck'=>0, 'label'=>'']) ?>
                    <strong style="padding-left: 10px; padding-right: 10px">E</strong> 
                    <?= \Yii::t('app/crf', 'Vessels on collision course and actual vessel collisions with an offshore installation') ?>
                </h4>
                <?php

                    $selGuidance = '<ol class="list-unstyled">';
                    $selGuidance .= '   <li>';
                    $selGuidance .=        \Yii::t('app/crf', 'Any collision, or potential collision, between a vessel and an offshore installation which has, or would have, enough energy to cause sufficient damage to the installation and/or vessel, to jeopardise the overall structural or process integrity.');
                    $selGuidance .= '   </li>';
                    $selGuidance .= '</ol>';


                    echo Accordion::widget([
                        'items' => [
                            [
                                'header' => Yii::t('app', 'Selection guidance'),
                                'headerOptions' => ['tag' => 'h3'],
                                'content' => $selGuidance,
                                'options' => ['tag' => 'div'],
                            ],
                        ],
                        'options' => ['tag' => 'div'],
                        'itemOptions' => ['tag' => 'div'],
                        'headerOptions' => ['tag' => 'h3'],
                        'clientOptions' => ['collapsible' => true, 'active'=>false],
                    ]);                        
                ?>            
            </div> <!-- end of E block -->            

            <div>
                <h4 style="padding-top: 16px">
                    <?= Html::activeCheckbox($model, 'is_f', ['value'=>1, 'uncheck'=>0, 'label'=>'']) ?>
                    <strong style="padding-left: 10px; padding-right: 10px">F</strong> 
                    <?= \Yii::t('app/crf', 'Helicopter accidents, on or near offshore installations') ?>
                </h4>
                <?php

                    $selGuidance = '<ol class="list-unstyled">';
                    $selGuidance .= '   <li>';
                    $selGuidance .=        \Yii::t('app/crf', 'Any collision, or potential collision, between a helicopter and an offshore installation.');
                    $selGuidance .= '   </li>';
                    $selGuidance .= '</ol>';


                    echo Accordion::widget([
                        'items' => [
                            [
                                'header' => Yii::t('app', 'Selection guidance'),
                                'headerOptions' => ['tag' => 'h3'],
                                'content' => $selGuidance,
                                'options' => ['tag' => 'div'],
                            ],
                        ],
                        'options' => ['tag' => 'div'],
                        'itemOptions' => ['tag' => 'div'],
                        'headerOptions' => ['tag' => 'h3'],
                        'clientOptions' => ['collapsible' => true, 'active'=>false],
                    ]);                        
                ?>            
            </div> <!-- end of F block -->            
            
            <div>
                <h4 style="padding-top: 16px">
                    <?= Html::activeCheckbox($model, 'is_g', ['value'=>1, 'uncheck'=>0, 'label'=>'']) ?>
                    <strong style="padding-left: 10px; padding-right: 10px">G</strong> 
                    <?= \Yii::t('app/crf', 'Any fatal accident to be reported under the requirements of Directive 92/91/EEC') ?>
                </h4>
            </div> <!-- end of G block -->            
            
            <div>
                <h4 style="padding-top: 16px">
                    <?= Html::activeCheckbox($model, 'is_h', ['value'=>1, 'uncheck'=>0, 'label'=>'']) ?>
                    <strong style="padding-left: 10px; padding-right: 10px">H</strong> 
                    <?= \Yii::t('app/crf', 'Any serious injuries to five or more persons in the same accident to be reported under the requirements of Directive 92/91/EEC') ?>
                </h4>
            </div> <!-- end of H block -->            
            
            <div>
                <h4 style="padding-top: 16px">
                    <?= Html::activeCheckbox($model, 'is_i', ['value'=>1, 'uncheck'=>0, 'label'=>'']) ?>
                    <strong style="padding-left: 10px; padding-right: 10px">I</strong> 
                    <?= \Yii::t('app/crf', 'Any evacuation of personnel') ?>
                </h4>
                <?php

                    $selGuidance = '<ol class="list-unstyled">';
                    $selGuidance .= '   <li>';
                    $selGuidance .=        \Yii::t('app/crf', 'Any unplanned emergency evacuation of part of or all personnel as a result of, or where there is a significant risk of a major accident.');
                    $selGuidance .= '   </li>';
                    $selGuidance .= '</ol>';


                    echo Accordion::widget([
                        'items' => [
                            [
                                'header' => Yii::t('app', 'Selection guidance'),
                                'headerOptions' => ['tag' => 'h3'],
                                'content' => $selGuidance,
                                'options' => ['tag' => 'div'],
                            ],
                        ],
                        'options' => ['tag' => 'div'],
                        'itemOptions' => ['tag' => 'div'],
                        'headerOptions' => ['tag' => 'h3'],
                        'clientOptions' => ['collapsible' => true, 'active'=>false],
                    ]);                        
                ?>            
            </div> <!-- end of I block -->            

            <div>
                <h4 style="padding-top: 16px">
                    <?= Html::activeCheckbox($model, 'is_j', ['value'=>1, 'uncheck'=>0, 'label'=>'']) ?>
                    <strong style="padding-left: 10px; padding-right: 10px">J</strong> 
                    <?= \Yii::t('app/crf', 'A major environmental incident') ?>
                </h4>
                <?php

                    $selGuidance = '<ol class="list-unstyled">';
                    $selGuidance .= '   <li>';
                    $selGuidance .=        \Yii::t('app/crf', 'Any major environmental incident as defined in Article 2.1.d and Article 2.37 of Directive 2013/30/EU.');
                    $selGuidance .= '   </li>';
                    $selGuidance .= '</ol>';


                    echo Accordion::widget([
                        'items' => [
                            [
                                'header' => Yii::t('app', 'Selection guidance'),
                                'headerOptions' => ['tag' => 'h3'],
                                'content' => $selGuidance,
                                'options' => ['tag' => 'div'],
                            ],
                        ],
                        'options' => ['tag' => 'div'],
                        'itemOptions' => ['tag' => 'div'],
                        'headerOptions' => ['tag' => 'h3'],
                        'clientOptions' => ['collapsible' => true, 'active'=>false],
                    ]);                        
                ?>            
            </div> <!-- end of J block -->            
            
            
        </div>
    </div>
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), [
            'id'=>'btnSubmit',
            'onclick' => 'proceed = 1;',
            'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?= $form->field($model, 'status')->hiddenInput()->label('') ?>
    
    <?php ActiveForm::end(); ?>

</div>
