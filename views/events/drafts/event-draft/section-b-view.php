<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\web\JqueryAsset;

use app\models\User;
use app\widgets\ValidationReportWidget;

/* @var $this yii\web\View */
/* @var $model app\models\rebuilt\EventDraft */
/* @var $v subsection index [0-4] */

$modelEvent = $model->event;
$this->title = \Yii::t('app/crf', 'Section {evt}', ['evt'=>'B']);

echo $this->render('/shared_parts/_breadcrumbs', [
  'title_page' => $this->title, 
  'model' => $model, 
  'modelEvent' => $modelEvent, 
  'module_name' => 'EVENT_SECTIONS'
]);
    
//check if flash is set
if (Yii::$app->session->hasFlash('msg'))
{
  $msg = Yii::$app->session->getFlash('msg');
  if (strpos($msg,'[err]'))
  {
    //failure
    //get rid of the [s]
    $msg =  str_replace('[err]', '', $msg);
    $msg =  str_replace('\n', '<br/>', $msg);

    echo "<div class='alert alert-danger'>$msg</div>";
  }
  else
  {
    //success
    $msg =  str_replace('\n', '<br/>', $msg);
    echo "<div class='alert alert-success'>$msg</div>";
  }
}
    
//get user's details anf fill in the model
$users = new User();
$user=$users->findOne($model->created_by);
if (is_object($user))
{
    $uname = $user->name;
}
else
{
    $uname = 'user_id ' . $model->created_by . 'not found';
}

$user=$users->findOne($model->modified_by);
if (is_object($user))
{
    $modifname = $user->name;
}
else
{
    $modifname = 'user_id ' . $model->modified_by . ' not found';
}


if ($model->hasErrors())
{
    $evt = 'B';
    echo ValidationReportWidget::widget([
        'title' => \Yii::t('app','Finalize Section {evt}', ['evt'=>$evt]),
        'errors_message' => \Yii::t('app','Section {evt} could not be finalized! Please review the information below and try again.', ['evt'=>$evt]),
        'errors'=>$model->errors,
    ]);
}                            
                
?>
<div class="event-draft-view">
    <?php
        $sa = $model->sectionB;
    
        $pathStr = 'events/drafts/event-draft/view-section-b';
        $path0=Yii::$app->urlManager->createUrl([$pathStr, 'id'=>$model->id, 'v'=>0]);
        $path1=Yii::$app->urlManager->createUrl([$pathStr, 'id'=>$model->id, 'v'=>1]);
        $path2=Yii::$app->urlManager->createUrl([$pathStr, 'id'=>$model->id, 'v'=>2]);
        $path3=Yii::$app->urlManager->createUrl([$pathStr, 'id'=>$model->id, 'v'=>3]);
        $path4=Yii::$app->urlManager->createUrl([$pathStr, 'id'=>$model->id, 'v'=>4]);

        $v==0 ? $a_def0 = '"list-group-item active"' : $a_def0 ='"list-group-item active"';
        $v==1 ? $a_def1 = '"list-group-item disabled"' : $a_def1 ='"list-group-item"';
        $v==2 ? $a_def2 = '"list-group-item disabled"' : $a_def2 ='"list-group-item"';
        $v==3 ? $a_def3 = '"list-group-item disabled"' : $a_def3 ='"list-group-item"';
        $v==4 ? $a_def4 = '"list-group-item disabled"' : $a_def4 ='"list-group-item"';
    ?>
    
    
    <div class="row">
        <div class="col-lg-3 col-md-4 col-sm-12">
            
            <div class="list-group list-unstyled">
                <div class='dropdown list-group-item active'>
                    <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        <?= Yii::t('app/crf', 'Section {evt}', ['evt'=>'B']) ?>
                        <span class="caret"></span>
                    </button>                            
                    <ul class='dropdown-menu' aria-labelledby="dropdownMenu1">
                        <li class='dropdown-header'>
                            <?= Yii::t('app', 'Other sections in this report') ?>
                        </li>
                        <?php
                            foreach(range('a', 'j') as $letter)
                            {
                                $is = 'is_'.$letter;
                                if ($model->$is == 1 && ($letter != 'g' && $letter != 'h'))
                                {
                                    $ssection = 'section' . strtoupper($letter);
                                    $cSection = $model->$ssection;
                                    $ssec_name = Yii::t('app/crf', 'Section {evt}', ['evt'=>  strtoupper($letter)]);
                                    $pathStr = 'events/drafts/event-draft/view-section-' . $letter;
                                    $url = Yii::$app->urlManager->createUrl([$pathStr, 'id'=>$model->id, 'v'=>0]);
                                    echo $ssection;
                                    //die();
                                    switch ($cSection->status) {
                                        case app\models\events\EventDeclaration::INCIDENT_REPORT_DRAFT:
                                            $stat_text = \Yii::t('app', 'draft');
                                            $stat_css = 'label-info';
                                            break;
                                        case app\models\events\EventDeclaration::INCIDENT_REPORT_LOCKED:
                                            $stat_text = \Yii::t('app', 'LOCKED');
                                            $stat_css = 'label-danger';
                                            $user = app\models\User::find(['id'=>$cSection->locked_by])->one();
                                            if (isset($user))
                                            {
                                                $stat_details = \Yii::t('app', 'by {evt1} since {evt2}', [
                                                    'evt1' => $user->full_name,
                                                    'evt2' => '<br/>' . date('Y-m-d H:i:s', $cSection->locked_at)
                                                ]);
                                            }
                                            break;
                                        case app\models\events\EventDeclaration::INCIDENT_REPORT_FINALIZED:
                                            $stat_text = strtoupper(\Yii::t('app', 'finalized'));
                                            $stat_css = 'label-success';
                                            break;
                                        case app\models\events\EventDeclaration::INCIDENT_REPORT_SIGNED:
                                            $stat_text = strtoupper(\Yii::t('app', 'signed'));
                                            $stat_css = 'label-success';
                                            break;
                                    }
                                    
                        ?>
                        <li>
                                    <?= yii\helpers\Html::a($ssec_name . "<span class='label $stat_css pull-right' style='margin-right:6px; margin-top:3px;'>$stat_text</span>", $url) ?>
                        </li>
                        <?php
                                }
                            }
                        ?>
                    </ul>
                </div>
                <!--<a class="list-group-item active">-->
                <a href= <?= $path0 ?> class= <?= $a_def0 ?> style='z-index:0'>
                    <h4 class="list-group-item-heading">
                    </h4>
                    <p class="list-group-item-text">
                        <?= Yii::t('app/crf', 'Loss of well control requiring actuation of well control equipment') ?>,
                        <?= Yii::t('app/crf', 'or failure of a well barrier requiring its replacement or repair') ?>
                        <br/><br/>
                        <small><i><?= Yii::t('app/crf', 'Click the sections below for drafting or narrowing down the displayed information.') ?></i></small>
                    </p>
                </a>
                <a href= <?= $path1 ?> class= <?= $a_def1 ?>>
                    <h4 class="list-group-item-heading">
                        <?= Yii::t('app/crf', 'Section') ?> B.1.
                    </h4>
                    <p class="list-group-item-text">
                        <?= Yii::t('app/crf', 'General information') ?>
                    </p>
                </a>
                <a href= <?= $path2 ?> class= <?= $a_def2 ?>>
                    <h4 class="list-group-item-heading">
                        <?= Yii::t('app/crf', 'Section') ?> B.2.
                    </h4>
                    <p class="list-group-item-text">
                        <?= Yii::t('app/crf', 'Description of circumstances, consequences of event and emergency response') ?> 
                    </p>
                </a>                
                <a href= <?= $path3 ?> class= <?= $a_def3 ?>>
                    <h4 class="list-group-item-heading">
                        <?= Yii::t('app/crf', 'Section') ?> B.3.
                    </h4>
                    <p class="list-group-item-text">
                        <?= Yii::t('app/crf', 'Preliminary direct and underlying causes.') ?> 
                        <p><small><?= Yii::t('app/crf', '(within 10 days of the event)') ?> </small></p>
                    </p>
                </a>                
                <a href= <?= $path4 ?> class= <?= $a_def4 ?>>
                    <h4 class="list-group-item-heading">
                        <?= Yii::t('app/crf', 'Section') ?> B.4.
                    </h4>
                    <p class="list-group-item-text">
                        <?= Yii::t('app/crf', 'Initial lessons learned and preliminary recommendations to prevent recurrence of similar events') ?>
                    </p>
                    <p><small><?= Yii::t('app/crf', '(within 10 days of the event)') ?> </small></p>
                </a>                
                
            </div> <!-- end nav div -->
        </div>
        
        
        
        
        
        
        
        
        <div class="col-lg-9 col-md-8 col-sm-12" id="subsection-content">
            <?php
                $sb = $model->sectionB;
            
              if ($sb->status == \app\models\events\EventDeclaration::INCIDENT_REPORT_LOCKED)
              {
                  $unlock = '';
                  if (\Yii::$app->user->can('op_raporteur') || \Yii::$app->user->can('inst_rapporteur')) {
                      $url = Yii::$app->urlManager->createUrl(['//events/drafts/event-draft/unlock-all', 'id'=>$model->id]);
                      $unlock = '<p>' . yii\helpers\Html::a('<i class="fa fa-unlock"></i>' . ' ' . Yii::t('app', 'Unlock'), $url, [
                          'class'=>'btn btn-danger',
                          'data-method' => 'post',
                          'data-confirm' => \Yii::t('app', 'Force unlock?')
                      ]) . '</p>';
                  }
                  
                  
                  $s = '<div class="alert alert-danger col-lg-12" role="alert">'
                          . '<p>' . \Yii::t('app', 'Opened in read-only mode.') . '</p>'
                          . '<p>' . \Yii::t('app', 'Locked by {evt1} since {evt2}.', [
                              'evt1'=>User::findOne($sb->locked_by)->full_name,
                              'evt2'=>date('Y-m-d H:i:s', $sb->locked_at)
                          ])
                          . '</p>'
                          . $unlock
                          . '</div>';
                  echo $s;
              }
              else if ($sb->status == \app\models\events\EventDeclaration::INCIDENT_REPORT_FINALIZED)
              {
                    //check if can reopen
                    if (Yii::$app->user->can('incident_draft_section_reopen_operators', ['section' => $sb])) {
                        $url = Yii::$app->urlManager->createUrl(['//events/drafts/event-draft/reopen-section', 'id'=>$model->id, 'section'=>'b']);
                        $reopen = '<p>' . yii\helpers\Html::a('<i class="fa fa-hand-o-up"></i> ' . Yii::t('app', 'reopen'), $url, [
                          'class'=>'btn btn-success',
                          'data-method' => 'post',
                          'data-confirm' => \Yii::t('app', 'Reopen section?')
                        ]) . '</p>';
                    } else {
                        $reopen = '';
                    }
                  
                  $s = '<div class="alert alert-success col-lg-12" role="alert">'
                          . '<p>Finalized</p>'
                          . $reopen
                          . '</div>';
                  echo $s;                
              }
                
                if ($v==0)
                {
                    $sb1 = $sb->b1;
                    $sb2 = $sb->b2;
                    $sb3 = $sb->b3;
                    $sb4 = $sb->b4;
                    
                    echo $this->renderAjax('/events/drafts/sections/subsections/b1/review', ['event_id' => $sb->event_classification_id, 'model'=>$sb1]);
                    echo $this->renderAjax('/events/drafts/sections/subsections/b2/review', ['model'=>$sb2]);
                    echo $this->renderAjax('/events/drafts/sections/subsections/b3/review', ['model'=>$sb3]);
                    echo $this->renderAjax('/events/drafts/sections/subsections/b4/review', ['model'=>$sb4]);
                }
                else if ($v==1)
                {
                    $sb1 = $sb->b1;
                    //echo print_r($sa1);
                    
                    echo $this->renderAjax('/events/drafts/sections/subsections/b1/view', ['model'=>$sb1]);
                }
                else if ($v==2)
                {
                    $sb2 = $sb->b2;
                    //echo print_r($sa2);
                    //http://localhost:8080/SyRIO_DEV/web/index.php?r=events/drafts/sections/subsections/b2
                    echo $this->renderAjax('/events/drafts/sections/subsections/b2/view', [ 'model'=>$sb2 ]);
                }
                else if ($v==3)
                {
                    $sb3 = $sb->b3;
                    //http://localhost:8080/SyRIO_DEV/web/index.php?r=events/drafts/sections/subsections/b3
                    echo $this->renderAjax('/events/drafts/sections/subsections/b3/view', [ 'model'=>$sb3 ]);
                }
                else if ($v==4)
                {
                    $sb4 = $sb->b4;
                    //http://localhost:8080/SyRIO_DEV/web/index.php?r=events/drafts/sections/subsections/b3
                    echo $this->renderAjax('/events/drafts/sections/subsections/b4/view', [ 'model'=>$sb4 ]);
                }
                else
                {
                    echo 'no subsection';
                }
            ?>
        </div>
    </div>
    
<?php
    //$this->registerCssFile('css/Section1.css');    
    //$this->registerJsFile('js/Section1.js');
    $this->registerJsFile('/SyRIO_DEV/web/js/abc.js', ['depends' => [JqueryAsset::className()], 'position' => \yii\web\View::POS_END]);
    //$this->registerJsFile('js/Section1.js');
    //['depends' => [JqueryAsset::className()]]
?>    
    
</div>
