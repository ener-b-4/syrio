<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use app\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\rebuilt\EventDraft */

$modelEvent = $model->event;
$this->title = \Yii::t('app/crf', 'Section {evt}', ['evt'=>'G']);

echo $this->render('/shared_parts/_breadcrumbs', [
  'title_page' => $this->title, 
  'model' => $model, 
  'modelEvent' => $modelEvent, 
  'module_name' => 'EVENT_SECTIONS'
]);

//get user's details anf fill in the model
$users = new User();
$user=$users->findOne($model->created_by);
if (is_object($user))
{
    $uname = $user->name;
}
else
{
    $uname = 'user_id ' . $model->created_by . 'not found';
}

$user=$users->findOne($model->modified_by);
if (is_object($user))
{
    $modifname = $user->name;
}
else
{
    $modifname = 'user_id ' . $model->modified_by . ' not found';
}

?>


<div class="event-draft-view">

  <div class="alert alert-info" role="alert">
    <h3>
      <?= Yii::t('app/crf', 'Section G shall be reported under the requirements of Directive 92/91/EEC') ?>
    </h3>
  </div> 
</div>
