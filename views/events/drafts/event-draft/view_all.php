<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;

?>

<h4>
    <?= Html::checkbox('isA', $model->is_a, ['disabled'=>true]) ?>
    <strong style="padding-left: 10px; padding-right: 10px">A</strong> 
    <?= Yii::t('app/crf', 'Unintended release of oil, gas or other hazardous substances, whether or not ignited') ?>
</h4>
<div class="col-lg-12 sy_pad_bottom_36">
    <!-- the description goes here -->
    <?php

        $selGuidance = '<ol>';
        $selGuidance .= '   <li>';
        $selGuidance .= \Yii::t('app/crf', 'Any unintentional release of ignited gas or oil on or from an offshore installation;');
        $selGuidance .= '   </li>';
        $selGuidance .= '   <li>';
        $selGuidance .=        \Yii::t('app/crf', 'The unintentional release on or from an offshore installation of:');
        $selGuidance .= '       <ol type="a">';
        $selGuidance .= '          <li>';
        $selGuidance .=                \Yii::t('app/crf', 'not ignited natural gas or evaporated associated gas if mass released >= 1 kg');
        $selGuidance .= '          </li>';
        $selGuidance .= '          <li>';
        $selGuidance .=                \Yii::t('app/crf', 'not ignited liquid of petroleum hydrocarbon if mass released >= 60 kg');
        $selGuidance .= '          </li>';
        $selGuidance .= '       </ol>';
        $selGuidance .= '   </li>';
        $selGuidance .= '   <li>';
        $selGuidance .= \Yii::t('app/crf', 'The unintentional	release	or escape of any hazardous substance, '
    . 'for which the major accident risk has been assessed in the report on major hazards, '
    . 'on or from an offshore installation, including wells and returns of drilling additives.');
        $selGuidance .= '   </li>';
        $selGuidance .= '</ol>';
    ?>
    <?= $selGuidance ?>
</div>

<h4>
<?= Html::checkbox('isB', $model->is_b, ['disabled'=>true]) ?>
<strong style="padding-left: 10px; padding-right: 10px">B</strong> 
<?= \Yii::t('app/crf', 'Loss of well control requiring actuation of well control equipment, or failure of a well barrier requiring its replacement or repair') ?>
</h4>

<div class="col-lg-12 sy_pad_bottom_36">
<?php

    $selGuidance = '<ol>';
    $selGuidance .= '   <li>';
    $selGuidance .=        \Yii::t('app/crf', 'Any blowout, regardless of the duration;');
    $selGuidance .= '   </li>';
    $selGuidance .= '   <li>';
    $selGuidance .=        \Yii::t('app/crf', 'The coming into operation of a blowout prevention or diverter system to control flow of well-fluids;');
    $selGuidance .= '   </li>';
    $selGuidance .= '   <li>';
    $selGuidance .=        \Yii::t('app/crf', 'The mechanical failure of any part of a well, whose purpose is to prevent or limit the effect of the unintentional release of fluids from a well or a reservoir being drawn on by a well, or whose failure would cause or contribute to such a release');
    $selGuidance .= '   </li>';
    $selGuidance .= '   <li>';
    $selGuidance .=        \Yii::t('app/crf', 'The taking of precautionary measures additional to any already contained in the original drilling programme where a planned minimum separation distance between adjacent wells was not maintained.');
    $selGuidance .= '   </li>';
    $selGuidance .= '</ol>';
                        
?>
    <?= $selGuidance ?>
</div>

<h4>
    <?= Html::checkbox('isC', $model->is_c, ['disabled'=>true]) ?>
    <strong style="padding-left: 10px; padding-right: 10px">C</strong> 
    <?= \Yii::t('app/crf', 'Failure of a safety and environmental critical element') ?>
</h4>
<div class="col-lg-12 sy_pad_bottom_36">
    <!-- the description goes here -->
        <?php

            $selGuidance = '<ol class="list-unstyled">';
            $selGuidance .= '   <li>';
            $selGuidance .=        \Yii::t('app/crf', 'Any loss or non-availability of a SECE requiring immediate remedial action.');
            $selGuidance .= '   </li>';
            $selGuidance .= '</ol>';
            
            echo $selGuidance;
        ?>                            
</div>

<!-- Section D  -->
<h4>
    <?= Html::checkbox('isD', $model->is_d, ['disabled'=>true]) ?>
    <strong style="padding-left: 10px; padding-right: 10px">D</strong> 
    <?= ucfirst(\Yii::t('app/crf', 'significant loss of structural integrity, or loss of protection against the effects of fire or explosion, or loss of station keeping in relation to a mobile installation')) ?>
</h4>
<div class="col-lg-12 sy_pad_bottom_36">
    <!-- the description goes here -->
        <?php

            $selGuidance = '<ol class="list-unstyled">';
            $selGuidance .= '   <li>';
            $selGuidance .=        \Yii::t('app/crf', 'Any detected condition that reduces the designed structural integrity of the installation, including stability, buoyancy and station keeping, to the extent that it requires '
                            . 'immediate remedial action.');
            $selGuidance .= '   </li>';
            $selGuidance .= '</ol>';
            
            echo $selGuidance;
        ?>
</div>

<h4>
    <?= Html::checkbox('isE', $model->is_e, ['disabled'=>true]) ?>
    <strong style="padding-left: 10px; padding-right: 10px">E</strong> 
    <?= \Yii::t('app/crf', 'Vessels on collision course and actual vessel collisions with an offshore installation') ?>
</h4>
<div class="col-lg-12 sy_pad_bottom_36">
<!-- the description goes here -->
    <?php

        $selGuidance = '<ol class="list-unstyled">';
        $selGuidance .= '   <li>';
        $selGuidance .=        \Yii::t('app/crf', 'Any collision, or potential collision, between a vessel and an offshore installation which has, or would have, enough energy to cause sufficient damage to the installation and/or vessel, to jeopardise the overall structural or process integrity.');
        $selGuidance .= '   </li>';
        $selGuidance .= '</ol>';
        
        echo $selGuidance;                 
    ?>                            
</div>

<h4>
    <?= Html::checkbox('isF', $model->is_f, ['disabled'=>true]) ?>
    <strong style="padding-left: 10px; padding-right: 10px">F</strong> 
    <?= \Yii::t('app/crf', 'Helicopter accidents, on or near offshore installations') ?>
</h4>
<div class="col-lg-12 sy_pad_bottom_36">
<?php

    $selGuidance = '<ol class="list-unstyled">';
    $selGuidance .= '   <li>';
    $selGuidance .=        \Yii::t('app/crf', 'Any collision, or potential collision, between a helicopter and an offshore installation.');
    $selGuidance .= '   </li>';
    $selGuidance .= '</ol>';

    echo $selGuidance;
?>
</div>

<div class='sy_pad_bottom_36'>
    <h4>
        <?= Html::checkbox('isG', $model->is_g, ['disabled'=>true]) ?>
            <strong class='sy_pad_bottom_36' style="padding-left: 10px; padding-right: 10px">G</strong> 
        <?= \Yii::t('app/crf', 'Any fatal accident to be reported under the requirements of Directive 92/91/EEC') ?>
    </h4>
</div>

<div class='sy_pad_bottom_36'>
<h4>
    <?= Html::checkbox('isH', $model->is_h, ['disabled'=>true]) ?>
    <strong class='sy_pad_bottom_36' style="padding-left: 10px; padding-right: 10px">H</strong> 
    <?= \Yii::t('app/crf', 'Any serious injuries to five or more persons in the same accident to be reported under the requirements of Directive 92/91/EEC') ?>
</h4>
</div>

<h4>
    <?= Html::checkbox('isI', $model->is_i, ['disabled'=>true]) ?>
    <strong style="padding-left: 10px; padding-right: 10px">I</strong> 
    <?= \Yii::t('app/crf', 'Any evacuation of personnel') ?>
</h4>
            
<div class="col-lg-12 sy_pad_bottom_36">
<!-- the description goes here -->
<?php

    $selGuidance = '<ol class="list-unstyled">';
    $selGuidance .= '   <li>';
    $selGuidance .=        \Yii::t('app/crf', 'Any unplanned emergency evacuation of part of or all personnel as a result of, or where there is a significant risk of a major accident.');
    $selGuidance .= '   </li>';
    $selGuidance .= '</ol>';

    echo $selGuidance;
?>                          
</div>

<h4>
    <?= Html::checkbox('isJ', $model->is_j, ['disabled'=>true]) ?>
    <strong style="padding-left: 10px; padding-right: 10px">J</strong> 
    <?= \Yii::t('app/crf', 'A major environmental incident') ?>
</h4>
<div class="col-lg-12 sy_pad_bottom_36">
<!-- the description goes here -->
<?php

    $selGuidance = '<ol class="list-unstyled">';
    $selGuidance .= '   <li>';
    $selGuidance .=        \Yii::t('app/crf', 'Any major environmental incident as defined in Article 2.1.d and Article 2.37 of Directive 2013/30/EU.');
    $selGuidance .= '   </li>';
    $selGuidance .= '</ol>';

    echo $selGuidance;
?>                          
</div>