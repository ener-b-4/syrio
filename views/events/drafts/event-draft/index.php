<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */


use yii\helpers\Html;

use app\components\helpers\DataGrid\CustomGridView;
use app\components\helpers\DataGrid\funDataGridClass;

use app\models\events\EventDeclaration;
use app\widgets\ActionMessageWidget;
use app\assets\FontAwesomeAsset;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\rebuilt\EventDraftSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $evtID integer the ID of the event */

FontAwesomeAsset::register($this);
//$this->registerAssetBundle(FontAwesomeAsset::className());

$_event = EventDeclaration::findOne(['id' => $evtID]);

$breadcrumbs = [
    [
        'label' => Yii::t('app', 'Registered events'),
        'url' => Url::to(['/events/event-declaration/index']),
        //'template' =>"<li>{link}</li>
    ],
    [
        'label' => $_event->event_name,
        'url' => Url::to(['/events/event-declaration/view', 'id'=>$_event->id]),
        //'template' =>"<li>{link}</li>
    ],
    [
        'label' => Yii::t('app', 'Reporting'),
        //'template' =>"<li>{link}</li>
    ],
];

$this->params['breadcrumbs'] = $breadcrumbs;
$this->title = Yii::t('app', 'Event reporting');
//$this->params['breadcrumbs'][] = [];

$pathReopenDraft = 'events/drafts/event-draft/reopen';
$pathSignDraft = 'events/drafts/event-draft/finalize';
$pathDel = 'events/drafts/event-draft/delete';
$pathUnlockDraft = 'events/drafts/event-draft/unlock-all';
$pathCreate = Yii::$app->getUrlManager()->createUrl(['create', 'evtId' => $evtID]);
$pathPDF = 'events/drafts/event-draft/view-pdf';

$heading = Yii::t('app', 'Report drafts list');

//var_dump(get_included_files());
//if ( ! class_exists('CustomGridView')) die('There is no hope!'  ); 
include '_parts/grid_columns.php';
//print_r($columns);
//die();
?>


<div class="event-draft-index">

    <?php
        //check if flash is set
        if (Yii::$app->session->hasFlash('msg'))
        {
            $msg = Yii::$app->session->getFlash('msg');
            if (strpos($msg,'[err]'))
            {
                //failure
                //get rid of the [s]
                $msg =  str_replace('[err]', '', $msg);
                $msg =  str_replace('\n', '<br/>', $msg);
                
                echo "<div class='alert alert-danger'>$msg</div>";
            }
            else
            {
                //success
                $msg =  str_replace('\n', '<br/>', $msg);
                echo "<div class='alert alert-success'>$msg</div>";
            }
        }
    ?>
    
    
    <?php
    /* use this on any view to include the actionmessage logic and widget */
    //include Yii::getAlias('@app') . '\views\shared_parts\message_snippet.php';
	include Yii::getAlias('@app') . '/views/shared_parts/message_snippet.php';
    ?>

    <h1><?= Html::encode($this->title) . ': ' . $_event->event_name ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?=
        Html::a(Yii::t('app', 'Create {evt}', [
              'evt' => Yii::t('app/crf', 'report draft'),
            ]), ['create', 'evtId' => $evtID], ['class' => 'btn btn-success hidden'])
        ?>
    </p>

    <?php
      
    $title_h_l = 'SyRIO ' . \Yii::t('app', 'Export');
    $title_h_c =  $this->title; //'Custom C';
    $title_h_r = 'Generated' . ': ' . date("D, d-M-Y g:i a T");
    $ourPdfHeader = funDataGridClass::getOurPdfHeader($title_h_l, $title_h_c, $title_h_r);

    $title_f = '';
    $ourPdfFooter = funDataGridClass::getOurPdfFooter($title_f);
    
    $exportConfig = funDataGridClass::getExportConfiguration($ourPdfHeader, $ourPdfFooter);

    
    $trashUrl = Yii::$app->urlManager->createAbsoluteUrl(['events/drafts/event-draft/trash', 'evtID'=>$_event->id]);

    //GridView::widget([
    echo CustomGridView::widget([
      'dataProvider' => $dataProvider,
      'filterModel' => $searchModel,
      'pjax' => false,
      'rowOptions' => function($model) {
        return $model->del_status == app\models\events\EventDeclaration::STATUS_DELETED ? ['class' => 'danger'] : [];
        /* asta cade - pun coloana separata */
        if ($model->ca_status == EventDeclaration::INCIDENT_REPORT_REJECTED) {
          return ['class' => 'danger'];
        }
        else if ($model->ca_status == EventDeclaration::INCIDENT_REPORT_ACCEPTED) {
          return ['class' => 'success'];
        }
      },
      'columns' => $columns,
      //END COLUMNS
          
      // set your toolbar
      'toolbar' => [
        ['content' =>
          Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create', 'evtId' => $evtID], [
            'class' => 'btn btn-success',
            'title' => Yii::t('app', 'Create {evt}', [
              'evt' => Yii::t('app/crf', 'report draft'),
            ]),
          ]),
          //Html::button('<i class="glyphicon glyphicon-plus"></i>', ['type'=>'button', 'title'=>Yii::t('kvgrid', 'Add Book'), 'class'=>'btn btn-success', 'onclick'=>'alert("This will launch the book creation form.\n\nDisabled for this demo!");']) . ' '.

          /*
            Html::button('<i class="glyphicon glyphicon-plus"></i>', [
            'type'=>'button',
            'title'=>Yii::t('app', 'Create'), 'class'=>'btn btn-success',
            'onclick'=>'alert("This will launch the book creation form.\n\nDisabled for this demo!");']) . ' '.
           *
           */
          Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['grid-demo'], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => Yii::t('app', 'Reset Grid')])
        ],
        '{export}',
        '{toggleData}',
      ],
                      
      // set export properties
      'export' => [
        'icon' => 'share-square-o',
        'encoding' => 'utf-8',
        'fontAwesome' => true,
        'target' => CustomGridView::TARGET_SELF,   //target: string, the target for submitting the export form, which will trigger the download of the exported file. 
        'showConfirmAlert' => TRUE,
        'header' => '<li role="presentation" class="dropdown-header">' . \Yii::t('app', 'Grid Export') . '</li>'
        //'header' => 'Export Page Data',
      ],
              
      //(start) Author: cavesje   Date: 14.08.2015  - extra configuration PDF export
      'exportConfig'     => $exportConfig,
      //(end) Author: cavesje   Date: 14.08.2015  - extra configuration PDF export
              
      'panel' => [
        //'type' => GridView::TYPE_PRIMARY,
        'type' => CustomGridView::TYPE_PRIMARY,
        'heading' => $heading,
        'before' => '<div class="col-sm-2">'
                . Html::a('<span class="glyphicon glyphicon-trash"></span> '
                        . '<span class="badge">' . \app\models\events\drafts\EventDraft::getNumberOfDeleted($_event->id) . '</span>',
                        $trashUrl
                        , [
                            'class' => 'btn btn-danger',
                            'title' => Yii::t('app', 'Deleted drafts. Click to manage.')])
                . '</div>',
      ],

      'persistResize' => false,
    ]);

  ?>

</div>
