<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\web\JqueryAsset;
use yii\jui\Accordion;

use app\models\User;
use app\models\events\EventDeclaration;

/* @var $this yii\web\View */
/* @var $model app\models\rebuilt\EventDraft */

$this->title = 'Section A';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Events Management'), 'url' => ['events/event-declaration/index']];
$this->params['breadcrumbs'][] = ['label' => $model->event->event_name, 'url' => ['events/event-declaration/display-drafts', 'id'=>$model->event->id]];
$this->params['breadcrumbs'][] = ['label' => $model->session_desc, 'url' => ['events/event-declaration/display-drafts', 'id'=>$model->event->id]];
$this->params['breadcrumbs'][] = $this->title;



//get user's details anf fill in the model
$users = new User();
$user=$users->findOne($model->created_by);
if (is_object($user))
{
    $uname = $user->name;
}
else
{
    $uname = 'user_id ' . $model->created_by . 'not found';
}

$user=$users->findOne($model->modified_by);
if (is_object($user))
{
    $modifname = $user->name;
}
else
{
    $modifname = 'user_id ' . $model->modified_by . ' not found';
}



?>
<div class="event-draft-view">

        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3>Section A: Unintended release of oil, gas or other hazardous substances, whether or not ignited.</h3>
                <small>bla bla</small>
            </div>
            <div class="panel-body">
                <small class='text-right'>
                <ul class="list-inline">
                    <li>
                        <?= ucfirst(\Yii::t('app', 'created at')) . ': ' . $model->created_at ?>
                    </li>
                    <li>
                        <?= \Yii::t('app', 'by') . ': ' . $uname ?>
                    </li>
                </ul>

                <ul class="list-inline">
                    <li>
                        <?= ucfirst(\Yii::t('app', 'modified at')) . ': ' . $model->modified_at ?>
                    </li>
                    <li>
                        <?= \Yii::t('app', 'by') . ': ' . $modifname ?>
                    </li>
                </ul>


                </small>

                <?php 
                ?>
                
                <p>Expand each of the sub-sections for reviewing</p>

                <div class='row'>
                    
                    <div class='col-lg-12'>
                        <?php
                        

                        ?>
                    </div>
                    <div class='col-lg-12'>
                    <?php
                        $sa = $model->getSectionA()->one();
                        $sa1 = $sa->getSA1s()->one();
                        //print_r($sa->id . $sa1->id);
                        
                        echo Accordion::widget([
                            'items' => [
                                [
                                    'header' => 'Hydrocarbon release details',
                                    'headerOptions' => ['tag' => 'h3'],
                                    'content' => $this->render('/crf/section1/view', ['model'=>$sa1]),
                                    'options' => ['tag' => 'div'],
                                ],
                            ],
                            'options' => ['tag' => 'div'],
                            'itemOptions' => ['tag' => 'div', 'style'=>'font-family: "Helvetica Neue", Helvetica, Arial, sans-serif; font-size: 14px'],
                            'headerOptions' => ['tag' => 'h3'],
                            'clientOptions' => ['collapsible' => true, 'active'=>false],
                        ]);                        
                    ?>                                
                    </div>
                </div>
            </div>
        </div>
    
    
<?php
    //$this->registerCssFile('css/Section1.css');    
    //$this->registerJsFile('js/Section1.js');
    $this->registerJsFile('/SyRIO_DEV/web/js/abc.js', ['depends' => [JqueryAsset::className()], 'position' => \yii\web\View::POS_END]);
    //$this->registerJsFile('js/Section1.js');
    //['depends' => [JqueryAsset::className()]]
?>    
    
</div>
