<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\widgets\DetailView;


use app\models\User;
use app\models\events\EventDeclaration;

/* @var $this yii\web\View */
/* @var $model app\models\events\drafts\EventDraft */

if (!\Yii::$app->user->can('incident_submit_operators', ['draft'=>$model]))
{
    return \Yii::$app->error->ThrowError(\app\components\Errors::RESTRICTED_OPERATION_ERROR);
}

$event = $model->event;
$dt = new DateTime($event->event_date_time);
?>

<div class="col-lg-12">
    <h1><?= \Yii::t('app', 'Submit Incident Report to the CA') ?></h1>
    <div class="alert alert-warning" role="alert">
        <h2><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span style="margin-right: 16px;"> 
                <?= \Yii::t('app', 'You are about to submit the Incident Report to the Competent Authority') ?>
            </span>
        </h2>
        <p><?= \Yii::t('app', 'Please read carefully the information below and choose Submit if agree, Cancel otherwise.') ?></p>
    </div>
    <div class="well col-lg-4 col-md-4 col-sm-12 col-xs-12" style="padding: 6px;">
        
            <h2><?= \Yii::t('app', 'What happens next') ?></h2>
            <p><?= \Yii::t('app', 'Once the Incident report is submitted:') ?></p>
            <ul>
                <li>
                    <?= \Yii::t('app', 'The Competent Authority is informed about the event;') ?>
                </li>
                <li>
                    <?= \Yii::t('app', 'You (or any member of your Organization):') ?>
                    <ul>
                        <li><?= \Yii::t('app', '<b>will</b> be able to view the reported incident') ?></li>
                        <li><?= \Yii::t('app', '<b>will not</b> be able to modify the information reported') ?>.<sup>a)</sup></li>
                        <li><?= \Yii::t('app', '<b>will not</b> be able to delete the information reported') ?>.<sup>a)</sup></li>
                    </ul>
                </li>
            </ul>
            <p><small class="text-muted" style="border-top: 1px solid black; padding-top:6px;"><?= \Yii::t('app', 'a) contact the system administrator (through SyRIO)') ?></small></p>
     </div>

    <div class="col-lg-8 col-md-4 col-sm-12 col-xs-12">
            <h4><span class="text-info"><?= ucfirst(\Yii::t('app/crf', 'Event Name')) . ': ' ?></span><?= $model->event->event_name . ' (' . \Yii::t('app', 'will not be submitted') . ')'?></h4>

            <h2><?= \Yii::t('app', 'Information to be submitted') ?></h2>
            <!-- render the event details -->
            <div class='sy_pad_bottom_18 sy_pad_top_18'>
                <h4><span class="text-info"><?= \Yii::t('app/crf', 'Event date/time') . ': ' ?></span></h4>
                <?= '(a) ' . \Yii::t('app/crf', 'Event date') . ': ' . date_format($dt, 'd-m-Y') ?>
                <br/>
                <?= '(b) ' . \Yii::t('app/crf', 'Event time') . ': ' . date_format($dt, 'H:i:s') ?>
            </div>


            <h4><span class="text-info"><?= \Yii::t('app/crf', 'Details of the location and of the person reporting the event') ?></span></h4>
            <div class='sy_pad_bottom_18'>
                <p>
                    <span class='text-warning'>
                        <sup>(*)</sup>
                        <?= \Yii::t('app', 'Details of the person reporting the event will be updated from your account information!') ?>
                    </span>
                </p>
                <?= DetailView::widget([
                    'model' => $event,
                    'attributes' => [
                        'operator',
                        'installation_name_type',
                        'field_name',
                        [
                            'format' => 'html',
                            'label' => $event->attributeLabels()['raporteur_name'] . '<sup>*</sup>',
                            'value' => $event->raporteur_name . ' / <span class="text-danger">'. \Yii::$app->user->identity->full_name . '</span>',
                        ],
                        [
                            'format' => 'html',
                            'label' => $event->attributeLabels()['raporteur_role'] . '<sup>*</sup>',
                            'value' => $event->raporteur_role . ' / <span class="text-danger">'. \Yii::$app->user->identity->role_in_organization . '</span>',
                        ],
                        [
                            'format' => 'html',
                            'label' => $event->attributeLabels()['contact_tel'] . '<sup>*</sup>',
                            'value' => $event->contact_tel . ' / <span class="text-danger">'. \Yii::$app->user->identity->phone . '</span>',
                        ],
                        [
                            'format' => 'html',
                            'label' => $event->attributeLabels()['contact_email'] . '<sup>*</sup>',
                            'value' => Html::a($event->contact_email, 'mailto:'.$event->contact_email) . ' / <span class="text-danger">'. \Yii::$app->user->identity->email . '</span>',
                        ],
                    ],
                ]) ?>
            </div>
            
            <h4><span class="text-info"><?= \Yii::t('app', 'Selected draft to submit') . ': ' ?> </span>
                <?= $model->session_desc ?>
                <b>
                <?= Html::a(Yii::t('app', 'review'). ' >>>', ['review', 'id' => $model->id], []) ?>
                </b>
            </h4>
            
            <div style="padding-top: 16px">
                <?= Html::a(Yii::t('app', 'Submit Incident Report'), ['submit-incident', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
                <?= Html::a(Yii::t('app/commands', 'Cancel'), ['cancel-submit', 'id' => $model->id], ['class' => 'btn btn-warning']) ?>            
            </div>
    </div>

    
 
  
</div>