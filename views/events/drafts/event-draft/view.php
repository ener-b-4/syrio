<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\web\JqueryAsset;
use yii\jui\Accordion;

use app\models\User;
use app\models\events\EventDeclaration;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\rebuilt\EventDraft */

//http://localhost:8080/SyRIO_DEV/web/index.php?r=events%2Fevent-declaration%2Fdisplay-drafts&id=5

$this->title = $model->session_desc;

$_event = EventDeclaration::findOne(['id' => $model->event_id]);
$breadcrumbs = [
    [
        'label' => Yii::t('app', 'Registered events'),
        'url' => Url::to(['/events/event-declaration/index']),
        //'template' =>"<li>{link}</li>
    ],
    [
        'label' => $_event->event_name,
        'url' => Url::to(['/events/event-declaration/view', 'id'=>$_event->id]),
        //'template' =>"<li>{link}</li>
    ],
    [
        'label' => Yii::t('app', 'Reporting'),
        'url' => Url::to(['/events/drafts/event-draft/index', 'evtID'=>$_event->id]),
    ],
    [
        'label' => $model->session_desc,
        //'url' => Url::to(['/events/event-declaration/view', 'id'=>$_event->id]),
    ],
];

$this->params['breadcrumbs'] = $breadcrumbs;
//$this->title = Yii::t('app', 'Event reporting');
//$this->params['breadcrumbs'][] = [];


//(start) Author : cavesje  Date : 20150617 - Removed (double declaration)
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Events Management'), 'url' => ['events/event-declaration/index', 'evtID'=>$model->event->id]];
//$this->params['breadcrumbs'][] = ['label' => $model->event->event_name, 'url' => ['events/event-declaration/view', 'id'=>$model->event->id]];
//$this->params['breadcrumbs'][] = $this->title;
//(end) Author : cavesje  Date : 20150617 - Removed (double declaration)

//http://localhost:8080/SyRIO_DEV/web/index.php?r=events/drafts/event-draft/view&id=8

//get user's details anf fill in the model
if (isset($definitionArr)) unset($definitionArr);
$definitionArr=[];

$user = $model->createdBy;
is_object($user) ? $definitionArr['creator_name'] = $user->full_name : $definitionArr['creator_name'] = '-';
if (isset($model->modified_by)) {
    $modif_user = User::find()->where(['id'=>$model->modified_by])->one();
    if (isset($modif_user)) {
        $definitionArr['modified_by'] = $modif_user->full_name;
    } else {
        $definitionArr['modified_by'] = '-';
    }
} else {
    $definitionArr['modified_by'] = '-';
}

if (isset($model->created_at))
{
    //$dt = new DateTime($model->created_at);
    $definitionArr['created_at']=date('Y-m-d H:i:s', $model->created_at);
}

$user=$model->modifiedBy;
is_object($user) ? $definitionArr['modifier_name'] = $user->name : $definitionArr['modifier_name'] = '-';

if (isset($model->modified_at))
{
    $definitionArr['modified_at']=date('Y-m-d H:i:s', $model->modified_at);
}

?>
<div class="event-draft-view">

    <h1><?= '<span style="margin-right: 12px;">' . Html::encode($this->title) . '</span><small class="text-muted"><em>' . $model->event->event_name . '</em></small>' ?></h1>

    <p>
        <?php
        
        if (\Yii::$app->user->can('incident_draft_delete', ['draft'=>$model]))
        {
            echo Html::a(\Yii::t('app', 'Delete {evt}', [
                'evt'=>Yii::t('app', 'draft')]), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]);
        }
                
        ?>

        <?= Html::a(Yii::t('app', 'Review all'), ['review', 'id' => $model->id], ['class' => 'btn btn-success pull-right']) ?>

    </p>

    <div style='margin-bottom: 24px;'>
        <?php
            //Author : cavesje  Date : 20150617 - BUG: duplicate $this->params['breadcrumbs'] (all items are duplicated) 
            //Removed $this->params['breadcrumbs'] in this view.
            echo $this->render('@app/views/events/event-declaration/view', ['model'=>$model->event, 'stripped'=>true]);
        ?>
    </div>            
    
        <div class="panel panel-primary">
            <div class="panel-heading" style="overflow-wrap: true ">
                <div class="row" style="overflow-x: hidden">
                    <div class="col-sm-9 col-xs-8">
                        <h3><?= Yii::t('app/crf', 'Event categorization') ?></h3>
                        <small><?= Yii::t('app/crf', 'According to Annex IX of Directive 2013/30/EU') ?></small>
                    </div>
                    <div class="col-sm-3 col-xs-4 text-right">
                        <h3></h3>
                        <?php
                        if (\Yii::$app->user->can('incident_update', ['event'=>$model->event]))
                        {                        
                            echo Html::a(Yii::t('app', 'Modify', [
                            ])
                            , ['update', 'id' => $model->id], ['class' => 'btn btn-success']);
                            }
                        ?>
                    </div>
                </div>
                
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-12 text-right">
                        <small>

                        <ul class="list list-unstyled">
                            <?php
                                if (key_exists('creator_name', $definitionArr))
                                {
                                    $s  = '';
                                    $s .= '<li>' . \Yii::t('app/crf', 'Created') . ': ' . $definitionArr['created_at'] . '  ';
                                    $s .= \Yii::t('app', 'by') . ': ' . $definitionArr['creator_name'] . '</li>';
                                }
                                else
                                {
                                    $s  = '';
                                    $s .= '<li>' . \Yii::t('app/crf', 'Created') . ': ' . $definitionArr['created_at'] . '  ';
                                    $s .= \Yii::t('app', 'by') . ': ' . 'creator credentials missing!' . ' </li>';
                                }
                                echo $s;
                            ?>
                            <?php
                                if (key_exists('modified_at', $definitionArr))
                                {
                                    $s  = '';
                                    $s .= '<li>' . \Yii::t('app', 'Modified') . ': ' . $definitionArr['modified_at'] . ' ';
                                    $s .= \Yii::t('app', 'by') . ': ' . $definitionArr['modified_by'] . '</li>';
                                }
                                else
                                {
                                    $s  = '';
                                    $s .= '<li>' . \Yii::t('app', 'Modified') . ': ';
                                    $s .= \Yii::t('app', 'never') . '</li>';
                                }
                                echo $s;
                            ?>
                        </ul>

                        </small>
                    </div>
                </div>
                
                
                <h3><?= Yii::t('app/crf', 'What type of event is being reported?') ?></h3>
                <p class="text-muted">
                    <em><?= Yii::t('app', 'More than one option might be chosen') ?></em>
                </p>

                <?php 
                include_once 'blocks/_hasA.php';
                include_once 'blocks/_hasB.php';            
                include_once 'blocks/_hasC.php';            
                include_once 'blocks/_hasD.php';            
                include_once 'blocks/_hasE.php';            
                include_once 'blocks/_hasF.php';            
                include_once 'blocks/_hasG.php';            
                include_once 'blocks/_hasH.php';            
                include_once 'blocks/_hasI.php';            
                include_once 'blocks/_hasJ.php';            
                ?>

            </div>
        </div>
    
    
<?php
    //$this->registerCssFile('css/Section1.css');    
    //$this->registerJsFile('js/Section1.js');
    //$this->registerJsFile('/SyRIO_DEV/web/js/abc.js', ['depends' => [JqueryAsset::className()], 'position' => \yii\web\View::POS_END]);
    //$this->registerJsFile('js/Section1.js');
    //['depends' => [JqueryAsset::className()]]
?>    
    
</div>
