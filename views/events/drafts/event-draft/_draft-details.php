<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use app\models\events\drafts\EventDraft;

/* @var $this yii\web\View */
/* @var $model app\models\events\drafts\EventDraft */

?>

<div class="col-lg-12">
    <div class='col-lg-12'>
        <small>
            <span class="text-muted"><?= Yii::t('app', 'created by') . ': ' ?></span>
            <span><?= isset($model->createdBy) ? $model->createdBy->name : '-' ?></span>
            <span class="text-muted"><?= Yii::t('app', ' at') . ': ' ?></span>
            <span style='padding-right: 16px;'><?= isset($model->created_at) ? date('Y-m-d H:i:s', $model->created_at) : '-' ?></span>

            <span class="text-muted"><?= Yii::t('app', 'modified by') . ': ' ?></span>
            <span><?= isset($model->modifiedBy) ? $model->modifiedBy->name : '-' ?></span>
            <span class="text-muted"><?= Yii::t('app', ' at') . ': ' ?></span>
            <span><?= isset($model->modified_at) ? date('Y-m-d H:i:s', $model->modified_at) : '-' ?></span>

        </small>        
    </div>
    <div class="col-md-10">
        <?php
            if ($model->ca_status == \app\models\events\EventDeclaration::INCIDENT_REPORT_REJECTED)
            {
        ?>
        <div class="col-lg-12">
            <div class="alert alert-danger" role="alert">
                <strong><?= Yii::t('app','Your report has been rejected by the Competent Authority, with the following justification.') ?></strong>
                <br/>
            </div>
            <div>
                <?=
                    Html::activeTextarea($model, 'ca_rejection_desc', [
                            'class' => 'sy_fill_textarea sy_narative-text',
                            'placeholder' => Yii::t('app', 'enter text...'),
                        ])                
                ?>
            </div>
        </div>
        <?php
            }
        ?>
    </div>
</div>