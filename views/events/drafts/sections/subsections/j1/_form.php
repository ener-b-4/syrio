<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\events\drafts\sections\subsections\J1 */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="j1-form">

    <?php $form = ActiveForm::begin(); ?>

    <table>
        <tbody>
            <tr class="sy_para">
                <td class="sy_crf_para sy_section_h1">J.1.</td>
                <td class="sy_crf_para sy_section_h1" style='width:100%;'>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <?= Yii::t('app/crf', 'General information') ?>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="sy_crf_para"></td>
                <td class="sy_crf_para">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <ul class="list-unstyled sy_padding_clear">
                            <li class="text-info">(a)  <?= Yii::t('app/crf', 'Name of contractor') ?> <?= Yii::t('app/crf', '(if applicable)') ?> </li>
                            <li>
                                <br/>
                                <?= Html::activeTextInput($model, 'j1_1', [
                                'placeholder'=>Yii::t('app', 'add text'),
                                'class' => 'form-control'
                            ]) ?></li>
                        </ul>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>


    <?php ActiveForm::end(); ?>

</div>
