<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\events\drafts\sections\subsections\F2 */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="f2-form">

    <?php $form = ActiveForm::begin(); ?>

    <table>
        <tbody>
            <tr class="sy_para">
                <td class="sy_crf_para sy_section_h1">F.1.</td>
                <td class="sy_crf_para sy_section_h1" style='width:100%;'>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <?= Yii::t('app/crf', 'General information') ?>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="sy_crf_para"></td>
                <td class="sy_crf_para">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <ul class="list-inline sy_padding_clear">
                            <li class="text-info sy_vert_top sy_pad_top_6">(a)  <?= Yii::t('app/crf', 'Name of helicopter contractor') ?> </li>
                            <li><?= $form->field($model, 'f1_1')->textInput()->label('',['class'=>'hiddenBlock']) ?></li>
                        </ul>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <ul class="list-inline sy_padding_clear">
                            <li class="text-info sy_vert_top sy_pad_top_6">
                                <div class="form-group">(b)  <?= Yii::t('app/crf', 'Helicopter type') ?> </li>
                                 
                            </li>
                            <li><?= $form->field($model, 'f1_2')->textInput()->label('',['class'=>'hiddenBlock']) ?></li>
                        </ul>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <ul class="list-inline sy_padding_clear">
                            <li class="text-info sy_vert_top sy_pad_top_6">
                                <div class="form-group">(c)  <?= Yii::t('app/crf', 'Number of persons on board') ?> </li>
                                 
                            </li>
                            <li><?= $form->field($model, 'f1_3')->textInput()->label('',['class'=>'hiddenBlock']) ?></li>
                        </ul>
                    </div>
                </td>
            </tr>   
        </tbody>
    </table>    
    
    
    <?php ActiveForm::end(); ?>

</div>
