<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

$p = realpath(__DIR__ . DIRECTORY_SEPARATOR . '../load_units.php');
require_once $p;

?>

<div class="section1-form">

    <?php $form = ActiveForm::begin(); ?>

            <!--
            SECTION A.1.IX
            -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="content">
                        <div class="row">
                            <div class="col-lg-12">
                                <strong>IX. System pressure</strong>
                            </div>
                        </div>
                        <div class="row form-inline">
                            <div class="col-lg-5 col-lg-offset-1">
                                <ul class="list-unstyled">
                                    <li>
                                        <?= $form->field($model, 'a1ix_a_q')->textInput()->label('Design pressure') ?>
                                        <?= $form->field($model,'a1ix_a_u')->dropDownList($pressureUnits, ['id'=>'unit'])->label(''); ?>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-lg-5">
                                <ul class="list-unstyled">
                                    <li>
                                        <?= $form->field($model, 'a1ix_b_q')->textInput()->label('Actual pressure') ?>
                                        <?= $form->field($model,'a1ix_b_u')->dropDownList($pressureUnits, ['id'=>'unit'])->label(''); ?>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  <!--end section A.1.IX -->


    <?php ActiveForm::end(); ?>

</div>
