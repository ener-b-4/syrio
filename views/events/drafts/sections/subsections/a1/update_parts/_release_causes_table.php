<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

/* @var $this yii\web\View */
/* @var $model app\models\crf\Section1 */
/* @var $form yii\widgets\ActiveForm */
/* @var $all_causes app\models\crf\EventReleaseCause[] */
/* @var $sel_causes app\models\crf\EventReleaseCause[] */
/* @var $other_attr mixed key=>value pair with key the category_id and value the model root attribute (should be appended with _code or _specify) */
 
/* get the leak types available */
$leak_categories = \app\models\crf\LeakCauseCategories::find()->where(['<>', 'id', 0])->all();

$disabled = 'true';
$template = '<input type="checkbox" name="rel_causes[]" value="{value}" {checked} {disabled}><span class="inline-label-right">{label}</span></input>';
$template = str_replace('{disabled}', $disabled, $template);

/*<span id='a1_fail_equipment_code_toggle'>
 * 
 * <div class="form-group field-a1-a1_fail_procedural_specify" has-success">
 * <input name="A1[a1_fail_equipment_specify]" class="form-control" id="a1-a1_fail_equipment_specify" type="text" maxlength="128" value="other equipment">
 * </div>
 *</span> 
 */

//{fg_class} field-a1-a1_fail_procedural_specify
//{toggle_id} a1_fail_equipment_code_toggle
//{other_name} A1[a1_fail_equipment_specify]
//{other_id} a1-a1_fail_equipment_specify
//{other_desc}
$template_other = '<input type="hidden" name="A1[{name}]" value="0"></input>'
        . '<input type="checkbox" name="A1[{name}]" value="2" {checked} {disabled} '
        . '     class="sy_data_toggle"'
        . '     sy_data_toggle_dest="{toggle_id}"'
        . '     sy_data_toggle_src_type="checkbox"'
        . '     sy_data_toggle_enabled="true"'
        . '     sy_data_toggle_visible_value="true"'
        . '><span class="inline-label-right">' . Yii::t('app/crf', 'Other, specify') . '</span>'
        . '</input>'
        . '<span id="{toggle_id}">'
        . '<div class="form-group {fg_class}">'
        . ' <input name="{other_name}" class="form-control" id="{other_id}" type="text" maxlength="128" value="{other_desc}">'
        . ' </input>'
        . '</div>'
        . '</span>';
$template_other = str_replace('{disabled}', $disabled, $template_other);

?>

<table class="release_causes">
    <tbody>
        <?php 
        foreach($leak_categories as $category) {
            /* render header */
            ?>
        <tr class="p-head">
            <td colspan="2"><?= Yii::t('app/crf', $category->name) ?></td>
        </tr>
        <?php
            /* get the causes */
            $index = 0;
            foreach ($all_causes[$category->id] as $cause) {
                $checked = in_array($cause['num_code'], $sel_causes) ? 'checked' : '';

                $cinput = str_replace('{value}', $cause['num_code'], $template);
                $cinput = str_replace('{label}', Yii::t('app/crf', $cause['cause']), $cinput);
                $cinput = str_replace('{checked}', $checked, $cinput);
                
                if ($index % 2 == 0) {
                    echo '<tr>';
                    echo '<td>'
                    . $cinput
                    //. $cause['cause'] . '('.$cause['num_code'].') '.$checked
                    . '</td>';
                } else {
                    echo '<td>'
                    . $cinput
                    //. $cause['cause'] . '('.$cause['num_code'].') '.$checked
                    . '</td>';
                    
                    echo '</tr>';
                }
                $index+=1;
            }
            
            /* check if other exists */
            if (array_key_exists($category->id, $other_attr)) {
                
                $attr1 = $other_attr[$category->id].'_code';        //e.g. a1_fail_equipment_code
                $attr2 = $other_attr[$category->id].'_specify';     //e.g. a1_fail_equipment_specify
                
                $checked = (isset($model->$attr1) && $model->$attr1 == 2) ? "checked" : "";
                
                //set-up $other_input block
                //{fg_class} field-a1-a1_fail_procedural_specify
                //{toggle_id} a1_fail_equipment_code_toggle
                //{other_name} A1[a1_fail_equipment_specify]
                //{other_id} a1-a1_fail_equipment_specify
                //{other_desc}
                $other_toggle_id = $attr1.'_toggle';
                $other_name = 'A1['.$attr2.']';
                $other_id = 'a1-'.$attr2;
                $other_value = $model->$attr2;
                $fg_class = 'field-a1-'.$attr2; //{fg_class} field-a1-a1_fail_procedural_specify
                
                
                $cinput = str_replace('{name}', $attr1, $template_other);
                $cinput = str_replace('{checked}', $checked, $cinput);
                $cinput = str_replace('{other_name}', $other_name, $cinput);
                $cinput = str_replace('{toggle_id}', $other_toggle_id, $cinput);
                $cinput = str_replace('{fg_class}', $fg_class, $cinput);
                $cinput = str_replace('{other_id}', $other_id, $cinput);
                $cinput = str_replace('{other_desc}', $other_value, $cinput);
                
                /* if index % 2 == 1 create an empty cell */
                if ($index % 2 == 1) {
                    echo '<td>'.$cinput.'</td>'.'</tr>';
                } else {
                    //create a new row
                    echo '<tr><td>'.$cinput.'</td>'.'<td></td></tr>';
                }
                
                $index+=1;
            }
            
            
            /* if index % 2 == 1 create an empty cell */
            if ($index % 2 == 1) {
                echo '<td></td></tr>';
            }
            
        }
        ?>
        
    </tbody>
</table>