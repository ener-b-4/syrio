<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

?>

<div class="section1-form">

    <?php $form = ActiveForm::begin(); ?>

            <div class="row">
                <div class="col-lg-12">
                    <div class="content">
                        <div class="row">                            
                            <div class="col-lg-12">
                                <h5>
                                    <strong>I. Hydrocarbon (HC) released</strong> <span class="small em"><?= Yii::t('app', 'Tick appropriate box') ?></span>                                    
                                </h5>
                            </div> 
                        </div>
                        <div class="row">
                            <div class="col-lg-10 col-lg-offset-1 form-inline">
                                <ul class="list-inline">
                                    <li>
                                        NON-PROCESS                                    
                                    </li>
                                    <li>
                                        <?= Html::activeCheckbox($model, 'a1i_np', ['onclick'=>'clicked(this)', 'id'=>'chkNp', 'value'=>1, 'uncheck'=>0, 'label'=>'']) ?>
                                    </li>
                                    <li>
                                        <span id='a1i_specify_container'>
                                            <?= $form->field($model, 'a1i_np_specify')->textInput(['maxlength' => 255])->label('') ?>
                                            <span><em>(please specify)</em></span>
                                        </span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-10 col-lg-offset-1 form-inline">
                                <ul class="list-inline">
                                    <li>
                                        PROCESS
                                    </li>
                                    <li>
                                        <?= Html::activeCheckbox($model, 'a1i_o', ['onclick'=>'clicked(this)', 'id'=>'chkO', 'value'=>1, 'uncheck'=>0, 'label'=>'Oil']) ?>
                                    </li>
                                    <li>
                                        <?= Html::activeCheckbox($model, 'a1i_co', ['onclick'=>'clicked(this)', 'id'=>'chkCo', 'value'=>1, 'uncheck'=>0, 'label'=>'Condensate']) ?>
                                    </li>
                                    <li>
                                        <?= Html::activeCheckbox($model, 'a1i_g', ['onclick'=>'clicked(this)', 'id'=>'chkG', 'value'=>1, 'uncheck'=>0, 'label'=>'Gas']) ?>
                                    </li>
                                    <li>
                                        <?= Html::activeCheckbox($model, 'a1i_2p', ['onclick'=>'clicked(this)', 'id'=>'chk2p', 'value'=>1, 'uncheck'=>0, 'label'=>'2-Phase']) ?>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-10 col-lg-offset-1 form-inline" id='h2s_container'>
                                <ul class="list-inline">
                                    <li>
                                        Level of H<sub>2</sub>S
                                    </li>
                                    <li>
                                        <?= $form->field($model, 'a1i_2p_h2s_q')->textInput()->label('') ?>
                                    </li>
                                    <li>
                                        <em>(estimated ppm)</em>
                                    </li>
                                    <li>
                                        this is NOT in the report <?= $form->field($model, 'a1i_2p_h2s_u')->textInput()->label('') ?>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


    <?php ActiveForm::end(); ?>

</div>
