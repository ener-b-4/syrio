<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

$p = realpath(__DIR__ . DIRECTORY_SEPARATOR . '../load_units.php');
require_once $p;

?>

<div class="section1-form">

    <?php $form = ActiveForm::begin(); ?>

           <!--
            SECTION A.1.X
            -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="content">
                        <div class="row">
                            <div class="col-lg-12">
                                <strong>X. Means of detection</strong>
                            </div>
                        </div>
                        <div class="row form-inline">
                            <div class="col-lg-12 col-lg-offset-1">
                                <ul class="list-unstyled">
                                    <li>
                                        <?= Html::activeCheckbox($model, 'a1x_a', ['onclick'=>'clicked(this)', 'id'=>'chk_a1x_a', 'value'=>1, 'uncheck'=>null, 'label'=>'Fire']) ?>
                                    </li>
                                    <li>
                                        <?= Html::activeCheckbox($model, 'a1x_b', ['onclick'=>'clicked(this)', 'id'=>'chk_a1x_b', 'value'=>1, 'uncheck'=>null, 'label'=>'Gas']) ?>
                                    </li>
                                    <li>
                                        <?= Html::activeCheckbox($model, 'a1x_c', ['onclick'=>'clicked(this)', 'id'=>'chk_a1x_c', 'value'=>1, 'uncheck'=>null, 'label'=>'Smoke']) ?>
                                    </li>
                                    <li>
                                        <ul class="list-inline">
                                            <li>
                                                <?= $form->field($model, 'a1x_dd')->checkbox(['onclick'=>'clicked(this)', 'id'=>'chk_a1x_d', 'value'=>1, 'uncheck'=>null, 'label'=>'Other']) ?>
                                            </li>
                                            <li>
                                                <?php // Html::activeCheckbox($model, 'a1x_d', ['onclick'=>'clicked(this)', 'id'=>'chk_a1x_d', 'value'=>1, 'uncheck'=>null, 'label'=>'Other']) ?>
                                            </li>
                                            <li>
                                                <span id="chk_a1x_d_container">
                                                    <?= $form->field($model, 'a1x_d_specify')->textInput(['maxlength' => 255])->label('') ?>
                                                    <em>(please specify)</em>
                                                </span>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  <!--end section A.1.X -->

    <?php ActiveForm::end(); ?>

</div>
