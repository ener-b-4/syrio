<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

$p = realpath(__DIR__ . DIRECTORY_SEPARATOR . '../load_units.php');
require_once $p;

?>

<div class="section1-form">

    <?php $form = ActiveForm::begin(); ?>

            <!--
            SECTION A.1.XIV
            -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="content">
                        <div class="row">
                            <div class="col-lg-12">
                                <strong>XIV. What emergency action was taken?</strong> <small><em><?= ucfirst(Yii::t('app/crf', 'tick the appropriate boxes')) ?></em></small>
                            </div>
                        </div>
                        <div class="row form-inline">
                            <div class="col-lg-12">
                                <ul class="list-inline">
                                    <li>
                                        <strong>Shutdown</strong>
                                    </li>
                                    <li>
                                        <?= Html::activeRadio($model, 'axiv_sd', ['onclick'=>'clicked(this)', 'id'=>'chk_axiv_sd', 'value'=>2, 'uncheck'=>null, 'label'=>'Automatic']) ?>
                                    </li>
                                    <li>
                                        <?= Html::activeRadio($model, 'axiv_sd', ['onclick'=>'clicked(this)', 'id'=>'chk_axiv_sd', 'value'=>1, 'uncheck'=>null, 'label'=>'Manual']) ?>
                                    </li>
                                    <li>
                                        <?= Html::activeRadio($model, 'axiv_sd', ['onclick'=>'clicked(this)', 'id'=>'chk_axiv_sd', 'value'=>0, 'uncheck'=>null, 'label'=>'Not taken']) ?>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="row form-inline">
                            <div class="col-lg-12">
                                <ul class="list-inline">
                                    <li>
                                        <strong>Blowdown</strong>
                                    </li>
                                    <li>
                                        <?= Html::activeRadio($model, 'axiv_bd', ['onclick'=>'clicked(this)', 'id'=>'chk_axiv_bd', 'value'=>2, 'uncheck'=>null, 'label'=>'Automatic']) ?>
                                    </li>
                                    <li>
                                        <?= Html::activeRadio($model, 'axiv_bd', ['onclick'=>'clicked(this)', 'id'=>'chk_axiv_bd', 'value'=>1, 'uncheck'=>null, 'label'=>'Manual']) ?>
                                    </li>
                                    <li>
                                        <?= Html::activeRadio($model, 'axiv_bd', ['onclick'=>'clicked(this)', 'id'=>'chk_axiv_bd', 'value'=>0, 'uncheck'=>null, 'label'=>'Not taken']) ?>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="row form-inline">
                            <div class="col-lg-12">
                                <ul class="list-inline">
                                    <li>
                                        <strong>Deluge</strong>
                                    </li>
                                    <li>
                                        <?= Html::activeRadio($model, 'axiv_d', ['onclick'=>'clicked(this)', 'id'=>'chk_axiv_d', 'value'=>2, 'uncheck'=>null, 'label'=>'Automatic']) ?>
                                    </li>
                                    <li>
                                        <?= Html::activeRadio($model, 'axiv_d', ['onclick'=>'clicked(this)', 'id'=>'chk_axiv_d', 'value'=>1, 'uncheck'=>null, 'label'=>'Manual']) ?>
                                    </li>
                                    <li>
                                        <?= Html::activeRadio($model, 'axiv_d', ['onclick'=>'clicked(this)', 'id'=>'chk_axiv_d', 'value'=>0, 'uncheck'=>null, 'label'=>'Not taken']) ?>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="row form-inline">
                            <div class="col-lg-12">
                                <ul class="list-inline">
                                    <li>
                                        <strong>CO<sub>2</sub>/Halon/inerts</strong>
                                    </li>
                                    <li>
                                        <?= Html::activeRadio($model, 'axiv_co2', ['onclick'=>'clicked(this)', 'id'=>'chk_axiv_co2', 'value'=>2, 'uncheck'=>null, 'label'=>'Automatic']) ?>
                                    </li>
                                    <li>
                                        <?= Html::activeRadio($model, 'axiv_co2', ['onclick'=>'clicked(this)', 'id'=>'chk_axiv_co2', 'value'=>1, 'uncheck'=>null, 'label'=>'Manual']) ?>
                                    </li>
                                    <li>
                                        <?= Html::activeRadio($model, 'axiv_co2', ['onclick'=>'clicked(this)', 'id'=>'chk_axiv_co2', 'value'=>0, 'uncheck'=>null, 'label'=>'Not taken']) ?>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="row form-inline">
                            <div class="col-lg-12">
                                <ul class="list-inline">
                                    <li>
                                        <strong>Call to muster</strong>
                                    </li>
                                    <li>
                                        <?= Html::activeRadio($model, 'axiv_ctm', ['onclick'=>'clicked(this)', 'id'=>'chk_axiv_ctm', 'value'=>2, 'uncheck'=>null, 'label'=>'At stations']) ?>
                                    </li>
                                    <li>
                                        <?= Html::activeRadio($model, 'axiv_ctm', ['onclick'=>'clicked(this)', 'id'=>'chk_axiv_ctm', 'value'=>1, 'uncheck'=>null, 'label'=>'At lifeboats']) ?>
                                    </li>
                                    <li>
                                        <?= Html::activeRadio($model, 'axiv_ctm', ['onclick'=>'clicked(this)', 'id'=>'chk_axiv_ctm', 'value'=>0, 'uncheck'=>null, 'label'=>'Not taken']) ?>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  <!--end section A.1.XIV -->


    <?php ActiveForm::end(); ?>

</div>
