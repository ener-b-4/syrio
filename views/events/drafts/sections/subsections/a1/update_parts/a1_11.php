<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

$p = realpath(__DIR__ . DIRECTORY_SEPARATOR . '../load_units.php');
require_once $p;

?>

<div class="section1-form">

    <?php $form = ActiveForm::begin(); ?>

            <!--
            SECTION A.1.XI
            -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="content">
                        <div class="row">
                            <div class="col-lg-12">
                                <strong>XI. Cause of leak</strong>
                                <div>
                                    Please give a short description and complete the information below
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <?= $form->field($model, 'axi_description')->textInput(['maxlength' => 512])->label('Description') ?>
                            </div>
                        </div>
                        <div class='row'>
                            <div class="col-lg-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading"><strong>Indicate the cause(s) of the release</strong></div>
                                    <div class="panel-body">
                                        <ul class="list-group">
                                            <li class='list-group-item'>
                                                <div class='form-inline'>
                                                    <span>(a) Design</span>
                                                    <?= $form->field($model,'a1_fail_design_code')->dropDownList($failureDesign, ['num_code'=>'cause'])->label(''); ?>
                                                </div>
                                            </li>
                                            <li class='list-group-item'>
                                                <div class='form-inline'>
                                                    <ul class="list-inline">
                                                        <li>
                                                            <span>(b) Equipment</span>
                                                        </li>
                                                        <li>
                                                            <?= $form->field($model,'a1_fail_equipment_code')->dropDownList($failureEquipment, ['num_code'=>'cause', 'onchange'=>'toggle(this, 2)', 'id'=>'a1_fail_equipment_code'])->label(''); ?>
                                                        </li>
                                                        <li>
                                                            <span id='a1_fail_equipment_code_toggle'>
                                                                <?= $form->field($model, 'a1_fail_equipment_specify')->textInput(['maxlength' => 128])->label('') ?>
                                                                <span><em>please specify</em></span>
                                                            </span>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                            <li class='list-group-item'>
                                                <div class='form-inline'>
                                                    <ul class="list-inline">
                                                        <li>
                                                            <span>(c) Operation</span>
                                                        </li>
                                                        <li>
                                                            <?= $form->field($model,'a1_fail_operation_code')->dropDownList($failureOperation, ['num_code'=>'cause', 'onchange'=>'toggle(this, 2)', 'id'=>'a1_fail_operation_code'])->label(''); ?>
                                                        </li>
                                                        <li>
                                                            <span id='a1_fail_operation_code_toggle'>
                                                                <?= $form->field($model, 'a1_fail_operation_specify')->textInput(['maxlength' => 128])->label('') ?>
                                                                <span><em>please specify</em></span>
                                                            </span>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                            <li class='list-group-item'>
                                                <div class='form-inline'>
                                                    <ul class="list-inline">
                                                        <li>
                                                            <span>(d) Procedural</span>
                                                        </li>
                                                        <li>
                                                            <?= $form->field($model,'a1_fail_procedural_code')->dropDownList($failureProcedural, ['num_code'=>'cause', 'onchange'=>'toggle(this, 2)', 'id'=>'a1_fail_procedural_code'])->label(''); ?>
                                                        </li>
                                                        <li>
                                                            <span id='a1_fail_procedural_code_toggle'>
                                                                <?= $form->field($model, 'a1_fail_procedural_specify')->textInput(['maxlength' => 128])->label('') ?>
                                                                <span><em>please specify</em></span>
                                                            </span>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            
                            
                                <div class="panel panel-default">
                                    <div class="panel-heading"><strong>Operational mode in the area at the time of release</strong></div>


                                    <div class="panel-body">
                                        <div>
                                            Indicate the operational mode in the area at the time of released
                                            <span class="hint-block"><em>Choose <u>one</u> parameter from the list below</em></span>
                                            <ul class='list-unstyled'>
                                                <li>
                                                      <?= Html::activeRadio($model, 'a1_op', ['onclick'=>'clicked(this)', 'id'=>'rb_a1op', 'value'=>601, 'uncheck'=>null, 'label'=>'Drilling']) ?>
                                                </li>
                                                <li class='form-inline'>
                                                      <?= Html::activeRadio($model, 'a1_op', ['onclick'=>'clicked(this)', 'id'=>'rb_a1op', 'value'=>602, 'uncheck'=>null, 'label'=>'Well operations']) ?>
                                                      <?= $form->field($model, 'a1_op_specify')->textInput(['maxlength' => 64, 'id'=>'txt_a1_op_specify'])->label('') ?>
                                                </li>
                                                <li>
                                                      <?= Html::activeRadio($model, 'a1_op', ['onclick'=>'clicked(this)', 'id'=>'rb_a1op', 'value'=>603, 'uncheck'=>null, 'label'=>'Production']) ?>
                                                </li>
                                                <li>
                                                      <?= Html::activeRadio($model, 'a1_op', ['onclick'=>'clicked(this)', 'id'=>'rb_a1op', 'value'=>604, 'uncheck'=>null, 'label'=>'Maintenance']) ?>
                                                </li>
                                                <li>
                                                      <?= Html::activeRadio($model, 'a1_op', ['onclick'=>'clicked(this)', 'id'=>'rb_a1op', 'value'=>605, 'uncheck'=>null, 'label'=>'Construction']) ?>
                                                </li>
                                                <li>
                                                      <?= Html::activeRadio($model, 'a1_op', ['onclick'=>'clicked(this)', 'id'=>'rb_a1op', 'value'=>606, 'uncheck'=>null, 'label'=>'Pipeline operations including pigging']) ?>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            
                            
                            </div>
                        </div>
                    </div>
                </div>
            </div>  <!--end section A.1.XI -->

    <?php ActiveForm::end(); ?>

</div>
