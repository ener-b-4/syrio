<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

$p = realpath(__DIR__ . DIRECTORY_SEPARATOR . '../load_units.php');
require_once $p;

?>

<div class="section1-form">

    <?php $form = ActiveForm::begin(); ?>

            <!--
            SECTION A.1.XII
            -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="content">
                        <div class="row form-inline">
                            <div class="col-lg-12 ">
                                <ul class="list-inline">
                                    <li>
                                        <strong>XII. Did ignition occur?</strong>
                                    </li>
                                    <li>
                                        <?= Html::activeRadio($model, 'axii', ['onclick'=>'clicked(this)', 'id'=>'rb_axii_yes', 'value'=>1, 'uncheck'=>null, 'label'=>'Yes']) ?>
                                    </li>
                                    <li>
                                        <?= Html::activeRadio($model, 'axii', ['onclick'=>'clicked(this)', 'id'=>'rb_axii_no', 'value'=>0, 'uncheck'=>null, 'label'=>'No']) ?>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="row form-inline" id="ignition_div">
                            <div class="col-lg-12 col-lg-offset-1">
                                <ul class='list-unstyled'>
                                    <li>
                                        <ul class="list-inline">
                                            <li>
                                                Ignition was:
                                            </li>
                                            <li>
                                                <?= Html::activeRadio($model, 'axii_code', ['onclick'=>'clicked(this)', 'id'=>'rb_axii_code_immediate', 'value'=>1, 'uncheck'=>null, 'label'=>'Immediate']) ?>
                                            </li>
                                            <li>
                                                <?= Html::activeRadio($model, 'axii_code', ['onclick'=>'clicked(this)', 'id'=>'rb_axii_code_delayed', 'value'=>0, 'uncheck'=>null, 'label'=>'Delayed']) ?>
                                            </li>
                                            <li>
                                                <span id="rb_axii_code_delayed_toggle">
                                                    <?= $form->field($model, 'axii_code_time')->textInput()->label(Yii::t('app/crf', 'Delay time')) ?>
                                                    <em>(sec)</em>
                                                </span>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <div>
                                            <?= Yii::t('app/crf', 'Was there') ?>:
                                            <em>(add sequence of events by numbering appropriate boxes in order of occurrence)</em>
                                        </div>
                                        <ul class="list-inline">
                                            <li>
                                                <?= $form->field($model, 'axii_flash_index')->textInput(['maxlength' => 1, 'style'=>'width: 40px'])->label('Flash fire') ?>
                                            </li>
                                            <li>
                                                <?= $form->field($model, 'axii_exp_index')->textInput(['maxlength' => 1, 'style'=>'width: 40px'])->label('Explosion') ?>
                                            </li>
                                            <li>
                                                <?= $form->field($model, 'axii_jet_index')->textInput(['maxlength' => 1, 'style'=>'width: 40px'])->label('Jet fire') ?>
                                            </li>
                                            <li>
                                                <?= $form->field($model, 'axii_pool_index')->textInput(['maxlength' => 1, 'style'=>'width: 40px'])->label('Pool fire') ?>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>  <!--end section A.1.XII -->

    <?php ActiveForm::end(); ?>

</div>
