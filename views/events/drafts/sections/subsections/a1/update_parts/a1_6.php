<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

$p = realpath(__DIR__ . DIRECTORY_SEPARATOR . '../load_units.php');
require_once $p;

?>

<div class="section1-form">

    <?php $form = ActiveForm::begin(); ?>

            <!--
            SECTION A.1.VI
            -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="content">
                        <div class="row">
                            <div class="col-lg-12">
                                <strong>VI. Hazardous area classification</strong><em>(i.e. zone at location of incident)</em>
                            </div>
                        </div>
                        <div class="row form-inline">
                            <div class="col-lg-12">
                                <ul class="list-inline">
                                    <li>
                                        <em><?= Yii::t('app', 'Tick appropriate box') ?></em>
                                    </li>
                                    <li>
                                        <?= Html::activeRadio($model, 'a1vi_code', ['onclick'=>'clicked(this)', 'id'=>'chk_a1i_vi_code', 'value'=>1, 'uncheck'=>null, 'label'=>'1']) ?>
                                    </li>
                                    <li>
                                        <?= Html::activeRadio($model, 'a1vi_code', ['onclick'=>'clicked(this)', 'id'=>'chk_a1i_vi_code', 'value'=>2, 'uncheck'=>null, 'label'=>'2']) ?>
                                    </li>
                                    <li>
                                        <?= Html::activeRadio($model, 'a1vi_code', ['onclick'=>'clicked(this)', 'id'=>'chk_a1i_vi_code', 'value'=>3, 'uncheck'=>null, 'label'=>'Unclassified']) ?>
                                    </li>
                                </ul>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>


    <?php ActiveForm::end(); ?>

</div>
