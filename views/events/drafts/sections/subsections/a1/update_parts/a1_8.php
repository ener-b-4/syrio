<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

$p = realpath(__DIR__ . DIRECTORY_SEPARATOR . '../load_units.php');
require_once $p;

?>

<div class="section1-form">

    <?php $form = ActiveForm::begin(); ?>

            <!--
            SECTION A.1.VIII
            -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="content">
                        <div class="row">
                            <div class="col-lg-12">
                                <strong>VIII. Weather conditions</strong>
                            </div>
                        </div>
                        <div class="row form-inline">
                            <div class="col-lg-12 col-lg-offset-1">
                                <ul class="list-unstyled">
                                    <li>
                                        <?= $form->field($model, 'a1viii_uw_q')->textInput()->label('Wind speed') ?>
                                        <?= $form->field($model,'a1viii_uw_u')->dropDownList($speedUnits, ['id'=>'unit', 'style'=>'vertical-align: top;'])->label(''); ?>
                                    </li>
                                    <li>
                                        <?= $form->field($model, 'a1viii_dw_q')->textInput()->label('Wind direction') ?>
                                        <span><em><?= \Yii::t('app','(heading, decimal degrees)') ?></em></span>
                                    </li>
                                    <li>
                                        <p style="margin-top:12px;">
                                            Provide a description of other relevant weather conditions
                                        </p>
                                        <div class="row">
                                            <div class="col-lg-10">
                                                <?= $form->field($model, 'a1viii_c')->textarea(['rows' => 6, 'cols'=>50])->label('') ?>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  <!--end section A.1.VIII -->

    <?php ActiveForm::end(); ?>

</div>
