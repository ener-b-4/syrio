<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

$p = realpath(__DIR__ . DIRECTORY_SEPARATOR . '../load_units.php');
require_once $p;

?>

<div class="section1-form">

    <?php $form = ActiveForm::begin(); ?>

            <!--
            SECTION A.1.VII
            -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="content">
                        <div class="row form-inline">
                            <div class="col-lg-12">
                                <ul class="list-inline">
                                    <li>
                                        <strong>VII. <?= Yii::t('app', 'Module ventilation') . '?' ?></strong>
                                    </li>
                                    <li>
                                        <?= Html::activeRadio($model, 'a1vii_code', ['onclick'=>'clicked(this)', 'id'=>'chk_a1vii_code_300', 'value'=>300, 'uncheck'=>null, 'label'=>Yii::t('app', 'Natural')]) ?>
                                    </li>
                                    <li>
                                        <?= Html::activeRadio($model, 'a1vii_code', ['onclick'=>'clicked(this)', 'id'=>'chk_a1vii_code_301', 'value'=>301, 'uncheck'=>null, 'label'=>Yii::t('app', 'Forced')]) ?>
                                    </li>
                                    <li>
                                        <?= Html::activeRadio($model, 'a1vii_code', ['onclick'=>'clicked(this)', 'id'=>'chk_a1vii_code_2', 'value'=>2, 'uncheck'=>null, 'label'=>Yii::t('app', 'Not applicable')]) ?>
                                    </li>
                                </ul>
                            </div> 
                        </div>
                        <div class="row form-inline" id='a1_vii_x_container hiddenBlock'>
                            <div class="col-lg-12 col-lg-offset-1">
                                <ul class="list-unstyled">
                                    <li>
                                        <?= $form->field($model, 'a1viia')->textInput(['maxlength' => 1])->label('How many sides enclosed?')->hint('<em>' . Yii::t('app/crf', '(Insert the number of walls, including floor and ceiling)') . '</em>') ?>
                                    </li>
                                    <li>
                                        <?= $form->field($model, 'a1viib')->textInput()->label(Yii::t('app', 'Module volume')) ?> <span>(m<sup>3</sup>)</span>
                                    </li>
                                    <li>
                                        <?= $form->field($model, 'a1viic')->textInput()->label(Yii::t('app', 'Estimated number of air changes') . ' <em>' . Yii::t('app', '(<u>if</u> known)') . '</em>') ?>
                                        <?= $form->field($model, 'a1viid')->textInput()->label(Yii::t('app', 'Specify hourly rate')) ?><span>(m<sup>3</sup>/h)</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


    <?php ActiveForm::end(); ?>

</div>
