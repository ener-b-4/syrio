<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

$p = realpath(__DIR__ . DIRECTORY_SEPARATOR . '../load_units.php');
require_once $p;

?>

<div class="section1-form">

    <?php $form = ActiveForm::begin(); ?>

            <!--
            SECTION A.1.III
            -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="content">
                        <div class="row form-inline">                            
                            <div class="col-lg-12">
                                <ul class="list-inline">
                                    <li>
                                        <?= $form->field($model, 'a1iii_q')->textInput()->label('III. Estimated initial release rate') ?>
                                    </li>
                                    <li>
                                        <?= $form->field($model,'a1iii_u')->dropDownList($massRateUnits, ['id'=>'unit'])->label(''); ?>
                                    </li>
                                </ul>
                                <h5>
                                </h5>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>


    <?php ActiveForm::end(); ?>

</div>
