<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\crf\Section1 */

$this->title = Yii::t('app', 'Create {evt}', [
    'modelClass' => 'Section1',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Section1s'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="section1-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php
    $this->registerCssFile('css/Section1.css');    
    $this->registerJsFile('js/Section1.js');
?>