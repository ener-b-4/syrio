<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\web\UrlManager;

/* @var $this yii\web\View */
/* @var $model app\models\crf\Section1 */

?>
<div class="section1-view">

    <p>
        <?php
            //$path = UrlManager::createUrl('crf/section-1');
        ?>
        
        <?= Html::a(Yii::t('app', 'Modify'), ['/crf/section1/update', 'id' => $model->id], ['class' => 'btn btn-primary', 'style' => 'color: #fff']) ?>
    </p>

    <div class="col-lg-12" style="padding-top: 16px; padding-left: -12px; padding-right: -12px;">
        <?php
            if ($model->a1==0)
            {
                include_once 'view_parts/_noRelease.php';
            }
            else
            {
                include_once 'view_parts/_withRelease.php';
            }
        ?>
    </div>

</div>
