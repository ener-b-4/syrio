<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

include_once '/load_units.php';

/* @var $this yii\web\View */
/* @var $model app\models\crf\Section1 */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="section1-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-lg-10">
            <h4>A.1. Was there another release of hydrocarbon substances?</h4>
        </div>
        <div>
            <?= Html::activeRadio($model, 'a1', ['onclick'=>'clicked(this)', 'id'=>'radio1', 'value'=>1, 'uncheck'=>null, 'label'=>'Yes']) ?>
            <?= Html::activeRadio($model, 'a1', ['onclick'=>'clicked(this)', 'id'=>'radio2', 'value'=>0, 'uncheck'=>null, 'label'=>'No']) ?>
        </div>
    </div>

    <div class="row hiddenBlock" id="a1_content">
        <div class="col-lg-12">
            <div class="text-muted">
                Please fill in the following sections
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="content">
                        <div class="row">                            
                            <div class="col-lg-12">
                                <h5>
                                    <span class="strong">I. Hydrocarbon (HC) released</span> <span class="small em">(<?= Yii::t('app', 'Tick appropriate box') ?>)</span>                                    
                                </h5>
                            </div> 
                        </div>
                        <div class="row">
                            <div class="col-lg-10 col-lg-offset-1 form-inline">
                                <ul class="list-inline">
                                    <li>
                                        NON-PROCESS                                    
                                    </li>
                                    <li>
                                        <?= Html::activeCheckbox($model, 'a1i_np', ['onclick'=>'clicked(this)', 'id'=>'chkNp', 'value'=>1, 'uncheck'=>0, 'label'=>'']) ?>
                                    </li>
                                    <li>
                                        <span id='a1i_specify_container'>
                                            <?= $form->field($model, 'a1i_np_specify')->textInput(['maxlength' => 255])->label('') ?>
                                            <span><em>(please specify)</em></span>
                                        </span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-10 col-lg-offset-1 form-inline">
                                <ul class="list-inline">
                                    <li>
                                        PROCESS
                                    </li>
                                    <li>
                                        <?= Html::activeCheckbox($model, 'a1i_o', ['onclick'=>'clicked(this)', 'id'=>'chkO', 'value'=>1, 'uncheck'=>0, 'label'=>'Oil']) ?>
                                    </li>
                                    <li>
                                        <?= Html::activeCheckbox($model, 'a1i_co', ['onclick'=>'clicked(this)', 'id'=>'chkCo', 'value'=>1, 'uncheck'=>0, 'label'=>'Condensate']) ?>
                                    </li>
                                    <li>
                                        <?= Html::activeCheckbox($model, 'a1i_g', ['onclick'=>'clicked(this)', 'id'=>'chkG', 'value'=>1, 'uncheck'=>0, 'label'=>'Gas']) ?>
                                    </li>
                                    <li>
                                        <?= Html::activeCheckbox($model, 'a1i_2p', ['onclick'=>'clicked(this)', 'id'=>'chk2p', 'value'=>1, 'uncheck'=>0, 'label'=>'2-Phase']) ?>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-10 col-lg-offset-1 form-inline" id='h2s_container'>
                                <ul class="list-inline">
                                    <li>
                                        Level of H<sub>2</sub>S
                                    </li>
                                    <li>
                                        <?= $form->field($model, 'a1i_2p_h2s_q')->textInput()->label('') ?>
                                    </li>
                                    <li>
                                        <em>(estimated ppm)</em>
                                    </li>
                                    <li>
                                        this is NOT in the report <?= $form->field($model, 'a1i_2p_h2s_u')->textInput()->label('') ?>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!--
            SECTION A.1.II
            -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="content">
                        <div class="row form-inline">                            
                            <div class="col-lg-12">
                                <ul class="list-inline">
                                    <li>
                                        <?= $form->field($model, 'a1ii_q')->textInput()->label('II. Estimated quantity released') ?>
                                    </li>
                                    <li>
                                        <?= $form->field($model,'a1ii_u')->dropDownList($massUnits, ['id'=>'unit'])->label(''); ?>
                                    </li>
                                </ul>
                                <h5>
                                </h5>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>

            
            <!--
            SECTION A.1.III
            -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="content">
                        <div class="row form-inline">                            
                            <div class="col-lg-12">
                                <ul class="list-inline">
                                    <li>
                                        <?= $form->field($model, 'a1iii_q')->textInput()->label('III. Estimated initial release rate') ?>
                                    </li>
                                    <li>
                                        <?= $form->field($model,'a1iii_u')->dropDownList($massRateUnits, ['id'=>'unit'])->label(''); ?>
                                    </li>
                                </ul>
                                <h5>
                                </h5>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>

            
            <!--
            SECTION A.1.IV
            -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="content">
                        <div class="row form-inline">                            
                            <div class="col-lg-12">
                                <ul class="list-inline">
                                    <li>
                                        <?= $form->field($model, 'a1iv_q')->textInput()->label('IV. Duration of leak') ?>
                                    </li>
                                    <li>
                                        <?= $form->field($model,'a1iv_u')->dropDownList($timeUnits, ['id'=>'unit'])->label(''); ?>
                                    </li>
                                </ul>
                                <div class="col-lg-12 help-block "  style="margin-top: -12px;">
                                    <em>(Estimated time from discovery, e.g. alarm, electronic log, to termination of leak)</em>
                                </div>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>

            <!--
            SECTION A.1.V
            -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="content">
                        <div class="row form-inline">                            
                            <div class="col-lg-12">
                                <ul class="list-inline">
                                    <li>
                                        <?= $form->field($model, 'a1v')->textInput(['maxlength' => 512])->label('V. Location of leak') ?>
                                    </li>
                                </ul>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>

            <!--
            SECTION A.1.VI
            -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="content">
                        <div class="row">
                            <div class="col-lg-12">
                                <strong>VI. Hazardous area classification</strong><em>(i.e. zone at location of incident)</em>
                            </div>
                        </div>
                        <div class="row form-inline">
                            <div class="col-lg-12">
                                <ul class="list-inline">
                                    <li>
                                        <em>(<?= \Yii::t('app', 'Tick appropriate box') ?>)</em>
                                    </li>
                                    <li>
                                        <?= Html::activeRadio($model, 'a1vi_code', ['onclick'=>'clicked(this)', 'id'=>'chk_a1i_vi_code', 'value'=>1, 'uncheck'=>null, 'label'=>'1']) ?>
                                    </li>
                                    <li>
                                        <?= Html::activeRadio($model, 'a1vi_code', ['onclick'=>'clicked(this)', 'id'=>'chk_a1i_vi_code', 'value'=>2, 'uncheck'=>null, 'label'=>'2']) ?>
                                    </li>
                                    <li>
                                        <?= Html::activeRadio($model, 'a1vi_code', ['onclick'=>'clicked(this)', 'id'=>'chk_a1i_vi_code', 'value'=>3, 'uncheck'=>null, 'label'=>'Unclassified']) ?>
                                    </li>
                                </ul>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>
            
            <!--
            SECTION A.1.VII
            -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="content">
                        <div class="row form-inline">
                            <div class="col-lg-12">
                                <ul class="list-inline">
                                    <li>
                                        <strong>VII. Module ventilation?</strong>
                                    </li>
                                    <li>
                                        <?= Html::activeRadio($model, 'a1vii_code', ['onclick'=>'clicked(this)', 'id'=>'chk_a1vii_code_300', 'value'=>300, 'uncheck'=>null, 'label'=>'Natural']) ?>
                                    </li>
                                    <li>
                                        <?= Html::activeRadio($model, 'a1vii_code', ['onclick'=>'clicked(this)', 'id'=>'chk_a1vii_code_301', 'value'=>301, 'uncheck'=>null, 'label'=>'Forced']) ?>
                                    </li>
                                    <li>
                                        <?= Html::activeRadio($model, 'a1vii_code', ['onclick'=>'clicked(this)', 'id'=>'chk_a1vii_code_2', 'value'=>2, 'uncheck'=>null, 'label'=>'Not applicable']) ?>
                                    </li>
                                </ul>
                            </div> 
                        </div>
                        <div class="row form-inline" id='a1_vii_x_container'>
                            <div class="col-lg-12 col-lg-offset-1">
                                <ul class="list-unstyled">
                                    <li>
                                        <?= $form->field($model, 'a1viia')->textInput(['maxlength' => 1])->label('How many sides enclosed?')->hint('<em>' . Yii::t('app/crf', '(Insert the number of walls, including floor and ceiling)') . '</em>') ?>
                                    </li>
                                    <li>
                                        <?= $form->field($model, 'a1viib')->textInput()->label('Module volume') ?> <span>(m<sup>3</sup>)</span>
                                    </li>
                                    <li>
                                        <?= $form->field($model, 'a1viic')->textInput()->label('Estimated number of air changes <em>(<u>if</u> known)</em>') ?>
                                        <?= $form->field($model, 'a1viic')->textInput()->label('Specify hourly rate') ?><span>(m<sup>3</sup>/h)</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            

            <!--
            SECTION A.1.VIII
            -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="content">
                        <div class="row">
                            <div class="col-lg-12">
                                <strong>VIII. Weather conditions</strong>
                            </div>
                        </div>
                        <div class="row form-inline">
                            <div class="col-lg-12 col-lg-offset-1">
                                <ul class="list-unstyled">
                                    <li>
                                        <?= $form->field($model, 'a1viii_uw_q')->textInput()->label('Wind speed') ?>
                                        <?= $form->field($model,'a1viii_uw_u')->dropDownList($speedUnits, ['id'=>'unit', 'style'=>'vertical-align: top;'])->label(''); ?>
                                    </li>
                                    <li>
                                        <?= $form->field($model, 'a1viii_dw_q')->textInput()->label('Wind direction') ?>
                                        <span><em>(heading, decimal degrees)</em></span>
                                    </li>
                                    <li>
                                        <p style="margin-top:12px;">
                                            Provide a description of other relevant weather conditions
                                        </p>
                                        <div class="row">
                                            <div class="col-lg-10">
                                                <?= $form->field($model, 'a1viii_c')->textarea(['rows' => 6, 'cols'=>50])->label('') ?>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  <!--end section A.1.VIII -->
            
            <!--
            SECTION A.1.IX
            -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="content">
                        <div class="row">
                            <div class="col-lg-12">
                                <strong>IX. System pressure</strong>
                            </div>
                        </div>
                        <div class="row form-inline">
                            <div class="col-lg-5 col-lg-offset-1">
                                <ul class="list-unstyled">
                                    <li>
                                        <?= $form->field($model, 'a1ix_a_q')->textInput()->label('Design pressure') ?>
                                        <?= $form->field($model,'a1ix_a_u')->dropDownList($pressureUnits, ['id'=>'unit'])->label(''); ?>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-lg-5">
                                <ul class="list-unstyled">
                                    <li>
                                        <?= $form->field($model, 'a1ix_b_q')->textInput()->label('Actual pressure') ?>
                                        <?= $form->field($model,'a1ix_b_u')->dropDownList($pressureUnits, ['id'=>'unit'])->label(''); ?>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  <!--end section A.1.X -->
            
            <!--
            SECTION A.1.X
            -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="content">
                        <div class="row">
                            <div class="col-lg-12">
                                <strong>X. Means of detection</strong>
                            </div>
                        </div>
                        <div class="row form-inline">
                            <div class="col-lg-12 col-lg-offset-1">
                                <ul class="list-unstyled">
                                    <li>
                                        <?= Html::activeCheckbox($model, 'a1x_a', ['onclick'=>'clicked(this)', 'id'=>'chk_a1x_a', 'value'=>1, 'uncheck'=>null, 'label'=>'Fire']) ?>
                                    </li>
                                    <li>
                                        <?= Html::activeCheckbox($model, 'a1x_b', ['onclick'=>'clicked(this)', 'id'=>'chk_a1x_b', 'value'=>1, 'uncheck'=>null, 'label'=>'Gas']) ?>
                                    </li>
                                    <li>
                                        <?= Html::activeCheckbox($model, 'a1x_c', ['onclick'=>'clicked(this)', 'id'=>'chk_a1x_c', 'value'=>1, 'uncheck'=>null, 'label'=>'Smoke']) ?>
                                    </li>
                                    <li>
                                        <ul class="list-inline">
                                            <li>
                                                <?= Html::activeCheckbox($model, 'a1x_d', ['onclick'=>'clicked(this)', 'id'=>'chk_a1x_d', 'value'=>1, 'uncheck'=>null, 'label'=>'Other']) ?>
                                            </li>
                                            <li>
                                                <span id="chk_a1x_d_container">
                                                    <?= $form->field($model, 'a1x_d_specify')->textInput(['maxlength' => 255])->label('') ?>
                                                    <em>(please specify)</em>
                                                </span>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  <!--end section A.1.X -->

            
            <!--
            SECTION A.1.XI
            -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="content">
                        <div class="row">
                            <div class="col-lg-12">
                                <strong>XI. Cause of leak</strong>
                                <div>
                                    Please give a short description and complete the information below
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <?= $form->field($model, 'axi_description')->textInput(['maxlength' => 512])->label('Description') ?>
                            </div>
                        </div>
                        <div class='row'>
                            <div class="col-lg-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading"><strong>Indicate the cause(s) of the release</strong></div>
                                    <div class="panel-body">
                                        <ul class="list-group">
                                            <li class='list-group-item'>
                                                <div class='form-inline'>
                                                    <span>(a) Design</span>
                                                    <?= $form->field($model,'a1_fail_design_code')->dropDownList($failureDesign, ['num_code'=>'cause'])->label(''); ?>
                                                </div>
                                            </li>
                                            <li class='list-group-item'>
                                                <div class='form-inline'>
                                                    <ul class="list-inline">
                                                        <li>
                                                            <span>(b) Equipment</span>
                                                        </li>
                                                        <li>
                                                            <?= $form->field($model,'a1_fail_equipment_code')->dropDownList($failureEquipment, ['num_code'=>'cause', 'onchange'=>'toggle(this, 2)', 'id'=>'a1_fail_equipment_code'])->label(''); ?>
                                                        </li>
                                                        <li>
                                                            <span id='a1_fail_equipment_code_toggle'>
                                                                <?= $form->field($model, 'a1_fail_equipment_specify')->textInput(['maxlength' => 128])->label('') ?>
                                                                <span><em>please specify</em></span>
                                                            </span>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                            <li class='list-group-item'>
                                                <div class='form-inline'>
                                                    <ul class="list-inline">
                                                        <li>
                                                            <span>(c) Operation</span>
                                                        </li>
                                                        <li>
                                                            <?= $form->field($model,'a1_fail_operation_code')->dropDownList($failureOperation, ['num_code'=>'cause', 'onchange'=>'toggle(this, 2)', 'id'=>'a1_fail_operation_code'])->label(''); ?>
                                                        </li>
                                                        <li>
                                                            <span id='a1_fail_operation_code_toggle'>
                                                                <?= $form->field($model, 'a1_fail_operation_specify')->textInput(['maxlength' => 128])->label('') ?>
                                                                <span><em>please specify</em></span>
                                                            </span>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                            <li class='list-group-item'>
                                                <div class='form-inline'>
                                                    <ul class="list-inline">
                                                        <li>
                                                            <span>(d) Procedural</span>
                                                        </li>
                                                        <li>
                                                            <?= $form->field($model,'a1_fail_procedural_code')->dropDownList($failureProcedural, ['num_code'=>'cause', 'onchange'=>'toggle(this, 2)', 'id'=>'a1_fail_procedural_code'])->label(''); ?>
                                                        </li>
                                                        <li>
                                                            <span id='a1_fail_procedural_code_toggle'>
                                                                <?= $form->field($model, 'a1_fail_procedural_specify')->textInput(['maxlength' => 128])->label('') ?>
                                                                <span><em>please specify</em></span>
                                                            </span>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            
                            
                                <div class="panel panel-default">
                                    <div class="panel-heading"><strong>Operational mode in the area at the time of release</strong></div>


                                    <div class="panel-body">
                                        <div>
                                            Indicate the operational mode in the area at the time of released
                                            <span class="hint-block"><em>Choose <u>one</u> parameter from the list below</em></span>
                                            <ul class='list-unstyled'>
                                                <li>
                                                      <?= Html::activeRadio($model, 'a1_op', ['onclick'=>'clicked(this)', 'id'=>'rb_a1op', 'value'=>601, 'uncheck'=>null, 'label'=>'Drilling']) ?>
                                                </li>
                                                <li class='form-inline'>
                                                      <?= Html::activeRadio($model, 'a1_op', ['onclick'=>'clicked(this)', 'id'=>'rb_a1op', 'value'=>602, 'uncheck'=>null, 'label'=>'Well operations']) ?>
                                                      <?= $form->field($model, 'a1_op_specify')->textInput(['maxlength' => 64, 'id'=>'txt_a1_op_specify'])->label('') ?>
                                                </li>
                                                <li>
                                                      <?= Html::activeRadio($model, 'a1_op', ['onclick'=>'clicked(this)', 'id'=>'rb_a1op', 'value'=>603, 'uncheck'=>null, 'label'=>'Production']) ?>
                                                </li>
                                                <li>
                                                      <?= Html::activeRadio($model, 'a1_op', ['onclick'=>'clicked(this)', 'id'=>'rb_a1op', 'value'=>604, 'uncheck'=>null, 'label'=>'Maintenance']) ?>
                                                </li>
                                                <li>
                                                      <?= Html::activeRadio($model, 'a1_op', ['onclick'=>'clicked(this)', 'id'=>'rb_a1op', 'value'=>605, 'uncheck'=>null, 'label'=>'Construction']) ?>
                                                </li>
                                                <li>
                                                      <?= Html::activeRadio($model, 'a1_op', ['onclick'=>'clicked(this)', 'id'=>'rb_a1op', 'value'=>606, 'uncheck'=>null, 'label'=>'Pipeline operations including pigging']) ?>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            
                            
                            </div>
                        </div>
                    </div>
                </div>
            </div>  <!--end section A.1.XI -->
            
            
            <div class="row">
                <div class="col-lg-12">
                    <div class="content">
                        <div class="row form-inline">
                            <div class="col-lg-12 ">
                                <ul class="list-inline">
                                    <li>
                                        <strong>XI. Did ignition occur?</strong>
                                    </li>
                                    <li>
                                        <?= Html::activeRadio($model, 'axii', ['onclick'=>'clicked(this)', 'id'=>'rb_axii_yes', 'value'=>1, 'uncheck'=>null, 'label'=>'Yes']) ?>
                                    </li>
                                    <li>
                                        <?= Html::activeRadio($model, 'axii', ['onclick'=>'clicked(this)', 'id'=>'rb_axii_no', 'value'=>0, 'uncheck'=>null, 'label'=>'No']) ?>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="row form-inline" id="ignition_div">
                            <div class="col-lg-12 col-lg-offset-1">
                                <ul class='list-unstyled'>
                                    <li>
                                        <ul class="list-inline">
                                            <li>
                                                Ignition was:
                                            </li>
                                            <li>
                                                <?= Html::activeRadio($model, 'axii_code', ['onclick'=>'clicked(this)', 'id'=>'rb_axii_code_immediate', 'value'=>1, 'uncheck'=>null, 'label'=>'Immediate']) ?>
                                            </li>
                                            <li>
                                                <?= Html::activeRadio($model, 'axii_code', ['onclick'=>'clicked(this)', 'id'=>'rb_axii_code_delayed', 'value'=>0, 'uncheck'=>null, 'label'=>'Delayed']) ?>
                                            </li>
                                            <li>
                                                <span id="rb_axii_code_delayed_toggle">
                                                    <?= $form->field($model, 'axii_code_time')->textInput()->label(Yii::t('app/crf', 'Delay time')) ?>
                                                    <em>(sec)</em>
                                                </span>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <div>
                                            <?= Yii::t('app/crf', 'Was there') ?>:
                                            <em>(add sequence of events by numbering appropriate boxes in order of occurrence)</em>
                                        </div>
                                        <ul class="list-inline">
                                            <li>
                                                <?= $form->field($model, 'axii_flash_index')->textInput(['maxlength' => 1, 'style'=>'width: 40px'])->label('Flash fire') ?>
                                            </li>
                                            <li>
                                                <?= $form->field($model, 'axii_exp_index')->textInput(['maxlength' => 1, 'style'=>'width: 40px'])->label('Explosion') ?>
                                            </li>
                                            <li>
                                                <?= $form->field($model, 'axii_jet_index')->textInput(['maxlength' => 1, 'style'=>'width: 40px'])->label('Jet fire') ?>
                                            </li>
                                            <li>
                                                <?= $form->field($model, 'axii_pool_index')->textInput(['maxlength' => 1, 'style'=>'width: 40px'])->label('Pool fire') ?>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>  <!--end section A.1.XII -->

            
            <!--
            SECTION A.1.XIII
            -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="content">
                        <div class="row">
                            <div class="col-lg-12">
                                <strong>XIII. Ignition source</strong> (<u>if</u> known)
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <?= $form->field($model, 'axiii')->textInput(['maxlength' => 512])->label('') ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  <!--end section A.1.XIII -->
            
            <!--
            SECTION A.1.XIV
            -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="content">
                        <div class="row">
                            <div class="col-lg-12">
                                <strong>XIV. What emergency action was taken?</strong> <small><em>(<?= ucfirst(Yii::t('app/crf', 'tick the appropriate boxes')) ?></em></small>
                            </div>
                        </div>
                        <div class="row form-inline">
                            <div class="col-lg-12">
                                <ul class="list-inline">
                                    <li>
                                        <strong>Shutdown</strong>
                                    </li>
                                    <li>
                                        <?= Html::activeRadio($model, 'axiv_sd', ['onclick'=>'clicked(this)', 'id'=>'chk_axiv_sd', 'value'=>2, 'uncheck'=>null, 'label'=>'Automatic']) ?>
                                    </li>
                                    <li>
                                        <?= Html::activeRadio($model, 'axiv_sd', ['onclick'=>'clicked(this)', 'id'=>'chk_axiv_sd', 'value'=>1, 'uncheck'=>null, 'label'=>'Manual']) ?>
                                    </li>
                                    <li>
                                        <?= Html::activeRadio($model, 'axiv_sd', ['onclick'=>'clicked(this)', 'id'=>'chk_axiv_sd', 'value'=>0, 'uncheck'=>null, 'label'=>'Not taken']) ?>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="row form-inline">
                            <div class="col-lg-12">
                                <ul class="list-inline">
                                    <li>
                                        <strong>Blowdown</strong>
                                    </li>
                                    <li>
                                        <?= Html::activeRadio($model, 'axiv_bd', ['onclick'=>'clicked(this)', 'id'=>'chk_axiv_bd', 'value'=>2, 'uncheck'=>null, 'label'=>'Automatic']) ?>
                                    </li>
                                    <li>
                                        <?= Html::activeRadio($model, 'axiv_bd', ['onclick'=>'clicked(this)', 'id'=>'chk_axiv_bd', 'value'=>1, 'uncheck'=>null, 'label'=>'Manual']) ?>
                                    </li>
                                    <li>
                                        <?= Html::activeRadio($model, 'axiv_bd', ['onclick'=>'clicked(this)', 'id'=>'chk_axiv_bd', 'value'=>0, 'uncheck'=>null, 'label'=>'Not taken']) ?>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="row form-inline">
                            <div class="col-lg-12">
                                <ul class="list-inline">
                                    <li>
                                        <strong>Deluge</strong>
                                    </li>
                                    <li>
                                        <?= Html::activeRadio($model, 'axiv_d', ['onclick'=>'clicked(this)', 'id'=>'chk_axiv_d', 'value'=>2, 'uncheck'=>null, 'label'=>'Automatic']) ?>
                                    </li>
                                    <li>
                                        <?= Html::activeRadio($model, 'axiv_d', ['onclick'=>'clicked(this)', 'id'=>'chk_axiv_d', 'value'=>1, 'uncheck'=>null, 'label'=>'Manual']) ?>
                                    </li>
                                    <li>
                                        <?= Html::activeRadio($model, 'axiv_d', ['onclick'=>'clicked(this)', 'id'=>'chk_axiv_d', 'value'=>0, 'uncheck'=>null, 'label'=>'Not taken']) ?>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="row form-inline">
                            <div class="col-lg-12">
                                <ul class="list-inline">
                                    <li>
                                        <strong>CO<sub>2</sub>/Halon/inerts</strong>
                                    </li>
                                    <li>
                                        <?= Html::activeRadio($model, 'axiv_co2', ['onclick'=>'clicked(this)', 'id'=>'chk_axiv_co2', 'value'=>2, 'uncheck'=>null, 'label'=>'Automatic']) ?>
                                    </li>
                                    <li>
                                        <?= Html::activeRadio($model, 'axiv_co2', ['onclick'=>'clicked(this)', 'id'=>'chk_axiv_co2', 'value'=>1, 'uncheck'=>null, 'label'=>'Manual']) ?>
                                    </li>
                                    <li>
                                        <?= Html::activeRadio($model, 'axiv_co2', ['onclick'=>'clicked(this)', 'id'=>'chk_axiv_co2', 'value'=>0, 'uncheck'=>null, 'label'=>'Not taken']) ?>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="row form-inline">
                            <div class="col-lg-12">
                                <ul class="list-inline">
                                    <li>
                                        <strong>Call to muster</strong>
                                    </li>
                                    <li>
                                        <?= Html::activeRadio($model, 'axiv_ctm', ['onclick'=>'clicked(this)', 'id'=>'chk_axiv_ctm', 'value'=>2, 'uncheck'=>null, 'label'=>'At stations']) ?>
                                    </li>
                                    <li>
                                        <?= Html::activeRadio($model, 'axiv_ctm', ['onclick'=>'clicked(this)', 'id'=>'chk_axiv_ctm', 'value'=>1, 'uncheck'=>null, 'label'=>'At lifeboats']) ?>
                                    </li>
                                    <li>
                                        <?= Html::activeRadio($model, 'axiv_ctm', ['onclick'=>'clicked(this)', 'id'=>'chk_axiv_ctm', 'value'=>0, 'uncheck'=>null, 'label'=>'Not taken']) ?>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  <!--end section A.1.XIII -->
            

            
            
            
        </div>
    </div>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
