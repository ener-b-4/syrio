<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */


/**
 * Performs a validation and rendering logic for section a_1_xiii
 * Render block in $a1xii_block
 *
 * @var $model app\models\crf\Section1
 * 
 * @author bogdanV
 */

/* @var $model app\models\crf\Section1 */

if (isset($arr))
{
    unset($arr);
}
$arr=[];

if (isset($model->axiii))
{
    if ($model->axiii!='')
    {
        $arr[] = 'Ignition source: ' . $model->axiii . ' s<br/>';
    }
    else
    {
        //there've been no ignition
        $arr[] = 'Ignition source: ' . 'uknown' . ' s<br/>';
    }
}
else
{
    //value not provided!
    //required!

    $a1vii_block_arr[] = ' <em class="text-warning">(not set)</em>';
}

$a1xiii_block = implode('', $arr);
unset($arr);

