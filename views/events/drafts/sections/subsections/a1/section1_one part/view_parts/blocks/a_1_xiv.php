<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */


/**
 * Performs a validation and rendering logic for section a_1_xiv
 * Render block in $a1xiv_block
 *
 * @var $model app\models\crf\Section1
 * 
 * @author bogdanV
 */

/* @var $model app\models\crf\Section1 */

if (isset($arr))
{
    unset($arr);
}
$arr=[];
$taken = 0;     //flag 1 if any emergency action is taken

    if (isset($model->axiv_sd)) 
    {
        switch ($model->axiv_sd)
        {
            case 1:
                $arr[] = 'Manual shutdown<br/>';
                $taken=1;
                break;
            case 2:
                $arr[] = 'Automatic shutdown<br/>';
                $taken=1;
                break;
            default:
        }
        switch ($model->axiv_bd)
        {
            case 1:
                $arr[] = 'Manual blowdown<br/>';
                $taken=1;
                break;
            case 2:
                $arr[] = 'Automatic blowdown<br/>';
                $taken=1;
                break;
            default:
        }
        switch ($model->axiv_d)
        {
            case 1:
                $arr[] = 'Manual deluge<br/>';
                $taken=1;
                break;
            case 2:
                $arr[] = 'Automatic deluge<br/>';
                $taken=1;
                break;
            default:
        }
        switch ($model->axiv_co2)
        {
            case 1:
                $arr[] = 'Manual CO<sub>2</sub>/Halon/inerts<br/>';
                $taken=1;
                break;
            case 2:
                $arr[] = 'Automatic CO<sub>2</sub>/Halon/inerts<br/>';
                $taken=1;
                break;
            default:
        }

        switch ($model->axiv_ctm)
        {
            case 1:
                $arr[] = 'Call to muster at stations<br/>';
                $taken=1;
                break;
            case 2:
                $arr[] = 'Call to muster at lifeboats<br/>';
                $taken=1;
                break;
            default:
        }
        
        if ($taken==0)
        {
            $arr[] = 'none taken<br/>';
        }  
    }
    else
    {
        $arr[] = 'not set';
    }
    
    $a1xiv_block = implode($arr);
    
