<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

  use yii\helpers\Html;
  use yii\widgets\ActiveForm;
  use app\models\crf\CrfHelper;
  use app\components\helpers\TArrayHelper;
  
  //use kartik\widgets\ActiveForm;

  //include_once '/load_units.php';
  include_once 'load_units.php';

  /* @var $this yii\web\View */
  /* @var $model app\models\crf\Section1 */
  /* @var $form yii\widgets\ActiveForm */
?>

<div class="section1-form">
    <!--<p>form</p> -->
    
    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-lg-10">
            <h4><?= \Yii::t('app/crf', 'A.1. Was there a release of hydrocarbon substances?') ?></h4>
        </div>
        <div class="col-lg-2">
            <?= $form->field($model, 'a1')
                ->radioList(
                    ['1' => \Yii::t('app', 'Yes'), '0' =>\Yii::t('app', 'No')],
                    [
                      'unselect' => null,
                      'item' => function($index, $label, $name, $checked, $value) {
                            $showPanel = $index == 0 ? "true" : "false";
                            $ischecked = $checked ? 'checked="checked"' : ''; // $value == '1' ? "checked " : '';
                            $setting = 'class="sy_data_toggle" '
                                    . 'sy_data_toggle_dest = "a1_content"'
                                    . 'sy_data_toggle_src_type = "radio"'
                                    . 'sy_data_toggle_visible_value = "' . $showPanel . '"' 
                                    . 'sy_data_toggle_enabled = "true"';
                    
                            $return = '<label>'
                                    . ' <input type="radio" name="' . $name . '" value="'. $value .'" ' 
                                    . $ischecked 
                                    . ' '
                                    . $setting
                                    . '>'
                                    . ' ' . $label
                                    . '</input>'
                                    . '</label>';
                            return $return;
                      }
                    ]    
                )->label(false) ?>
            
        </div>
    </div>

    <br/>
    
    <div class="row hiddenBlock" id="a1_content">
        <div class="col-lg-12">
            <div class="text-muted">
                <?= \Yii::t('app', 'Please fill in the following sections') ?>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="content">
                        <div class="row">                            
                            <div class="col-lg-12">
                                <h5>
                                    <strong>I. <?= CrfHelper::SectionsTitle()['A.1.1'] ?></strong> <em>(<?= Yii::t('app', 'Tick appropriate box') ?>)</em>                                    
                                </h5>
                            </div> 
                        </div>
                        <div class="row">
                            <div class="col-lg-10 col-lg-offset-1 form-inline">
                                <ul class="list-inline">
                                    <li>
                                        <?= Yii::t('app/crf', 'NON-PROCESS') ?>
                                    </li>
                                    <li>
                                        <?= Html::activeCheckbox($model, 'a1i_np', ['id'=>'chkNp', 'value'=>1, 'uncheck'=>0, 'label'=>'',
                                            'class' => 'sy_data_toggle',
                                            'sy_data_toggle_dest' => 'a1i_specify_container',
                                            'sy_data_toggle_src_type' => 'checkbox',
                                            'sy_data_toggle_visible_value' => 'true',
                                            'sy_data_toggle_enabled' => 'true'
                                            ]) ?>
                                    </li>
                                    <li>
                                        <span id='a1i_specify_container'>
                                            <?= $form->field($model, 'a1i_np_specify')->textInput(['maxlength' => 255])->label('') ?>
                                            <span><em>(<?= ucfirst(Yii::t('app', 'specify')) ?>)</em></span>
                                        </span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-10 col-lg-offset-1 form-inline">
                                <ul class="list-inline">
                                    <li>
                                        <?= Yii::t('app/crf', 'PROCESS') ?>
                                    </li>
                                    <li>
                                        <?= Html::activeCheckbox($model, 'a1i_o', [
                                            'id'=>'chkO', 'value'=>1, 'uncheck'=>0, 'label'=>Yii::t('app/crf', 'Oil')
                                            ]) ?>
                                    </li>
                                    <li>
                                        <?= Html::activeCheckbox($model, 'a1i_co', [
                                            'id'=>'chkCo', 'value'=>1, 'uncheck'=>0, 'label'=>Yii::t('app/crf', 'Condensate')
                                            ]) ?>
                                    </li>
                                    <li>
                                        <?= Html::activeCheckbox($model, 'a1i_g', [
                                            'id'=>'chkG', 'value'=>1, 'uncheck'=>0, 'label'=>Yii::t('app/crf', 'Gas'),
                                            ]) ?>
                                    </li>
                                    <li>
                                        <?= Html::activeCheckbox($model, 'a1i_2p', [
                                            'id'=>'chk2p', 'value'=>1, 'uncheck'=>0, 'label'=>Yii::t('app/crf', '2-Phase'),
                                            ]) ?>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-10 col-lg-offset-1 form-inline sy_toggle_dest" id='h2s_container'
                                        sy_toggle_src_dependent_on = 'chkG chk2p'>
                                <ul class="list-inline">
                                    <li>
                                        <?= \Yii::t('app/crf', 'Level of H')?><sub>2</sub>S
                                    </li>
                                    <li>
                                        <?= $form->field($model, 'a1i_2p_h2s_q')->textInput()->label('') ?>
                                    </li>
                                    <li>
                                        <em><?= Yii::t('app/crf', '(estimated ppm)') ?></em>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            
            <!--
            SECTION A.1.II
            -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="content">
                        <div class="row form-inline">                            
                            <div class="col-lg-12">
                                <?php
                                $roman = 'II. ';
                                $current = 'A.1.2';
                                $lbl = $roman . CrfHelper::SectionsTitle()[$current];
                                ?>
                                
                                <ul class="list-inline">
                                    <li>
                                        <?= $form->field($model, 'a1ii_q')->textInput()->label($lbl) ?>
                                    </li>
                                    <li>
                                        <?= $form->field($model,'a1ii_u')->dropDownList($massUnits, ['id'=>'unit'])->label(''); ?>
                                    </li>
                                </ul>
                                <h5>
                                </h5>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>

            
            <!--
            SECTION A.1.III
            -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="content">
                        <div class="row form-inline">                            
                            <div class="col-lg-12">
                                <?php
                                $roman = 'III. ';
                                $current = 'A.1.3';
                                $lbl = $roman . CrfHelper::SectionsTitle()[$current];
                                ?>
                                
                                <ul class="list-inline">
                                    <li>
                                        <?= $form->field($model, 'a1iii_q')->textInput()->label($lbl) ?>
                                    </li>
                                    <li>
                                        <?= $form->field($model,'a1iii_u')->dropDownList($massRateUnits, ['id'=>'unit'])->label(''); ?>
                                    </li>
                                </ul>
                                <h5>
                                </h5>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>

            
            <!--
            SECTION A.1.IV
            -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="content">
                        <div class="row form-inline">                            
                            <div class="col-lg-12">
                                <?php
                                $roman = 'IV. ';
                                $current = 'A.1.4';
                                $lbl = $roman . CrfHelper::SectionsTitle()[$current];
                                ?>
                                <ul class="list-inline">
                                    <li>
                                        <?= $form->field($model, 'a1iv_q')->textInput()->label($lbl) ?>
                                    </li>
                                    <li>
                                        <?= $form->field($model,'a1iv_u')->dropDownList($timeUnits, ['id'=>'unit'])->label(''); ?>
                                    </li>
                                </ul>
                                <div class="col-lg-12 help-block "  style="margin-top: -12px;">
                                    <em>(<?= \Yii::t('app/crf', 'Estimated time from discovery, e.g. alarm, electronic log, to termination of leak') ?>)</em>
                                </div>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>

            <!--
            SECTION A.1.V
            -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="content">
                        <div class="row form-inline">                            
                            <div class="col-lg-12">
                                <?php
                                $roman = 'V. ';
                                $current = 'A.1.5';
                                $lbl = $roman . CrfHelper::SectionsTitle()[$current];
                                ?>
                                <ul class="list-inline">
                                    <li>
                                        <?= $form->field($model, 'a1v')->textInput(['maxlength' => 512])->label($lbl) ?>
                                    </li>
                                </ul>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>

            <!--
            SECTION A.1.VI
            -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="content">
                        <div class="row">
                            <div class="col-lg-12" style="padding-left: 11px;">
                                <?php
                                $roman = 'VI. ';
                                $current = 'A.1.6';
                                echo '<strong>'
                                        . $roman . CrfHelper::SectionsTitle()[$current] . ': '
                                        . '</strong>'
                                        . ' <em>(' . \Yii::t('app/crf', 'i.e. zone at location of incident') . ')</em>';
                                ?>
                            </div>
                        </div>
                        <div class="row form-inline">
                            <div class="col-lg-12">
                                <ul class="list-inline">
                                    <li>
                                        <em>(<?= Yii::t('app', 'Tick appropriate box') ?>)</em>
                                    </li>
                                    <li>
                                        <?= Html::activeRadio($model, 'a1vi_code', ['onclick'=>'clicked(this)', 'id'=>'chk_a1i_vi_code', 'value'=>1, 'uncheck'=>null, 'label'=>'1']) ?>
                                    </li>
                                    <li>
                                        <?= Html::activeRadio($model, 'a1vi_code', ['onclick'=>'clicked(this)', 'id'=>'chk_a1i_vi_code', 'value'=>2, 'uncheck'=>null, 'label'=>'2']) ?>
                                    </li>
                                    <li>
                                        <?= Html::activeRadio($model, 'a1vi_code', ['onclick'=>'clicked(this)', 'id'=>'chk_a1i_vi_code', 'value'=>3, 'uncheck'=>null, 'label'=>Yii::t('app/crf', 'Unclassified')]) ?>
                                    </li>
                                </ul>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>
            
            <!--
            SECTION A.1.VII
            -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="content">
                        <div class="row form-inline">
                            <div class="col-lg-12">
                                <?php
                                $roman = 'VII. ';
                                $current = 'A.1.7';
                                $lbl = $roman . CrfHelper::SectionsTitle()[$current];
                                ?>
                                
                                <ul class="list-inline">
                                    <li>
                                        <div class="form-group">
                                            <div><strong><?= $lbl ?></strong></div>
                                            <div class="help-block"></div>
                                        </div>
                                    </li>
                                    <li>
                                        <?= $form->field($model, 'a1vii_code')
                                            ->radioList(
                                                ['300' => Yii::t('app/crf', 'Natural'), '301' =>Yii::t('app/crf', 'Forced'), '2'=>Yii::t('app/crf', 'Not applicable')],
                                                [
                                                  'unselect' => null,
                                                  'item' => function($index, $label, $name, $checked, $value) {
                                                        $showPanel = $index < 2 ? "true" : "false";
                                                        $ischecked = $checked ? 'checked="checked"' : ''; // $value == '1' ? "checked " : '';
                                                        $setting = 'class="sy_data_toggle" '
                                                                . 'sy_data_toggle_dest = "a1_vii_x_container"'
                                                                . 'sy_data_toggle_src_type = "radio"'
                                                                . 'sy_data_toggle_visible_value = "' . $showPanel . '"' 
                                                                . 'sy_data_toggle_enabled = "true"';

                                                        $return = '<label>'
                                                                . ' <input type="radio" name="' . $name . '" value="'. $value .'" ' 
                                                                . $ischecked 
                                                                . ' '
                                                                . $setting
                                                                . '>'
                                                                . ' ' . $label
                                                                . '</input>'
                                                                . '</label>';
                                                        return $return;
                                                  }
                                                ]    
                                            )->label(false) ?>                                        
                                    </li>
                                </ul>
                            </div> 
                        </div>
                        <div class="row form-inline hiddenBlock" id='a1_vii_x_container'>
                            <div class="col-lg-12 col-lg-offset-1">
                                <ul class="list-unstyled">
                                    <li>
                                        <?= $form->field($model, 'a1viia')->textInput(['maxlength' => 1])->label(Yii::t('app/crf', 'How many sides enclosed?'))->hint('<em>' . Yii::t('app/crf', '(Insert the number of walls, including floor and ceiling)') . '</em>') ?>
                                    </li>
                                    <li>
                                        <?= $form->field($model, 'a1viib')->textInput()->label(Yii::t('app/crf', 'Module volume')) ?> <span>(m<sup>3</sup>)</span>
                                    </li>
                                    <li>
                                        <?php 
                                            $label = Yii::t('app/crf', 'Estimated number of air changes') . ': <em>(' . Yii::t('app', 'if known') . ')</em>';
                                        ?>
                                        <?= $form->field($model, 'a1viic')->textInput()->label($label) ?>
                                        <br/>
                                        <?php
                                            $label = Yii::t('app/crf', 'Specify hourly rate');
                                        ?>
                                        <?= $form->field($model, 'a1viid')->textInput()->label($label) ?><span>(m<sup>3</sup>/h)</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            

            <!--
            SECTION A.1.VIII
            -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="content">
                        <div class="row">
                            <div class="col-lg-12">
                                <?php
                                $roman = 'VIII. ';
                                $current = 'A.1.8';
                                $lbl = $roman . CrfHelper::SectionsTitle()[$current];
                                ?>
                                
                                <strong><?= $lbl ?></strong>
                            </div>
                        </div>
                        <div class="row form-inline">
                            <div class="col-lg-12 col-lg-offset-1">
                                <ul class="list-unstyled">
                                    <li>
                                        <?= $form->field($model, 'a1viii_uw_q')->textInput()->label(Yii::t('app/crf', 'Wind speed')) ?>
                                        <?= $form->field($model,'a1viii_uw_u')->dropDownList($speedUnits, ['id'=>'unit', 'style'=>'vertical-align: top;'])->label(''); ?>
                                    </li>
                                    <li>
                                        <?= $form->field($model, 'a1viii_dw_q')->textInput()->label(Yii::t('app/crf', 'Wind direction')) ?>
                                        <span><em><?= \Yii::t('app','(heading, decimal degrees)') ?></em></span>
                                    </li>
                                    <li>
                                        <p style="margin-top:12px;">
                                            <?= Yii::t('app/crf', 'Provide a description of other relevant weather conditions') ?>
                                        </p>
                                        <div class="row">
                                            <div class="col-lg-10">
                                                <?= $form->field($model, 'a1viii_c')->textarea(['rows' => 6, 'cols'=>50])->label('') ?>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  <!--end section A.1.VIII -->
            
            <!--
            SECTION A.1.IX
            -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="content">
                        <div class="row">
                            <div class="col-lg-12">
                                <?php
                                $roman = 'IX. ';
                                $current = 'A.1.9';
                                $lbl = $roman . CrfHelper::SectionsTitle()[$current];
                                ?>
                                
                                <strong><?= $lbl ?></strong>
                            </div>
                        </div>
                        <div class="row form-inline">
                            <div class="col-lg-5 col-lg-offset-1">
                                <ul class="list-unstyled">
                                    <li>
                                        <?= $form->field($model, 'a1ix_a_q')->textInput()->label(Yii::t('app/crf', 'Design pressure')) ?>
                                        <?= $form->field($model,'a1ix_a_u')->dropDownList($pressureUnits, ['id'=>'unit'])->label(''); ?>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-lg-5">
                                <ul class="list-unstyled">
                                    <li>
                                        <?= $form->field($model, 'a1ix_b_q')->textInput()->label(Yii::t('app/crf', 'Actual pressure')) ?>
                                        <?= $form->field($model,'a1ix_b_u')->dropDownList($pressureUnits, ['id'=>'unit'])->label(''); ?>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  <!--end section A.1.IX -->
            
            <!--
            SECTION A.1.X
            -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="content">
                        <div class="row">
                            <div class="col-lg-12">
                                <?php
                                $roman = 'X. ';
                                $current = 'A.1.10';
                                $lbl = $roman . CrfHelper::SectionsTitle()[$current];
                                ?>
                                
                                <strong><?= $lbl ?></strong>
                            </div>
                        </div>
                        <div class="row form-inline">
                            <div class="col-lg-12 col-lg-offset-1">
                                <ul class="list-unstyled sy_pad_top_18">
                                    <li>
                                        <?= $form->field($model, 'a1x_a')->checkbox(['label'=>\Yii::t('app/crf', 'Fire')]) ?>
                                    </li>
                                    <li>
                                        <?= $form->field($model, 'a1x_b')->checkbox(['label'=>\Yii::t('app/crf', 'Gas')]) ?>
                                    </li>
                                    <li>
                                        <?= $form->field($model, 'a1x_c')->checkbox(['label'=>\Yii::t('app/crf', 'Smoke')]) ?>
                                    </li>
                                    <li>
                                        <ul class="list-inline" style="margin-left: 0px;">
                                            <li>
                                                <?= $form->field($model, 'a1x_d')->checkbox(['id'=>'chk_a1x_d', 'label'=>\Yii::t('app/crf', 'Other'), 'onclick'=>'clicked(this)']) ?>
                                            </li>
                                            <li>
                                                <span id="chk_a1x_d_container">
                                                    <?= $form->field($model, 'a1x_d_specify')->textInput(['maxlength' => 255])->label('') ?>
                                                    <em><?= \Yii::t('app/crf', '(Please specify)') ?></em>
                                                </span>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  <!--end section A.1.X -->

            
            <!--
            SECTION A.1.XI
            -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="content">
                        <div class="row">
                            <div class="col-lg-12">
                                <?php
                                $roman = 'XI. ';
                                $current = 'A.1.11';
                                $lbl = $roman . CrfHelper::SectionsTitle()[$current];
                                ?>
                                
                                <strong><?= $lbl ?></strong>
                                <div>
                                    <?= Yii::t('app/crf', '(Please give a short description and complete the "Cause" checklist below)') ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12"  style="padding-bottom: 6px; padding-top: 12px;">
                                <?= $form->field($model, 'axi_description')->textArea(['maxlength' => 512, 'rows'=>6])->label(\Yii::t('app', 'Description')) ?>
                            </div>
                        </div>
                        <div class='row'>
                            <div class="col-lg-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading"><strong><?= Yii::t('app/crf','Indicate the cause(s) of the release') ?></strong></div>
                                    <div class="panel-body">
                                        <p>
                                            <em class='text-muted'><span style='text-decoration: underline'><?= Yii::t('app/crf','From each of the following categories')?> </span>
                                            <?= ' ' . Yii::t('app/crf', 'tick the appropriate boxes') ?></em>
                                        </p>
                                        
                                        <?php
                                        $all_causes = \app\models\crf\LeakCause::find()
                                                ->where(['<>', 'category_id', 0])
                                                ->asArray()
                                                ->all();
                                        $all_causes =  TArrayHelper::tIndex($all_causes, 'category_id');

                                        $sel_causes = TArrayHelper::getColumn($model->releaseCauses, 'num_code');
                                                //->asArray()
                                                //->all();
                                        ?>
                                        
                                        <?= $this->renderAjax('update_parts/_release_causes_table', [
                                            'form' => $form,
                                            'model'=>$model,
                                            'all_causes'=>$all_causes, 
                                            'sel_causes'=>$sel_causes,
                                            'other_attr'=>[
                                                "2"=>"a1_fail_equipment",
                                                "3"=>"a1_fail_operation",
                                                "4"=>"a1_fail_procedural"
                                            ]]) ?>
                                    </div>
                                </div>
                            
                            
                                <div class="panel panel-default">
                                    <div class="panel-heading"><strong><?= Yii::t('app/crf', 'Operational mode in the area at the time of release') ?></strong></div>


                                    <div class="panel-body">
                                        <div>
                                            <?= Yii::t('app/crf', 'Indicate the operational mode in the area at the time of release') ?>
                                            <p>
                                                <span class="hint-block">
                                                    <em>
                                                        <?= ' ' . Yii::t('app', 'Choose') ?>
                                                        <span style='text-decoration: underline'><?= ' ' . Yii::t('app','one')?> </span>
                                                        <?= ' ' . Yii::t('app', 'parameter from the following categories, and tick the appropriate boxes') ?>
                                                    </em>
                                                </span>
                                            </p>
                                            
                                            <ul class='list-unstyled'>
                                                <li>
                                                      <?= Html::activeRadio($model, 'a1_op', ['onclick'=>'clicked(this)', 'id'=>'rb_a1op', 'value'=>600, 'uncheck'=>null, 'label'=>\Yii::t('app/crf', 'Drilling')]) ?>
                                                </li>
                                                <li class='form-inline'>
                                                      <?= Html::activeRadio($model, 'a1_op', ['onclick'=>'clicked(this)', 'id'=>'rb_a1op', 'value'=>601, 'uncheck'=>null, 'label'=>\Yii::t('app/crf', 'Well operations'), 
                                                                'class' => "sy_data_toggle",
                                                                'sy_data_toggle_dest' => "txt_a1_op_specify", //to set
                                                                'sy_data_toggle_src_type' => "radio",
                                                                'sy_data_toggle_visible_value' => 'true',
                                                                'sy_data_toggle_enabled' => "true"
                                                          
                                                          ]) ?>
                                                      <?= $form->field($model, 'a1_op_specify')->textInput(['id'=>'field-a1-a1_op_specify', 'maxlength' => 64, 'id'=>'txt_a1_op_specify', 'class'=>'form-control hidden'])->label('') ?>
                                                </li>
                                                <li>
                                                      <?= Html::activeRadio($model, 'a1_op', ['onclick'=>'clicked(this)', 'id'=>'rb_a1op', 'value'=>602, 'uncheck'=>null, 'label'=>\Yii::t('app/crf', 'Production')]) ?>
                                                </li>
                                                <li>
                                                      <?= Html::activeRadio($model, 'a1_op', ['onclick'=>'clicked(this)', 'id'=>'rb_a1op', 'value'=>603, 'uncheck'=>null, 'label'=>\Yii::t('app/crf', 'Maintenance')]) ?>
                                                </li>
                                                <li>
                                                      <?= Html::activeRadio($model, 'a1_op', ['onclick'=>'clicked(this)', 'id'=>'rb_a1op', 'value'=>604, 'uncheck'=>null, 'label'=>\Yii::t('app/crf', 'Construction')]) ?>
                                                </li>
                                                <li>
                                                      <?= Html::activeRadio($model, 'a1_op', ['onclick'=>'clicked(this)', 'id'=>'rb_a1op', 'value'=>605, 'uncheck'=>null, 'label'=>\Yii::t('app/crf', 'Pipeline operations including pigging')]) ?>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            
                            
                            </div>
                        </div>
                    </div>
                </div>
            </div>  <!--end section A.1.XI -->
            
            
            <div class="row">
                <div class="col-lg-12">
                    <div class="content">
                        <div class="row form-inline">
                            <div class="col-lg-12 ">
                                <ul class="list-inline">
                                    <li>
                                        <div class="form-group">
                                            <div>
                                                <?php
                                                $roman = 'XII. ';
                                                $current = 'A.1.12';
                                                $lbl = $roman . CrfHelper::SectionsTitle()[$current];
                                                ?>

                                                <label><?= $lbl ?></label>
                                            </div>
                                            <div class="help-block"></div>
                                        </div>
                                    </li>
                                    
                                    <li>
                                        <?= $form->field($model, 'axii')
                                            ->radioList(
                                                ['1' => \Yii::t('app', 'Yes'), '0' =>\Yii::t('app', 'No')],
                                                [
                                                  'unselect' => null,
                                                  'item' => function($index, $label, $name, $checked, $value) {
                                                        $showPanel = $index == 0 ? "true" : "false";
                                                        $ischecked = $checked ? 'checked="checked"' : ''; // $value == '1' ? "checked " : '';
                                                        $setting = 'class="sy_data_toggle" '
                                                                . 'sy_data_toggle_dest = "ignition_div"'
                                                                . 'sy_data_toggle_src_type = "radio"'
                                                                . 'sy_data_toggle_visible_value = "' . $showPanel . '"' 
                                                                . 'sy_data_toggle_enabled = "true"';

                                                        $return = '<label>'
                                                                . ' <input type="radio" name="' . $name . '" value="'. $value .'" ' 
                                                                . $ischecked 
                                                                . ' '
                                                                . $setting
                                                                . '>'
                                                                . ' ' . $label
                                                                . '</input>'
                                                                . '</label>';
                                                        return $return;
                                                  }
                                                ]    
                                            )->label(false) ?>                                        
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="row form-inline hiddenBlock" id="ignition_div">
                            <div class="col-lg-12 col-lg-offset-1">
                                <ul class='list-unstyled'>
                                    <li>
                                        <ul class="list-inline">
                                            <li>
                                                <div class="form-group">
                                                    <div>
                                                        <label style="font-weight: normal; padding-left: 5px">
                                                            <?= Yii::t('app/crf', 'Ignition was:') ?>
                                                        </label>
                                                    </div>
                                                    <div class="help-block"></div>
                                                </div>
                                            </li>
                                            <li>
                                                <?= $form->field($model, 'axii_code')
                                                    ->radioList(
                                                        ['1' => \Yii::t('app/crf', 'Immediate'), '0' =>\Yii::t('app/crf', 'Delayed')],
                                                        [
                                                          'unselect' => null,
                                                          'item' => function($index, $label, $name, $checked, $value) {
                                                                $showPanel = $index == 0 ? "false" : "true";
                                                                $ischecked = $checked ? 'checked="checked"' : ''; // $value == '1' ? "checked " : '';
                                                                $setting = 'class="sy_data_toggle" '
                                                                        . 'sy_data_toggle_dest = "rb_axii_code_delayed_toggle"'
                                                                        . 'sy_data_toggle_src_type = "radio"'
                                                                        . 'sy_data_toggle_visible_value = "' . $showPanel . '"' 
                                                                        . 'sy_data_toggle_enabled = "true"';

                                                                $return = '<label style="padding-right: 24px;">'
                                                                        . ' <input type="radio" name="' . $name . '" value="'. $value .'" ' 
                                                                        . $ischecked 
                                                                        . ' '
                                                                        . $setting
                                                                        . '>'
                                                                        . ' ' . $label
                                                                        . '</input>'
                                                                        . '</label>';
                                                                return $return;
                                                          }
                                                        ]    
                                                    )->label(false) ?>                                        
                                            </li>
                                            <li>
                                                <span id="rb_axii_code_delayed_toggle" class="hiddenBlock">
                                                    <?= $form->field($model, 'axii_code_time')->textInput()->label(Yii::t('app/crf', 'Delay time')) ?>
                                                    <em><?= Yii::t('app/crf', '(sec)') ?></em>
                                                </span>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <div class="sy_pad_bottom_18">
                                            <?= Yii::t('app/crf', 'Was there') ?>:
                                            <em><?= Yii::t('app/crf', '(add sequence of events by numbering appropriate boxes in order of occurrence)') ?></em>
                                            <div class="text-muted">
                                                <em>
                                                    <?= Yii::t('app', 'Please use comma delimited numbers for separating the events of the same type (e.g. two explosions)') ?>
                                                </em>
                                            </div>
                                        </div>
                                        <ul class="list-unstyled">
                                            <li>
                                                <?php //$form->field($model, 'axii_flash_index')->textInput() ?>
                                                
                                                <?= $form->field($model, 'axii_flash_index', [
                                                ])->widget(\yii\widgets\MaskedInput::className(), [
                                                    'mask'=>'9[9](,9[9]){0,20}',
                                                    'definitions'=>[
                                                        ','=>[
                                                            'validator'=>'\,',
                                                            'cardinality'=>1,
                                                        ],
                                                        '_'=>[
                                                            'validator'=>'',
                                                            'cardinality'=>2,
                                                        ]
                                                    ],
                                                    'clientOptions' => [
                                                        'greedy'=>FALSE,
                                                        //'removeMaskOnSubmit' => false,
                                                    ]
                                                ])->label(Yii::t('app/crf', 'A flash fire')) ?>
                                                
                                            </li>
                                            <li>
                                                <?= $form->field($model, 'axii_jet_index', [
                                                ])->widget(\yii\widgets\MaskedInput::className(), [
                                                    'mask'=>'9[9](,9[9]){0,20}',
                                                    'definitions'=>[
                                                        ','=>[
                                                            'validator'=>'\,',
                                                            'cardinality'=>1,
                                                        ],
                                                        '_'=>[
                                                            'validator'=>'',
                                                            'cardinality'=>2,
                                                        ]
                                                    ],
                                                    'clientOptions' => [
                                                        'greedy'=>FALSE,
                                                    ]
                                                ])->label(Yii::t('app/crf', 'A jet fire')) ?>
                                                
                                            </li>
                                            <li>
                                                <?= $form->field($model, 'axii_pool_index', [
                                                    //'enableClientValidation'=>false
                                                ])->widget(\yii\widgets\MaskedInput::className(), [
                                                    'mask'=>'9[9](,9[9]){0,20}',
                                                    'definitions'=>[
                                                        ','=>[
                                                            'validator'=>'\,',
                                                            'cardinality'=>1,
                                                        ],
                                                        '_'=>[
                                                            'validator'=>'',
                                                            'cardinality'=>2,
                                                        ]
                                                    ],
                                                    'clientOptions' => [
                                                        'greedy'=>FALSE,
                                                        //'removeMaskOnSubmit' => false,
                                                    ]
                                                ])->label(Yii::t('app/crf', 'A pool fire')) ?>
                                            </li>
                                            <li>
                                                <?= $form->field($model, 'axii_exp_index', [
                                                    //'enableClientValidation'=>false
                                                ])->widget(\yii\widgets\MaskedInput::className(), [
                                                    'mask'=>'9[9](,9[9]){0,20}',
                                                    'definitions'=>[
                                                        ','=>[
                                                            'validator'=>'\,',
                                                            'cardinality'=>1,
                                                        ],
                                                        '_'=>[
                                                            'validator'=>'',
                                                            'cardinality'=>2,
                                                        ]
                                                    ],
                                                    'clientOptions' => [
                                                        'greedy'=>FALSE,
                                                        //'removeMaskOnSubmit' => false,
                                                    ]
                                                ])->label(Yii::t('app/crf', 'An expolsion')) ?>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>  <!--end section A.1.XII -->

            
            <!--
            SECTION A.1.XIII
            -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="content">
                        <div class="row">
                            <div class="col-lg-12">
                                <?php
                                $roman = 'XIII. ';
                                $current = 'A.1.13';
                                $lbl = $roman . CrfHelper::SectionsTitle()[$current];
                                ?>

                                <strong><?= $lbl ?></strong><?= ' (' . Yii::t('app/crf', '<u>if</u> known') . ')' ?>
                                
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <?=  $form->field($model, 'axiii')->textArea(['maxlength' => 512, 'rows'=>7])->label('')  ?>
                                <?php // $form->field($model, 'axiii')->textInput(['maxlength' => 512])->label('') ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  <!--end section A.1.XIII -->
            
            <!--
            SECTION A.1.XIV
            -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="content">
                        <div class="row">
                            <div class="col-lg-12">
                                <div style='padding-bottom: 12px;'>
                                <?php
                                $roman = 'XIV. ';
                                $current = 'A.1.14';
                                $lbl = $roman . CrfHelper::SectionsTitle()[$current];
                                ?>
                                <strong><?= $lbl ?></strong> 
                                <em class='text-muted'><?= Yii::t('app', 'Tick appropriate box') ?></em>
                                </div>
                            </div>
                            
                            <div class='col-lg-12'>
                                <table>
                                    <tr>
                                        <td style='padding-right: 15px;'>
                                        <strong><?= Yii::t('app/crf','Shutdown') ?></strong>
                                        </td>
                                        <td style='padding-right: 12px;'>
                                        <?= Html::activeRadio($model, 'axiv_sd', ['id'=>'chk_axiv_sd', 'value'=>2, 'uncheck'=>null, 'label'=>Yii::t('app/crf','Automatic')]) ?>
                                        </td>
                                        <td style='padding-right: 12px;'>
                                        <?= Html::activeRadio($model, 'axiv_sd', ['id'=>'chk_axiv_sd', 'value'=>1, 'uncheck'=>null, 'label'=>Yii::t('app/crf','Manual')]) ?>
                                        </td>
                                        <td style='padding-right: 12px;'>
                                        <?= Html::activeRadio($model, 'axiv_sd', ['id'=>'chk_axiv_sd', 'value'=>0, 'uncheck'=>null, 'label'=>Yii::t('app/crf','Not taken')]) ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style='padding-right: 15px;'>
                                        <strong><?= Yii::t('app/crf','Blowdown') ?></strong>
                                        </td>
                                        <td style='padding-right: 12px;'>
                                        <?= Html::activeRadio($model, 'axiv_bd', ['id'=>'chk_axiv_bd', 'value'=>2, 'uncheck'=>null, 'label'=>Yii::t('app/crf','Automatic')]) ?>
                                        </td>
                                        <td style='padding-right: 12px;'>
                                        <?= Html::activeRadio($model, 'axiv_bd', ['id'=>'chk_axiv_bd', 'value'=>1, 'uncheck'=>null, 'label'=>Yii::t('app/crf','Manual')]) ?>
                                        </td>
                                        <td style='padding-right: 12px;'>
                                        <?= Html::activeRadio($model, 'axiv_bd', ['id'=>'chk_axiv_bd', 'value'=>0, 'uncheck'=>null, 'label'=>Yii::t('app/crf','Not taken')]) ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style='padding-right: 15px;'>
                                        <strong><?= Yii::t('app/crf','Deluge') ?></strong>
                                        </td>
                                        <td style='padding-right: 12px;'>
                                        <?= Html::activeRadio($model, 'axiv_d', ['id'=>'chk_axiv_d', 'value'=>2, 'uncheck'=>null, 'label'=>Yii::t('app/crf','Automatic')]) ?>
                                        </td>
                                        <td style='padding-right: 12px;'>
                                        <?= Html::activeRadio($model, 'axiv_d', ['id'=>'chk_axiv_d', 'value'=>1, 'uncheck'=>null, 'label'=>Yii::t('app/crf','Manual')]) ?>
                                        </td>
                                        <td style='padding-right: 12px;'>
                                        <?= Html::activeRadio($model, 'axiv_d', ['id'=>'chk_axiv_d', 'value'=>0, 'uncheck'=>null, 'label'=>Yii::t('app/crf','Not taken')]) ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style='padding-right: 15px;'>
                                        <strong>CO<sub>2</sub><?= '/' . Yii::t('app/crf','Halon/inerts') ?></strong>
                                        </td>
                                        <td style='padding-right: 12px;'>
                                        <?= Html::activeRadio($model, 'axiv_co2', ['id'=>'chk_axiv_co2', 'value'=>2, 'uncheck'=>null, 'label'=>Yii::t('app/crf','Automatic')]) ?>
                                        </td>
                                        <td style='padding-right: 12px;'>
                                        <?= Html::activeRadio($model, 'axiv_co2', ['id'=>'chk_axiv_co2', 'value'=>1, 'uncheck'=>null, 'label'=>Yii::t('app/crf','Manual')]) ?>
                                        </td>
                                        <td style='padding-right: 12px;'>
                                        <?= Html::activeRadio($model, 'axiv_co2', ['id'=>'chk_axiv_co2', 'value'=>0, 'uncheck'=>null, 'label'=>Yii::t('app/crf','Not taken')]) ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style='padding-right: 15px;'>
                                        <strong><?= Yii::t('app/crf','Call to muster') ?></strong>
                                        </td>
                                        <td style='padding-right: 12px;'>
                                        <?= Html::activeRadio($model, 'axiv_ctm', ['id'=>'chk_axiv_ctm', 'value'=>2, 'uncheck'=>null, 'label'=>Yii::t('app/crf','At stations')]) ?>
                                        </td>
                                        <td style='padding-right: 12px;'>
                                        <?= Html::activeRadio($model, 'axiv_ctm', ['id'=>'chk_axiv_ctm', 'value'=>1, 'uncheck'=>null, 'label'=>Yii::t('app/crf','At lifeboats')]) ?>
                                        </td>
                                        <td style='padding-right: 12px;'>
                                        <?= Html::activeRadio($model, 'axiv_ctm', ['id'=>'chk_axiv_ctm', 'value'=>0, 'uncheck'=>null, 'label'=>Yii::t('app/crf','Not taken')]) ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style='padding-right: 15px; '>
                                            <label>
                                                <?= $model->attributeLabels()['axiv_other'] ?>
                                            </label>
                                        </td>
                                        <td style='padding-right: 12px; '>
                                            <?php 
                                            if (isset($model->axiv_other) && $model->axiv_other != 0) {
                                                $checked = 'checked = "true"';
                                            } else {
                                                $checked = '';
                                            }
                                            ?>
                                            <div class="form-group field-a1-axiv_other">
                                                <input type="hidden" value="0" name="A1[axiv_other]">
                                                <label>
                                                    <?= '' ?>
                                                </label>
                                                <input id='a1-axiv_other'
                                                       value='1'
                                                       type="checkbox" 
                                                       <?= ' '.$checked.' '?>
                                                       sy_data_toggle_enabled="true" 
                                                       sy_data_toggle_visible_value="true" 
                                                       sy_data_toggle_src_type="checkbox" 
                                                       sy_data_toggle_dest="axiv_container" 
                                                       template="{label}{input}" 
                                                       name="A1[axiv_other]" 
                                                       class="sy_data_toggle" id="a1-axiv_other">                                                        
                                                <?= '<em> (' . \Yii::t('app', 'specify') . ')</em>' ?>
                                                <div class="help-block"></div>
                                            </div>
                                        </td>
                                        <td colspan="2">
                                            <span id="axiv_container" class="hiddenBlock">
                                                <?= $form->field($model, 'axiv_specify')->textInput(['class'=>'form-control', 'label'=>''])->label('null', ['class'=>'hiddenBlock']) ?>
                                            </span>
                                        </td>
                                    </tr>
                                    
                                </table>
                            </div>
                            
                        </div>
                        
                    </div>
                </div>
            </div>  <!--end section A.1.XIV -->
            
            <!--
            SECTION A.1.XV
            -->
            <div class="row">
                <div class="col-lg-12 sy_pad_bottom_18 sy_pad_top_6">
                    <div class="content">
                        <div class="row">
                            <div class="col-lg-12">
                                <strong>XV. <?= \app\models\crf\CrfHelper::SectionsTitle()['A.1.15'] ?></strong> 
                            </div>
                            <div class='col-lg-12 sy_pad_top_6'>
                                <?= Html::activeTextarea($model, 'a1xv', [
                                    'class' => 'form-control',
                                    'rows' => 6,
                                    'cols' => 50
                                ]) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  <!--end section A.1.XV -->

            
            
            
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
