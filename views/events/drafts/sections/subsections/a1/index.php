<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\crf\Section1Search */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Section1s');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="section1-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create {evt}', [
    'modelClass' => 'Section1',
]), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'a1',
            'a1i_np',
            'a1i_np_specify',
            'a1i_2p_h2s_q',
            // 'a1i_2p_h2s_u',
            // 'a1ii_q',
            // 'a1ii_u',
            // 'a1iii_q',
            // 'a1iii_u',
            // 'a1iv_q',
            // 'a1iv_u',
            // 'a1v',
            // 'a1vi_code',
            // 'a1vii_code',
            // 'a1viia',
            // 'a1viib',
            // 'a1viic',
            // 'a1viii_uw_q',
            // 'a1viii_uw_u',
            // 'a1viii_dw_q',
            // 'a1viii_dw_u',
            // 'a1viii_c:ntext',
            // 'a1ix_a_q',
            // 'a1ix_a_u',
            // 'a1ix_b_q',
            // 'a1ix_b_u',
            // 'a1x_a',
            // 'a1x_b',
            // 'a1x_c',
            // 'a1x_d',
            // 'a1x_d_specify',
            // 'axi_description',
            // 'axii',
            // 'axii_code',
            // 'axii_code_time',
            // 'axii_flash_index',
            // 'axii_jet_index',
            // 'axii_exp_index',
            // 'axii_pool_index',
            // 'axiii',
            // 'axiv_sd',
            // 'axiv_bd',
            // 'axiv_d',
            // 'axiv_co2',
            // 'axiv_ctm',
            // 'a1i_o',
            // 'a1i_co',
            // 'a1i_g',
            // 'a1i_2p',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
