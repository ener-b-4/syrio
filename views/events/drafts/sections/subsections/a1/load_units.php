<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\ArrayHelper;
use app\components\helpers\TArrayHelper;

$massUnits=ArrayHelper::map(\app\models\units\Units::find()->where(['scope'=>1])->asArray()->all(), 'id', 'unit');
$massRateUnits=ArrayHelper::map(\app\models\units\Units::find()->where(['scope'=>3])->asArray()->all(), 'id', 'unit');
$timeUnits=ArrayHelper::map(\app\models\units\Units::find()->where(['scope'=>2])->asArray()->all(), 'id', 'unit');
$speedUnits=ArrayHelper::map(\app\models\units\Units::find()->where(['scope'=>4])->asArray()->all(), 'id', 'unit');
$pressureUnits=ArrayHelper::map(\app\models\units\Units::find()->where(['scope'=>5])->asArray()->all(), 'id', 'unit');

$operations = ArrayHelper::map(\app\models\constants\Constants::find()->where(['type'=>7])->asArray()->all(), 'num_code', 'name');

$failureEquipment = TArrayHelper::tMap(\app\models\crf\LeakCause::find()
        ->where(['category_id'=>2])
        ->orWhere(['category_id'=>0])->asArray()->all(),
        'num_code', 'cause',
        'app/crf',
        null,
        [
            'none'=>\Yii::t('app', '(none)'),
            'other'=>'('.\Yii::t('app/cpf', 'other').')',
            'not applicable'=>'('.\Yii::t('app/crf', 'Not applicable').')',
        ]);
$failureDesign = TArrayHelper::tMap(\app\models\crf\LeakCause::find()
        ->where(['category_id'=>1])
        ->orWhere(['category_id'=>0])->asArray()->all(), 
        'num_code', 'cause',
        'app/crf',
        null,
        [
            'none'=>\Yii::t('app', '(none)'),
            'other'=>'('.\Yii::t('app/cpf', 'other').')',
            'not applicable'=>'('.\Yii::t('app/crf', 'Not applicable').')',
        ]);
$failureOperation = TArrayHelper::tMap(\app\models\crf\LeakCause::find()
        ->where(['category_id'=>3])
        ->orWhere(['category_id'=>0])->asArray()->all(), 
        'num_code', 'cause',
        'app/crf',
        null,
        [
            'none'=>\Yii::t('app', '(none)'),
            'other'=>'('.\Yii::t('app/cpf', 'other').')',
            'not applicable'=>'('.\Yii::t('app/crf', 'Not applicable').')',
        ]);
$failureProcedural = TArrayHelper::tMap(\app\models\crf\LeakCause::find()
        ->where(['category_id'=>4])
        ->orWhere(['category_id'=>0])->asArray()->all(), 
        'num_code', 
        'cause',
        'app/crf',
        null,
        [
            'none'=>\Yii::t('app', '(none)'),
            'other'=>'('.\Yii::t('app/cpf', 'other').')',
            'not applicable'=>'('.\Yii::t('app/crf', 'Not applicable').')',
        ]);
