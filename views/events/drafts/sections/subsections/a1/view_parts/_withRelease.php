<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

include_once 'blocks/a_1_xi.php';
include_once 'blocks/a_1_xii.php';
include_once 'blocks/a_1_xiii.php';
include_once 'blocks/a_1_xiv.php';

use app\models\units\Units;
use yii\helpers\Html;

/* @var $model app\models\crf\Section1 */

    //include_once '..\load_units.php';

    $units = new Units();

    $flag = 0; //needed for temporary choices

    /*
     * the main part of the $url for subsection view
     * the &s=[subsection_number] is added in place
     */
    $url = Yii::$app->getUrlManager()->createUrl([
        //'/crf/section1/update', 
        '/events/drafts/sections/subsections/a1/update', 
        'id' => $model->id,
    ]);
//http://localhost:8080/SyRIO_DEV_post_brussels/web/index.php/events/drafts/sections/subsections/a1/update?id=13&s=0


    $aI1_text_array = [];
    if ($model->a1i_o) $aI1_text_array[]='Oil';
    if ($model->a1i_co) $aI1_text_array[]='Condensate';
    if ($model->a1i_g)
    {
        $aI1_text_array[]='Gas';
        $flag=1;
    }
    if ($model->a1i_2p)
    {
        $aI1_text_array[]='2-Phase';
        $flag=1;
    }

if (count($aI1_text_array)==0)
{
	$aI1_text_array[] = \Yii::t('app', '(not set)');
}

    $aI1_text=implode(', ', $aI1_text_array);
    
    if ($flag==1)
    {
        //add the level of H2S
        $H2S_level_DOM = '<br/><span class="text-primary" style="padding-left: 12px;">level of H<sub>2</sub>S: </span>' . $model->a1i_2p_h2s_q . ' <em>(ppm, estimated, total)</em>';
    }
        
    //hazardous area classification
    if ($model->a1vi_code==1)
    {
        $a1vi_block = '1';
    }
    else if ($model->a1vi_code==2)
    {
        $a1vi_block = '2';
    }
    else if ($model->a1vi_code==3)
    {
        $a1vi_block = 'Unclassified';
    }
    else
    {
        $a1vi_block = '<em class="text-warning">(not set)</em>';
    }
    
    
    //module ventilation block
    $a1vii_block_arr = [];
    if (isset($model->a1vii_code))
    {
        if ($model->a1vii_code == 300)
        {
            //natural
            $a1vii_block_arr[] = 'Natural';

            //continue with details
            //  sides enclosed
            $a1vii_block_arr[] = '<br/>';
            $a1vii_block_arr[] = '<span class=text-primary style="padding-left: 12px;">Number of enclosed sides: </span>';
            if (isset($model->a1viia))
            {
                $a1vii_block_arr[] = $model->a1viia;
                $a1vii_block_arr[] = ' <em class="text-muted">(number of walls, including floor and ceiling)</em>';
            }
            else
            {
                $a1vii_block_arr[] = ' <em class="text-warning">(not set)</em>';
            }

            //  module volume
            $a1vii_block_arr[] = '<br/>';
            $a1vii_block_arr[] = '<span class=text-primary style="padding-left: 12px;">Module volume: </span>';
            if (isset($model->a1viib) && $model->a1viib > 0)
            {
                $a1vii_block_arr[] = $model->a1viib;
                $a1vii_block_arr[] = ' <em>(m<sup>3</sup>)</em>';
            }
            else
            {
                $a1vii_block_arr[] = ' <em class="text-warning">(not set)</em>';
            }

            //  air changes
            $a1vii_block_arr[] = '<br/>';
            $a1vii_block_arr[] = '<span class=text-primary style="padding-left: 12px;">Number of air changes: </span>';
            if (isset($model->a1viic) && $model->a1viic > 0)
            {
                $a1vii_block_arr[] = $model->a1viic;
                $a1vii_block_arr[] = ' <em>(m<sup>3</sup>, estimated)</em>';
            }
            else
            {
                $a1vii_block_arr[] = ' <em class="text-warning">(not set, assumed unknown)</em>';
            }

            $a1vii_block = implode(' ', $a1vii_block_arr);
        }
        else if ($model->a1vii_code == 301)
        {
            //forced
            $a1vii_block_arr[] = 'Forced';


            $a1vii_block = implode('', $a1vii_block_arr);
        }
        else if ($model->a1vii_code == 2)
        {
            //not applicable
            $a1vii_block_arr[] = 'not applicable';
            $a1vii_block = implode('', $a1vii_block_arr);
        }
        else
        {
            //not set
            $a1vii_block_arr[] = '<em class="text-warning">(not set)</em>';        
            $a1vii_block = implode('', $a1vii_block_arr);
        }
        
    }
    else
    {
        //not set
        $a1vii_block_arr[] = '<em class="text-warning">(not set)</em>';                
        $a1vii_block = implode('', $a1vii_block_arr);
    }
    //end of $a1vii_block
    
    //$a1vii_block
    //reset array
    //unset($arr);
    $arr=[];
    
    //  wind speed
    if (isset($model->a1viii_uw_q))
    {
        $unit = Units::findOne($model->a1viii_uw_u)->unit;
        $arr[]=$model->a1viii_uw_q . ' <em>(' . $unit . ')</em>';
    }
    else
    {
        //not set
        $arr[] = '<em class="text-warning">(not set)</em>';        
    }
    
    $arr[] = '<br/>';
    $arr[]='<span class=text-primary style="padding-left: 12px;">Wind direction: </span>';
    
    //  wind direction
    if (isset($model->a1viii_dw_q))
    {
        $arr[]=$model->a1viii_dw_q;
        $arr[]=' <em>(heading, decimal degrees, North by East)</em>';
    }
    else
    {
        //not set
        $arr[] = '<em class="text-warning">(not set)</em>';        
    }

    //additional weather
    
    if (isset($model->a1viii_c) && $model->a1viii_c!='')
    {
        $arr[]='<br/><span class=text-primary style="padding-left: 12px;">Additional weather conditions: </span>';
        $arr[]=$model->a1viii_c;
    }
    else
    {
        $arr[]='<br/><span class=text-primary style="padding-left: 12px;">No other weather conditions information </span>';
    }
    
    $a1viii_block = implode('', $arr);
    
    //reset arr
    unset($arr);
    $arr=[];

    // END additional weather
    
    //system pressure
    $arr[]='    <strong class="strong text-primary">IX. System pressure: </strong>';
    $arr[]='    <br/>';

    $arr[]='    <span class=text-primary style="padding-left: 12px;">Design: </span>';
    if (isset($model->a1ix_a_q))
    {
        $unit = Units::findOne($model->a1ix_a_u)->unit;
        $arr[]=$model->a1ix_a_q . ' <em>(' . $unit . ')</em>';
    }
    else
    {
        //not set
        $arr[] = '<em class="text-warning">(not set)</em>';        
    }

    $arr[]='    <br/><span class=text-primary style="padding-left: 12px;">Actual: </span>';
    if (isset($model->a1ix_b_q))
    {
        $unit = Units::findOne($model->a1ix_b_u)->unit;
        $arr[]=$model->a1ix_b_q . ' <em>(' . $unit . ')</em>';
    }
    else
    {
        //not set
        $arr[] = '<em class="text-warning">(not set)</em>';        
    }
    
    $a1ix_block = implode('', $arr);
    
    //reset arr
    unset($arr);
    $arr=[];
    
    // END system pressure (1IX)
    
    //X. Means of detection
    //system pressure
    $arr[]='    <strong class="text-primary">X. Means of detection: </strong>';
    $temp=[];
    if ($model->a1x_a == 1) $temp[] = 'fire';
    if ($model->a1x_b == 1) $temp[] = 'gas';
    if ($model->a1x_c == 1) $temp[] = 'smoke';
    if ($model->a1x_d == 1 && isset($model->a1x_d_specify) && $model->a1x_d_specify != '') $temp[] = $model->a1x_d_specify;
    
    if (count($temp) != 0)
    {
        $arr[] = implode(', ', $temp);
    }
    else
    {
        //not set
        $arr[] = '<em class="text-warning">(not set)</em>';        
    }

    $a1x_block = implode('', $arr);
    
    //reset arr
    unset($arr);
    $arr=[];
    // END means of detection (1X)
    
    ?>


   <div class="row">
        <div class="col-lg-10">
            <h4>A.1. Was there another release of hydrocarbon substances?</h4>
        </div>
        <div>
            <?= Html::activeRadio($model, 'a1', ['disabled', 'onclick'=>'clicked(this)', 'id'=>'radio1', 'value'=>1, 'uncheck'=>null, 'label'=>'Yes']) ?>
            <?= Html::activeRadio($model, 'a1', ['disabled'=>'clicked(this)', 'id'=>'radio2', 'value'=>0, 'uncheck'=>null, 'label'=>'No']) ?>
        </div>
    </div>    

    <div class="alert alert-info" role="alert">
        <h3>
            <strong>Hydrocarbon release</strong> has been reported.
        </h3>

    </div>

    <p class="text-muted">
        The following has been reported so far:
    </p>
    <div class="row">                            
        <div class="col-lg-12">
            <p>
                <strong class="strong text-primary">I. Hydrocarbon (HC) released: </strong>
                <?= $aI1_text ?>
                <?= isset($H2S_level_DOM) ? $H2S_level_DOM : '' ?>
                <a href=<?= $url . '&s=1'?> aria-label="Left Align">
                   <span class="glyphicon glyphicon-pencil"></span>
                </a>
            </p>
            <p>
                <strong class="strong text-primary">II. Estimated quantity released: </strong>
                <?= $model->a1ii_q ?>
                <?php
                    $unit = \app\models\shared\NullValueFormatter::FormatObject(Units::findOne($model->a1ii_u), 'unit');
                ?>
                <em>(<?= $unit ?>)</em>
                <a href=<?= $url . '&s=2'?> aria-label="Left Align">
                   <span class="glyphicon glyphicon-pencil"></span>
                </a>
            </p>
            <p>
                <strong class="strong text-primary">III. Estimated release rate: </strong>
                <?= $model->a1iii_q ?>
                <?php
                    //$unit = Units::findOne($model->a1iii_u)->unit;
                    $unit = \app\models\shared\NullValueFormatter::FormatObject(Units::findOne($model->a1iii_u), 'unit', \Yii::t('app', 'not set'));
                ?>
                <em>(<?= $unit ?>)</em>
                <a href=<?= $url . '&s=3' ?> aria-label="Left Align">
                   <span class="glyphicon glyphicon-pencil"></span>

                </a>
            </p>
            <p>
                <strong class="strong text-primary">IV. Duration of leak: </strong>
                <?= $model->a1iv_q ?>
                <?php
                    //$unit = Units::findOne($model->a1iv_u)->unit;
                    $unit = \app\models\shared\NullValueFormatter::FormatObject(Units::findOne($model->a1iv_u), 'unit');
                ?>
                <em>(<?= $unit ?>)</em>
                <a href=<?= $url . '&s=4'?> aria-label="Left Align">
                   <span class="glyphicon glyphicon-pencil"></span>
                </a>
            </p>
            <p>
                <strong class="strong text-primary">V. Location of leak: </strong>
                <?= $model->a1v == '' || !isset($model->a1v) ? '<em class="text-warning">(not set)</em>' : $model->a1v ?>
                <a href=<?= $url . '&s=5'?> aria-label="Left Align">
                   <span class="glyphicon glyphicon-pencil"></span>
                </a>
            </p>
            <p>
                <strong class="strong text-primary">VI. Hazardous area classification: </strong>
                <?= $a1vi_block ?>
                <a href=<?= $url . '&s=6'?> aria-label="Left Align">
                   <span class="glyphicon glyphicon-pencil"></span>
                </a>
            </p>
            <p>
                <strong class="strong text-primary">VII. Module ventilation: </strong>
                <?= $a1vii_block ?>
                <a href=<?= $url . '&s=7'?> aria-label="Left Align">
                   <span class="glyphicon glyphicon-pencil"></span>
                </a>
            </p>
            <p>
                <strong class="strong text-primary">VIII. Weather conditions: </strong>
                <br/>
                <span class=text-primary style="padding-left: 12px;">Wind speed: </span>                
                <?= $a1viii_block ?>
                <a href=<?= $url . '&s=8'?> aria-label="Left Align">
                   <span class="glyphicon glyphicon-pencil"></span>
                </a>
            </p>
            <p>
                <?= $a1ix_block ?>
                <a href=<?= $url . '&s=9'?> aria-label="Left Align">
                   <span class="glyphicon glyphicon-pencil"></span>
                </a>

            </p>
            <p>
                <?= $a1x_block ?>
                <a href=<?= $url . '&s=10'?> aria-label="Left Align">
                   <span class="glyphicon glyphicon-pencil"></span>
                </a>
            </p>
            <p>
                <?= $a1xi_block ?>
            </p>
            <p>
                <?= $a1xii_block ?>
            </p>
            <p>
                <?= $a1xiii_block ?>
            </p>
            <p>
                <strong class="text-info">What emergency action was taken?</strong>
                <a href=<?= $url . '&s=14'?> aria-label="Left Align">
                   <span class="glyphicon glyphicon-pencil"></span>
                </a>
                <br/>
                <?= $a1xiv_block ?>
            </p>
        </div> 
    </div>
