<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\components\helpers\TArrayHelper;
use yii\widgets\ActiveForm;
use app\models\shared\NullValueFormatter;
use app\models\crf\CrfHelper;

include_once Yii::getAlias('@app') . '/views/events/drafts/sections/subsections/a1/load_units.php';

/* @var $this yii\web\View */
/* @var $model app\models\crf\Section1 */

?>

<div class="section1-form">
    
    <div class="row">
        <div class="col-lg-12">
            <p>
                <strong style="padding-right: 32px;"> <?= \Yii::t('app/crf', 'A.1. Was there a release of hydrocarbon substances?') ?></strong>
                <span style="padding-right: 16px;"><?= \Yii::t('app' , 'Yes') ?> <?= Html::radio('r', $model->a1, ['disabled' => true]) ?></span>
                <span style="padding-right: 32px;"><?= \Yii::t('app' , 'No') ?> <?= Html::radio('r', !$model->a1, ['disabled' => true]) ?></span>
            </p>
        </div>
        <div class="col-lg-12 sy_indent_52">
            <p>
            <?= \Yii::t('app/crf', 'If yes, fill in the following sections') ?>.
            </p>
            <br/>

            <div class="row">
                <div class="col-lg-12">
                    <div class="content">
                        <div class="row">                            
                            <div class="col-lg-12">
                                <h5>
                                    <strong><?= CrfHelper::SectionsTitle()['A.1.1'] ?></strong> <em class="text-muted">(<?= Yii::t('app', 'Tick appropriate box') ?>)</em>
                                </h5>
                            </div> 
                        </div>
                        <div class="row">
                            <div class="col-lg-10 col-lg-offset-1 form-inline">
                                <br/>
                                <ul class="list-inline">
                                    <li>
                                        <?= Yii::t('app/crf', 'NON-PROCESS') ?>

                                    </li>
                                    <li>
                                        <?= Html::checkbox('chk', $model->a1i_np, ['disabled' => true]) ?>
                                    </li>
                                    <li>
                                        <span>
                                            <em style="padding-right: 32px;">(<?= ucfirst(Yii::t('app', 'specify')) ?>)</em><?= $model->a1i_np_specify ?>
                                        </span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="row sy_pad_top_6">
                            <div class="col-lg-10 col-lg-offset-1 form-inline">
                                <ul class="list-inline">
                                    <li>
                                        <?= Yii::t('app/crf', 'PROCESS') ?>
                                    </li>
                                    <li>
                                        <?= Html::activeCheckbox($model, 'a1i_o', ['disabled'=>true, 'onclick'=>'clicked(this)', 'id'=>'chkO', 'value'=>1, 'uncheck'=>0, 'label'=>Yii::t('app/crf', 'Oil')]) ?>
                                    </li>
                                    <li>
                                        <?= Html::activeCheckbox($model, 'a1i_co', ['disabled'=>true,'onclick'=>'clicked(this)', 'id'=>'chkCo', 'value'=>1, 'uncheck'=>0, 'label'=>Yii::t('app/crf', 'Condensate')]) ?>
                                    </li>
                                    <li>
                                        <?= Html::activeCheckbox($model, 'a1i_g', ['disabled'=>true,'onclick'=>'clicked(this)', 'id'=>'chkG', 'value'=>1, 'uncheck'=>0, 'label'=>Yii::t('app/crf', 'Gas')]) ?>
                                    </li>
                                    <li>
                                        <?= Html::activeCheckbox($model, 'a1i_2p', ['disabled'=>true,'onclick'=>'clicked(this)', 'id'=>'chk2p', 'value'=>1, 'uncheck'=>0, 'label'=>Yii::t('app/crf', '2-Phase')]) ?>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="row sy_pad_top_6 sy_pad_bottom_18">
                            <div class="col-lg-10 col-lg-offset-1 form-inline" id=''>
                                <ul class="list-inline">
                                    <li>
                                        <?= \Yii::t('app/crf', 'Level of H')?><sub>2</sub>S
                                    </li>
                                    <li>
                                        <?php // Html::activeTextInput($model, 'a1i_2p_h2s_q', ['disabled'=>true]) ?>
                                        <?= NullValueFormatter::Format($model->a1i_2p_h2s_q) ?>
                                        <?php //$form->field($model, 'a1i_2p_h2s_q')->textInput(['disabled'=>true])->label('') ?>
                                    </li>
                                    <li>
                                        <em><?= Yii::t('app/crf', '(estimated ppm)') ?></em>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            
        </div>
    </div>

    <div class="row" id="a1_content">
        <div class="col-lg-12 sy_indent_52">
            <div class="text-muted">
            </div>

            
            
            <!--
            SECTION A.1.II
            -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="content">
                        <div class="row form-inline">                            
                            <div class="col-lg-12 sy_pad_bottom_18">
                                <?php
                                $roman = 'II. ';
                                $current = 'A.1.2';
                                $attr_core = 'a1ii';
                                $attribute_q = $attr_core.'_q';
                                $attribute_u = $attr_core.'_u';
                                if (isset($model->$attribute_u)) {
                                    $unit = $massUnits[$model->$attribute_u];
                                } else {
                                    $unit = NullValueFormatter::FormatObject($model, $attribute_u);
                                }

                                echo '<strong>'
                                        . $roman . CrfHelper::SectionsTitle()[$current] . ': '
                                        . '</strong>'
                                        . NullValueFormatter::Format($model->$attribute_q)
                                        . ' '
                                        . $unit
                                ?>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>

            
            <!--
            SECTION A.1.III
            -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="content">
                        <div class="row form-inline">                            
                            <div class="col-lg-12 sy_pad_bottom_18">
                                <?php
                                $roman = 'III. ';
                                $current = 'A.1.3';
                                $attr_core = 'a1iii';
                                $attribute_q = $attr_core.'_q';
                                $attribute_u = $attr_core.'_u';
                                
                                if (isset($model->$attribute_u)) {
                                    $unit = $massRateUnits[$model->$attribute_u];
                                } else {
                                    $unit = NullValueFormatter::FormatObject($model, $attribute_u);
                                }

                                echo '<strong>'
                                        . $roman . CrfHelper::SectionsTitle()[$current] . ': '
                                        . '</strong>'
                                        . NullValueFormatter::Format($model->$attribute_q)
                                        . ' '
                                        . $unit
                                ?>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>

            
            <!--
            SECTION A.1.IV
            -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="content">
                        <div class="row form-inline">                            
                            <div class="col-lg-12 sy_pad_bottom_18">
                                <?php
                                $roman = 'IV. ';
                                $current = 'A.1.4';
                                $attr_core = 'a1iv';
                                $attribute_q = $attr_core.'_q';
                                $attribute_u = $attr_core.'_u';
                                
                                if (isset($model->$attribute_u)) {
                                    $unit = $timeUnits[$model->$attribute_u];
                                } else {
                                    $unit = NullValueFormatter::FormatObject($model, $attribute_u);
                                }
                                
                                echo '<strong>'
                                        . $roman . CrfHelper::SectionsTitle()[$current] . ': '
                                        . '</strong>'
                                        . NullValueFormatter::Format($model->$attribute_q)
                                        . ' '
                                        . $unit
                                ?>
                            </div> 
                            <div class="col-lg-12 help-block "  style="margin-top: -12px;">
                                <em>(<?= \Yii::t('app/crf', 'Estimated time from discovery, e.g. alarm, electronic log, to termination of leak') ?>)</em>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!--
            SECTION A.1.V
            -->
            <div class="row">
                
                <div class="col-lg-12">
                    <div class="content">
                        <div class="row form-inline">                            
                            <div class="col-lg-12">
                                <?php
                                $roman = 'V. ';
                                $current = 'A.1.5';
                                $attr_core = 'a1v';
                                $attribute_q = $attr_core;
                                $attribute_u = null;
                                $unit = null;

                                echo '<strong>'
                                        . $roman . CrfHelper::SectionsTitle()[$current] . ': '
                                        . '</strong>'
                                ?>
                            </div> 
                            <div class="col-lg-12 sy_pad_bottom_18">
                                <?php
                                    echo nl2br(NullValueFormatter::Format($model->$attribute_q));
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!--
            SECTION A.1.VI
            -->
            <div class="row">
                <div class="col-lg-12  sy_pad_bottom_18">
                    <div class="content">
                        <div class="row">
                            <div class="col-lg-12">
                                <?php
                                $roman = 'VI. ';
                                $current = 'A.1.6';
                                echo '<strong>'
                                        . $roman . CrfHelper::SectionsTitle()[$current] . ': '
                                        . '</strong>'
                                        . ' <em>(' . \Yii::t('app/crf', 'i.e. zone at location of incident') . ')</em>';
                                ?>
                            </div>
                        </div>
                        <div class="row form-inline">
                            <div class="col-lg-12">
                                <ul class="list-inline">
                                    <li>
                                        <em class="text-muted"><?= Yii::t('app', 'Tick appropriate box') ?></em>
                                    </li>
                                    <li>
                                        <?= Html::activeRadio($model, 'a1vi_code', ['disabled'=>true, 'onclick'=>'clicked(this)', 'id'=>'chk_a1i_vi_code', 'value'=>1, 'uncheck'=>null, 'label'=>'1']) ?>
                                    </li>
                                    <li>
                                        <?= Html::activeRadio($model, 'a1vi_code', ['disabled'=>true, 'onclick'=>'clicked(this)', 'id'=>'chk_a1i_vi_code', 'value'=>2, 'uncheck'=>null, 'label'=>'2']) ?>
                                    </li>
                                    <li>
                                        <?= Html::activeRadio($model, 'a1vi_code', ['disabled'=>true, 'onclick'=>'clicked(this)', 'id'=>'chk_a1i_vi_code', 'value'=>3, 'uncheck'=>null, 'label'=>Yii::t('app/crf', 'Unclassified')]) ?>
                                    </li>
                                </ul>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>
            
            <!--
            SECTION A.1.VII
            -->
            <div class="row">
                <div class="col-lg-12 sy_pad_bottom_18">
                    <div class="content">
                        <div class="row form-inline">
                            <div class="col-lg-12">
                                <ul class="list-inline">
                                    <li>
                                        <?php
                                        $roman = 'VII. ';
                                        $current = 'A.1.7';
                                        echo '<strong>'
                                                . $roman . CrfHelper::SectionsTitle()[$current] . '? '
                                                . '</strong>';                                        
                                        ?>
                                    </li>
                                    <li>
                                        <?= Html::activeRadio($model, 'a1vii_code', ['disabled'=>true, 'onclick'=>'clicked(this)', 'id'=>'chk_a1vii_code_300', 'value'=>300, 'uncheck'=>null, 'label'=>Yii::t('app/crf', 'Natural')]) ?>
                                    </li>
                                    <li>
                                        <?= Html::activeRadio($model, 'a1vii_code', ['disabled'=>true, 'onclick'=>'clicked(this)', 'id'=>'chk_a1vii_code_301', 'value'=>301, 'uncheck'=>null, 'label'=>Yii::t('app/crf', 'Forced')]) ?>
                                    </li>
                                    <li>
                                        <?= Html::activeRadio($model, 'a1vii_code', ['disabled'=>true, 'onclick'=>'clicked(this)', 'id'=>'chk_a1vii_code_2', 'value'=>2, 'uncheck'=>null, 'label'=>Yii::t('app/crf', 'Not applicable')]) ?>
                                    </li>
                                </ul>
                            </div> 
                        </div>
                        <div class="row form-inline" id='a1_vii_x_containers'>
                            <div class="col-lg-11 col-lg-offset-1 ">
                                <?php
                                $label = Yii::t('app/crf', 'How many sides enclosed?');
                                $attr_core = 'a1viia';

                                echo '<span>'
                                        . $label . ' ' . NullValueFormatter::Format($model->$attr_core)
                                        . '</span><br/>'
                                        . '<em class="text-muted">' . Yii::t('app/crf', '(Insert the number of walls, including floor and ceiling)') . '</em>';
                                ?>
                            </div>
                            <div class="col-lg-11 col-lg-offset-1 sy_pad_top_6">
                                <?php
                                $label = Yii::t('app/crf', 'Module volume');
                                $attr_core = 'a1viib';

                                echo '<span>'
                                        . $label . ': ' . NullValueFormatter::Format($model->$attr_core)
                                        . '</span> (m<sup>3</sup>)'
                                ?>
                            </div>
                            <div class="col-lg-11 col-lg-offset-1 sy_pad_top_6">
                                <?php
                                $label = Yii::t('app/crf', 'Estimated number of air changes') . ': <em>(' . Yii::t('app', 'if known') . ')</em>';
                                $attr_core = 'a1viic';

                                echo '<span>'
                                        . $label . ': ' . NullValueFormatter::Format($model->$attr_core)
                                        . '</span>'
                                ?>
                            </div>
                            <div class="col-lg-11 col-lg-offset-1">
                                <?php
                                $label = Yii::t('app/crf', 'Specify hourly rate');
                                $attr_core = 'a1viid';

                                echo '<em>'
                                        . $label . '</em>: ' . NullValueFormatter::Format($model->$attr_core)
                                        . ' (m<sup>3</sup>/h)'
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            

            <!--
            SECTION A.1.VIII
            -->
            <div class="row">
                <div class="col-lg-12 sy_pad_bottom_18">
                    <div class="content">
                        <div class="row">
                            <div class="col-lg-12 sy_pad_bottom_18">
                                <strong>VIII. <?= CrfHelper::SectionsTitle()['A.1.8'] ?></strong>
                            </div>
                        </div>
                        <div class="row form-inline">
                            <div class="col-lg-11 col-lg-offset-1">
                                <?php
                                $roman = null;
                                $current = null;
                                $label = Yii::t('app/crf', 'Wind speed');
                                $attr_core = 'a1viii_uw';
                                $attribute_q = $attr_core.'_q';
                                $attribute_u = $attr_core.'_u';
                                
                                if (isset($model->$attribute_u)) {
                                    $unit = $speedUnits[$model->$attribute_u];
                                } else {
                                    $unit = NullValueFormatter::FormatObject($model, $attribute_u);
                                }
                                
                                echo '<span>'
                                        . $label . ': '
                                        . '</span>'
                                        . NullValueFormatter::Format($model->$attribute_q)
                                        . ' '
                                        . $unit
                                ?>
                            </div>    
                            <div class="col-lg-11 col-lg-offset-1 sy_pad_top_6">
                                <?php
                                $roman = null;
                                $current = null;
                                $label = Yii::t('app/crf', 'Wind direction');
                                $attr_core = 'a1viii_dw';
                                $attribute_q = $attr_core.'_q';
                                $attribute_u = $attr_core.'_u';

                                echo '<span>'
                                        . $label . ': '
                                        . '</span>'
                                        . NullValueFormatter::Format($model->$attribute_q)                                        
                                        . ' <span><em>' . \Yii::t('app','(heading, decimal degrees)') . '</em></span>'
                                ?>
                            </div>    
                            <div class="col-lg-11 col-lg-offset-1 sy_pad_top_6">
                                <p style="margin-top:12px;">
                                    <?= Yii::t('app/crf', 'Provide a description of other relevant weather conditions') ?>
                                </p>
                                <div>
                                    <?= nl2br(NullValueFormatter::Format($model->a1viii_c)) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  <!--end section A.1.VIII -->
            
            <!--
            SECTION A.1.IX
            -->
            <div class="row">
                <div class="col-lg-12 sy_pad_bottom_18">
                    <div class="content">
                        <div class="row">
                            <div class="col-lg-12 sy_pad_bottom_18">
                                <strong>IX. <?= CrfHelper::SectionsTitle()['A.1.9'] ?></strong>
                            </div>
                        </div>
                        <div class="row form-inline">
                            <div class="col-lg-11 col-lg-offset-1">
                                <?php
                                $roman = null;
                                $current = null;
                                $label = Yii::t('app/crf', 'Design pressure');
                                $attr_core = 'a1ix_a';
                                $attribute_q = $attr_core.'_q';
                                $attribute_u = $attr_core.'_u';
                                
                                if (isset($model->$attribute_u)) {
                                    $unit = $pressureUnits[$model->$attribute_u];
                                } else {
                                    $unit = NullValueFormatter::FormatObject($model, $attribute_u);
                                }
                                
                                echo '<span>'
                                        . $label . ': '
                                        . '</span>'
                                        . NullValueFormatter::Format($model->$attribute_q)
                                        . ' '
                                        . $unit
                                ?>
                            </div>    
                            <div class="col-lg-11 col-lg-offset-1 sy_pad_top_6">
                                <?php
                                $roman = null;
                                $current = null;
                                $label = Yii::t('app/crf', 'Actual pressure');
                                $attr_core = 'a1ix_b';
                                $attribute_q = $attr_core.'_q';
                                $attribute_u = $attr_core.'_u';

                                echo '<span>'
                                        . $label . ': '
                                        . '</span>'
                                        . NullValueFormatter::Format($model->$attribute_q)
                                        . ' '
                                        . $unit
                                ?>
                            </div>    
                        </div>
                    </div>
                </div>
            </div>  <!--end section A.1.IX -->
            
            <!--
            SECTION A.1.X
            -->
            <div class="row">
                <div class="col-lg-12 sy_pad_bottom_18">
                    <div class="content">
                        <div class="row">
                            <div class="col-lg-12">
                                
                                <strong>X. <?= ' ' . CrfHelper::SectionsTitle()['A.1.10'] ?></strong>
                            </div>
                        </div>
                        <div class="row form-inline">
                            <div class="col-lg-12 col-lg-offset-1 sy_pad_top_18">
                                <ul class="list-unstyled">
                                    <li>
                                        <?= Html::activeCheckbox($model, 'a1x_a', ['disabled'=>true, 'onclick'=>'clicked(this)', 'id'=>'chk_a1x_a', 'uncheck'=>null, 'label'=>\Yii::t('app/crf', 'Fire')]) ?>
                                    </li>
                                    <li>
                                        <?= Html::activeCheckbox($model, 'a1x_b', ['disabled'=>true, 'onclick'=>'clicked(this)', 'id'=>'chk_a1x_b', 'uncheck'=>null, 'label'=>\Yii::t('app/crf', 'Gas')]) ?>
                                    </li>
                                    <li>
                                        <?= Html::activeCheckbox($model, 'a1x_c', ['disabled'=>true, 'onclick'=>'clicked(this)', 'id'=>'chk_a1x_c', 'uncheck'=>null, 'label'=>\Yii::t('app/crf', 'Smoke')]) ?>
                                    </li>
                                    <li>
                                        <ul class="list-inline" style="margin-left: 0px">
                                            <li>
                                                <?= Html::activeCheckbox($model, 'a1x_d', ['disabled'=>true, 'onclick'=>'clicked(this)', 'id'=>'chk_a1x_d', 'uncheck'=>null, 'label'=>\Yii::t('app/crf', 'Other')]) ?>
                                            </li>
                                            <li>
                                                <span id="chk_a1x_d_container">
                                                    <em><?= \Yii::t('app/crf', '(Please specify)') ?></em>
                                                    <?= NullValueFormatter::Format($model->a1x_d_specify) ?>
                                                </span>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  <!--end section A.1.X -->

            
            <!--
            SECTION A.1.XI
            -->
            <div class="row">
                <div class="col-lg-12 sy_pad_bottom_18">
                    <div class="content">
                        <div class="row">
                            <div class="col-lg-12">
                                <p>
                                    <strong>XI. <?= CrfHelper::SectionsTitle()['A.1.11'] ?></strong>
                                </p>
                                <div>
                                    <?= Yii::t('app/crf', '(Please give a short description and complete the "Cause" checklist below)') ?>
                                </div>
                            </div>
                        </div>
                        <div class="row sy_pad_top_6">
                            <div class="col-lg-12">
                                <?= NullValueFormatter::Format($model->axi_description) ?>
                            </div>
                        </div>
                        <div class='row'>
                            
                            <div class="col-lg-12 sy_pad_top_6">
                                <div class="panel panel-default">
                                    <div class="panel-heading"><strong><?= Yii::t('app/crf','Indicate the cause(s) of the release') ?></strong></div>
                                    <div class="panel-body">
                                        <p>
                                            <em class='text-muted'><span style='text-decoration: underline'><?= Yii::t('app/crf','From each of the following categories')?> </span>
                                            <?= ' ' . Yii::t('app/crf', 'tick the appropriate boxes') ?></em>
                                        </p>
                                        
                                        <?php
                                        $all_causes = \app\models\crf\LeakCause::find()
                                                ->where(['<>', 'category_id', 0])
                                                ->asArray()
                                                ->all();
                                        $all_causes =  TArrayHelper::tIndex($all_causes, 'category_id');

                                        $sel_causes = TArrayHelper::getColumn($model->releaseCauses, 'num_code');
                                                //->asArray()
                                                //->all();
                                        ?>
                                        <?= $this->renderAjax('view_parts/_release_causes_table', [
                                            'model'=>$model,
                                            'all_causes'=>$all_causes, 
                                            'sel_causes'=>$sel_causes,
                                            'other_attr'=>[
                                                "2"=>"a1_fail_equipment",
                                                "3"=>"a1_fail_operation",
                                                "4"=>"a1_fail_procedural"
                                            ]]) ?>
                                    </div>
                                </div>
                            
                            
                                <div class="panel panel-default">
                                    <div class="panel-heading"><strong><?= Yii::t('app/crf', 'Indicate the operational mode in the area at the time of release') ?></strong></div>


                                    <div class="panel-body">
                                        <div>
                                            <?= Yii::t('app/crf', 'Operational mode in the area at the time of release') ?>
                                            
                                            <p>
                                                <em class='text-muted'>
                                                    <?= ' ' . Yii::t('app', 'Choose') ?>
                                                    <span style='text-decoration: underline'><?= ' ' . Yii::t('app','one')?> </span>
                                                    <?= ' ' . Yii::t('app', 'parameter from the following categories, and tick the appropriate boxes') ?>
                                                </em>
                                            </p>
                                            
                                            <ul class='list-unstyled'>
                                                <li>
                                                      <?= Html::activeRadio($model, 'a1_op', ['disabled'=>true, 'id'=>'rb_a1op', 'value'=>600, 'uncheck'=>null, 'label'=> \Yii::t('app/crf', 'Drilling'), 'checked'=>'checked']) ?>
                                                </li>
                                                <li>
                                                      <?= Html::activeRadio($model, 'a1_op', ['disabled'=>true, 'id'=>'rb_a1op', 'value'=>601, 'uncheck'=>null, 'label'=>\Yii::t('app/crf', 'Well operations')]) ?>
                                                      
                                                    <?php
                                                        if (isset($model->a1_op) && $model->a1_op == 601) {
                                                            echo ': ' .$model->a1_op_specify;
                                                        }
                                                    ?>
                                                </li>
                                                <li>
                                                      <?= Html::activeRadio($model, 'a1_op', ['disabled'=>true, 'id'=>'rb_a1op', 'value'=>602, 'uncheck'=>null, 'label'=>\Yii::t('app/crf', 'Production')]) ?>
                                                </li>
                                                <li>
                                                      <?= Html::activeRadio($model, 'a1_op', ['disabled'=>true, 'id'=>'rb_a1op', 'value'=>603, 'uncheck'=>null, 'label'=>\Yii::t('app/crf', 'Maintenance')]) ?>
                                                </li>
                                                <li>
                                                      <?= Html::activeRadio($model, 'a1_op', ['disabled'=>true, 'id'=>'rb_a1op', 'value'=>604, 'uncheck'=>null, 'label'=>\Yii::t('app/crf', 'Construction')]) ?>
                                                </li>
                                                <li>
                                                      <?= Html::activeRadio($model, 'a1_op', ['disabled'=>true, 'id'=>'rb_a1op', 'value'=>605, 'uncheck'=>null, 'label'=>\Yii::t('app/crf', 'Pipeline operations including pigging')]) ?>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            
                            
                            </div>
                        </div>
                    </div>
                </div>
            </div>  <!--end section A.1.XI -->
            
            
            <div class="row">
                <div class="col-lg-12  sy_pad_bottom_18">
                    <div class="content">
                        <div class="row form-inline">
                            <div class="col-lg-12 ">
                                <ul class="list-inline">
                                    <li>
                                        <strong>XII. <?= CrfHelper::SectionsTitle()['A.1.12'] ?></strong>
                                    </li>
                                    <li>
                                        <?= Html::activeRadio($model, 'axii', ['disabled'=>true, 'id'=>'rb_axii_yes', 'value'=>1, 'uncheck'=>null, 'label'=>Yii::t('app', 'Yes')]) ?>
                                    </li>
                                    <li>
                                        <?= Html::activeRadio($model, 'axii', ['disabled'=>true, 'id'=>'rb_axii_no', 'value'=>0, 'uncheck'=>null, 'label'=>Yii::t('app', 'No')]) ?>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="row form-inline" id="ignition_divs">
                            <div class="col-lg-12 col-lg-offset-1">
                                <ul class='list-unstyled'>
                                    <li>
                                        <ul class="list-inline sy_pad_bottom_18">
                                            <li>
                                                <?= Yii::t('app/crf', 'Ignition was:') ?>
                                            </li>
                                            <li>
                                                <?= Html::activeRadioList($model, 'axii_code', [1 => \Yii::t('app/crf', 'Immediate'), 0 => \Yii::t('app/crf', 'Delayed')], [
                                                    'id'=>'rb_axii_code',
                                                    'disabled'=>'disabled'
                                                ]) ?>
                                            </li>
                                            <li>
                                                <span id="rb_axii_code_delayed_toggle" style="padding-left: 5px">
                                                    <?= Yii::t('app/crf', 'Delay time') . ': ' ?>
                                                    <?= NullValueFormatter::Format($model->axii_code_time) . ' ' ?>
                                                    <em><?= Yii::t('app/crf', '(sec)') ?></em>
                                                </span>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <div class='sy_pad_bottom_18'>
                                            <?= Yii::t('app/crf', 'Was there') ?>:
                                            <em><?= Yii::t('app/crf', '(add sequence of events by numbering appropriate boxes in order of occurrence)') ?></em>
                                        </div>
                                        <ul class="list-unstyled">
                                            <li>
                                                <strong><?= Yii::t('app/crf', 'A flash fire') . ': ' ?></strong>
                                                <?= NullValueFormatter::Format($model->axii_flash_index, Yii::t('app', 'No')) ?>
                                            </li>
                                            <li>
                                                <strong><?= Yii::t('app/crf', 'A jet fire') . ': ' ?></strong>
                                                <?= NullValueFormatter::Format($model->axii_jet_index, Yii::t('app', 'No')) ?>
                                            </li>
                                            <li>
                                                <strong><?= Yii::t('app/crf', 'A pool fire') . ': ' ?></strong>
                                                <?= NullValueFormatter::Format($model->axii_pool_index, Yii::t('app', 'No')) ?>
                                            </li>
                                            <li>
                                                <strong><?= Yii::t('app/crf', 'An explosion') . ': ' ?></strong>
                                                <?= NullValueFormatter::Format($model->axii_exp_index, Yii::t('app', 'No')) ?>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>  <!--end section A.1.XII -->

            
            <!--
            SECTION A.1.XIII
            -->
            <div class="row">
                <div class="col-lg-12 sy_pad_bottom_18">
                    <div class="content">
                        <div class="row">
                            <div class="col-lg-12">
                                <strong>XIII. <?= CrfHelper::SectionsTitle()['A.1.13'] ?></strong> <?= Yii::t('app/crf', '<u>if</u> known') ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 sy_pad_top_6">
                                <?= nl2br(NullValueFormatter::Format($model->axiii)) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  <!--end section A.1.XIII -->
            
            <!--
            SECTION A.1.XIV
            -->
            <div class="row">
                <div class="col-lg-12 sy_pad_bottom_18 sy_pad_top_6">
                    <div class="content">
                        <div class="row">
                            <div class="col-lg-12 sy_pad_bottom_18">
                                <strong>XIV. <?= CrfHelper::SectionsTitle()['A.1.14'] ?></strong> 
                                <em class='text-muted'><?= ucfirst(Yii::t('app/crf', 'tick the appropriate boxes')) ?></em>
                            </div>
                            <div class='col-lg-12'>
                                <table>
                                    <tr>
                                        <td style='padding-right: 15px;'>
                                        <strong><?= Yii::t('app/crf','Shutdown') ?></strong>
                                        </td>
                                        <td style='padding-right: 12px;'>
                                        <?= Html::activeRadio($model, 'axiv_sd', ['disabled'=>true, 'id'=>'chk_axiv_sd', 'value'=>2, 'uncheck'=>null, 'label'=>Yii::t('app/crf','Automatic')]) ?>
                                        </td>
                                        <td style='padding-right: 12px;'>
                                        <?= Html::activeRadio($model, 'axiv_sd', ['disabled'=>true, 'id'=>'chk_axiv_sd', 'value'=>1, 'uncheck'=>null, 'label'=>Yii::t('app/crf','Manual')]) ?>
                                        </td>
                                        <td style='padding-right: 12px;'>
                                        <?= Html::activeRadio($model, 'axiv_sd', ['disabled'=>true, 'id'=>'chk_axiv_sd', 'value'=>0, 'uncheck'=>null, 'label'=>Yii::t('app/crf','Not taken')]) ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style='padding-right: 15px;'>
                                        <strong><?= Yii::t('app/crf','Blowdown') ?></strong>
                                        </td>
                                        <td style='padding-right: 12px;'>
                                        <?= Html::activeRadio($model, 'axiv_bd', ['disabled'=>true, 'id'=>'chk_axiv_bd', 'value'=>2, 'uncheck'=>null, 'label'=>Yii::t('app/crf','Automatic')]) ?>
                                        </td>
                                        <td style='padding-right: 12px;'>
                                        <?= Html::activeRadio($model, 'axiv_bd', ['disabled'=>true, 'id'=>'chk_axiv_bd', 'value'=>1, 'uncheck'=>null, 'label'=>Yii::t('app/crf','Manual')]) ?>
                                        </td>
                                        <td style='padding-right: 12px;'>
                                        <?= Html::activeRadio($model, 'axiv_bd', ['disabled'=>true, 'id'=>'chk_axiv_bd', 'value'=>0, 'uncheck'=>null, 'label'=>Yii::t('app/crf','Not taken')]) ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style='padding-right: 15px;'>
                                        <strong><?= Yii::t('app/crf','Deluge') ?></strong>
                                        </td>
                                        <td style='padding-right: 12px;'>
                                        <?= Html::activeRadio($model, 'axiv_d', ['disabled'=>true, 'id'=>'chk_axiv_d', 'value'=>2, 'uncheck'=>null, 'label'=>Yii::t('app/crf','Automatic')]) ?>
                                        </td>
                                        <td style='padding-right: 12px;'>
                                        <?= Html::activeRadio($model, 'axiv_d', ['disabled'=>true, 'id'=>'chk_axiv_d', 'value'=>1, 'uncheck'=>null, 'label'=>Yii::t('app/crf','Manual')]) ?>
                                        </td>
                                        <td style='padding-right: 12px;'>
                                        <?= Html::activeRadio($model, 'axiv_d', ['disabled'=>true, 'id'=>'chk_axiv_d', 'value'=>0, 'uncheck'=>null, 'label'=>Yii::t('app/crf','Not taken')]) ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style='padding-right: 15px;'>
                                        <strong>CO<sub>2</sub><?= '/' . Yii::t('app/crf','Halon/inerts') ?></strong>
                                        </td>
                                        <td style='padding-right: 12px;'>
                                        <?= Html::activeRadio($model, 'axiv_co2', ['disabled'=>true, 'id'=>'chk_axiv_co2', 'value'=>2, 'uncheck'=>null, 'label'=>Yii::t('app/crf','Automatic')]) ?>
                                        </td>
                                        <td style='padding-right: 12px;'>
                                        <?= Html::activeRadio($model, 'axiv_co2', ['disabled'=>true, 'id'=>'chk_axiv_co2', 'value'=>1, 'uncheck'=>null, 'label'=>Yii::t('app/crf','Manual')]) ?>
                                        </td>
                                        <td style='padding-right: 12px;'>
                                        <?= Html::activeRadio($model, 'axiv_co2', ['disabled'=>true, 'id'=>'chk_axiv_co2', 'value'=>0, 'uncheck'=>null, 'label'=>Yii::t('app/crf','Not taken')]) ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style='padding-right: 15px;'>
                                        <strong><?= Yii::t('app/crf','Call to muster') ?></strong>
                                        </td>
                                        <td style='padding-right: 12px;'>
                                        <?= Html::activeRadio($model, 'axiv_ctm', ['disabled'=>true, 'id'=>'chk_axiv_ctm', 'value'=>2, 'uncheck'=>null, 'label'=>Yii::t('app/crf','At stations')]) ?>
                                        </td>
                                        <td style='padding-right: 12px;'>
                                        <?= Html::activeRadio($model, 'axiv_ctm', ['disabled'=>true, 'id'=>'chk_axiv_ctm', 'value'=>1, 'uncheck'=>null, 'label'=>Yii::t('app/crf','At lifeboats')]) ?>
                                        </td>
                                        <td style='padding-right: 12px;'>
                                        <?= Html::activeRadio($model, 'axiv_ctm', ['disabled'=>true, 'id'=>'chk_axiv_ctm', 'value'=>0, 'uncheck'=>null, 'label'=>Yii::t('app/crf','Not taken')]) ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style='padding-right: 15px;'>
                                        <strong><?= \Yii::t('app/crf' ,'Other') ?></strong>
                                        </td>
                                        <td style='padding-right: 12px;' colspan="2">
                                        <?= Html::activeCheckbox($model, 'axiv_other', [
                                            'label'=>'',
                                            'disabled'=>'disabled',
                                            
                                            'class'=>'sy_data_toggle',
                                            'sy_data_toggle_dest'=>'axiv_container',
                                            'sy_data_toggle_src_type'=>'checkbox',
                                            'sy_data_toggle_visible_value'=>'true',
                                            'sy_data_toggle_enabled'=>'true',
                                            
                                            ]) ?>
                                        <span id="axiv_container" class="hiddenBlock">
                                            <?= NullValueFormatter::Format($model->axiv_specify) ?>
                                        </span>
                                        </td>
                                    </tr>
                                    
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  <!--end section A.1.XIV -->
            

            <!--
            SECTION A.1.XV
            -->
            <div class="row">
                <div class="col-lg-12 sy_pad_bottom_18 sy_pad_top_6">
                    <div class="content">
                        <div class="row">
                            <div class="col-lg-12">
                                <strong>XV. <?= CrfHelper::SectionsTitle()['A.1.15'] ?></strong> 
                            </div>
                            <div class='col-lg-12 sy_pad_top_6'>
                                <?= nl2br(NullValueFormatter::Format($model->a1xv)) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  <!--end section A.1.XV -->
            
            
        </div>
    </div>

</div> <!-- end Section1 form -->

<?php

$JS = <<< EOF

$('input[type=checkbox]').each(function() {
    $ (this).attr('disabled', 'disabled');    
});        
$('input[type=text]').each(function() {
    $ (this).attr('disabled', 'disabled');    
});        

$('input[type=radio]').each(function() {
    $ (this).attr('disabled', 'disabled');    
});        

// $("input[id^='is_major_']").each(function() {
//     $ (this).attr('disabled', null);    
// });        


EOF;

$this->registerJs($JS, yii\web\View::POS_READY);

?>