<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

?>

    <div class="alert alert-info" role="alert">
        <h3>
            There is <strong>NO hydrocarbon release</strong> reported.
        </h3>
    </div>
