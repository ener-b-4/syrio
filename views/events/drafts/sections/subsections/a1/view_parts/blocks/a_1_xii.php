<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */


/**
 * Performs a validation and rendering logic for section a_1_xii
 * Render block in $a1xii_block
 *
 * @var $model app\models\crf\Section1
 * 
 * @author bogdanV
 */

/* @var $model app\models\crf\Section1 */

if (isset($arr))
{
    unset($arr);
}
$arr=[];

$arr[] =                 '<strong class="strong text-primary">XII. Did ignition occured? </strong>';
if (isset($model->axii))
{
    if ($model->axii==1)
    {
        if (isset($model->axii_code))   
        {
            if ($model->axii_code == 1)             //ignition immediate
            {
                $arr[] = 'YES: immediate<br/>';
            }
            else if ($model->axii_code_time > 0)   // igntion delay - time ok
            {
                $arr[] = 'YES: delayed with ' . $model->axii_code_time . ' s<br/>';
                
                $seqArr = create_fire_sequence_view($model);
                $arr[] = '<strong class="text-info">Fire sequence:</strong> ';
                if (count($seqArr[0]) == 4)
                {
                    //no sequence defined - err
                    $arr[] = 'NO SEQUENCE defined!';
                $arr[] = '<a href=\'' . trim($url) . '&s=12\' aria-label="Left Align">
                   <span class="glyphicon glyphicon-pencil"></span>
                </a><br/>';                    
                }
                else
                {
                    $follow=' followed by ';
                    $and = ' and ';
                    $s = '';
                    for ($i = 1; $i < 5; $i++)
                    {
                        $items = count($seqArr[$i]);
                        $ss = '';
                        if ($items!=0)
                        {
                            if ($items == 1)
                            {
                                $ss = $ss . $seqArr[$i][0];
                            }
                            else
                            {
                                $ss = $ss . implode($and, $seqArr[$i]);
                            }
                            
                            $s=='' ? $s=$ss : $s = $s . $follow . $ss;
                        }
                    }
                    
                    $arr[] = $s;
                $arr[] = '<a href=\'' . trim($url) . '&s=12\' aria-label="Left Align">
                   <span class="glyphicon glyphicon-pencil"></span>
                </a><br/>';
                }
            }
            else                                    // ignition delayed - time wrong!
            {
                $arr[] = 'ignition: delayed. BAD timing: ' . $model->axii_code_time;
                $arr[] = '<a href=\'' . trim($url) . '&s=12\' aria-label="Left Align">
                   <span class="glyphicon glyphicon-pencil"></span>
                </a><br/>';
                        
            }
        }
        else //no ignition type has been selected!
        {
            $arr[] = 'no ignition TYPE has been selected<br/>';
        }
    }
    else
    {
        //there've been no ignition
        $arr[] = 'no ignition occured';
        $arr[] = '<a href=\'' . trim($url) . '&s=12\' aria-label="Left Align">
                   <span class="glyphicon glyphicon-pencil"></span>
                </a>';
        $arr[] = '<br/>';
    }
}
else
{
    //value not provided!
    //required!

    $a1vii_block_arr[] = ' <em class="text-warning">(not set)</em>';
}

$a1xii_block = implode('', $arr);
unset($arr);

/*
 * Renders the fire types sequence
 * 
 * @param Section1 $model
 * 
 */
function create_fire_sequence_view($model)
{
    $seqArr = array(0 => [], 1 => [], 2 => [], 3 => [], 4 => []);
    if (isset($model->axii_flash_index)) $seqArr[$model->axii_flash_index][] = 'flash fire';
    if (isset($model->axii_exp_index)) $seqArr[$model->axii_exp_index][] =  'explosion';
    if (isset($model->axii_jet_index)) $seqArr[$model->axii_jet_index][] =  'jet fire';
    if (isset($model->axii_pool_index)) $seqArr[$model->axii_pool_index][] =  'pool fire';
    
    return $seqArr;
}