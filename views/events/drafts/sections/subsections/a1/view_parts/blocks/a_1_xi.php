<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */


/**
 * Performs a validation and rendering logic for section a_1_x
 * Render block in $a1xii_block
 *
 * @var $model app\models\crf\Section1
 * 
 * @author bogdanV
 */

/* @var $model app\models\crf\Section1 */

use app\models\crf\constants\LeakCause;
use app\models\constants\Constants;
    $url = Yii::$app->getUrlManager()->createUrl([
        '/events/drafts/sections/subsections/a1/update', 
        'id' => $model->id,
    ]);

if (isset($arr))
{
    unset($arr);
}
$arr=[];

    //XI. Cause of leak
    //  description
    $arr[]='    <strong class="strong text-primary">XI. Causes of leak: </strong>';
    if (isset($model->axi_description) && $model->axi_description != '')
    {
        $arr[] = $model->axi_description;
    }
    else
    {
        //not set
        $arr[] = '<em class="text-warning">(not set)</em>';        
    }
    $arr[] = '<a href=\'' . trim($url) . '&s=11\' aria-label="Left Align">
                   <span class="glyphicon glyphicon-pencil"></span>
                </a>';
    
    unset($temp);
    $temp=[];
    
    //drilldown causes of leak
    
    if (isset($model->a1_fail_design_code))
    {
        if ($model->a1_fail_design_code!=1)  //NOT none
        {
            $leakCause = LeakCause::findOne($model->a1_fail_design_code)->cause;

            $temp[] = '<strong class="text-primary" style = "padding-left:12px;">Design: </strong>';
            $temp[] = $leakCause;
            $temp[] = '<br/>';
        }
        else
        {
            $temp[] = '<strong class="text-primary" style = "padding-left:12px;">Design: </strong>';
            $temp[] = 'none';
            $temp[] = '<br/>';
        }
    }
    
    if (isset($model->a1_fail_equipment_code))
    {
        if ($model->a1_fail_equipment_code == 2) //'other' code
        {
            if ($model->a1_fail_equipment_specify == '')
            {
                //other has been selected but no specification has been made
                $temp[] = '<strong class="text-primary" style = "padding-left:12px;">Equipment: </strong>';
                $temp[] = '<em class="text-warning" style = "padding-left:12px;">(other has been been selected but no specification has been made)</em>';;
                $temp[] = '<br/>';
            }
            else
            {
                //other specified
                $temp[] = '<strong class="text-primary" style = "padding-left:12px;">Equipment: </strong>';
                $temp[] = $model->a1_fail_equipment_specify;
                $temp[] = '<em class="text-muted" > (other)</em><br/>';
            }
        }
        else if ($model->a1_fail_equipment_code != 1) //'none
        {
                //code specified
                $leakCause = LeakCause::findOne($model->a1_fail_equipment_code)->cause;
                $temp[] = '<strong class="text-primary" style = "padding-left:12px;">Equipment: </strong>';
                $temp[] = $leakCause;
                $temp[] = '<br/>';
        }
        else
        {
                $leakCause = LeakCause::findOne($model->a1_fail_operation_code)->cause;
                $temp[] = '<strong class="text-primary" style = "padding-left:12px;">Operation: </strong>';
                $temp[] = 'none';
                $temp[] = '<br/>';
        }
        
    }
    
    if (isset($model->a1_fail_operation_code))
    {
        if ($model->a1_fail_operation_code == 2) //'other' code
        {
            if ($model->a1_fail_operation_specify == '')
            {
                //other has been selected but no specification has been made
                $temp[] = '<strong class="text-primary" style = "padding-left:12px;">Operation: </strong>';
                $temp[] = '<em class="text-warning" style = "padding-left:12px;">(other has been been selected but no specification has been made)</em>';;
                $temp[] = '<br/>';
            }
            else
            {
                //other specified
                $temp[] = '<strong class="text-primary" style = "padding-left:12px;">Operation: </strong>';
                $temp[] = $model->a1_fail_operation_specify;
                $temp[] = '<em class="text-muted" > (other)</em><br/>';
            }
        }
        else if ($model->a1_fail_operation_code != 1) //'NOT none
        {
                //code specified
                $leakCause = LeakCause::findOne($model->a1_fail_operation_code)->cause;
                $temp[] = '<strong class="text-primary" style = "padding-left:12px;">Operation: </strong>';
                $temp[] = $leakCause;
                $temp[] = '<br/>';
        }
        else
        {
                $leakCause = LeakCause::findOne($model->a1_fail_operation_code)->cause;
                $temp[] = '<strong class="text-primary" style = "padding-left:12px;">Operation: </strong>';
                $temp[] = 'none';
                $temp[] = '<br/>';
        }
    }
    
    if (isset($model->a1_fail_procedural_code))
    {
        if ($model->a1_fail_procedural_code == 2) //'other' code
        {
            if ($model->a1_fail_procedural_specify == '')
            {
                //other has been selected but no specification has been made
                $temp[] = '<strong class="text-primary" style = "padding-left:12px;">Procedural: </strong>';
                $temp[] = '<em class="text-warning" style = "padding-left:12px;">(other has been been selected but no specification has been made)</em>';;
                $temp[] = '<br/>';
            }
            else
            {
                //other specified
                $temp[] = '<strong class="text-primary" style = "padding-left:12px;">Procedural: </strong>';
                $temp[] = $model->a1_fail_procedural_specify;
                $temp[] = '<em class="text-muted" > (other)</em><br/>';
            }
        }
        else if ($model->a1_fail_operation_code != 1) //'NOT none
        {
                //code specified
                $leakCause = LeakCause::findOne($model->a1_fail_procedural_code)->cause;
                $temp[] = '<strong class="text-primary" style = "padding-left:12px;">Procedural: </strong>';
                $temp[] = $leakCause;
                $temp[] = '<br/>';
        }
        else
        {
                $leakCause = LeakCause::findOne($model->a1_fail_procedural_code)->cause;
                $temp[] = '<strong class="text-primary" style = "padding-left:12px;">Procedural: </strong>';
                $temp[] = 'none';
                $temp[] = '<br/>';
        }
    }    
    
    
    
    
    if (count($temp) !=0 )
    {
        $arr[] = '<br/><strong class="text-primary" style="padding-left=12px;"><em>Causes of leak - drilldown</em></strong><br/>';
        $arr[] = implode('', $temp);
    }
    
    
    $opModeView = create_op_mode_view($model);
    $arr[] = '<strong class="text-primary" style="padding-left=12px;"><em>Operational mode in the area at the time of release: </em></strong>';
    $arr[] = $opModeView;
    
    $a1xi_block = implode('', $arr);
    unset($arr);
    $arr=[];
    
    //END  XI drilldown causes of leak

function create_op_mode_view($model)
{
    if (isset($temp)) unset($temp);
    $temp=[];
    
    if (isset($model->a1_op))
    {
        $operation = Constants::findOne($model->a1_op)->name;
        switch ($model->a1_op)
        {
            case 602:
                $temp[] = 'Well operations: ';
                //check if the operation is specified
                if (isset($model->a1_op_specify) && $model->a1_op_specify!='')
                {
                    $temp[] = $model->a1_op_specify;
                }
                else
                {
                    $temp[] = ' <i>operation not specified</i>';
                }
                break;
            default:
                $temp[] = $operation . '<br/>';
        }
    }
    else
    {
        //missing required info
        $temp[] = 'Operational mode information missing!<br/>';
    }
    
    return implode('', $temp);
}