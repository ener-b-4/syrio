<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */


/**
 * Performs a validation and rendering logic for section a_1_xiii
 * Render block in $a1xii_block
 *
 * @var $model app\models\crf\Section1
 * 
 * @author bogdanV
 */

/* @var $model app\models\crf\Section1 */

if (isset($arr))
{
    unset($arr);
}
$arr=[];

if (isset($model->axiii))
{
    if ($model->axiii!='')
    {
        $arr[] = '<strong class="text-info">XIII. Ignition source:</strong> ' . $model->axiii;
                $arr[] = '<a href=\'' . trim($url) . '&s=13\' aria-label="Left Align">
                   <span class="glyphicon glyphicon-pencil"></span>
                </a><br/>';           
        
    }
    else
    {
        //there've been no ignition
        $arr[] = '<strong class="text-info">XIII. Ignition source:</strong> ' . 'uknown';
                $arr[] = '<a href=\'' . trim($url) . '&s=13\' aria-label="Left Align">
                   <span class="glyphicon glyphicon-pencil"></span>
                </a><br/>';           
    }
}
else
{
    //value not provided!
    //required!

    $a1vii_block_arr[] = ' <em class="text-warning">(not set)</em>';
}

$a1xiii_block = implode('', $arr);
unset($arr);

