<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */


  use yii\helpers\Html;

  /* @var $this yii\web\View */
  /* @var $model app\models\crf\Section1 */


  $section_name = 'A';
  $this->title = \Yii::t('app', 'SECTION {evt}', ['evt'=>$section_name]);

?>

<div class="a1-view col-lg-12">

  <?php if (!isset($from_ca) || $from_ca != 1) {?>
  
  <h3><strong><?= strtoupper($this->title) ?></strong></h3>
  <div class="form-group">
      <?php echo Html::a(Yii::t('app', '<i class="fa fa-file-pdf-o"></i> PDF'), 
        ['pdfgenerator/default/index', 'id' => $event_id , 'section' => $section_name], 
        ['class' => 'btn btn-default']); 
      ?>
      
      <?php
        if ($model->sA->status < \app\models\events\EventDeclaration::INCIDENT_REPORT_FINALIZED && !isset($model->sA->locked_by)) {
          echo Html::a(Yii::t('app', 'Finalize {evt}', ['evt'=>$section_name]), ['events/drafts/event-draft/finalize-section', 'id' => $model->sA->eventDraft->id, 's'=>'A'], ['class' => 'btn btn-success pull-right']);
        }
      ?>
  </div>
  <!--(end) Bug 7 - Finalisation of "Generate PDF" -->
  
  <p>
    <?php
      // use this on any view to include the actionmessage logic and widget 
      //include Yii::getAlias('@app') . '\views\shared_parts\message_snippet.php';
      include Yii::getAlias('@app') . '/views/shared_parts/message_snippet.php';
    ?>

  </p>
  
  <?php } //end of the stripped part?>
  
  <div class="col-lg-12" style="padding-top: 16px; padding-left: -12px; padding-right: -12px;">
    <?php
//        if ($model->a1==0)
//        {
//            include_once 'view_parts/_noRelease.php';
//        }
//        else
//        {
            include_once 'view_parts/_review.php';
//        }
    ?>  
  </div>

</div>

