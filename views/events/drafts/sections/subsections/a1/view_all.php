<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

/* @var $this yii\web\View */
/* @var $model app\models\crf\Section1 */

include_once 'view_parts/_review.php';