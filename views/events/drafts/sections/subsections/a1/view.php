<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */


  use yii\helpers\Html;

  /* @var $this yii\web\View */
  /* @var $model app\models\crf\Section1 */


  $section_name = 'A';
  $subsection_name = '1';
  $title_subsection = $section_name . '.' . $subsection_name . '.';           //'A.1.'

  $this->title = Yii::t('app/crf', 'Section {id}', ['id'=>$title_subsection]);

  echo $this->render('/shared_parts/_breadcrumbs', [
    'title_subsection' => $title_subsection, 
    'module_name' => 'ADD_SUBSECTION'
  ]);

?>

<div class="a1-view col-lg-12">

  <p>
    <?php
//                    echo '<pre>';
//                    echo var_dump($model->sA->eventDraft);
//                    echo '</pre>';
        //$path = UrlManager::createUrl('crf/section-1');

      // use this on any view to include the actionmessage logic and widget 
      //include Yii::getAlias('@app') . '\views\shared_parts\message_snippet.php';
      include Yii::getAlias('@app') . '/views/shared_parts/message_snippet.php';

      if ($model->sA->status == \app\models\events\EventDeclaration::INCIDENT_REPORT_DRAFT)
      { 
        //echo Html::a(Yii::t('app', 'Modify'), ['events/drafts/sections/subsections/a1/update', 'id' => $model->id, 's' => 0], ['class' => 'btn btn-primary']);
        //echo Html::a(Yii::t('app', 'Finalize'), ['events/drafts/event-draft/finalize-section', 'id' => $model->sA->eventDraft->id, 's'=>'A'], ['class' => 'btn btn-success pull-right']);
        echo Html::a(Yii::t('app', 'Modify'), ['events/drafts/sections/subsections/a1/update', 'id' => $model->id], ['class' => 'btn btn-primary']);
        echo Html::a(Yii::t('app', 'Finalize {evt}', ['evt'=>$section_name]), ['events/drafts/event-draft/finalize-section', 'id' => $model->sA->eventDraft->id, 's'=>'A'], ['class' => 'btn btn-success pull-right']);

      }
      //(start) only display pdf if not stripped = 2
      if (!isset($stripped) || $stripped !== 2) {
          echo Html::a(Yii::t('app', '<i class="fa fa-file-pdf-o"></i> PDF'), 
      ['pdfgenerator/default/index', 'id' => $model->sA->eventDraft->id , 'section' => $section_name.'.'.$subsection_name.'.'], 
          ['class' => 'btn btn-default']); 
      }
      //(end) Bug 7 - Finalisation of "Generate PDF"

    ?>

  </p>

    <div class="col-lg-12" style="padding-top: 16px; padding-left: -12px; padding-right: -12px;">
        <?php
            include_once 'view_parts/_review.php';
//            if ($model->a1==0)
//            {
//                include_once 'view_parts/_withRelease.php';
//            }
//            else
//            {
//                include_once 'view_parts/_withRelease.php';
//            }
        ?>
    </div>

</div>

