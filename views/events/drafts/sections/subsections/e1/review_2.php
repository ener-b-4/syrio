<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

  use yii\helpers\Html;

  /* @var $this yii\web\View */
  /* @var $model app\models\events\drafts\sections\subsections\E1 */

?>
<div class="e1-view col-lg-12">

    <table>
        <tbody>
            <tr class="sy_para">
                <td class="sy_crf_para"><strong>E.1.</strong></td>
                <td class="sy_crf_para">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <strong><?= Yii::t('app/crf', 'General information') ?></strong>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="sy_crf_para"></td>
                <td class="sy_crf_para">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <ul class="list-inline sy_padding_clear">
                            <li>(a)  <?= Yii::t('app/crf', 'Name / Flag State of vessel (If applicable)') ?>: </li>
                            <li><?= isset($model->e1_1) && trim($model->e1_1)!='' ? $model->e1_1:'<span class="sy_not_set">' . \Yii::t('app', '(not set)') . '</span>' ?></li>
                        </ul>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <ul class="list-inline sy_padding_clear">
                            <li>(b)  <?= Yii::t('app/crf', 'Type / Tonnage of vessel (If applicable)') ?>: </li>
                            <li><?= isset($model->e1_2) && trim($model->e1_2)!='' ? $model->e1_2:'<span class="sy_not_set">' . \Yii::t('app', '(not set)') . '</span>' ?></li>
                        </ul>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <ul class="list-inline sy_padding_clear">
                            <li>(c)  <?= Yii::t('app/crf', 'Contact via AIS?') ?>: </li>
                            <li><?= isset($model->e1_3) && trim($model->e1_3)!='' ? $model->e1_3:'<span class="sy_not_set">' . \Yii::t('app', '(not set)') . '</span>' ?></li>
                        </ul>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>

</div>
