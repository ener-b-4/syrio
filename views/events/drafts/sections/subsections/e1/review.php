<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

  use yii\helpers\Html;

  /* @var $this yii\web\View */
  /* @var $model app\models\events\drafts\sections\subsections\E1 */

    $strip = isset($stripped);
    if (!$strip) { 
        //(start) Bug 7 - Finalisation of "Generate PDF" 
        $section_name = 'E';
        $this->title = \Yii::t('app', 'SECTION {evt}', ['evt'=>$section_name]);
        //(end) Bug 7 - Finalisation of "Generate PDF" 
    }
  
  
?>
<div class="e1-view col-lg-12">

  <?php if (!isset($from_ca) || $from_ca != 1) {?>
    
  <!--(start) Bug 7 - Finalisation of "Generate PDF" -->
  <h3><strong><?= strtoupper($this->title) ?></strong></h3>
  <?php echo Html::a(Yii::t('app', '<i class="fa fa-file-pdf-o"></i> PDF'), 
    ['pdfgenerator/default/index', 'id' => $event_id , 'section' => $section_name], 
    ['class' => 'btn btn-default']); 
  ?>
  <!--(end) Bug 7 - Finalisation of "Generate PDF" -->
  
  <?php
    if ($model->sE->status < \app\models\events\EventDeclaration::INCIDENT_REPORT_FINALIZED && !isset($model->sE->locked_by)) {
      echo Html::a(Yii::t('app', 'Finalize {evt}', ['evt'=>$section_name]), ['events/drafts/event-draft/finalize-section', 'id' => $model->sE->eventDraft->id, 's'=>'E'], ['class' => 'btn btn-success pull-right']); 
    }
  ?>
  
    <p>
        <?php
        if (!$strip)
        {
            
            /* use this on any view to include the actionmessage logic and widget */
            //include Yii::getAlias('@app') . '\views\shared_parts\message_snippet.php';
            include Yii::getAlias('@app') . '/views/shared_parts/message_snippet.php';
            
            
        }
        ?>
         
    </p>

 <?php } //end ca_from ?>
    
    <table>
        <tbody>
            <tr class="sy_para">
                <td class="sy_crf_para"><strong>E.1.</strong></td>
                <td class="sy_crf_para">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <strong><?= Yii::t('app/crf', 'General information') ?></strong>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="sy_crf_para"></td>
                <td class="sy_crf_para">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <ul class="list-inline sy_padding_clear">
                            <li>(a)  <?= Yii::t('app/crf', 'Name / Flag State of vessel (If applicable)') ?>: </li>
                            <li><?= isset($model->e1_1) && trim($model->e1_1)!='' ? $model->e1_1:'<span class="sy_not_set">' . \Yii::t('app', '(not set)') . '</span>' ?></li>
                        </ul>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <ul class="list-inline sy_padding_clear">
                            <li>(b)  <?= Yii::t('app/crf', 'Type / Tonnage of vessel (If applicable)') ?>: </li>
                            <li><?= isset($model->e1_2) && trim($model->e1_2)!='' ? $model->e1_2:'<span class="sy_not_set">' . \Yii::t('app', '(not set)') . '</span>' ?></li>
                        </ul>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <ul class="list-inline sy_padding_clear">
                            <li>(c)  <?= Yii::t('app/crf', 'Contact via AIS?') ?>: </li>
                            <li><?= isset($model->e1_3) && trim($model->e1_3)!='' ? $model->e1_3:'<span class="sy_not_set">' . \Yii::t('app', '(not set)') . '</span>' ?></li>
                        </ul>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>

</div>
