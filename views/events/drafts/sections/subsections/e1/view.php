<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

  use yii\helpers\Html;

  /* @var $this yii\web\View */
  /* @var $model app\models\events\drafts\sections\subsections\E1 */
  $strip = isset($stripped);
      //(start) Bug 7 - Finalisation of "Generate PDF" 
      $section_name = 'E';
      $subsection_name = '1';
      $title_subsection = $section_name . '.' . $subsection_name . '.';           //'B.4.'
      //(end) Bug 7 - Finalisation of "Generate PDF" 

  if (!$strip)
  {
      
      $this->title = Yii::t('app/crf', 'Section {id}', ['id'=>$title_subsection]);  

    echo $this->render('/shared_parts/_breadcrumbs', [
      'title_subsection' => $title_subsection, 
      'module_name' => 'ADD_SUBSECTION'
    ]);
  }
?>
<div class="e1-view col-lg-12">

    <p>
        <?php
        if (!$strip)
        {
            
          /* use this on any view to include the actionmessage logic and widget */
          //include Yii::getAlias('@app') . '\views\shared_parts\message_snippet.php';
          include Yii::getAlias('@app') . '/views/shared_parts/message_snippet.php';
          
           if ($model->sE->status == \app\models\events\EventDeclaration::INCIDENT_REPORT_DRAFT)
            { 
                echo Html::a(Yii::t('app', 'Modify'), ['events/drafts/sections/subsections/e1/update', 'id' => $model->id], ['class' => 'btn btn-primary']);
                echo Html::a(Yii::t('app', 'Finalize {evt}', ['evt'=>$section_name]), ['events/drafts/event-draft/finalize-section', 'id' => $model->sE->eventDraft->id, 's'=>'E'], ['class' => 'btn btn-success pull-right']);
            }
            
        }
        
      //(start) only display pdf if not stripped = 2
      if (!isset($stripped) || $stripped !== 2) {
      echo Html::a(Yii::t('app', '<i class="fa fa-file-pdf-o"></i> PDF'), 
          ['pdfgenerator/default/index', 'id' => $model->sE->eventDraft->id , 'section' => $section_name.'.'.$subsection_name.'.'], 
      ['class' => 'btn btn-default']); 
      }        
        ?>
         
    </p>

    <table>
        <tbody>
            <tr class="sy_para">
                <td class="sy_crf_para sy_section_h1">E.1.</td>
                <td class="sy_crf_para sy_section_h1">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <?= Yii::t('app/crf', 'General information') ?>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="sy_crf_para"></td>
                <td class="sy_crf_para">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <ul class="list-inline sy_padding_clear">
                            <li class="text-info">(a)  <?= Yii::t('app/crf', 'Name / Flag State of vessel (If applicable)') ?>: </li>
                            <li><?= isset($model->e1_1) && trim($model->e1_1)!='' ? $model->e1_1:'<span class="sy_not_set">' . \Yii::t('app', '(not set)') . '</span>' ?></li>
                        </ul>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <ul class="list-inline sy_padding_clear">
                            <li class="text-info">(b)  <?= Yii::t('app/crf', 'Type / Tonnage of vessel (If applicable)') ?>: </li>
                            <li><?= isset($model->e1_2) && trim($model->e1_2)!='' ? $model->e1_2:'<span class="sy_not_set">' . \Yii::t('app', '(not set)') . '</span>' ?></li>
                        </ul>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <ul class="list-inline sy_padding_clear">
                            <li class="text-info">(c)  <?= Yii::t('app/crf', 'Contact via AIS?') ?>: </li>
                            <li><?= isset($model->e1_3) && trim($model->e1_3)!='' ? $model->e1_3:'<span class="sy_not_set">' . \Yii::t('app', '(not set)') . '</span>' ?></li>
                        </ul>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>

</div>
