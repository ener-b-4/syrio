<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\events\drafts\sections\subsections\C1 */
/* @var $form yii\widgets\ActiveForm */

//$distUnits=ArrayHelper::map(\app\models\units\Units::find()->where(['scope'=>6])->asArray()->all(), 'id', 'unit');

?>

<div class="b1-form">

    <?php $form = ActiveForm::begin(); ?>

    <table>
        <tbody>
            <tr class="sy_para">
                <td class="sy_crf_para sy_section_h1">C.1.</td>
                <td class="sy_crf_para sy_section_h1">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <strong><?= \Yii::t('app/crf', 'General information'); ?></strong>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="sy_crf_para"></td>
                <td class="sy_crf_para">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <ul class="list-inline sy_padding_clear">
                            <li class="text-info sy_vert_top sy_pad_top_6">(a)  <?= \Yii::t('app/crf', 'Name of the independent verifier (if applicable)'); ?>: </li>
                            <li><?= $form->field($model, 'c1_1')->textInput()->label('',['class'=>'hiddenBlock']) ?></li>
                        </ul>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    

    <?php ActiveForm::end(); ?>

</div>

<?php
$js = <<< 'SCRIPT'
SCRIPT;
// Register tooltip/popover initialization javascript
$this->registerJs($js);
?>