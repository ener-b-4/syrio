<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

  use yii\helpers\Html;

  /* @var $this yii\web\View */
  /* @var $model app\models\events\drafts\sections\subsections\C1 */
?>

<div class="c1-view col-lg-12">

  <table>
    <tbody>
      <tr class="sy_para">
          <td class="sy_crf_para"><strong>C.1.</strong></td>
        <td class="sy_crf_para">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <strong><?= \Yii::t('app/crf', 'General information'); ?></strong>
          </div>
        </td>
      </tr>
      <tr>
        <td class="sy_crf_para"></td>
        <td class="sy_crf_para">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <ul class="list-inline sy_padding_clear">
                <li>(a)  <?= \Yii::t('app/crf', 'Name of the independent verifier (if applicable)'); ?>: </li>
                <li><?= isset($model->c1_1) && trim($model->c1_1)!='' ? $model->c1_1:'<span class="sy_not_set">' . \Yii::t('app', '(not set)') . '</span>' ?></li>
            </ul>
          </div>
        </td>
      </tr>
    </tbody>
  </table>

</div>
