<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

  use yii\helpers\Html;

  /* @var $this yii\web\View */
  /* @var $model app\models\events\drafts\sections\subsections\B3 */

  $strip = isset($stripped);
?>

<div class="b3-view col-lg-12">

    <p>
        <?php
        if (!$strip)
        {
            // use this on any view to include the actionmessage logic and widget 
            //include Yii::getAlias('@app') . '\views\shared_parts\message_snippet.php';
            include Yii::getAlias('@app') . '/views/shared_parts/message_snippet.php';
            
        }
        ?>       

    </p>

    <table>
        <tbody>
            <tr class="sy_para">
                <td class="sy_crf_para"><strong>B.3.</strong></td>
                <td class="sy_crf_para" style='width:100%;'>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <!--<strong>Preliminary <i>direct</i> and <i>underlying</i> causes<br/></strong>-->
                        <strong><em><?= Yii::t('app/crf', 'Preliminary direct and underlying causes.') ?></em> </strong>
                        <span class='text-danger'><?= Yii::t('app/crf', '(within 10 days of the event)') ?> </span>
                        <br/>
                        <div class='sy_pad_top_6'>
                        <?php
                          $s='';
                          $s .= '<div>'
                              . nl2br(\app\models\shared\NullValueFormatter::Format($model->b3_1)) 
                             . '</div>';

                          echo $s;
                        ?>
                        </div>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    
</div>
