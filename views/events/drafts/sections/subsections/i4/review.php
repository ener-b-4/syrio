<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\events\drafts\sections\subsections\I4 */

  $strip = isset($stripped);
?>
<div class="i4-view col-lg-12">

    <p>
        <?php
        if (!$strip)
        {
            // use this on any view to include the actionmessage logic and widget 
            //include Yii::getAlias('@app') . '\views\shared_parts\message_snippet.php';
            include Yii::getAlias('@app') . '/views/shared_parts/message_snippet.php';
            
        }
        ?>               
        
    </p>

    <table>
        <tbody>
            <tr class="sy_para">
                <td class="sy_crf_para"><strong>I.4.</strong></td>
                <td class="sy_crf_para" style='width:100%;'>
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <span>
                            <strong><?= Yii::t('app/crf', 'Initial lessons learned and preliminary recommendations to prevent recurrence of similar events') ?></strong>
                        </span>
                        <br/>
                        <span class='text-danger sy_narative-text'>
                            <?= Yii::t('app/crf', '(within 10 days of the event)') ?>
                        </span>
                        <div class='sy_pad_top_6'>
                        <?php
                          $s='';
                          $s .= '<div>'
                              . nl2br(\app\models\shared\NullValueFormatter::Format($model->i4_1)) 
                             . '</div>';

                          echo $s;
                        ?>
                        </div>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    
</div>
