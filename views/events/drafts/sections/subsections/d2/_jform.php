<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\events\drafts\sections\subsections\D2 */
/* @var $form yii\widgets\ActiveForm */

$timeUnits=ArrayHelper::map(\app\models\units\Units::find()->where(['scope'=>2])->asArray()->all(), 'id', 'unit');
$flowUnits=ArrayHelper::map(\app\models\units\Units::find()->where(['scope'=>3])->asArray()->all(), 'id', 'unit');
$volumeUnits=ArrayHelper::map(\app\models\units\Units::find()->where(['scope'=>8])->asArray()->all(), 'id', 'unit');

?>

<div class="d2-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <table>
        <tbody>
            <tr class="sy_para">
                <td class="sy_crf_para">D.2.</td>
                <td class="sy_crf_para">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <strong>Description of circumstances, consequences of event and emergency response</strong>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="sy_crf_para"></td>                
                <td class="sy_crf_para" style='width:100%;'>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 sy_padding_clear">
                        Consequences of event and emergency response
                        <br/>
                        <?= $form->field($model, 'd2_1')->textarea(['class'=>'sy_fill_textarea'])->label('') ?>                      
                    </div>
                </td>                
            </tr>        
        </tbody>
    </table>    
    
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), [
            'id'=>'btnSubmit',
            'onclick' => 'proceed = 1;',
            'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app/commands', 'Cancel'), 
                ['cancel', 'id' => $model->id, 'ref' => $model->ref], 
                [
                    'class' => 'btn btn-warning',
                    'onclick' => 'proceed = 1',
                    'id' => 'btnCancel'
                ]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
