<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\units\Units;

/* @var $this yii\web\View */
/* @var $model app\models\events\drafts\sections\subsections\D2 */

$strip = isset($stripped);

if (!$strip)
{
    $this->title = $model->id;
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'D2s'), 'url' => ['index']];
    $this->params['breadcrumbs'][] = $this->title;
}
?>
<div class="d2-view">


    <p>
        <?php
        if (!$strip)
        {
           if ($model->sD->status == \app\models\events\EventDeclaration::INCIDENT_REPORT_DRAFT)
            { 
                echo Html::a(Yii::t('app', 'Modify'), ['events/drafts/sections/subsections/d2/update', 'id' => $model->id], ['class' => 'btn btn-primary']);
                echo Html::a(Yii::t('app', 'Finalize {evt}', ['evt'=>$section_name]), ['events/drafts/event-draft/finalize-section', 'id' => $model->sD->eventDraft->id, 's'=>'D'], ['class' => 'btn btn-success pull-right']);
            }
            else if ($model->sD->status == \app\models\events\EventDeclaration::INCIDENT_REPORT_LOCKED)
            {
                $s = '<div class="alert alert-info" role="alert">'
                        . '<p>Opened in read-only mode</p>'
                        . '</div>';
                echo $s;
            }
            else if ($model->sD->status == \app\models\events\EventDeclaration::INCIDENT_REPORT_FINALIZED)
            {
                $s = '<div class="alert alert-info" role="alert">'
                        . '<p>Finalized</p>'
                        . '</div>';
                echo $s;                
            }
        }
        ?>        
      
    </p>

    <table>
        <tbody>
            <tr class="sy_para">
                <td class="sy_crf_para">D.2.</td>
                <td class="sy_crf_para">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <strong>Description of circumstances, consequences of event and emergency response</strong>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="sy_crf_para"></td>                
                <td class="sy_crf_para" style='width:100%;'>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 sy_padding_clear">
                        Consequences of event and emergency response
                        <br/>
                        <?= Html::textarea('d2_1', $model->d2_1, ['class'=>'sy_fill_textarea', 'readonly'=>true]) ?>
                    </div>
                </td>                
            </tr>            
        </tbody>
    </table>
</div>
