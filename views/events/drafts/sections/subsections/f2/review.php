<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

  use yii\helpers\Html;

  /* @var $this yii\web\View */
  /* @var $model app\models\events\drafts\sections\subsections\F2 */
  $strip = isset($stripped);
?>
<div class="f2-view col-lg-12">

    <p>
        <?php
        if (!$strip)
        {
            
            /* use this on any view to include the actionmessage logic and widget */
            //include Yii::getAlias('@app') . '\views\shared_parts\message_snippet.php';
            include Yii::getAlias('@app') . '/views/shared_parts/message_snippet.php';
            
        }
        ?>
         
    </p>

    <table>
        <tbody>
            <tr class="sy_para">
                <td class="sy_crf_para"><strong>F.2.</strong></td>
                <td class="sy_crf_para">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <strong><?= Yii::t('app/crf', 'Description of circumstances, consequences of event and emergency response') ?></strong>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="sy_crf_para"></td>                
                <td class="sy_crf_para" style='width:100%;'>
                    <div class="col-lg-12">
                        <p>
                            <?php
                            $s1 = 'Indicate the system that failed and provide a description of the circumstances of the event / describe what has happened (weather conditions)';
                            echo Yii::t('app/crf', $s1);
                            ?>
                        </p>
                        <?php
                          $s='';
                          $s .= '<div>'
                              . nl2br(\app\models\shared\NullValueFormatter::Format($model->f2_1)) 
                             . '</div>';

                          echo $s;
                        ?>
                        
                    </div>
                </td>                
            </tr>            
        </tbody>
    </table>

</div>
