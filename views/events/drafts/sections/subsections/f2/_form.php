<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\events\drafts\sections\subsections\F2 */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="f2-form">

    <?php $form = ActiveForm::begin(); ?>

    <table>
        <tbody>
            <tr class="sy_para">
                <td class="sy_crf_para sy_section_h1">F.2.</td>
                <td class="sy_crf_para sy_section_h1" style='width:100%;'>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <strong><?= Yii::t('app/crf', 'Description of circumstances, consequences of event and emergency response') ?></strong>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="sy_crf_para"></td>                
                <td class="sy_crf_para" style='width:100%;'>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 sy_padding_clear">
                        <p>
                            <?php
                            $s1 = 'Indicate the system that failed and provide a description of the circumstances of the event / describe what has happened (weather conditions)';
                            echo Yii::t('app/crf', $s1);
                            ?>
                        </p>
                        <?= $form->field($model, 'f2_1')->textarea(['class'=>'sy_fill_textarea form-control'])->label('') ?>                    
                    </div>
                </td>                
            </tr>
        </tbody>
    </table>

    <?php ActiveForm::end(); ?>

</div>
