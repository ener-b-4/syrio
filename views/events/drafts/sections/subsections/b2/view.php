<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

  use yii\helpers\Html;
  use app\models\units\Units;

  use app\models\shared\NullValueFormatter;
  
  /* @var $this yii\web\View */
  /* @var $model app\models\events\drafts\sections\subsections\B2 */

  $strip = isset($stripped);

      //(start) Bug 7 - Finalisation of "Generate PDF" 
      $section_name = 'B';
      $subsection_name = '2';
      $title_subsection = $section_name . '.' . $subsection_name . '.';           //'B.1.'
      //(end) Bug 7 - Finalisation of "Generate PDF" 
  
  if (!$strip)
  {
    $this->title = Yii::t('app/crf', 'Section {id}', ['id'=>$title_subsection]);  
    
    echo $this->render('/shared_parts/_breadcrumbs', [
      'title_subsection' => $title_subsection, 
      'module_name' => 'ADD_SUBSECTION'
    ]);
  }
?>

<div class="b2-view col-lg-12">


    <p>
        <?php
        if (!$strip)
        {
            // use this on any view to include the actionmessage logic and widget 
            //include Yii::getAlias('@app') . '\views\shared_parts\message_snippet.php';
            include Yii::getAlias('@app') . '/views/shared_parts/message_snippet.php';
            
           if ($model->sB->status == \app\models\events\EventDeclaration::INCIDENT_REPORT_DRAFT)
            { 
                echo Html::a(Yii::t('app', 'Modify'), ['events/drafts/sections/subsections/b2/update', 'id' => $model->id], ['class' => 'btn btn-primary']);
                echo Html::a(Yii::t('app', 'Finalize {evt}', ['evt'=>$section_name]), ['events/drafts/event-draft/finalize-section', 'id' => $model->sB->eventDraft->id, 's'=>'B'], ['class' => 'btn btn-success pull-right']);
            }
        }
        
      //(start) only display pdf if not stripped = 2
      if (!isset($stripped) || $stripped !== 2) {
      echo Html::a(Yii::t('app', '<i class="fa fa-file-pdf-o"></i> PDF'), 
      ['pdfgenerator/default/index', 'id' => $model->sB->eventDraft->id , 'section' => $section_name.'.'.$subsection_name.'.'], 
      ['class' => 'btn btn-default']); 
      }        
        ?>        
      
    </p>

    <table>
        <tbody>
            <tr class="sy_para">
                <td class="sy_crf_para sy_section_h1">B.2.</td>
                <td class="sy_crf_para sy_section_h1">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <em><?= \Yii::t('app/crf', 'Description of circumstances, consequences of event and emergency response') ?></em>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="sy_crf_para"></td>
                <td class="sy_crf_para">
                    <?= \Yii::t('app/crf', 'Blowout prevention equipment activated') . ': ' ?>
                    <br/>
                    <?php
                    if (isset($model->b2_1))
                    {
                        $s = Html::checkbox('chk_b2_1', $model->b2_1)
                                . '<span style="padding-left:6px;">' . lcfirst(\Yii::t('app', 'Yes')) . '</span><br/>'
                                . Html::checkbox('chk_b2_1', !$model->b2_1)
                                . '<span style="padding-left:6px;">' . lcfirst(\Yii::t('app', 'No')) . '</span><br/>';
                    }
                    else
                    {
                        $s = Html::checkbox('chk_b2_3', false)
                                . '<span style="padding-left:6px;">' . lcfirst(\Yii::t('app', 'Yes')) . '</span><br/>'
                                . Html::checkbox('chk_b2_3', false)
                                . '<span style="padding-left:6px;">' . lcfirst(\Yii::t('app', 'No')) . '</span><br/>';
                    }
                    echo $s;
                    ?>
                </td>
            </tr>
            <tr>
                <td class="sy_crf_para"></td>
                <td class="sy_crf_para">
                    <?= \Yii::t('app/crf', 'Diverter system in operation') . ': '?> 
                    <br/>
                    <?php
                    if (isset($model->b2_2))
                    {
                        $s = Html::checkbox('chk_b2_2', $model->b2_2)
                                . '<span style="padding-left:6px;">' . lcfirst(\Yii::t('app', 'Yes')) . '</span><br/>'
                                . Html::checkbox('chk_b2_2', !$model->b2_2)
                                . '<span style="padding-left:6px;">' . lcfirst(\Yii::t('app', 'No')) . '</span><br/>';
                    }
                    else
                    {
                        $s = Html::checkbox('chk_b2_3', false)
                                . '<span style="padding-left:6px;">' . lcfirst(\Yii::t('app', 'Yes')) . '</span><br/>'
                                . Html::checkbox('chk_b2_3', false)
                                . '<span style="padding-left:6px;">' . lcfirst(\Yii::t('app', 'No')) . '</span><br/>';
                    }
                    echo $s;
                    ?>
                </td>
            </tr>
            <tr>
                <td class="sy_crf_para"></td>
                <td class="sy_crf_para">
                    <?= \Yii::t('app/crf', 'Pressure build-up and/or positive flow check') . ': ' ?>
                    <br/>
                    <?php
                    if (isset($model->b2_3))
                    {
                        $s = Html::checkbox('chk_b2_3', $model->b2_3)
                                . '<span style="padding-left:6px;">' . lcfirst(\Yii::t('app', 'Yes')) . '</span><br/>'
                                . Html::checkbox('chk_b2_3', !$model->b2_3)
                                . '<span style="padding-left:6px;">' . lcfirst(\Yii::t('app', 'No')) . '</span><br/>';
                    }
                    else
                    {
                        $s = Html::checkbox('chk_b2_3', false)
                                . '<span style="padding-left:6px;">' . lcfirst(\Yii::t('app', 'Yes')) . '</span><br/>'
                                . Html::checkbox('chk_b2_3', false)
                                . '<span style="padding-left:6px;">' . lcfirst(\Yii::t('app', 'No')) . '</span><br/>';
                    }
                    echo $s;
                    ?>
                </td>
            </tr>
            <tr>
                <td class="sy_crf_para"></td>
                <td class="sy_crf_para">
                    <?= \Yii::t('app/crf', 'Failing well barriers'); ?>:
                    <br/>
                    <?php
                        $s = Html::activeCheckbox($model, 'b2_4_a', [
                            'label' => '',                                
                        ]). '<span style="padding-left:6px; padding-right:6px;">(a)</span>';
                        
                        if ($model->b2_4_a)
                        {
                            $ss= \app\models\shared\NullValueFormatter::Format($model->b2_4_a_desc) ;
                            $s.= $ss;
                        }
                        
                        echo $s. '<br/>';

                        $s = Html::activeCheckbox($model, 'b2_4_b', [
                            'label' => '',                                
                        ]). '<span style="padding-left:6px; padding-right:6px;">(b)</span>';
                        
                        if ($model->b2_4_b)
                        {
                            $ss= \app\models\shared\NullValueFormatter::Format($model->b2_4_b_desc) ;
                            $s.= $ss;
                        }
                        
                        echo $s. '<br/>';
                        
                        $s = Html::activeCheckbox($model, 'b2_4_c', [
                            'label' => '',                                
                        ]). '<span style="padding-left:6px; padding-right:6px;">(c)</span>';
                        
                        if ($model->b2_4_c)
                        {
                            $ss= \app\models\shared\NullValueFormatter::Format($model->b2_4_c_desc) ;
                            $s.= $ss;
                        }
                        
                        echo $s. '<br/>';
                        ?>
                </td>
            </tr>

            <tr>
                <td class="sy_crf_para"></td>                
                <td class="sy_crf_para" style='width:100%;'>
                    <div class="row">
                        <div class="col-lg-12">
                            <p>
                                <em class ="sy_section_h2">
                                    <?= Yii::t('app/crf', 'Description of circumstances') ?>
                                </em>
                                <br/>
                            </p>
                            <?php
                              $s='';
                              $s .= '<div>'
                                  . nl2br(NullValueFormatter::Format($model->b2_5)) 
                                 . '</div>';

                              echo $s;
                            ?>
                        </div>
                    </div>
                </td>                
            </tr>

            <tr>
                <td class="sy_crf_para"></td>
                <td>
                    <div class="sy_pad_bottom_18" style="padding-right: 6px">
                        <?= Yii::t('app/crf', 'Further details') ?> <i><?= Yii::t('app/crf', '(specify units)') ?></i>
                    </div>
             
                        <ul class="list-inline sy_padding_clear clear-margin-left">
                            <li class="text-info">
                                <?= Yii::t('app/crf','Duration of uncontrolled flow of well-fluids') . ': ' ?> 
                            </li>
                            <li>
                                <?= isset($model->b2_6_a_q) ? ($model->b2_6_a_q) :'<span class="sy_not_set">' . \Yii::t('app', '(not set)') . '</span>' ?>
                                <?= isset($model->b2_6_a_u) ? (' (' . Units::findOne($model->b2_6_a_u)->unit . ')') :'<span class="sy_not_set">' . \Yii::t('app', '(not set)') . '</span>' ?>
                            </li>
                        </ul>                    
                   
                    <br/>
                    
                        <ul class="list-inline sy_padding_clear clear-margin-left">
                            <li class="text-info">
                                <?= Yii::t('app/crf','Flowrate') . ': ' ?> 
                            </li>
                            <li>
                                <?= isset($model->b2_6_b_q) ? ($model->b2_6_b_q) :'<span class="sy_not_set">' . \Yii::t('app', '(not set)') . '</span>' ?>
                                <?= isset($model->b2_6_b_u) ? (' (' . Units::findOne($model->b2_6_b_u)->unit . ')') :'<span class="sy_not_set">' . \Yii::t('app', '(not set)') . '</span>' ?>
                            </li>
                        </ul>                    
                   
                    <br/>
                    
                        <ul class="list-inline sy_padding_clear clear-margin-left">
                            <li class="text-info">
                                <?= Yii::t('app/crf','Liquid volume') . ': ' ?>
                            </li>
                            <li>
                                <?= isset($model->b2_6_c_q) ? ($model->b2_6_c_q) :'<span class="sy_not_set">' . \Yii::t('app', '(not set)') . '</span>' ?>
                                <?= isset($model->b2_6_c_u) ? (' (' . Units::findOne($model->b2_6_c_u)->unit . ')') :'<span class="sy_not_set">' . \Yii::t('app', '(not set)') . '</span>' ?>
                            </li>
                        </ul>                    
                   
                    <br/>
                    
                        <ul class="list-inline sy_padding_clear clear-margin-left">
                            <li class="text-info">
                                <?= Yii::t('app/crf','Gas volume') . ': ' ?> 
                            </li>
                            <li>
                                <?= isset($model->b2_6_d_q) ? ($model->b2_6_d_q) :'<span class="sy_not_set">' . \Yii::t('app', '(not set)') . '</span>' ?>
                                <?= isset($model->b2_6_d_u) ? (' (' . Units::findOne($model->b2_6_d_u)->unit . ')') :'<span class="sy_not_set">' . \Yii::t('app', '(not set)') . '</span>' ?>
                            </li>
                        </ul>                    
                </td>
            </tr>
            <tr>
                <td class="sy_crf_para"></td>                
                <td class="sy_crf_para" style='width:100%;'>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 sy_padding_clear">
                        <p style="padding-top: 24px;">
                            <em class ="sy_section_h2">
                                <?= Yii::t('app/crf', 'Consequences of event and emergency response') ?>
                            </em>
                            <br/>
                        </p>
                        <?php
                          $s='';
                          $s .= '<div>'
                              . nl2br(NullValueFormatter::Format($model->b2_7)) 
                             . '</div>';

                          echo $s;
                        ?>
                    </div>
                </td>                
            </tr>            
        </tbody>
    </table>
</div>



<?php

$JS = <<< EOF

$('input[type=checkbox]').each(function() {
    $ (this).attr('disabled', 'disabled');    
});        
        
EOF;

$this->registerJs($JS, yii\web\View::POS_READY);

?>