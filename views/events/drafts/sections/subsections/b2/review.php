<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

  use yii\helpers\Html;
  use app\models\units\Units;

  /* @var $this yii\web\View */
  /* @var $model app\models\events\drafts\sections\subsections\B2 */

  $strip = isset($stripped);
?>

<div class="b2-view col-lg-12">


    <p>
        <?php
        if (!$strip)
        {
            // use this on any view to include the actionmessage logic and widget 
            //include Yii::getAlias('@app') . '\views\shared_parts\message_snippet.php';
            include Yii::getAlias('@app') . '/views/shared_parts/message_snippet.php';
        }
        ?>        
      
    </p>

    <table>
        <tbody>
            <tr class="sy_para">
                <td class="sy_crf_para"><strong>B.2.</strong></td>
                <td class="sy_crf_para">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <strong><?= \Yii::t('app/crf', 'Description of circumstances, consequences of event and emergency response'); ?></strong>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="sy_crf_para"></td>
                <td class="sy_crf_para">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <?= \Yii::t('app/crf', 'Blowout prevention equipment activated'); ?>:
                    <br/>
                    <?php
                    if (isset($model->b2_1))
                    {
                        $s = Html::checkbox('chk_b2_1', $model->b2_1)
                                . '<span style="padding-left:6px;">' . lcfirst(\Yii::t('app', 'Yes')) . '</span><br/>'
                                . Html::checkbox('chk_b2_1', !$model->b2_1)
                                . '<span style="padding-left:6px;">' . lcfirst(\Yii::t('app', 'No')) . '</span><br/>';
                    }
                    else
                    {
                        $s = Html::checkbox('chk_b2_3', false)
                                . '<span style="padding-left:6px;">' . lcfirst(\Yii::t('app', 'Yes')) . '</span><br/>'
                                . Html::checkbox('chk_b2_3', false)
                                . '<span style="padding-left:6px;">' . lcfirst(\Yii::t('app', 'No')) . '</span><br/>';
                    }
                    echo $s;
                    ?>
                  </div>
                </td>
            </tr>
            <tr>
                <td class="sy_crf_para"></td>
                <td class="sy_crf_para">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <?= \Yii::t('app/crf', 'Diverter system in operation'); ?>:
                    <br/>
                    <?php
                    if (isset($model->b2_2))
                    {
                        $s = Html::checkbox('chk_b2_2', $model->b2_2)
                                . '<span style="padding-left:6px;">' . lcfirst(\Yii::t('app', 'Yes')) . '</span><br/>'
                                . Html::checkbox('chk_b2_2', !$model->b2_2)
                                . '<span style="padding-left:6px;">' . lcfirst(\Yii::t('app', 'No')) . '</span><br/>';
                    }
                    else
                    {
                        $s = Html::checkbox('chk_b2_3', false)
                                . '<span style="padding-left:6px;">' . lcfirst(\Yii::t('app', 'Yes')) . '</span><br/>'
                                . Html::checkbox('chk_b2_3', false)
                                . '<span style="padding-left:6px;">' . lcfirst(\Yii::t('app', 'No')) . '</span><br/>';
                    }
                    echo $s;
                    ?>
                  </div>
                </td>
            </tr>
            <tr>
                <td class="sy_crf_para"></td>
                <td class="sy_crf_para">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <?= \Yii::t('app/crf', 'Pressure build-up and/or positive flow check'); ?>:
                    <br/>
                    <?php
                    if (isset($model->b2_3))
                    {
                        $s = Html::checkbox('chk_b2_3', $model->b2_3)
                                . '<span style="padding-left:6px;">' . lcfirst(\Yii::t('app', 'Yes')) . '</span><br/>'
                                . Html::checkbox('chk_b2_3', !$model->b2_3)
                                . '<span style="padding-left:6px;">' . lcfirst(\Yii::t('app', 'No')) . '</span><br/>';
                    }
                    else
                    {
                        $s = Html::checkbox('chk_b2_3', false)
                                . '<span style="padding-left:6px;">' . lcfirst(\Yii::t('app', 'Yes')) . '</span><br/>'
                                . Html::checkbox('chk_b2_3', false)
                                . '<span style="padding-left:6px;">' . lcfirst(\Yii::t('app', 'No')) . '</span><br/>';
                    }
                    echo $s;
                    ?>
                  </div>
                </td>
            </tr>
            <tr>
                <td class="sy_crf_para"></td>
                <td class="sy_crf_para">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <?= \Yii::t('app/crf', 'Failing well barriers'); ?>:
                    <br/>
                    <?php
                        $s = Html::activeCheckbox($model, 'b2_4_a', [
                            'label' => '',                                
                        ]). '<span style="padding-left:6px; padding-right:6px;">(a)</span>';
                        
                        if ($model->b2_4_a)
                        {
                            $ss= \app\models\shared\NullValueFormatter::Format($model->b2_4_a_desc) ;
                            $s.= $ss;
                        }
                        
                        echo $s. '<br/>';

                        $s = Html::activeCheckbox($model, 'b2_4_b', [
                            'label' => '',                                
                        ]). '<span style="padding-left:6px; padding-right:6px;">(b)</span>';
                        
                        if ($model->b2_4_b)
                        {
                            $ss= \app\models\shared\NullValueFormatter::Format($model->b2_4_b_desc) ;
                            $s.= $ss;
                        }
                        
                        echo $s. '<br/>';
                        
                        $s = Html::activeCheckbox($model, 'b2_4_c', [
                            'label' => '',                                
                        ]). '<span style="padding-left:6px; padding-right:6px;">(c)</span>';
                        
                        if ($model->b2_4_c)
                        {
                            $ss= \app\models\shared\NullValueFormatter::Format($model->b2_4_c_desc) ;
                            $s.= $ss;
                        }
                        
                        echo $s. '<br/>';
                        ?>
                  </div>
                </td>
            </tr>

            <tr class="sy_para">
                <td class="sy_crf_para"></td>
                <td class="sy_crf_para">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <p>
                            <strong><?= Yii::t('app/crf', 'Description of circumstances') ?></strong>
                            <br/>
                        </p>

                        <?php
                          $s='';
                          $s .= '<div>'
                              . nl2br(\app\models\shared\NullValueFormatter::Format($model->b2_5)) 
                             . '</div>';

                          echo $s;
                        ?>

                    </div>
                </td>
            </tr>

            <tr>
                <td class="sy_crf_para"></td>
                <td class="sy_crf_para">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="sy_pad_bottom_18">
                                <?= Yii::t('app/crf', 'Further details') ?> <i><?= Yii::t('app/crf', '(specify units)') ?></i>
                        </div>
             
                      <ul class="list-inline sy_padding_clear clear-margin-left">
                        <li><?= Yii::t('app/crf', 'Duration of uncontrolled flow of well-fluids') ?>: </li>
                        <li>
                            <?= isset($model->b2_6_a_q) ? ($model->b2_6_a_q) :'<span class="sy_not_set">' . \Yii::t('app', '(not set)') . '</span>' ?>
                            <?= isset($model->b2_6_a_u) ? (' (' . Units::findOne($model->b2_6_a_u)->unit . ')') :'<span class="sy_not_set">(unit missing)</span>' ?>
                        </li>
                      </ul>                    
                   
                    <br/>
                    
                      <ul class="list-inline sy_padding_clear clear-margin-left">
                        <li><?= Yii::t('app/crf', 'Flowrate') ?>: </li>
                        <li>
                            <?= isset($model->b2_6_b_q) ? ($model->b2_6_b_q) :'<span class="sy_not_set">' . \Yii::t('app', '(not set)') . '</span>' ?>
                            <?= isset($model->b2_6_b_u) ? (' (' . Units::findOne($model->b2_6_b_u)->unit . ')') :'<span class="sy_not_set">(unit missing)</span>' ?>
                        </li>
                      </ul>                    
                   
                    <br/>
                    
                      <ul class="list-inline sy_padding_clear clear-margin-left">
                        <li><?= Yii::t('app/crf', 'Liquid volume') ?>: </li>
                        <li>
                            <?= isset($model->b2_6_c_q) ? ($model->b2_6_c_q) :'<span class="sy_not_set">' . \Yii::t('app', '(not set)') . '</span>' ?>
                            <?= isset($model->b2_6_c_u) ? (' (' . Units::findOne($model->b2_6_c_u)->unit . ')') :'<span class="sy_not_set">(unit missing)</span>' ?>
                        </li>
                      </ul>                    
                   
                    <br/>
                    
                      <ul class="list-inline sy_padding_clear clear-margin-left">
                        <li><?= Yii::t('app/crf', 'Gas volume') ?>: </li>
                        <li>
                            <?= isset($model->b2_6_d_q) ? ($model->b2_6_d_q) :'<span class="sy_not_set">' . \Yii::t('app', '(not set)') . '</span>' ?>
                            <?= isset($model->b2_6_d_u) ? (' (' . Units::findOne($model->b2_6_d_u)->unit . ')') :'<span class="sy_not_set">(unit missing)</span>' ?>
                        </li>
                      </ul> 
                    </div>
                </td>
            </tr>
            <tr>
                <td class="sy_crf_para"></td>                
                <td class="sy_crf_para">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <p>
                            <strong><?= Yii::t('app/crf', 'Consequences of event and emergency response') ?></strong>
                            <br/>
                        </p>
                        <?php
                          $s='';
                          $s .= '<div>'
                              . nl2br(\app\models\shared\NullValueFormatter::Format($model->b2_7)) 
                             . '</div>';

                          echo $s;
                        ?>
                    </div>
                </td>                
            </tr>            
        </tbody>
    </table>
</div>

<?php

$JS = <<< EOF

$('input[type=checkbox]').each(function() {
    $ (this).attr('disabled', 'disabled');    
});        
        
EOF;

$this->registerJs($JS, yii\web\View::POS_READY);

?>