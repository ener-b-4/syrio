<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\events\drafts\sections\subsections\B2Search */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'B2s');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="b2-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create {evt}', [
    'modelClass' => 'B2',
]), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'b2_1',
            'b2_2',
            'b2_3',
            'b2_4_a',
            // 'b2_4_a_desc',
            // 'b2_4_b',
            // 'b2_4_b_desc',
            // 'b2_4_c',
            // 'b2_4_c_desc',
            // 'b2_5:ntext',
            // 'b2_6_a_q',
            // 'b2_6_a_u',
            // 'b2_6_b_q',
            // 'b2_6_b_u',
            // 'b2_6_c_q',
            // 'b2_6_c_u',
            // 'b2_6_d_q',
            // 'b2_6_d_u',
            // 'b2_7:ntext',
            // 'sb_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
