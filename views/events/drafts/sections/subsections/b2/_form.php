<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\events\drafts\sections\subsections\B2 */
/* @var $form yii\widgets\ActiveForm */

$timeUnits=ArrayHelper::map(\app\models\units\Units::find()->where(['scope'=>2])->asArray()->all(), 'id', 'unit');
$flowUnits=ArrayHelper::map(\app\models\units\Units::find()->where(['scope'=>3])->asArray()->all(), 'id', 'unit');
$volumeUnits=ArrayHelper::map(\app\models\units\Units::find()->where(['scope'=>8])->asArray()->all(), 'id', 'unit');

?>

<div class="b2-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <table>
        <tbody>
            <tr class="sy_para">
                <td class="sy_crf_para sy_section_h1">B.2.</td>
                <td class="sy_crf_para sy_section_h1">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <em><?= \Yii::t('app/crf', 'Description of circumstances, consequences of event and emergency response') ?></em>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="sy_crf_para"></td>
                <td class="sy_crf_para">
                    <?= \Yii::t('app/crf', 'Blowout prevention equipment activated') . ': ' ?>
                    
                    <ul class="list-unstyled">
                        <li class="sy_vert_top"><?= Html::activeRadio($model, 'b2_1', ['id'=>'b2_1_y', 'label'=>lcfirst(\Yii::t('app', 'Yes')), 'value'=>1, 'uncheck'=>null]) ?>
                        <li class="sy_vert_top"><?= Html::activeRadio($model, 'b2_1', ['id'=>'b2_1_n', 'label'=>lcfirst(\Yii::t('app', 'No')), 'value'=>0, 'uncheck'=>null]) ?>
                    </ul>
                </td>
            </tr>
            <tr>
                <td class="sy_crf_para"></td>
                <td class="sy_crf_para">
                    <?= \Yii::t('app/crf', 'Diverter system in operation') . ': '?> 
                    
                    <ul class="list-unstyled">
                        <li class="sy_vert_top"><?= Html::activeRadio($model, 'b2_2', ['id'=>'b2_2_y', 'label'=>lcfirst(\Yii::t('app', 'Yes')), 'value'=>1, 'uncheck'=>null]) ?>
                        <li class="sy_vert_top"><?= Html::activeRadio($model, 'b2_2', ['id'=>'b2_2_n', 'label'=>lcfirst(\Yii::t('app', 'No')), 'value'=>0, 'uncheck'=>null]) ?>
                    </ul>
                </td>
            </tr>
            <tr>
                <td class="sy_crf_para"></td>
                <td class="sy_crf_para">
                    <?= \Yii::t('app/crf', 'Pressure build-up and/or positive flow check') . ': ' ?>
                    
                    <ul class="list-unstyled">
                        <li class="sy_vert_top">
                            <?= Html::activeRadio($model, 'b2_3', ['id'=>'b2_3_y', 'label'=>lcfirst(\Yii::t('app', 'Yes')), 'value'=>1, 'uncheck'=>null]) ?>
                        </li>
                        <li class="sy_vert_top">
                            <?= Html::activeRadio($model, 'b2_3', ['id'=>'b2_3_n', 'label'=>lcfirst(\Yii::t('app', 'No')), 'value'=>0, 'uncheck'=>null]) ?>
                        </li>
                    </ul>
                </td>
            </tr>
            <tr>
                <td class="sy_crf_para"></td>
                <td class="sy_crf_para">
                    <?= \Yii::t('app/crf', 'Failing well barriers'); ?>:
                    <br/>
                    
                    <ul class="list-unstyled" style="margin-left:6px; margin-top: 6px;">
                        <li class="sy_vert_top">
                            <ul class="list list-inline">
                                <li class="sy_vert_top sy_pad_top_6">
                                    <?= $form->field($model, 'b2_4_a')->checkbox([
                                        'class' => 'sy_data_toggle',
                                        'sy_data_toggle_dest'=>'b2_4_a_y_toggle',
                                        'sy_data_toggle_src_type'=>'checkbox',
                                        'sy_data_toggle_enabled' => 'true',
                                        'sy_data_toggle_visible_value' => 'true',
                                        
                                        ], false)->label('(a)') ?>
                                </li>
                                <li class="sy_vert_top">
                                    <span id="b2_4_a_y_toggle">
                                        <?= $form->field($model, 'b2_4_a_desc')->textInput()->label('', ['class'=>'hiddenBlock']) ?>
                                    </span>
                                </li>
                            </ul>
                            <br/>
                            <ul class="list list-inline">
                                <li class="sy_vert_top sy_pad_top_6">
                                    <?= $form->field($model, 'b2_4_b')->checkbox([
                                        'class' => 'sy_data_toggle',
                                        'sy_data_toggle_dest'=>'b2_4_b_y_toggle',
                                        'sy_data_toggle_src_type'=>'checkbox',
                                        'sy_data_toggle_enabled' => 'true',
                                        'sy_data_toggle_visible_value' => 'true',
                                        
                                        ], false)->label('(b)') ?>
                                </li>
                                <li class="sy_vert_top">
                                    <span id="b2_4_b_y_toggle">
                                        <?= $form->field($model, 'b2_4_b_desc')->textInput()->label('', ['class'=>'hiddenBlock']) ?>
                                    </span>
                                </li>
                            </ul>
                            <br/>
                            <ul class="list list-inline">
                                <li class="sy_vert_top sy_pad_top_6">
                                    <?= $form->field($model, 'b2_4_c')->checkbox([
                                        'class' => 'sy_data_toggle',
                                        'sy_data_toggle_dest'=>'b2_4_c_y_toggle',
                                        'sy_data_toggle_src_type'=>'checkbox',
                                        'sy_data_toggle_enabled' => 'true',
                                        'sy_data_toggle_visible_value' => 'true',
                                        ], false)->label('(c)') ?>
                                    
                                </li>
                                <li class="sy_vert_top">
                                    <span id="b2_4_c_y_toggle">
                                        <?= $form->field($model, 'b2_4_c_desc')->textInput()->label('', ['class'=>'hiddenBlock']) ?>
                                    </span>
                                </li>
                            </ul>
                            
                        </li>
                    </ul>
                </td>
            </tr>

            <tr>
                <td class="sy_crf_para"></td>                
                <td class="sy_crf_para" style='width:100%;'>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 sy_padding_clear">
                        <p>
                            <em class ="sy_section_h2">
                                <?= Yii::t('app/crf', 'Description of circumstances') ?>
                            </em>
                            <br/>
                        </p>
                        <?= $form->field($model, 'b2_5')->textarea(['class'=>'sy_fill_textarea form-control'])->label('', ['class'=>'hiddenBlock']) ?>                      
                    </div>
                </td>                
            </tr>

            <tr>
                <td class="sy_crf_para"></td>
                <td>
                    <div class="sy_pad_bottom_18" style="padding-right: 6px">
                        <strong>
                            <?= Yii::t('app/crf', 'Further details') ?>                            
                        </strong>
                        <i><?= Yii::t('app/crf', '(specify units)') ?></i>
                    </div>
             
                        <ul class="list-inline clear-margin-left">
                            <li class="sy_vert_top sy_pad_top_6">
                                <?= Yii::t('app/crf','Duration of uncontrolled flow of well-fluids') . ': ' ?> 
                            </li>
                            <li class="sy_vert_top">
                                <?= $form->field($model, 'b2_6_a_q')->textInput()->label('', ['class'=>'hiddenBlock']) ?>
                            </li>
                            <li class="sy_vert_top">
                                <?= $form->field($model,'b2_6_a_u')->dropDownList($timeUnits, ['id'=>'unit'])->label('', ['class'=>'hiddenBlock']); ?>
                            </li>
                        </ul>                    
                   
                    <br/>
                    
                        <ul class="list-inline clear-margin-left">
                            <li class="sy_vert_top sy_pad_top_6">
                                <?= Yii::t('app/crf','Flowrate') . ': ' ?> 
                            </li>
                            <li class="sy_vert_top">
                                <?= $form->field($model, 'b2_6_b_q')->textInput()->label('', ['class'=>'hiddenBlock']) ?>
                            </li>
                            <li class="sy_vert_top">
                                <?= $form->field($model,'b2_6_b_u')->dropDownList($flowUnits, ['id'=>'unit'])->label('', ['class'=>'hiddenBlock']); ?>
                            </li>
                        </ul>                    
                   
                    <br/>
                    
                        <ul class="list-inline clear-margin-left">
                            <li class="sy_vert_top sy_pad_top_6">
                                <?= Yii::t('app/crf','Liquid volume') . ': ' ?>
                            </li>
                            <li class="sy_vert_top">
                                <?= $form->field($model, 'b2_6_c_q')->textInput()->label('', ['class'=>'hiddenBlock']) ?>
                            </li>
                            <li class="sy_vert_top">
                                <?= $form->field($model,'b2_6_c_u')->dropDownList($volumeUnits, ['id'=>'unit'])->label('', ['class'=>'hiddenBlock']); ?>
                            </li>
                        </ul>                    
                   
                    <br/>
                    
                        <ul class="list-inline clear-margin-left">
                            <li class="sy_vert_top sy_pad_top_6">
                                <?= Yii::t('app/crf','Gas volume') . ': ' ?> 
                            </li>
                            <li class="sy_vert_top">
                                <?= $form->field($model, 'b2_6_d_q')->textInput()->label('', ['class'=>'hiddenBlock']) ?>
                            </li>
                            <li class="sy_vert_top">
                                <?= $form->field($model,'b2_6_d_u')->dropDownList($volumeUnits, ['id'=>'unit'])->label('', ['class'=>'hiddenBlock']); ?>
                            </li>
                        </ul>                    
                    
                    
                </td>
            </tr>
            <tr>
                <td class="sy_crf_para"></td>                
                <td class="sy_crf_para" style='width:100%;'>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 sy_padding_clear">
                        <p style="padding-top: 24px;">
                            <em class ="sy_section_h2">
                                <?= Yii::t('app/crf', 'Consequences of event and emergency response') ?>
                            </em>
                            <br/>
                        </p>
                        <?= $form->field($model, 'b2_7')->textarea(['class'=>'sy_fill_textarea form-control'])->label('') ?>                      
                    </div>
                </td>                
            </tr>            
        </tbody>
    </table>    
    

    <?php ActiveForm::end(); ?>

</div>
