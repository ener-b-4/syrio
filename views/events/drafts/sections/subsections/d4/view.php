<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

  use yii\helpers\Html;
  use app\models\shared\NullValueFormatter;
  use app\models\crf\CrfHelper;

  /* @var $this yii\web\View */
  /* @var $model app\models\events\drafts\sections\subsections\D4 */
      //(start) Bug 7 - Finalisation of "Generate PDF" 
      $section_name = 'D';
      $subsection_name = '4';
      $title_subsection = $section_name . '.' . $subsection_name . '.';           //'B.4.'
      //(end) Bug 7 - Finalisation of "Generate PDF" 

  $strip = isset($stripped);
  if (!$strip)
  {
    
    
    $this->title = Yii::t('app/crf', 'Section {id}', ['id'=>$title_subsection]);  

    echo $this->render('/shared_parts/_breadcrumbs', [
      'title_subsection' => $title_subsection, 
      'module_name' => 'ADD_SUBSECTION'
    ]);
  }
?>
<div class="d4-view col-lg-12">

    <p>
        <?php
        if (!$strip)
        {
          // use this on any view to include the actionmessage logic and widget 
          //include Yii::getAlias('@app') . '\views\shared_parts\message_snippet.php';
          include Yii::getAlias('@app') . '/views/shared_parts/message_snippet.php';
            
           if ($model->sD->status == \app\models\events\EventDeclaration::INCIDENT_REPORT_DRAFT)
            { 
                echo Html::a(Yii::t('app', 'Modify'), ['events/drafts/sections/subsections/d4/update', 'id' => $model->id], ['class' => 'btn btn-primary']);
                echo Html::a(Yii::t('app', 'Finalize {evt}', ['evt'=>$section_name]), ['events/drafts/event-draft/finalize-section', 'id' => $model->sD->eventDraft->id, 's'=>'D'], ['class' => 'btn btn-success pull-right']);
            }
        }
        
      //(start) only display pdf if not stripped = 2
      if (!isset($stripped) || $stripped !== 2) {
      echo Html::a(Yii::t('app', '<i class="fa fa-file-pdf-o"></i> PDF'), 
          ['pdfgenerator/default/index', 'id' => $model->sD->eventDraft->id , 'section' => $section_name.'.'.$subsection_name.'.'], 
      ['class' => 'btn btn-default']); 
      }        
        
        ?>               
        
    </p>

    <?php 
        $section_title = strtoupper($section_name . '.' . $subsection_name);
        $attr = strtolower($section_name).$subsection_name.'_1';
    ?>
    <table>
        <tbody>
            <tr class="sy_para">
                <td class="sy_crf_para sy_section_h1"><?= $title_subsection ?></td>
                <td class="sy_crf_para" style='width:100%;'>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <em class="sy_section_h1"><strong><?= CrfHelper::SectionsTitle()[$section_title] ?></strong><br/></em>
                        <span class='text-danger sy_narative-text'><?= Yii::t('app/crf', '(within 10 days of the event)') ?> </span>

                        <br/>
                        <br/>
                        <?php
                          $s='';
                          $s .= '<div>'
                              . nl2br(NullValueFormatter::Format($model->$attr)) 
                             . '</div>';

                          echo $s;
                        ?>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    
</div>
