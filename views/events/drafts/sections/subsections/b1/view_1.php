<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\events\drafts\sections\subsections\B1 */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'B1s'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="b1-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Modify'), ['events/drafts/sections/subsections/b1/update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'b1_1',
            'b1_2',
            'b1_3',
            'b1_4_s',
            'b1_4_e',
            'b1_5',
            'b1_6',
            'b1_7',
            'b1_8_1_q',
            'b1_8_1_u',
            'b1_8_2_q',
            'b1_8_2_u',
            'b1_8_3_q',
            'b1_8_3_u',
            'b1_9',
            'b1_9_details',
            'sb_id',
        ],
    ]) ?>

</div>
