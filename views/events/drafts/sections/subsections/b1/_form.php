<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

  use yii\helpers\Html;
  use yii\widgets\ActiveForm;
  use yii\helpers\ArrayHelper;
  use app\components\helpers\TArrayHelper;
  use kartik\widgets\DateTimePicker;

  /* @var $this yii\web\View */
  /* @var $model app\models\events\drafts\sections\subsections\B1 */
  /* @var $form yii\widgets\ActiveForm */

  $liquidTypes=TArrayHelper::tMap(\app\models\shared\Constants::find()->where(['type'=>10])->asArray()->all(), 'num_code', 'name', 'app/data');
  $wellheadTypes=TArrayHelper::tMap(\app\models\shared\Constants::find()->where(['type'=>11])->asArray()->all(), 'num_code', 'name', 'app/data');
  $activityTypes = TArrayHelper::tMap(\app\models\shared\Constants::find()->where(['type'=>8])->asArray()->all(), 'num_code', 'name', 'app/data', null, [
      'Other' => \Yii::t('app/crf', 'Other')
  ]);
  $servicesTypes = TArrayHelper::tMap(\app\models\shared\Constants::find()->where(['type'=>9])->asArray()->all(), 'num_code', 'name', 'app/data');
  $pressureUnits=ArrayHelper::map(\app\models\units\Units::find()->where(['scope'=>5])->asArray()->all(), 'id', 'unit');
  $tempUnits=ArrayHelper::map(\app\models\units\Units::find()->where(['scope'=>7])->asArray()->all(), 'id', 'unit');
  $distUnits=ArrayHelper::map(\app\models\units\Units::find()->where(['scope'=>6])->asArray()->all(), 'id', 'unit');

?>

<div class="b1-form">

    <?php $form = ActiveForm::begin(); ?>

    <table>
        <tbody>
            <tr class="sy_para">
                <td class="sy_crf_para sy_section_h1">B.1.</td>
                <td class="sy_crf_para sy_section_h1">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <strong><?= \Yii::t('app/crf', 'General information'); ?></strong>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="sy_crf_para"></td>
                <td class="sy_crf_para">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <ul class="list-inline sy_padding_clear">
                            <li class="text-info sy_vert_top sy_pad_top_6">(a) <?= ' ' . \Yii::t('app/crf', 'Name/code of well') . ': ' ?></li>
                            <li><?= $form->field($model, 'b1_1')->textInput()->label('',['class'=>'hiddenBlock']) ?></li>
                        </ul>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <ul class="list-inline sy_padding_clear">
                            <li class="text-info sy_vert_top sy_pad_top_6">
                                <div class="form-group">(b) <?= ' ' . \Yii::t('app/crf', 'Name of drilling contractor (if relevant)') . ': ' ?></div>
                                 
                            </li>
                            <li><?= $form->field($model, 'b1_2')->textInput()->label('',['class'=>'hiddenBlock']) ?></li>
                        </ul>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <ul class="list-inline sy_padding_clear">
                            <li class="text-info sy_vert_top sy_pad_top_6">(c) <?= ' ' . \Yii::t('app/crf', 'Name/type of drilling rig (if relevant)') . ': ' ?></li>
                            <li><?= $form->field($model, 'b1_3')->textInput()->label('',['class'=>'hiddenBlock']) ?></li>
                        </ul>
                    </div> 
                    
                    
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                       <ul class="list-unstyled sy_padding_clear" style='margin-left: -4px'>
                           <li class="text-info  sy_vert_top sy_pad_top_6">(d) <?= ' ' . \Yii::t('app/crf', 'Start and end date/time of loss of well control') . ': ' ?></li>
                           <li class=" sy_vert_top  sy_pad_top_6">
                                       
                                   <div style='padding-left: 22px;'>
                                       <small><a href="#" id="getEventDate"> > <?= \Yii::t('app', 'use Event date time as start date') ?> </a></small>
                                        <?= Html::hiddenInput('event_date_time', $model->sB->eventDraft->event->event_date_time, [
                                            'id'=>'event_date_time',
                                        ]) ?>
                                   </div>
                                   <div style='padding-left: 22px;'>
                                    <?php
                                        echo $form->field($model, 'b1_4_s')->widget(DateTimePicker::classname(), [
                                                'options' => [
                                                    'placeholder' => 'Enter event start date and time ...',
                                                    'style' => 'max-width:250px',
                                                    'convertFormat' => true,
                                                    ],
                                                'pluginOptions' => [
                                                    'format' => 'yyyy-mm-dd hh:ii:ss',
                                                    'todayHighlight' => false,
                                                    'autoclose' => true,
                                                    'endDate' => \date("Y-m-d H:i:s"),
                                                ]
                                        ]);            



                                        echo $form->field($model, 'b1_4_e')->widget(DateTimePicker::classname(), [
                                                'options' => [
                                                    'convertFormat' => true,
                                                    'placeholder' => 'Enter event end date and time ...',
                                                    'style' => 'max-width:250px;'
                                                    ],
                                                'pluginOptions' => [
                                                    'format' => 'yyyy-mm-dd hh:ii:ss',
                                                    'todayHighlight' => false,
                                                    'autoclose' => true,
                                                    'endDate' => \date("Y-m-d H:i:s"),
                                                ]
                                        ]);

                                    ?>


                                    </div>
                           </li>
                       </ul>
                   </div>
                   
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <ul class="list-inline sy_padding_clear">
                            <li class="text-info sy_vert_top sy_pad_top_6">(e) <?= ' ' . \Yii::t('app/crf', 'Type of fluid: brine / oil / gas / ... (if relevant)') . ': ' ?></li>
                            <li><?= $form->field($model, 'b1_5')->dropDownList($liquidTypes,[
                                'prompt' => \Yii::t('app', '(not set)'),
                            ])->label('',['class'=>'hiddenBlock']) ?></li>
                        </ul>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <ul class="list-inline sy_padding_clear">
                            <li class="text-info sy_vert_top sy_pad_top_6">(f) <?= ' ' . \Yii::t('app/crf', 'Well head completion') . ': ' ?></li>
                            <li><?= $form->field($model, 'b1_6')->dropDownList($wellheadTypes,[
                                'prompt' => \Yii::t('app', '(not set)'),
                            ])->label('',['class'=>'hiddenBlock']) ?></li>
                        </ul>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <ul class="list-inline sy_padding_clear">
                            <li class="text-info sy_vert_top sy_pad_top_6">(g) <?= ' ' . \Yii::t('app/crf', 'Water depth') . ': ' ?></li>
                            <li><?= $form->field($model, 'b1_7')->textInput()->label('',['class'=>'hiddenBlock']) ?></li>
                        </ul>
                    </div> 
                    
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <ul class="list-inline sy_padding_clear">
                            <li class="text-info sy_vert_top sy_pad_top_6">(h) <?= ' ' . \Yii::t('app/crf', 'Reservoir: pressure / temperature / depth') . ': ' ?></li>
                        </ul>
                        <br/>
                        <ul class="list-inline sy_padding_clear sy_pad_top_6" style="padding-left: 23px">
                            <li class="sy_vert_top sy_pad_top_6">
                                <?= \Yii::t('app', 'Pressure') ?> 
                            </li>
                            <li class="sy_vert_top">
                                <?= $form->field($model, 'b1_8_1_q')->textInput()->label('',['class'=>'hiddenBlock']) ?>
                            </li>
                            <li class="sy_vert_top">
                                <?= $form->field($model, 'b1_8_1_u')->dropDownList($pressureUnits)->label('',['class'=>'hiddenBlock']) ?>
                            </li>
                        </ul>
                        <br/>
                        <ul class="list-inline sy_padding_clear" style="padding-left: 23px">
                            <li class="sy_vert_top sy_pad_top_6">
                                <?= \Yii::t('app', 'Temperature') ?> 
                            </li>
                            <li class="sy_vert_top">
                                <?= $form->field($model, 'b1_8_2_q')->textInput()->label('',['class'=>'hiddenBlock']) ?>
                            </li>
                            <li class="sy_vert_top">
                                <?= $form->field($model, 'b1_8_2_u')->dropDownList($tempUnits)->label('',['class'=>'hiddenBlock']) ?>
                            </li>
                        </ul>
                        <br/>
                        <ul class="list-inline sy_padding_clear" style="padding-left: 23px">
                            <li class="sy_vert_top sy_pad_top_6">
                                <?= \Yii::t('app', 'Depth') ?> 
                            </li>
                            <li class="sy_vert_top">
                                <?= $form->field($model, 'b1_8_3_q')->textInput()->label('',['class'=>'hiddenBlock']) ?>
                            </li>
                            <li class="sy_vert_top">
                                <?= $form->field($model, 'b1_8_3_u')->dropDownList($distUnits)->label('',['class'=>'hiddenBlock']) ?>
                            </li>
                        </ul>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <ul class="list-inline sy_padding_clear">
                            <li class="text-info sy_vert_top sy_pad_top_6">(i) <?= ' ' . \Yii::t('app/crf', 'Type of activity') . ': ' ?></li>
                            <li><?= $form->field($model, 'b1_9')->dropDownList($activityTypes, [
                                'prompt' => Yii::t('app', '(none)'),
                                'class' => 'form-control sy_data_toggle',
                                'sy_data_toggle_dest' => 'b1j_toggle',
                                'sy_data_toggle_src_type' => 'dropdown',
                                'sy_data_toggle_enabled' => 'true',
                                'sy_data_toggle_visible_value' => 704,
                                ])->label('',['class'=>'hiddenBlock']) ?>
                            </li>
                        </ul>
                    </div>
                    <div id = 'b1j_toggle' class="col-lg-12 col-md-12 col-sm-12 col-xs-12 hiddenBlock">
                        <ul class="list-inline sy_padding_clear">
                            <li class="text-info sy_vert_top sy_pad_top_6">(j) <?= ' ' . \Yii::t('app/crf', 'Type of well services (if applicable): wire line / coiled tubing / snubbing') . ': ' ?></li>
                            <li><?= $form->field($model, 'b1_10')->dropDownList($servicesTypes, [
                                'prompt' => Yii::t('app', '(none)'),
                            ])->label('',['class'=>'hiddenBlock']) ?>
                            </li>
                        </ul>
                    </div>
                  
                </td>
            </tr>
        </tbody>
    </table>
    

    <?php ActiveForm::end(); ?>

</div>

<?php
$js = <<< 'SCRIPT'
   //update the start date as event date
   $('#getEventDate').click(function($evt){
        $('#b1-b1_4_s').val($('#event_date_time').val());

        return false;
   });

SCRIPT;

$this->registerJs($js);

?>

<?php
$js = <<< 'SCRIPT'
/* To initialize BS3 tooltips set this below */
$(" :input").each(function(index) {
    $( this ).css("display", "inline-block");
}
);

SCRIPT;
// Register tooltip/popover initialization javascript
$this->registerJs($js);
?>