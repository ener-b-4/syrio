<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\events\drafts\sections\subsections\B1 */

$this->title = Yii::t('app', 'Create {evt}', [
    'modelClass' => 'B1',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'B1s'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="b1-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
