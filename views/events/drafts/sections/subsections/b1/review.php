<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

  use yii\helpers\Html;
  use app\models\shared\Constants;
  use app\models\units\Units;


  /* @var $this yii\web\View */
  /* @var $model app\models\events\drafts\sections\subsections\B1 */
  
    $strip = isset($stripped);
    if (!$strip) { 
    //(start) Bug 7 - Finalisation of "Generate PDF" 
    $section_name = 'B';
    $this->title = \Yii::t('app', 'SECTION {evt}', ['evt'=>$section_name]);
    //(end) Bug 7 - Finalisation of "Generate PDF" 
  }
  
?>
<div class="b1-view col-lg-12">

  <?php if (!isset($from_ca) || $from_ca != 1) {?>
  
  <!--(start) Bug 7 - Finalisation of "Generate PDF" -->
  <h3><strong><?= strtoupper($this->title) ?></strong></h3>
  
  <?php echo Html::a(Yii::t('app', '<i class="fa fa-file-pdf-o"></i> PDF'), 
    ['pdfgenerator/default/index', 'id' => $event_id , 'section' => $section_name], 
    ['class' => 'btn btn-default']); 
  ?>
  <!--(end) Bug 7 - Finalisation of "Generate PDF" -->
    
  <?php
    if ($model->sB->status < \app\models\events\EventDeclaration::INCIDENT_REPORT_FINALIZED && !isset($model->sB->locked_by)) {
      echo Html::a(Yii::t('app', 'Finalize {evt}', ['evt'=>$section_name]), ['events/drafts/event-draft/finalize-section', 'id' => $model->sB->eventDraft->id, 's'=>'B'], ['class' => 'btn btn-success pull-right']); 
    }
  ?>
  
  <p>
    <?php
    if (!$strip)
    {
      // use this on any view to include the actionmessage logic and widget 
      //include Yii::getAlias('@app') . '\views\shared_parts\message_snippet.php';
      include Yii::getAlias('@app') . '/views/shared_parts/message_snippet.php';
    }
    ?>
         
    </p>

    
  <?php } //end ca_from ?>
    
    <table>
        <tbody>
            <tr class="sy_para">
                <td class="sy_crf_para"><strong>B.1.</strong></td>
                <td class="sy_crf_para">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <strong><?= \Yii::t('app/crf', 'General information'); ?></strong>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="sy_crf_para"></td>
                <td class="sy_crf_para">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <ul class="list-inline sy_padding_clear">
                            <li>(a) <?= ' ' . \Yii::t('app/crf', 'Name/code of well') . ': ' ?></li>
                            <li> <?= \app\models\shared\NullValueFormatter::Format($model->b1_1) ?> </li>
                        </ul>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <ul class="list-inline sy_padding_clear">
                            <li>(b) <?= ' ' . \Yii::t('app/crf', 'Name of drilling contractor') . ': ' ?></li>
                            <li> <?= \app\models\shared\NullValueFormatter::Format($model->b1_2) ?> </li>
                        </ul>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <ul class="list-inline sy_padding_clear">
                            <li>(c) <?= ' ' . \Yii::t('app/crf', 'Name/type of drilling rig (if relevant)') . ': ' ?></li>
                            <li> <?= \app\models\shared\NullValueFormatter::Format($model->b1_3) ?> </li>
                        </ul>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <ul class="list-inline sy_padding_clear">
                            <li>(d) <?= ' ' . \Yii::t('app/crf', 'Start and end date/time of loss of well control') . ': ' ?></li>
                            <li> <?= \app\models\shared\NullValueFormatter::Format($model->b1_4_s) ?> </li>
                            <li><span class="glyphicon glyphicon-arrow-right"></span></li>
                            <li> <?= \app\models\shared\NullValueFormatter::Format($model->b1_4_e) ?> </li>
                        </ul>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <ul class="list-inline sy_padding_clear">
                            <li>(e) <?= ' ' . \Yii::t('app/cpf', 'Type of fluid') ?></li>
                            <li> <?= ' ' . \Yii::t('app/crf', '(if relevant)') . ': ' ?></li>
                            <li><?= isset($model->b1_5) ? \Yii::t('app/data', Constants::findOne(['num_code'=>$model->b1_5])->name) :
                            \app\models\shared\NullValueFormatter::Format($model->b1_5) ?></li>
                        </ul>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <ul class="list-inline sy_padding_clear">
                            <li>(f) <?= ' ' . \Yii::t('app/crf', 'Well head completion') . ': ' ?></li>
                            <li><?= isset($model->b1_6) ? \Yii::t('app/data', Constants::findOne(['num_code'=>$model->b1_6])->name) :
                            \app\models\shared\NullValueFormatter::Format($model->b1_6) ?></li>
                        </ul>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <ul class="list-inline sy_padding_clear">
                            <li>(g) <?= ' ' . \Yii::t('app/crf', 'Water depth') . ': ' ?></li>
                            <li><?= isset($model->b1_7) ? ($model->b1_7 . ' (m)') :
                            \app\models\shared\NullValueFormatter::Format($model->b1_7) ?></li>
                        </ul>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <ul class="list-inline sy_padding_clear">
                            <li>(h) <?= ' ' . \Yii::t('app/crf', 'Reservoir: pressure / temperature / depth') . ': ' ?></li>
                            <li>
                                <?php
                                    if (isset($model->b1_8_1_q)) {
                                        echo $model->b1_8_1_q . ' ';
                                        echo isset($model->b1_8_1_u) ? (' (' . Units::findOne($model->b1_8_1_u)->unit . ')') : \app\models\shared\NullValueFormatter::Format($model->b1_8_1_u);
                                    }
                                    else
                                    {
                                        echo \app\models\shared\NullValueFormatter::Format($model->b1_8_1_q);
                                    }
                                ?>
                            </li>
                            <li> / </li>
                            <li>
                                <?php
                                    if (isset($model->b1_8_2_q)) {
                                        echo $model->b1_8_2_q . ' ';
                                        echo isset($model->b1_8_2_u) ? (' (' . Units::findOne($model->b1_8_2_u)->unit . ')') : \app\models\shared\NullValueFormatter::Format($model->b1_8_2_u);
                                    }
                                    else
                                    {
                                        echo \app\models\shared\NullValueFormatter::Format($model->b1_8_2_q);
                                    }
                                ?>
                            </li>
                            <li> / </li>
                            <li>
                                <?php
                                    if (isset($model->b1_8_3_q)) {
                                        echo $model->b1_8_3_q . ' ';
                                        echo isset($model->b1_8_3_u) ? (' (' . Units::findOne($model->b1_8_3_u)->unit . ')') : \app\models\shared\NullValueFormatter::Format($model->b1_8_3_u);
                                    }
                                    else
                                    {
                                        echo \app\models\shared\NullValueFormatter::Format($model->b1_8_3_q);
                                    }
                                ?>
                            </li>
                        </ul>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <ul class="list-inline sy_padding_clear">
                            <li>(i) <?= ' ' . \Yii::t('app/crf', 'Type of activity') . ': ' ?></li>
                            <li><?= isset($model->b1_9) ? \Yii::t('app/data', Constants::findOne(['num_code'=>$model->b1_9])->name) :'<span class="sy_not_set">' . \Yii::t('app', '(not set)') . '</span>' ?></li>
                        </ul>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <ul class="list-inline sy_padding_clear">
                            <li>(j) <?= ' ' . \Yii::t('app', 'Type of well services') . ': ' ?></li>
                            <li>
                                <?php
                                if (isset($model->b1_9))
                                {
                                    if ($model->b1_9 == 704)
                                    {
                                        if (isset($model->b1_10)) 
                                        { 
                                            echo \Yii::t('app/data', Constants::findOne(['num_code'=>$model->b1_10])->name);
                                        }
                                        else
                                        {
                                            echo '<span class="sy_not_set">' . \Yii::t('app', '(not set)') . '</span>';                                                
                                        }
                                    }
                                    else
                                    {
                                        echo '<span>NA</span>';
                                    }
                                }
                                else
                                {
                                    echo '<span>-</span>';
                                }
                                ?>
                            </li>
                        </ul>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>

</div>
