<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\events\drafts\sections\subsections\B1 */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="b1-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'b1_1')->textInput(['maxlength' => 64]) ?>

    <?= $form->field($model, 'b1_2')->textInput() ?>

    <?= $form->field($model, 'b1_3')->textInput() ?>

    <?= $form->field($model, 'b1_4_s')->textInput() ?>

    <?= $form->field($model, 'b1_4_e')->textInput() ?>

    <?= $form->field($model, 'b1_5')->textInput() ?>

    <?= $form->field($model, 'b1_6')->textInput() ?>

    <?= $form->field($model, 'b1_7')->textInput() ?>

    <?= $form->field($model, 'b1_8_1_q')->textInput() ?>

    <?= $form->field($model, 'b1_8_1_u')->textInput() ?>

    <?= $form->field($model, 'b1_8_2_q')->textInput() ?>

    <?= $form->field($model, 'b1_8_2_u')->textInput() ?>

    <?= $form->field($model, 'b1_8_3_q')->textInput() ?>

    <?= $form->field($model, 'b1_8_3_u')->textInput() ?>

    <?= $form->field($model, 'b1_9')->textInput() ?>

    <?= $form->field($model, 'sb_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), [
            'id'=>'btnSubmit',
            'onclick' => 'proceed = 1;',
            'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app/commands', 'Cancel'), 
                ['cancel', 'id' => $model->id, 'ref' => $model->ref], 
                [
                    'class' => 'btn btn-warning',
                    'onclick' => 'proceed = 1',
                    'id' => 'btnCancel'
                ]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
