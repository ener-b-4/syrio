<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

  use yii\helpers\Html;
  use app\models\shared\Constants;
  use app\models\units\Units;


  /* @var $this yii\web\View */
  /* @var $model app\models\events\drafts\sections\subsections\B1 */
  
      //(start) Bug 7 - Finalisation of "Generate PDF" 
      $section_name = 'B';
      $subsection_name = '1';
      $title_subsection = $section_name . '.' . $subsection_name . '.';           //'B.1.'
      //(end) Bug 7 - Finalisation of "Generate PDF" 
  
  $strip = isset($stripped);
  if (!$strip)
  {  

      
      $this->title = Yii::t('app/crf', 'Section {id}', ['id'=>$title_subsection]);

    echo $this->render('/shared_parts/_breadcrumbs', [
      'title_subsection' => $title_subsection, 
      'module_name' => 'ADD_SUBSECTION'
    ]);
  }
  
?>
<div class="b1-view col-lg-12">

    <p>
        <?php
        if (!$strip)
        {
            // use this on any view to include the actionmessage logic and widget 
            //include Yii::getAlias('@app') . '\views\shared_parts\message_snippet.php';
            include Yii::getAlias('@app') . '/views/shared_parts/message_snippet.php';
        
           if ($model->sB->status == \app\models\events\EventDeclaration::INCIDENT_REPORT_DRAFT)
            { 
                echo Html::a(Yii::t('app', 'Modify'), ['events/drafts/sections/subsections/b1/update', 'id' => $model->id], ['class' => 'btn btn-primary']);
                echo Html::a(Yii::t('app', 'Finalize {evt}', ['evt'=>$section_name]), ['events/drafts/event-draft/finalize-section', 'id' => $model->sB->eventDraft->id, 's'=>'B'], ['class' => 'btn btn-success pull-right']);
            }
            
        }
      //(start) only display pdf if not stripped = 2
      if (!isset($stripped) || $stripped !== 2) {
      echo Html::a(Yii::t('app', '<i class="fa fa-file-pdf-o"></i> PDF'), 
      ['pdfgenerator/default/index', 'id' => $model->sB->eventDraft->id , 'section' => $section_name.'.'.$subsection_name.'.'], 
      ['class' => 'btn btn-default']); 
      }        
        ?>
         
    </p>

    <table>
        <tbody>
            <tr class="sy_para">
                <td class="sy_crf_para sy_section_h1">B.1.</td>
                <td class="sy_crf_para sy_section_h1">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <strong><em><?= \Yii::t('app/crf', 'General information'); ?></em></strong>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="sy_crf_para"></td>
                <td class="sy_crf_para">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <ul class="list-inline sy_padding_clear">
                            <li class="text-info">(a) <?= ' ' . \Yii::t('app/crf', 'Name/code of well') . ': ' ?></li>
                            <li><?= isset($model->b1_1) && trim($model->b1_1)!='' ? $model->b1_1:'<span class="sy_not_set">' . \Yii::t('app', '(not set)') . '</span>' ?></li>
                        </ul>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <ul class="list-inline sy_padding_clear">
                            <li class="text-info">(b) <?= ' ' . \Yii::t('app/crf', 'Name of drilling contractor (if relevant)') . ': ' ?></li>
                            <li><?= isset($model->b1_2) && trim($model->b1_2)!='' ? $model->b1_2:'<span class="sy_not_set">' . \Yii::t('app', '(not set)') . '</span>' ?></li>
                        </ul>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <ul class="list-inline sy_padding_clear">
                            <li class="text-info">(c) <?= ' ' . \Yii::t('app/crf', 'Name/type of drilling rig (if relevant)') . ': ' ?></li>
                            <li><?= isset($model->b1_3) && trim($model->b1_3)!='' ? $model->b1_3:'<span class="sy_not_set">' . \Yii::t('app', '(not set)') . '</span>' ?></li>
                        </ul>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <ul class="list-inline sy_padding_clear">
                            <li class="text-info">(d) <?= ' ' . \Yii::t('app/crf', 'Start and end date/time of loss of well control') . ': ' ?></li>
                            <li><?= isset($model->b1_4_s) ? $model->b1_4_s:'<span class="sy_not_set">' . \Yii::t('app', '(not set)') . '</span>' ?></li>
                            <li><span class="glyphicon glyphicon-arrow-right"></span></li>
                            <li><?= isset($model->b1_4_e) ? $model->b1_4_e:'<span class="sy_not_set">' . \Yii::t('app', '(not set)') . '</span>' ?></li>
                        </ul>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <ul class="list-inline sy_padding_clear">
                            <li class="text-info">(e) <?= ' ' . \Yii::t('app/crf', 'Type of fluid: brine / oil / gas / ... (if relevant)') . ': ' ?></li>
                            <li><?= isset($model->b1_5) ? \Yii::t('app/data', Constants::findOne(['num_code'=>$model->b1_5])->name) :'<span class="sy_not_set">' . \Yii::t('app', '(not set)') . '</span>' ?></li>
                        </ul>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <ul class="list-inline sy_padding_clear">
                            <li class="text-info">(f) <?= ' ' . \Yii::t('app/crf', 'Well head completion') . ': ' ?></li>
                            <li><?= isset($model->b1_6) ? \Yii::t('app/data', Constants::findOne(['num_code'=>$model->b1_6])->name) :'<span class="sy_not_set">' . \Yii::t('app', '(not set)') . '</span>' ?></li>
                        </ul>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <ul class="list-inline sy_padding_clear">
                            <li class="text-info">(g) <?= ' ' . \Yii::t('app/crf', 'Water depth') . ': ' ?></li>
                            <li><?= isset($model->b1_7) ? ($model->b1_7 . ' (m)') :'<span class="sy_not_set">' . \Yii::t('app', '(not set)') . '</span>' ?></li>
                        </ul>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <ul class="list-inline sy_padding_clear">
                            <li class="text-info">(h) <?= ' ' . \Yii::t('app/crf', 'Reservoir: pressure / temperature / depth') . ': ' ?></li>
                            <li>
                                <?php
                                    if (isset($model->b1_8_1_q)) {
                                        echo $model->b1_8_1_q . ' ';
                                        echo isset($model->b1_8_1_u) ? (' (' . Units::findOne($model->b1_8_1_u)->unit . ')') : \app\models\shared\NullValueFormatter::Format($model->b1_8_1_u);
                                    }
                                    else
                                    {
                                        echo \app\models\shared\NullValueFormatter::Format($model->b1_8_1_q);
                                    }
                                ?>
                            </li>
                            <li> / </li>
                            <li>
                                <?php
                                    if (isset($model->b1_8_2_q)) {
                                        echo $model->b1_8_2_q . ' ';
                                        echo isset($model->b1_8_2_u) ? (' (' . Units::findOne($model->b1_8_2_u)->unit . ')') : \app\models\shared\NullValueFormatter::Format($model->b1_8_2_u);
                                    }
                                    else
                                    {
                                        echo \app\models\shared\NullValueFormatter::Format($model->b1_8_2_q);
                                    }
                                ?>
                            </li>
                            <li> / </li>
                            <li>
                                <?php
                                    if (isset($model->b1_8_3_q)) {
                                        echo $model->b1_8_3_q . ' ';
                                        echo isset($model->b1_8_3_u) ? (' (' . Units::findOne($model->b1_8_3_u)->unit . ')') : \app\models\shared\NullValueFormatter::Format($model->b1_8_3_u);
                                    }
                                    else
                                    {
                                        echo \app\models\shared\NullValueFormatter::Format($model->b1_8_3_q);
                                    }
                                ?>
                            </li>
                        </ul>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <ul class="list-inline sy_padding_clear">
                            <li class="text-info">(i) <?= ' ' . \Yii::t('app/crf', 'Type of activity') . ': ' ?></li>
                            <li><?= isset($model->b1_9) ? \Yii::t('app/data', Constants::findOne(['num_code'=>$model->b1_9])->name) :'<span class="sy_not_set">' . \Yii::t('app', '(not set)') . '</span>' ?></li>
                        </ul>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <ul class="list-inline sy_padding_clear">
                            <li class="text-info">(j) <?= ' ' . \Yii::t('app', 'Type of well services') . ': ' ?></li>
                            <li>
                                <?php
                                if (isset($model->b1_9))
                                {
                                    if ($model->b1_9 == 704)
                                    {
                                        if (isset($model->b1_10)) 
                                        { 
                                            echo \Yii::t('app/data', Constants::findOne(['num_code'=>$model->b1_10])->name);
                                        }
                                        else
                                        {
                                            echo '<span class="sy_not_set">' . \Yii::t('app', '(not set)') . '</span>';                                                
                                        }
                                    }
                                    else
                                    {
                                        echo '<span>NA</span>';
                                    }
                                }
                                else
                                {
                                    echo '<span>-</span>';
                                }
                                ?>
                            </li>
                        </ul>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>

</div>
