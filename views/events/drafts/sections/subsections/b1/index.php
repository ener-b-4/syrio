<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\events\drafts\sections\subsections\B1Search */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'B1s');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="b1-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create {evt}', [
    'modelClass' => 'B1',
]), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'b1_1',
            'b1_2',
            'b1_3',
            'b1_4_s',
            // 'b1_4_e',
            // 'b1_5',
            // 'b1_6',
            // 'b1_7',
            // 'b1_8_1_q',
            // 'b1_8_1_u',
            // 'b1_8_2_q',
            // 'b1_8_2_u',
            // 'b1_8_3_q',
            // 'b1_8_3_u',
            // 'b1_9',
            // 'b1_9_details',
            // 'sb_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
