<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

  use yii\widgets\ActiveForm;

  /* @var $this yii\web\View */
  /* @var $model app\models\events\drafts\sections\subsections\E4 */
  /* @var $form yii\widgets\ActiveForm */
?>

<div class="i4-form">

    <?php $form = ActiveForm::begin(); ?>

    <table>
        <tbody>
            <tr class="sy_para">
                <td class="sy_crf_para sy_section_h1">E.4.</td>
                <td class="sy_crf_para sy_section_h1" style='width:100%;'>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <span>
                            <?= Yii::t('app/crf', 'Initial lessons learned and preliminary recommendations to prevent recurrence of similar events') ?>
                        </span>
                        <br/>
                        <span class='text-danger sy_narative-text'>
                            <?= Yii::t('app/crf', '(within 10 days of the event)') ?>
                        </span>
                        <?= $form->field($model, 'e4_1')->textarea(['class'=>'sy_fill_textarea sy_narative-text form-control'])->label('') ?>                        
                    </div>
                </td>
            </tr>
        </tbody>
    </table>

    <?php ActiveForm::end(); ?>

</div>
