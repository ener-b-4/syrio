<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\events\drafts\sections\subsections\J2 */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="j2-form">

    <?php $form = ActiveForm::begin(); ?>

    <table>
        <tbody>
            <tr class="sy_para">
                <td class="sy_crf_para sy_section_h1">J.2.</td>
                <td class="sy_crf_para sy_section_h1" style='width:100%;'>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <strong><?= Yii::t('app/crf', 'Description of circumstances, consequences of event and emergency response') ?></strong>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="sy_crf_para"></td>
                <td class="sy_crf_para">
                    <div class='col-lg-12'>
                        <p>
                            <?php
                            $s1 = 'Indicate the system that failed and provide a description of the circumstances of the event / describe what has happened. What are or are likely to be the significant adverse effects on the environment?';
                            echo Yii::t('app/crf', $s1);
                            ?>
                        </p>
                        <?= $form->field($model, 'j2_1')->textarea([
                            'class'=>'sy_fill_textarea sy_narative-text form-control',
                            'placeholder' => Yii::t('app', 'add text')
                            ])->label('') ?>                      
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    
    <?php ActiveForm::end(); ?>

</div>
