<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\widgets\DetailView;

use yii\helpers\ArrayHelper;

use app\models\crf\Sece;

/* @var $this yii\web\View */
/* @var $model app\models\events\drafts\sections\subsections\C2 */
/* @var $section2_1 app\models\events\drafts\sections\subsections\C2_1 */
/* @var $section2_2 app\models\events\drafts\sections\subsections\C2_2 */

$this->title = 'C.2.';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'C2s'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?php

/*
$available_seces_a = ArrayHelper::map(
        Sece::find()->where(['and', 'id>=100', 'id<200'])
        ->orderBy('id')->asArray()->all(), 'id', 'name');
*/

$available_seces = ArrayHelper::map(
        Sece::find()->orderBy('id')->asArray()->all(), 'id', 'name');
$model_seces = ArrayHelper::map($section2_1->sC21Seces,'sece_id','sece_id');
$last_group='';
$group_letters = [
    100 => ['a'=>Yii::t('app/crf', 'Structural integrity systems')], 
    200 => ['b'=>Yii::t('app/crf', 'Process containment systems')], 
    300 => ['c'=>Yii::t('app/crf', 'Ignition control systems')], 
    400 => ['d'=>Yii::t('app/crf', 'Detection systems')], 
    500 => ['e'=>Yii::t('app/crf', 'Process containment release systems')],  
    600 => ['f'=>Yii::t('app/crf', 'Protection systems')], 
    700 => ['g'=>Yii::t('app/crf', 'Shutdown systems')], 
    800 => ['h'=>Yii::t('app/crf', 'Navigational aids')], 
    900 => ['i'=>Yii::t('app/crf', 'Rotating equipment - power supply')], 
    1000 => ['j'=>Yii::t('app/crf', 'Escape, evacuation and rescue equipment')], 
    1100 => ['k'=>Yii::t('app/crf', 'Communication system')], 
    1200 => ['l'=>Yii::t('app/crf', 'Other, specify')],];

$UIitems=[];
$current_key = 'a';
$group_seed = 100;

$s='';
foreach ($available_seces as $key=>$value){
    //get group letter
    $offset = $key - $group_seed;
    if ($offset > 0 && $offset < 100) {
        //echo 'item in' . $group_seed . ': ' . $key . '<br/>';
        
        $s.='<div class=\'col-sm-4 sy_table_cell\'>';
        $s.=Html::checkbox('sel_sece[]', in_array($key, $model_seces), [
            'label' => $value,
            'value' => $key,
            'disabled' => true,
        ]);
        $s.='</div>';
        
        //$UIitems[$current_key] .= $s;
        //echo $s;
    }
    else
    {
        //add the 'other section and close the string for current subgroup
        $s.='<div class=\'col-sm-4 sy_table_cell\'>';
        $s.='   <div class=\'other_container\'>';
        $s.='       <div class=\'other_label\'>';
        $s.= Html::checkbox('other[]', false, ['label'=>'other']);
        $s.='       </div>';
        $s.='       <div class=\'other_text\'>';
        $s.= Html::textInput('other_text[]', null, ['readonly'=>true]);
        $s.='       </div>';
        $s.='   </div>';
        $s.='</div>';
        
        //echo $s;
        //die();
        
        $UIitems[$current_key] = $s;
        
        //set all for the new subgroup
        $s='';
        $group_seed += 100;
        $current_key = key($group_letters[$group_seed]);
        //echo '<p>' . $group_letters[$group_seed][$current_key] . '</p>';
        //echo 'item in' . $group_seed . ': ' . $key . '<br/>';

        $s.='<div class=\'col-sm-4 sy_table_cell\'>';
        $s.=Html::checkbox('sel_sece[]', in_array($key, $model_seces), [
            'label' => $value,
            'value' => $key,
        ]);
        $s.='</div>';

        //create a checkbox with the following:
        //echo $s;
    }
}
//add the last one manually
$UIitems[$current_key] = $s;

//add the 'l' manually
$UIitems['l'] = '';


//echo '<pre>';
//echo var_dump($UIitems);
//echo '</pre>';
//die();


?>


<div class="c2-view">

    <h1><?= Html::encode(Yii::t('app', 'Description of circumstances, consequences of an event and emergency response')) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <div class='hiddenBlock'>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'sc_id',
            ],
            'class' => 'hiddenBlock'
        ]) ?>
    </div>

    
    
    <p>
        <span class='text-info'><strong>C.2. </strong></span>
        <span class='text-info'><strong><em>Description of circumstances, consequences of an event and emergency response</em></strong></span>
    </p>
    
    
    <div>
        <p>
            <span class='text-info'><strong>C.2.1. </strong></span>
            <span class='text-info'><strong><?= Yii::t('app/crf', 'Description of SECE and circumstances') ?></strong></span>
        </p>
        <div style='margin-left:44px;'>
            <p>
                <?php
                    $text = 'Which Safety and Environmental Critical systems were reported by the independent verifier as lost or unavailable, requiring immediate remedial action, or have failed during an incident?';
                    echo Yii::t('app/crf',$text);
                ?>
            </p>
        </div>
        <div>
            <p style='float: left; margin-right: 12px;'>
                <strong><em><?= \Yii::t('app/crf', 'Origin') ?>:</em></strong>
            </p>
            <div style='float: left'>
                <ul class='list-unstyled'>
                    <li>
                        <div>
                            <?= Html::label(Yii::t('app/crf', 'Report independent verifier (report nr. / date / verifier)'),null, [
                                'class' => 'sy_normal_label',
                            ]) ?>
                            <br/>
                            <?= isset($section2_1->c21_1) ? $section2_1->c21_1 : '<div class = "sy_not_set">' . 
                            Yii::t('app', '(not set)') . '</div>' ?>
                        </div>
                    </li>

                    <li>
                        <div>
                            <?= Html::label(Yii::t('app/crf', 'Failure during major accident: details (date /accident description / ...)'),null, [
                                'class' => 'sy_normal_label',
                            ]) ?>
                            <br/>
                            <?= isset($section2_1->c21_2) ? $section2_1->c21_2 : '<div class = "sy_not_set">' . 
                            Yii::t('app', '(not set)') . '</div>' ?>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    </p>
    

    <p style='clear: both'>
        <strong class="text-info"><?= Yii::t('app/crf', 'Safety and Environmental Critical Elements concerned'); ?></strong>
    </p>
    <table class="table table-condensed sy_table_sece">
        <tbody>
            <?php
            reset($group_letters);
            foreach ($group_letters as $key=>$value) {
                $current_key = current($value);
                $current_letter = key($value);
                //create the sece type row
                
                $row = "<tr class='sy_table_sece_header'>"
                        . " <td sy_table_sece>"
                        . "<b>($current_letter) </b>$current_key"
                        . " </td>"
                        . "</tr>";
                echo $row;
                
                $contentRow = '<tr>'
                        . ' <td style="border-top: 0px" class="sy_table_sece">'
                        . $UIitems[$current_letter]                        
                        . ' </td>'
                        . '</tr>';
                echo $contentRow;
                
                
            }
            ?>
        </tbody>
    </table>
    
    <div>
        <p>
            <span class='text-info'><?= Yii::t('app/crf', 'C.2.2. Description of consequences') ?></span>
        </p>
        <div style='margin-left:44px;'>
            <em>
                <strong>
                    <?= Yii::t('app/crf','Is the incident likely to cause degradation to the surrounding marine environment?') ?>
                </strong>
            </em>
            <p>
                <span style='padding-right:12px;'>
                    <?= Html::radio('chk2_2_yes', $section2_2->c22_1, [
                        'label' => 'Yes'
                        ]) ?>
                </span>
                <span style='padding-right:12px;'>
                <?= Html::radio('chk2_2_no', !$section2_2->c22_1, [
                    'label' => 'No'
                    ]) ?>
                </span>
            </p>
            <?php
                $s='';
                
                $descUI = isset($section2_2->c22_desc) && strlen($section2_2->c22_desc) > 0 ? $section2_2->c22_desc :
                                '<div class="sy_not_set">' . Yii::t('app','(not set)') . '</div>';
                
                if ($section2_2->c22_1) {
                    $s .= '<div>'
                            . ' <p>'
                            . Yii::t('app', 'If yes, outline the environmental impacts which already have been observed or are likely to result from the incident'.'.')
                            . ' </p>'
                            . ' <div class = "sy_description">'
                            . $descUI
                            . ' </div>'
                       . '</div>';
                    
                    echo $s;
                }
            ?>
            
        </div>
    </div>

    
</div>
