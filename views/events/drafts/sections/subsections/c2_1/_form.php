<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use app\models\events\drafts\sections\subsections\C2_1;
use app\models\events\drafts\sections\subsections\C2_2;
use app\models\crf\Sece;

/* @var $this yii\web\View */
/* @var $model app\models\events\drafts\sections\subsections\C2 */
/* @var $sC2_1 app\models\events\drafts\sections\subsections\C2_1 */
/* @var $sC2_2 app\models\events\drafts\sections\subsections\C2_2 */
/* @var $form yii\widgets\ActiveForm */
?>

<?php

    //$section2_1 = $model->sC2_1;
    //$section2_2 = $model->sC2_2;

    $available_seces = ArrayHelper::map(
            Sece::find()->orderBy('id')->asArray()->all(), 'id', 'name');
    $model_seces = ArrayHelper::map($sC2_1->sC21Seces,'sece_id','sece_id');
    $last_group='';
    $group_letters = [
        100 => ['a'=>Yii::t('app/crf', 'Structural integrity systems')], 
        200 => ['b'=>Yii::t('app/crf', 'Process containment systems')], 
        300 => ['c'=>Yii::t('app/crf', 'Ignition control systems')], 
        400 => ['d'=>Yii::t('app/crf', 'Detection systems')], 
        500 => ['e'=>Yii::t('app/crf', 'Process containment release systems')],  
        600 => ['f'=>Yii::t('app/crf', 'Protection systems')], 
        700 => ['g'=>Yii::t('app/crf', 'Shutdown systems')], 
        800 => ['h'=>Yii::t('app/crf', 'Navigational aids')], 
        900 => ['i'=>Yii::t('app/crf', 'Rotating equipment - power supply')], 
        1000 => ['j'=>Yii::t('app/crf', 'Escape, evacuation and rescue equipment')], 
        1100 => ['k'=>Yii::t('app/crf', 'Communication system')], 
        1200 => ['l'=>Yii::t('app/crf', 'Other, specify')],];

$UIitems=[];
$current_key = 'a';
$group_seed = 100;

$s='';
foreach ($available_seces as $key=>$value){
    //get group letter
    $offset = $key - $group_seed;
    if ($offset > 0 && $offset < 100) {
        //echo 'item in' . $group_seed . ': ' . $key . '<br/>';
        
        $s.='<div class=\'col-sm-4 sy_table_cell\'>';
        $s.=Html::checkbox('sel_sece[]', in_array($key, $model_seces), [
            'label' => $value,
            'value' => $key,
            'disabled' => false,
        ]);
        $s.='</div>';
        
        //$UIitems[$current_key] .= $s;
        //echo $s;
    }
    else
    {
        $attr = 'c21_3_' . key($group_letters[$group_seed]);
        //echo $attr;
        //die();
        
        //add the 'other section and close the string for current subgroup
        $s.='<div class=\'col-sm-4 sy_table_cell\'>';
        $s.='   <div class=\'other_container\'>';
        $s.='       <div class=\'other_label\'>';
        //$s.= Html::checkbox('other[]', false, ['label'=>'other']);
        $s .= Html::activeCheckbox($sC2_1, $attr, [
            'uncheck'=>0,
            'class'=>'toggle-src',
            'tag'=>'c2_1-' . $attr . '_desc',
        ]);
        $s.='       </div>';
        $s.='       <div class=\'other_text\'>';
        $s.=Html::activeTextInput($sC2_1, $attr . '_desc');
        //$s.= Html::textInput('other_text[]', null, ['readonly'=>false]);
        $s.='       </div>';
        $s.='   </div>';
        $s.='</div>';
        
        //echo $s;
        //die();
        
        $UIitems[$current_key] = $s;
        
        //set all for the new subgroup
        $s='';
        $group_seed += 100;
        $current_key = key($group_letters[$group_seed]);
        //echo '<p>' . $group_letters[$group_seed][$current_key] . '</p>';
        //echo 'item in' . $group_seed . ': ' . $key . '<br/>';

        $s.='<div class=\'col-sm-4 sy_table_cell\'>';
        $s.=Html::checkbox('sel_sece[]', in_array($key, $model_seces), [
            'label' => $value,
            'value' => $key,
        ]);
        $s.='</div>';

        //create a checkbox with the following:
        //echo $s;
    }
}

    //add the last ones manually
    //add the 'other section and close the string for current subgroup
    $attr = 'c21_3_' . key($group_letters[$group_seed]);
    
    $s.='<div class=\'col-sm-4 sy_table_cell\'>';
    $s.='   <div class=\'other_container\'>';
    $s.='       <div class=\'other_label\'>';
    //$s.= Html::checkbox('other[]', false, ['label'=>'other']);
    $s .= Html::activeCheckbox($sC2_1, $attr, [
        'uncheck'=>0,
        'class'=>'toggle-src',
        'tag'=>'c2_1-' . $attr . '_desc',
    ]);
    $s.='       </div>';
    $s.='       <div class=\'other_text\'>';
    $s.=Html::activeTextInput($sC2_1, $attr . '_desc');
    //$s.= Html::textInput('other_text[]', null, ['readonly'=>false]);
    $s.='       </div>';
    $s.='   </div>';
    $s.='</div>';
    $UIitems[$current_key] = $s;

    //add the 'l' manually
    $group_seed += 100;
    $s='';
    //add the last ones manually
    //add the 'other section and close the string for current subgroup
    $attr = 'c21_3_' . key($group_letters[$group_seed]);
    
    $s.='<div class=\'col-sm-4 sy_table_cell\'>';
    $s.='   <div class=\'other_container\'>';
    $s.='       <div class=\'other_label\'>';
    //$s.= Html::checkbox('other[]', false, ['label'=>'other']);
    $s .= Html::activeCheckbox($sC2_1, $attr, [
        'uncheck'=>0,
        'class'=>'toggle-src',
        'tag'=>'c2_1-' . $attr . '_desc',
    ]);
    $s.='       </div>';
    $s.='       <div class=\'other_text\'>';
    $s.=Html::activeTextInput($sC2_1, $attr . '_desc');
    //$s.= Html::textInput('other_text[]', null, ['readonly'=>false]);
    $s.='       </div>';
    $s.='   </div>';
    $s.='</div>';
    $UIitems['l'] = '';
    $UIitems[$current_key] = $s;
    
?>

<div class="c2-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'sc_id')->textInput(['class'=>'hiddenBlock'])->label('', ['class'=>'hiddenBlock']) ?>

    <p class="sy_section_h1">
        <span>C.2. </span>
        <span><em>Description of circumstances, consequences of an event and emergency response</em></span>
    </p>
    
    <div>
        <p class="sy_section_h2">
            <span>C.2.1. <?= Yii::t('app/crf', 'Description of SECE and circumstances') ?></span>
        </p>
        <div style='margin-left:44px;'>
            <p>
                <?php
                    $text = 'Which Safety and Environmental Critical systems were reported by the independent verifier as lost or unavailable, requiring immediate remedial action, or have failed during an incident?';
                    echo Yii::t('app/crf',$text);
                ?>
            </p>
        </div>
        <div>
            <p style='float: left; margin-right: 12px;'>
                <strong><em><?= \Yii::t('app/crf', 'Origin') ?>:</em></strong>
            </p>
            <?= Html::activeHiddenInput($sC2_1, 'id') ?>
            <?= Html::activeHiddenInput($sC2_1, 'sc_2_id') ?>
            <div style='float: left'>
                <ul class='list-unstyled'>
                    <li>
                        <div>
                            <?= Html::label(Yii::t('app/crf', 'Report independent verifier (report nr. / date / verifier)'),null, [
                                'class' => 'sy_normal_label',
                            ]) ?>
                            <br/>
                            <?= Html::activeTextInput($sC2_1, 'c21_1', ['class' => 'form-control']) ?>
                        </div>
                    </li>

                    <li>
                        <div>
                            <?= Html::label(Yii::t('app/crf', 'Failure during major accident: details (date /accident description / ...)'),null, [
                                'class' => 'sy_normal_label',
                            ]) ?>
                            <br/>
                            <?= Html::activeTextarea($sC2_1, 'c21_2', ['class'=>'sy_fill_textarea form-control']) ?>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    </p>
    
    <p style='clear: both'>
        <strong class="text-info"><?= Yii::t('app/crf', 'Safety and Environmental Critical Elements concerned'); ?></strong>
    </p>
    <table class="table table-condensed sy_table_sece">
        <tbody>
            <?php
            reset($group_letters);
            foreach ($group_letters as $key=>$value) {
                $current_key = current($value);
                $current_letter = key($value);
                //create the sece type row
                
                $row = "<tr class='sy_table_sece_header'>"
                        . " <td sy_table_sece>"
                        . "<b>($current_letter) </b>$current_key"
                        . " </td>"
                        . "</tr>";
                echo $row;
                
                $contentRow = '<tr>'
                        . ' <td style="border-top: 0px" class="sy_table_sece">'
                        . $UIitems[$current_letter]                        
                        . ' </td>'
                        . '</tr>';
                echo $contentRow;
                
                
            }
            ?>
        </tbody>
    </table>
    
    
    <div>
        <p class="sy_section_h2">
            <span><?= Yii::t('app/crf', 'C.2.2. Description of consequences') ?></span>
        </p>
        <div style='margin-left:44px; padding-bottom: 32px;'>
            <em>
                <strong>
                    <?= Yii::t('app/crf','Is the incident likely to cause degradation to the surrounding marine environment?') ?>
                </strong>
            </em>
            <p>
                <span style='padding-right:12px;'>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <?= $form->field($sC2_2, 'c22_1')
                            ->radioList(
                                    ['1' => \Yii::t('app', 'Yes'), '0' =>\Yii::t('app', 'No')],
                                    [
                                        'unselect' => null,
                                        'item' => function($index, $label, $name, $checked, $value) {
                                              $showPanel = $index == 0 ? "true" : "false";
                                              $ischecked = $checked ? 'checked="checked"' : ''; // $value == '1' ? "checked " : '';
                                              $setting = 'class="sy_data_toggle" '
                                                      . 'sy_data_toggle_dest = "divC221"'
                                                      . 'sy_data_toggle_src_type = "radio"'
                                                      . 'sy_data_toggle_visible_value = "' . $showPanel . '"' 
                                                      . 'sy_data_toggle_enabled = "true"';
  
                                              $return = '<label>'
                                                      . ' <input type="radio" name="' . $name . '" value="'. $value .'" ' 
                                                      . $ischecked 
                                                      . ' '
                                                      . $setting
                                                      . '>'
                                                      . ' ' . $label
                                                      . '</input>'
                                                      . '</label>';
                                              return $return;
                                        }                                        
                                    ]
                                    )->label('', ['class'=>'hiddenBlock']) ?>
                    </div>
                    
                    
                    <?php // Html::activeRadioList($sC2_2, 'c22_1', $arr) ?>
                </span>
            </p>
            <div id="divC221" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 hiddenBlock" style='padding-right:12px; padding-bottom: 32px;'>
                <?= Yii::t('app/crf', 'If yes, outline the environmental impacts which already have been observed or are likely to result from the incident') .'.' ?> <br/>
                <?= Html::activeTextarea($sC2_2, 'c22_desc', ['class'=>'sy_fill_textarea form-control']) ?>
            </div>
            
        </div>
    </div>
    
    
    <?php ActiveForm::end(); ?>

</div>
