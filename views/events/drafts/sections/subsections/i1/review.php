<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

  use yii\helpers\Html;

  /* @var $this yii\web\View */
  /* @var $model app\models\events\drafts\sections\subsections\I1 */

    $strip = isset($stripped);
    if (!$strip) { 
        //(start) Bug 7 - Finalisation of "Generate PDF" 
        $section_name = 'I';
        $this->title = \Yii::t('app', 'SECTION {evt}', ['evt'=>$section_name]);
        //(end) Bug 7 - Finalisation of "Generate PDF" 
    }
  
  
  ?>
<div class="i1-view col-lg-12">

  <?php if (!isset($from_ca) || $from_ca != 1) {?>
    
  <!--(start) Bug 7 - Finalisation of "Generate PDF" -->
  <h3><strong><?= strtoupper($this->title) ?></strong></h3>
  <?php echo Html::a(Yii::t('app', '<i class="fa fa-file-pdf-o"></i> PDF'), 
    ['pdfgenerator/default/index', 'id' => $event_id , 'section' => $section_name], 
    ['class' => 'btn btn-default']); 
  ?>
  <!--(end) Bug 7 - Finalisation of "Generate PDF" -->
        
  <?php
    if ($model->sI->status < \app\models\events\EventDeclaration::INCIDENT_REPORT_FINALIZED && !isset($model->sI->locked_by)) {
      echo Html::a(Yii::t('app', 'Finalize {evt}', ['evt'=>$section_name]), ['events/drafts/event-draft/finalize-section', 'id' => $model->sI->eventDraft->id, 's'=>'I'], ['class' => 'btn btn-success pull-right']); 
    }
  ?>
  
    <p>
        <?php
        if (!$strip)
        {
            
            /* use this on any view to include the actionmessage logic and widget */
            //include Yii::getAlias('@app') . '\views\shared_parts\message_snippet.php';
            include Yii::getAlias('@app') . '/views/shared_parts/message_snippet.php';
            
        }
        ?>
         
    </p>

  <?php } //end ca_from ?>
    
    <table>
        <tbody>
            <tr class="sy_para">
                <td class="sy_crf_para"><strong>I.1.</strong></td>
                <td class="sy_crf_para">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <strong><?= Yii::t('app/crf', 'General information') ?></strong>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="sy_crf_para"></td>
                <td class="sy_crf_para">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                       
                        <ul class="list-inline sy_padding_clear">
                            <li><?= Yii::t('app/crf', 'Start and end date/time of evacuation') ?>: </li>
                            <li>
                                <div>
                                    <?= isset($model->i1_1_s) && trim($model->i1_1_s)!='' ? $model->i1_1_s:'<span class="sy_not_set">' . \Yii::t('app', '(not set)') . '</span>' ?>
                                    <span class="glyphicon glyphicon-arrow-right"></span>
                                    <?= isset($model->i1_1_e) && trim($model->i1_1_e)!='' ? $model->i1_1_e:'<span class="sy_not_set">' . \Yii::t('app', '(not set)') . '</span>' ?>
                                </div>
                            </li>
                            <li><?php // isset($model->i1_1) && trim($model->i1_1)!='' ? $model->i1_1:'<span class="sy_not_set">' . \Yii::t('app', '(not set)') . '</span>' ?></li>
                        </ul>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>

</div>
