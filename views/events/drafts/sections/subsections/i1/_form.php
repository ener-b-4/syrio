<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

  use yii\helpers\Html;
  use yii\widgets\ActiveForm;
  use kartik\widgets\DateTimePicker;

  /* @var $this yii\web\View */
  /* @var $model app\models\events\drafts\sections\subsections\I1 */
  /* @var $form yii\widgets\ActiveForm */

  //$distUnits=ArrayHelper::map(\app\models\units\Units::find()->where(['scope'=>6])->asArray()->all(), 'id', 'unit');

?>

<div class="b1-form">

    <?php $form = ActiveForm::begin([
        'enableAjaxValidation' => 'true'
    ]); ?>

    <table>
        <tbody>
            <tr class="sy_para">
                <td class="sy_crf_para sy_section_h1">I.1.</td>
                <td class="sy_crf_para sy_section_h1" style='width:100%;'>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <?= Yii::t('app/crf', 'General information') ?>
                    </div>
                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                       
                        <ul class="list-inline sy_padding_clear">
                            <li>
                               <div>
                                   <div>
                                       <small><a href="#" id="getEventDate"> > <?= \Yii::t('app', 'use Event date time as start date') ?> </a></small>
                                   </div>
                                    <?= Html::hiddenInput('event_date_time', $model->sI->eventDraft->event->event_date_time, [
                                        'id'=>'event_date_time',
                                    ]) ?>
                                   
                                   
                                <?php
                                    echo $form->field($model, 'i1_1_s')->widget(DateTimePicker::classname(), [
                                            'options' => [
                                                'placeholder' => 'Enter event start date and time ...',
                                                'style' => 'max-width:250px',
                                                'convertFormat' => true,
                                                ],
                                            'pluginOptions' => [
                                                'format' => 'yyyy-mm-dd hh:ii:ss',
                                                'todayHighlight' => false,
                                                'autoclose' => true,
                                                'endDate' => \date("Y-m-d H:i:s"),
                                            ]
                                    ]);            
                                                                   

                                    
                                    echo $form->field($model, 'i1_1_e')->widget(DateTimePicker::classname(), [
                                            'options' => [
                                                'placeholder' => 'Enter event end date and time ...',
                                                'style' => 'max-width:250px;',
                                                'convertFormat' => true,                                                
                                                ],
                                            'pluginOptions' => [
                                                'format' => 'yyyy-mm-dd hh:ii:ss',
                                                'todayHighlight' => false,
                                                'autoclose' => true,
                                                'endDate' => \date("Y-m-d H:i:s"),
                                            ]
                                    ]);            
                                ?>                                    
                                </div>
                            </li>
                            <li><?php // isset($model->i1_1) && trim($model->i1_1)!='' ? $model->i1_1:'<span class="sy_not_set">' . \Yii::t('app', '(not set)') . '</span>' ?></li>
                        </ul>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    

    <?php ActiveForm::end(); ?>

</div>

<?php
$js = <<< 'SCRIPT'
   //update the start date as event date
   $('#getEventDate').click(function($evt){
        $('#i1-i1_1_s').val($('#event_date_time').val());

        return false;
   });

SCRIPT;

$this->registerJs($js);

?>