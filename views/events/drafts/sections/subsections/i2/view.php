<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */


  use yii\helpers\Html;
  use app\models\shared\NullValueFormatter;
  use app\models\crf\CrfHelper;

  /* @var $this yii\web\View */
  /* @var $model app\models\events\drafts\sections\subsections\I2 */

      //(start) Bug 7 - Finalisation of "Generate PDF" 
      $section_name = 'I';
      $subsection_name = '2';
      $title_subsection = $section_name . '.' . $subsection_name . '.';           //'B.4.'
      //(end) Bug 7 - Finalisation of "Generate PDF" 
  
  $strip = isset($stripped);
  if (!$strip)
  {
    $this->title = Yii::t('app/crf', 'Section {id}', ['id'=>$title_subsection]);  

    echo $this->render('/shared_parts/_breadcrumbs', [
      'title_subsection' => $title_subsection, 
      'module_name' => 'ADD_SUBSECTION'
    ]);
  }
?>
<div class="i2-view col-lg-12">

    <p>
        <?php
        if (!$strip)
        {
            
          /* use this on any view to include the actionmessage logic and widget */
          //include Yii::getAlias('@app') . '\views\shared_parts\message_snippet.php';
          include Yii::getAlias('@app') . '/views/shared_parts/message_snippet.php';
          
           if ($model->sI->status == \app\models\events\EventDeclaration::INCIDENT_REPORT_DRAFT)
            { 
                echo Html::a(Yii::t('app', 'Modify'), ['events/drafts/sections/subsections/i2/update', 'id' => $model->id], ['class' => 'btn btn-primary']);
                echo Html::a(Yii::t('app', 'Finalize {evt}', ['evt'=>$section_name]), ['events/drafts/event-draft/finalize-section', 'id' => $model->sI->eventDraft->id, 's'=>'I'], ['class' => 'btn btn-success pull-right']);
            }
            
        }
      //(start) only display pdf if not stripped = 2
      if (!isset($stripped) || $stripped !== 2) {
      echo Html::a(Yii::t('app', '<i class="fa fa-file-pdf-o"></i> PDF'), 
          ['pdfgenerator/default/index', 'id' => $model->sI->eventDraft->id , 'section' => $section_name.'.'.$subsection_name.'.'], 
      ['class' => 'btn btn-default']); 
      }
        ?>
         
    </p>

    <table>
        <tbody>
            <tr class="sy_para">
                <td class="sy_crf_para sy_section_h1">I.2.</td>
                <td class="sy_crf_para sy_section_h1">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <strong><?= Yii::t('app/crf', 'Description of circumstances, consequences of event and emergency response') ?></strong>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="sy_crf_para"></td>
                <td class="sy_crf_para">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                       
                        <ul class="sy_padding_clear list-unstyled">
                            <li class="text-info"><?= Yii::t('app/crf', 'Was the evacuation precautionary or emergency?') ?> </li>
                            <li>
                                <ul class='list-unstyled list-inline' style='padding-top:12px; padding-bottom:6px; padding-left:5px;'>
                                    <li style='padding-right:12px;'>                                        
                                        <?= Html::radio('rb_i2_1', $model->i2_1 == 1, ['disabled'=>true]) ?>
                                        <span><?= Yii::t('app/crf', 'Precautionary')?></span>
                                    </li>
                                    <li>
                                        <?= Html::radio('rb_i2_1', $model->i2_1 == 2, ['disabled'=>true]) ?>
                                        <span><?= Yii::t('app/crf', 'Emergency')?></span>
                                    </li>
                                    <li>
                                        <?= Html::radio('rb_i2_1', $model->i2_1 == 3, ['disabled'=>true]) ?>
                                        <span><?= Yii::t('app/crf', 'Both')?></span>
                                    </li>
                                </ul>
                            </li>
                            <li style='padding-top:12px'>
                                <div class='sy_padding_clear'>
                                    <span class='text-info'>
                                        <?= $model->attributeLabels()['i2_2'] ?>
                                    </span>
                                    <?= isset($model->i2_2) && trim($model->i2_2)!='' ? $model->i2_2:'<span class="sy_not_set">' . \Yii::t('app', '(not set)') . '</span>' ?>
                                </div>
                                <?php // isset($model->i1_1) && trim($model->i1_1)!='' ? $model->i1_1:'<span class="sy_not_set">' . \Yii::t('app', '(not set)') . '</span>' ?>
                            </li>
                            <li style='padding-top:12px'>
                                <div class='sy_padding_clear'>
                                    <span class='text-info'>
                                        <?= $model->attributeLabels()['i2_3'] ?>
                                    </span>
                                    <?= isset($model->i2_3) && trim($model->i2_3)!='' ? $model->i2_3:'<span class="sy_not_set">' . \Yii::t('app', '(not set)') . '</span>' ?>
                                    
                                    <span class='text-muted'><?= \Yii::t('app/crf', '(e.g. helicopter)') ?></span>
                                </div>
                                <?php // isset($model->i1_1) && trim($model->i1_1)!='' ? $model->i1_1:'<span class="sy_not_set">' . \Yii::t('app', '(not set)') . '</span>' ?>
                            </li>
                        </ul>
                    </div>
                </td>                
            </tr>
            <tr>
                <td class="sy_crf_para"></td>
                <td class="sy_crf_para">
                    <div class='col-lg-12'>
                        <p>
                            <?php
                            $s1 = 'Indicate the system that failed and provide a description of the circumstances of the event / describe what has happened, unless already reported in a previous section of this report';
                            echo Yii::t('app/crf', $s1);
                            ?>
                        </p>
                        
                        <?php
                          $s='';
                          $s .= '<div>'
                              . nl2br(NullValueFormatter::Format($model->i2_4)) 
                             . '</div>';

                          echo $s;
                        ?>
                        
                    </div>
                </td>
            </tr>
        </tbody>
    </table>

</div>
