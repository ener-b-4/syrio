<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

  use yii\helpers\Html;

  /* @var $this yii\web\View */
  /* @var $model app\models\events\drafts\sections\subsections\I2 */
  $strip = isset($stripped);
?>
<div class="i2-view col-lg-12">

    <p>
        <?php
        if (!$strip)
        {
            
            /* use this on any view to include the actionmessage logic and widget */
            //include Yii::getAlias('@app') . '\views\shared_parts\message_snippet.php';
            include Yii::getAlias('@app') . '/views/shared_parts/message_snippet.php';
            
        }
        ?>
         
    </p>

    <table>
        <tbody>
            <tr class="sy_para">
                <td class="sy_crf_para"><strong>I.2.</strong></td>
                <td class="sy_crf_para">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <strong><?= Yii::t('app/crf', 'Description of circumstances, consequences of event and emergency response') ?></strong>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="sy_crf_para"></td>
                <td class="sy_crf_para">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                       
                        <ul class="sy_padding_clear list-unstyled">
                            <li><?= Yii::t('app/crf', 'Was the evacuation precautionary or emergency?') ?> </li>
                            <li>
                                <ul class='list-unstyled list-inline' style='padding-top:12px; padding-left: 5px;'>
                                    <li style='padding-right:12px;'>                                        
                                        <?= Html::radio('rb_i2_1', $model->i2_1 == 1, ['disabled'=>true]) ?>
                                        <span><?= Yii::t('app/crf', 'Precautionary')?></span>
                                    </li>
                                    <li>
                                        <?= Html::radio('rb_i2_1', $model->i2_1 == 2, ['disabled'=>true]) ?>
                                        <span><?= Yii::t('app/crf', 'Emergency')?></span>
                                    </li>
                                    <li>
                                        <?= Html::radio('rb_i2_1', $model->i2_1 == 3, ['disabled'=>true]) ?>
                                        <span><?= Yii::t('app/crf', 'Both')?></span>
                                    </li>
                                </ul>
                            </li>
                            <li style='padding-top:12px'>
                                <div class='sy_padding_clear'>
                                    <span><?= $model->attributeLabels()['i2_2'] . ': ' ?></span>
                                    <?= \app\models\shared\NullValueFormatter::Format($model->i2_2) ?>
                                </div>
                                <?php // isset($model->i1_1) && trim($model->i1_1)!='' ? $model->i1_1:'<span class="sy_not_set">' . \Yii::t('app', '(not set)') . '</span>' ?>
                            </li>
                            <li style='padding-top:12px'>
                                <div class='sy_padding_clear'>
                                    <span><?= $model->attributeLabels()['i2_3'] ?></span>
                                    <span class='text-muted'> <?= Yii::t('app/crf', '(e.g. helicopter)') . ': ' ?> </span>
                                    
                                    <?= \app\models\shared\NullValueFormatter::Format($model->i2_3) . ' ' ?>                                    
                                </div>
                                <?php // isset($model->i1_1) && trim($model->i1_1)!='' ? $model->i1_1:'<span class="sy_not_set">' . \Yii::t('app', '(not set)') . '</span>' ?>
                            </li>
                        </ul>
                    </div>
                </td>                
            </tr>
            <tr>
                <td class="sy_crf_para"></td>
                <td class="sy_crf_para">
                    <div class='col-lg-12'>
                        <p>
                            <?php
                            $s1 = 'Indicate the system that failed and provide a description of the circumstances of the event / describe what has happened, unless already reported in a previous section of this report';
                            echo Yii::t('app/crf', $s1);
                            ?>
                        </p>
                        
                        <?php
                          $s='';
                          $s .= '<div>'
                              . nl2br(\app\models\shared\NullValueFormatter::Format($model->i2_4)) 
                             . '</div>';

                          echo $s;
                        ?>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>

</div>
