<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

  use yii\helpers\Html;
  use yii\widgets\ActiveForm;

  /* @var $this yii\web\View */
  /* @var $model app\models\events\drafts\sections\subsections\I2 */
  /* @var $form yii\widgets\ActiveForm */

  $rbList = [
      1=>Yii::t('app/crf', 'Precautionary'),
      2=>Yii::t('app/crf', 'Emergency'),
      3=>Yii::t('app/crf', 'Both')
  ];
  $selList = [];
  $selList[] = $model->i2_1;
?>

<div class="i2-form">

    <?php $form = ActiveForm::begin([
        'enableAjaxValidation' => 'true'
    ]); ?>

    <table>
        <tbody>
            <tr class="sy_para">
                <td class="sy_crf_para sy_section_h1">I.2.</td>
                <td class="sy_crf_para sy_section_h1" style='width:100%;'>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <strong><?= Yii::t('app/crf', 'Description of circumstances, consequences of event and emergency response') ?></strong>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="sy_crf_para"></td>
                <td class="sy_crf_para">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                       
                        <ul class="sy_padding_clear list-unstyled">
                            <li class="text-info"><?= Yii::t('app/crf', 'Was the evacuation precautionary or emergency?') ?> </li>
                            <li>
                                <?= Html::radioList('rbList', $selList, $rbList) ?>
                            </li>
                            <li style='padding-top:12px'>
                                <div class='sy_padding_clear'>
                                    <ul class="list-unstyled">
                                        <li>
                                            <span class='text-info'>
                                                <?= $model->attributeLabels()['i2_2'] ?>
                                            </span>
                                        </li>
                                        <li>
                                            <?= Html::activeTextInput($model, 'i2_2',[
                                                'placeholder' => Yii::t('app','positive integer'),
                                                'class' => 'form-control',
                                                'style' => 'max-width: 170px;'
                                            ]) ?>
                                        </li>
                                    </ul>
                                </div>
                                <?php // isset($model->i1_1) && trim($model->i1_1)!='' ? $model->i1_1:'<span class="sy_not_set">' . \Yii::t('app', '(not set)') . '</span>' ?>
                            </li>
                            <li style='padding-top:12px'>
                                <div class='sy_padding_clear'>
                                    <ul class="list-unstyled">
                                        <li>
                                            <span class='text-info'>
                                                <?= $model->attributeLabels()['i2_3'] ?>
                                            </span>
                                        </li>
                                        <li>
                                            <?= Html::activeTextInput($model, 'i2_3',[
                                                'placeholder' => Yii::t('app','e.g. helicopter'),
                                                'class'=>'form-control'
                                            ]) ?>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                </td>                
            </tr>
            <tr>
                <td class="sy_crf_para"></td>
                <td class="sy_crf_para">
                    <div class='col-lg-12'>
                        <p>
                            <?php
                            $s1 = 'Indicate the system that failed and provide a description of the circumstances of the event / describe what has happened, unless already reported in a previous section of this report';
                            echo Yii::t('app/crf', $s1);
                            ?>
                        </p>
                        <?= Html::activeTextarea($model, 'i2_4', [
                            'class'=>'sy_fill_textarea sy_narative-text form-control', 
                            'readonly'=>false,
                            'placeholder'=>Yii::t('app', 'add text'),
                        ]) ?>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    

    <?php ActiveForm::end(); ?>

</div>

<?php
$js = <<< 'SCRIPT'
SCRIPT;
// Register tooltip/popover initialization javascript
$this->registerJs($js);
?>