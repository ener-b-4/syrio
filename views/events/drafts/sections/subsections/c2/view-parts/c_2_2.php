<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

  use yii\helpers\Html;

  use app\models\crf\CrfHelper;
  use app\models\shared\TextFormatter;
  use app\models\shared\NullValueFormatter;
  use app\assets\AppAssetPdf;
  
  /* @var $this yii\web\View */
  /* @var $model app\models\events\drafts\sections\subsections\C2 */
  //Compose the request si/no
  if ($is_pdf == 1) { 
    //Registrtion different ASSETS for PDF
    AppAssetPdf::register($this);
  }

?>

<?php
    $s = '<div class="row" id="content-c2_2">';
    echo $s; 
?>

<div class='sy_margin_bottom_20 col-lg-12'>
        
        <div class='row form-inline'>
            
            <div class="col-lg-12 sy_margin_bottom_10 crf-h1-font">
                <span class='title-number'><strong><?= \Yii::t('app/crf', 'C.2.2.') ?></strong></span> 
                <span class='title-text'><strong><?= CrfHelper::SectionsTitle()['C.2.2'] ?></strong></span>
            </div>

            <div class="crf-h1 col-lg-12">
                <div class="section-content crf-h1-section-content">
                    <div class='row'>
                        <div class="col-lg-12 sy_margin_bottom_10">
                            <strong><i><?php
                                $text = \Yii::t('app/crf', 'Is the incident likely to cause degradation to the surrounding marine environment?');
                                echo $text;
                            ?></i></strong>
                        </div>
                        <div class="col-lg-12 sy_margin_bottom_10">
                            <?php
                                //GENREATED BY VIEW
                                if ($is_pdf == 0) {
                                    $checkList = Html::checkboxList(
                                    'chka2_3',    
                                    $model->sC2_2->c22_1, 
                                    [
                                        1 => \Yii::t('app', 'Yes'),
                                        0 => \Yii::t('app', 'No'),
                                    ],
                                    [
                                            'unselect' => null,
                                            'disabled',
                                            'item' => function($index, $label, $name, $checked, $value) {
                                                  $showPanel = $index == 0 ? "true" : "false";
                                                  $ischecked = $checked ? 'checked="checked"' : ''; // $value == '1' ? "checked " : '';
                                                  $setting = 'class="sy_data_toggle" '
                                                          . 'sy_data_toggle_dest = "c2_2_div"'
                                                          . 'sy_data_toggle_src_type = "radio"'
                                                          . 'sy_data_toggle_visible_value = "' . $showPanel . '"' 
                                                          . 'sy_data_toggle_enabled = "true"';

                                                  $return = '<label>'
                                                          . ' <input type="checkbox" disabled name="' . $name . '" value="'. $value .'" ' 
                                                          . $ischecked 
                                                          . ' '
                                                          . $setting
                                                          . '>'
                                                          . ' <span class="crf-base-font sy_pad_right_18">' . $label . '</span>'
                                                          . '</input>'
                                                          . '</label>';
                                                  return $return;
                                            }                                        
                                    ]); 

                                    echo NullValueFormatter::FormatObject($model->sC2_2->c22_1, 'c22_1', null, $checkList);

                                } else //PDF
                                {
                                    $is_yes_no = $model->sC2_2->c22_1;
                                    $l_si_no = \Yii::t('app', 'Yes') ;

                                    if ($is_yes_no == true)  { $l_si_no = $l_si_no . '<img src="css/images/checkbox-selected.png" width="16" height="16" style="margin:0 10 0 10;" />'; }; 
                                    if ($is_yes_no == false) { $l_si_no = $l_si_no . '<img src="css/images/checkbox.png" width="16" height="16" style="margin:0 10 0 10;" />'; }; 

                                    $l_si_no = $l_si_no . \Yii::t('app/crf', 'No') ;
                                    if (!$is_yes_no == true)  { $l_si_no = $l_si_no . '<img src="css/images/checkbox-selected.png" width="16" height="16"  style="margin:0 10 0 10;" />'; }; 
                                    if (!$is_yes_no == false) { $l_si_no = $l_si_no . '<img src="css/images/checkbox.png" width="16" height="16"  style="margin:0 10 0 10;" />'; }; 

                                    echo $l_si_no;
                                }
                            ?>
                        </div> <!-- end col -->
                        <div class="col-lg-12 sy_margin_bottom_10">
                            <p>
                            <?php
                                $txt = 'If yes, outline the environmental impacts which already have been observed or are likely to result from the incident';
                                echo \Yii::t('app/crf',  $txt) . '.<br/>';
                            ?>
                            </p>

                            
                            <div class="sy_descriptive_text <?php echo json_encode($is_pdf) == 1 ? 'text-info' : '' ?>">
                                <?= NullValueFormatter::Format(
                                            TextFormatter::TextToHtml($model->sC2_2->c22_desc)) ?>
                            </div>
                        </div> <!-- end col -->
                    </div>
                </div>            
            </div>
        </div>
</div>

<?php
    $s = '</div>';
    echo $s;
?>

