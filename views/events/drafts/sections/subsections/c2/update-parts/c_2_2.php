<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

/**
 * c_2_1 form
 *
 * @author vamanbo
 */
use yii\helpers\Html;
use kartik\form\ActiveForm;
use yii\helpers\ArrayHelper;

use app\models\events\drafts\sections\subsections\C2_2;
use app\models\crf\Sece;

use app\models\crf\CrfHelper;
use app\models\shared\NullValueFormatter;

/* @var $this yii\web\View */
/* @var $model app\models\events\drafts\sections\subsections\C2 */
/* @var $sC2_1 app\models\events\drafts\sections\subsections\C2_2 */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="row" id='content-c2_2'>
    <div class='col-lg-12 sy_margin_bottom_26 crf-h2p'>
        <span class='title-number'>C.2.2.</span> 
        <span class='title-text'><?= CrfHelper::SectionsTitle()['C.2.2'] ?></span>
        <div class='col-lg-12 section-content'>
            <div class='row' style="margin-left: 6px;">   <!-- content row -->
                <div class='col-lg-12'>
                <p>
                    <strong>
                        <em>
                        <?php
                            $text = Yii::t('app/crf', 'Is the incident likely to cause degradation to the surrounding marine environment?');
                            echo $text;
                        ?>
                        </em>
                    </strong>
                </p>
                </div> 
                
                <div class="col-lg-12">
                    <?php
                        $checklist = $form->field($model->sC2_2, 'c22_1', [
                            'showLabels'=>false
                        ])->radioList(
                        ['1' => \Yii::t('app', 'Yes'), '0' =>\Yii::t('app', 'No')],
                        [
                                'unselect' => null,
                                'item' => function($index, $label, $name, $checked, $value) {
                                      $showPanel = $index == 0 ? "true" : "false";
                                      $ischecked = $checked ? 'checked="checked"' : ''; // $value == '1' ? "checked " : '';
                                      $setting = 'class="sy_data_toggle" '
                                              . 'sy_data_toggle_dest = "c2_2_div"'
                                              . 'sy_data_toggle_src_type = "radio"'
                                              . 'sy_data_toggle_visible_value = "' . $showPanel . '"' 
                                              . 'sy_data_toggle_enabled = "true"';

                                      $return = '<label>'
                                              . ' <input type="radio" name="' . $name . '" value="'. $value .'" ' 
                                              . $ischecked 
                                              . ' '
                                              . $setting
                                              . '>'
                                              . ' <span class="crf-base-font sy_pad_right_18">' . $label . '</span>'
                                              . '</input>'
                                              . '</label>';
                                      return $return;
                                }                                        
                        ]); 

                        echo $checklist;
                    ?>
                </div>
                <div class='col-lg-12 hiddenBlock' id = 'c2_2_div'>
                    <?php
                    $txt = \Yii::t('app/crf', 'If yes, outline the environmental impacts which already have been observed or are likely to result from the incident').'.';
                    echo $txt . '<br/>';
                    ?>
                    <div class='sy_pad_top_6 sy_margin_bottom_10'>
                        <?= $form->field($model->sC2_2, 'c22_desc', [
                            'showLabels' => FALSE,                               //this only works for kartik
                            'options' => [                                      //this works with yii2 ActiveInput
                                'class' => 'full-width'
                            ],
                            'addon' => [                                        //this works for kartik
                                'groupOptions' => [
                                    'class' => 'full-width',
                                ]
                            ]
                        ])->textarea([
                            'rows' => 8,
                            //'cols' => 100,
                            'class' => 'form-control full-width',
                            'style' => 'border-radius: 6px;'                    //hard-coded cause it doesn't get renderred else
                            ]) ?>
                    </div>                                
                </div>
            </div> 
        </div>      <!-- end section content 1 -->
    </div>
</div>