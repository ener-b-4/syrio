<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

/**
 * c_2_1 form
 *
 * @author vamanbo
 */
use yii\helpers\Html;
use kartik\form\ActiveForm;
use yii\helpers\ArrayHelper;

use app\models\events\drafts\sections\subsections\C2_1;
use app\models\crf\Sece;

use app\models\crf\CrfHelper;
use app\models\shared\NullValueFormatter;

/* @var $this yii\web\View */
/* @var $model app\models\events\drafts\sections\subsections\C2 */
/* @var $sC2_1 app\models\events\drafts\sections\subsections\C2_1 */
/* @var $form yii\widgets\ActiveForm */
?>


<?php

$section2_1 = $model->sC2_1;

$available_seces = ArrayHelper::map(
        Sece::find()->orderBy('id')->asArray()->all(), 'id', 'name');
$model_seces = ArrayHelper::map($section2_1->sC21Seces,'sece_id','sece_id');
$last_group='';
$group_letters = [
    100 => ['a'=>Yii::t('app/crf', 'Structural integrity systems')], 
    200 => ['b'=>Yii::t('app/crf', 'Process containment systems')], 
    300 => ['c'=>Yii::t('app/crf', 'Ignition control systems')], 
    400 => ['d'=>Yii::t('app/crf', 'Detection systems')], 
    500 => ['e'=>Yii::t('app/crf', 'Process containment release systems')],  
    600 => ['f'=>Yii::t('app/crf', 'Protection systems')], 
    700 => ['g'=>Yii::t('app/crf', 'Shutdown systems')], 
    800 => ['h'=>Yii::t('app/crf', 'Navigational aids')], 
    900 => ['i'=>Yii::t('app/crf', 'Rotating equipment - power supply')], 
    1000 => ['j'=>Yii::t('app/crf', 'Escape, evacuation and rescue equipment')], 
    1100 => ['k'=>Yii::t('app/crf', 'Communication system')], 
    1200 => ['l'=>Yii::t('app/crf', 'Other, specify')],];

?>


<div class="row" id='content-c2_1'>
    <div class='col-lg-12 sy_margin_bottom_26 crf-h2p'>
        <span class='title-number'>C.2.1.</span> 
        <span class='title-text'><?= CrfHelper::SectionsTitle()['C.2.1'] ?></span>
        <div class='col-lg-12 section-content'>
            <div class='row' style="margin-left: 6px;">   <!-- content row -->
                <div class='col-lg-12 sy_pad_top_6'>
                <p>
                    <?php
                        $text = 'Which Safety and Environmental Critical systems were reported by the independent verifier as lost or unavailable, requiring immediate remedial action, or have failed during an incident?';
                        echo Yii::t('app/crf',$text);
                    ?>
                </p>
                </div> 
                
                <div class="col-lg-12">
                    <table class="crf-table-nb full-width">
                        <tr>
                            <td rowspan="2">
                                <strong>
                                    <em class="sy_pad_right_18">
                                        <?= \Yii::t('app/crf', 'Origin') . ': ' ?>
                                    </em>
                                </strong>
                            </td>
                            <td>
                                <div class="sy_pad_bottom_6">
                                    <?= $section2_1->attributeLabels()['c21_1'] ?>
                                    <br/><br/>
                                    <span class="sy_descriptive_text">
                                        <?= $form->field($section2_1, 'c21_1', [
                                            'showLabels' => false
                                        ])->textInput([
                                            'placeholder' => \Yii::t('app', '(not set)')
                                        ]) ?>
                                    </span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div>
                                    <?= $section2_1->attributeLabels()['c21_2'] ?>
                                    
                                    <div class="full-width sy_pad_top_18">
                                        <?= $form->field($section2_1, 'c21_2', [
                                            'showLabels' => false
                                        ])->textArea([
                                            'placeholder' => \Yii::t('app', '(not set)'),
                                            'class'=>'form-control full-width',
                                            'rows'=>8,
                                        ]) ?>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div> 
        </div>      <!-- end section content 1 -->
    </div>
    
    <div class='col-lg-12 sy_margin_bottom_26 crf-h2p'>
        <div class='title-text sy_pad_top_18'>
            <?= \Yii::t('app/crf', 'Safety and Environmental Critical Elements concerned') ?>
        </div>
        
        
        <div class='col-lg-12 section-content crf-h2-box'>
            <table class="table crf-table">
                <tbody>
                    <?php
                    reset($group_letters);
                    foreach ($group_letters as $key=>$value) {
                        //  key             = 100
                        //  value           = ['a', 'Structural integrity']
                        //  key($value)     = 'a'
                        //  current($value) = 'Structural integrity'
                        
                        $current_letter = key($value);
                        
                        $current_seces = ArrayHelper::map( 
                                Sece::find()
                                ->where(['between', 'id', intval($key), intval($key+99)])->all(),
                                'id', 'name');

                        //create the sece type header row
                        $row = "<tr class='sy_table_sece_header'>"
                                . " <th colspan='3'>"
                                . " <b>($current_letter)" . ' ' . current($value) . "</b>"
                                . " </th>"
                                . "</tr>";
                        echo $row;
                        
                        //populate the seces cells in a three cell layout
                        $attr = 'sece_'.$current_letter;
                        echo Html::activeCheckboxList($section2_1, $attr, $current_seces, [
                            'item' => function ($index, $label, $name, $checked, $value) {
                                $openRow = $index % 3 === 0 ? '<tr>' : '';
                                $closeRow = $index % 3 === 2 ? '</tr>' : '';
                            
                                $cell = '<td class="third">'
                                        . '<span class="crf-item-label">'
                                        . Html::checkbox($name, $checked, [
                                            'value' => $value,
                                            'label' => \Yii::t('app/crf', $label),
                                            'labelOptions' => [
                                                'class' => 'crf-base-font'
                                            ]
                                        ])
                                        . '</span>'
                                        . '</td>';
                                
                                return $openRow . $cell . $closeRow;
                            }
                        ]);
                        
                        $last_row_cells_count = count($current_seces) % 3;
                        
                        //create the other cell
                        $other_attr = 'c21_3_'.$current_letter;
                        $other_text_attr = 'c21_3_'.$current_letter.'_desc';
                        $other_text_div_id = $other_text_attr . '_div';
                        $other_cell = '<td class="third">'
                                . '<div class="form-inline">'
                                . '<span class="crf-item-label">'
                                . Html::activeCheckbox($section2_1, $other_attr, [
                                    'class' => 'sy_data_toggle',
                                    'sy_data_toggle_dest' => $other_text_div_id,
                                    'sy_data_toggle_src_type' => 'checkbox',
                                    'sy_data_toggle_enabled' => 'true',
                                    'sy_data_toggle_visible_value' => 'true',
                                    
                                    'labelOptions' => [
                                        'class' => 'crf-base-font'
                                    ]
                                ])
                                . '</span>'
                                . '<span id = "' . $other_text_div_id . '" class="sy_descriptive_text">'
                                . ' ' . 
                                $form->field($section2_1, $other_text_attr, [
                                    'showLabels' => false
                                ])->textInput([
                                    'placeholder' => \Yii::t('app', '(not set)')
                                ])
                                . '</span>'
                                . '</div>'
                                . '</td>';
                        
                        $openRow = $last_row_cells_count % 3 === 0 ? '<tr>' : '';
                        //$closeRow = $last_row_cells_count === 2 ? '</tr>' : '';

                        echo $openRow . $other_cell;
                        $last_row_cells_count++;
                        
                        //create the additional cells (if needed)
                        $missing_cells = 3 - ($last_row_cells_count !== 0 ? $last_row_cells_count : 3) ; // intval(count($current_seces) / 3);
                        
                        if ($missing_cells !== 0 && $missing_cells < 3) {
                            for($i = 1; $i<=$missing_cells; $i++) {
                                echo '<td></td>';
                            }
                            echo '</tr>';
                        }
                    }
                    ?>
                </tbody>
            </table>
        </div>
        
    </div> 
</div> 
