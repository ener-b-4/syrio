<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

  use yii\helpers\Html;
  use yii\widgets\DetailView;

  use yii\helpers\ArrayHelper;

  use app\models\crf\Sece;
  use app\widgets\ActionMessageWidget;

  use app\models\shared\NullValueFormatter;
  /* @var $this yii\web\View */
  /* @var $model app\models\events\drafts\sections\subsections\C2 */
  /* @var $section2_1 app\models\events\drafts\sections\subsections\C2_1 */
  /* @var $section2_2 app\models\events\drafts\sections\subsections\C2_2 */

  $strip = isset($stripped);
  $col_lg_number = 'col-lg-12';
  $is_pdf = 0;
?>

<?php

/*
$available_seces_a = ArrayHelper::map(
        Sece::find()->where(['and', 'id>=100', 'id<200'])
        ->orderBy('id')->asArray()->all(), 'id', 'name');
*/

$available_seces = ArrayHelper::map(
        Sece::find()->orderBy('id')->asArray()->all(), 'id', 'name');
$model_seces = ArrayHelper::map($section2_1->sC21Seces,'sece_id','sece_id');
$last_group='';
$group_letters = [
    100 => ['a'=>Yii::t('app/crf', 'Structural integrity systems')], 
    200 => ['b'=>Yii::t('app/crf', 'Process containment systems')], 
    300 => ['c'=>Yii::t('app/crf', 'Ignition control systems')], 
    400 => ['d'=>Yii::t('app/crf', 'Detection systems')], 
    500 => ['e'=>Yii::t('app/crf', 'Process containment release systems')],  
    600 => ['f'=>Yii::t('app/crf', 'Protection systems')], 
    700 => ['g'=>Yii::t('app/crf', 'Shutdown systems')], 
    800 => ['h'=>Yii::t('app/crf', 'Navigational aids')], 
    900 => ['i'=>Yii::t('app/crf', 'Rotating equipment - power supply')], 
    1000 => ['j'=>Yii::t('app/crf', 'Escape, evacuation and rescue equipment')], 
    1100 => ['k'=>Yii::t('app/crf', 'Communication system')], 
    1200 => ['l'=>Yii::t('app/crf', 'Other, specify')],];

$UIitems=[];
$current_key = 'a';
$group_seed = 100;

$s='';
foreach ($available_seces as $key=>$value){
    //get group letter
    $offset = $key - $group_seed;
    if ($offset > 0 && $offset < 100) {
        //echo 'item in' . $group_seed . ': ' . $key . '<br/>';
        
        $s.='<div class=\'col-sm-4 sy_table_cell\'>';
        $s.=Html::checkbox('sel_sece[]', in_array($key, $model_seces), [
            'label' => $value,
            'value' => $key,
            'disabled' => true,
        ]);
        $s.='</div>';
        
        //$UIitems[$current_key] .= $s;
        //echo $s;
    }
    else
    {
        $attr = 'c21_3_' . key($group_letters[$group_seed]);
        
        //add the 'other section and close the string for current subgroup
        $s.='<div class=\'col-sm-4 sy_table_cell\'>';
        $s.='   <div class=\'other_container\'>';
        $s.='       <div class=\'other_label\'>';
        $s.= Html::checkbox($attr, $section2_1->getAttribute($attr), ['label'=>'other', 'disabled' => true]);
        $s.='       </div>';
        $s.='       <div class=\'other_text\'>';
        $s.= Html::textInput($attr . '_desc', $section2_1->getAttribute($attr . '_desc'), ['readonly'=>true]);
        $s.='       </div>';
        $s.='   </div>';
        $s.='</div>';
        
        //echo $s;
        //die();
        
        $UIitems[$current_key] = $s;
        
        //set all for the new subgroup
        $s='';
        $group_seed += 100;
        $current_key = key($group_letters[$group_seed]);
        //echo '<p>' . $group_letters[$group_seed][$current_key] . '</p>';
        //echo 'item in' . $group_seed . ': ' . $key . '<br/>';

        $s.='<div class=\'col-sm-4 sy_table_cell\'>';
        $s.=Html::checkbox('sel_sece[]', in_array($key, $model_seces), [
            'label' => $value,
            'value' => $key,
        ]);
        $s.='</div>';

        //create a checkbox with the following:
        //echo $s;
    }
}
//add the last one manually
$UIitems[$current_key] = $s;

//add the 'l' manually
$UIitems['l'] = '';


//echo '<pre>';
//echo var_dump($UIitems);
//echo '</pre>';
//die();


?>


<div class="c2-view col-lg-12">

    <p>
        <?php
        if (!$strip)
        {
            /* use this on any view to include the actionmessage logic and widget */
            //include Yii::getAlias('@app') . '\views\shared_parts\message_snippet.php';
            include Yii::getAlias('@app') . '/views/shared_parts/message_snippet.php';
            
        }
        ?>
         
    </p>
    
    <div class='hiddenBlock'>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'sc_id',
            ],
            'class' => 'hiddenBlock'
        ]) ?>
    </div>
    
    
    <p>
        <span><strong>C.2. </strong></span>
        <span><strong><?= \Yii::t('app/crf', 'Description of circumstances, consequences of event and emergency response') ?></strong></span>
    </p>
        
    <div>
        <p>
            <span><strong>C.2.1. <?= Yii::t('app/crf', 'Description of SECE and circumstances') ?></strong></span>
        </p>
        <div style='margin-left:44px;'>
            <p>
                <?php
                    $text = 'Which Safety and Environmental Critical systems were reported by the independent verifier as lost or unavailable, requiring immediate remedial action, or have failed during an incident?';
                    echo Yii::t('app/crf',$text);
                ?>
            </p>
        </div>
        <div>
            <p style='float: left; margin-right: 12px;'>
                <strong><em><?= \Yii::t('app/crf', 'Origin') . ': '?></em></strong>
            </p>
            <div style='margin-left:44px; float: left'>
                <ul class='list-unstyled'>
                    <li>
                        <div class="sy_pad_bottom_18">
                            <?= Html::label(Yii::t('app/crf', 'Report independent verifier (report nr. / date / verifier)'),null, [
                                'class' => 'sy_normal_label',
                            ]) ?>
                            <br/>
                            <?= NullValueFormatter::Format($section2_1->c21_1) ?>
                        </div>
                    </li>

                    <li>
                        <div>
                            <?= Html::label(\Yii::t('app/crf', 'Failure during major accident: details (date /accident description / ...)'),null, [
                                'class' => 'sy_normal_label',
                            ]) ?>
                            <br/>
                            <i>
                            <?= NullValueFormatter::Format($section2_1->c21_2) ?>
                            </i>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    </p>
    

    <div style='clear: both' class='sy_pad_bottom_18'>
        <em><strong><?= Yii::t('app/crf', 'Safety and Environmental Critical Elements concerned') ?></strong></em>
    </div>

    
    <div style='margin-left:44px'>
    <!-- Which Safety and Environmental Critical Systems were reported by the independent verifier -->
       
        <div class='row form-inline'>

                <div class='<?= $col_lg_number ?> section-content'>
                    <!-- Which Safety and Environmental Critical Systems were reported by the independent verifier -->
                    <div class='row'>
                        <div class="<?= $col_lg_number ?>">
                            <div class="row form-inline">                            
                                <div class="<?= $col_lg_number ?>">
                                    <table class="table crf-table">
                                            <tbody>
                                                    <?php
                                                    reset($group_letters);
                                                    foreach ($group_letters as $key=>$value) {
                                                            //  key             = 100
                                                            //  value           = ['a', 'Structural integrity']
                                                            //  key($value)     = 'a'
                                                            //  current($value) = 'Structural integrity'

                                                            $current_letter = key($value);

                                                            $current_seces = ArrayHelper::map( 
                                                                            Sece::find()
                                                                            ->where(['between', 'id', intval($key), intval($key+99)])->all(),
                                                                            'id', 'name');

                                                            //create the sece type header row
                                                            $row = "<tr class='sy_table_sece_header'>"
                                                                            . " <th colspan='3'>"
                                                                            . " <b>($current_letter)" . ' ' . current($value) . "</b>"
                                                                            . " </th>"
                                                                            . "</tr>";
                                                            echo $row;

                                                            //populate the seces cells in a three cell layout
                                                            $attr = 'sece_'.$current_letter;

                                                            if ($is_pdf == 0) {                                                            // VIEW pages
                                                                echo Html::activeCheckboxList($section2_1, $attr, $current_seces, [
                                                                    'item' => function ($index, $label, $name, $checked, $value) {
                                                                            $openRow = $index % 3 === 0 ? '<tr>' : '';
                                                                            $closeRow = $index % 3 === 2 ? '</tr>' : '';

                                                                            $cell = '<td class="third">'
                                                                                            . Html::checkbox($name, $checked, [
                                                                                                    'value' => $value,
                                                                                                    'label' => \Yii::t('app/crf', $label),
                                                                                                    'disabled' => true,
                                                                                                    'labelOptions' => [
                                                                                                            'class' => 'crf-base-font'
                                                                                                    ]
                                                                                            ])
                                                                                            . '</td>';
                                                                            
                                                                            return $openRow . $cell . $closeRow;
                                                                    }
                                                                ]);
                                                            }
                                                            else
                                                            {   // PDF page
                                                                echo Html::activeCheckboxList($section2_1, $attr, $current_seces, [
                                                                    'item' => function ($index, $label, $name, $checked, $value) {
                                                                            $openRow = $index % 3 === 0 ? '<tr>' : '';
                                                                            $closeRow = $index % 3 === 2 ? '</tr>' : '';

                                                                            $cell = '<td>';
                                                                            
                                                                            if ($checked == true)  { $check_img = '<img src="css/images/checkbox-selected.png" width="16" height="16" style="padding-top: 3px;" />'; }; 
                                                                            if ($checked == false) { $check_img = '<img src="css/images/checkbox.png" width="16" height="16" style="padding-top: 3px;" />';          };   
                                                                            
                                                                            $cell_t = '<table border="0" cellspacing="0" cellpadding="0">'
                                                                                  . '<tr>'
                                                                                  . '  <td style = "border-bottom:0px;border-left:0px;border-top:0px;border-right:0px; padding:0px; padding-right:3px; ">' . $check_img . '</td>'
                                                                                  . '  <td style = "border-bottom:0px;border-left:0px;border-top:0px;border-right:0px; padding:0px; padding-right:3px; padding-top:2px">' . $label . '</td>'
                                                                                  . '</tr>'
                                                                                  . '</table>';
                                                                            
                                                                            //$cell = $cell . $label . '</td>';
                                                                            $cell .= $cell_t . '</td>';

                                                                            return $openRow . $cell . $closeRow;
                                                                    }
                                                                ]);
                                                            }
                                                            
                                                            $last_row_cells_count = count($current_seces) % 3;

                                                            //create the other cell
                                                            $other_attr = 'c21_3_'.$current_letter;
                                                            $other_text_attr = 'c21_3_'.$current_letter.'_desc';
                                                            $other_text_div_id = $other_text_attr . '_div';

                                                            $other_cell = '';
                                                            if ($is_pdf == 0) {						// VIEW pages
                                                                $other_cell = '<td class="third">'
                                                                        . '<span>'
                                                                            . Html::activeCheckbox($section2_1, $other_attr, [
                                                                                    'class' => 'sy_data_toggle',
                                                                                    'sy_data_toggle_dest' => $other_text_div_id,
                                                                                    'sy_data_toggle_src_type' => 'checkbox',
                                                                                    'sy_data_toggle_enabled' => 'true',
                                                                                    'sy_data_toggle_visible_value' => 'true',

                                                                                    'disabled' => true,

                                                                                    'labelOptions' => [
                                                                                            'class' => 'crf-base-font'
                                                                                    ]
                                                                            ])
                                                                            . '</span>';
                                                                            $other_cell = $other_cell                                                                            
                                                                                            . '<span id = "' . $other_text_div_id . '" class="sy_descriptive_text' . (json_encode($is_pdf) == 1 ? ' text-info ' : ' ') . '">' 
                                                                                            . ' ' . NullValueFormatter::Format($section2_1->$other_text_attr)
                                                                                            . '</span>'
                                                                                            . '</td>';
                                                                    
                                                            }
                                                            else {						// PDF pages
                                                                    $check_img = null;
                                                                    if ($section2_1->$other_attr == 1)  { $check_img = '<img src="css/images/checkbox-selected.png" width="16" height="16" style="padding-top: 3px; float: left;" />'; }; 
                                                                    if ($section2_1->$other_attr == 0) { $check_img = '<img src="css/images/checkbox.png" width="16" height="16" style="padding-top: 3px; float: left;" />';          };   
                                                                    $other_cell = $other_cell . '<td>';
                                                                    
                                                                    $other_html = isset($section2_1->$other_text_attr) && 
                                                                            $section2_1->$other_text_attr !== '' ? NullValueFormatter::Format($section2_1->$other_text_attr) : '';
                                                                    
                                                                    $cell_t = '<table border="0" cellspacing="0" cellpadding="0">'
                                                                          . '<tr>'
                                                                          . '  <td style = "border-bottom:0px;border-left:0px;border-top:0px;border-right:0px; padding:0px; padding-right:3px; ">' . $check_img . '</td>'
                                                                          . '  <td style = "border-bottom:0px;border-left:0px;border-top:0px;border-right:0px; padding:0px; padding-right:3px; padding-top:2px">' . 
                                                                            $section2_1->getAttributeLabel($other_attr) 
                                                                            . '</td>'
                                                                            . '<td style = "border-bottom:0px;border-left:0px;border-top:0px;border-right:0px; padding:0px; padding-right:3px; padding-top:2px">'
                                                                                    . '<span id = "' . $other_text_div_id . '" class="sy_descriptive_text' . (json_encode($is_pdf) == 1 ? ' text-info ' : ' ') . '">' 
                                                                                    . ' ' . $other_html
                                                                                    . '</span>'
                                                                            . '</td>'                                                                            
                                                                          . '</tr>'
                                                                          . '</table>';
                                                                    $other_cell = $other_cell . $cell_t                                                                            
                                                                                    . '</td>';
                                                                    
                                                                          
                                                            }
                                                            
                                                            $openRow = $last_row_cells_count % 3 === 0 ? '<tr>' : '';
                                                            //$closeRow = $last_row_cells_count === 2 ? '</tr>' : '';

                                                            echo $openRow . $other_cell;
                                                            $last_row_cells_count++;

                                                            //create the additional cells (if needed)
                                                            $missing_cells = 3 - ($last_row_cells_count !== 0 ? $last_row_cells_count : 3) ; // intval(count($current_seces) / 3);

                                                            if ($missing_cells !== 0 && $missing_cells < 3) {
                                                                    for($i = 1; $i<=$missing_cells; $i++) {
                                                                            echo '<td></td>';
                                                                    }
                                                                    echo '</tr>';
                                                            }
                                                    }
                                                    ?>
                                            </tbody>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div> 

                </div>
            
            
        </div>
    
    
        
        
        
    </div>
    
    <div>
        <p>
          <span><strong><?= Yii::t('app/crf', 'C.2.2. Description of consequences') ?></strong></span>
        </p>
        <div style='margin-left:44px;'>
            <em>
                <strong>
                    <?= Yii::t('app/crf','Is the incident likely to cause degradation to the surrounding marine environment?') ?>
                </strong>
            </em>
            
            <p>
                <span style='padding-right:12px;'>
                    <?= Html::radio('chk2_2_yes', $section2_2->c22_1, [
                        'label' => \Yii::t('app', 'Yes'), 'disabled'=>'disabled'
                        ]) ?>
                </span>
                <span style='padding-right:12px;'>
                <?= Html::radio('chk2_2_no', !$section2_2->c22_1, [
                    'label' => \Yii::t('app', 'No'), 'disabled'=>'disabled'
                    ]) ?>
                </span>
            </p>
            <?php
                $s='';
                
                $helpBlock = Yii::t('app/crf', 'If yes, outline the environmental impacts which already have been observed or are likely to result from the incident') . '.';
                
                  $descUI='';
                  $descUI .= '<div>'
                      . nl2br(NullValueFormatter::Format($section2_2->c22_desc)) 
                     . '</div>';
                  
                if ($section2_2->c22_1) {

                    $s .= '<div class="sy_pad_top_32 sy_pad_bottom_6">'
                            . ' <p>'
                            . $helpBlock
                            . ' </p>'
                            . ' <div class = "">'
                            . $descUI
                            . ' </div>'
                       . '</div>';
                    
                    echo $s;
                }
            ?>
            
        </div>
    </div>
</div>

<?php

$JS = <<< EOF

$('input[type=checkbox]').each(function() {
    $ (this).attr('disabled', 'disabled');    
});        
$('input[type=text]').each(function() {
    $ (this).attr('disabled', 'disabled');    
});        
        
EOF;

$this->registerJs($JS, yii\web\View::POS_READY);

?>