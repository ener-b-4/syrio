<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use app\models\events\drafts\sections\subsections\C2_1;
use app\models\events\drafts\sections\subsections\C2_2;
use app\models\crf\Sece;

use app\models\crf\CrfHelper;


/* @var $this yii\web\View */
/* @var $model app\models\events\drafts\sections\subsections\C2 */
/* @var $sC2_1 app\models\events\drafts\sections\subsections\C2_1 */
/* @var $sC2_2 app\models\events\drafts\sections\subsections\C2_2 */
/* @var $form yii\widgets\ActiveForm */
?>

<?php

?>

<div class="col-lg-12" style="padding-top: 16px; padding-left: -12px; padding-right: -12px;">
    <div class='sectionC-2 sy_pad_bottom_18'>
        <div class="row">
            <div class='col-lg-12'>
                <span class='title-number'>C.2. </span>
                <span class='title-text'><?= CrfHelper::SectionsTitle()['C.2'] ?></span>
            </div>              <!-- end crf-h1 -->
        </div>
        <div class="crf-h2-box">
            <!-- put the C2 form parts as they're needed when loading the model -->
            <?= $form->field($model, 'id')->hiddenInput()->label('', ['class'=>'hiddenBlock']) ?>
            
            <?= $this->render('update-parts/c_2_1', [
                'model' => $model,
                'form' => $form
            ]) ?>
            
            <?= $this->render('update-parts/c_2_2', [
                'model' => $model,
                'form' => $form
            ]) ?>
            
        </div>
        
    </div>

</div>
