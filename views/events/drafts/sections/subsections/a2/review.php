<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

  use yii\helpers\Html;
  use app\models\units\Units;

  use app\models\shared\NullValueFormatter;
  use app\models\crf\CrfHelper;
  
  /* @var $this yii\web\View */
  /* @var $model app\models\events\drafts\sections\subsections\A2 */

  $strip = isset($stripped);
?>
<div class="a2-view col-lg-12">
      
  <p>
    <!-- pass the effective controller/action, since this is rendered as partial from SectionA -->
    <?php
    if (!$strip)
    {
      // use this on any view to include the actionmessage logic and widget 
      //include Yii::getAlias('@app') . '\views\shared_parts\message_snippet.php';
      include Yii::getAlias('@app') . '/views/shared_parts/message_snippet.php';
    }
    ?>
  </p>

    
  <table>
      <tbody>
          <tr class="sy_para">
              <td class="sy_pad_bottom_6"><strong>A.2.</strong></td>
              <td class="sy_pad_bottom_6 ">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <strong><?= CrfHelper::SectionsTitle()['A.2'] ?></strong>
                  </div>
              </td>
          </tr>
          <tr>
              <td></td>
              <td>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 sy_pad_bottom_18">
                    <?php
                      $s='';
                      $s .= '<div>'
                          . nl2br(NullValueFormatter::Format($model->a2_desc)) 
                         . '</div>';

                      echo $s;
                    ?>
                  </div>
              </td>
          </tr>
          <tr class="sy_para">
              <td class="sy_crf_para "><strong>A.2.1.</strong></td>
              <td class="sy_crf_para">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <strong><?= CrfHelper::SectionsTitle()['A.2.1'] ?></strong>
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <ul class="list-inline" style="padding-left: 5px">
                          <li><?= Html::radio('rb_a2_1', isset($model->a2_1) ? $model->a2_1 : 0, ["label"=>'Yes', 'disabled'=>true]) ?></li>
                          <li><?= Html::radio('rb_a2_1', isset($model->a2_1) ? !$model->a2_1 : 0, ["label"=>'No', 'disabled'=>true]) ?></li>
                      </ul>
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <?= Yii::t('app/crf', 'If') . ' <u>' . lcfirst(\Yii::t('app', 'Yes')) . '</u>, ' . \Yii::t('app/crf', 'specify the type and quantity of released substance') . ' '; ?>
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <ul class="list-inline"  style="padding-left: 5px; padding-top:6px;">
                          <li>
                              <?= ucfirst(\Yii::t('app', 'type')) ?>: 
                              <?= ' ' . NullValueFormatter::Format($model->a2_1_t) ?>
                          </li>
                          <li>
                              <?= ucfirst(\Yii::t('app', 'quantity')) ?>: 
                              <?= NullValueFormatter::Format($model->a2_1_q)?>
                              <?= !isset($model->a2_1_u) ? ' ' . NullValueFormatter::Format($model->a2_1_u) : Units::findOne($model->a2_1_u)->unit ?>
                          </li>
                      </ul>
                  </div>
              </td>
          </tr>
          <tr class="sy_para">
              <td class="sy_crf_para"><strong>A.2.2.</strong></td>
              <td class="sy_crf_para">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <strong><?=  CrfHelper::SectionsTitle()['A.2.2'] ?></strong>
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <ul class="list-inline" style="padding-left: 5px; padding-top:6px;">
                          <li><?= Html::radio('rb_a2_2', isset($model->a2_2) ? $model->a2_2 : 0, ["label"=>'Yes', 'disabled'=>true]) ?></li>
                          <li><?= Html::radio('rb_a2_2', isset($model->a2_2) ? !$model->a2_2 : 0, ["label"=>'No', 'disabled'=>true]) ?></li>
                      </ul>
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 sy_pad_bottom_18">
                    <?= Yii::t('app/crf', 'Describe circumstances') . ':' ?><br/><br/>
                    <?php
                      $s='';
                      $s .= '<div>'
                          . nl2br(NullValueFormatter::Format($model->a2_2_desc)) 
                         . '</div>';

                      echo $s;
                    ?>
                  </div>
              </td>
          </tr>
          <tr class="sy_para">
              <td class="sy_crf_para "><strong>A.2.3.</strong></td>
              <td class="sy_crf_para">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <strong><?= CrfHelper::SectionsTitle()['A.2.3'] ?></strong>
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <ul class="list-inline" style="padding-left: 5px; padding-top:6px;">
                          <li><?= Html::radio('rb_a2_3', isset($model->a2_3) ? $model->a2_3 : 0, ["label"=>'Yes', 'disabled'=>true]) ?></li>
                          <li><?= Html::radio('rb_a2_3', isset($model->a2_3) ? !$model->a2_3 : 0, ["label"=>'No', 'disabled'=>true]) ?></li>
                      </ul>
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 sy_pad_bottom_18">
                    <?= Yii::t('app/crf', 'If yes, outline the environmental impacts which already have been observed or are likely to result from the incident') . '.' ?><br/><br/>
                    <?php
                      $s='';
                      $s .= '<div>'
                          . nl2br(NullValueFormatter::Format($model->a2_3_desc)) 
                         . '</div>';

                      echo $s;
                    ?>
                  </div>
              </td>
          </tr>
      </tbody>
  </table>
    
    
</div>
