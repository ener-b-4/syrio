<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\events\drafts\sections\subsections\A2search */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'A2s');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="a2-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create {evt}', [
    'modelClass' => 'A2',
]), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'a2_1',
            'a2_1_q',
            'a2_1_u',
            'a2_1_t',
            // 'a2_2',
            // 'a2_2_desc:ntext',
            // 'a2_3',
            // 'a2_3_desc:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
