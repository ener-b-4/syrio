<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\crf\CrfHelper;

/* @var $this yii\web\View */
/* @var $model app\models\events\drafts\sections\subsections\A2 */
/* @var $form yii\widgets\ActiveForm */

$massUnits=ArrayHelper::map(\app\models\units\Units::find()->where(['scope'=>1])->asArray()->all(), 'id', 'unit');

?>

<div class="a2-form">
    
    <?php $form = ActiveForm::begin(); ?>
    <table>
        <tbody>
            <tr class="sy_para sy_section_h1">
                <td class="">A.2.</td>
                <td class="">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <em><?= CrfHelper::SectionsTitle()['A.2'] ?></em>
                    </div>
                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-bottom: 32px;">
                        <?= $form->field($model, 'a2_desc')->textarea(['class'=>'sy_fill_textarea sy_narative-text form-control'])->label('') ?>                      
                    </div>
                </td>
            </tr>
            <tr class="sy_para">
                <td class="sy_crf_para sy_section_h2">A.2.1.</td>
                <td class="sy_crf_para">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <?php 
                            $label_text = CrfHelper::SectionsTitle()['A.2.1'];
                            $label = '<em class="sy_section_h2">' . $label_text .'</em>'; 
                        ?>
                        <?= $form->field($model, 'a2_1')
                            ->radioList(
                                    ['1' => \Yii::t('app', 'Yes'), '0' =>\Yii::t('app', 'No')],
                                    [
                                        'unselect' => null,
                                        'item' => function($index, $label, $name, $checked, $value) {
                                              $showPanel = $index == 0 ? "true" : "false";
                                              $ischecked = $checked ? 'checked="checked"' : ''; // $value == '1' ? "checked " : '';
                                              $setting = 'class="sy_data_toggle" '
                                                      . 'sy_data_toggle_dest = "divA21"'
                                                      . 'sy_data_toggle_src_type = "radio"'
                                                      . 'sy_data_toggle_visible_value = "' . $showPanel . '"' 
                                                      . 'sy_data_toggle_enabled = "true"';
  
                                              $return = '<label>'
                                                      . ' <input type="radio" name="' . $name . '" value="'. $value .'" ' 
                                                      . $ischecked 
                                                      . ' '
                                                      . $setting
                                                      . '>'
                                                      . ' ' . $label
                                                      . '</input>'
                                                      . '</label>';
                                              return $return;
                                        }                                        
                                    ]
                                    )->label($label) ?>
                        
                        <!--
                        <ul class="list-inline">
                            <li class="sy_vert_top"><?= Html::activeRadio($model, 'a2_1', ['id'=>'a2_1_y', 'label'=>'Yes', 'value'=>1, 'uncheck'=>null, 'onclick'=>'a_2_toggle()']) ?>
                            <li class="sy_vert_top"><?= Html::activeRadio($model, 'a2_1', ['label'=>'No', 'value'=>0, 'uncheck'=>null, 'onclick'=>'a_2_toggle()']) ?>
                        </ul>
                        -->
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 hiddenBlock" id="divA21">
                        <?= ucfirst(\Yii::t('app/crf', 'specify the type and quantity of released substance')) ?>
                        <br/>
                        <ul class="list-inline sy_pad_top_18" style="padding-left:5px">
                            <li class="sy_vert_top">
                                <?= $form->field($model, 'a2_1_t')->textInput(['value'=>$model->a2_1_t])->label($model->attributeLabels()['a2_1_t']) ?>
                            </li>
                            <li class="sy_vert_top">
                                <?= $form->field($model, 'a2_1_q')->textInput(['value'=>$model->a2_1_q])->label($model->attributeLabels()['a2_1_q']) ?>
                            </li>
                            <li class="sy_vert_top">
                                <?= $form->field($model,'a2_1_u')->dropDownList($massUnits, ['id'=>'unit'])->label($model->attributeLabels()['a2_1_u']); ?>
                            </li>
                        </ul>
                    </div>
                </td>
            </tr>
            <tr class="sy_para">
                <td class="sy_crf_para sy_section_h2">A.2.2.</td>
                <td class="sy_crf_para">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <?php 
                            $label_text = CrfHelper::SectionsTitle()['A.2.2'];
                            $label = '<em class="sy_section_h2">' . $label_text .'</em>'; 
                        ?>
                        <?= $form->field($model, 'a2_2')
                            ->radioList(
                                    ['1' => \Yii::t('app', 'Yes'), '0' =>\Yii::t('app', 'No')],
                                    [
                                        'unselect' => null,
                                        'item' => function($index, $label, $name, $checked, $value) {
                                              $showPanel = $index == 0 ? "true" : "false";
                                              $ischecked = $checked ? 'checked="checked"' : ''; // $value == '1' ? "checked " : '';
                                              $setting = 'class="sy_data_toggle" '
                                                      . 'sy_data_toggle_dest = "divA22"'
                                                      . 'sy_data_toggle_src_type = "radio"'
                                                      . 'sy_data_toggle_visible_value = "' . $showPanel . '"' 
                                                      . 'sy_data_toggle_enabled = "true"';
  
                                              $return = '<label>'
                                                      . ' <input type="radio" name="' . $name . '" value="'. $value .'" ' 
                                                      . $ischecked 
                                                      . ' '
                                                      . $setting
                                                      . '>'
                                                      . ' ' . $label
                                                      . '</input>'
                                                      . '</label>';
                                              return $return;
                                        }                                        
                                    ]
                                    )->label($label) ?>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 hiddenBlock" id="divA22">
                        <?= \Yii::t('app/crf', 'Describe circumstances') . ': ' ?><br/>
                        <?= $form->field($model, 'a2_2_desc')->textarea(['class'=>'sy_fill_textarea form-control'])->label('') ?>                      
                    </div>
                </td>
            </tr>
            <tr class="sy_para">
                <td class="sy_crf_para sy_section_h2">A.2.3.</td>
                <td class="sy_crf_para">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <?php 
                            $label_text = \Yii::t('app/crf', 'Is the incident likely to cause degradation to the surrounding marine environment?');
                            $label = '<em class="sy_section_h2">' . $label_text .'</em>'; 
                        ?>
                        <?= $form->field($model, 'a2_3')
                            ->radioList(
                                    ['1' => \Yii::t('app', 'Yes'), '0' =>\Yii::t('app', 'No')],
                                    [
                                        'unselect' => null,
                                        'item' => function($index, $label, $name, $checked, $value) {
                                              $showPanel = $index == 0 ? "true" : "false";
                                              $ischecked = $checked ? 'checked="checked"' : ''; // $value == '1' ? "checked " : '';
                                              $setting = 'class="sy_data_toggle" '
                                                      . 'sy_data_toggle_dest = "divA23"'
                                                      . 'sy_data_toggle_src_type = "radio"'
                                                      . 'sy_data_toggle_visible_value = "' . $showPanel . '"' 
                                                      . 'sy_data_toggle_enabled = "true"';
  
                                              $return = '<label>'
                                                      . ' <input type="radio" name="' . $name . '" value="'. $value .'" ' 
                                                      . $ischecked 
                                                      . ' '
                                                      . $setting
                                                      . '>'
                                                      . ' ' . $label
                                                      . '</input>'
                                                      . '</label>';
                                              return $return;
                                        }                                        
                                    ]
                                    )->label($label) ?>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 hiddenBlock" id='divA23'>
                        <?= \Yii::t('app/crf', 'If yes, outline the environmental impacts which already have been observed or are likely to result from the incident') . '.' ?>  <br/>
                        <?= $form->field($model, 'a2_3_desc')->textarea(['class'=>'sy_fill_textarea form-control'])->label('') ?>                      
                    </div>
                </td>
            </tr>
        </tbody>
    </table>

   
    
    <?php ActiveForm::end(); ?>        
    
</div>
