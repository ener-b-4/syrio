<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

  use yii\helpers\Html;
  use app\models\units\Units;
  use app\models\crf\CrfHelper;
  use app\models\shared\NullValueFormatter;
  
  /* @var $this yii\web\View */
  /* @var $model app\models\events\drafts\sections\subsections\A2 */

      $section_name = 'A';
      $subsection_name = '2';
      $title_subsection = $section_name . '.' . $subsection_name . '.';           //'A.2.'
  $strip = isset($stripped);
  if (!$strip)
  {

      $this->title = Yii::t('app/crf', 'Section {id}', ['id'=>$title_subsection]);

    echo $this->render('/shared_parts/_breadcrumbs', [
      'title_subsection' => $title_subsection, 
      'module_name' => 'ADD_SUBSECTION'
    ]);
  }
?>
<div class="a2-view col-lg-12">

    <p>
        <!-- pass the effective controller/action, since this is rendered as partial from SectionA -->
        <?php
        if (!$strip)
        {
            // use this on any view to include the actionmessage logic and widget 
            //include Yii::getAlias('@app') . '\views\shared_parts\message_snippet.php';
            include Yii::getAlias('@app') . '/views/shared_parts/message_snippet.php';
            
            $updateUrl = Yii::$app->urlManager->createUrl(['events/drafts/sections/subsections/a2/update', 'id'=>$model->id]);
        
            if ($model->sA->status == \app\models\events\EventDeclaration::INCIDENT_REPORT_DRAFT)
            {
                if ($model->hasErrors())
                {
                    echo "<div class='alert alert-danger' role='alert'>";
                    echo '<h3>The Incident report could not be signed!</h3>';
                    echo '<p>Please review the information below and try again.</p>';
                    echo '<ul>';

                    foreach($model->errors as $key => $errors)
                    {
                        foreach($errors as $error)
                        {
                           echo "<li>" . $error . '</li>';
                        }
                        //echo "<br/>" . $value;
                    }
                    echo '</ul>';
                    echo "</div>";
                    echo '<pre>';
                    echo var_dump($model->errors);
                    echo '</pre>';
                }                
                echo Html::a(Yii::t('app', 'Modify'), ['events/drafts/sections/subsections/a2/update', 'id' => $model->id], ['class' => 'btn btn-primary']);
                echo Html::a(Yii::t('app', 'Finalize {evt}', ['evt'=>$section_name]), ['events/drafts/event-draft/finalize-section', 'id' => $model->sA->eventDraft->id, 's'=>'A'], ['class' => 'btn btn-success pull-right']);
            }
        }

      //(start) only display pdf if not stripped = 2
      if (!isset($stripped) || $stripped !== 2) {
          echo Html::a(Yii::t('app', '<i class="fa fa-file-pdf-o"></i> PDF'), 
          ['pdfgenerator/default/index', 'id' => $model->sA->eventDraft->id , 'section' => $section_name.'.'.$subsection_name.'.'], 
          ['class' => 'btn btn-default']); 
      }
        ?>
    </p>

    
    <table>
        <tbody>
            <tr class="sy_para sy_section_h1">
                <td class="sy_crf_para">A.2.</td>
                <td class="sy_crf_para">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <em><?= CrfHelper::SectionsTitle()['A.2'] ?></em>
                    </div>
                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-bottom: 32px;">
                    <?php
                      $s='';
                      $s .= '<div>'
                          . nl2br(NullValueFormatter::Format($model->a2_desc)) 
                         . '</div>';

                      echo $s;
                    ?>
                    </div>
                </td>
            </tr>
            <tr class="sy_para">
                <td class="sy_crf_para sy_section_h2">A.2.1.</td>
                <td class="sy_crf_para">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <em class="sy_section_h2"><?= CrfHelper::SectionsTitle()['A.2.1'] ?></em>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-left: 5px;">
                        <?= Html::radioList(
                                'rb_a2_1', 
                                $model->a2_1, 
                                ['1' => \Yii::t('app', 'Yes'), '0' =>\Yii::t('app', 'No')],
                                [
                                        'unselect' => null,
                                        'disabled',
                                        'item' => function($index, $label, $name, $checked, $value) {
                                              $showPanel = $index == 0 ? "true" : "false";
                                              $ischecked = $checked ? 'checked="checked"' : ''; // $value == '1' ? "checked " : '';
                                              $setting = 'class="sy_data_toggle" '
                                                      . 'sy_data_toggle_dest = "divA21"'
                                                      . 'sy_data_toggle_src_type = "radio"'
                                                      . 'sy_data_toggle_visible_value = "' . $showPanel . '"' 
                                                      . 'sy_data_toggle_enabled = "true"';
  
                                              $return = '<label>'
                                                      . ' <input type="radio" disabled name="' . $name . '" value="'. $value .'" ' 
                                                      . $ischecked 
                                                      . ' '
                                                      . $setting
                                                      . '>'
                                                      . ' ' . $label
                                                      . '</input>'
                                                      . '</label>';
                                              return $return;
                                        }                                        
                                ]) ?>
                    </div>
                    <div id="divA21" class="hiddenBlock">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 sy_pad_top_6" style="margin-left: 5px; padding-bottom: 6px;">
                            <?= Yii::t('app/crf', 'If') . ' <u>' . lcfirst(\Yii::t('app', 'Yes')) . '</u>, ' . \Yii::t('app/crf', 'specify the type and quantity of released substance') . ' '; ?>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <ul class="list-inline" style="padding-left: 5px">
                                <li>
                                    <?= ucfirst(\Yii::t('app', 'type')) ?>: 
                                    <?php
                                    if (isset($model->a2_1_t) && trim($model->a2_1_t!=''))
                                    {
                                        $s = $model->a2_1_t;
                                        $class='';
                                    }
                                    else
                                    {
                                        $s = \Yii::t('app', '(not set)');
                                        $class='sy_not_set';
                                    }
                                    ?>
                                    <span class=<?=$class?>><?= $s ?></span>
                                </li>
                                <li>
                                    <?= ucfirst(\Yii::t('app', 'quantity')) ?>: 
                                    <?php
                                        if (isset($model->a2_1_q))
                                        {
                                            $s = $model->a2_1_q;
                                            $s .= ' ' . Units::findOne($model->a2_1_u)->unit ;
                                            $class = '';
                                        }
                                        else
                                        {
                                            $s = \Yii::t('app', '(not set)');
                                            $class='sy_not_set';
                                        }
                                    ?>
                                    <span class=<?=$class?>><?= $s ?></span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </td>
            </tr>
            <tr class="sy_para">
                <td class="sy_crf_para sy_section_h2">A.2.2.</td>
                <td class="sy_crf_para">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <em class="sy_section_h2"><?= \app\models\crf\CrfHelper::SectionsTitle()['A.2.2'] ?></em>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <?= Html::radioList(
                                'rb_a2_2', 
                                $model->a2_2, 
                                ['1' => \Yii::t('app', 'Yes'), '0' =>\Yii::t('app', 'No')],
                                [
                                        'unselect' => null,
                                        'disabled',
                                        'item' => function($index, $label, $name, $checked, $value) {
                                              $showPanel = $index == 0 ? "true" : "false";
                                              $ischecked = $checked ? 'checked="checked"' : ''; // $value == '1' ? "checked " : '';
                                              $setting = 'class="sy_data_toggle" '
                                                      . 'sy_data_toggle_dest = "divA22"'
                                                      . 'sy_data_toggle_src_type = "radio"'
                                                      . 'sy_data_toggle_visible_value = "' . $showPanel . '"' 
                                                      . 'sy_data_toggle_enabled = "true"';
  
                                              $return = '<label>'
                                                      . ' <input type="radio" disabled name="' . $name . '" value="'. $value .'" ' 
                                                      . $ischecked 
                                                      . ' '
                                                      . $setting
                                                      . '>'
                                                      . ' ' . $label
                                                      . '</input>'
                                                      . '</label>';
                                              return $return;
                                        }                                        
                                ]) ?>
                    </div>
                    <div id ="divA22" class='hiddenBlock'>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 sy_pad_top_6">
                            <?= Yii::t('app/crf', 'Describe circumstances') . ':' ?><br/><br/>
                            <?php
                              $s='';
                              $s .= '<div>'
                                  . nl2br(NullValueFormatter::Format($model->a2_2_desc)) 
                                 . '</div>';

                              echo $s;
                            ?>
                        </div>
                    </div>
                </td>
            </tr>
            <tr class="sy_para">
                <td class="sy_crf_para sy_section_h2">A.2.3.</td>
                <td class="sy_crf_para">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <em class="sy_section_h2"><?= CrfHelper::SectionsTitle()['A.2.3'] ?></em>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <?= Html::radioList(
                                'rb_a2_3', 
                                $model->a2_3, 
                                ['1' => \Yii::t('app', 'Yes'), '0' =>\Yii::t('app', 'No')],
                                [
                                        'unselect' => null,
                                        'disabled',
                                        'item' => function($index, $label, $name, $checked, $value) {
                                              $showPanel = $index == 0 ? "true" : "false";
                                              $ischecked = $checked ? 'checked="checked"' : ''; // $value == '1' ? "checked " : '';
                                              $setting = 'class="sy_data_toggle" '
                                                      . 'sy_data_toggle_dest = "divA23"'
                                                      . 'sy_data_toggle_src_type = "radio"'
                                                      . 'sy_data_toggle_visible_value = "' . $showPanel . '"' 
                                                      . 'sy_data_toggle_enabled = "true"';
  
                                              $return = '<label>'
                                                      . ' <input type="radio" disabled name="' . $name . '" value="'. $value .'" ' 
                                                      . $ischecked 
                                                      . ' '
                                                      . $setting
                                                      . '>'
                                                      . ' ' . $label
                                                      . '</input>'
                                                      . '</label>';
                                              return $return;
                                        }                                        
                                ]) ?>
                    </div>
                    <div id="divA23" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 hiddenBlock sy_pad_top_16">
                        <div style="padding-bottom:6px">
                            <?= Yii::t('app/crf', 'If yes, outline the environmental impacts which already have been observed or are likely to result from the incident') . '.' ?><br/>
                        </div>
                        <div class="sy_pad_top_6">
                            <?php
                              $s='';
                              $s .= '<div>'
                                  . nl2br(NullValueFormatter::Format($model->a2_3_desc)) 
                                 . '</div>';

                              echo $s;
                            ?>
                        </div>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    
    
</div>

<?php
$scriptPath = Yii::getAlias('@web');
$this->registerJsFile($scriptPath . '/js/sy_data_toggle.js', ['depends' => [\yii\bootstrap\BootstrapAsset::className()]]);

?>
