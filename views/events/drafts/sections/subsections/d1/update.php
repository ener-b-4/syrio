<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

  use yii\helpers\Html;

  /* @var $this yii\web\View */
  /* @var $model app\models\events\drafts\sections\subsections\D3 */

  $evt = 'D1';
  $this->title = Yii::t('app', 'Section {evt} - Drafting', [
      'evt' => $evt,
  ]);
  
?>
<br />
<div class="d3-update">

    <?php 
	  //include Yii::getAlias('@app') . '\views\shared_parts\message_snippet.php';
	  include Yii::getAlias('@app') . '/views/shared_parts/message_snippet.php';
    ?>

    <div class='text-muted sy_pad_bottom_18'>
        <em>
        <?= \Yii::t('app/messages', 'To leave this page please use the buttons at the bottom the page.') ?>
        </em>
    </div>
    <div class='lead'>
        <?php
            $let = 'D';
        ?>
        <?= $let . '. ' . \app\models\crf\CrfHelper::SectionsTitle()[$let]?>
    </div>
    
    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), [
            'id'=>'btnSubmit',
            'form'=>'w0',
            'onclick' => 'proceed = 1;',
            'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app/commands', 'Cancel'), 
                ['cancel', 'id' => $model->id, 'ref' => $model->ref], 
                [
                    'class' => 'btn btn-warning',
                    'onclick' => 'proceed = 1',
                    'id' => 'btnCancel'
                ]) ?>
    </div>
    
</div>

<?php
$lnk = Yii::$app->getUrlManager()->createAbsoluteUrl(['/events/drafts/sections/subsections/d1/cancel-a', 'id'=>$model->id]);
//echo Html::input('textbox', null, $lnk, ['id'=>'rst', 'class'=>'hiddenBlocks']); 

/* Author: vamanbo   Date: 201507.30   DO NOT Comment the next line because it blows away the unlocking mechanism
* The textfield was there to check that it exists and its value
* To hide it set the class hiddenBlock
* 
* N.B. the $lnk MUST have cancel-a in ALL the subseactions!
*/
echo Html::hiddenInput('cancel_url', $lnk, ['id'=>'rst']);
echo Html::activeHiddenInput($model, 'ref', [
    'form' => 'w0',
]);

$script = <<< JS
$(window).bind('unload', function() {
        if (typeof proceed === 'undefined')
        {
            discard=$("#rst").val();
    
            var i = document.createElement("img");
            i.src = discard;
            document.appendChild(i);
          
            return;
        }
        });
        
$(window).bind('beforeunload', function() {
        if (typeof proceed === 'undefined')
        {        
            _msg = 'Please only use Update and Cancel buttons at the bottom of this page to navigate away!';
            return _msg;
        }
        });

$('button[form="w0"]').click(function()
{
        event.preventDefault();
        event.stopPropagation();
    $('#w0').submit();
});        
   
JS;
$this->registerJs($script, \yii\web\View::POS_LOAD);

?>