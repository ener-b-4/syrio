<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\events\drafts\sections\subsections\D1 */

    $section_name = 'D';
    $subsection_name = '1';
    $title_subsection = $section_name . '.' . $subsection_name . '.';           //'D.1.'

$strip = isset($stripped);
if (!$strip)
{

    
    $this->title = Yii::t('app/crf', 'Section {id}', ['id'=>$title_subsection]);  

  echo $this->render('/shared_parts/_breadcrumbs', [
    'title_subsection' => $title_subsection, 
    'module_name' => 'ADD_SUBSECTION'
  ]);
}
?>
<div class="d3-view col-lg-12">

    <p>
        <?php
        if (!$strip)
        {
            // use this on any view to include the actionmessage logic and widget 
            //include Yii::getAlias('@app') . '\views\shared_parts\message_snippet.php';
            include Yii::getAlias('@app') . '/views/shared_parts/message_snippet.php';
            
           if ($model->sD->status == \app\models\events\EventDeclaration::INCIDENT_REPORT_DRAFT)
            { 
                echo Html::a(Yii::t('app', 'Modify'), ['events/drafts/sections/subsections/d1/update', 'id' => $model->id], ['class' => 'btn btn-primary']);
                echo Html::a(Yii::t('app', 'Finalize {evt}', ['evt'=>$section_name]), ['events/drafts/event-draft/finalize-section', 'id' => $model->sD->eventDraft->id, 's'=>'D'], ['class' => 'btn btn-success pull-right']);
            }
        }
        
      //(start) only display pdf if not stripped = 2
      if (!isset($stripped) || $stripped !== 2) {
      echo Html::a(Yii::t('app', '<i class="fa fa-file-pdf-o"></i> PDF'), 
          ['pdfgenerator/default/index', 'id' => $model->sD->eventDraft->id , 'section' => $section_name.'.'.$subsection_name.'.'], 
      ['class' => 'btn btn-default']); 
      }        
        ?>       

    </p>

    <table>
        <tbody>
            <tr class="sy_para">
                <td class="sy_crf_para sy_section_h1">D.1.</td>
                <td class="sy_crf_para sy_section_h1" style='width:100%;'>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <?= Yii::t('app/crf', 'General information') ?>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="sy_crf_para"></td>
                <td class="sy_crf_para">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <ul class="list-inline sy_padding_clear">
                            <li class="text-info">(a)  <?= $model->attributeLabels()['d1_1'] . ' ' . Yii::t('app/crf', '(if applicable)') ?>: </li>
                            <li><?= isset($model->d1_1) && trim($model->d1_1)!='' ? $model->d1_1:'<span class="sy_not_set">' . \Yii::t('app', '(not set)') . '</span>' ?></li>
                        </ul>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    
</div>
