<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\events\drafts\sections\subsections\D1 */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="d1-form">

    <?php $form = ActiveForm::begin(); ?>

    <table>
        <tbody>
            <tr class="sy_para">
                <td class="sy_crf_para sy_section_h1">D.1.</td>
                <td class="sy_crf_para sy_section_h1" style='width:100%;'>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <?= Yii::t('app/crf', 'General information') ?>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="sy_crf_para"></td>
                <td class="sy_crf_para">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <ul class="list-inline sy_padding_clear">
                            <li class="text-info sy_vert_top sy_pad_top_6">(a)  <?= $model->attributeLabels()['d1_1'] . ' ' . Yii::t('app/crf', '(if applicable)') ?>: </li>
                            <li><?= $form->field($model, 'd1_1')->textInput()->label('',['class'=>'hiddenBlock']) ?></li>
                        </ul>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>

    <?php ActiveForm::end(); ?>

</div>
