<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

  use yii\helpers\Html;

  /* @var $this yii\web\View */
  /* @var $model app\models\events\drafts\sections\subsections\D1 */

?>
<div class="d3-view col-lg-12">
  
    <table>
        <tbody>
            <tr class="sy_para">
                <td class="sy_crf_para"><strong>D.1.</strong></td>
                <td class="sy_crf_para" style='width:100%;'>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <strong><?= Yii::t('app/crf', 'General information') ?></strong>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="sy_crf_para"></td>
                <td class="sy_crf_para">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <ul class="list-inline sy_padding_clear">
                            <li>(a)  <?= $model->attributeLabels()['d1_1'] . ' ' . Yii::t('app/crf', '(if applicable)') ?>: </li>
                            <li><?= isset($model->d1_1) && trim($model->d1_1)!='' ? $model->d1_1:'<span class="sy_not_set">' . \Yii::t('app', '(not set)') . '</span>' ?></li>
                        </ul>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    
</div>
