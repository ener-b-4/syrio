<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\DateTimePicker;

//use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\events\EventDeclaration */
/* @var $form yii\widgets\ActiveForm */
/* @var $step the step of the current request */
/* @var $installations the available installations depending on the type of user (available in step 2) */

?>

<?php

    if (!isset($step))
    {
        $step2_css = "display: none";
        $step3_css = "display: none";
        
        $txt = 'Next';
    }
    elseif ($step == 2)
    {
        $step2_css = "display: inherit";
        $step3_css = "display: none";

        $txt = 'Next';
    }
    elseif ($step == 3)
    {
        $step2_css = "display: inherit";
        $step3_css = "display: inherit";

        $txt = 'Create';
    }

    $taken_from_text = '(' . \Yii::t('app', 'taken from user credentials') . ')';
    
?>

<div class="event-declaration-form">

    <?php $form = ActiveForm::begin([
        'enableAjaxValidation' => 'true',
    ]); ?>

    <div class="panel panel-primary">
        <div class="panel-heading">
            <?= \Yii::t('app/crf', 'Event date/time') ?>
        </div>
        <div class="panel-body">
            <?php
            echo $form->field($model, 'event_date_time')->widget(DateTimePicker::classname(), [
                    'options' => ['placeholder' => \Yii::t('app/crf', 'Event date and time') . '...'],
                                    'pluginOptions' => [
                                        'format' => 'yyyy-mm-dd hh:ii:ss',
                                        'todayHighlight' => false,
                                        'autoclose' => true,
                                        'endDate' => \date("Y-m-d H:i:s"),
                    ]
            ]);            
            ?>
            
            <?php //$form->field($model, 'event_date_time')->textInput(['placeholder'=>'yyyy/MM/dd hh:mm:ss'])->hint('Local date and time at the incident location') ?>
            <?php // $form->field($model,'event_date_time')->widget(DatePicker::className(),['clientOptions' => ['defaultDate' => '2014-01-01']]) ?>

        </div>
    </div>
    
    
    <div class="panel panel-primary" id='step2' style="<?= $step2_css ?>">
        <div class="panel-heading">
            <?= \Yii::t('app/crf', 'Details of the location and of the person reporting the event') ?>
        </div>
        <div class="panel-body">
            <div class="panel panel-default">
                <div class="panel-body">
                    <?= $form->field($model, 'operator')->textInput(['maxlength' => 128, 'disabled' => 'true'])->hint($taken_from_text) ?>

                    <?= isset($installations) ? $form->field($model, 'installation_id')->dropDownList($installations, ['prompt' => '-- select installation --'])
                        ->hint(\Yii::t('app/crf', 'Please state the name and/or type of the installation'))
                            ->label(Yii::t('app/crf','Installation name/type')): '' ?>
                    
                    <?= $form->field($model, 'field_name')->textInput(['maxlength' => 128, 'placeholder'=> strtolower(\Yii::t('app/crf', 'Field name/code')) ])->hint(\Yii::t('app/crf', 'The name or code of the installation operation field (if relevant)')) ?>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-body">
                    <?= $form->field($model, 'raporteur_name')->textInput(['maxlength' => 64, 'disabled' => 'true'])->hint($taken_from_text) ?>

                    <?= $form->field($model, 'raporteur_role')->textInput(['maxlength' => 128, 'disabled' => 'true'])->hint($taken_from_text) ?>
                </div>
            </div>                
        </div>
    </div>

    <div class="panel panel-primary"  style="<?= $step2_css ?>">
        <div class="panel-heading">
            <?= \Yii::t('app/crf', 'Contact details') ?>            
        </div>
        <div class="panel-body">
            <?= $form->field($model, 'contact_tel')->textInput(['maxlength' => 32, 'disabled'=>'true']) ?>
        
            <?= $form->field($model, 'contact_email')->textInput(['maxlength' => 128, 'disabled'=>'true']) ?>
        </div>
    </div>

    
    <div class="panel panel-info" id='step3'  style="<?= $step3_css ?>">
        <div class="panel-heading">
            <?= \Yii::t('app/crf', 'Additional compulsory information') ?>                        
        </div>
        <div class="panel-body">
            <p>
                <?= \Yii::t('app/crf', 'The information below is required for SyRIO only.') ?>                        
            </p>
            <p>
                <?= \Yii::t('app/crf', 'None of the information will be part of the CRF report.') ?>                                        
            </p>
        
            <?= $form->field($model, 'event_name')->textInput(['maxlength' => 64])->hint(\Yii::t('app', 'Please provide a unique name for internal reference of this event')) ?>
        </div>
    </div>
    
    

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', $txt) : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(\Yii::t('app/commands', 'Cancel'), ['index'], [
            'class' => 'btn btn-warning'
        ]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
