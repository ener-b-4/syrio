<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\events\EventDeclaration */

$this->title = 'Registration confirmed'; //$model->id;
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Event Declarations'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<!--<div class="event-declaration-view">-->

<div class="container">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default"
                <div class="panel-body">
                    <div class="bg-success" style="padding: 10px;">
                        The event <strong><?=Html::encode($model->event_name)?></strong> has been successfully registered.
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <p>
                <?= Html::a(Yii::t('app', 'Review'), ['view', 'id' => 7], ['class' => 'btn btn-default']) ?>
                <?= Html::a(Yii::t('app', 'Start drafting'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            </p>
        </div>
    </div>
    
</div>
