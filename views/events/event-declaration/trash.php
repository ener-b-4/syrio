<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use kartik\grid\GridView;
use app\components\helpers\DataGrid\CustomGridView;

use app\widgets\ActionMessageWidget;
use app\models\common\ActionMessage;
use app\components\helpers\DataGrid\funDataGridClass;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\events\EventDeclarationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $canRegister bool weather or not the current user can register a new incident */

$this->title = Yii::t('app', 'Deleted events');

$breadcrumbs = [
    [
        'label' => Yii::t('app', 'Registered events'),
        'url' => Url::to(['/events/event-declaration/']),
        //'template' =>"<li>{link}</li>
    ],
    [
        'label' => Yii::t('app', 'Trash'),
        //'url' => Url::to(['/admin/users/user']),
        //'template' =>"<li>{link}</li>
    ],
];
$this->params['breadcrumbs'] = $breadcrumbs;

?>
<div class="event-declaration-index">
    <?php
        //check if flash is set
        if (Yii::$app->session->hasFlash('msg'))
        {
            $msg = Yii::$app->session->getFlash('msg');
            if (strpos($msg,'[err]'))
            {
                //failure
                //get rid of the [s]
                $msg =  str_replace('[err]', '', $msg);
                $msg =  str_replace('\n', '<br/>', $msg);
                
                echo "<div class='alert alert-danger'>$msg</div>";
            }
            else
            {
                //success
                $msg =  str_replace('\n', '<br/>', $msg);
                echo "<div class='alert alert-success'>$msg</div>";
            }
        }
    ?>
    
    <?php
    
    $session_msg = Yii::$app->session->get('finished_action_result');     //$session_msg is of type ActionMessage
    if (isset($session_msg))
    {
        echo ActionMessageWidget::widget(['actionMessage' => $session_msg,]);
        //disable the next line if you want the message to persist
        unset(Yii::$app->session['finished_action_result']);
    }
    
    ?>
    
    <?php
    /* grid export */
    /*
    =====================================================================================
    */
    $title_h_l =  'SyRIO ' . \Yii::t('app', 'Export'); //'Custom L';
    $title_h_c =  $this->title; //'Custom C';
    $title_h_r = 'Generated' . ': ' . date("D, d-M-Y g:i a T");

    $ourPdfHeader = funDataGridClass::getOurPdfHeader($title_h_l, $title_h_c, $title_h_r);

    $title_f = '';
    $ourPdfFooter = funDataGridClass::getOurPdfFooter($title_f);
    $exportConfig = funDataGridClass::getExportConfiguration($ourPdfHeader, $ourPdfFooter);
    /*
    =====================================================================================
    */    
    ?>
    
    <h1><?= Html::encode($this->title) ?></h1>
    <?php //echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php
    
        include 'index_code/grid_columns.php';
 
        $trashUrl = Yii::$app->urlManager->createAbsoluteUrl(['events/event-declaration/trash']);
        
        /* set-up the beforeHeader section */
        if (\Yii::$app->user->identity->isOperatorUser || \Yii::$app->user->can('sys_admin'))
        {
            $beforeHeaderColumns = [
                ['content'=>'Event', 'options'=>['colspan'=>5, 'class'=>'text-center warning']], 
                ['content'=>'Reporting', 'options'=>['colspan'=>2, 'class'=>'text-center warning']], 
            ];
        }
        else
        {
            $beforeHeaderColumns = [
                ['content'=>'Event', 'options'=>['colspan'=>4, 'class'=>'text-center warning']], 
                ['content'=>'Reporting', 'options'=>['colspan'=>2, 'class'=>'text-center warning']], 
            ];
        }

        echo CustomGridView::widget([
            'dataProvider'=> $dataProvider,
            'filterModel' => $searchModel,
            'rowOptions' => function($model) {
                return $model->del_status == app\models\events\EventDeclaration::STATUS_DELETED ? ['class' => 'danger'] : [];
            },
            'pjax' => false,
            'beforeHeader'=>[
                [
                    'columns'=>$beforeHeaderColumns,
                    'options'=>['class'=>'skip-export'] // remove this row from export
                ]
            ],
            'toolbar' =>  [
                ['content'=>
                    $toolbar_content
//                    Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['grid-demo'], ['data-pjax'=>0, 'class' => 'btn btn-default', 'title'=>Yii::t('app', 'Reset Grid')])
                ],
                '{export}',
                '{toggleData}',
            ],
            'columns' => $columns,
            'responsive'=>true,
            //'responsiveWrap' => true, //false by default
            'hover'=>true,
            'export'=>false,
                    
            //'containerOptions' => [
            //    'style' => 'overflow-x: scroll;'
            //],                    
            'panel' => [
                    'type' => GridView::TYPE_DANGER,
                    'heading' => '<i class="glyphicon glyphicon-th-list"></i> ' . Yii::t('app', 'Events list (trash)'),
                    'before' => ''
                ],
                    
              // set export properties
              'export' => [
                'icon' => 'share-square-o',
                'encoding' => 'utf-8',
                'fontAwesome' => true,
                'target' => CustomGridView::TARGET_SELF,   //target: string, the target for submitting the export form, which will trigger the download of the exported file. 
                'showConfirmAlert' => TRUE,
                'header' => '<li role="presentation" class="dropdown-header">' . \Yii::t('app', 'Grid Export') . '</li>'
                //'header' => 'Export Page Data',
              ],
              'exportConfig'     => $exportConfig,
        ]);
 
    ?>
</div>
