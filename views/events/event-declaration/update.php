<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\events\EventDeclaration */
/* @var $step the step of the current request */
/* @var $installations the available installations depending on the type of user (available in step 2) */

$this->title = ucfirst(Yii::t('app', 'modify {evt}', [
    'evt' => $model->event_name,
]));
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Events management'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->event_name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Modify');

?>
<div class="event-declaration-update">

    
    <?php
        //check if flash is set
        if (Yii::$app->session->hasFlash('msg'))
        {
            $msg = Yii::$app->session->getFlash('msg');
            if (strpos($msg,'[err]'))
            {
                //failure
                //get rid of the [s]
                $msg =  str_replace('[err]', '', $msg);
                $msg =  str_replace('\n', '<br/>', $msg);
                
                echo "<div class='alert alert-danger'>$msg</div>";
            }
            else
            {
                //success
                $msg =  str_replace('\n', '<br/>', $msg);
                echo "<div class='alert alert-success'>$msg</div>";
            }
        }
    ?>    
    
    
    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'step' => isset($step) ? $step : null,
        'installations' => isset($installations) ? $installations : null,
    ]) ?>

</div>

<?php
$script = <<< JS
$('button[form="w0"]').click(function()
{
    $('#w0').submit();
});        
        
JS;
//$this->registerJs($script, \yii\web\View::POS_LOAD);
?>