<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use \app\models\events\EventDeclaration;

/**
 * Go thru the $model->drafts and populate the arrays depending on the $draft->status
 * 
 * @var $model app\models\events\EventDeclaration
 * 
 * @author bogdanV
 */

//initialize the table rows arrays
$open_drafts = [];
$finalized_drafts = [];
$signed_drafts = [];
$reported_drafts = [];

foreach ($model->drafts as $draft)
{
    switch ($draft->status)
    {
        case EventDeclaration::INCIDENT_REPORT_DRAFT: 
            $open_drafts[] = $draft;
            break;
        case EventDeclaration::INCIDENT_REPORT_FINALIZED:
            $finalized_drafts[] = $draft;
            break;
        case EventDeclaration::INCIDENT_REPORT_SIGNED:
            $signed_drafts[] = $draft;
            break;
        case EventDeclaration::INCIDENT_REPORT_REPORTED:
            $reported_drafts[] = $draft;
            break;
    }
}