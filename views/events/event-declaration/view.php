<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\events\EventDeclaration */
/* @var $stripped bool */

$this->title = '\'' . $model->event_name . '\'';

if (!isset($stripped) || !$stripped)
{
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Registered events'), 'url' => ['index', 'evtID'=>$model->id]];
    $this->params['breadcrumbs'][] = $this->title;
}


//$userFlag is used for rednering / not rendering UI
Yii::$app->user->can('op_raporteur') || Yii::$app->user->can('inst_rapporteur') ? $userFlag = 1 : $userFlag=0;

?>
<div class="event-declaration-view">

    <h1><?= Html::encode($this->title) ?><small class="text-muted">   <em><?= Yii::t('app', 'Event Declaration') ?></em></small></h1>
    
    <p>
        <?= Yii::t('app', 'This corresponds to {evt1} and {evt2} sections of the Common Reporting Format (EU IR 1112/2014)', [
            'evt1' => '<strong>' . Yii::t('app/crf', 'Event date and time') . '</strong>', 
            'evt2' => '<strong>' . Yii::t('app/crf', 'Details of the location and of the person reporting the event') . '</strong>'
        ]) ?>
    </p>
    
        <?php
            $dt = new DateTime($model->event_date_time);
            
            if ($userFlag && !isset($stripped))
            {
//                $url = Yii::$app->getUrlManager()->createUrl([
//                       'events/drafts/event-draft/index?evtID=31',
//                        'id' => $model->id,
//                ]);
//                
//                $url = yii\helpers\Url::toRoute(['events/drafts/event-draft/index', 'evtID' => '31', 'id' => $model->id]);
                $url = yii\helpers\Url::toRoute(['events/drafts/event-draft/index', 'evtID' => $model->id]);
                
                echo Html::a(Yii::t('app', 'Modify'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']);
                echo Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                        'method' => 'post',
                    ]]);
                echo "<a href= '$url' class='btn btn-success pull-right', style='margin-left:24px; '>Reporting drafts <span class='badge'>" . count($model->drafts) . "</span></a>";
            }
        ?>
        
    </p>


    <!-- render the event details -->
    <h3><?= Yii::t('app/crf', 'Event date and time') ?></h3>
    (a) <?= Yii::t('app/crf', 'Event date') ?>: <?= date_format($dt, 'd-m-Y') ?>
    <br/>
    (b) <?= Yii::t('app/crf', 'Event time') ?>: <?= date_format($dt, 'H:i:s') ?>


    <h3><?= Yii::t('app/crf', 'Details of the location and of the person reporting the event') ?></h3>
    
    <table class='sy_simple-table'>
        <tr>
            <td><?= $model->attributeLabels()['operator'] ?></td>
            <td><?= $model->operator ?></td>
        </tr>
        <tr>
            <td><?= $model->attributeLabels()['installation_name_type'] ?></td>
            <td><?= $model->installation_name_type ?></td>
        </tr>
        <tr>
            <td><?= $model->attributeLabels()['field_name'] . ' ' . \Yii::t('app/crf', '(if relevant)') ?></td>
            <td><?= $model->field_name ?></td>
        </tr>
        <tr style='min-height: 32px; max-height: 32px; height: 32px;'>
            <td colspan='2'></td>
        </tr>
        <tr>
            <td><?= $model->attributeLabels()['raporteur_name'] ?></td>
            <td><?= $model->raporteur_name ?></td>
        </tr>
        <tr>
            <td><?= $model->attributeLabels()['raporteur_role'] ?></td>
            <td><?= $model->raporteur_role ?></td>
        </tr>
        <tr style='min-height: 32px; max-height: 32px; height: 32px;'>
            <td colspan='2'></td>
        </tr>
        <tr>
            <td colspan='2'><strong><?= Yii::t('app/crf','Contact details') ?></strong></td>
        </tr>
        <tr>
            <td><?= $model->attributeLabels()['contact_tel'] ?></td>
            <td><?= $model->contact_tel ?></td>
        </tr>
        <tr>
            <td><?= $model->attributeLabels()['contact_email'] ?></td>
            <td><?= $model->contact_email ?></td>
        </tr>
    </table>
    
</div>
