<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\events\EventDeclaration */
/* @var $step the step of the current request */
/* @var $installations the available installations depending on the type of user (available in step 2) */

$this->title = Yii::t('app', 'Register new event', []);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Events management'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-declaration-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php
        //check if flash is set
        if (Yii::$app->session->hasFlash('msg'))
        {
            $msg = Yii::$app->session->getFlash('msg');
            if (strpos($msg,'[err]'))
            {
                //failure
                //get rid of the [s]
                $msg =  str_replace('[err]', '', $msg);
                $msg =  str_replace('\n', '<br/>', $msg);
                
                echo "<div class='alert alert-danger'>$msg</div>";
            }
            else
            {
                //success
                $msg =  str_replace('\n', '<br/>', $msg);
                echo "<div class='alert alert-success'>$msg</div>";
            }
        }
    ?>    
    <?= $this->render('_form', [
        'model' => $model,
        'step' => isset($step) ? $step : null,
        'installations' => isset($installations) ? $installations : null,
    ]) ?>

</div>

<?php
if (isset($step)) {
    $stp = 'step'.$step;
} else {
    $stp = 'step1';
}

$js = <<<JS
        //scroll page to bottom
        //$(window).load(function() {

    //get the top of step2 block
    if (!$("#$stp").position()) {
        var top = 0;
    } else {
        var top = $("#$stp").position().top;
    }
    
    $("html, body").animate({ scrollTop: top - $("#w1").height() }, 1000);        
    //$('html, body').scrollTop( top - $("#w1").height() );
        //});
JS
?>

<?php
$this->registerJs($js, \yii\web\View::POS_LOAD);
?>