<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use app\models\events\EventDeclaration;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\events\EventDeclaration */

// check if 'msg' flag exists
$msg = \Yii::$app->session->getFlash('msg');
if (isset($msg))
{
    echo '<div class="alert alert-info">' . $msg . '</div>';
}


$drafts = $model->getDrafts();
//echo $drafts->count('*');

$this->title = Yii::t('app', '\'{modelClass}\' reports', [
    'modelClass' => $model->event_name,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Events management'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->event_name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Reports (drafts)');
?>

<ul>
    
<?php
        //$userFlag is used for rednering / not rendering UI
        Yii::$app->user->can('op_raporteur') ? $userFlag = 1 : $userFlag=0;
        
        $p = Yii::getAlias('@app') . '\\views\events\event-declaration\view-parts\_openDrafts.php';
        //group the drafts based on status
        require $p;
        
        //paths
        $pathCreateDraft = Yii::$app->urlManager->createUrl(['events/drafts/event-draft/create','evtId'=>$model->id]);
        //$path = Yii::$app->urlManager->createUrl(['events/drafts/event-draft/view','id'=>$draft->id]);
        //$pathSignDraft = Yii::$app->urlManager->createUrl(['events/drafts/event-draft/finalize','id'=>$draft->id]);
        //$pathSignDraft = 'events/drafts/event-draft/finalize';
        $pathReopenDraft = 'events/drafts/event-draft/reopen';
        $pathSignDraft = 'events/drafts/event-draft/finalize';
        $pathDel = 'events/drafts/event-draft/delete';
        $pathUnlockDraft = 'events/drafts/event-draft/unlock-all'
        

        
?>

</ul>

<div class="event-declaration-index">
    <h1><?= Html::encode('\'' . $model->event_name . '\' reports') ?></h1>

    <div>
            <?= Html::a(Yii::t('app', 'Create new {modelClass}', [
            'modelClass' => 'Event Draft',
            ]), $pathCreateDraft, ['class' => 'btn btn-success']) ?>     
    </div>
            
                <?php
            if ($drafts->count()==0)
            {
                $s = '<div class="well">';
                $s .= ' <div class="alert alert-info" role="alert">';
                $s .= '     You have no drafts available for this event';
                $s .= ' </div>';
                $s .= '</div>';
                echo $s;
            }
            else
            {
            ?>
    <table class="table table-condensed">
        <thead>
        <th>Status</th>
        <th>Description</th>
        <th></th>
        </thead>
        <tbody>
            <?php
            foreach ($model->drafts as $draft)
            {
                echo '<tr>';
                switch ($draft->status)
                {
                    case EventDeclaration::INCIDENT_REPORT_DRAFT:
                        $class='sy_draftt';
                        $display='draft';
                        break;
                    case EventDeclaration::INCIDENT_REPORT_FINALIZED:
                        $class='sy_finalized';
                        $display='finalized';
                        break;
                    case EventDeclaration::INCIDENT_REPORT_SIGNED:
                        $class='sy_signed';
                        $display='signed';
                        break;
                    case EventDeclaration::INCIDENT_REPORT_REPORTED:
                        $class='sy_submitted';
                        $display='reported';
                        break;
                    case EventDeclaration::INCIDENT_REPORT_LOCKED:
                        $class='sy_locked sy_drafting';
                        $display='drafting';
                        break;
                    default:
                        $class='sy_draftt';
                        $display = 'draftt';
                        break;
                }
                
                /*
                echo '<pre>';
                echo var_dump($draft);
                echo '</pre>';
                die();
                 * 
                 */

                echo '<td class="sy_drafts_list ' . $class . '"></td>';
                
                echo '<td class="sy_drafts_list sy_drafts_list_desc">';
                echo $draft->session_desc;
                echo '</td>';
                
                echo '<td>';
                echo buildControls($draft, $pathDel, $pathReopenDraft);
                //echo $s;
                echo '</td>';
                echo '</tr>';
            }
            ?>
        </tbody>
    </table>
            <?php }?>
</div>



<?php

    function buildControls($draft, $pathDel, $pathReopenDraft)
    {
        //echo 'draft status:' . $draft->status;
        if ($draft->status == EventDeclaration::INCIDENT_REPORT_DRAFT)
        {
            //everybody can continue drafting
            $s =  Html::a(Yii::t('app', '<span class="glyphicon glyphicon-pencil"></span> <small>Draft</small>'), 
                    ['events/drafts/event-draft/view','id'=>$draft->id], 
                    ['class' => 'btn sy_padding_clear']);
            
            //everybody can delete it
            $s .= Html::a(Yii::t('app', '<span class="glyphicon glyphicon-trash sy_alert_color" style="padding-left:6px;"></span> <small>Delete</small>'), [$pathDel, 'id' => $draft->id], [
                'class' => 'btn sy_padding_clear',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]);                    

        }
        else if ($draft->status == EventDeclaration::INCIDENT_REPORT_FINALIZED)
        {
            //everybody can review it
            $s =  Html::a(Yii::t('app', '<span class="glyphicon glyphicon-eye-open"></span> <small>Details</small>'), 
                    ['events/drafts/event-draft/view','id'=>$draft->id], 
                    ['class' => 'btn sy_padding_clear']);

            //only the raporteur can reopen-it, review and sign
            if (Yii::$app->user->can('op_raporteur'))
            {
                $s .= Html::a(Yii::t('app', '<span class="glyphicon glyphicon-hand-down text-warning" style="padding-left:6px;"></span> <small>Re-open</small>'), [$pathReopenDraft, 'id' => $draft->id], [
                    'class' => 'btn sy_padding_clear',
                    'data' => [
                        'confirm' => Yii::t('app', 'Are you sure you want to re-open this item?'),
                    ],
                ]);
                
                $pathSignDraft = 'events/drafts/event-draft/sign';
                //glyphicon-check
                $s .= Html::a(Yii::t('app', '<span class="glyphicon glyphicon-check text-success" style="padding-left:6px;"></span> <small>Sign</small>'), [$pathSignDraft, 'id' => $draft->id], [
                    'class' => 'btn sy_padding_clear',
                ]);
            }
        }
        else if ($draft->status == EventDeclaration::INCIDENT_REPORT_SIGNED)
        {
            //everybody can review it
            $s =  Html::a(Yii::t('app', '<span class="glyphicon glyphicon-eye-open"></span> <small>Details</small>'), 
                    ['events/drafts/event-draft/view','id'=>$draft->id], 
                    ['class' => 'btn sy_padding_clear']);

            //only the raporteur can submit
            if (Yii::$app->user->can('op_raporteur'))
            {   
                //glyphicon-han-down
                $s .= Html::a(Yii::t('app', '<span class="glyphicon glyphicon-hand-down text-warning" style="padding-left:6px;"></span> <small>Re-open</small>'), [$pathReopenDraft, 'id' => $draft->id], [
                    'class' => 'btn sy_padding_clear',
                    'data' => [
                        'confirm' => Yii::t('app', 'Are you sure you want to re-open this item?'),
                    ],
                ]);

                //glyphicon-send
                $pathSubmitDraft = 'events/drafts/event-draft/submit-confirmation';
                $s .= Html::a(Yii::t('app', '<span class="glyphicon glyphicon-send text-success" style="padding-left:6px;"></span> <small>Submit to CA</small>'), [$pathSubmitDraft, 'id' => $draft->id], [
                    'class' => 'btn sy_padding_clear',

                ]);
            }
        }
        else if ($draft->status > EventDeclaration::INCIDENT_REPORT_SIGNED)
        {
            //everybody can review it
            $s =  Html::a(Yii::t('app', '<span class="glyphicon glyphicon-eye-open"></span> <small>View</small>'), 
                    ['events/drafts/event-draft/review','id'=>$draft->id], 
                    ['class' => 'btn sy_padding_clear']);
        }
        else if ($draft->status == EventDeclaration::INCIDENT_REPORT_LOCKED)
        {
            //everybody can continue drafting
            $s =  Html::a(Yii::t('app', '<span class="glyphicon glyphicon-pencil"></span> <small>Draft</small>'), 
                    ['events/drafts/event-draft/view','id'=>$draft->id], 
                    ['class' => 'btn sy_padding_clear']);
            
            //glyphicon-han-down
            $s .= Html::a(Yii::t('app', '<span class="glyphicon glyphicon-lock text-success" style="padding-left:6px;"></span> <small>Unlock all</small>'), [$pathReopenDraft, 'id' => $draft->id], [
                'class' => 'btn sy_padding_clear',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to force unlocking this item?'),
                ],
            ]);
        }

        
        return $s;
    }


    /*
     * @var $model app\models\events\EventDeclaration
     * @returns $arr[]
     */
    function getDetails($model)
    {
        //get user's details anf fill in the model
        if (isset($definitionArr)) unset($definitionArr);
        $definitionArr=[];

        $user = $model->createdBy;
        if (is_object($user))
        {
            $definitionArr['creator_name'] = $user->name;
        }

        if (isset($model->created_at))
        {
            $dt = new DateTime($model->created_at);
            $definitionArr['created_at']=date_format($dt, 'Y-m-d H:i:s');
        }

        $user=$model->modifiedBy;
        if (is_object($user))
        {
            $definitionArr['modifier_name'] = $user->name;
        }

        if (isset($model->modified_at))
        {
            $dt = new DateTime($model->modified_at);
            $definitionArr['modified_at']=date_format($dt, 'Y-m-d H:i:s');
        }
        return $definitionArr;
    }

?>
 
