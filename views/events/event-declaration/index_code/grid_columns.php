<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use yii\helpers\Html;
use app\models\events\EventDeclaration;

/**
 * This is where the settings for the kartik grid_columns is done
 *
 * @author vamanbo
 */


/*
 * 
 * ACTION COLUMNS
 * 
 * 
 */    

$actionColumn = [
    'class' => '\kartik\grid\ActionColumn',
    'hiddenFromExport' => true,
    'header' => ucfirst(Yii::t('app', 'actions')),
    'vAlign' => 'top',    
    'buttons' => [
        'delete' => function ($url, $model, $key) {
                /* @var $model \app\models\events\EventDeclaration */
                
                if (!Yii::$app->user->can('incident_delete', ['event'=>$model]) || $model->del_status != \app\models\events\EventDeclaration::STATUS_ACTIVE)
                {
                    return '';
                }

                $msg = Yii::t('app', 'Are you sure you want to delete {data}?', [
                    'data'=>$model->event_name]);
                $msg .= ' ';
                $msg .= Yii::t('app', 'Operation CAN be undone.');
                return $model->status !== 0 ? 
                        Html::a('<i class="glyphicon glyphicon-trash"></i>', $url, [
                            'title' => Yii::t('app', 'Remove'),
                            'data-confirm' => $msg,
                            'data-method' => 'POST']) : '';
            },
        'purge' => function ($url, $model, $key) {
                /* @var $model \app\models\events\EventDeclaration */

                //return $model->del_status;
                
                if (!Yii::$app->user->can('incident_purge', ['event'=>$model]) || $model->del_status != \app\models\events\EventDeclaration::STATUS_DELETED)
                {
                    return '';
                }
                $msg = Yii::t('app', 'Are you sure you want to PERMANENTLY delete {data}?', [
                    'data'=>$model->event_name]);
                $msg .= ' ';
                $msg .= Yii::t('app', 'Operation CAN NOT be undone.');
                return $model->del_status == 0 ? 
                        Html::a('<i class="glyphicon glyphicon-remove"></i>', $url, [
                            'title' => Yii::t('app/commands', 'Purge'),
                            'data-confirm' => $msg,
                            'data-method' => 'POST']) : '';
            },
        'reactivate' => function ($url, $model, $key) {
                /* @var $model \app\models\events\EventDeclaration */
                
                if (!Yii::$app->user->can('incident_delete', ['event'=>$model]) || $model->del_status != \app\models\events\EventDeclaration::STATUS_DELETED)
                {
                    return '';
                }
                
                return $model->del_status == 0 ? 
                        Html::a('<i class="glyphicon glyphicon-repeat"></i>', $url, [
                            'title' => Yii::t('app', 'Undo delete'), 
                            'data-method'=>'POST']) : '';
            },
                    
        'view' => function ($url, $model, $key) {
                if (!Yii::$app->user->can('incident_view', ['event'=>$model]))
                {
                    return '';
                }
                
                if (Yii::$app->user->can('incident_update', ['event'=>$model]))
                {
                    /* put the view declaration normal */
                    return Html::a('<i class="glyphicon glyphicon-eye-open"></i>', $url, ['title' => Yii::t('app', 'View details')]);
                }
            },
        'update' => function ($url, $model, $key) {
                /* @var $model \app\models\events\EventDeclaration */
                
                if (!Yii::$app->user->can('incident_update', ['event'=>$model]) || $model->del_status == \app\models\events\EventDeclaration::STATUS_DELETED)
                {
                    return '';
                }

                return Html::a('<i class="glyphicon glyphicon-pencil"></i>', $url, ['title' => Yii::t('app', 'Edit')]);
            },
        'view-cpf' => function ($url, $model, $key) {
                /* @var $model \app\models\events\EventDeclaration */

                if (!Yii::$app->user->can('incident_view', ['event'=>$model]))
                {
                    return '';
                } else {
                    /* put the view as cpf */
                    $report = $model->report;
                    if (isset($report))
                    {
                        $url = Yii::$app->urlManager->createAbsoluteUrl(['events/drafts/event-draft/review', 'id'=>$model->report->id]);
                        return Html::a('<i class="fa fa-file-text"></i>', $url, ['title' => Yii::t('app', 'View report in CRF')]);
                    }
                }
                
            },
    ],
    'template' => '{view-cpf} {view} {update}  {reactivate} {delete} {purge}',
];

$actionColumnDrafts = [
  
    'class' => '\kartik\grid\ActionColumn',
    'hiddenFromExport' => true,
    'header' => ucfirst(Yii::t('app', 'drafts')),
    'vAlign' => 'top',

    'buttons' => [
            'view-drafts' => function ($url, $model, $key) {
                /* @var $model \app\models\events\EventDeclaration */

                if (!Yii::$app->user->can('incident_view', ['event'=>$model]))
                {
                    return '';
                }

                $s = '<span class="badge">'
                . $model->numberOfDrafts
                . '</span>';
                
                $url = \yii\helpers\Url::to(['events/drafts/event-draft/index', 'evtID' => $model->id]);
                return Html::a($s. ' <i class="glyphicon glyphicon-th"></i>', $url, ['title' => Yii::t('app', 'Display the report/drafts section')]);
            }
        ],
    'template' => '{view-drafts}',
];

$columns = [
    [
        'class' => kartik\grid\SerialColumn::className(),
        'hidden' => true,
        'hiddenFromExport' => FALSE
    ],
    
    [
        'class' => \app\utilities\RegModDataColumn::className(),
        'hiddenFromExport' => true,
    ],

    [
        'attribute' => 'event_name_ext',
        'class' => '\kartik\grid\DataColumn',        
        //'attribute' => 'event_date_time',
        'label' => Yii::t('app', 'Name'),
        'filterInputOptions' => ['placeholder'=>\Yii::t('app', 'incident name'), 'class'=>'form-control'],
        
        
        'content' => function ($model, $key, $index, $column) {
                    /* @var $model \app\models\events\EventDeclaration */
                    return '<b>' . $model->event_name . '</b>';
        }
    ],
    [
        /* reporting users */
        'class' => '\kartik\grid\DataColumn',        
        'attribute' => 'event_date_time',
        'format' => ['date', 'php:D, d M Y H:i:s'],
        'label' => Yii::t('app', 'Date/Time'),
        
          'filterType' => \yii\widgets\MaskedInput::className(),
          'filterWidgetOptions' => [
              'mask'=>'(9999)|*-(99)|*-(99)|*',
              //'mask'=>'(9999)|*-(99)|*-(99)|* (99)|*:(99)|*:(99)|*',
              'definitions'=>[
                  '*'=>[
                      'validator'=>'\*',
                      'cardinality'=>1,
                  ]
              ],
          ],
        'filterInputOptions' => ['placeholder'=>'Y-m-d', 'class'=>'form-control'],
    ],
            
    [
        'class' => app\utilities\EventTypesDataColumn::className(),
        'forExport' => true,
        'hiddenFromExport' => false,
        
        'attribute' => 'event_types',
        'label' => \Yii::t('app', 'Classification'),
          'filterType' => \yii\widgets\MaskedInput::className(),
          'filterWidgetOptions' => [
              'mask'=>'a[\,a][\,a][\,a][\,a][\,a][\,a][\,a][\,a][\,a]',
              //'mask'=>'(9999)|*-(99)|*-(99)|* (99)|*:(99)|*:(99)|*',
              'definitions'=>[
                  ','=>[
                      'validator'=>'\,',
                      'cardinality'=>1,
                  ]
              ],
          ],
        'filterInputOptions' => ['placeholder'=>\Yii::t('app', 'comma-delimited letters (a to j)'), 'class'=>'form-control'],
        
    ],
    
];

\Yii::$app->user->identity->isOperatorUser || \Yii::$app->user->can('sys_admin') ?
        $columns = ArrayHelper::merge($columns, [
            [
                'attribute' => 'installation',
                'hiddenFromExport' => true,
                
                'label' => ucfirst(Yii::t('app', 'installation')) . '/' . ucfirst(Yii::t('app', 'type')) . '/' . ucfirst(Yii::t('app', 'field')),
                'content' => function ($model, $key, $index, $column) {
                    
                    if (!isset($model->installation))
                    {
                        $inst_name = '(not set)';
                        $inst_type = '(not set)';
                        $url = '';
                    }
                    else
                    {
                        $inst_name = $model->installation->name;
                        $inst_type = $model->installation->type;
                        $url = \Yii::$app->urlManager->createAbsoluteUrl(['management/installations/view', 'id'=>$model->installation_id, 'operator_id'=>$model->operator_id]);
                        
                    }
                    
                    $field = isset($model->field_name) ? Html::encode($model->field_name) : '';
                    
                    return implode('/', [
                        '<b>'. Html::a($inst_name, $url) . '</b>',
                        Html::encode($inst_type),
                        $field
                    ]);
                },                                               
                        
                'filterInputOptions'=>['placeholder'=>Yii::t('app', 'inst/type/field'), 'class'=>'form-control'],
            ], 
        ]) : 
        $columns = ArrayHelper::merge($columns, [
            [
                'attribute' => 'field_name',
                'hiddenFromExport' => true,                
                'label' => ucfirst(Yii::t('app', 'field')),
                'content' => function ($model, $key, $index, $column) {
                    return isset($model->field_name) ? Html::encode($model->field_name) : '-';
                },
                        
                'filterInputOptions'=>['placeholder'=>Yii::t('app', 'field'), 'class'=>'form-control'],
            ]
        ]);

// inst/type/field export
$columns = ArrayHelper::merge($columns, [
    [
        'label' => ucfirst(Yii::t('app', 'installation')),
        'hidden' => true,
        'hiddenFromExport' => false,                
        'attribute'=>'installation.name'
    ],
    [
        'label' => ucfirst(Yii::t('app', 'type')),
        'hidden' => true,
        'hiddenFromExport' => false,                
        'attribute' => 'installation.type'
    ],
    [
        'label' => ucfirst(Yii::t('app', 'field')),
        'hidden' => true,
        'hiddenFromExport' => false,                
        'attribute'=>'field_name'
    ],
    [
        'label' => Yii::t('app', 'Deadline'),
        'attribute' => 'deadline',
        'hidden' => true,
        'hiddenFromExport' => false,                
    ],
    [
        'label' => Yii::t('app', 'Reporting status'),
        'hidden' => true,
        'hiddenFromExport' => false,                
          'content' => function($model, $key, $index, $column) {
            if (isset($model->report)) {
                if ($model->report->ca_status == EventDeclaration::INCIDENT_REPORT_REPORTED) {
                  $s = \Yii::t('app', "REPORTED TO THE CA. PENDING FOR ACCEPTANCE.");
                  return $s;
                }
                else if ($model->report->ca_status == EventDeclaration::INCIDENT_REPORT_REJECTED) {
                  $s = \Yii::t('app', "REJECTED BY THE CA. CHECK THE REASON THEN RESUBMIT.");
                  return $s;
                }
                else if ($model->report->ca_status == EventDeclaration::INCIDENT_REPORT_ACCEPTED) {
                  $s = \Yii::t('app', "ACCEPTED AND ASSESSED BY THE CA.");
                  return $s;
                }
                else {
                  $s = \Yii::t('app', "NOT SUBMITTED.");
                  return $s;
                };
            } else {
                $s = \Yii::t('app', "NOT SUBMITTED.");
                return $s;
            }
            //$model->refreshFinalizeStatus();
          },
    ],
    
]);
                
$columns = ArrayHelper::merge($columns, [
    /* here insert the first action column */
    $actionColumn,
    
    
    [
        /* reporting dates column */
        
        'attribute' => 'reporting_dates',
        'hiddenFromExport' => true,                
        'label' => Yii::t('app', 'Dates') . '/' . Yii::t('app', 'Status'),
        'filterType' => \kartik\grid\GridView::FILTER_SELECT2,
        'filter' => [
            '0' => Yii::t('app', 'in due time'),
            '1' => Yii::t('app', 'late'),
            '2' => Yii::t('app', 'overdue'),
            '3' => Yii::t('app', 'submitted'),
            '4' => Yii::t('app', 'submitted late'),
            '5' => Yii::t('app', 'submitted in due time'),
            '7' => Yii::t('app', 'CA accepted'),    //to correspond to self::
            '6' => Yii::t('app', 'CA rejected'),    //to correspond to self::
            '8' => Yii::t('app', 'CA pending'),
            ],
        'filterWidgetOptions'=>[
                'pluginOptions'=>['allowClear'=>true],
            ],
        'filterInputOptions'=>['placeholder'=>Yii::t('app', 'Status')],
        
        'content' => function($data) {
            /* @var $data \app\models\events\EventDeclaration */            
            /* @var $timeLeft DateInterval */            
            
            $timeLeft = $data->timeLeft;
            
            /*create the display string */
            $timeLeftString = '';
            $timeLeft->y > 0 ? $timeLeftString .= Yii::t('app', '{evt, plural, =1{# year} other{# years} }', ['evt' => $timeLeft->y]) . ' ' : '';
            $timeLeft->m > 0 ? $timeLeftString .= Yii::t('app', '{evt, plural, =1{# month} other{# months}} ', ['evt' => $timeLeft->m]) : '';
            $timeLeft->d > 0 && strlen($timeLeftString) > 0 ? $timeLeftString .= Yii::t('app', 'and') . ' ' . Yii::t('app', '{evt, plural, =1{# day} other{# days}}', ['evt' => $timeLeft->d]) : '';
            $timeLeft->d > 0 && strlen($timeLeftString) == 0 ? $timeLeftString .= Yii::t('app', '{evt, plural, =1{# day} other{# days}}', ['evt' => $timeLeft->d]) : '';
            $timeLeft->d = 0 && strlen($timeLeftString) == 0 ? $timeLeftString .= Yii::t('app', 'less than 1 day') : '';
            
            $cellCss = '';
            /* set the cell css function Event status */
            
            $time_to_submission_text = Yii::t('app', 'Time to submission');
            switch ($data->reportingTimeStatus)
            {
                case app\models\events\EventDeclaration::TIME_STATUS_SUBMITTED:
                    $cellCss = 'sy_deadline_container_submitted';
                    $time_to_submission_text = Yii::t('app', 'Time from submission');
                    break;
                case app\models\events\EventDeclaration::TIME_STATUS_LATE:
                    $cellCss = 'sy_deadline_container_late';
                    break;
                case app\models\events\EventDeclaration::TIME_STATUS_OVERDUE:
                    $cellCss = 'sy_deadline_container_overdue';
                    $time_to_submission_text = ucfirst(Yii::t('app', 'overdue'));
                    break;
                case app\models\events\EventDeclaration::TIME_STATUS_IN_TIME:
                    $cellCss = 'sy_deadline_container_in_time';
                    break;
            }
            
            $s = '<div class="' . $cellCss . '">'
                    . '<small>'
                    . '<ul class="list list-unstyled">'
                    . '     <li>'
                    . '         <span class="text-info">'
                    .               Yii::t('app', 'Deadline') . ': '
                    . '         </span>'
                    .           $data->event_deadline
                    . '     </li>'
                    . '     <li>'
                    . '         <span class="text-info">'
                    .               $time_to_submission_text . ': '
                    . '         </span>'
                    .           $timeLeftString
                    . '     </li>'
                    . '</ul>'
                    . '</small>'
                    . '</div>';
            
            
            $title = \Yii::t('app', 'Submission dates');
            $popover_content = [];
            
            $glyph_td_css = 'margin: -6px; padding-right: 6px; padding-left: 6px; height: 100%;';
            $late_report_css =  'color: gray';
            
            
            if ($data->report !== null) {
                $subTasks = $data->getSubmissionTasks();
                
                if ($subTasks['late_code'] == 0) {
                    $late_report_css = 'green';
                    $popover_content[] = '<p style = "color: ' . $late_report_css . '">'
                            . \Yii::t('app', 'This report has been submitted before the deadline.')
                            . '</p>';
                } else {
                    $late_report_css = 'red';
                    $popover_content[] = '<p style = "color: ' . $late_report_css . '">'
                            . \Yii::t('app', 'This report has been submitted after the deadline.')
                            . '</p>';
                }
                
                foreach($subTasks as $key => $value) {
                    if ($key !== 'late_code') {
                        //$popover_content[] = '<small><b>' . Yii::t('app', $key) . ':</b> ' . $value . '</small>';
                        $popover_content[] = '<small><b>' . $key . ':</b> ' . $value . '</small>';
                    }
                    else 
                    {
                        $late_report_css = $value == 1 ? 
                                'color: red' : 
                                'color: green';
                    }
                }
            } else {
                $popover_content[] = '<em class="text-muted">' . \Yii::t('app','The incident must to be reported before having this information available.') . '</span>';
            }
            
            $popover = Html::tag(
                'a',
                '',
                [
                    'style' => $late_report_css,
                'class' => 'audit-toggler glyphicon
                glyphicon-time',
                'data-toggle' => 'popover',
                'data-html' => 'true',
                'data-title' => $title,
                'data-placement'=>'left',  //to display to the right
                'data-trigger'=>'focus',    //to close at user's next click
                'data-content' =>
                implode('<br/>', $popover_content)
            ]);
            
            if (isset($data->report) && $data->report->ca_status == app\models\events\EventDeclaration::INCIDENT_REPORT_REJECTED) {
                $pop2_class = 'fa fa-exclamation-triangle fa-lg sy_error';
                $pop2_title = Yii::t('app', 'CA status');
                $pop2_data_content = Yii::t('app', 'REJECTED BY THE CA. CHECK THE REASON (in the drafts section) THEN RESUBMIT.');
            }
            else if (isset($data->report) && $data->report->ca_status == app\models\events\EventDeclaration::INCIDENT_REPORT_ACCEPTED) {
                $pop2_class = 'fa fa-check fa-lg text-success';
                $pop2_title = Yii::t('app', 'CA status');
                $pop2_data_content = Yii::t('app', 'ACCEPTED AND ASSESSED BY THE CA.');
            }
            
            if (isset($pop2_class)) {
                $popover2 = '<br/>' . Html::tag(
                    'i',
                    '',
                    [
                        'style' => '',
                        'class' => 'audit-toggler ' . $pop2_class,
                        'data-toggle' => 'popover',
                        'data-html' => 'true',
                        'data-title' => $pop2_title,
                        'data-placement'=>'left',  //to display to the right
                        'data-trigger'=>'focus',    //to close at user's next click
                        'data-content' => $pop2_data_content
                        
                ]);
            }
            else {
                $popover2 = '';
            }
            
            
            
//            echo '<pre>';
//            echo var_dump(implode('<br/>', $popover_content));
//            echo '</pre>';
            $tbl = '<table style="width:100%; height:100%">'
                    . '<tr>'
                    . '<td style = "' . $glyph_td_css .' vertical-align: top">'
                    .   $popover . $popover2
                    . '</td>'
                    . '<td>' . $s . '</td>'
                    . '</tr>'
                    . '</table>';
            return $tbl;
        },
        
        /* (end) reporting dates column */
    ],
            
            $actionColumnDrafts,
            
]);
    

    
$toolbar_content = ''; 

$canRegister != '' ? 
        $toolbar_content .= Html::a('<i class="glyphicon glyphicon-plus"></i>', Yii::$app->getUrlManager()->createUrl('events/event-declaration/create'), ['class'=>'btn btn-success', 'title'=>Yii::t('app', 'Register {evt}', ['evt'=>Yii::t('app', 'new event')])]) 
        : '';

$toolbar_content .=  Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], [
                        'class' => 'btn btn-default', 
                        'title' => Yii::t('app', 'Reset Grid')
                    ]);
