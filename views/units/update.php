<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\units\Units */

$this->title = Yii::t('app', 'Update {evt}', [
    'evt' => \Yii::t('app','unit'),
]) . ' ' . $model->unit;

$this->params['breadcrumbs'][] =     [
        'label' => Yii::t('app', 'Management'),
        'url' => Url::to(['/management/']),
    ];

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Units'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="units-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
