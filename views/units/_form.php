<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\units\Units */
/* @var $form yii\widgets\ActiveForm */

$us_array = [
    0 => \Yii::t('app/units', 'metric'),
    1 => \Yii::t('app/units', 'imperial')
];



$types = [
    1 => \Yii::t('app/units', 'mass'),
    2 => \Yii::t('app/units', 'time'),
    3 => \Yii::t('app/units', 'rate (mass)'),
    4 => \Yii::t('app/units', 'speed'),
    5 => \Yii::t('app/units', 'pressure'),
    6 => \Yii::t('app/units', 'length'),
    7 => \Yii::t('app/units', 'temperature'),
    8 => \Yii::t('app/units', 'metric volume'),
    9 => \Yii::t('app/units', 'unit of energy')
]
?>

<div class="units-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'scope')->dropDownList($types, [
        'prompt' => '-- ' . \Yii::t('app', 'type') .' --',        
    ]) ?>

    <?= $form->field($model, 'unit')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'c_factor')->textInput() ?>

    <?= $form->field($model, 's_unit')->textInput() ?>

    <?= $form->field($model, 'expression')->textarea([
        'class' => 'form-control'
    ]) ?>

    <?= $form->field($model, 'conversion_class')->textInput() ?>
    
    <?= $form->field($model, 'us')->dropDownList($us_array, [
        'prompt' => '-- ' . \Yii::t('app', 'type') .' --',        
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
