<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\units\UnitsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = ucfirst(Yii::t('app', 'units'));
$this->params['breadcrumbs'][] =     [
        'label' => Yii::t('app', 'Management'),
        'url' => Url::to(['/management/']),
    ];
$this->params['breadcrumbs'][] = $this->title;

$us_array = [
    0 => \Yii::t('app/units', 'metric'),
    1 => \Yii::t('app/units', 'imperial')
];



$types = [
    1 => \Yii::t('app/units', 'mass'),
    2 => \Yii::t('app/units', 'time'),
    3 => \Yii::t('app/units', 'rate (mass)'),
    4 => \Yii::t('app/units', 'speed'),
    5 => \Yii::t('app/units', 'pressure'),
    6 => \Yii::t('app/units', 'length'),
    7 => \Yii::t('app/units', 'temperature'),
    8 => \Yii::t('app/units', 'metric volume'),
    9 => \Yii::t('app/units', 'unit of energy')
];

        if (\Yii::$app->user->can('units-edit'))
        {
            $template = '{update}';
        } else {
            $template = '';
        }

?>
<div class="units-index">

    <?php include Yii::getAlias('@app') . '/views/shared_parts/message_snippet.php'; ?>
    
    <h1><?= Html::encode($this->title) ?></h1>
    <?php if ((\Yii::$app->user->can('sys_admin') || \Yii::$app->user->identity->isCaUser) ) { 
        include_once '_columns_ca.php';
    ?>

    <p>
        <?php if (\Yii::$app->user->can('units-create')) {
            echo Html::a(Yii::t('app', 'Create {evt}', ['evt'=>ucfirst(\Yii::t('app','unit'))]), ['create'], ['class' => 'btn btn-success']);
        }?>
    </p>
    
    <?php } else { 
        include_once '_columns_oo.php';
    } ?>

    <div class='sy_pad_top_18'>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $columns,
    ]); ?>
    </div>
</div>
