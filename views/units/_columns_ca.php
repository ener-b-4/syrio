<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;


$columns = [
            'id',
            'unit',
            //'scope',
            [
                'class' => \yii\grid\DataColumn::className(),
                'attribute' => 'scope',
                'content' => function($model){
        
                    switch($model->scope) {
                        case 1:
                            return \Yii::t('app/units', 'mass');
                            break;
                        case 2:
                            return \Yii::t('app/units', 'time');
                            break;
                        case 3:
                            return \Yii::t('app/units', 'rate (mass)');
                            break;
                        case 4:
                            return \Yii::t('app/units', 'speed');
                            break;
                        case 5:
                            return \Yii::t('app/units', 'pressure');
                            break;
                        case 6:
                            return \Yii::t('app/units', 'length');
                            break;
                        case 7:
                            return \Yii::t('app/units', 'temperature');
                            break;
                        case 8:
                            return \Yii::t('app/units', 'metric volume');
                            break;
                        case 9:
                            return \Yii::t('app/units', 'unit of energy');
                            break;
                    }
                },
                'filter' => Html::activeDropDownList(
                        $searchModel, 
                        'scope', 
                        $types,
                        [
                            'class'=>'form-control',
                            'prompt' => '-- ' . \Yii::t('app', 'type') .' --']),
            ],
            
            [
                'class' => \yii\grid\DataColumn::className(),
                'attribute' => 'us',
                'content' => function($model){
                    return $model->us == 1 ? \Yii::t('app/units', 'imperial') : \Yii::t('app/units', 'metric');
                },
                'filter' => Html::activeDropDownList(
                        $searchModel, 
                        'us', 
                        $us_array,
                        [
                            'class'=>'form-control',
                            'prompt' => '-- ' . \Yii::t('app', 'type') .' --']),        
            ],
                        
            'c_factor',
            's_unit',
            'expression',
            'conversion_class',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => $template
            ],
        ];