<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\HolidaysSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = \Yii::t('app', '{n, plural, =1{Holiday} other{Holidays}}', ['n'=>2]);

$breadcrumbs = [
	[
		'label' => Yii::t('app', 'Management'),
		'url' => \Yii::$app->urlManager->createUrl(['/management/']),
		//'template' =>"<li>{link}</li>
	],
];
$this->params['breadcrumbs'] = $breadcrumbs;

$this->params['breadcrumbs'][] = $this->title;

require __DIR__ . '/grid_parts/grid_columns.php';

?>
<div class="holidays-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php
        if ((\Yii::$app->user->can('holiday_crud')) )
        {
            echo Html::a(Yii::t('app', 'Add {evt}', [
                'evt' => \Yii::t('app', '{n, plural, =1{Holiday} other{Holidays}}', ['n'=>1]),
            ]), ['create'], ['class' => 'btn btn-success']);
        }        
        ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $columns,
    ]); ?>

</div>
