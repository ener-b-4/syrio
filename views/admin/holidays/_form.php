<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\DateTimePicker;
use kartik\widgets\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Holidays */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="holidays-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
    echo $form->field($model, 'dateAll')->widget(DatePicker::className(), [
        'name' => 'dp',
        'type' => DatePicker::TYPE_COMPONENT_PREPEND,
        //'value' => '2016-04-26',
        'convertFormat' => true,
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'php:Y-m-d'
        ]
    ]);
    
    ?>    

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'repeat')->dropDownList([
        1 => Yii::t('app', 'Yes'),
        0 => Yii::t('app', 'No'),
    ], ['prompt'=>Yii::t('app', '(not set)')]) ?>

    
    <div>
        <ul class="list list-inline " style="margin-left: 0px;">
            <li><span class="text-info"><?= ucfirst(\Yii::t('app', 'created at')) . ': ' ?></span> <?= date('Y-m-d H:i:s', $model->created_at) ?></li>
            <li><span class="text-info"><?= ucfirst(\Yii::t('app', 'modified at')) . ': ' ?></span> <?= date('Y-m-d H:i:s', $model->updated_at) ?></li>
        </ul>
    </div>
    
    <?= $form->field($model, 'request')->hiddenInput()->label('', ['class'=>'hiddenBlock']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
