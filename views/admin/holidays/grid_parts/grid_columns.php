<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use kartik\grid\GridView;


$columns = [];

$columns = [

            [
                'class' => kartik\grid\DataColumn::className(),
                'attribute' => 'repeat',
                'group' => true,
                'groupedRow' => true,
                
                'value' => function ($model, $key, $index, $column) {
                    if (isset($model->repeat) && $model->repeat === 1) {
                        return Yii::t('app/calendar', 'Repeat every year');
                    } else {
                        return Yii::t('app/calendar', 'Individual');
                    }
                },
                //'format' => 'html'
            ],
            
            
            ['class' => \kartik\grid\SerialColumn::className()],

            [
                'class' => kartik\grid\DataColumn::className(),
                'attribute' => 'year',
                'label' => 'Year',
                'content' => function ($model, $key, $index, $column) {
                    //check if this is a search
                    $request = \Yii::$app->request;
                    if ($request->get('HolidaysSearch') !== NULL) {
                        if (key_exists('year', $request->get('HolidaysSearch'))) {                            
                            $my_year = intval(\Yii::$app->request->get('HolidaysSearch')['year']);
                            if ($my_year === 0) {
                                $my_year = null;
                            }
                        }
                    }
                    
                    
                    //create the date
                    $dt = new DateTime();
                    if ($model->repeat) {
                        if (isset($my_year)) {
                            $dt->setDate(date($my_year), $model->month, $model->day);
                        } else {
                            //use current year
                            $dt->setDate(date('Y'), $model->month, $model->day);
                        }
                    } else {
                        $dt->setDate($model->year, $model->month, $model->day);
                    }
                    
                    return $dt->format('D, Y-m-d');
                },
                        
                'filterType' => GridView::FILTER_SELECT2,   //GridView::FILTER_CHECKBOX,
                'filter' => \yii\helpers\ArrayHelper::map(\app\models\Holidays::find()
                        ->asArray()
                        ->select('year')
                        ->where(['repeat'=>0])
                        ->groupBy('year')->all()
                        , 'year', 'year'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear'=>true],
                ],
                'filterInputOptions' => ['placeholder'=>\Yii::t('app', "any")],                
                        
            ],
            
            [
                'class' => kartik\grid\DataColumn::className(),
                'attribute' => 'name',
                'group' => true
            ],
                        
        ];
        
if (\Yii::$app->user->can('holiday_crud')) {
    $columns[] = [
        'class' => \kartik\grid\ActionColumn::className(),
        'template' => '{update} {delete}',
        'width' => '36px',
        'hAlign' => 'center'
        ];
}
