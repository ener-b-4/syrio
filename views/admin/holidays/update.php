<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Holidays */

$this->title = Yii::t('app', 'Update {evt}', [
    'evt' => \Yii::t('app', '{n, plural, =1{Holiday} other{Holidays}}', ['n'=>1]),
]);

$breadcrumbs = [
	[
		'label' => Yii::t('app', 'Management'),
		'url' => \Yii::$app->urlManager->createUrl(['/management/']),
		//'template' =>"<li>{link}</li>
	],
];
$this->params['breadcrumbs'] = $breadcrumbs;

$this->params['breadcrumbs'][] = ['label' => \Yii::t('app', '{n, plural, =1{Holiday} other{Holidays}}', ['n'=>2]), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="holidays-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
