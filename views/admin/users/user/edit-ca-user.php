<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

/**
 * the view for editing a CA user
 * 
 * @author bogdanV
 */


use app\models\admin\rbac\Role;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\widgets\ValidationReportWidget;
//use app\models\ca\CompetentAuthority;



/* @var $this yii\web\View */
/* @var $form kartik\form\ActiveForm */
/* @var $model app\models\User */
/* @var $organizations mixed the array of OrganizationAcronym -> Organization_name
/* @var $ca_roles app\models\admin\rbac\Role the Roles associated with Competent Authorities */
/* @var $selected_assignments array of current roles */


/* breadcrumbs section */
use yii\helpers\Url;
$breadcrumbs = [
    [
        'label' => Yii::t('app', 'Management'),
        'url' => Url::to(['/management/']),
        //'template' =>"<li>{link}</li>
    ],
    [
        'label' => ucfirst(Yii::t('app', 'users')),
        'url' => Url::to(['/admin/users/user']),
        //'template' =>"<li>{link}</li>
    ],
    [
        'label' => Yii::t('app', '{action} {evt} user', [
            'evt'=>Yii::t('app', 'Competent Authority'),
            'action' => Yii::t('app', 'Edit')]),
        //'template' =>"<li>{link}</li>
    ],
];
$this->params['breadcrumbs'] = $breadcrumbs;

$this->title = Yii::t('app', '{evt} User', ['evt'=>Yii::t('app', 'Edit')]);

//$roles = ArrayHelper::map($ca_roles, 'name', 'description');
$roles = ArrayHelper::map($ca_roles, 'name', 'name');

foreach ($roles as $key=>$val) {
    $roles[$key]= Yii::t('app', $val) . ' (' . $val . ')';
}


/* use this on any view to include the actionmessage logic and widget */
//include Yii::getAlias('@app') . '\views\shared_parts\message_snippet.php';
include Yii::getAlias('@app') . '/views/shared_parts/message_snippet.php';

if ($model->hasErrors())
{
    echo ValidationReportWidget::widget([
        'title' => \Yii::t('app','Edit {evt}', ['evt'=>Yii::t('app', 'CA user')]),
        'errors_message' => \Yii::t('app','Unable to save changes to the user! Please review the information below and try again.'),
        'errors'=>$model->errors,
        'no_errors_message' => \Yii::t('app','User\'s {evt} details has been modified.', ['evt'=>$model->username]),
    ]);
}                            

?>

<?php $title = Yii::t('app', 'Edit user'); ?>
    
<h1><?= Yii::t('app', 'Registered users') ?></h1>    
<h3><span class="text-primary"><?= Html::encode($title) ?></span></h3>


<div>
    <div class="col-lg-12">
    <?=
        $this->render('_ca-user-form', ['model'=>$model, 'ca_roles'=>$roles, 'organizations'=>$organizations]);
    ?>
    </div>
</div>

