<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use kartik\grid\GridView;
use app\widgets\ActionMessageWidget;
use app\models\User;
use app\models\admin\rbac\Role;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * Description of list-users
 *
 * @author bogdanV
 */

/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel app\temp\models\CaUserSearch */

?>

<div>
    <?php
        $session_msg = Yii::$app->session->get('finished_action_result');     //$session_msg is of type ActionMessage
        if (isset($session_msg))
        {
            echo ActionMessageWidget::widget(['actionMessage' => $session_msg,]);

            //disable the next line if you want the message to persist
            unset(Yii::$app->session['finished_action_result']);
        }
    ?>    

    <h1>Registered Users</h1>
    
    <?php

    //build the html for assignments
    
        function buildAssignmentsHtml($model) {
            $names = [];
            foreach ($model->assignments as $assignment) {
                //$names[] = $assignment->item_name;
                $names[] = $model->getAssignmentName($assignment->item_name);
            }
            
            return implode(', ', $names);
        }
        
/*        
        $actionColumn = new kartik\grid\ActionColumn();
        $actionColumn->buttons[] = ['delete' => function ($url, $model, $key) {
                                        //return $model->status === 10 ? Html::a('my_action', $url) : '';
                                        return Html::a('<i class="glyphicon glyphicon-trash"></i>', $url, ['title' => Yii::t('app', 'Remove')]);
                                    }]
*/
        
    ?>
    
    <?php //echo var_dump(Yii::$app->user->getIdentity()->possibleRoles); die(); ?>
    
    <div>
        <?= GridView::widget([
            'dataProvider'=> $dataProvider,
            'filterModel' => $searchModel,
            'rowOptions' => function($model) {
                return $model->status == User::STATUS_DELETED ? ['class' => 'danger'] : [];
            },
            'pjax' => true,
            'toolbar' =>  [
                ['content'=>
                    Html::a('<i class="glyphicon glyphicon-plus"></i>', Yii::$app->getUrlManager()->createUrl('/admin/users/user/create-ca-user'), ['class'=>'btn btn-success', 'title'=>Yii::t('app', 'Add user')]),
                    Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['grid-demo'], ['data-pjax'=>0, 'class' => 'btn btn-default', 'title'=>Yii::t('app', 'Reset Grid')])
                ],
                '{toggleData}',
            ],
            'columns' => [
                /*
                [
                    'class'=>'kartik\grid\ExpandRowColumn',
                    'value'=>function($model, $key, $index, $column) {
                        return GridView::ROW_COLLAPSED;
                    },
                    'detail'=>function($model, $key, $index, $column) {
                        $s =    "<p>"
                                . "Details: " // . $model->caOrganizations[0]->organization_name . "<br/>"
                                . "Member of: " . buildAssignmentsHtml($model)
                                . "</p>";
                        return $s;
                    },
                    'expandTitle' => Yii::t('app', 'Show details'),
                    'collapseTitle' => Yii::t('app', 'Hide details'),
                    'expandAllTitle' => Yii::t('app', 'Show details (all)'),
                    'collapseAllTitle' => Yii::t('app', 'Hide details (all)'),
                ],
                 * 
                 */
                [
                    'class' => '\kartik\grid\DataColumn',
                    //'attribute' => 'organizationType',
                    'value' => function($data) {
                        if (isset($data->organization)) {
                            switch ($data->organization->organizationType)
                            {
                                case app\modules\management\models\Organization::COMPETENT_AUTHORITY:
                                    $imgUrl = Yii::getAlias("@web") . '/css/images/24x24/ca_generic.png';
                                    return '<img src="' . $imgUrl . '" />';
                                    break;
                                case app\modules\management\models\Organization::OPERATORS_OWNERS:
                                    $imgUrl = Yii::getAlias("@web") . '/css/images/24x24/operator_black.png';
                                    return '<img src="' . $imgUrl . '" />';
                                    break;
                                default:
                                    return '-';
                            }
                        }
                    },
                    'options' => [
                        'width'=> '50px',
                    ],
                    'format' => 'html',
                ],
                [
                    'class' => '\kartik\grid\DataColumn',
                    'attribute' => 'full_name',
                    'value' => 'full_name',
                    'options' => [
                        'width'=> '325px',
                    ]
                ],
                [
                    'class' => '\kartik\grid\DataColumn',
                    'attribute' => 'assignments',
                    'value' => function($data) {
                        return buildAssignmentsHtml($data);
                    },
                    'filterType' => GridView::FILTER_SELECT2,   // '\kartik\widgets\Select2',
                    'filter' => \yii\helpers\ArrayHelper::map(Yii::$app->user->getIdentity()->possibleRoles, 'name', 'name'),
                    'filterWidgetOptions' => [
                        'pluginOptions' => ['allowClear'=>true],
                    ],
                    'filterInputOptions' => ['placeholder'=>'Any assignment'],
                    'format' => 'raw',
                ],
                [
                    'class' => '\kartik\grid\DataColumn',
                    'value' => function($data) {
                        return isset($data->organization->organization_acronym) ? $data->organization->organization_acronym : '-';
                    }
                ],
                [
                    'class' => '\kartik\grid\DataColumn',
                    'attribute' => 'username',
                ],
                /*
                [
                    'class' => '\kartik\grid\DataColumn',
                    'attribute' => 'organization.organization_acronym',
                    'value' => 'organization.organization_acronym',
                ],
                 * 
                 */
                [
                    'class' => '\kartik\grid\ActionColumn',
                    'buttons' => [
                        'delete' => function ($url, $model, $key) {
                                //$url = 'delete';
                                $msg = Yii::t('app', 'Are you sure you want to delete {data}?', [
                                    'data'=>$model->full_name]);
                                $msg .= ' ';
                                $msg .= Yii::t('app', 'Operation CAN be undone.');
                                return $model->status === 10 ? 
                                        Html::a('<i class="glyphicon glyphicon-trash"></i>', $url, [
                                            'title' => Yii::t('app', 'Remove'),
                                            'data-confirm' => $msg,
                                            'data-method' => 'POST']) : '';
                            },
                        'undo-delete' => function ($url, $model, $key) {
                                return $model->status === 0 ? 
                                        Html::a('<i class="glyphicon glyphicon-repeat"></i>', $url, [
                                            'title' => Yii::t('app', 'Undo'), 
                                            'data-method'=>'POST']) : '';
                            },
                        'edit-ca-user' => function ($url, $model, $key) {
                                //return $model->status === 10 ? Html::a('my_action', $url) : '';
                                return Html::a('<i class="glyphicon glyphicon-pencil"></i>', $url, ['title' => Yii::t('app', 'Edit')]);
                            }
                    ],
                    'template' => '{edit-ca-user} {undo-delete} {delete}',
                    //'deleteOptions' => ['label' => '<i class="glyphicon glyphicon-remove"></i>']
                ],
            ],
            'responsive'=>true,
            'hover'=>true,
            'export'=>false,
            'panel' => [
                    'type' => GridView::TYPE_PRIMARY,
                    'heading' => '<i class="glyphicon glyphicon-user"></i>  CA Users',
                ],                        
        ]) ?>
    </div>
</div>
