<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\form\ActiveForm;
use app\widgets\ValidationReportWidget;


/**
 * Description of account
 *
 * @author vamanbo
 */

/* @var $this \yii\web\View */
/* @var $model app\models\forms\ChangeOwnDetails */

$this->title=Yii::t('app', 'Change contact details');

$breadcrumbs = [
	[
		'label' => Yii::t('app', 'Management'),
		'url' => \Yii::$app->urlManager->createUrl(['/management/']),
	],
	[
		'label' => Yii::t('app', 'Account'),
		'url' => \Yii::$app->urlManager->createUrl(['/admin/users/user/account']),
	],
];
$this->params['breadcrumbs'] = $breadcrumbs;

$this->params['breadcrumbs'][] = $this->title;


/* use this on any view to include the actionmessage logic and widget */
//include Yii::getAlias('@app') . '\views\shared_parts\message_snippet.php';
include Yii::getAlias('@app') . '/views/shared_parts/message_snippet.php';

if ($model->hasErrors())
{
    echo ValidationReportWidget::widget([
        'title' => \Yii::t('app','Change {evt}', ['evt'=>\Yii::t('app/crf', 'Contact details')]),
        'errors_message' => \app\components\helpers\TGenericMessage::Error(true),
        'errors'=>$model->errors,
    ]);
}                            

$title = Yii::t('app','Change contact details');

$cUser = \Yii::$app->user->identity;

/* get the information related to the current user */
?>


<div class='container'>
    <div class='row'>
        <div class='col-lg-12 sy_pad_bottom_18'>
            <h1><?= Yii::t('app', 'Account') ?></h1>  
        </div>
        <div id='leftPannel' class="col-md-2 col-sm-3 col-xs-12">
            <div class='panel panel-default'>
                <div class='panel-heading'>
                    <strong> <?= ucfirst(Yii::t('app' ,'actions')) ?> </strong>                    
                </div>
                <ul class='list list-unstyled list-group'>
                    <li class='list-group-item'>
                        <?= Html::a(\Yii::t('app','Change {evt}', ['evt'=>\Yii::t('app', 'password')]), Url::to(['admin/users/user/change_password']), []) ?>
                    </li>
                    <li class='list-group-item'>
                        <?= Html::a(Yii::t('app','Change {evt}', ['evt'=>lcfirst(\Yii::t('app', 'Username'))]), Url::to(['admin/users/user/change_username']), []) ?>
                    </li>
                    <li class='list-group-item active'>
                        <span><?= Yii::t('app','Change contact details') ?></span>
                    </li>
                </ul>
            </div>
        </div>
        
        <?php
            echo $this->render('_contact_details_form', ['model'=>$model]);
        ?>
    </div>
        
</div>


