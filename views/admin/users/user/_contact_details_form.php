<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\widgets\ActiveForm;
use app\widgets\ValidationReportWidget;


/**
 * Description of account
 *
 * @author vamanbo
 */

/* @var $this \yii\web\View */
/* @var $model app\models\User */


?>

    <?php $form = \kartik\form\ActiveForm::begin([
        'formConfig' => [
            'showLabels' => false
        ]
        
    ]) ?>
        <div id='contentPannel' class="col-md-10 col-sm-9 col-xs-12">
            <div class='panel panel-default'>
                <div class='panel-body'>
                    <div>
                        <ul class='list list-unstyled'>
                            <li>    <!-- name -->
                                <ul class='list list-inline'>
                                    <li class='sy_align_top'>
                                        <strong><?= \Yii::t('app', 'Name') . ': ' ?></strong>                                        
                                    </li>
                                    <li class='sy_align_top'>
                                        <?= $form->field($model, 'first_name')->textInput([
                                            'placeholder' => \Yii::t('app', 'First Name')
                                        ]) ?>
                                        <?php // $model->full_name ?>
                                    </li>
                                    <li class='sy_align_top'>
                                        <?= $form->field($model, 'last_name')->textInput([
                                            'placeholder' => \Yii::t('app', 'Last Name')
                                        ]) ?>
                                    </li>
                                </ul>
                            </li> <!-- end name -->
                            <li class='sy_pad_top_6'>    <!-- email -->
                                <ul class='list list-inline'>
                                    <li class='sy_align_top'>
                                        <strong><?= Yii::t('app', 'Email') . ': ' ?></strong>
                                    </li>
                                    <li class='sy_align_top'>
                                        <?= $form->field($model, 'email')->textInput([
                                            'placeholder' => strtolower(Yii::t('app', 'Email'))
                                        ]) ?>
                                    </li>
                                    <li class='sy_align_top'>
                                        <?= $form->field($model, 'email_repeat')->textInput([
                                            'placeholder' => strtolower(Yii::t('app', 'Repeat email'))
                                        ]) ?>
                                    </li>
                                </ul>
                            </li> <!-- end email -->
                            <li class='sy_pad_top_6'>    <!-- phone -->
                                <ul class='list list-inline'>
                                    <li class='sy_align_top'>
                                        <strong><?= Yii::t('app', 'Phone') . ': ' ?></strong>
                                    </li>
                                    <li class='sy_align_top'>
                                        <?= $form->field($model, 'phone')->textInput([
                                            'placeholder' => strtolower(Yii::t('app', 'Phone'))
                                        ]) ?>
                                    </li>
                                </ul>
                            </li> <!-- end phone -->
                            <li class='sy_pad_top_6'>    <!-- role -->
                                <ul class='list list-inline'>
                                    <li class='sy_align_top'>
                                        <strong><?= Yii::t('app', 'Role in organization') . ': ' ?></strong>
                                    </li>
                                    <li class='sy_align_top'>
                                        <?= $form->field($model, 'role')->textInput([
                                            'placeholder' => strtolower(Yii::t('app', 'Role in organization'))
                                        ]) ?>
                                    </li>
                                </ul>
                            </li> <!-- end role -->
                            
                            
                        </ul>
                        
                        <div class='row'>
                            <div class='col-lg-12 text-info text-right'>
                                <h3><?= Yii::t('app', 'Are you human?') ?><br/>
                                    <small><span class='text-muted'><?= \Yii::t('app', 'Please provide the verification text from the image') ?></span></small>
                                </h3>
                            </div>
                        </div>
                        <?= $form->field($model, 'captcha')->widget(\yii\captcha\Captcha::className())->label('') ?>

                    </div>
                    
                    <div class='row'>
                        <div class="col-lg-12 form-group">
                            <?= Html::submitButton(\Yii::t('app/commands', 'Submit'), [
                                'class'=>'btn btn-primary'
                            ] ) ?>
                            <?= Html::a(Yii::t('app/commands','Cancel'), 
                                    Url::to(['admin/users/user/account']), [
                                        'class' => 'btn btn-warning'
                                    ]) ?>

                        </div>
                    </div>
                    
                </div>
            </div>

            
        </div>
        <?php \kartik\form\ActiveForm::end() ?>

