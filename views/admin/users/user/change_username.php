<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use kartik\form\ActiveForm;
use app\models\forms\ChangePassword;
use yii\helpers\Html;
use app\widgets\ValidationReportWidget;
use yii\helpers\Url;

/**
 * Description of account
 *
 * @author vamanbo
 */

/* @var $this \yii\web\View */
/* @var $model app\models\forms\ChangeUsername */
/* @var $form kartik\form\ActiveForm */

$this->title=Yii::t('app', 'Change {evt}', ['evt'=>lcfirst(Yii::t('app', 'Username'))]);

$breadcrumbs = [
	[
		'label' => Yii::t('app', 'Management'),
		'url' => \Yii::$app->urlManager->createUrl(['/management/']),
	],
	[
		'label' => Yii::t('app', 'Account'),
		'url' => \Yii::$app->urlManager->createUrl(['/admin/users/user/account']),
	],
];
$this->params['breadcrumbs'] = $breadcrumbs;

$this->params['breadcrumbs'][] = $this->title;


/* use this on any view to include the actionmessage logic and widget */
//include Yii::getAlias('@app') . '\views\shared_parts\message_snippet.php';
include Yii::getAlias('@app') . '/views/shared_parts/message_snippet.php';

if ($model->hasErrors())
{
    echo ValidationReportWidget::widget([
        'title' => \Yii::t('app','Change {evt}', ['evt'=>\Yii::t('app', 'Username')]),
        'errors_message' => \app\components\helpers\TGenericMessage::Error(true),
        'errors'=>$model->errors,
    ]);
}                            

$title = Yii::t('app','Change {evt}', ['evt'=>lcfirst(\Yii::t('app', 'Username'))]);

?>

<div class='container'>
    
    <div class='row'>
        <div  class='col-lg-12 sy_pad_bottom_18'>
            <h1><?= Yii::t('app', 'Account') ?></h1>    
        </div>
    </div>
    
    <div class='row'>
        
        <div id='leftPannel' class="col-md-2 col-sm-3 col-xs-12">
            <div class='panel panel-default'>
                <div class='panel-heading'>
                    <strong> <?= ucfirst(Yii::t('app' ,'actions')) ?> </strong>                    
                </div>
                <ul class='list list-unstyled list-group'>
                    <li class='list-group-item'>
                        <?= Html::a(\Yii::t('app','Change {evt}', ['evt'=>\Yii::t('app', 'password')]), Url::to(['admin/users/user/change_password']), []) ?>
                    </li>
                    <li class='list-group-item active'>
                        <span><?= Yii::t('app','Change {evt}', ['evt'=>lcfirst(\Yii::t('app', 'Username'))]) ?></span>
                    </li>
                    <li class='list-group-item '>
                        <?= Html::a(Yii::t('app','Change contact details'), Url::to(['admin/users/user/change_contact_details']), []) ?>
                    </li>
                </ul>
            </div>
        </div>
        
        <div id='contentPannel'class="col-md-10 col-sm-9 col-xs-12">

            <div class='panel panel-default'>
                <div class='panel-body'>
                    <?php $form = ActiveForm::begin([
                        //'enableAjaxValidation' => false,
                        //'enableClientValidation' => false
                    ]); ?>

                    <?= $form->field($model, 'new_username', [
            //            'enableAjaxValidation' => 'false',
            //            'enableClientValidation' => 'true'
                    ])->textInput()
                            ->hint(Yii::t('app', 'The new username you will use for login'), [
                    ]) ?>

                    <div class='sy_pad_top_18'>
                        <?=  $form->field($model, 'captcha', [
            //                'enableAjaxValidation' => 'false',
            //                'enableClientValidation' => 'false'
                        ])->widget(\yii\captcha\Captcha::classname(), [
                            // configure additional widget properties here                
                        ]) ?>
                    </div>        

                    <div class="row">
                        <div class="col-lg-12 form-group" >
                            <?= Html::submitButton(Yii::t('app/commands', 'Submit'), [
                                'class' => 'btn btn-primary'
                                ]) ?>
                            <?= Html::a(Yii::t('app/commands','Cancel'), 
                                    Url::to(['admin/users/user/account']), [
                                        'class' => 'btn btn-warning'
                                    ]) ?>
                        </div>
                    </div>

                <?php
                ActiveForm::end();
                ?>
                </div>
            </div>
        </div>
    </div>
</div>


<?php

$script = <<< JS

$(function() {
        //executes when the DOM is ready
        alert('DOM ready');
        $('#w0').submit(function(event) {
            //event.preventDefault();
            alert('submitting...');
            event.stopImmediatePropagation();
            encryptData();
        });
});        
JS;

//$this->registerJs($script, $this::POS_READY);
?>



<script type="text/javascript">
    
  function encryptData(){
    
    var key = CryptoJS.enc.Hex.parse('bcb04b7e103a0cd8b54763051cef08bc55abe029fdebae5e1d417e2ffb2a00a3');
    var iv  = CryptoJS.enc.Hex.parse('101112131415161718191a1b1c1d1e1f');

    var old_password = document.getElementById('changepassword-old_password').value;
    var new_password = document.getElementById('changepassword-new_password').value;
    var new_password_repeat = document.getElementById('changepassword-password_repeat').value;
    
    var encrypted_old = CryptoJS.AES.encrypt(old_password, key, { iv: iv });
    var encrypted_new = CryptoJS.AES.encrypt(new_password, key, { iv: iv });
    var encrypted_new_repeat = CryptoJS.AES.encrypt(new_password_repeat, key, { iv: iv });

    var password_base64_old = encrypted_old.ciphertext.toString(CryptoJS.enc.Base64); 
    var password_base64_new = encrypted_new.ciphertext.toString(CryptoJS.enc.Base64); 
    var password_base64_new_repeat = encrypted_new_repeat.ciphertext.toString(CryptoJS.enc.Base64); 
    
    //return password_base64; 
    //alert(password_base64);
    $('#changepassword-old_password').val(password_base64_old);
    $('#changepassword-new_password').val(password_base64_new);
    $('#changepassword-password_repeat').val(password_base64_new_repeat);
    
    //$('#changepassword-old_password').val('');
    //$('#changepassword-new_password').val('');
    //$('#changepassword-new_pass_coded').val('');
    
// nu merge    decryptData(password_base64_new);
  };


</script>
