<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

/**
 * the view for adding a new CA user
 * 
 * @author bogdanV
 */


use app\models\admin\rbac\Role;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\widgets\ValidationReportWidget;
//use app\models\ca\CompetentAuthority;



/* @var $this yii\web\View */
/* @var $form kartik\form\ActiveForm */
/* @var $model */
/* @var $ca_roles app\models\admin\rbac\Role the Roles associated with Competent Authorities */

//$roles = ArrayHelper::map($ca_roles, 'name', 'description');
$roles = ArrayHelper::map($ca_roles, 'name', 'name');

foreach ($roles as $key=>$val) {
    $roles[$key]= Yii::t('app', $val) . ' (' . $val . ')';
}


/* use this on any view to include the actionmessage logic and widget */
//include Yii::getAlias('@app') . '\views\shared_parts\message_snippet.php';
include Yii::getAlias('@app') . '/views/shared_parts/message_snippet.php';

if ($model->hasErrors())
{
    echo ValidationReportWidget::widget([
        'title' => \Yii::t('app','Register {evt}', ['evt'=>Yii::t('app', 'CA user')]),
        'errors_message' => \Yii::t('app','Unable to register the user! Please review the information below and try again.'),
        'errors'=>$model->errors,
        'no_errors_message' => \Yii::t('app','User {evt} has been created.', ['evt'=>$model->username]),
    ]);
}                            

?>

<?php $title = Yii::t('app', 'Register user'); ?>
    
<h1><?= Html::encode($title) ?></h1>

<?=
    $this->render('_ca-user-form', ['model'=>$model, 'ca_roles'=>$roles]);
?>
