<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Description of account
 *
 * @author vamanbo
 */

/* @var $this \yii\web\View */
/* @var $model app\models\User */


/* get the information related to the current user */


$user_type_html = Yii::t('app', 'System Administrator');

$roles = app\components\helpers\AdminHelper::getUserRolesDescription($model->id);
$role_html = '';
foreach ($roles as $key=>$value) {
    $role_html .= '<span>'
            . $value['short']
            . '</span>'
            . ' <small><em class="text-muted">'
            . $value['description']
            . ' </em></small>'
            . '<br/>';
}

$date = date_create($model->created_at);
$member_since_html = date_format($date, 'd F Y');

?>

        <div id='contentPannel' class="col-md-10 col-sm-9 col-xs-12">
            <div class='panel panel-default'>
                <div class='panel-body'>
                    <div>
                        <ul class='list list-unstyled'>
                            <li>    <!-- name -->
                                <ul class='list list-inline'>
                                    <li class='sy_align_top'>
                                        <strong><?= Yii::t('app', 'Name') . ': ' ?></strong>
                                    </li>
                                    <li>
                                        <?= $model->full_name ?>
                                    </li>
                                </ul>
                            </li> <!-- end name -->
                            <li class='sy_pad_top_6'>    <!-- organization -->
                                <ul class='list list-inline'>
                                    <li class='sy_align_top'>
                                        <strong><?= Yii::t('app', 'Organization') . ': ' ?></strong>
                                    </li>
                                    <li>
                                        <?= isset($organization_html) ? $organization_html : '-' ?>
                                    </li>
                                </ul>
                            </li> <!-- end organization -->
                            <li class='sy_pad_top_6'>    <!-- role in organization -->
                                <ul class='list list-inline'>
                                    <li class='sy_align_top'>
                                        <strong><?= Yii::t('app', 'Role in organization') . ': ' ?></strong>
                                    </li>
                                    <li>
                                        <?= $model->role_in_organization ?>
                                    </li>
                                </ul>
                            </li> <!-- end role in organization -->
                            <li class='sy_pad_top_6'>    <!-- member since -->
                                <ul class='list list-inline'>
                                    <li class='sy_align_top'>
                                        <strong><?= Yii::t('app', 'Member since') . ': ' ?></strong>
                                    </li>
                                    <li>
                                        <?= $member_since_html ?>
                                    </li>
                                </ul>
                            </li> <!-- end member since -->
                            <li class='sy_pad_top_6'>    <!-- email -->
                                <ul class='list list-inline'>
                                    <li class='sy_align_top'>
                                        <strong><?= Yii::t('app', 'Email') . ': ' ?></strong>
                                    </li>
                                    <li>
                                        <?= $model->email ?>
                                    </li>
                                </ul>
                            </li> <!-- end email -->
                            <li class='sy_pad_top_6'>    <!-- phone -->
                                <ul class='list list-inline'>
                                    <li class='sy_align_top'>
                                        <strong><?= Yii::t('app', 'Phone') . ': ' ?></strong>
                                    </li>
                                    <li>
                                        <?= $model->phone ?>
                                    </li>
                                </ul>
                            </li> <!-- end phone -->
                            
                        </ul>
                    </div>
                    
                </div>
            </div>

            
            <div class='panel panel-default'>
                <div class='panel-body'>
                    <div>
                        <ul class='list list-unstyled'>
                            <li>    <!-- username -->
                                <ul class='list list-inline'>
                                    <li class='sy_align_top'>
                                        <strong><?= Yii::t('app', 'Username') . ': ' ?></strong>
                                    </li>
                                    <li>
                                        <?= $model->username ?>
                                    </li>
                                </ul>
                            </li> <!-- end username -->
                            <li class='sy_pad_top_6'>    <!-- user types -->
                                <ul class='list list-inline'>
                                    <li class='sy_align_top'>
                                        <strong><?= Yii::t('app', 'User type') . ': ' ?></strong>
                                    </li>
                                    <li>
                                        <?= $user_type_html ?>
                                    </li>
                                </ul>
                            </li> <!-- end user types -->
                            <li class='sy_pad_top_6'>    <!-- roles -->
                                <ul class='list list-inline'>
                                    <li class='sy_align_top'>
                                        <strong><?= Yii::t('app', 'Role(s)') . ': ' ?></strong>
                                    </li>
                                    <li>
                                        <?= $role_html ?>
                                    </li>
                                </ul>
                            </li> <!-- end roles -->
                        </ul>
                    </div>
                    
                </div>
            </div>            
        </div>


