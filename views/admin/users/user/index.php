<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use kartik\grid\GridView;
use app\widgets\ActionMessageWidget;
use app\models\User;
use app\models\admin\rbac\Role;

use app\assets\SyrioGlyphsAsset;

SyrioGlyphsAsset::register($this);

/**
 * Description of list-users
 *
 * @author bogdanV
 */

/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel app\temp\models\CaUserSearch */

/* breadcrumbs section */
use yii\helpers\Url;
$breadcrumbs = [
    [
        'label' => Yii::t('app', 'Management'),
        'url' => Url::to(['/management/']),
        //'template' =>"<li>{link}</li>
    ],
    [
        'label' => ucfirst(Yii::t('app', 'users')),
        //'url' => Url::to(['/admin/users/user']),
        //'template' =>"<li>{link}</li>
    ],
];
$this->params['breadcrumbs'] = $breadcrumbs;

$this->title = ucfirst(Yii::t('app', 'users'));

//include the grid columns
include_once '_grid_columns.php';
?>

<div>
    <?php
        $session_msg = Yii::$app->session->get('finished_action_result');     //$session_msg is of type ActionMessage
        if (isset($session_msg))
        {
            echo ActionMessageWidget::widget(['actionMessage' => $session_msg,]);

            //disable the next line if you want the message to persist
            unset(Yii::$app->session['finished_action_result']);
        }
    ?>    

    <h1><?= Yii::t('app', 'Registered Users') ?></h1>
    
    <?php

    //build the html for assignments
    
        function buildAssignmentsHtml($model) {
            $roles = app\components\helpers\AdminHelper::getUserRolesDescription($model->id);
            
            $names = [];
            foreach ($roles as $role) {
                //$names[] = $assignment->item_name;
                //$names[] = $model->getAssignmentName($assignment->item_name);
                $names[] = $role['short'];
            }
            
            return implode(', <br/><br/>', $names);
        }
        
/*        
        $actionColumn = new kartik\grid\ActionColumn();
        $actionColumn->buttons[] = ['delete' => function ($url, $model, $key) {
                                        //return $model->status === 10 ? Html::a('my_action', $url) : '';
                                        return Html::a('<i class="glyphicon glyphicon-trash"></i>', $url, ['title' => Yii::t('app', 'Remove')]);
                                    }]
*/
        
    ?>
    
    <?php 
        //echo var_dump(Yii::$app->user->getIdentity()->possibleRoles); die(); 
    
        /*
         * Create the link towards the appropriate create user
         */
         $_createUserLink = '';
         if (Yii::$app->user->can('sys_admin'))
         {
             $_createUserLink = '/admin/users/user/create-user';
         }
         elseif (Yii::$app->user->can('ca-user-create'))
         {
             $_createUserLink = '/admin/users/user/create-ca-user';
         }
         elseif (Yii::$app->user->can('oo-user-create'))
         {
             $_createUserLink = '/admin/users/user/create-oo-user?org_id=' . \Yii::$app->user->identity->organization->id;
         }
         elseif (Yii::$app->user->can('inst-user-create'))
         {
             $_createUserLink = '/admin/users/user/create-installation-user';
         }
    ?>
    
    <div>
        <?= GridView::widget([
            'dataProvider'=> $dataProvider,
            'filterModel' => $searchModel,
            'rowOptions' => function($model) {
                return $model->status == User::STATUS_DELETED ? ['class' => 'danger'] : [];
            },
            'pjax' => true,
            'toolbar' =>  [
                ['content'=>
                    $_createUserLink != '' ? Html::a('<i class="glyphicon glyphicon-plus"></i>', Yii::$app->getUrlManager()->createUrl($_createUserLink), ['class'=>'btn btn-success', 'title'=>Yii::t('app', 'Add user')]) : '',
                    Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['grid-demo'], ['data-pjax'=>0, 'class' => 'btn btn-default', 'title'=>Yii::t('app', 'Reset Grid')])
                ],
                '{toggleData}',
            ],
            'columns' => $columns,
            'responsive'=>true,
            'hover'=>true,
            'export'=>false,
            'panel' => [
                    'type' => GridView::TYPE_PRIMARY,
                    'heading' => '<i class="glyphicon glyphicon-user"></i>' . ' ' . \Yii::t('app', 'Users list'),
                ],                        
        ]) ?>
    </div>
</div>
