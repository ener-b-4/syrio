<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\helpers\Url;
use app\widgets\ValidationReportWidget;



/**
 * Description of account
 *
 * @author vamanbo
 */

/* @var $this \yii\web\View */
/* @var $model app\models\User */

$this->title=Yii::t('app', 'Account');

$breadcrumbs = [
	[
		'label' => Yii::t('app', 'Management'),
		'url' => \Yii::$app->urlManager->createUrl(['/management/']),
		//'template' =>"<li>{link}</li>
	],
];
$this->params['breadcrumbs'] = $breadcrumbs;

$this->params['breadcrumbs'][] = $this->title;



/* use this on any view to include the actionmessage logic and widget */
//include Yii::getAlias('@app') . '\views\shared_parts\message_snippet.php';
include Yii::getAlias('@app') . '/views/shared_parts/message_snippet.php';

if ($model->hasErrors())
{
    echo ValidationReportWidget::widget([
        'title' => \Yii::t('app','Change {evt}', ['evt'=>\Yii::t('app', 'password')]),
        'errors_message' => \Yii::t('app','Unable to change your password! Please review the information below and try again.'),
        'errors'=>$model->errors,
        'no_errors_message' => \Yii::t('app','Your password has been successfully modified.', []),
    ]);
}                            

$title = Yii::t('app', 'Account');


/* get the information related to the current user */

?>

<div class='container'>
    <div class='row'>
        <div class='col-lg-12 sy_pad_bottom_18'>
            <h1><?= Yii::t('app', 'Account') ?></h1>    
        </div>
        <div id='leftPannel' class="col-md-2 col-sm-3 col-xs-12">
            <div class='panel panel-default'>
                <div class='panel-heading'>
                    <strong>
                    <?= ucfirst(Yii::t('app', 'actions')) ?>
                    </strong>
                </div>
                <ul class='list list-unstyled list-group'>
                    <li class='list-group-item'>                        
                        <?= Html::a(\Yii::t('app','Change {evt}', ['evt'=>\Yii::t('app', 'password')]), Url::to(['admin/users/user/change_password']), []) ?>
                    </li>
                    <li class='list-group-item'>
                        <?= Html::a(Yii::t('app','Change {evt}', ['evt'=>lcfirst(\Yii::t('app', 'Username'))]), Url::to(['admin/users/user/change_username']), []) ?>
                    </li>
                    <li class='list-group-item'>
                        <?= Html::a(Yii::t('app','Change contact details'), Url::to(['admin/users/user/change_contact_details']), []) ?>
                    </li>
                </ul>
            </div>
        </div>
        
        <?php
        if (\Yii::$app->user->can('sys_admin')) {
            echo $this->render('_account_sys', ['model' => $model]);
        }
        elseif ($model->isOperatorUser || $model->isInstallationUser) 
        {
            echo $this->render('_account_oo', ['model' => $model]);
        }
        elseif ($model->isCaUser) 
        {
            echo $this->render('_account_ca', ['model' => $model]);
        }

        ?>
    </div>
</div>


