<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use kartik\form\ActiveForm;
use app\models\admin\users\User;
use yii\helpers\Html;

/**
 * The form used for adding / editing a Operator/Owner user
 *
 * @author vamanbo
 * 
 * 20/07/2015
 */

/* @var $form kartik\form\ActiveForm */
/* @var $model \app\models\User the user registration form */
/* @var $op_roles app\models\admin\rbac\Role the Roles associated with Operators/Owners */
/* @var $organization Operators Organization */

?>

    
<?php
$form = ActiveForm::begin();
?>

<p>
    <?= Html::encode(Yii::t('app', 'Organization') . ': ') ?>
    <?= $organization->organization_acronym . ' - ' . $organization->organization_name ?>
</p>

<?= $form->field($model, 'org_id')->hiddenInput()->label(null, ['class'=>'hidden']) ?>

<?= $form->field($model, 'first_names')->textInput() ?>
<?= $form->field($model, 'last_name')->textInput() ?>

<?= $form->field($model, 'role_in_organization')->textInput() ?>

<?= $form->field($model, 'phone')->textInput() ?>
<?= $form->field($model, 'email')->textInput() ?>

<?php

/*
echo '<pre>';
echo var_dump($ca_roles);
echo '</pre>';

echo '<pre>';
echo var_dump($model->currentRoles);
echo '</pre>';
*/

?>

<?php // $form->field($model, 'myRoles')->checkboxList($model->allRoles) ?>
<?= $form->field($model, 'currentRoles')->checkboxList($oo_roles) ?>

<div class="row">
    <div class="col-md-3 col-sm-3 col-xs-4 form-group" >
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app/commands', 'Submit') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
</div>

<?php
ActiveForm::end();
?>
