<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use kartik\grid\GridView;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\components\helpers\AdminHelper;

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$possible_r = \yii\helpers\ArrayHelper::map(Yii::$app->user->getIdentity()->possibleRoles, 'name', 'name');
$links = [
    0 => Yii::t('app', 'System Administrator'),
    1 => Yii::t('app', 'Competent Authority'),
    2 => Yii::t('app', 'Operators/Owners'),
    3 => ucfirst(Yii::t('app', 'installation'))
];

$possible_roles = [
    $links[0] => [],
    $links[1] => [],
    $links[2] => [],
    $links[3] => []
];

if (Yii::$app->user->identity->isOperatorUser) {
    unset($possible_roles[$links[0]]);
    unset($possible_roles[$links[1]]);
}

if (Yii::$app->user->identity->isInstallationUser) {
    unset($possible_roles[$links[2]]);
}


foreach ($possible_r as $key=>$value) {
    $item = AdminHelper::getRoles()[$key];
    //$possible_roles[$key] = AdminHelper::getRoles()[$key]['short'];
    $possible_roles[$links[$item['grade']]][$key] = AdminHelper::getRoles()[$key]['short'];
}

//print_r($possible_roles);

$columns = [
                //the icon column
                [
                    'class' => '\kartik\grid\DataColumn',
                    //'attribute' => 'organizationType',
                    'value' => function($data) {
                        if (isset($data->organization)) {
                            switch ($data->organization->organizationType)
                            {
                                case app\modules\management\models\Organization::COMPETENT_AUTHORITY:
                                    $imgUrl = Yii::getAlias("@web") . '/css/images/24x24/ca_generic.png';
                                    return '<img src="' . $imgUrl . '" />';
                                    break;
                                case app\modules\management\models\Organization::OPERATORS_OWNERS:
                                    $imgUrl = Yii::getAlias("@web") . '/css/images/24x24/operator_black.png';
                                    return '<img src="' . $imgUrl . '" />';
                                    break;
                                default:
                                    return '-';
                            }
                        }
                    },
                    'options' => [
                        'width'=> '50px',
                    ],
                    'format' => 'html',
                ],
    
];
                    
if (Yii::$app->user->can('sys_admin') || Yii::$app->user->can('op_admin') || Yii::$app->user->can('inst_admin') || Yii::$app->user->can('ca_admin')) {
    //include the username
    $columns[] = [
                    'class' => '\kartik\grid\DataColumn',
                    'attribute' => 'username',
                    'label' => Yii::t('app', 'Username')
                ];

}

$the_rest = [
                /*
                [
                    'class'=>'kartik\grid\ExpandRowColumn',
                    'value'=>function($model, $key, $index, $column) {
                        return GridView::ROW_COLLAPSED;
                    },
                    'detail'=>function($model, $key, $index, $column) {
                        $s =    "<p>"
                                . "Details: " // . $model->caOrganizations[0]->organization_name . "<br/>"
                                . "Member of: " . buildAssignmentsHtml($model)
                                . "</p>";
                        return $s;
                    },
                    'expandTitle' => Yii::t('app', 'Show details'),
                    'collapseTitle' => Yii::t('app', 'Hide details'),
                    'expandAllTitle' => Yii::t('app', 'Show details (all)'),
                    'collapseAllTitle' => Yii::t('app', 'Hide details (all)'),
                ],
                 * 
                 */
                [
                    'class' => '\kartik\grid\DataColumn',
                    'attribute' => 'full_name',
                    'label' => \Yii::t('app', 'Full name'),
                    //'value' => 'full_name',
                    'value' => function($data)
                    {
                        if ($data->id == Yii::$app->user->id) {
                            return '<b>' . $data->full_name . '</b>';
                        }
                        else
                        {
                            return $data->full_name;
                        }
                    },
                    'options' => [
                        'width'=> '325px',
                    ],
                    'format' => 'html',
                ],
                [
                    'class' => '\kartik\grid\DataColumn',
                    'attribute' => 'assignments',
                    'label' => \Yii::t('app', 'Role(s)'),
                    'value' => function($data) {
                        return buildAssignmentsHtml($data);
                    },
                    'filterType' => GridView::FILTER_SELECT2,   // '\kartik\widgets\Select2',
                    'filter' => $possible_roles,
                    'filterWidgetOptions' => [
                        'pluginOptions' => ['allowClear'=>true],
                    ],
                    'filterInputOptions' => ['placeholder'=>'-- '.Yii::t('app','any').' --'],
                    'format' => 'raw',
                ],
                [
                    'class' => '\kartik\grid\DataColumn',
                    'attribute' => 'org_acronym',
                    'label' => Yii::t('app', 'Organization'),
                    'format' => 'html',
                    'value' => function($data) {
                        $retS = '';
                        //(start) Author: vamanbo   Date: 24.08.2015  - on BUG 20: fix id parameter
                        $retS = isset($data->organization->organization_acronym) ? 
                                '<a href = "' . Url::to([
                                        'management/organization/view', 
                                        'id'=>$data->organization->id,
                                        ]) . '" title = "' . $data->organization->organization_name . '">'
                                    . 
                                $data->organization->organization_acronym . 
                                '</a></em>'
                                : 
                                '-';
                        //(end) Author: vamanbo   Date: 24.08.2015  - on BUG 20: fix id parameter
                        if ($data->isInstallationUser)
                        {
                            $retS .= ' / <span class="icon-syrio_glyphs"></span><em>'
                                    . '<a href = "' . Url::to([
                                        'management/installations/view', 
                                        'id'=>$data->installation->id,
                                        'operator_id'=>$data->installation->operator_id]) 
                                    . '" title="' . $data->installation->type . '">'
                                    . $data->installation->name . '</a></em>';
                        }
                        return $retS;
                    },
                ],
                            
                'role_in_organization',
                /*
                [
                    'class' => '\kartik\grid\DataColumn',
                    'attribute' => 'organization.organization_acronym',
                    'value' => 'organization.organization_acronym',
                ],
                 * 
                 */
                [
                    'class' => '\kartik\grid\ActionColumn',
                    'header' => ucfirst(\Yii::t('app', 'actions')),
                    'buttons' => [
                        'delete' => function ($url, $model, $key) {
                                //extra check for self
                                if (!Yii::$app->user->can('user-delete', ['id'=>$model->id]) || Yii::$app->user->id == $key)
                                {
                                    return '';
                                }

                                //$url = 'delete';
                                $msg = Yii::t('app', 'Are you sure you want to delete {data}?', [
                                    'data'=>$model->full_name]);
                                $msg .= ' ';
                                $msg .= Yii::t('app', 'Operation CAN be undone.');
                                return $model->status !== 0 ? 
                                        Html::a('<i class="glyphicon glyphicon-trash"></i>', $url, [
                                            'title' => Yii::t('app', 'Remove'),
                                            'data-confirm' => $msg,
                                            'data-method' => 'POST']) : '';
                            },
                        'purge' => function ($url, $model, $key) {
                                if (!Yii::$app->user->can('user-purge', ['id'=>$model->id]))
                                {
                                    return '';
                                }
                                if ($model->isCaUser || $model->isOperatorUser || $model->isInstallationUser) {

                                    if (!$model->canBePurged) {
                                        return '';
                                    }
                                }
                    
                                $msg = Yii::t('app', 'Are you sure you want to PERMANENTLY delete {data}?', [
                                    'data'=>$model->full_name]);
                                $msg .= ' ';
                                $msg .= Yii::t('app', 'Operation CAN NOT be undone.');
                                return $model->status === 0 ? 
                                        Html::a('<i class="glyphicon glyphicon-remove"></i>', $url, [
                                            'title' => Yii::t('app/commands', 'Purge'),
                                            'data-confirm' => $msg,
                                            'data-method' => 'POST']) : '';
                            },
                        'reactivate' => function ($url, $model, $key) {
                                return $model->status === 0 ? 
                                        Html::a('<i class="glyphicon glyphicon-repeat"></i>', $url, [
                                            'title' => Yii::t('app', 'Reactivate'), 
                                            'data-method'=>'POST']) : '';
                            },
                        'edit-ca-user' => function ($url, $model, $key) {
                                //get the type of the $model user
                                if ($model->isCaUser)
                                {
                                    if (Yii::$app->user->can('user-edit', ['id'=>$model->id, 'org_id'=>$model->organization->id]))
                                    {
                                        return Html::a('<i class="glyphicon glyphicon-pencil"></i>', $url, ['title' => Yii::t('app', 'Edit')]);
                                    }
                                }
                                
                                //return $model->status === 10 ? Html::a('my_action', $url) : '';
                            },
                        'edit-oo-user' => function ($url, $model, $key) {
                                //get the type of the $model user
                                if ($model->isOperatorUser)
                                {
                                    if (Yii::$app->user->can('user-edit', ['user'=>$model]))
                                    {
                                        return Html::a('<i class="glyphicon glyphicon-pencil"></i>', $url, ['title' => Yii::t('app', 'Edit')]);
                                    }
                                }
                                
                                //return $model->status === 10 ? Html::a('my_action', $url) : '';
                            },
                        'edit-installation-user' => function ($url, $model, $key) {
                                //get the type of the $model user
                                if ($model->isInstallationUser)
                                {
                                    if (Yii::$app->user->can('user-edit', ['user'=>$model]))
                                    {
                                        return Html::a('<i class="glyphicon glyphicon-pencil"></i>', $url, ['title' => Yii::t('app', 'Edit')]);
                                    }
                                }
                                
                                //return $model->status === 10 ? Html::a('my_action', $url) : '';
                            },
                    ],
                    'template' => '{view} {edit-ca-user} {edit-oo-user} {edit-installation-user} {reactivate} {delete} {purge}',
                    //'deleteOptions' => ['label' => '<i class="glyphicon glyphicon-remove"></i>']
                ],
            ];

    /* merge all the other columns */
    $columns = ArrayHelper::merge($columns, $the_rest);

