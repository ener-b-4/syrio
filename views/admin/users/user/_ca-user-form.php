<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use kartik\form\ActiveForm;
use app\models\admin\users\User;
use yii\helpers\Html;

/**
 * The form used for adding / editing a Competent Authority user
 *
 * @author vamanbo
 * 
 * 13/07/2015
 */

/* @var $form kartik\form\ActiveForm */
/* @var $model \app\models\User the user registration form */
/* @var $ca_roles app\models\admin\rbac\Role the Roles associated with Competent Authorities */
/* @var $organizations mixed the array of OrganizationAcronym -> Organization_name */
?>

    
<?php
$form = ActiveForm::begin();
?>

<?= $form->field($model, 'org_id')->dropDownList($organizations, ['prompt'=>'- select user organization -']) ?>

<?= $form->field($model, 'first_names')->textInput() ?>
<?= $form->field($model, 'last_name')->textInput() ?>

<?= $form->field($model, 'role_in_organization')->textInput() ?>

<?= $form->field($model, 'phone')->textInput() ?>
<?= $form->field($model, 'email')->textInput() ?>

<?php

/*
echo '<pre>';
echo var_dump($ca_roles);
echo '</pre>';

echo '<pre>';
echo var_dump($model->currentRoles);
echo '</pre>';
*/

?>

<?php // $form->field($model, 'myRoles')->checkboxList($model->allRoles) ?>
<?= $form->field($model, 'currentRoles')->checkboxList($ca_roles) ?>

<div class="row">
    <div class="col-md-3 col-sm-3 col-xs-4 form-group" >
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app/commands', 'Submit') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
</div>

<?php
ActiveForm::end();
?>
