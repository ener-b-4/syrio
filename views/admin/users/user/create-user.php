<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\web\View;
use yii\helpers\ArrayHelper;
use kartik\helpers\Html;
use kartik\form\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\admin\CreateUserModel */
/* @var $targets    array the array of CA countries
 *              OR  array of Operators Organizations (available in step 2) */
/* @var $target string the selected CA OR Organization (the same with $model->target) */

/* breadcrumbs section */
use yii\helpers\Url;
$breadcrumbs = [
    [
        'label' => Yii::t('app', 'Management'),
        'url' => Url::to(['/management/']),
        //'template' =>"<li>{link}</li>
    ],
    [
        'label' => ucfirst(Yii::t('app', 'users')),
        'url' => Url::to(['/admin/users/user']),
        //'template' =>"<li>{link}</li>
    ],
    [
        'label' => Yii::t('app', 'Create'),
        //'url' => Url::to(['/admin/users/user/create-user']),
        //'template' =>"<li>{link}</li>
    ],
];
$this->params['breadcrumbs'] = $breadcrumbs;

$this->title = Yii::t('app', 'Create {evt}', ['evt'=>Yii::t('app', 'user')]);


$user_types = [
    'ca' => Yii::t('app', '{evt} user', ['evt'=>\Yii::t('app', 'Competent Authority')]),
    'oo' => Yii::t('app', '{evt} user', ['evt'=>\Yii::t('app/crf', 'Operator/owner')]),
    'inst' => Yii::t('app', '{evt} user', ['evt'=>ucfirst(\Yii::t('app', 'installation'))]),
];

/* use this on any view to include the actionmessage logic and widget */
//include Yii::getAlias('@app') . '\views\shared_parts\message_snippet.php';
include Yii::getAlias('@app') . '/views/shared_parts/message_snippet.php';

if ($model->hasErrors())
{
    echo ValidationReportWidget::widget([
        'title' => \Yii::t('app','Register {evt}', ['evt'=>Yii::t('app', 'CA user')]),
        'errors_message' => \Yii::t('app','Unable to register the user! Please review the information below and try again.'),
        'errors'=>$model->errors,
        'no_errors_message' => \Yii::t('app','User {evt} has been created.', ['evt'=>$model->username]),
    ]);
}                            

$title = Yii::t('app', 'Register new user') . ' - ' . Yii::t('app', 'select user type');

?>

<h1><?= Yii::t('app', 'Registered Users') ?></h1>    
<h3><span class="text-primary"><?= Html::encode($title) ?></span></h3>
<p class="text-info">
    <?= Yii::t('app', 'Please select the type of user you would like to create.'); ?>
</p>

<?php
$form = ActiveForm::begin();
?>

<div>
    <div class="col-lg-12">
        <?php
            if (isset($targets))
            {
                echo $form->field($model, 'user_type')->dropDownList($user_types, [
                    'prompt'=>'- select user type -',
                    'disabled' => true])->label(ucfirst(\Yii::t('app', 'type')));
                
                echo $form->field($model, 'user_type')->hiddenInput(['class'=>'collapsed'])->label(null, ['class'=>'hidden']);  //I need this because a disabled dropdown DOES NOT send the model property
                
                if ($model->user_type == 'ca')
                {
                    echo $form->field($model, 'target')
                                ->dropDownList(ArrayHelper::map($targets, 'country', 'short_name'), ['prompt'=>\Yii::t('app', '(none)')]);
                }
                elseif ($model->user_type == 'oo')
                {
                    echo $form->field($model, 'target')
                                ->dropDownList(ArrayHelper::map($targets, 'id', 'full_name'), ['prompt'=>\Yii::t('app', '(none)')])
                                ->label(ucfirst(\Yii::t('app', 'Organization')));
                }
                elseif ($model->user_type == 'inst')
                {
                    echo $form->field($model, 'target')
                                ->dropDownList(ArrayHelper::map($targets, 'id', 'full_name'), ['prompt'=>\Yii::t('app', '(none)')])
                                ->label(ucfirst(\Yii::t('app', 'installation')));
                }
            }
            else
            {
                //step 1
                echo $form->field($model, 'user_type')
                        ->dropDownList($user_types, ['prompt'=>\Yii::t('app', '(none)')])
                        ->label(ucfirst(\Yii::t('app', 'type')));
            }
        
        ?>
        <?php // isset($targets) ? $form->field($model, 'target')
            //->dropDownList(ArrayHelper::map($targets, 'country', 'short_name'), ['prompt'=>'- select competent authority country -']) : '' ?>
    </div>
</div>

<div class="row">
    <div class="col-md-3 col-sm-3 col-xs-4 form-group" >
        <?= Html::submitButton(Yii::t('app', 'Next'), ['class' => 'btn btn-primary']) ?>
    </div>
</div>

<?php
ActiveForm::end();
?>

