<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;

use yii\widgets\DetailView;

/**
 * Description of view
 *
 * @author vamanbo
 */

/* @var $this yii\web\View */
/* @var $model app\models\User */

/* breadcrumbs section */
use yii\helpers\Url;
$breadcrumbs = [
    [
        'label' => Yii::t('app', 'Management'),
        'url' => Url::to(['/management/']),
        //'template' =>"<li>{link}</li>
    ],
    [
        'label' => ucfirst(Yii::t('app', 'users')),
        'url' => Url::to(['/admin/users/user']),
        //'template' =>"<li>{link}</li>
    ],
    [
        'label' => Yii::t('app', 'View'),
    ]
];
$this->params['breadcrumbs'] = $breadcrumbs;

$this->title = Yii::t('app', 'User details');


//get the objects I need
//$address = $model->address;
//$address_html = \app\widgets\AddressWidget::widget([
//    'address' => $model->address,
//    'style' => 0
//]);

?>

    <h1><?= Html::encode($this->title) ?></h1>

    <!-- CONTACT BLOCK -->
    <div id="contactblock" class="container sy_pad_top_36">

    <?php

    $columns = [
        'first_names',
        'last_name',
        'phone',
        [
            'label' => ucfirst(\Yii::t('app', 'Email')),
            'value' => '<a href="mailto:' . $model->email . '">' . $model->email . '</a>',
            'format' => 'raw'
        ],
//        [
//            'label' => ucfirst(\Yii::t('app', 'address')),
//            'value' => $address_html,
//            'format' => 'raw'
//        ],
        
    ];
    
    


    $sec_columns = [];
    $sec_columns[] = [
        'label' => ucfirst(\Yii::t('app', 'created by')),
        'value' => $model->created_by
    ];
    $sec_columns[] = [
        'label' => ucfirst(\Yii::t('app', 'created at')),
        'value' => $model->created_at
    ];

    if (isset($model->modified_by) && strlen($model->modified_by) > 0) {
        $sec_columns[] = [
            'label' => ucfirst(\Yii::t('app', 'modified at')),
            'value' => $model->modifiedBy->full_name
        ];
        $sec_columns[] = [
            'label' => ucfirst(\Yii::t('app', 'modified by')),
            'value' => $model->modified_at
        ];
    } else {
        $sec_columns[] = [
            'label' => \Yii::t('app', 'Modified'),
            'value' => null
        ];
    }

?>
    
    
    <div class="row sy_pad_top_18">
        <div class="col-sm-9">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => $columns
            ]) ?>
        </div>
<!--        <div class="col-sm-3">
            <?php// DetailView::widget([
                //'model' => $model,
                //'attributes' => $sec_columns
            ]) ?>
        </div> -->
    </div>
    
</div>


