<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\web\View;
use yii\helpers\ArrayHelper;
use kartik\helpers\Html;
use app\widgets\ValidationReportWidget;

/* @var $this yii\web\View */
/* @var $form kartik\form\ActiveForm */
/* @var $model app\models\User */
/* @var $organization Operators Organization
/* @var $op_roles app\models\admin\rbac\Role the Roles associated with Competent Authorities */


/* breadcrumbs section */
use yii\helpers\Url;
$breadcrumbs = [
    [
        'label' => Yii::t('app', 'Management'),
        'url' => Url::to(['/management/']),
        //'template' =>"<li>{link}</li>
    ],
    [
        'label' => ucfirst(Yii::t('app', 'users')),
        'url' => Url::to(['/admin/users/user']),
        //'template' =>"<li>{link}</li>
    ],
    [
        'label' => Yii::t('app', 'Create'),
        'url' => Url::to(['/admin/users/user/create-user']),
        //'template' =>"<li>{link}</li>
    ],
    [
        'label' => Yii::t('app', '{evt} user', ['evt'=>Yii::t('app/crf', 'Operator/owner')]),
        //'template' =>"<li>{link}</li>
    ],
];
$this->params['breadcrumbs'] = $breadcrumbs;

$this->title = Yii::t('app', 'Create {evt}', ['evt'=>Yii::t('app', 'user')]);


$roles = ArrayHelper::map($op_roles, 'name', 'name');

/* use this on any view to include the actionmessage logic and widget */
//include Yii::getAlias('@app') . '\views\shared_parts\message_snippet.php';
include Yii::getAlias('@app') . '/views/shared_parts/message_snippet.php';

if ($model->hasErrors())
{
    echo ValidationReportWidget::widget([
        'title' => \Yii::t('app','Register {evt}', ['evt'=>Yii::t('app', 'CA user')]),
        'errors_message' => \Yii::t('app','Unable to register the user! Please review the information below and try again.'),
        'errors'=>$model->errors,
    ]);
}                            

$title = Yii::t('app', 'Register new user');

?>

<h1><?= Yii::t('app', 'Registered Users') ?></h1>    
<h3><span class="text-primary"><?= Html::encode($title) ?></span></h3>


<div>
    <div class="col-lg-12">
    <?=
        $this->render('_oo-user-form', ['model'=>$model, 'oo_roles'=>$roles, 'organization'=>$organization]);
    ?>
    </div>
</div>



