# SyRIO

**SyRIO** (*System for Reporting the Incidents in the Off-shore Operations*) is the IT solution proposed by the European Commission ([DG ENERGY](https://ec.europa.eu/energy/):earth_americas:) to the Member States (MS) to be adopted as the communication platform of the offshore incidents from the Operators/Owners to the Competent Authorities, as required by the Offshore Safety Directive ([Directive 2013/30/EU the OSD](http://euoag.jrc.ec.europa.eu/files/attachments/osd_final_eu_directive_2013_30_eu1.pdf):earth_americas:) and the [Implementing Regulation 1112/2014](http://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32014R1112):earth_americas:.

SyRIO has been designed to support operators, Member States and the Commission to meet the reporting requirements through:

- providing offshore operators and owners an efficient and secure way for sending incident/major accident reports to the MS Competent Authorities;
- providing the MS Competent Authorities:
    - the ways and means of assessing the reports submitted by the operators and owners;
    - the tools for drafting the Annual Report (to be submitted to the Commission) based on the aggregated information in the individual reports;
- allowing MS Competent Authorities to keep track of (and store information on) all incidents/major accidents which occurred in their jurisdictions;
- providing operators/owners and the Member States the necessary means for managing incident-related data.

The development of SyRIO has been commissioned by [DG ENERGY](https://ec.europa.eu/energy/):earth_americas: to [DG JRC Directorate C. Energy, Transport and Climate (ETC)](https://ec.europa.eu/jrc/en):earth_americas:.

The platform has been distributed by DG ENERGY, under the [EUPL license](https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12):earth_americas: following the [Commission Decision of 28 October 2016](http://eur-lex.europa.eu/legal-content/EN/ALL/?uri=CELEX:32016D1115(01)):earth_americas:, to all Competent Authorities of the MS. The Competent Authorities should, upon adopting the solution, install SyRIO on their own servers (application and database), thus becoming the owners of the platform (application and data).


For more information related to SyRIO, please consult the [User manual](https://code.europa.eu/ener-b-4/syrio/-/wikis/SyRIO/Documentation/syrio_user_manual_1.1.3.pdf).



## License
European Union Public Licence [EUPL](https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12):earth_americas:

## Installation
To install SyRIO, please follow the procedures described in the Wiki [Installation](https://code.europa.eu/ener-b-4/syrio/-/wikis/SyRIO/Installation/PHP5.6) section.
