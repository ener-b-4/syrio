<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\utilities;

use yii\helpers\Html;


/**
 * Data Column for created ad and by
 *
 * @author vamanbo
 */
class GraphicBoolDataColumn extends \kartik\grid\DataColumn {

    //public $attribute = '';
    
    public function init() {

        parent::init();
        
        //$this->content = [$this, 'makeContent'];
        
    }

    protected function renderDataCellContent($model, $key, $index) {
        //parent::renderDataCellContent($model, $key, $index);
        if (is_a($model, \app\models\ca\IncidentCategorization::className())) {
            return $this->makeContentIncidentCategorization($model);
        } else {
            //else generic bool
            $attribute = $this->attribute;

            if (!isset($model->$attribute) || !$model->$attribute) {
                $glyph_class = "glyphicon-unchecked";
            } else {
                $glyph_class = "glyphicon-check";
            }

            return Html::tag('span', '', [
                'class' => 'glyphicon ' . $glyph_class
            ]);
        }
    }
    
    
    protected function makeContentIncidentCategorization($model) {
        $attribute = $this->attribute;
        $glyph_class = "glyphicon glyphicon-unchecked unassessed-disabled";
        $glyph_title = 'Attribute not set';
        $data_title = 'AVB checkbox';
        
        if (isset($attribute)) {
            if ($attribute == 'is_done') {
                //return $model->$attribute;
                $data_title = \Yii::t('app', 'Assessment as Major Accident');
                
                switch ($model->$attribute) {
                    case '-1':
                        //return '-1';
                        //rejected
                        $glyph_class = "glyphicon glyphicon-remove rejected";
                        $glyph_title = \Yii::t('app', 'Report {evt}.' , [
                            'evt' => strtoupper(\Yii::t('app', 'rejected'))
                        ]);
                        break;
                    case 0:
                        //unassessed
                        $glyph_class = "glyphicon glyphicon-unchecked unassessed";
                        $glyph_title = \Yii::t('app', 'Report {evt}.' , [
                            'evt' => \Yii::t('app', 'unassessed')
                        ]);
                        break;
                    case 1:
                        //accepted
                        if ($model->is_major) {
                            //red
                            $glyph_class = "glyphicon glyphicon-check assessed-major";
                            $glyph_title = \Yii::t('app', 'Report {evt}.' , [
                                'evt' => strtoupper(\Yii::t('app', 'accepted'))
                            ]). '<br/>' .
                                \Yii::t('app', 'Event categorized as {evt}', [
                                    'evt' => '<em>' . \Yii::t('app', 'Major Accident') . '</em>'
                                ]);
                            //$glyph_title = \Yii::t('app', 'Major Accident');
                        } else {
                            //green
                            $glyph_class = "glyphicon glyphicon-check text-success";
                            $glyph_title = \Yii::t('app', 'Report {evt}.' , [
                                'evt' => strtoupper(\Yii::t('app', 'accepted'))
                            ]). '<br/>' .
                                \Yii::t('app', 'Event categorized as {evt}', [
                                    'evt' => '<em>' . \Yii::t('app', 'Minor Incident') . '</em>'
                                ]);
                        }
                        break;
                }
                    
            } //end attribute is_done
            elseif ($attribute == 'cpf_assessed') {
                //can be:
                $data_title = \Yii::t('app', 'Report assessed in terms of CPF Section 4.');

                if (!is_null($model->$attribute)) {
                    switch ($model->$attribute) {
                        case -10:
                            //hard-coded value returned from IncidentCategorization->cpf_assessed
                            //it means that the current IncidentCategorization has not been included in the list
                            $glyph_title = \Yii::t('app', 'The report is not included in the list to be assessed in terms of Major accident.', [
                                
                            ]);
                            $glyph_title .= \Yii::t('app', 
                                    '<br/>Please <strong>Refresh</strong> the list in <em>Annual report preparation</em>', [
                                
                            ]);
                            $glyph_class = "glyphicon glyphicon-unchecked unassessed-disabled";
                            break;
                        case -20:
                            //hard-coded value returned from IncidentCategorization->cpf_assessed
                            //it means that the current IncidentCategorization has not been included in the list
                            $glyph_title = \Yii::t('app', 'This report is rejected. No need to be assessed for CPF.', [
                                
                            ]);
                            $glyph_class = "glyphicon glyphicon-remove text-muted";
                            break;
                        case \app\modules\cpfbuilder\models\CpfCrfAssessments::ASSESS_STATUS_NEW:
                        case \app\modules\cpfbuilder\models\CpfCrfAssessments::ASSESS_STATUS_IN_PROGRESS:
                            $glyph_title = \Yii::t('app', 'The report {evt} in terms of Major accident.', [
                                'evt' => \Yii::t('app', 'is NOT assessed')
                            ]);
                            $glyph_class = "glyphicon glyphicon-unchecked unassessed";
                            break;
                        case \app\modules\cpfbuilder\models\CpfCrfAssessments::ASSESS_STATUS_FINALIZED:
                            $glyph_title = \Yii::t('app', 'The report {evt} in terms of Major accident.', [
                                'evt' => \Yii::t('app', 'HAS BEEN ASSESSED')
                            ]);
                            $glyph_class = "glyphicon glyphicon-check text-success";                        
                            break;
                        default:
                            $glyph_class = "glyphicon glyphicon-minus";
                            $glyph_title = \Yii::t('app', 'The report {evt} in terms of Major accident.', [
                                'evt' => \Yii::t('app', 'must be assessed first')
                            ]);
                            break;
                    }
                } else {
                    $glyph_title = \Yii::t('app', 'The report {evt} in terms of Major accident.', [
                        'evt' => \Yii::t('app', 'must be assessed first')
                    ]);
                }
            }
        } else {
            //unassessed
            $glyph_class = "glyphicon glyphicon-unchecked unassessed";
            $glyph_title = \Yii::t('app', 'Report {evt}.' , [
                'evt' => \Yii::t('app', 'unassessed')
            ]);
        }
        
        return Html::tag('span', '', [
            'class' => $glyph_class,
            'data-toggle' => 'popover',
            'data-html' => 'true',
            'data-title' => $data_title,
            'data-content' =>
            $glyph_title
            
        ]);
        
    }
    
    
}
