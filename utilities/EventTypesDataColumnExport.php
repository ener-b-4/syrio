<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\utilities;

use yii\helpers\Html;


/**
 * Data Ciolumn for created ad and by
 *
 * @author vamanbo
 */
class EventTypesDataColumnExport extends \kartik\grid\DataColumn {
    
    public function init() {
        parent::init();
        
        //$this->content = [$this, 'makeContent'];
    }
    
    protected function renderDataCellContent($model, $key, $index) {
        //parent::renderDataCellContent($model, $key, $index);
        return $this->makeContent($model);
    }    
    
    protected function makeContent($model) {
        if (is_a($model, \app\models\ca\IncidentCategorization::className())) {
            $is_ca = true;
        }

        if (is_a($model, \app\models\events\EventDeclaration::className())) {
            $is_oo = true;
        }
        
        if (isset($model->report)) {
            $ret = [];
            foreach (range('a', 'j') as $letter) {
                $is_attr = 'is_'.$letter;
                
                
                if ($model->report->$is_attr) {
                    $ret[] = strtoupper($letter) . '. ' . \app\models\crf\CrfHelper::SectionsTitleShort()[strtoupper($letter)];
                }
            }
            
            return implode('<br/>', $ret);
        } else {
            return '(still drafting...)';
        }
        
    }
}
