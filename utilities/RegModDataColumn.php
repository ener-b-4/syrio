<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\utilities;

use yii\helpers\Html;


/**
 * Data Ciolumn for created ad and by
 *
 * @author vamanbo
 */
class RegModDataColumn extends \kartik\grid\DataColumn {
    public function init() {
        
        $this->_view = $this->grid->getView();
        if ($this->mergeHeader && !isset($this->vAlign)) {
            $this->vAlign = GridView::ALIGN_MIDDLE;
        }
        if ($this->grid->bootstrap === false) {
            Html::removeCssClass($this->filterInputOptions, 'form-control');
        }
        $this->parseFormat();
        $this->parseVisibility();
        $this->checkValidFilters();
        parent::init();
        $this->setPageRows();
        $this->initGrouping();
        
        $this->content = [$this, 'makeContent'];
    }
    
    protected function makeContent($model) {
        $url = \Yii::$app->urlManager->createUrl(['/events/event-declaration/history', 'evt_id'=>$model->id]);
        
        $popover = $this->makePopoverElement($this->getValues($model), $model);
        
        //add the second icon (history)
        $history=  Html::a('<span class="fa fa-history"></span>', $url, []);
        
        return $popover . '<br/>'. $history;
    }
    
    protected function makePopoverElement($values, $model=null) {
        $title = \Yii::t('app', 'Audit');
        
        if (isset($model)) {
            if (is_a($model, \app\models\events\EventDeclaration::className())) {
                $title = \Yii::t('app', '{evt} Audit', [
                    'evt'=>\Yii::t('app', 'Event Declaration')
                ]);
            }
        }
        
        return Html::tag(
            'span',
            '',
            [
            'class' => 'audit-toggler glyphicon
            glyphicon-calendar',
            'data-toggle' => 'popover',
            'data-html' => 'true',
            'data-title' => $title,
            'data-content' =>
            $this->makePopoverContent($values)
        ]);
    }
    
    protected function makePopoverContent($values) {
        $formatter = function ($pair) {
            return sprintf(
                "<div><strong>%s:</strong>&nbsp;%s</div>",
                $pair[0],    
                $pair[1]
            );
        };
        $appender = function ($accumulator, $value) {
            return $accumulator . $value;
        };
        return array_reduce(array_map($formatter, $values), $appender, "");
    }        
    
    protected function getValues($model) {
        $ret_values = [];
        
        $ret_values[] = [
                $model->getAttributeLabel('created_at'),
                date('Y-m-d H:i:s', $model->created)
            ];

        if (\app\models\User::findOne($model->created_by) !== NULL) {
            $ret_values[] = [
                $model->getAttributeLabel('created_by'),
                $model->createdBy->full_name
            ];
        }

        if (isset($model->modified_by_id)) {
            $ret_values[] = [
                    $model->getAttributeLabel('modified_at'),
                    date('Y-m-d H:i:s', $model->modified_at)
                ];
            if (\app\models\User::findOne($model->modified_by_id) !== NULL) {
                $ret_values[] = [
                    $model->getAttributeLabel('modified_by'),
                    $model->modifiedBy->full_name
                ];
            }
        }
        
        
        return $ret_values;
    }
    
}
