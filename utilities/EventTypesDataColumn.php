<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\utilities;

use yii\helpers\Html;


/**
 * Data Ciolumn for created ad and by
 *
 * @author vamanbo
 */
class EventTypesDataColumn extends \kartik\grid\DataColumn {
    
    public $forExport = false;
        
    public function init() {
        parent::init();
        
        //$this->forExport = false;
        //$this->content = [$this, 'makeContent'];
    }
    
    protected function renderDataCellContent($model, $key, $index) {
        //parent::renderDataCellContent($model, $key, $index);
        
        if (isset($this->forExport) && $this->forExport) {
            return $this->makeContentForExport($model);;
        }
        
        return $this->makeContent($model);
    }    
    
    protected function makeContent($model) {
        
        if (is_a($model, \app\models\ca\IncidentCategorization::className())) {
            $is_ca = true;
        }

        if (is_a($model, \app\models\events\EventDeclaration::className())) {
            $is_oo = true;
        }
        
        
        if (isset($model->report)) {
            $ret = [];
            foreach (range('a', 'j') as $letter) {
                $is_attr = 'is_'.$letter;
                
                
                if ($model->report->$is_attr) {
                    if (isset($is_ca)) {
                        $url = \Yii::$app->urlManager->createUrl(['//ca/incident-categorization/section', 'id'=>$model->id, 'section'=>$letter]);
                        $a = Html::a('<span class="glyphicon glyphicon-new-window"></span>', $url);
                        $ret[] = strtoupper($letter) . '. ' . \app\models\crf\CrfHelper::SectionsTitleShort()[strtoupper($letter)] . ' ' . $a;
                    }
                    elseif (isset($is_oo)) {
                        $url = \Yii::$app->urlManager->createUrl(['//events/drafts/event-draft/review', 'id'=>$model->report->id]) . '#' . $letter . '_block';
                        $a = Html::a('<span class="glyphicon glyphicon-new-window"></span>', $url);
                        $ret[] = strtoupper($letter) . '. ' . \app\models\crf\CrfHelper::SectionsTitleShort()[strtoupper($letter)] . ' ' . $a;
                    }
                    else {
                        $ret[] = strtoupper($letter) . '. ' . \app\models\crf\CrfHelper::SectionsTitleShort()[strtoupper($letter)];
                    }
                }
            }
            
            return implode('<br/>', $ret);
        } else {
            $sd = \Yii::t('app', 'still drafting');
            return '<em class="sy_alert_color">(' . $sd . '...)</em>';
        }
        
    }
    
    protected function makeContentForExport($model) {
        
        if (is_a($model, \app\models\ca\IncidentCategorization::className())) {
            $is_ca = true;
        }

        if (is_a($model, \app\models\events\EventDeclaration::className())) {
            $is_oo = true;
        }
        
        
        if (isset($model->report)) {
            $ret = [];
            foreach (range('a', 'j') as $letter) {
                $is_attr = 'is_'.$letter;
                
                
                if ($model->report->$is_attr) {
                    if (isset($is_ca)) {
                        $ret[] = strtoupper($letter) . '. ' . \app\models\crf\CrfHelper::SectionsTitleShort()[strtoupper($letter)] . ' ';
                    }
                    elseif (isset($is_oo)) {
                        $ret[] = strtoupper($letter) . '. ' . \app\models\crf\CrfHelper::SectionsTitleShort()[strtoupper($letter)] . ' ';
                    }
                    else {
                        $ret[] = strtoupper($letter) . '. ' . \app\models\crf\CrfHelper::SectionsTitleShort()[strtoupper($letter)];
                    }
                }
            }
            
            return implode('<br/>', $ret);
        } else {
            $sd = \Yii::t('app', 'still drafting');
            return '(' . $sd . '...)';
        }
        
    }
    
    
}
