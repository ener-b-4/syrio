<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\utilities;

use yii\helpers\Html;


/**
 * Data Column for event submission
 *
 * @author vamanbo
 */
class CaRegModDataColumn extends \kartik\grid\DataColumn {
    
    public function init() {
        $this->content = [$this, 'makeContent'];
    }
    
    protected function makeContent($model) {
        $popover = $this->makePopoverElement($model);
        
        return $popover;
    }
    
    protected function makePopoverElement($model=null) {
        $title = \Yii::t('app', 'Reporting dates');
        
        $subTasks = $model->draft->event->getSubmissionTasks();

        $clock_css = '';
        if ($subTasks['late_code'] == 0) {
            $clock_css = 'text-success';
            $text = \Yii::t('app', 'in due time');
        }
        else {
            $clock_css = 'text-danger';
            $text = \Yii::t('app', 'late');
        }
        
        return Html::tag(
            'span',
            '',
            [
            'class' => 'audit-toggler glyphicon
            glyphicon-time ' . $clock_css,
            'data-toggle' => 'popover',
            'data-html' => 'true',
            'data-title' => $title,
            'data-content' =>
            $this->makePopoverContent($subTasks)
        ]) . ' ' .
                "<span class='$clock_css'>" .
                $text . '</span>';
    }
    
    protected function makePopoverContent($subTasks) {
            $glyph_td_css = 'margin: -6px; padding-right: 6px; padding-left: 6px; height: 100%;';
            $late_report_css =  'color: gray';
            
            
            if ($subTasks !== null) {
                
                if ($subTasks['late_code'] == 0) {
                    $late_report_css = 'green';
                    $popover_content[] = '<p style = "color: ' . $late_report_css . '">'
                            . \Yii::t('app', 'This report has been submitted before the deadline.')
                            . '</p>';
                } else {
                    $late_report_css = 'red';
                    $popover_content[] = '<p style = "color: ' . $late_report_css . '">'
                            . \Yii::t('app', 'This report has been submitted after the deadline.')
                            . '</p>';
                }
                
                foreach($subTasks as $key => $value) {
                    if ($key !== 'late_code') {
                        $popover_content[] = '<small><b>' . $key . ':</b> ' . $value . '</small>';
                    }
                    else 
                    {
                        $late_report_css = $value == 1 ? 
                                'color: red' : 
                                'color: green';
                    }
                }
            } else {
                $popover_content[] = '<em class="text-muted">' . \Yii::t('app','The incident must to be reported before having this information available.') . '</span>';
            }
            
            return implode('<br/>', $popover_content);
            
            $popover = Html::tag(
                'a',
                '',
                [
                    'style' => $late_report_css,
                'class' => 'audit-toggler glyphicon
                glyphicon-time',
                'data-toggle' => 'popover',
                'data-html' => 'true',
                'data-title' => $title,
                'data-placement'=>'left',  //to display to the right
                'data-trigger'=>'focus',    //to close at user's next click
                'data-content' =>
                implode('<br/>', $popover_content)
            ]);
            
    }        
    
}
