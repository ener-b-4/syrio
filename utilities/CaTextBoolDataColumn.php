<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\utilities;

use yii\helpers\Html;


/**
 * Data Column for created ad and by
 *
 * @author vamanbo
 */
class CaTextBoolDataColumn extends \kartik\grid\DataColumn {

    //public $attribute = '';
    
    public function init() {

        parent::init();
        
        //$this->content = [$this, 'makeContent'];
        
    }

    protected function renderDataCellContent($model, $key, $index) {
        //parent::renderDataCellContent($model, $key, $index);
        if (is_a($model, \app\models\ca\IncidentCategorization::className())) {
            return $this->makeContentIncidentCategorization($model);
        } else {
            //else generic bool
            $attribute = $this->attribute;

            if (!isset($model->$attribute) || !$model->$attribute) {
                $glyph_class = "text-danger";
                $glyph_content = \Yii::t('app', 'please provide attribute');
            } else {
                $glyph_class = "";
                $glyph_content = $model->$attribute ? \Yii::t('app', 'Yes') : \Yii::t('app', 'No');
            }

            return Html::tag('span', $glyph_content, [
                'class' => $glyph_class
            ]);
        }
    }
    
    
    protected function makeContentIncidentCategorization($model) {
        $attribute = $this->attribute;
        $glyph_class = "";
        $glyph_content = 'please provide attribute';
        
        if (isset($attribute)) {
            if ($attribute == 'is_done') {
                //return $model->$attribute;                                
                switch ($model->$attribute) {
                    case '-1':
                        //return '-1';
                        //rejected
                        $glyph_content = strtoupper(\Yii::t('app', 'rejected' , []));
                        break;
                    case 0:
                        //unassessed
                        $glyph_content = strtoupper(\Yii::t('app', 'unassessed' , []));
                        break;
                    case 1:
                        //accepted
                        if ($model->is_major) {
                            //red
                            $glyph_content = strtoupper(\Yii::t('app', 'major' , []));
                        } else {
                            //green
                            $glyph_content = strtoupper(\Yii::t('app', 'minor' , []));
                        }
                        break;
                }
            } //end attribute is_done
            elseif ($attribute == 'cpf_assessed') {
                if (!is_null($model->$attribute)) {
                    switch ($model->$attribute) {
                        case -10:
                            //hard-coded value returned from IncidentCategorization->cpf_assessed
                            //it means that the current IncidentCategorization has not been included in the list
                            $glyph_content = strtoupper(\Yii::t('app', 'not included in the assessment list' , []));
                            break;
                        case -20:
                            //hard-coded value returned from IncidentCategorization->cpf_assessed
                            //it means that the current IncidentCategorization has not been included in the list
                            $glyph_content = strtoupper(\Yii::t('app', 'no need to assess. report rejected' , []));
                            break;
                        case \app\modules\cpfbuilder\models\CpfCrfAssessments::ASSESS_STATUS_NEW:
                        case \app\modules\cpfbuilder\models\CpfCrfAssessments::ASSESS_STATUS_IN_PROGRESS:
                            $glyph_content = strtoupper(\Yii::t('app', 'unassessed' , []));
                            break;
                        case \app\modules\cpfbuilder\models\CpfCrfAssessments::ASSESS_STATUS_FINALIZED:
                            $glyph_content = strtoupper(\Yii::t('app', 'assessed' , []));
                            break;
                        default:
                            $glyph_content = strtoupper(\Yii::t('app', 'no need to assess. report unassessed as MA.' , []));
                            break;
                    }
                } else {
                    $glyph_content = strtoupper(\Yii::t('app', 'no need to assess. report unassessed as MA.' , []));
                }
            }
        } else {
            //unassessed
            $glyph_content = strtoupper(\Yii::t('app', 'unassessed' , []));
        }
        
        return Html::tag('span', $glyph_content, [
            'class' => $glyph_class,
        ]);
        
    }
    
    
}
