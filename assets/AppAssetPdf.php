<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAssetPdf extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'css/sy_main.css',
        'css/cpf.css',
        'css/sy_pdf.css',
        'css/syrio_pdf.css',
    ];
    public $js = [
        'js/crypto/base64.js',
        'js/crypto/encrypt.js',
        'js/crypto/aes.js',
        'js/fix_nav.js',
        'js/sy_data_toggle.js',
        //'js/less.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'app\assets\AppAsset',                          //This is necessary for dependency between AppAsset and AppAssetPdf
    ];
}
