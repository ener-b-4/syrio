<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'css/sy_main.css',
        'css/sy_pdf.css',
        'css/cpf.css',
        'css/syrio.css'
    ];
    public $js = [
        'js/crypto/base64.js',
        'js/crypto/encrypt.js',
        'js/crypto/aes.js',
        'js/fix_nav.js',
        'js/sy_data_toggle.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
