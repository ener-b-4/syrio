<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\assets;

use yii\web\AssetBundle;

class SyrioGlyphsAsset extends AssetBundle 
{
    public $sourcePath = '@app/web/syrio_glyphs'; 
    public $css = [ 
        'style.css', 
    ]; 
    
}