<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\behaviors;

use \yii\base\Behavior;
use Yii;
use app\models\common\ActionMessage;

/**
 * Handles the session related settings:
 * 
 * - language settings
 *
 * @author vamanbo
 */
class SyrioSessionBehavior extends Behavior {
    
    public function events()
    {
        return [yii\web\Application::EVENT_BEFORE_REQUEST => 'handleBeginRequest'];
    }
    
    public function handleBeginRequest($event)
    {
        if (Yii::$app->getRequest()->getCookies()->has('_lang')
                && array_key_exists(Yii::$app->getRequest()->getCookies()->getValue('_lang'), Yii::$app->params['languages']))
        {
            Yii::$app->language = Yii::$app->getRequest()->getCookies()->getValue('_lang');
        }
    }
    
}
