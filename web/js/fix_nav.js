/* fixing anchor jumps
 * 
 * this fixes the issue with the main menu covering the content when using tag-based navigation in page
 * 
 * How-to:
 * - just add the 'anchor' class to the <div> you want to navigate to
 * 
 *     
 */

var nav_height = 86;

$(window).bind('hashchange', function(e){
    if($(location.hash).hasClass('anchor')){
        scrollBy(0, nav_height);
    }
    return false;
});
$(document).ready(function(){
    if($(location.hash).hasClass('anchor')){
        $('html,body').animate({
            scrollTop: $(location.hash).offset().top - nav_height - 10
        }, 10 );
    }
});