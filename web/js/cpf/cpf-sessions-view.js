$(function() {
    //executes when the DOM is ready
    $('#info-list').on('shown.bs.collapse', function(e) {
        $("#chevron1").addClass('glyphicon-chevron-up').removeClass('glyphicon-chevron-down');
      });

    $('#info-list').on('hidden.bs.collapse', function(e) {
        $("#chevron1").addClass('glyphicon-chevron-down').removeClass('glyphicon-chevron-up');
      });      
      
      
    $('#how-to-list').on('shown.bs.collapse', function(e) {
        $("#chevron2").addClass('glyphicon-chevron-up').removeClass('glyphicon-chevron-down');
      });

    $('#how-to-list').on('hidden.bs.collapse', function(e) {
        $("#chevron2").addClass('glyphicon-chevron-down').removeClass('glyphicon-chevron-up');
      });      
      
});        
