$(function() {
    //executes when the DOM is ready
    //alert('here');
    
    $('.section_4_content').each(function(){
        var id = '#' + $( this ).attr('id');
        $(id).on('shown.bs.collapse', function(e) {
            $ ("#" + $ (this).attr('chevron')).addClass('glyphicon-chevron-up').removeClass('glyphicon-chevron-down');
          });
        $(id).on('hidden.bs.collapse', function(e) {
            $ ("#" + $ (this).attr('chevron')).addClass('glyphicon-chevron-down').removeClass('glyphicon-chevron-up');
          });
    })
    
});        
