/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(function() {
//    if ($("input[name='A1[a1_op]'][value===601]").attr('checked', 'checked')) {
//        $('#txt_a1_op_specify').removeClass('hidden');
//    } else {
//        if (!$('#txt_a1_op_specify').hasClass('hidden')) {
//            $('#txt_a1_op_specify').addClass('hidden');
//        }
//    };
    
    if ($('#rb_axii_code_immediate').attr("checked", "checked")) {
        $('#ignition_divs').removeClass('hidden');
    }
    
    //rb_axii_code_delayed
//    if ($('#rb_axii_code_delayed').attr("checked", "checked")) {
//        $('#rb_axii_code_delayed_toggle').removeClass('hiddenBlock');
//    }
    
    //ignition divs
    if ($('#rb_axii_yes').is(':checked'))
    {
        $('#ignition_divs').removeClass('hidden').addClass('visible');
    }
    else
    {
        $('#ignition_divs').removeClass('visible').addClass('hidden');
    }
    
});


layout_section_1();

function clicked(value)
{
    if (value.id=='radio1')
    {
        //alert($('#a1_content').html);
        $('#a1_content').removeClass('row hiddenBlock').addClass('row visibleBlock');
    }
    else if (value.id=='radio2')
    {
        //alert('r2');
        $('#a1_content').removeClass('row visibleBlock').addClass('row hiddenBlock');
    }
    else if (value.id=='chkNp')
    {
        if ($('#chkNp').is(":checked"))
        {
            $('#a1i_specify_container').removeClass('hiddenBlock').addClass('visibleBlock');
        }
        else
        {
            $('#a1i_specify_container').removeClass('visibleBlock').addClass('hiddenBlock');
        }
    }
    else if (value.id=='chk2p' || value.id=='chkG')
    {
        //get the jquery object corresponding to the radiobutton
        $source = $('#'+value.id);
        //get the jquery object of the other checkbox
        if (value.id=='chk2p')
        {
            $dependingChk = $('#chkG');
        }
        else
        {
            $dependingChk = $('#chk2p');
        }
        $hideContainer = $('#h2s_container');
        
        if ($source.is(":checked"))
        {
            if (!$hideContainer.hasClass('visibleBlock'))
            {
                $('#h2s_container').removeClass('hiddenBlock').addClass('visibleBlock');
            }
        }
        else
        {
            if (!$dependingChk.is(":checked"))
            {
                if (!$hideContainer.hasClass('hiddenBlock'))
                {
                    $('#h2s_container').removeClass('visibleBlock').addClass('hiddenBlock');
                }
            }
        }
    }
    else if (value.id=='rb_axii_yes' || value.id=='rb_axii_no')
    {
        if ($('#rb_axii_yes').is(':checked'))
        {
            $('#ignition_div').removeClass('hiddenBlock').addClass('visibleBlock');
        }
        else
        {
            $('#ignition_div').removeClass('visibleBlock').addClass('hiddenBlock');
        }
            
    }
    else if (value.id==='chk_a1x_d')
    {
        if ($('#chk_a1x_d').is(':checked'))
        {
            $('#chk_a1x_d_container').removeClass('hiddenBlock').addClass('visibleBlock');
        }
        else
        {
            $('#chk_a1x_d_container').removeClass('visibleBlock').addClass('hiddenBlock');
        }
    }
//    else if (value.id==='rb_axii_code_delayed')
//    {
//        $('#rb_axii_code_delayed_toggle').removeClass('hiddenBlock').addClass('visibleBlock');
//    }
//    else if (value.id==='rb_axii_code_immediate')
//    {
//        $('#rb_axii_code_delayed_toggle').removeClass('visibleBlock').addClass('hiddenBlock');
//    }
    //rb_a1op
    else if (value.id==='rb_a1op')
    {
        //console.log(value);
        if (value.value == "601") {
            $('#txt_a1_op_specify').removeClass('hidden');
        } else if (!$('#txt_a1_op_specify').hasClass('hidden')) {
            $('#txt_a1_op_specify').addClass('hidden');
        }
        //$('#rb_axii_code_delayed_toggle').removeClass('visibleBlock').addClass('hiddenBlock');
    }
    
}

function toggle(source, toggledValue)
{
    //alert ($('#' + source.id).val().toString());
    //create the toggled container if from source id
    var toggledID = '#' + source.id + '_toggle';
    
    //get the jquery object corresponding to the dropdown
    $sender = $('#' + source.id);
    //get the jquery object corresponding to the toggable div
    $togDiv = $(toggledID);
    
    if (toggledValue!=$sender.val())
    {
        $togDiv.removeClass('visibleBlock').addClass('hiddenBlock');
    }
    else
    {
        if (!$togDiv.hasClass('visibleBlock'))
        {
            $togDiv.removeClass('hiddenBlock').addClass('visibleBlock');
        }
    }
    
}

function layout_section_1()
{
    
        if ($('#radio1').is(":checked"))
        {
            $('#a1_content').removeClass('row hiddenBlock').addClass('row visibleBlock');
        };    
    
        if ($('#chkNp').is(":checked"))
        {
            $('#a1i_specify_container').removeClass('hiddenBlock').addClass('visibleBlock');
        }
        else
        {
            $('#a1i_specify_container').removeClass('visibleBlock').addClass('hiddenBlock');
        }
        
        if ($('#chkG').is(':checked') || $('#chk2p').is(':checked'))
        {
            $('#h2s_container').addClass('visibleBlock');
        }
        else
        {
            $('#h2s_container').addClass('hiddenBlock');
        }
        
        if ($('#chk_a1x_d').is(':checked'))
        {
            $('#chk_a1x_d_container').addClass('visibleBlock');
        }
        else
        {
            $('#chk_a1x_d_container').addClass('hiddenBlock');
        }

//        if ($('#rb_axii_code_delayed').is(':checked'))
//        {
//            $('#rb_axii_code_delayed_toggle').removeClass('hiddenBlock').addClass('visibleBlock');
//        }
//        else
//        {
//            $('#rb_axii_code_delayed_toggle').removeClass('visibleBlock').addClass('hiddenBlock');
//        }
        
}