$( ".sy_data_toggle" ).each(function(index) {
    $that = $(this);
    //alert('here');
    var $toggle_dest = $that.attr("sy_data_toggle_dest");
    var $dest = $('#' + $toggle_dest);  //the block to be toggled
    
    //get the type of the trigger
    var $trigger_type = $that.attr("sy_data_toggle_src_type");
    //get if toggle is enabled
    var $toggle_enabled = $that.attr("sy_data_toggle_enabled");
    
    if ($toggle_enabled) {
        switch ($trigger_type)
        {
            case 'radio':
                //sy_visible_value doesn't matter here
                if ($that.is(':checked') && $that.attr('sy_data_toggle_visible_value') == 'true') {
                    //$dest.fadeIn();
                    $dest.removeClass('hiddenBlock');
                    $dest.removeClass('hidden');
                }
                else if ($that.is(':checked') && $that.attr('sy_data_toggle_visible_value') != 'true')
                {
                    //$dest.fadeOut();
                    $dest.addClass('hiddenBlock');
                }
                break;
            case 'checkbox':
                if ($that.attr('sy_data_toggle_visible_value') == 'true' &&
                        $that.is(':checked')) {
                    $dest.removeClass('hiddenBlock');
                    $dest.removeClass('hidden');
                }
                else if ($that.attr('sy_data_toggle_visible_value') != 'true' &&
                        $that.is(':checked')) {
                    $dest.addClass('hiddenBlock');
                }
                else {
                    $dest.addClass('hiddenBlock');
                }
                break;
            case 'dropdown':
                if ($that.val() == $that.attr('sy_data_toggle_visible_value'))
                {
                    $dest.removeClass('hiddenBlock');
                    $dest.removeClass('hidden');
                }
                else
                {
                    $dest.addClass('hiddenBlock');
                }
                break;
            case 'text':
                break;
        }
    }
})

$( ".sy_data_toggle" ).change(function() {
    $that = $(this);
    
    var $toggle_dest = $that.attr("sy_data_toggle_dest");
    var $dest = $('#' + $toggle_dest);  //the block to be toggled
    
    //get the type of the trigger
    var $trigger_type = $that.attr("sy_data_toggle_src_type");
    //get if toggle is enabled
    var $toggle_enabled = $that.attr("sy_data_toggle_enabled");
    
    if ($toggle_enabled) {
        switch ($trigger_type)
        {
            case 'radio':
                if ($that.attr('sy_data_toggle_visible_value') == 'true') {
                    //$dest.fadeIn();
                    //$dest.removeClass('hiddenBlock');
                    
                    //$dest.slideToggle(1000);
                    //$dest.slideDown(1000);
                    //$dest.slideDown();
                    $dest.fadeIn();
                }
                else
                {
                    //$dest.fadeOut();
                    //$dest.addClass('hiddenBlock');
                    
                    //$dest.slideToggle();
                    $dest.slideUp();
                }
                break;
            case 'checkbox':
                if ($that.attr('sy_data_toggle_visible_value') == 'true' &&
                        $that.is(':checked')) {
                    $dest.fadeIn();
                }
                else if ($that.attr('sy_data_toggle_visible_value') != 'true' &&
                        $that.is(':checked')) {
                    $dest.fadeOut();
                }
                else {
                    $dest.fadeOut();
                }
                break;
            case 'dropdown':
                if ($that.val() == $that.attr('sy_data_toggle_visible_value'))
                {
                    $dest.fadeIn();
                }
                else
                {
                    $dest.fadeOut();
                }
                break;
            case 'text':
                break;
        }
    }
})


/**
 * Dependable toggle section
 */

$( ".sy_toggle_dest" ).each(function() {
    $that = $( this );
    
    //get the depending_on inputs
    var $dependent_on_ids = $that.attr("sy_toggle_src_dependent_on").split(" ");
    
    //sy_toggle_dest($dependent_on_ids);
    
    if ($dependent_on_ids) {
        $show = false;
        $a='abc';
        
        for (i=0; i<$dependent_on_ids.length; i++) {
            $item = $dependent_on_ids[i];
            //get the inputs corresponding to the dependent item
            var $src = $('#' + $item);  //the source input
            $show = $show || $src.is(':checked');
            $src.attr('sy_toggle_src_dependent_on', $dependent_on_ids);
            $src.attr('sy_toggle_src_dest', $( this ).attr('id'));

            $src.click(function() {
                var $dependent_on_ids = $(this).attr("sy_toggle_src_dependent_on").split(",");
                var $dest = $('#' + $(this).attr('sy_toggle_src_dest')); 
                $show = false;
                $dependent_on_ids.forEach(function($item) {
                    //get the inputs corresponding to the dependent item
                    var $src = $('#' + $item);  //the source input
                    $show = $show || $src.is(':checked');
                })
                if ($show)
                {
                    //$dest.removeClass('hiddenBlock');
                    $dest.fadeIn();
                }
                else
                {
                    //$dest.addClass('hiddenBlock');
                    $dest.fadeOut();
                }
            });
        }
        
    }
    
    if ($show)
    {
        $that.removeClass('hiddenBlock');
    }
    else
    {
        $that.addClass('hiddenBlock');
    }
    
    return;
})

/**
 * sy_click_toggle_section
 */

$( ".sy_click_toggle" ).each(function() {
    $that = $( this );
    
    var $toggle_dest = $that.attr("sy_click_toggle_dest");
    var $dest = $('#' + $toggle_dest);  //the block to be toggled
    
    var $on = $that.attr("sy_click_toggle_visible_on");
    
    if ($that.is(':checked')) {
        if ($on === 'true')
        {
            $dest.removeClass('hiddenBlock');
        }
        else
        {
            $dest.addClass('hiddenBlock');
        }
    }
    else
    {
        if ($on === 'true')
        {
            $dest.addClass('hiddenBlock');
        }
        else
        {
            $dest.removeClass('hiddenBlock');
        }
    }
})


