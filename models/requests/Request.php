<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\requests;

use Yii;

/**
 * This is the model class for table "request".
 *
 * @property integer $id
 * @property integer $request_type
 * @property integer $from
 * @property string $content
 * @property integer $status
 * @property integer $processed_by_id
 * @property integer $processed_at
 * @property boolean $send_justification
 * 
 * @property RequestTypes $requestType
 */
class Request extends \yii\db\ActiveRecord
{
    
    const ANONYMOUS_REQUEST = -1;
    
    const STATUS_OPEN = 0;
    const STATUS_ACCEPTED_CLOSED = 10;
    const STATUS_ACCEPTED_REJECTED = 20;
    
    const DEL_STATUS_DELETED = 0;
    const DEL_STATUS_ACTIVE = 10;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'request';
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'request_type'], 'required'],
            [['id', 'request_type', 'from', 'status', 'processed_by_id', 'processed_at'], 'integer'],
            [['content'], 'string', 'max' => 32255],
            [['processed_by_id', 'processed_at', 'justification', 'send_justification'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Submitted'),
            'request_type' => Yii::t('app', 'Request Type'),
            'from' => Yii::t('app', 'From'),
            'content' => Yii::t('app', 'Content'),
            'status' => Yii::t('app', 'Status'),
            'processed_by_id' => Yii::t('app', 'Processed By ID'),
            'processed_at' => Yii::t('app', 'Processed At'),
            'justification' => Yii::t('app', 'Justification'),
        ];
    }

    public static function AllLabels()
    {
        return [
            'organization' => Yii::t('app', 'Organization'),
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequestType()
    {
        return $this->hasOne(RequestTypes::className(), ['code' => 'request_type']);
    }

    /**
     * @inheritdoc
     * @return RequestQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RequestQuery(get_called_class());
    }
    
    public $send_justification;
    
    
}
