<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\requests;

use Yii;

/**
 * This is the model class for table "request_types".
 *
 * @property integer $code
 * @property string $description
 *
 * @property Request[] $requests
 */
class RequestTypes extends \yii\db\ActiveRecord
{
    const NEW_CA_USER = 100;
    const NEW_OO_USER = 101;
    const NEW_INST_USER = 102;
    const NEW_CA_ORG = 200;
    const NEW_OO_ORG = 201;
    const NEW_INST = 202;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'request_types';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description'], 'required'],
            [['description'], 'string', 'max' => 128]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'code' => Yii::t('app', 'Code'),
            'description' => Yii::t('app', 'Description'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequests()
    {
        return $this->hasMany(Request::className(), ['request_type' => 'code']);
    }

    /**
     * @inheritdoc
     * @return RequestTypesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RequestTypesQuery(get_called_class());
    }
}
