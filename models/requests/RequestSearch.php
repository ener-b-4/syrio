<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\requests;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\requests\Request;

/**
 * RequestSearch represents the model behind the search form about `app\models\requests\Request`.
 */
class RequestSearch extends Request
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'from', 'status', 'processed_by_id'], 'integer'],
            [['request_type'], 'string'],
            [['content'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Request::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $params = \yii\helpers\ArrayHelper::htmlEncode($params, false);
        
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            $query->where('0=1');
            return $dataProvider;
        }

        $code = 0;
        switch (\strtolower($this->request_type))
        {
            case 'create new competent authority user':
                $code = 100;
                break;
            case 'create new operator/owner user':
                $code = 101;
                break;
            case 'create new installation user':
                $code = 102;
                break;
            case 'register new competent authority organization':
                $code = 200;
                break;
            case 'register new operator/owners organization':
                $code = 201;
                break;
            case 'register new installation':
                $code = 202;
                break;
        }
        
        
//        echo  $code;
//        die();
        
        $query->andFilterWhere([
            'id' => $this->id,
//            'from' => $this->from,
            'processed_by_id' => $this->processed_by_id,
        ]);

        if ($this->status == '' || $this->status == '-1') {
            $this->status = null;
        }
        if (isset($this->status)) {
            //special case for open which, in the db, is null
            if ($this->status == 0) {
                $query->andWhere(['or',
                    ['status' => NULL],
                    ['status' => $this->status],
                    ]);
            } else {
                $query->andWhere(['or',
                    ['status' => $this->status],
                    ]);
            }
        }
        
        $code != 0 ? $query->andFilterWhere(['request_type' => $code]) : '';
        
        $query->andFilterWhere(['like', 'contentt', $this->content]);

        
        return $dataProvider;
    }
}
