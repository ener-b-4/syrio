<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\shared;

/**
 * Description of TextFormatter
 *
 * @author vamanbo
 */
class TextFormatter {
    public static function TextToHtml($text) {
        $text = preg_replace("/\r\n|\r|\n/",'<br/>',$text);
        return $text;
    }
    
}
