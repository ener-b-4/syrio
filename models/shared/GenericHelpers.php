<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\shared;

/**
 * Description of GenericHelpers
 *
 * @author vamanbo
 */
class GenericHelpers {
    
    /**
     * 
     * @param array $array
     * @param type $needle
     * @return the value of the key if found
     */
    static function recursiveFind(array $array, $needle)
    {
        $iterator  = new \RecursiveArrayIterator($array);
        $recursive = new \RecursiveIteratorIterator($iterator,
                             \RecursiveIteratorIterator::SELF_FIRST);
        foreach ($recursive as $key => $value) {
            if ($key === $needle) {
                return $value;
            }
        }
    }
    
    
    /**
     * 
     * @param array $array
     * @param type $needle
     * @return true value if found
     */
    static function recursiveInArray(array $array, $needle)
    {
        $iterator  = new \RecursiveArrayIterator($array);
        $recursive = new \RecursiveIteratorIterator($iterator,
                             \RecursiveIteratorIterator::SELF_FIRST);
        foreach ($recursive as $key => $value) {
            if ($value === $needle) {
                return TRUE;
            }
        }
        
        return FALSE;
    }
    
}
