<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\shared;

use Yii;
use app\models\crf\CrfHelper;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * returns the (not set) red span
 *
 * @author vamanbo
 */
class NullValueFormatter {

    /**
     * 
     * @param mixed $obj the object to be tested as for being null
     * @param string $check_attr the object's attribute who's value will be returned if the object exists
     * @param string $null_text
     * @param string $not_null_text
     * @return type
     */
    public static function FormatObject($obj, $check_attr, $null_text = NULL, $not_null_text = NULL)
    {
        if (!isset($null_text))
        {
            $null_text = Yii::t('app', '(not set)');
        }
        
        if (isset($obj))
        {
            if (!isset($not_null_text)) {
                return $obj->$check_attr;
            }
            else {
                return $not_null_text;
            }
        }
        else
        {
            return '<span class="sy_alert_color">'
            . '<i>'
            . $null_text
            . '</i>'
                    . '</span>';
        }
    }
    
    
    public static function FormatDropdown($key, $array, $null_text = NULL, $t_class = NULL) {
        if (!isset($null_text))
        {
            $null_text = Yii::t('app', '(not set)');
        }
        
        if (key_exists($key, $array)) {
            if (isset($t_class)) {
                return Yii::t($t_class, $array[$key]);
            } else {
                return $array[$key];
            }
        }
        else {
            return '<span class="sy_alert_color">'
            . '<em>'
            . $null_text
            . '</em>'
                    . '</span>';
        }
    }
    
    
    public static function Format($val, $null_text = NULL, $not_null_text = NULL)
    {
        if (!isset($null_text))
        {
            $null_text = Yii::t('app', '(not set)');
        }
        
        if (isset($val) && $val!=='')
        {
            /* check for not being -9999 */
            if (CrfHelper::IsSpecialValue($val)) {
                return CrfHelper::SpecialValueLiteral($val, 1);
            }
            
            if (isset($not_null_text)) {
                return $not_null_text;
            } else {
                return $val;
            }
            
        }
        else
        {
            return '<span class="sy_alert_color">'
            . '<i>'
            . $null_text
            . '</i>'
                    . '</span>';
        }
    }
    
    public static function FormatCheckbox($val, $null_text = NULL)
    {
        if (!isset($null_text))
        {
            $null_text = Yii::t('app', '(not set)');
        }
        
        if (isset($val))
        {
            return $val;
        }
        else
        {
            return '<span class="sy_alert_color">'
            . '<em>'
            . $null_text
            . '</em>'
                    . '</span>';
        }
    }    
}
