<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\shared;

use Yii;

/**
 * This is the model class for table "constants".
 *
 * @property integer $num_code
 * @property string $name
 * @property integer $type
 */
class Constants extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'constants';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['num_code', 'name', 'type'], 'required'],
            [['num_code', 'type'], 'integer', 'min'=>0],  //Author: cavesje Date:20151119
            [['name'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'num_code' => ucfirst(Yii::t('app', 'code')),
            'name' => Yii::t('app', 'Name'),
            'type' => ucfirst(Yii::t('app', 'type')),
        ];
    }
}
