<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "holidays".
 *
 * @property integer $id
 * @property integer $day
 * @property integer $month
 * @property integer $year
 * @property string $name
 * @property bool $repeat
 * @property integer $created_at
 */
class Holidays extends \yii\db\ActiveRecord
{
    public $request;
    
    public $date;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'holidays';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'repeat'], 'required'],
            [['day', 'month', 'year', 'created_at'], 'integer', 'min'=>0],  //Author: cavesje Date:20151119
            [['repeat'], 'integer'],
            [['repeat'], 'in', 'range'=>[0,1]],
            [['name'], 'string', 'max' => 45],
            [['day', 'month', 'year', 'repeat'], 'unique', 'targetAttribute' => ['day', 'month', 'year', 'repeat'], 'message' => 'The combination of Day, Month and Year has already been taken.'],
            
            [['request'], 'string'],
            [['request'], 'safe'],
            
            [['repeat'], 'filter', 'filter' => 'intval', 'skipOnEmpty'=>true],
            [['day', 'month', 'year'], 'filter', 'filter' => 'intval', 'skipOnEmpty'=>true],
            
            [['dateAll'], 'required'],
            [['dateAll'], 'date', 'format' => 'php:Y-m-d']
        ];
    }

    public function behaviors() {
        return [
            [
                'class' => \yii\behaviors\TimestampBehavior::className(),                
            ]
        ];
    }
    
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'day' => Yii::t('app', 'Day'),
            'month' => Yii::t('app', 'Month'),
            'year' => Yii::t('app/cpf', 'Year'),
            'name' => Yii::t('app', 'Name'),
            'repeat' => Yii::t('app/calendar', 'Repeat every year'),
            'dateAll' => ucfirst(Yii::t('app/crf', 'date')),
            'created_at' => Yii::t('app', 'created at'),
            'modified_at' => ucfirst(Yii::t('app', 'modified at')),
        ];
    }
    
    
    public static function ResetLogicAttributes() {
        return [
            ['repeat'    ,   1,  ['year']],
        ];
    }

    public function beforeSave($insert) {
        
        if (count($this->dirtyAttributes) > 0) {
            if (key_exists('repeat', $this->dirtyAttributes) && $this->dirtyAttributes['repeat'] === 0) {
                //set the year as current
                if (!isset($model->year) || $model->year===0) {
                    $this->year = date('Y');
                }
            }
            
            
            $this->PrepareForSave();
        }
        return parent::beforeSave($insert);            //return parent::beforeSave($insert);
    }

    /**
     * the cascading update of attributes
     */
    public function PrepareForSave() {
        \app\components\DataCleaning::refreshAttributes($this, self::ResetLogicAttributes());
    }
    
    
    public function setDateAll($val) {
        $d = \DateTime::createFromFormat('Y-m-d', $val);
        $d->setTime(0,0,1);
        
        $this->year = $d->format('Y');
        $this->month = $d->format('m');
        $this->day = $d->format('d');
    }
    
    public function getDateAll() {
        $year = isset($this->year) ? $this->year : date('Y');
        $month = isset($this->month) ? $this->month: date('m');
        $day = isset($this->day) ? $this->day: date('d');
        
        $d = new \DateTime();
        $d->setDate($year, $month, $day);
        
        return $d->format('Y-m-d');
    }
    
}
