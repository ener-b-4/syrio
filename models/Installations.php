<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models;

use Yii;
use app\models\InstallationTypes;
use app\modules\management\models\Organization;
use app\modules\management\models\Country;

/**
 * This is the model class for table "installations".
 *
 * @property string $id
 * @property string $operator_id
 * @property string $name
 * @property string $type
 * @property string $type_other
 * @property string $code
 * @property string $flag
 * @property integer $year_of_construction
 * @property integer $number_of_beds
 * @property integer $status
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $modified_at
 * @property string $modified_by
 *
 * @property InstallationTypes $typeObject
 * @property Organization organization
 * 
 * @property \app\modules\management\models\Country country the country if MODU
 * 
 * @property array[key, value] operationalStatuses (READONLY) the possible operational statuses of an installation
 */
class Installations extends \yii\db\ActiveRecord
{
    const STATUS_DELETED = 0;
    const STATUS_REQUEST = 1;
    const STATUS_ACVTIVE = 10;
    const OP_STATUS_INOPERATIVE = 100;
    const OP_STATUS_DECOMMISSIONED = 101;
    const OP_STATUS_OPERATIVE = 110;

    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'installations';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'operator_id', 'name', 'type', 'code', 'op_status'], 'required'],
            [['status', 'created_at', 'created_by', 'modified_at', 'modified_by', 'op_status'], 'integer'],
            [['year_of_construction'], 'integer', 'min'=>1950, 'max'=>9999],
            [['number_of_beds'], 'integer', 'min'=>0, 'max'=>500],  //should match the XML limit
            [['id', 'operator_id'], 'string', 'max' => 12],
            [['name'], 'string', 'max' => 64],
            [['type'], 'string', 'max' => 8],
            [['type_other'], 'string', 'max' => 45],
            [['flag'], 'string', 'min'=>2, 'max' => 2],
            [['code'], 'string', 'max' => 32],
            ['flag', 'required', 'when'=>function($model) {
                return $model->typeObject->mobile == 1;
            }, 'whenClient' => "function (attribute, value) {
                return $('#installations-type').val() == 'MODU';
            }"],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'operator_id' => 'Operator ID',
            'name' => Yii::t('app', 'Name of installation'),
            'type' => Yii::t('app/cpf', 'Type of installation'),
            'type_other' => Yii::t('app', 'Type (other)'),
            'code' => ucfirst(Yii::t('app', 'code')),
            'flag' => Yii::t('app', 'Flag (if MODU)'),
            'year_of_construction' => Yii::t('app/cpf', 'Year of construction'),
            'number_of_beds' => Yii::t('app/cpf', 'Number of beds'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => ucwords(Yii::t('app', 'created at')),
            'created_by' => Yii::t('app', 'created by'),
            'modified_at' => ucwords(Yii::t('app', 'modified at')),
            'modified_by' => ucwords(Yii::t('app', 'modified by')),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTypeObject()
    {
        return $this->hasOne(InstallationTypes::className(), ['type' => 'type']);
    }
    
    /**
     * SyRIO manual
     * Returns the Operator/Owner organization
     * 
     * @return type \yii\db\ActiveQuery
     */
    public function getOperator()
    {
        return $this->hasOne(Organization::className(), ['id'=>'operator_id']);
    }
    
    public static function find() {
        if (Yii::$app->user->can('sys_admin'))
        {
            return parent::find();
        }
        elseif (Yii::$app->user->identity->isOperatorUser)
        {
            return parent::find()
                    ->where(['operator_id' => Yii::$app->user->identity->organization->id]);
        }
        elseif (Yii::$app->user->identity->isInstallationUser)
        {
            return parent::find()
                    ->where(['id' => Yii::$app->user->identity->inst_id]);
        }
        elseif (Yii::$app->user->identity->isCaUser)
        {
            return parent::find();
        }
    }
    
    /**
     * Gets the possible statuses of an installation.
     * May be used directly in a dropdaownlist.
     * 
     * @return array[key value] The possible statuses of an installation
     * @author Bogdan Vamanu <bogdan.vamanu@jrc.ec.europa.eu>
     */
    public function getStatuses()
    {
        return [
            [0 => Yii::t('app', 'Deleted')],
            [1 => Yii::t('app', 'Request')],
            [10 => Yii::t('app', 'Active')]
        ];
    }

    /**
     * Gets the possible statuses of an installation.
     * May be used directly in a dropdownlist.
     * 
     * @return array[key value] The possible operational statuses of an installation
     * @author Bogdan Vamanu <bogdan.vamanu@jrc.ec.europa.eu>
     */
    public function getOperationalStatuses()
    {
        return [
            100 => Yii::t('app', 'Inoperative'),
            1 => Yii::t('app', 'Decommissioned'),
            10 => Yii::t('app', 'Operative')
        ];
    }

    /*
     * Returns the number of active users in a given Organization
     */
    public function getNumberOfUsers()
    {
        return \app\models\User::find()
                ->where(['inst_id' => $this->id])
                ->andWhere(['org_id' => $this->operator_id])
                ->andWhere(['status' => \app\models\User::STATUS_ACTIVE])
                ->count();
    }

    
    /**
     * Generates an Installation ID in the form of IN + timestamp of request
     * 
     * @return string the generated ID for an installation
     * @author Bogdan Vamanu <bogdan.vamanu@jrc.ec.europa.eu>
     */
    public static function GenerateId()
    {
        return 'IN' . time();
    }
    

    public function getCountry() {
        return $this->hasOne(Country::className(), ['iso2' => 'flag']);
    }
    
    public function getCreatedBy() {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    public function getModifiedBy() {
        return $this->hasOne(User::className(), ['id' => 'modified_by']);
    }
    
    public function getEvents() {
        return events\EventDeclaration::find()
                ->where(['installation_id'=>$this->id])
                ->andWhere(['operator_id'=>$this->operator_id])
                ->all();
    }
    
    public function getHasEvents() {
        $query = events\EventDeclaration::find()
                ->where(['installation_id'=>$this->id])
                ->andWhere(['operator_id'=>$this->operator_id])
                ->count();
        return $query > 0;
    }
    
}
