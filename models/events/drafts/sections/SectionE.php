<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\events\drafts\sections;

use Yii;

use app\models\events\drafts\EventDraft;
use app\models\events\drafts\sections\subsections\E1;
use app\models\events\drafts\sections\subsections\E2;
use app\models\events\drafts\sections\subsections\E3;
use app\models\events\drafts\sections\subsections\E4;

/**
 * This is the model class for table "s_e".
 *
 * @property integer $id
 * @property integer $event_classification_id
 * @property integer $status
 * @property integer $locked_by
 *
 * @property EventClassifications $eventClassification
 * @property \app\models\events\drafts\sections\subsections\E1 $e1
 * @property \app\models\events\drafts\sections\subsections\E2 $e2
 * @property \app\models\events\drafts\sections\subsections\E3 $e3
 * @property \app\models\events\drafts\sections\subsections\E4 $e4
 */
class SectionE extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 's_e';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['event_classification_id'], 'required'],
            [['event_classification_id', 'status', 'locked_by'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'event_classification_id' => Yii::t('app/cpf', 'Event Classification ID'),
            'status' => Yii::t('app/cpf', 'Status'),
            'locked_by' => Yii::t('app/cpf', 'Locked By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventDraft()
    {
        return $this->hasOne(EventDraft::className(), ['id' => 'event_classification_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getE1()
    {
        return $this->hasOne(E1::className(), ['se_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getE2()
    {
        return $this->hasOne(E2::className(), ['se_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getE3()
    {
        return $this->hasOne(E3::className(), ['se_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getE4()
    {
        return $this->hasOne(E4::className(), ['se_id' => 'id']);
    }
}
