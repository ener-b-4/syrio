<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\events\drafts\sections;

use Yii;

use app\models\events\drafts\EventDraft;
use app\models\events\drafts\sections\subsections\F1;
use app\models\events\drafts\sections\subsections\F2;
use app\models\events\drafts\sections\subsections\F3;
use app\models\events\drafts\sections\subsections\F4;

/**
 * This is the model class for table "s_f".
 *
 * @property integer $id
 * @property integer $event_classification_id
 * @property integer $status
 * @property integer $locked_by
 * @property integer $locked_at
 * 
 * @property \app\models\events\drafts\EventDraft $eventDraft
 * @property \app\models\events\drafts\sections\subsections\F1 $f1
 * @property \app\models\events\drafts\sections\subsections\F2 $f2
 * @property \app\models\events\drafts\sections\subsections\F3 $f3
 * @property \app\models\events\drafts\sections\subsections\F4 $f4
 */
class SectionF extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 's_f';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['event_classification_id'], 'required'],
            [['event_classification_id', 'status', 'locked_by', 'locked_at'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'event_classification_id' => Yii::t('app/cpf', 'Event Classification ID'),
            'status' => Yii::t('app/cpf', 'Status'),
            'locked_by' => Yii::t('app/cpf', 'Locked By'),
            'locked_at' => Yii::t('app/cpf', 'Locked At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventDraft()
    {
        return $this->hasOne(EventDraft::className(), ['id' => 'event_classification_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getF1()
    {
        return $this->hasOne(F1::className(), ['sf_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getF2()
    {
        return $this->hasOne(F2::className(), ['sf_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getF3()
    {
        return $this->hasOne(F3::className(), ['sf_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getF4()
    {
        return $this->hasOne(F4::className(), ['sf_id' => 'id']);
    }
}
