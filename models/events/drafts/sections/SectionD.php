<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\events\drafts\sections;

use Yii;
use app\models\events\drafts\EventDraft;
use app\models\events\drafts\sections\subsections\D1;
use app\models\events\drafts\sections\subsections\D2;
use app\models\events\drafts\sections\subsections\D3;
use app\models\events\drafts\sections\subsections\D4;


/**
 * This is the model class for table "s_d".
 *
 * @property integer $id
 * @property integer $event_classification_id
 * @property integer $status
 * @property integer $locked_by
 * @property integer $locked_at
 *
 * @property \app\models\events\drafts\EventDraft $eventDraft
 * @property \app\models\events\drafts\sections\subsections\D1 $d1
 * @property \app\models\events\drafts\sections\subsections\D2 $d2
 * @property \app\models\events\drafts\sections\subsections\D3 $d3
 * @property \app\models\events\drafts\sections\subsections\D4 $d4
 */
class SectionD extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 's_d';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['event_classification_id'], 'required'],
            [['event_classification_id', 'status', 'locked_by', 'locked_at'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'event_classification_id' => 'Event Classification ID',
            'status' => Yii::t('app', 'Status'),
            'locked_by' => Yii::t('app', 'Locked By'),
            'locked_at' => Yii::t('app/cpf', 'Locked At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventDraft()
    {
        return $this->hasOne(EventDraft::className(), ['id' => 'event_classification_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getD1()
    {
        return $this->hasOne(D1::className(), ['sd_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getD2()
    {
        return $this->hasOne(D2::className(), ['sd_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getD3()
    {
        return $this->hasOne(D3::className(), ['sd_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getD4()
    {
        return $this->hasOne(D4::className(), ['sd_id' => 'id']);
    }
}
