<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\events\drafts\sections;

use Yii;
use app\models\events\drafts\EventDraft;
use app\models\events\drafts\sections\subsections\B1;
use app\models\events\drafts\sections\subsections\B2;
use app\models\events\drafts\sections\subsections\B3;
use app\models\events\drafts\sections\subsections\B4;


/**
 * This is the model class for table "s_b".
 *
 * @property integer $id
 * @property integer $event_classification_id
 * @property integer $status
 * @property integer $locked_by
 *
 * @property EventDraft $eventDraft
 * @property \app\models\events\drafts\sections\subsections\B1 $b1
 * @property \app\models\events\drafts\sections\subsections\B2 $b2
 * @property \app\models\events\drafts\sections\subsections\B3 $b3
 * @property \app\models\events\drafts\sections\subsections\B4 $b4
 */
class SectionB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 's_b';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['event_classification_id'], 'required'],
            [['event_classification_id', 'status', 'locked_by'], 'integer']  
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'event_classification_id' => 'Event Classification ID',
            'status' => Yii::t('app', 'Status'),
            'locked_by' => Yii::t('app', 'Locked By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventDraft()
    {
        return $this->hasOne(EventDraft::className(), ['id' => 'event_classification_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getB1()
    {
        return $this->hasOne(B1::className(), ['sb_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getB2()
    {
        return $this->hasOne(B2::className(), ['sb_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getB3()
    {
        return $this->hasOne(B3::className(), ['sb_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getB4()
    {
        return $this->hasOne(B4::className(), ['sb_id' => 'id']);
    }
}
