<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */


namespace app\models\events\drafts\sections\subsections;

use Yii;
use app\models\events\drafts\sections\subsections\SC21Sece;

/**
 * This is the model class for table "s_c_2_1".
 *
 * @property integer $id
 * @property string $c21_1
 * @property string $c21_2
 * @property integer $sc_2_id
 * @property integer $c21_3_a
 * @property string $c21_3_a_desc
 * @property integer $c21_3_b
 * @property string $c21_3_b_desc
 * @property integer $c21_3_c
 * @property string $c21_3_c_desc
 * @property integer $c21_3_d
 * @property string $c21_3_d_desc
 * @property integer $c21_3_e
 * @property string $c21_3_e_desc
 * @property integer $c21_3_f
 * @property string $c21_3_f_desc
 * @property integer $c21_3_g
 * @property string $c21_3_g_desc
 * @property integer $c21_3_h
 * @property string $c21_3_h_desc
 * @property integer $c21_3_i
 * @property string $c21_3_i_desc
 * @property integer $c21_3_j
 * @property string $c21_3_j_desc
 * @property integer $c21_3_k
 * @property string $c21_3_k_desc
 * @property integer $c21_3_l
 * @property string $c21_3_l_desc
*
 * @property SC2 $sc2
 * @property SC21Sece[] $sC21Seces
 * @property Sece[] $seces
 */
class C2_1 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 's_c_2_1';
    }

    
    public function beforeSave($insert) {
        //check if other is 0
        //if so, make the other_description = null
        foreach(range('a', 'l') as $letter) {
            $attr = 'c21_3_'.$letter;
            $desc_attr = 'c21_3_'.$letter.'_desc';
            
            if ($this->$attr == "0") {
                $this->$desc_attr = null;
            }
        }
        
        return parent::beforeSave($insert);
    }
    
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['c21_2'], 'string'],
            [['sc_2_id'], 'required'],
            [['sc_2_id', 'c21_3_a', 'c21_3_b', 'c21_3_c', 'c21_3_d', 'c21_3_e', 'c21_3_f', 'c21_3_g', 'c21_3_h', 'c21_3_i', 'c21_3_j', 'c21_3_k', 'c21_3_l'], 'integer'],
            [['c21_1', 'c21_3_a_desc', 'c21_3_b_desc', 'c21_3_c_desc', 'c21_3_d_desc', 'c21_3_e_desc', 'c21_3_f_desc', 'c21_3_g_desc', 'c21_3_h_desc', 'c21_3_i_desc', 'c21_3_j_desc', 'c21_3_k_desc', 'c21_3_l_desc'], 'string', 'max' => 255],
            
            [['c21_3_a_desc'], 'required', 'when' => function($model){
                return $model->c21_3_a > 0;
            }, 'enableClientValidation' => false, 'message' => Yii::t('app', 'Provide a value for the other system that failed.') . '[(a)' . Yii::t('app/crf', 'Structural integrity systems') . ']' 
            ],
            [['c21_3_b_desc'], 'required', 'when' => function($model){
                return $model->c21_3_b > 0;
            }, 'enableClientValidation' => false, 'message' => Yii::t('app', 'Provide a value for the other system that failed.') . '[(b)' . Yii::t('app/crf', 'Process containment systems')  . ']' 
            ],
            [['c21_3_c_desc'], 'required', 'when' => function($model){
                return $model->c21_3_c > 0;
            }, 'enableClientValidation' => false, 'message' => Yii::t('app', 'Provide a value for the other system that failed.') . '[(c)' . Yii::t('app/crf', 'Ignition control systems')  . ']' 
            ],
            [['c21_3_d_desc'], 'required', 'when' => function($model){
                return $model->c21_3_d > 0;
            }, 'enableClientValidation' => false, 'message' => Yii::t('app', 'Provide a value for the other system that failed.') . '[(d)' . Yii::t('app/crf', 'Detection systems')  . ']' 
            ],
            [['c21_3_e_desc'], 'required', 'when' => function($model){
                return $model->c21_3_e > 0;
            }, 'enableClientValidation' => false, 'message' => Yii::t('app', 'Provide a value for the other system that failed.') . '[(e)' . Yii::t('app/crf', 'Process containment release systems') . ']'  
            ],
            [['c21_3_f_desc'], 'required', 'when' => function($model){
                return $model->c21_3_f > 0;
            }, 'enableClientValidation' => false, 'message' => Yii::t('app', 'Provide a value for the other system that failed.') . '[(f)' . Yii::t('app/crf', 'Protection systems')  . ']' 
            ],
            [['c21_3_g_desc'], 'required', 'when' => function($model){
                return $model->c21_3_g > 0;
            }, 'enableClientValidation' => false, 'message' => Yii::t('app', 'Provide a value for the other system that failed.') . '[(g)' . Yii::t('app/crf', 'Shutdown systems')  . ']' 
            ],
            [['c21_3_h_desc'], 'required', 'when' => function($model){
                return $model->c21_3_h > 0;
            }, 'enableClientValidation' => false, 'message' => Yii::t('app', 'Provide a value for the other system that failed.') . '[(h)' . Yii::t('app/crf', 'Navigational aids')  . ']' 
            ],
            [['c21_3_i_desc'], 'required', 'when' => function($model){
                return $model->c21_3_i > 0;
            }, 'enableClientValidation' => false, 'message' => Yii::t('app', 'Provide a value for the other system that failed.') . '[(i)' . Yii::t('app/crf', 'Rotating equipment - power supply')  . ']' 
            ],
            [['c21_3_j_desc'], 'required', 'when' => function($model){
                return $model->c21_3_j > 0;
            }, 'enableClientValidation' => false, 'message' => Yii::t('app', 'Provide a value for the other system that failed.') . '[(j)' . Yii::t('app/crf', 'Escape, evacuation and rescue equipment')  . ']' 
            ],
            [['c21_3_k_desc'], 'required', 'when' => function($model){
                return $model->c21_3_k > 0;
            }, 'enableClientValidation' => false, 'message' => Yii::t('app', 'Provide a value for the other system that failed.') . '[(k)' . Yii::t('app/crf', 'Communication system')  . ']' 
            ],
            [['c21_3_l_desc'], 'required', 'when' => function($model){
                return $model->c21_3_l > 0;
            }, 'enableClientValidation' => false, 'message' => Yii::t('app', 'Provide a value for the other system that failed.') . '[(l)' . Yii::t('app/crf', 'Other, specify')  . ']' 
            ],
                    
                    [[
                        'sece_a', 'sece_b', 
                        'sece_c', 'sece_d', 
                        'sece_e', 'sece_f', 
                        'sece_g', 'sece_h', 
                        'sece_i', 'sece_j', 
                        'sece_k', 
                        ], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'c21' => Yii::t('app/crf', 'Description of SECE and circumstances'),    //phoney
            'c21_1' => Yii::t('app/crf', 'Report independent verifier (report nr. / date / verifier)'),
            'c21_2' => Yii::t('app/crf', 'Failure during major accident: details (date /accident description / ...)'),
            'sc_2_id' => 'Sc 2 ID',
            'c21_3_a' => Yii::t('app/crf', 'Other'),
            'c21_3_a_desc' => Yii::t('app/crf', 'specify'),
            'c21_3_b' => Yii::t('app/crf', 'Other'),
            'c21_3_b_desc' => Yii::t('app/crf', 'specify'),
            'c21_3_c' => Yii::t('app/crf', 'Other'),
            'c21_3_c_desc' => Yii::t('app/crf', 'specify'),
            'c21_3_d' => Yii::t('app/crf', 'Other'),
            'c21_3_d_desc' => Yii::t('app/crf', 'specify'),
            'c21_3_e' => Yii::t('app/crf', 'Other'),
            'c21_3_e_desc' => Yii::t('app/crf', 'specify'),
            'c21_3_f' => Yii::t('app/crf', 'Other'),
            'c21_3_f_desc' => Yii::t('app/crf', 'specify'),
            'c21_3_g' => Yii::t('app/crf', 'Other'),
            'c21_3_g_desc' => Yii::t('app/crf', 'specify'),
            'c21_3_h' => Yii::t('app/crf', 'Other'),
            'c21_3_h_desc' => Yii::t('app/crf', 'specify'),
            'c21_3_i' => Yii::t('app/crf', 'Other'),
            'c21_3_i_desc' => Yii::t('app/crf', 'specify'),
            'c21_3_j' => Yii::t('app/crf', 'Other'),
            'c21_3_j_desc' => Yii::t('app/crf', 'specify'),
            'c21_3_k' => Yii::t('app/crf', 'Other'),
            'c21_3_k_desc' => Yii::t('app/crf', 'specify'),
            'c21_3_l' => Yii::t('app/crf', 'Other'),
            'c21_3_l_desc' => Yii::t('app/crf', 'specify'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSc2()
    {
        return $this->hasOne(SC2::className(), ['id' => 'sc_2_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSC21Seces()
    {
        return $this->hasMany(SC21Sece::className(), ['s_c_2_1_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSeces()
    {
        //return $this->hasMany(SC21Sece::className(), ['id' => 'sece_id'])->viaTable('s_c_2_1_sece', ['s_c_2_1_id' => 'id']);
        return $this->hasMany(\app\models\crf\Sece::className(), ['id' => 'sece_id'])->viaTable('s_c_2_1_sece', ['s_c_2_1_id' => 'id']);        
    }
    
    public $_sece_a;
    public function getSece_a()
    {
        return $this->hasMany(\app\models\crf\Sece::className(), ['id' => 'sece_id'])->viaTable('s_c_2_1_sece', ['s_c_2_1_id' => 'id'])
            ->where(['between', 'id', 100, 199]);        
    }
    public function setSece_a($val) {
        $this->_sece_a = $val;
    }

    
    public $_sece_b;
    public function getSece_b()
    {
        return $this->hasMany(\app\models\crf\Sece::className(), ['id' => 'sece_id'])->viaTable('s_c_2_1_sece', ['s_c_2_1_id' => 'id'])
            ->where(['between', 'id', 200, 299]);        
    }
    public function setSece_b($val) {
        $this->_sece_b = $val;
    }

        public $_sece_c;
    public function getSece_c()
    {
        return $this->hasMany(\app\models\crf\Sece::className(), ['id' => 'sece_id'])->viaTable('s_c_2_1_sece', ['s_c_2_1_id' => 'id'])
            ->where(['between', 'id', 300, 399]);        
    }
    public function setSece_c($val) {
        $this->_sece_c = $val;
    }

        public $_sece_d;
    public function getSece_d()
    {
        return $this->hasMany(\app\models\crf\Sece::className(), ['id' => 'sece_id'])->viaTable('s_c_2_1_sece', ['s_c_2_1_id' => 'id'])
            ->where(['between', 'id', 400, 499]);        
    }
    public function setSece_d($val) {
        $this->_sece_d = $val;
    }

        public $_sece_e;
    public function getSece_e()
    {
        return $this->hasMany(\app\models\crf\Sece::className(), ['id' => 'sece_id'])->viaTable('s_c_2_1_sece', ['s_c_2_1_id' => 'id'])
            ->where(['between', 'id', 500, 599]);        
    }
    public function setSece_e($val) {
        $this->_sece_e = $val;
    }

        public $_sece_f;
    public function getSece_f()
    {
        return $this->hasMany(\app\models\crf\Sece::className(), ['id' => 'sece_id'])->viaTable('s_c_2_1_sece', ['s_c_2_1_id' => 'id'])
            ->where(['between', 'id', 600, 699]);        
    }
    public function setSece_f($val) {
        $this->_sece_f = $val;
    }

        public $_sece_g;
    public function getSece_g()
    {
        return $this->hasMany(\app\models\crf\Sece::className(), ['id' => 'sece_id'])->viaTable('s_c_2_1_sece', ['s_c_2_1_id' => 'id'])
            ->where(['between', 'id', 700, 799]);        
    }
    public function setSece_g($val) {
        $this->_sece_g = $val;
    }

        public $_sece_h;
    public function getSece_h()
    {
        return $this->hasMany(\app\models\crf\Sece::className(), ['id' => 'sece_id'])->viaTable('s_c_2_1_sece', ['s_c_2_1_id' => 'id'])
            ->where(['between', 'id', 800, 899]);        
    }
    public function setSece_h($val) {
        $this->_sece_h = $val;
    }

        public $_sece_i;
    public function getSece_i()
    {
        return $this->hasMany(\app\models\crf\Sece::className(), ['id' => 'sece_id'])->viaTable('s_c_2_1_sece', ['s_c_2_1_id' => 'id'])
            ->where(['between', 'id', 900, 999]);        
    }
    public function setSece_i($val) {
        $this->_sece_i = $val;
    }

        public $_sece_j;
    public function getSece_j()
    {
        return $this->hasMany(\app\models\crf\Sece::className(), ['id' => 'sece_id'])->viaTable('s_c_2_1_sece', ['s_c_2_1_id' => 'id'])
            ->where(['between', 'id', 1000, 1099]);        
    }
    public function setSece_j($val) {
        $this->_sece_j = $val;
    }

        public $_sece_k;
    public function getSece_k()
    {
        return $this->hasMany(\app\models\crf\Sece::className(), ['id' => 'sece_id'])->viaTable('s_c_2_1_sece', ['s_c_2_1_id' => 'id'])
            ->where(['between', 'id', 1100, 1199]);        
    }
    public function setSece_k($val) {
        $this->_sece_k = $val;
    }

        public $_sece_l;
        
    public function getSece_l()
    {
        //this is the other
    }
    public function setSece_l($val) {
    }

    
}
