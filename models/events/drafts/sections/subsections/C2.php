<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\events\drafts\sections\subsections;

use Yii;
use app\models\events\drafts\sections\SectionC;
use app\models\events\drafts\sections\subsections\C2_1;
use app\models\events\drafts\sections\subsections\C2_2;

/**
 * This is the model class for table "s_c_2".
 *
 * @property integer $id
 * @property integer $sc_id
 *
 * @property SC $sc
 * @property SC21 $sC2_1
 * @property SC22 $sC2_2
 */
class C2 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 's_c_2';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sc_id'], 'required', 'on'=>'default'],
            [['sc_id'], 'required', 'on'=>'finalize'],
            [['sc_id'], 'integer'],
            [['ref'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sc_id' => 'Sc ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSC()
    {
        return $this->hasOne(SectionC::className(), ['id' => 'sc_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSC2_1()
    {
        return $this->hasOne(C2_1::className(), ['sc_2_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSC2_2()
    {
        return $this->hasOne(C2_2::className(), ['sc_2_id' => 'id']);
    }
    
    
    private $_ref = '';
    public function getRef()
    {
        return $this->_ref;
    }
    public function setRef($url)
    {
        $this->_ref = $url;
    }    
}
