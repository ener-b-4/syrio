<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\events\drafts\sections\subsections;

use Yii;
use app\models\events\drafts\sections\SectionD;

/**
 * This is the model class for table "s_d_3".
 *
 * @property integer $id
 * @property string $d3_1
 * @property integer $sd_id
 * @property integer $is_direct
 *
 * @property SD $sD
 */
class D3 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 's_d_3';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['d3_1'], 'string'],
            [['d3_1'], 'required'],
            [['d3_1'], 'required', 'on'=>'finalize'],
            [['sd_id', 'is_direct'], 'integer', 'min'=>0],  //Author: cavesje Date:20151119
            
            [['ref'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'd3_1' => Yii::t('app/crf', 'Preliminary direct and underlying causes'),
            'sd_id' => 'parent section id',
            'is_direct' => 'if True this is a direct cause; underlying otherwise',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSD()
    {
        return $this->hasOne(SectionD::className(), ['id' => 'sd_id']);
    }

    private $_ref = '';
    public function getRef()
    {
        return $this->_ref;
    }
    public function setRef($url)
    {
        $this->_ref = $url;
    }    
    
    
    
}
