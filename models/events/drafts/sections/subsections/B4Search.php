<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\events\drafts\sections\subsections;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\events\drafts\sections\subsections\B4;

/**
 * B4Search represents the model behind the search form about `app\models\events\drafts\sections\subsections\B4`.
 */
class B4Search extends B4
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'is_lesson_learn', 'sb_id'], 'integer', 'min'=>0],  //Author: cavesje Date:20151119
            [['b4_1'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = B4::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'is_lesson_learn' => $this->is_lesson_learn,
            'sb_id' => $this->sb_id,
        ]);

        $query->andFilterWhere(['like', 'b4_1', $this->b4_1]);

        return $dataProvider;
    }
}
