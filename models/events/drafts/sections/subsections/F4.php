<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\events\drafts\sections\subsections;

use Yii;
use app\models\events\drafts\sections\SectionF;

/**
 * This is the model class for table "s_f_4".
 *
 * @property integer $id
 * @property string $f4_1
 * @property integer $sf_id
 *
 * @property app\models\events\drafts\sections\SectionI $sI
 */
class F4 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 's_f_4';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['f4_1'], 'string'],
            [['f4_1'], 'required', 'on'=>'finalize'],
            [['sf_id'], 'integer'],

            [['ref'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'f4_1' => Yii::t('app', 'Initial lessons learned and preliminary recommendations'),
            'sf_id' => 'parent section id',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSF()
    {
        return $this->hasOne(SectionF::className(), ['id' => 'sf_id']);
    }

    private $_ref = '';
    public function getRef()
    {
        return $this->_ref;
    }
    public function setRef($url)
    {
        $this->_ref = $url;
    }        
}
