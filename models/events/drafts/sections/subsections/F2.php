<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\events\drafts\sections\subsections;

use Yii;
use app\models\events\drafts\sections\SectionF;

/**
 * This is the model class for table "s_f_2".
 *
 * @property integer $id
 * @property string $f2_1
 * @property integer $sf_id
 *
 * @property SF $sf
 */
class F2 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 's_f_2';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['f2_1'], 'string'],
            [['sf_id'], 'integer'],
            [['f2_1'], 'required', 'on'=>'finalize'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'f2_1' => Yii::t('app/crf', 'Description of circumstances'),
            'sf_id' => 'the id of the parent Section F',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSF()
    {
        return $this->hasOne(SectionF::className(), ['id' => 'sf_id']);
    }
    
    private $_ref = '';
    public function getRef()
    {
        return $this->_ref;
    }
    public function setRef($url)
    {
        $this->_ref = $url;
    }        
    
}
