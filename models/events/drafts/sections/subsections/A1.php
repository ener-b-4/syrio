<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\events\drafts\sections\subsections;

use Yii;
use app\models\events\drafts\sections\SectionA;
use app\models\units\Units;
use app\models\crf\LeakCause;

/**
 * This is the model class for table "s_a_1".
 *
 * @property integer $id
 * @property integer $a1
 * @property integer $a1i_np
 * @property string $a1i_np_specify
 * @property double $a1i_2p_h2s_q
 * @property integer $a1i_2p_h2s_u
 * @property double $a1i_2p_h2s_n
 * @property double $a1ii_q
 * @property integer $a1ii_u
 * @property double $a1ii_n
 * @property double $a1iii_q
 * @property integer $a1iii_u
 * @property double $a1iii_n
 * @property double $a1iv_q
 * @property integer $a1iv_u
 * @property double $a1iv_n
 * @property string $a1v
 * @property integer $a1vi_code
 * @property integer $a1vii_code
 * @property integer $a1viia
 * @property double $a1viib
 * @property double $a1viic
 * @property double $a1viii_uw_q
 * @property integer $a1viii_uw_u
 * @property double $a1viii_uw_n
 * @property double $a1viii_dw_q
 * @property integer $a1viii_dw_u
 * @property double $a1viii_dw_n
 * @property string $a1viii_c
 * @property double $a1ix_a_q
 * @property integer $a1ix_a_u
 * @property double $a1ix_a_n
 * @property double $a1ix_b_q
 * @property integer $a1ix_b_u
 * @property double $a1ix_b_n
 * @property integer $a1x_a
 * @property integer $a1x_b
 * @property integer $a1x_c
 * @property integer $a1x_d
 * @property string $a1x_d_specify
 * @property string $axi_description
 * @property integer $axii
 * @property integer $axii_code
 * @property double $axii_code_time
 * @property integer $axii_flash_index
 * @property integer $axii_jet_index
 * @property integer $axii_exp_index
 * @property integer $axii_pool_index
 * @property string $axiii
 * @property integer $axiv_sd
 * @property integer $axiv_bd
 * @property integer $axiv_d
 * @property integer $axiv_co2
 * @property integer $axiv_ctm
 * @property integer $a1i_o
 * @property integer $a1i_co
 * @property integer $a1i_g
 * @property integer $a1i_2p
 * @property integer $a1_op
 * @property string $a1_op_specify
 * @property integer $a1_fail_design_code
 * @property integer $a1_fail_equipment_code
 * @property string $a1_fail_equipment_specify
 * @property integer $a1_fail_operation_code
 * @property string $a1_fail_operation_specify
 * @property integer $a1_fail_procedural_code
 * @property string $a1_fail_procedural_specify
 * @property string $a1xv
 *
 * @property SA1Comments[] $sA1Comments
 */
class A1 extends \yii\db\ActiveRecord
{
    
    public static function ResetLogicAttributes() {
        return [
            ['a1i_np'   ,   1,  ['a1i_o', 'a1i_co', 'a1i_g', 'a1i_2p', 'a1i_2p_h2s_q', 'a1i_2p_h2s_u']],
            ['a1i_o'    ,   1,  ['a1i_np', 'a1i_np_specify']],
            ['a1i_co'   ,   1,  ['a1i_np', 'a1i_np_specify']],
            ['a1i_g'    ,   1,  ['a1i_np', 'a1i_np_specify']],
            ['a1i_2p'   ,   1,  ['a1i_np', 'a1i_np_specify']],
            [['a1i_g', 'a1i_2p']    ,   0,  ['a1i_2p_h2s_q', 'a1i_2p_h2s_u']],    //if BOTH of these are 0
            ['a1vii_code',  2,  ['a1viia', 'a1viib', 'a1viic', 'a1viid']],
            ['a1x_d',       0,  ['a1x_d_specify']],
            ['axii',        0,  ['axii_code', 'axii_code_time', 'axii_flash_index', 'axii_jet_index', 'axii_pool_index', 'axii_exp_index', 'axiii']],
            ['axii_code',   1,  ['axii_code_time']],
            
            ['axiv_other',  0,  ['axiv_specify']],
            
            ['a1_fail_equipment_code',  2,  ['a1_fail_equipment_specify'], 'dif'],
            ['a1_fail_operation_code',  2,  ['a1_fail_operation_specify'], 'dif'],
            ['a1_fail_procedural_code',  2,  ['a1_fail_procedural_specify'], 'dif'],
            
            ['a1_op', 601, ['a1_op_specify'], 'dif'],
        ];
    }
    
    
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 's_a_1';
	}

        public function beforeSave($insert) {
            if (count($this->dirtyAttributes) > 0) {
                $this->PrepareForSave();
            }
            return parent::beforeSave($insert);            //return parent::beforeSave($insert);
        }
        
        
	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
                    [['id', 'sa_id'], 'required'],
                    [['id', 'sa_id'], 'integer'],
                    
			[['a1', 'a1i_np', 'a1i_2p_h2s_u', 'a1ii_u', 'a1iii_u', 'a1iv_u', 'a1vi_code', 'a1vii_code', 'a1viia', 'a1viii_uw_u', 'a1viii_dw_u', 'a1ix_a_u', 'a1ix_b_u', 'a1x_a', 'a1x_b', 'a1x_c', 'a1x_d', 'axii', 'axii_code', 'axiv_sd', 'axiv_bd', 'axiv_d', 'axiv_co2', 'axiv_ctm', 'a1_op', 'a1_fail_design_code', 'a1_fail_equipment_code', 'a1_fail_operation_code', 'a1_fail_procedural_code'], 'integer', 'min'=>0, 'skipOnEmpty'=>true],
                        [['a1i_o', 'a1i_co', 'a1i_g', 'a1i_2p', 'axiv_other'], 'integer', 'skipOnEmpty'=>true],
			[['a1'], 'required', 'on'=>'finalize'],
			[['a1i_2p_h2s_q', 'a1ii_q', 'a1iii_q', 'a1iv_q', 'a1viii_uw_q', 'a1viii_dw_q', 'a1ix_a_q', 'a1ix_b_q', 'axii_code_time'], 'number', 'min'=>0, 'skipOnEmpty'=>true],   //Author: cavesje Date:20151119
                        //[['a1i_2p_h2s_q', 'a1ii_q', 'a1iii_q', 'a1iv_q',  'a1viii_uw_q', 'a1viii_dw_q', 'a1ix_a_q', 'a1ix_b_q', 'axii_code_time'], 'number', 'isEmpty'=>null],   //Author: cavesje Date:20151119
			[['a1viii_c', 'a1xv'], 'string', 'skipOnEmpty'=>true],
			[['a1i_np_specify', 'a1x_d_specify'], 'string', 'max' => 255, 'skipOnEmpty'=>true],
			[['a1v', 'axi_description', 'axiii'], 'string', 'max' => 512, 'skipOnEmpty'=>true],
			[['a1_op_specify'], 'string', 'max' => 64, 'skipOnEmpty'=>true],
			[['a1_fail_equipment_specify', 'a1_fail_operation_specify'], 'string', 'max' => 128, 'skipOnEmpty'=>true],
			[['a1_fail_procedural_specify', 'axiv_specify'], 'string', 'max' => 45, 'skipOnEmpty'=>true],
			[['ref'], 'safe'],
                    
                    [
                        [
                            'a1', 'a1i_np',
                            'a1ii_u', 'a1iii_u', 'a1iv_u', 'a1viia', 'a1vii_code', 'a1viii_uw_u', 'a1ix_a_u', 'a1ix_b_u', 'a1_fail_design_code', 'a1_fail_equipment_code', 'a1_fail_operation_code', 'a1_fail_procedural_code',
                        ], 'filter', 'filter' => 'intval', 'skipOnEmpty'=>true
                    ],
                    [
                        [
                            'a1i_o', 'a1i_co', 'a1i_g', 'a1i_2p', 'a1x_d', 'axiv_other', 'a1_op'
                        ], 'filter', 'filter' => 'intval', 'skipOnEmpty'=>true
                    ],
                    [
                        [
                            'a1i_2p_h2s_q', 'a1ii_q', 'a1iii_q', 'a1iv_q', 'a1viib', 'a1viic', 'a1viid', 'a1viii_uw_q', 'a1viii_dw_q', 'a1ix_a_q', 'a1ix_b_q', 'axii_code_time'
                        ], 'filter', 'filter' => 'floatval', 'skipOnEmpty'=>true
                    ],
                    
                    
                    [['a1x_d'], 'safe'],
                    
                    //@since 1.3
                    [['axii_flash_index', 'axii_jet_index', 'axii_exp_index', 'axii_pool_index'], 'string', 'max'=>64, 'skipOnEmpty' => true],
                    [['axii_flash_index', 'axii_jet_index', 'axii_exp_index', 'axii_pool_index'], 'match', 'pattern' => '#^[1-9]{1}([0-9])?(,[1-9]{1}([0-9])?){0,20}$#', 'skipOnEmpty' => true],
//                    [['axii_flash_index', 'axii_jet_index', 'axii_exp_index', 'axii_pool_index'], 'match', 'pattern' => '#^[1-9]([0-9])?(,[1-9][0-9]){,4}#', 'message'=>'pula!', 'skipOnEmpty' => true],
                    
                    //finalize scenario validators
                    [['a1i_np_specify'], 'required', 'when'=>function($model) {
                        return isset($model->a1i_np) && $model->a1i_np != 0;
                    }, 'on'=>'finalize', 'message'=>Yii::t('app/messages/errors', 'You must specify the NON-PROCESS release')],
                    [['a1i_2p_h2s_q'], 'required', 'when'=>function($model) {
                        return (isset($model->a1i_g) && $model->a1i_g != 0) || (isset($model->a1i_2p) && $model->a1i_2p != 0);
                    }, 'on'=>'finalize', 'message'=>Yii::t('app/messages/errors', 'Level of H2S must be specified if Gas or 2-Phase releases have been declared')],
                    
                    
                    
                    [['a1_fail_equipment_specify'], 'required', 'when'=>function($model) {
                        return isset($model->a1_fail_equipment_code) && $model->a1_fail_equipment_code == 2;
                    }, 'on'=>'finalize', 'message'=>Yii::t('app/messages/errors', 'You must specify the EQUIPMENT failure')],
                    [['a1_fail_operation_specify'], 'required', 'when'=>function($model) {
                        return isset($model->a1_fail_operation_code) && $model->a1_fail_operation_code == 2;
                    }, 'on'=>'finalize', 'message'=>Yii::t('app/messages/errors', 'You must specify the OPERATION failure')],
                    [['a1_fail_procedural_specify'], 'required', 'when'=>function($model) {
                        return isset($model->a1_fail_procedural_code) && $model->a1_fail_procedural_code == 2;
                    }, 'on'=>'finalize', 'message'=>Yii::t('app/messages/errors', 'You must specify the PROCEDURAL failure')],

                    [['a1viia'], 'required', 'when'=>function($model) {
                        return (isset($model->a1vii_code) && ($model->a1vii_code == 300 || $model->a1vii_code == 301));
                    }, 'on'=>'finalize'],
                    [['a1viib'], 'required', 'when'=>function($model) {
                        return (isset($model->a1vii_code) && ($model->a1vii_code == 300 || $model->a1vii_code == 301));
                    }, 'on'=>'finalize'],

                    //delay time if ignition and delayed
                    [['axii_code_time'], 'required', 'when'=>function($model) {
                        return (isset($model->axii_code) && ($model->axii_code == 0));
                    }, 'on'=>'finalize'],
                            
                            
                    // other means of detection a1x_d_specify         
                    [['a1x_d_specify'], 'required', 'when'=>function($model) {
                        return isset($model->a1x_d) && $model->a1x_d != 0;
                    }, 'on'=>'finalize', 'message'=>Yii::t('app/messages/errors', 'You must specify the OTHER MEANS OF DETECTION')],

                    //delay time if ignition and delayed
                    [['axiv_specify'], 'required', 'when'=>function($model) {
                        return (isset($model->axiv_other) && ($model->axiv_other != 0));
                    }, 'on'=>'finalize', 'message'=>Yii::t('app/messages/errors', 'You must specify the OTHER EMERGENCY ACTION taken')],
                            
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'a1' => Yii::t('app/crf', 'A.1. Was there a release of hydrocarbon substances?'),
			'a1i_np' => Yii::t('app/crf', 'NON-PROCESS'),
			'a1i_np_specify' => Yii::t('app/crf', 'Specify'),
            'a1i_o' => Yii::t('app/crf', 'Oil'),
            'a1i_co' => Yii::t('app/crf', 'Condensate'),
            'a1i_g' => Yii::t('app/crf', 'Gas'),
            'a1i_2p' => Yii::t('app/crf', '2-Phase'),
			'a1i_2p_h2s_q' => Yii::t('app/crf', 'Level of H').'2S',
			'a1i_2p_h2s_u' => Yii::t('app/crf', 'Level of H').'2S '.Yii::t('app','unit'),
                    
			'a1ii_q' => Yii::t('app/crf', 'Estimated quantity released'),
			'a1ii_u' => Yii::t('app/crf', 'Estimated quantity released').' '.Yii::t('app','unit'),
			'a1iii_q' => Yii::t('app/crf', 'Estimated initial release rate'),
			'a1iii_u' => Yii::t('app/crf', 'Estimated initial release rate').' '.Yii::t('app','unit'),
			'a1iv_q' => Yii::t('app/crf', 'Duration of leak'),
			'a1iv_u' => Yii::t('app/crf', 'Duration of leak').' '.Yii::t('app','unit'),
			'a1v' => Yii::t('app/crf', 'Location of leak'),
			
                        'a1vi_code' => Yii::t('app/crf', 'Hazardous area classification'),
                        'a1vii_code' => Yii::t('app/crf', 'Module ventilation?'),
                        'a1viia' => Yii::t('app/crf', 'How many sides enclosed?'),
                        'a1viib' => Yii::t('app/crf', 'Module volume'),
                        'a1viic' => Yii::t('app/crf', 'Estimated number of air changes'),
                        'a1viid' => Yii::t('app/crf', 'Specify hourly rate'),

                        'a1viii'  => Yii::t('app/crf', 'Weather conditions'), //fake just for view
                    
			'a1viii_uw_q' => Yii::t('app/crf', 'Wind speed'),
			'a1viii_uw_u' => Yii::t('app/crf', 'Wind speed').' '.Yii::t('app','unit'),
			'a1viii_dw_q' => Yii::t('app/crf', 'Wind direction'),
			'a1viii_dw_u' => Yii::t('app/crf', 'Wind direction').' '.Yii::t('app','unit'),
			'a1viii_c' => Yii::t('app/crf', 'Description of other relevant weather conditions'),
                    
                        'a1ix'  => Yii::t('app/crf', 'System pressure'), //fake just for view
			'a1ix_a_q' => Yii::t('app/crf', 'Design pressure'),
			'a1ix_a_u' => Yii::t('app/crf', 'Design pressure').' '.Yii::t('app','unit'),
			'a1ix_b_q' => Yii::t('app/crf', 'Actual pressure'),
			'a1ix_b_u' => Yii::t('app/crf', 'Actual pressure').' '.Yii::t('app','unit'),
                        
                        'a1x'  => Yii::t('app/crf', 'Means of detection'), //fake just for view
                        'a1x_a' => Yii::t('app/crf', 'Fire'),
                        'a1x_b' => Yii::t('app/crf', 'Gas'),
                        'a1x_c' => Yii::t('app/crf', 'Smoke'),
                        'a1x_d' => Yii::t('app/crf', 'Other'),
			'a1x_d_specify' => Yii::t('app/crf', 'Other means of detection') . '(' . Yii::t('app', 'specify') . ')',
			
                        'a1xi'  => Yii::t('app/crf', 'Cause of leak'), //fake just for view
                        'axi_description' => Yii::t('app/crf', 'Cause of leak'),
			
                        'axii' => Yii::t('app/crf', 'Did ignition occur?'),
			'axii_code' => Yii::t('app/crf', 'Immediate/Delayed'),
			'axii_code_time' => Yii::t('app/crf', 'Delay time'),
                    
                        'axii_flash_index' => Yii::t('app/crf', 'A flash fire'),
                        'axii_jet_index' => Yii::t('app/crf', 'A jet fire'),
                        'axii_exp_index' => Yii::t('app/crf', 'An explosion'),
                        'axii_pool_index' => Yii::t('app/crf', 'A pool fire'),
                    
			'axiii' => Yii::t('app/crf', 'Ignition source'),
                    
                        'axiv'  => Yii::t('app/crf', 'What emergency action was taken'), //fake just for view
                        'axiv_other' => Yii::t('app/crf', 'Other'),
                        'axiv_specify' => Yii::t('app/crf', 'specify'),
                        'axiv_sd' => Yii::t('app/crf', 'Shutdown'),
                        'axiv_bd' => Yii::t('app/crf', 'Blowdown'),
                        'axiv_d' => Yii::t('app/crf', 'Deluge'),
                        'axiv_co2' => Yii::t('app/crf', 'CO2/Halon/inerts'),
                        'axiv_ctm' => Yii::t('app/crf', 'Call to muster'),

                        'a1xv' => Yii::t('app/crf', 'Any additional comments'),

                        'a1_fail_design_code' => Yii::t('app/crf', '(a) Design:'),
                        //'a1_fail_equipment_code' => Yii::t('app/crf', 'Other') . ', ' . Yii::t('app/crf', 'specify'),
                        'a1_fail_equipment_code' => Yii::t('app/crf', '(b) Equipment:'),
                        //'a1_fail_equipment_specify' => Yii::t('app/crf', 'specify'),
                        'a1_fail_equipment_specify' => Yii::t('app/crf', 'Other') . ', ' . Yii::t('app/crf', 'specify'),
                        //'a1_fail_operation_code' => Yii::t('app/crf', 'Other') . ', ' . Yii::t('app/crf', 'specify'),
                        'a1_fail_operation_code' => Yii::t('app/crf', '(c) Operation:'),
                        //'a1_fail_operation_specify' => Yii::t('app/crf', 'A1 Fail Operation Specify'),
                        'a1_fail_operation_specify' => Yii::t('app/crf', 'Other') . ', ' . Yii::t('app/crf', 'specify'),
                        //'a1_fail_procedural_code' => Yii::t('app/crf', 'Other') . ', ' . Yii::t('app/crf', 'specify'),
                        'a1_fail_procedural_code' => Yii::t('app/crf', '(d) Procedural:'),
                        //'a1_fail_procedural_specify' => Yii::t('app/crf', 'A1 Fail Procedural Specify'),
                        'a1_fail_procedural_specify' => Yii::t('app/crf', 'Other') . ', ' . Yii::t('app/crf', 'specify'),

                        'a1_op' => Yii::t('app/crf', 'Operational mode in the area at the time of release'),
                        'a1_op_specify' => Yii::t('app/crf', 'Specify actual operation. e.g. wire line, well test, etc.'),
		];
	}

        
	private $_ref = '';
	public function getRef()
	{
		return $this->_ref;
	}
	public function setRef($url)
	{
		$this->_ref = $url;
	}
	
	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getSA1Comments()
	{
		return $this->hasMany(SA1Comments::className(), ['s_a_1_id' => 'id']);
	}
	
	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getSA()
	{
		return $this->hasOne(\app\models\events\drafts\sections\SectionA::className(), ['id'=>'sa_id']);
	}
        
        /**
         * @return \yii\db\ActiveQuery
         */
        public function getReleaseCauses()
        {
            return $this->hasMany(LeakCause::className(), ['num_code'=>'leak_cause_id'])
                    ->viaTable('a1_release_causes', ['a1_id'=>'id']);
        }
        

        /**
         * the cascading update of attributes
         */
        public function PrepareForSave() {
            if ($this->oldAttributes['a1'] == 1 && $this->a1 == 0) {
                //reset all attributes BUT a1
                $exceptions = ['id', 'sa_id', 'a1'];
                \app\components\DataCleaning::ResetSection($this, $exceptions);
            }
            else {
                \app\components\DataCleaning::refreshAttributes($this, self::ResetLogicAttributes());
                
                //do transformations;
                $this->a1i_2p_h2s_n = isset($this->a1i_2p_h2s_u) && isset($this->a1i_2p_h2s_q) ? Units::Normalize($this->a1i_2p_h2s_q, $this->a1i_2p_h2s_u) : null;
                $this->a1ii_n = isset($this->a1ii_q) && isset($this->a1ii_u) ? Units::Normalize($this->a1ii_q, $this->a1ii_u) : null;
                $this->a1iii_n = isset($this->a1iii_q) && isset($this->a1iii_u) ? Units::Normalize($this->a1iii_q, $this->a1iii_u) : null;
                $this->a1iv_n = isset($this->a1iv_q) && isset($this->a1iv_u) ? Units::Normalize($this->a1iv_q, $this->a1iv_u) : null;
                $this->a1viii_uw_n = isset($this->a1viii_uw_q) && isset($this->a1viii_uw_u) ? Units::Normalize($this->a1viii_uw_q, $this->a1viii_uw_u) : null;
                $this->a1viii_dw_n = isset($this->a1viii_dw_q) && isset($this->a1viii_dw_u) ? Units::Normalize($this->a1viii_dw_q, $this->a1viii_dw_u) : null;
                $this->a1ix_a_n = isset($this->a1ix_a_q) && isset($this->a1ix_a_u) ? Units::Normalize($this->a1ix_a_q, $this->a1ix_a_u) : null;
                $this->a1ix_b_n = isset($this->a1ix_b_q) && isset($this->a1ix_b_u) ? Units::Normalize($this->a1ix_b_q, $this->a1ix_b_u) : null;
            }
            //\app\components\FileModelLogger::LogDirtyAttributes($this);
        }
        
}
