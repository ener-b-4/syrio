<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\events\drafts\sections\subsections;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\events\drafts\sections\subsections\B2;

/**
 * B2Search represents the model behind the search form about `app\models\events\drafts\sections\subsections\B2`.
 */
class B2Search extends B2
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'b2_1', 'b2_2', 'b2_3', 'b2_4_a', 'b2_4_b', 'b2_4_c', 'b2_6_a_u', 'b2_6_b_u', 'b2_6_c_u', 'b2_6_d_u', 'sb_id'], 'integer', 'min'=>0],  //Author: cavesje Date:20151119
            [['b2_4_a_desc', 'b2_4_b_desc', 'b2_4_c_desc', 'b2_5', 'b2_7'], 'safe'],
            [['b2_6_a_q', 'b2_6_b_q', 'b2_6_c_q', 'b2_6_d_q'], 'number', 'min'=>0],  //Author: cavesje Date:20151119
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = B2::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'b2_1' => $this->b2_1,
            'b2_2' => $this->b2_2,
            'b2_3' => $this->b2_3,
            'b2_4_a' => $this->b2_4_a,
            'b2_4_b' => $this->b2_4_b,
            'b2_4_c' => $this->b2_4_c,
            'b2_6_a_q' => $this->b2_6_a_q,
            'b2_6_a_u' => $this->b2_6_a_u,
            'b2_6_b_q' => $this->b2_6_b_q,
            'b2_6_b_u' => $this->b2_6_b_u,
            'b2_6_c_q' => $this->b2_6_c_q,
            'b2_6_c_u' => $this->b2_6_c_u,
            'b2_6_d_q' => $this->b2_6_d_q,
            'b2_6_d_u' => $this->b2_6_d_u,
            'sb_id' => $this->sb_id,
        ]);

        $query->andFilterWhere(['like', 'b2_4_a_desc', $this->b2_4_a_desc])
            ->andFilterWhere(['like', 'b2_4_b_desc', $this->b2_4_b_desc])
            ->andFilterWhere(['like', 'b2_4_c_desc', $this->b2_4_c_desc])
            ->andFilterWhere(['like', 'b2_5', $this->b2_5])
            ->andFilterWhere(['like', 'b2_7', $this->b2_7]);

        return $dataProvider;
    }
}
