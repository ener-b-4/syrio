<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\events\drafts\sections\subsections;

use Yii;
use app\models\events\drafts\sections\SectionC;

/**
 * This is the model class for table "s_c_4".
 *
 * @property integer $id
 * @property string $c4_1
 * @property integer $sc_id
 *
 * @property SC $sC
 */
class C4 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 's_c_4';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['c4_1'], 'string'],
            [['c4_1'], 'required', 'on'=>'finalize'],
            [['sc_id'], 'required'],
            [['sc_id'], 'integer'],
            [['ref'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'c4_1' => 'C4 1',
            'sc_id' => 'Sc ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSC()
    {
        return $this->hasOne(SectionC::className(), ['id' => 'sc_id']);
    }
    
    private $_ref = '';
    public function getRef()
    {
        return $this->_ref;
    }
    public function setRef($url)
    {
        $this->_ref = $url;
    }    
    
}
