<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\events\drafts\sections\subsections;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\events\drafts\sections\subsections\D1;

/**
 * D1Search represents the model behind the search form about `app\models\events\drafts\sections\subsections\D1`.
 */
class D1Search extends D1
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'sd_id'], 'integer'],
            [['d1_1'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = D1::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'sd_id' => $this->sd_id,
        ]);

        $query->andFilterWhere(['like', 'd1_1', $this->d1_1]);

        return $dataProvider;
    }
}
