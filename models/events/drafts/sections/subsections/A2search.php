<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\events\drafts\sections\subsections;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\events\drafts\sections\subsections\A2;

/**
 * A2search represents the model behind the search form about `app\models\events\drafts\sections\subsections\A2`.
 */
class A2search extends A2
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'a2_1', 'a2_1_u', 'a2_2', 'a2_3'], 'integer', 'min'=>0],  //Author: cavesje Date:20151119
            [['a2_1_q'], 'number', 'min'=>0],  //Author: cavesje Date:20151119
            [['a2_1_t', 'a2_2_desc', 'a2_3_desc'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = A2::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'a2_1' => $this->a2_1,
            'a2_1_q' => $this->a2_1_q,
            'a2_1_u' => $this->a2_1_u,
            'a2_2' => $this->a2_2,
            'a2_3' => $this->a2_3,
        ]);

        $query->andFilterWhere(['like', 'a2_1_t', $this->a2_1_t])
            ->andFilterWhere(['like', 'a2_2_desc', $this->a2_2_desc])
            ->andFilterWhere(['like', 'a2_3_desc', $this->a2_3_desc]);

        return $dataProvider;
    }
}
