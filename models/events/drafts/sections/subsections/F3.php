<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\events\drafts\sections\subsections;

use Yii;
use app\models\events\drafts\sections\SectionF;

/**
 * This is the model class for table "s_f_3".
 *
 * @property integer $id
 * @property string $f3_1
 * @property integer $sf_id
 *
 * @property app\models\events\drafts\sections\SectionI $sI
 */
class F3 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 's_f_3';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['f3_1'], 'string'],
            [['sf_id'], 'required'],
            [['sf_id'], 'integer'],
            
            [['f3_1'], 'required', 'on'=>'finalize'],
            
            [['ref'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'f3_1' => Yii::t('app/crf', 'Preliminary direct and underlying causes'),
            'sf_id' => 'Sf ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSF()
    {
        return $this->hasOne(SectionF::className(), ['id' => 'sf_id']);
    }

    private $_ref = '';
    public function getRef()
    {
        return $this->_ref;
    }
    public function setRef($url)
    {
        $this->_ref = $url;
    }        
}
