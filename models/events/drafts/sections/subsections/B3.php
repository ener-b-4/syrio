<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\events\drafts\sections\subsections;

use Yii;
use app\models\events\drafts\sections\SectionB;

/**
 * This is the model class for table "s_b_3".
 *
 * @property integer $id
 * @property string $b3_1
 * @property integer $sb_id
 * @property integer $is_direct
 *
 * @property SB $sb
 */
class B3 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 's_b_3';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['b3_1'], 'string'],
            [['b3_1'], 'required', 'on'=>'finalize'],
            [['sb_id', 'is_direct'], 'integer', 'min'=>0],  //Author: cavesje Date:20151119
            [['ref'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'b3_1' => Yii::t('app/crf', 'Preliminary direct and underlying causes'),
            'sb_id' => 'the id of the parent SectionA',
            'is_direct' => 'if True this is a direct cause; underlying otherwise',
        ];
    }

    private $_ref = '';
    public function getRef()
    {
        return $this->_ref;
    }
    public function setRef($url)
    {
        $this->_ref = $url;
    }    
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSB()
    {
        return $this->hasOne(SectionB::className(), ['id' => 'sb_id']);
    }
}
