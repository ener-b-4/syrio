<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\events\drafts\sections\subsections;

use Yii;
use app\models\events\drafts\sections\SectionE;

/**
 * This is the model class for table "s_e_2".
 *
 * @property integer $id
 * @property string $e2_1
 * @property integer $se_id
 *
 * @property SE $se
 */
class E2 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 's_e_2';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['e2_1'], 'string'],
            [['se_id'], 'integer'],
            [['e2_1'], 'required', 'on'=>'finalize'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'e2_1' => Yii::t('app/crf', 'Description of circumstances'),
            'se_id' => 'the id of the parent Section E',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSE()
    {
        return $this->hasOne(SectionE::className(), ['id' => 'se_id']);
    }
    
    private $_ref = '';
    public function getRef()
    {
        return $this->_ref;
    }
    public function setRef($url)
    {
        $this->_ref = $url;
    }        
    
}
