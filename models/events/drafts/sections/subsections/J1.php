<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\events\drafts\sections\subsections;

use Yii;
use app\models\events\drafts\sections\SectionJ;

/**
 * This is the model class for table "s_j_1".
 *
 * @property integer $id
 * @property string $j1_1
 * @property integer $sj_id
 *
 * @property app\models\events\drafts\sections\subsections\SectionJ $sJ
 */
class J1 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 's_j_1';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sj_id'], 'integer'],
            [['j1_1'], 'string', 'max' => 128],
            
            [['j1_1'], 'safe', 'on' => 'finalize'],
            [['ref'], 'safe']            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'j1_1' => Yii::t('app/crf', 'Name of contractor'),
            'sj_id' => 'parent section id',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSJ()
    {
        return $this->hasOne(SectionJ::className(), ['id' => 'sj_id']);
    }

    private $_ref = '';
    public function getRef()
    {
        return $this->_ref;
    }
    public function setRef($url)
    {
        $this->_ref = $url;
    }        
}
