<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\events\drafts\sections\subsections;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\events\drafts\sections\subsections\C1;

/**
 * C1ControllerSearch represents the model behind the search form about `app\models\events\drafts\sections\subsections\C1`.
 */
class C1Search extends C1
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'sc_id'], 'integer'],
            [['c1_1'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = C1::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'sc_id' => $this->sc_id,
        ]);

        $query->andFilterWhere(['like', 'c1_1', $this->c1_1]);

        return $dataProvider;
    }
}
