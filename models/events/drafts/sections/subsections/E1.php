<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\events\drafts\sections\subsections;

use Yii;
use app\models\events\drafts\sections\SectionE;

/**
 * This is the model class for table "s_e_1".
 *
 * @property integer $id
 * @property string $e1_1
 * @property string $e1_2
 * @property string $e1_3
 * @property integer $se_id
 *
 * @property SE $se
 */
class E1 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 's_e_1';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['se_id'], 'integer'],
            [['e1_1', 'e1_2', 'e1_3'], 'string', 'max' => 64],
            [['se_id'], 'required', 'on' => 'finalize'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'e1_1' => Yii::t('app/crf', 'Name/code of the vessel'),
            'e1_2' => Yii::t('app/crf', 'Type/tonnage of the vessel'),
            'e1_3' => Yii::t('app/crf', 'Contact via AIS?'),
            'se_id' => 'the id of the parent Section E',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSE()
    {
        return $this->hasOne(SectionE::className(), ['id' => 'se_id']);
    }
    
    private $_ref = '';
    public function getRef()
    {
        return $this->_ref;
    }
    public function setRef($url)
    {
        $this->_ref = $url;
    }    

    
}
