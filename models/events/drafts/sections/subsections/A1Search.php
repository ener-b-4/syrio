<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\events\drafts\sections\subsections;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\events\drafts\sections\subsections\A1;

/**
 * Section1Search represents the model behind the search form about `app\models\crf\Section1`.
 */
class A1Search extends A1
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'a1', 'a1i_np', 'a1i_2p_h2s_u', 'a1ii_u', 'a1iii_u', 'a1iv_u', 'a1vi_code', 'a1vii_code', 'a1viia', 'a1viii_uw_u', 'a1viii_dw_u', 'a1ix_a_u', 'a1ix_b_u', 'a1x_a', 'a1x_b', 'a1x_c', 'a1x_d', 'axii', 'axii_code', 'axii_flash_index', 'axii_jet_index', 'axii_exp_index', 'axii_pool_index', 'axiv_sd', 'axiv_bd', 'axiv_d', 'axiv_co2', 'axiv_ctm', 'a1i_o', 'a1i_co', 'a1i_g', 'a1i_2p', 'a1_op'], 'integer', 'min'=>0], //Author: cavesje Date:20151119
            [['a1i_np_specify', 'a1v', 'a1viii_c', 'a1x_d_specify', 'axi_description', 'axiii', 'a1_op_specify'], 'safe'],
            [['a1i_2p_h2s_q', 'a1ii_q', 'a1iii_q', 'a1iv_q', 'a1viib', 'a1viic', 'a1viii_uw_q', 'a1viii_dw_q', 'a1ix_a_q', 'a1ix_b_q', 'axii_code_time'], 'number', 'min'=>0], //Author: cavesje Date:20151119
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Section1::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'a1' => $this->a1,
            'a1i_np' => $this->a1i_np,
            'a1i_2p_h2s_q' => $this->a1i_2p_h2s_q,
            'a1i_2p_h2s_u' => $this->a1i_2p_h2s_u,
            'a1ii_q' => $this->a1ii_q,
            'a1ii_u' => $this->a1ii_u,
            'a1iii_q' => $this->a1iii_q,
            'a1iii_u' => $this->a1iii_u,
            'a1iv_q' => $this->a1iv_q,
            'a1iv_u' => $this->a1iv_u,
            'a1vi_code' => $this->a1vi_code,
            'a1vii_code' => $this->a1vii_code,
            'a1viia' => $this->a1viia,
            'a1viib' => $this->a1viib,
            'a1viic' => $this->a1viic,
            'a1viii_uw_q' => $this->a1viii_uw_q,
            'a1viii_uw_u' => $this->a1viii_uw_u,
            'a1viii_dw_q' => $this->a1viii_dw_q,
            'a1viii_dw_u' => $this->a1viii_dw_u,
            'a1ix_a_q' => $this->a1ix_a_q,
            'a1ix_a_u' => $this->a1ix_a_u,
            'a1ix_b_q' => $this->a1ix_b_q,
            'a1ix_b_u' => $this->a1ix_b_u,
            'a1x_a' => $this->a1x_a,
            'a1x_b' => $this->a1x_b,
            'a1x_c' => $this->a1x_c,
            'a1x_d' => $this->a1x_d,
            'axii' => $this->axii,
            'axii_code' => $this->axii_code,
            'axii_code_time' => $this->axii_code_time,
            'axii_flash_index' => $this->axii_flash_index,
            'axii_jet_index' => $this->axii_jet_index,
            'axii_exp_index' => $this->axii_exp_index,
            'axii_pool_index' => $this->axii_pool_index,
            'axiv_sd' => $this->axiv_sd,
            'axiv_bd' => $this->axiv_bd,
            'axiv_d' => $this->axiv_d,
            'axiv_co2' => $this->axiv_co2,
            'axiv_ctm' => $this->axiv_ctm,
            'a1i_o' => $this->a1i_o,
            'a1i_co' => $this->a1i_co,
            'a1i_g' => $this->a1i_g,
            'a1i_2p' => $this->a1i_2p,
            'a1_op' => $this->a1_op,
        ]);

        $query->andFilterWhere(['like', 'a1i_np_specify', $this->a1i_np_specify])
            ->andFilterWhere(['like', 'a1v', $this->a1v])
            ->andFilterWhere(['like', 'a1viii_c', $this->a1viii_c])
            ->andFilterWhere(['like', 'a1x_d_specify', $this->a1x_d_specify])
            ->andFilterWhere(['like', 'axi_description', $this->axi_description])
            ->andFilterWhere(['like', 'axiii', $this->axiii])
            ->andFilterWhere(['like', 'a1_op_specify', $this->a1_op_specify]);

        return $dataProvider;
    }
}
