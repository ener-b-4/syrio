<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\events\drafts\sections\subsections;

use Yii;
use app\models\events\drafts\sections\SectionI;

/**
 * This is the model class for table "s_i_3".
 *
 * @property integer $id
 * @property string $i3_1
 * @property integer $si_id
 *
 * @property app\models\events\drafts\sections\SectionI $sI
 */
class I3 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 's_i_3';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['i3_1'], 'string'],
            [['si_id'], 'required'],
            [['si_id'], 'integer'],
            
            [['i3_1'], 'required', 'on'=>'finalize'],
            
            [['ref'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'i3_1' => Yii::t('app/crf', 'Preliminary direct and underlying causes'),
            'si_id' => 'Si ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSI()
    {
        return $this->hasOne(SectionI::className(), ['id' => 'si_id']);
    }

    private $_ref = '';
    public function getRef()
    {
        return $this->_ref;
    }
    public function setRef($url)
    {
        $this->_ref = $url;
    }        
}
