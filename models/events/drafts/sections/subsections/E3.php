<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\events\drafts\sections\subsections;

use Yii;
use app\models\events\drafts\sections\SectionE;

/**
 * This is the model class for table "s_e_3".
 *
 * @property integer $id
 * @property string $e3_1
 * @property integer $se_id
 *
 * @property app\models\events\drafts\sections\SectionI $sI
 */
class E3 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 's_e_3';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['e3_1'], 'string'],
            [['se_id'], 'required'],
            [['se_id'], 'integer'],
            
            [['e3_1'], 'required', 'on'=>'finalize'],
            
            [['ref'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'e3_1' => Yii::t('app/crf', 'Preliminary direct and underlying causes'),
            'se_id' => 'se id',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSE()
    {
        return $this->hasOne(SectionE::className(), ['id' => 'se_id']);
    }

    private $_ref = '';
    public function getRef()
    {
        return $this->_ref;
    }
    public function setRef($url)
    {
        $this->_ref = $url;
    }        
}
