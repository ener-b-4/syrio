<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\events\drafts\sections\subsections;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\events\drafts\sections\subsections\B1;

/**
 * B1Search represents the model behind the search form about `app\models\events\drafts\sections\subsections\B1`.
 */
class B1Search extends B1
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'b1_3', 'b1_5', 'b1_6', 'b1_8_1_u', 'b1_8_2_u', 'b1_8_3_u', 'b1_9', 'sb_id'], 'integer', 'min'=>0],  //Author: cavesje Date:20151119
            [['b1_1', 'b1_4_s', 'b1_4_e', 'b1_9_details'], 'safe'],
            [['b1_2', 'b1_7', 'b1_8_1_q', 'b1_8_2_q', 'b1_8_3_q'], 'number', 'min'=>0],  //Author: cavesje Date:20151119
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = B1::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'b1_2' => $this->b1_2,
            'b1_3' => $this->b1_3,
            'b1_4_s' => $this->b1_4_s,
            'b1_4_e' => $this->b1_4_e,
            'b1_5' => $this->b1_5,
            'b1_6' => $this->b1_6,
            'b1_7' => $this->b1_7,
            'b1_8_1_q' => $this->b1_8_1_q,
            'b1_8_1_u' => $this->b1_8_1_u,
            'b1_8_2_q' => $this->b1_8_2_q,
            'b1_8_2_u' => $this->b1_8_2_u,
            'b1_8_3_q' => $this->b1_8_3_q,
            'b1_8_3_u' => $this->b1_8_3_u,
            'b1_9' => $this->b1_9,
            'sb_id' => $this->sb_id,
        ]);

        $query->andFilterWhere(['like', 'b1_1', $this->b1_1])
            ->andFilterWhere(['like', 'b1_9_details', $this->b1_9_details]);

        return $dataProvider;
    }
}
