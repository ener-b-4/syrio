<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\events\drafts\sections\subsections;

use Yii;
use app\models\events\drafts\sections\SectionI;

/**
 * This is the model class for table "s_i_1".
 *
 * @property integer $id
 * @property string $i1_1_s
 * @property string $i1_1_e
 * @property integer $si_id
 *
 * @property app\models\events\drafts\sections\SectionI $sI
 */
class I1 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 's_i_1';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['i1_1_s', 'i1_1_e'], 'safe'],
            ['i1_1_s', 'validate_date'],
            ['i1_1_e', 'validate_date'],
            [['i1_1_s'], 'required'],            
            //[['i1_1_e'], 'required', 'message'=>'Required!'],            
            [['i1_1_s'], 'required', 'on'=>'finalize'],            
            //[['i1_1_e'], 'required', 'on'=>'finalize'],   
          
            //Author : cavesje  Date : 20150616 - Format of datetime is wrong
            //[['i1_1_s', 'i1_1_e'], 'date', 'format' => 'yyyy-MM-dd kk:mm:ss', 'message'=>'Please provide the date in the \'yyyy-MM-dd hh:mm\' format!'],            
            [['i1_1_s', 'i1_1_e'], 'date', 'format' => 'yyyy-MM-dd HH:mm:ss', 'message'=>'Please provide the date in the \'yyyy-MM-dd hh:mm\' format!'],            
          
            [['si_id'], 'integer'],
            [['ref'], 'safe']     
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'i1_1' => Yii::t('app/crf', 'Start and end date/time of evacuation'), //dummy 
            'i1_1_s' => Yii::t('app', 'Start date/time of evacuation'),
            'i1_1_e' => Yii::t('app', 'End date/time of evacuation'),
            'si_id' => 'parent section id',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSI()
    {
        return $this->hasOne(SectionI::className(), ['id' => 'si_id']);
    }
    
    private $_ref = '';
    public function getRef()
    {
        return $this->_ref;
    }
    public function setRef($url)
    {
        $this->_ref = $url;
    }    

    
    function validate_date($attribute, $params) {
        if (isset($this->i1_1_s) && isset($this->i1_1_e) && strlen(trim($this->i1_1_e)) > 0) {
            $endDate = date($this->i1_1_e);
            $startDate = date($this->i1_1_s);
            
            if ($startDate>$endDate) {
                $this->addError('i1_1_s', Yii::t('app', 'Start date cannot be higher than the end date.'));
            }
            if ($startDate>$endDate) {
                $this->addError('i1_1_e', Yii::t('app', 'End date cannot be smaller than the start date.'));
            }
        }
    }
    
}
