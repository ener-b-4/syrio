<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\events\drafts\sections\subsections;

use Yii;
use app\models\events\drafts\sections\SectionA;
use app\models\units\Units;

/**
 * This is the model class for table "s_a_2".
 *
 * @property integer $id
 * @property integer $sa_id
 * @property integer $a2_desc
 * @property integer $a2_1
 * @property double $a2_1_q
 * @property integer $a2_1_u
 * @property double $a2_1_n
 * @property string $a2_1_t
 * @property integer $a2_2
 * @property string $a2_2_desc
 * @property integer $a2_3
 * @property string $a2_3_desc
 */
class A2 extends \yii\db\ActiveRecord
{
    
    public static function ResetLogicAttributes() {
        return [
            ['a2_1'     ,   0,  ['a2_1_t', 'a2_1_q']],
            ['a2_2'     ,   0,  ['a2_2_desc']],
            ['a2_3'     ,   0,  ['a2_3_desc']],
        ];
    }
    
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 's_a_2';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['a2_desc', 'a2_1', 'a2_1_u', 'a2_2', 'a2_3', 'sa_id', 'a2_1_q', 'a2_1_t'], 'safe'],
            [['a2_desc', 'a2_1'], 'required', 'on'=>'finalize'],
            [['a2_desc'], 'string'],
            [['a2_1_t', 'a2_1_q', 'a2_1_u'], 'required', 'when'=>function($model) {
                return $model->a2_1==1;
            }, 'whenClient' => "function (attribute, value) {
                return $('#a2_1_y').is(':checked');
            }"],
            [['a2_1', 'a2_1_u', 'a2_2', 'a2_3', 'sa_id'], 'integer', 'min'=>0],  
            [['a2_1_q'], 'number', 'min'=>0], //Author: cavesje Date:20151119
            [['a2_2_desc', 'a2_3_desc'], 'string'],
            [['a2_2_desc'], 'required', 'when'=>function($model) {
                return $model->a2_2==1;
            }, 'whenClient' => "function (attribute, value) {
                return $('#a2_2').is(':checked');
            }"],
            [['a2_3_desc'], 'required', 'when'=>function($model) {
                return $model->a2_3==1;
            }, 'whenClient' => "function (attribute, value) {
                return $('#a2_3').is(':checked');
            }"],

            [['a2_1_t'], 'string', 'max' => 45],
            [['ref'], 'safe'],

                    
            [[ 'a2_1', 'a2_2', 'a2_3' ], 'default', 'value' => null],
            [[ 'a2_1', 'a2_2', 'a2_3', 'a2_1_u' ], 'filter', 'filter' => 'intval', 'skipOnEmpty' => true],
            [[ 'a2_1_q' ], 'filter', 'filter' => 'floatval'],

            //delay time if ignition and delayed
            [['a2_desc'], 'required', 'when'=>function($model) {
                return (isset($model->a2_2) && ($model->a2_2 != 0));
            }, 'on'=>'finalize'],
                    
                    
        ];
    }

    
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sa_id' => 'sa id',
            'a2_desc' => Yii::t('app/crf', 'Description of circumstances, consequences of event and emergency response'),
            'a2_1' => Yii::t('app/crf', 'Was there a release of a non-hydrocarbon hazardous substance?'),
            'a2_1_q' => Yii::t('app', 'released quantity'),
            'a2_1_u' => Yii::t('app', 'released quantity').' '.Yii::t('app','unit'),
            'a2_1_t' => ucfirst(Yii::t('app', 'type')),
            'a2_2' => Yii::t('app/crf', 'Was there a non-hydrocarbon fire (e.g. electrical) with a significant potential to cause a major accident?'),
            'a2_2_desc' => Yii::t('app', 'Describe circumstances'),
            'a2_3' => Yii::t('app/crf', 'Is the incident likely to cause degradation to the surrounding marine environment?'),
            'a2_3_desc' => Yii::t('app', 'If yes, outline the environmental  impacts which have already been observed  or are likely to result from the incident'),
        ];
    }
    
    /**
     * 
     * @param bool $insert if true the record is new
     * @return boolean
     * 
     * @author vamanbo <bogdan.vamanu@jrc.ec.europa.eu>
     * @version 1.0.20150906
     * 
     */
    public function beforeSave($insert) {
        if (count($this->dirtyAttributes) > 0) {
            $this->PrepareForSave();
        }
        
        //do transformations;
        $this->a2_1_n = isset($this->a2_1_u) && isset($this->a2_1_q) ? Units::Normalize($this->a2_1_q, $this->a2_1_u) : null;
        
        return parent::beforeSave($insert);            //return parent::beforeSave($insert);
    }    
    
    private $_ref = '';
    public function getRef()
    {
        return $this->_ref;
    }
    public function setRef($url)
    {
        $this->_ref = $url;
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSA()
    {
        return $this->hasOne(SectionA::className(), ['id'=>'sa_id']);
    }

    /**
     * the cascading update of attributes
     */
    public function PrepareForSave() {
        
        //apply reset logic
        \app\components\DataCleaning::refreshAttributes($this, self::ResetLogicAttributes());

        //\app\components\FileModelLogger::LogDirtyAttributes($this);
    }
    
    
}
