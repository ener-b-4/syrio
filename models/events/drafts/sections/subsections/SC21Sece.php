<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\events\drafts\sections\subsections;

use Yii;

/**
 * This is the model class for table "s_c_2_1_sece".
 *
 * @property integer $s_c_2_1_id
 * @property integer $sece_id
 *
 * @property SC21 $sC21
 * @property Sece $sece
 */
class SC21Sece extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 's_c_2_1_sece';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['s_c_2_1_id', 'sece_id'], 'required'],
            [['s_c_2_1_id', 'sece_id'], 'integer', 'min'=>0]  //Author: cavesje Date:20151119
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            's_c_2_1_id' => 'S C 2 1 ID',
            'sece_id' => 'Sece ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSC21()
    {
        return $this->hasOne(SC21::className(), ['id' => 's_c_2_1_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSece()
    {
        return $this->hasOne(Sece::className(), ['id' => 'sece_id']);
    }
}
