<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\events\drafts\sections\subsections;

use Yii;
use app\models\events\drafts\sections\SectionE;

/**
 * This is the model class for table "s_e_4".
 *
 * @property integer $id
 * @property string $e4_1
 * @property integer $se_id
 *
 * @property app\models\events\drafts\sections\SectionI $sI
 */
class E4 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 's_e_4';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['e4_1'], 'string'],
            [['e4_1'], 'required', 'on'=>'finalize'],
            [['se_id'], 'integer'],

            [['ref'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'e4_1' => Yii::t('app', 'Initial lessons learned and preliminary recommendations'),
            'se_id' => 'parent section id',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSE()
    {
        return $this->hasOne(SectionE::className(), ['id' => 'se_id']);
    }

    private $_ref = '';
    public function getRef()
    {
        return $this->_ref;
    }
    public function setRef($url)
    {
        $this->_ref = $url;
    }        
}
