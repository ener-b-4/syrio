<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\events\drafts\sections\subsections;

use Yii;
use app\models\events\drafts\sections\SectionJ;

/**
 * This is the model class for table "s_j_2".
 *
 * @property integer $id
 * @property string $j2_1
 * @property integer $sj_id
 *
 * @property app\models\events\drafts\sections\subsections\SectionJ $sJ
 */
class J2 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 's_j_2';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['j2_1'], 'string'],
            [['sj_id'], 'integer'],
            
            [['j2_1'], 'required', 'on'=>'finalize'],
            
            [['ref'], 'safe'],   
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'j2_1' => Yii::t('app/crf', 'Description of circumstances, consequences of event and emergency response'),
            'sj_id' => 'parent section id',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSJ()
    {
        return $this->hasOne(SectionJ::className(), ['id' => 'sj_id']);
    }
    
    private $_ref = '';
    public function getRef()
    {
        return $this->_ref;
    }
    public function setRef($url)
    {
        $this->_ref = $url;
    }            
}
