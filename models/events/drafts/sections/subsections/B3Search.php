<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\events\drafts\sections\subsections;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\events\drafts\sections\subsections\B3;

/**
 * B3Search represents the model behind the search form about `app\models\events\drafts\sections\subsections\B3`.
 */
class B3Search extends B3
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'sb_id', 'is_direct'], 'integer', 'min'=>0],  //Author: cavesje Date:20151119
            [['b3_1'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = B3::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'sb_id' => $this->sb_id,
            'is_direct' => $this->is_direct,
        ]);

        $query->andFilterWhere(['like', 'b3_1', $this->b3_1]);

        return $dataProvider;
    }
}
