<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\events\drafts\sections\subsections;

use Yii;
use app\models\events\drafts\sections\subsections\C2;

/**
 * This is the model class for table "s_c_2_2".
 *
 * @property integer $id
 * @property integer $c22_1
 * @property string $c22_desc
 * @property integer $sc_2_id
 *
 * @property SC2 $sc2
 */
class C2_2 extends \yii\db\ActiveRecord
{
    public static function ResetLogicAttributes() {
        return [
            ['c22_1'    ,   0,  ['c22_desc']],
        ];
    }
    
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 's_c_2_2';
    }

    function beforeSave($insert) {
        
        if (count($this->dirtyAttributes) > 0) {
            $this->PrepareForSave();
        }
        
        //do transformations;
        //none in this case
        
        return parent::beforeSave($insert);            //return parent::beforeSave($insert);
    }
    
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['c22_1', 'sc_2_id'], 'integer'],
            [['c22_desc'], 'string', 'skipOnEmpty' => true],
            [['sc_2_id'], 'required'],
            [['c22_1'], 'safe'],
            [['c22_desc'], 'required', 'when' => function($model){
                return $model->c22_1 > 0;
            }, 'enableClientValidation' => false, 'message' => Yii::t('app', 'You must outline the observed environmental impacts.') 
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'c22_1' => Yii::t('app', 'Is the incident likely to cause degradation to the surrounding marine environment?'),
            'c22_desc' => Yii::t('app', 'If yes, outline the environmental  impacts which have already been observed  or are likely to result from the incident.'),
            'sc_2_id' => 'Sc 2 ID',
            
            [[ 'c22_1' ], 'default', 'value' => null],
            [[ 'c22_1' ], 'filter', 'filter' => 'intval', 'skipOnEmpty' => true],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSC2()
    {
        return $this->hasOne(SC2::className(), ['id' => 'sc_2_id']);
    }

    public function PrepareForSave() {
        
        //apply reset logic
        \app\components\DataCleaning::refreshAttributes($this, self::ResetLogicAttributes());

        //\app\components\FileModelLogger::LogDirtyAttributes($this);
    }
    
}
