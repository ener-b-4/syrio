<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\events\drafts\sections\subsections;

use Yii;
use app\models\events\drafts\sections\SectionJ;

/**
 * This is the model class for table "s_j_3".
 *
 * @property integer $id
 * @property string $j3_1
 * @property integer $sj_id
 * @property string $ref
 *
 * @property app\models\events\drafts\sections\subsections\SectionJ $sJ
 */
class J3 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 's_j_3';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['j3_1'], 'string'],
            [['sj_id'], 'required'],
            [['sj_id'], 'integer'],
            
            [['j3_1'], 'required', 'on'=>'finalize'],
            
            [['ref'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'j3_1' => Yii::t('app/crf', 'Preliminary direct and underlying causes'),
            'sj_id' => 'Sj ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSJ()
    {
        return $this->hasOne(SectionJ::className(), ['id' => 'sj_id']);
    }
    
    private $_ref = '';
    public function getRef()
    {
        return $this->_ref;
    }
    public function setRef($url)
    {
        $this->_ref = $url;
    }            
}
