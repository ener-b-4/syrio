<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\events\drafts\sections\subsections;

use Yii;
use app\models\events\drafts\sections\SectionB;
use app\models\units\Units;

/**
 * This is the model class for table "s_b_1".
 *
 * @property integer $id
 * @property string $b1_1
 * @property double $b1_2
 * @property integer $b1_3
 * @property string $b1_4_s
 * @property string $b1_4_e
 * @property integer $b1_5
 * @property integer $b1_6
 * @property double $b1_7
 * @property double $b1_8_1_q
 * @property integer $b1_8_1_u
 * @property double $b1_8_1_n
 * @property double $b1_8_2_q
 * @property integer $b1_8_2_u
 * @property double $b1_8_2_n
 * @property double $b1_8_3_q
 * @property integer $b1_8_3_u
 * @property double $b1_8_3_n
 * @property integer $b1_9
 * @property integer $b1_10
 * @property string $b1_9_details
 * @property integer $sb_id
 *
 * @property SB $sb
 */
class B1 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 's_b_1';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['b1_1'], 'required', 'on'=>'finalize'],
            [['b1_7', 'b1_8_1_q', 'b1_8_2_q', 'b1_8_3_q'], 'number', 'min'=>0],  //Author: cavesje Date:20151119
            [['b1_5', 'b1_6', 'b1_8_1_u', 'b1_8_2_u', 'b1_8_3_u', 'b1_9', 'b1_10', 'sb_id'], 'integer', 'min'=>0],  //Author: cavesje Date:20151119
            [['b1_4_s', 'b1_4_e'], 'safe'],
            [['b1_4_s', 'b1_4_e'], 'date', 'format' => 'yyyy-MM-dd HH:mm:ss', 'message'=>'Please provide the date in the \'yyyy-MM-dd hh:mm:ss\' format!', 'on'=>'finalize'],            
            ['b1_4_s', 'validate_date', 'skipOnEmpty'=>true],
            ['b1_4_e', 'validate_date', 'skipOnEmpty'=>true],
            //[['b1_4_e'], 'required'],            
            [['b1_4_s'], 'required', 'on'=>'finalize'],            
            //[['b1_4_e'], 'required', 'on'=>'finalize'],            
            [['b1_1', 'b1_2', 'b1_3'], 'string', 'max' => 64],
            [['ref'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'b1_1' => Yii::t('app', 'Name/code of the well'),
            'b1_2' => Yii::t('app/crf', 'Name of drilling contractor (if relevant)'),
            'b1_3' => Yii::t('app/crf', 'Name/type of drilling rig (if relevant)'),
            'b1_4' => Yii::t('app/crf', 'Start and end date/time of loss of well control'),
            'b1_4_s' => Yii::t('app', 'Start date/time of loss of well control'),
            'b1_4_e' => Yii::t('app', 'End date/time of loss of well control'),
            'b1_5' => Yii::t('app', 'Type of fluid: brine / oil / gas'),
            'b1_6' => Yii::t('app/crf', 'Well head completion: surface / subsea'),
            'b1_8' => Yii::t('app/crf', 'Reservoir: pressure / temperature / depth'),
            'b1_7' => Yii::t('app/crf', 'Water depth') . ' (m)',
            'b1_8_1_q' => Yii::t('app', 'Reservoir pressure'),
            'b1_8_1_u' => Yii::t('app', 'Reservoir pressure').' '.Yii::t('app','unit'),
            'b1_8_2_q' => Yii::t('app', 'Reservoir temperature'),
            'b1_8_2_u' => Yii::t('app', 'Reservoir temperature unit').' '.Yii::t('app','unit'),
            'b1_8_3_q' => Yii::t('app', 'Reservoir depth'),
            'b1_8_3_u' => Yii::t('app', 'Reservoir depth unit').' '.Yii::t('app','unit'),
            'b1_9' => Yii::t('app/crf', 'Type of activity'),
            'b1_9_details' => Yii::t('app/crf', 'Type of activity'),
            'b1_10' => Yii::t('app', 'Type of well services'),
            'sb_id' => 'parent section id',
        ];
    }

    
    /**
     * 
     * @param bool $insert if true the record is new
     * @return boolean
     * 
     * @author vamanbo <bogdan.vamanu@jrc.ec.europa.eu>
     * @version 1.0.20150906
     * 
     */
    public function beforeSave($insert) {
        if (parent::beforeSave($insert))
        {
            $this->b1_8_1_n = isset($this->b1_8_1_u) && isset($this->b1_8_1_q) ? Units::Normalize($this->b1_8_1_q, $this->b1_8_1_u) : null;
            $this->b1_8_2_n = isset($this->b1_8_2_u) && isset($this->b1_8_2_q) ? Units::Normalize($this->b1_8_2_q, $this->b1_8_2_u) : null;
            $this->b1_8_3_n = isset($this->b1_8_3_u) && isset($this->b1_8_3_q) ? Units::Normalize($this->b1_8_3_q, $this->b1_8_3_u) : null;
        
            return true;
        }
        else
        {
            return FALSE;
        }
    }        
    
    
    private $_ref = '';
    public function getRef()
    {
        return $this->_ref;
    }
    public function setRef($url)
    {
        $this->_ref = $url;
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSB()
    {
        return $this->hasOne(SectionB::className(), ['id' => 'sb_id']);
    }
    
    
    function validate_date($attribute, $params) {
        //echo $this->b1_4_e;
        //die();
        if (isset($this->b1_4_s) && isset($this->b1_4_e) && strlen(trim($this->b1_4_e)) > 0) {
            $endDate = date($this->b1_4_e);
            $startDate = date($this->b1_4_s);
            
            if ($startDate>$endDate) {
                if ($attribute == 'b1_4_s') {
                    $this->addError($attribute, Yii::t('app', 'Start date cannot be higher than the end date.'));   
                }
                else
                {
                    $this->addError($attribute, Yii::t('app', 'End date cannot be smaller than the start date.'));   
                }
            }
        } 
    }
    
}
