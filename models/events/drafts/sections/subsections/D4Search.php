<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\events\drafts\sections\subsections;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\events\drafts\sections\subsections\D4;

/**
 * D4Search represents the model behind the search form about `app\models\events\drafts\sections\subsections\D4`.
 */
class D4Search extends D4
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'is_lesson_learn', 'sd_id'], 'integer', 'min'=>0],  //Author: cavesje Date:20151119
            [['d4_1'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = D4::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'is_lesson_learn' => $this->is_lesson_learn,
            'sd_id' => $this->sd_id,
        ]);

        $query->andFilterWhere(['like', 'd4_1', $this->d4_1]);

        return $dataProvider;
    }
}
