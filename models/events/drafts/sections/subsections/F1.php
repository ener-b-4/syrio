<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\events\drafts\sections\subsections;

use Yii;
use app\models\events\drafts\sections\SectionF;


/**
 * This is the model class for table "s_f_1".
 *
 * @property integer $id
 * @property string $f1_1
 * @property string $f1_2
 * @property string $f1_3
 * @property integer $sf_id
 *
 * @property SF $sf
 */
class F1 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 's_f_1';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sf_id'], 'integer'],
            [['f1_1', 'f1_2'], 'string', 'max' => 64],
            [['f1_3'], 'integer', 'min'=>0],
            [['f1_1', 'f1_2', 'f1_3'], 'required', 'on'=>'finalize'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'f1_1' => Yii::t('app/crf', 'Name of helicopter contractor'),
            'f1_2' => Yii::t('app/crf', 'Helicopter type'),
            'f1_3' => Yii::t('app/crf', 'Number of persons on board'),
            'sf_id' => 'the id of the parent Section F',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSF()
    {
        return $this->hasOne(SectionF::className(), ['id' => 'sf_id']);
    }
    
    private $_ref = '';
    public function getRef()
    {
        return $this->_ref;
    }
    public function setRef($url)
    {
        $this->_ref = $url;
    }    

    
}
