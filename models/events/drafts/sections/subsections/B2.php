<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\events\drafts\sections\subsections;

use Yii;
use app\models\events\drafts\sections\SectionB;
use app\models\units\Units;

/**
 * This is the model class for table "s_b_2".
 *
 * @property integer $id
 * @property integer $b2_1
 * @property integer $b2_2
 * @property integer $b2_3
 * @property integer $b2_4_a
 * @property string $b2_4_a_desc
 * @property integer $b2_4_b
 * @property string $b2_4_b_desc
 * @property integer $b2_4_c
 * @property string $b2_4_c_desc
 * @property string $b2_5
 * @property double $b2_6_a_q
 * @property integer $b2_6_a_u
 * @property double $b2_6_a_n
 * @property double $b2_6_b_q
 * @property integer $b2_6_b_u
 * @property double $b2_6_b_n
 * @property double $b2_6_c_q
 * @property integer $b2_6_c_u
 * @property double $b2_6_c_n
 * @property double $b2_6_d_q
 * @property integer $b2_6_d_u
 * @property double $b2_6_d_n
 * @property string $b2_7
 * @property integer $sb_id
 *
 * @property SB $sb
 */
class B2 extends \yii\db\ActiveRecord
{
    
    public static function ResetLogicAttributes() {
        return [
            ['b2_4_a'     ,   0,  ['b2_4_a_desc']],
            ['b2_4_b'     ,   0,  ['b2_4_b_desc']],
            ['b2_4_c'     ,   0,  ['b2_4_c_desc']],
        //    ['a2_3'     ,   0,  ['a2_3_desc']],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 's_b_2';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['b2_5', 'b2_7'], 'safe'],
            [['b2_1', 'b2_2', 'b2_3', 'b2_4_a', 'b2_4_b', 'b2_4_c', 'b2_6_a_u', 'b2_6_b_u', 'b2_6_c_u', 'b2_6_d_u', 'sb_id'], 'integer', 'min'=>0, 'skipOnEmpty' => true],  //Author: cavesje Date:20151119
            [['b2_5', 'b2_7'], 'string', 'skipOnEmpty' => true],
            [['b2_6_a_q', 'b2_6_b_q', 'b2_6_c_q', 'b2_6_d_q'], 'number', 'min'=>0, 'skipOnEmpty'=>true],  //Author: cavesje Date:20151119
            [['b2_4_a_desc', 'b2_4_b_desc', 'b2_4_c_desc'], 'string', 'max' => 64, 'skipOnEmpty'=>true],
            //[['b2_1', 'b2_2', 'b2_5'], 'required'],
            [['b2_1', 'b2_2', 'b2_5'], 'required', 'on'=>'finalize'],
            [['ref'], 'safe'],
            
            [['b2_6_a_q', 'b2_6_b_q', 'b2_6_c_q', 'b2_6_d_q'], 'default', 'value'=>null],
            [['b2_6_a_q', 'b2_6_b_q', 'b2_6_c_q', 'b2_6_d_q'], 'filter', 'filter'=>'floatval', 'skipOnEmpty'=>true],
            [['b2_6_a_u', 'b2_6_b_u', 'b2_6_c_u', 'b2_6_d_u'], 'filter', 'filter'=>'intval', 'skipOnEmpty'=>true],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'b2_1' => Yii::t('app/crf', 'Blowout prevention equipment activated'),
            'b2_2' => Yii::t('app/crf', 'Diverter system in operation'),
            'b2_3' => Yii::t('app/crf', 'Pressure build-up and/or positive flow check'),
            'b2_4' => Yii::t('app/crf', 'Failing well barriers'),   //fake
            'b2_4_a' => Yii::t('app/crf', 'Failing well barrier ({evt})', ['evt'=>'a']),
            'b2_4_a_desc' => Yii::t('app/crf', 'Failing well barrier ({evt})', ['evt'=>'a']),
            'b2_4_b' => Yii::t('app/crf', 'Failing well barrier ({evt})', ['evt'=>'b']),
            'b2_4_b_desc' => Yii::t('app/crf', 'Failing well barrier ({evt})', ['evt'=>'b']),
            'b2_4_c' => Yii::t('app/crf', 'Failing well barrier ({evt})', ['evt'=>'c']),
            'b2_4_c_desc' => Yii::t('app/crf', 'Failing well barrier ({evt})', ['evt'=>'c']),
            'b2_5' => Yii::t('app/crf', 'Description of circumstances'),
            'b2_6_a_q' => Yii::t('app/crf', 'Duration of uncontrolled flow of well-fluids'),
            'b2_6_a_u' => Yii::t('app/crf', 'Duration of uncontrolled flow of well-fluids').' '.Yii::t('app','unit'),
            'b2_6_b_q' => Yii::t('app/crf', 'Flowrate'),
            'b2_6_b_u' => Yii::t('app/crf', 'Flowrate').' '.Yii::t('app','unit'),
            'b2_6_c_q' => Yii::t('app/crf', 'Liquid volume'),
            'b2_6_c_u' => Yii::t('app/crf', 'Liquid volume').' '.Yii::t('app','unit'),
            'b2_6_d_q' => Yii::t('app/crf', 'Gas volume'),
            'b2_6_d_u' => Yii::t('app/crf', 'Gas volume').' '.Yii::t('app','unit'),
            'b2_7' => Yii::t('app', 'Consequences of event and emergency response'),
            'sb_id' => 'parent section id',
        ];
    }

    /**
     * 
     * @param bool $insert if true the record is new
     * @return boolean
     * 
     * @author vamanbo <bogdan.vamanu@jrc.ec.europa.eu>
     * @version 1.0.20150906
     * 
     */
    public function beforeSave($insert) {
        
        if (count($this->dirtyAttributes) > 0) {
            $this->PrepareForSave();
        }
        
        //do transformations;
        $this->b2_6_a_n = isset($this->b2_6_a_u) && isset($this->b2_6_a_q) ? Units::Normalize($this->b2_6_a_q, $this->b2_6_a_u) : null;
        $this->b2_6_b_n = isset($this->b2_6_b_u) && isset($this->b2_6_b_q) ? Units::Normalize($this->b2_6_b_q, $this->b2_6_b_u) : null;
        $this->b2_6_c_n = isset($this->b2_6_c_u) && isset($this->b2_6_c_q) ? Units::Normalize($this->b2_6_c_q, $this->b2_6_c_u) : null;
        $this->b2_6_d_n = isset($this->b2_6_d_u) && isset($this->b2_6_d_q) ? Units::Normalize($this->b2_6_d_q, $this->b2_6_d_u) : null;
        
        return parent::beforeSave($insert);            //return parent::beforeSave($insert);
        
    }            
    
    private $_ref = '';
    public function getRef()
    {
        return $this->_ref;
    }
    public function setRef($url)
    {
        $this->_ref = $url;
    }
    
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSB()
    {
        return $this->hasOne(SectionB::className(), ['id' => 'sb_id']);
    }
    
    /**
     * the cascading update of attributes
     */
    public function PrepareForSave() {
        
        //apply reset logic
        \app\components\DataCleaning::refreshAttributes($this, self::ResetLogicAttributes());

        //\app\components\FileModelLogger::LogDirtyAttributes($this);
    }
    
}
