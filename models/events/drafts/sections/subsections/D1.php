<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\events\drafts\sections\subsections;

use Yii;
use app\models\events\drafts\sections\SectionD;

/**
 * This is the model class for table "s_d_1".
 *
 * @property integer $id
 * @property string $d1_1
 * @property integer $sd_id
 *
 * @property SD $sD
 */
class D1 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 's_d_1';
    }
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['d1_1'], 'required', 'on'=>'finalize'],
            [['sd_id'], 'integer'],
            [['d1_1'], 'string', 'max' => 64],
            
            [['ref'], 'safe']                 
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'd1_1' => Yii::t('app/crf', 'Name of the vessel'),
            'sd_id' => 'parent section id',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSD()
    {
        return $this->hasOne(SectionD::className(), ['id' => 'sd_id']);
    }

    private $_ref = '';
    public function getRef()
    {
        return $this->_ref;
    }
    public function setRef($url)
    {
        $this->_ref = $url;
    }
    
}
