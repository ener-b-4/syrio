<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\events\drafts\sections\subsections;

use Yii;
use app\models\events\drafts\sections\SectionI;

/**
 * This is the model class for table "s_i_2".
 *
 * @property integer $id
 * @property integer $i2_1
 * @property integer $i2_2
 * @property string $i2_3
 * @property string $i2_4
 * @property integer $si_id
 *
 * @property app\models\events\drafts\sections\SectionI $sI
 */
class I2 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 's_i_2';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['i2_1', 'i2_2', 'si_id'], 'integer', 'min'=>0],  //Author: cavesje Date:20151119
            [['i2_3'], 'string', 'max' => 255],
            [['i2_4'], 'string'],
            [['i2_1', 'i2_2', 'i2_3'], 'required'],           
            [['i2_1', 'i2_2', 'i2_3', 'i2_4'], 'required', 'on'=>'finalize'],
            
            [['ref'], 'safe']                 
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            //(start) Author: vamanbo   Date: 24.08.2015  - on BUG 10: modified error message
            'i2_1' => Yii::t('app/crf', 'Evacuation type'),
            //(end) Author: vamanbo   Date: 24.08.2015  - on BUG 10: modified error message
            'i2_2' => Yii::t('app/crf', 'Number of persons evacuated'),
            'i2_3' => Yii::t('app/crf', 'Means of evacuation'),
            'i2_4' => Yii::t('app/crf', 'Failing system and events description'),
            'si_id' => 'the id of the parent SectionI',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSI()
    {
        return $this->hasOne(SectionI::className(), ['id' => 'si_id']);
    }

    private $_ref = '';
    public function getRef()
    {
        return $this->_ref;
    }
    public function setRef($url)
    {
        $this->_ref = $url;
    }        
}
