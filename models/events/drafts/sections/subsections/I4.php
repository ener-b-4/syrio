<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\events\drafts\sections\subsections;

use Yii;
use app\models\events\drafts\sections\SectionI;

/**
 * This is the model class for table "s_i_4".
 *
 * @property integer $id
 * @property string $i4_1
 * @property integer $si_id
 *
 * @property app\models\events\drafts\sections\SectionI $sI
 */
class I4 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 's_i_4';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['i4_1'], 'string'],
            [['i4_1'], 'required', 'on'=>'finalize'],
            [['si_id'], 'integer'],

            [['ref'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'i4_1' => Yii::t('app', 'Initial lessons learned and preliminary recommendations'),
            'si_id' => 'parent section id',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSI()
    {
        return $this->hasOne(SectionI::className(), ['id' => 'si_id']);
    }

    private $_ref = '';
    public function getRef()
    {
        return $this->_ref;
    }
    public function setRef($url)
    {
        $this->_ref = $url;
    }        
}
