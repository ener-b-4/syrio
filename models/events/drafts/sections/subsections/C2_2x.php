<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\events\drafts\sections\subsections;

use Yii;
use app\models\events\drafts\sections\subsections\C2;

/**
 * This is the model class for table "s_c_2_2".
 *
 * @property integer $id
 * @property integer $c22_1
 * @property string $c22_desc
 * @property integer $sc_2_id
 *
 * @property SC2 $sc2
 */
class C2_2 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 's_c_2_2';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['c22_1', 'sc_2_id'], 'integer', 'min'=>0],   //Author: cavesje Date:20151119
            [['c22_desc'], 'string'],
            [['sc_2_id'], 'required'],
            [['c22_1'], 'safe'],
            [['c22_desc'], 'required', 'when' => function($model){
                return $model->c22_1 > 0;
            }, 'enableClientValidation' => false, 'message' => Yii::t('app', 'You must outline the observed environmental impacts.') 
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'c22_1' => Yii::t('app', 'C22 1'),
            'c22_desc' => Yii::t('app', 'C22 Desc'),
            'sc_2_id' => 'Sc 2 ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSC2()
    {
        return $this->hasOne(SC2::className(), ['id' => 'sc_2_id']);
    }
    
}
