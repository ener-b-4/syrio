<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\events\drafts\sections\subsections;

use Yii;
use app\models\events\drafts\sections\SectionA;

/**
 * This is the model class for table "s_a_4".
 *
 * @property integer $id
 * @property string $a4_1
 * @property integer $is_lesson_learn
 * @property integer $sa_id
 *
 * @property SA $sa
 */
class A4 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 's_a_4';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['a4_1'], 'string'],
            [['a4_1'], 'required', 'on'=>'finalize'],
            [['is_lesson_learn', 'sa_id'], 'integer', 'min'=>0],  //Author: cavesje Date:20151119
            [['ref'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'a4_1' => Yii::t('app', 'Initial lessons learned and preliminary recommendations'),
            'is_lesson_learn' => 'if True this is classified as lesson learned; recommendation otherwise',
            'sa_id' => 'the id of the parent SectionA',
        ];
    }

    private $_ref = '';
    public function getRef()
    {
        return $this->_ref;
    }
    public function setRef($url)
    {
        $this->_ref = $url;
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSA()
    {
        return $this->hasOne(SectionA::className(), ['id' => 'sa_id']);
    }
}
