<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\events\drafts\sections\subsections;

use Yii;
use app\models\events\drafts\sections\SectionD;

/**
 * This is the model class for table "s_d_4".
 *
 * @property integer $id
 * @property string $d4_1
 * @property integer $is_lesson_learn
 * @property integer $sd_id
 *
 * @property SD $sD
 */
class D4 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 's_d_4';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['d4_1'], 'string'],
            [['d4_1'], 'required'],
            [['d4_1'], 'required', 'on'=>'finalize'],
            [['is_lesson_learn', 'sd_id'], 'integer', 'min'=>0],  //Author: cavesje Date:20151119
            
            [['ref'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'd4_1' => Yii::t('app/crf', 'Initial lessons learned and preliminary recommendations'),
            'is_lesson_learn' => 'if True this is classified as lesson learned; recommendation otherwise',
            'sd_id' => 'parent section id',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSD()
    {
        return $this->hasOne(SectionD::className(), ['id' => 'sd_id']);
    }
    
    private $_ref = '';
    public function getRef()
    {
        return $this->_ref;
    }
    public function setRef($url)
    {
        $this->_ref = $url;
    }
}
