<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\events\drafts\sections;

use Yii;
use app\models\events\drafts\sections\subsections\I1;
use app\models\events\drafts\sections\subsections\I2;
use app\models\events\drafts\sections\subsections\I3;
use app\models\events\drafts\sections\subsections\I4;
use app\models\events\drafts\EventDraft;

/**
 * This is the model class for table "s_i".
 *
 * @property integer $id
 * @property integer $event_classification_id
 * @property integer $status
 * @property integer $locked_by
 * @property integer $locked_at
 *
 * @property EventDraft $eventDraft
 * @property I1 $i1
 * @property I2 $i2
 * @property I3 $i3
 * @property I4 $i4
 */
class SectionI extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 's_i';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['event_classification_id'], 'required'],
            [['event_classification_id', 'status', 'locked_by', 'locked_at'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'event_classification_id' => 'Event Classification ID',
            'status' => Yii::t('app', 'Status'),
            'locked_by' => Yii::t('app', 'Locked By'),
            'locked_at' => Yii::t('app', 'Locked At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventDraft()
    {
        return $this->hasOne(EventDraft::className(), ['id' => 'event_classification_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getI1()
    {
        return $this->hasOne(I1::className(), ['si_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getI2()
    {
        return $this->hasOne(I2::className(), ['si_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getI3()
    {
        return $this->hasOne(I3::className(), ['si_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getI4()
    {
        return $this->hasOne(I4::className(), ['si_id' => 'id']);
    }
}
