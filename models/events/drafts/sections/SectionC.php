<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\events\drafts\sections;

use Yii;
use app\models\events\drafts\sections\subsections\C1;
use app\models\events\drafts\sections\subsections\C2;
use app\models\events\drafts\sections\subsections\C3;
use app\models\events\drafts\sections\subsections\C4;
use app\models\events\drafts\EventDraft;

/**
 * This is the model class for table "s_c".
 *
 * @property integer $id
 * @property integer $event_classification_id
 * @property integer $locked_by
 * @property integer $locked_at
 *
 * @property EventDraft $eventDraft
 * @property C1 $c1
 * @property C2 $c2
 * @property C3 $c3
 * @property C4 $c4
 * @property integer status
 */
class SectionC extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 's_c';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['event_classification_id'], 'required'],
            [['event_classification_id', 'locked_by', 'locked_at'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'event_classification_id' => 'Event Classification ID',
            'locked_by' => Yii::t('app', 'Locked By'),
            'locked_at' => Yii::t('app', 'Locked At'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventDraft()
    {
        return $this->hasOne(EventDraft::className(), ['id' => 'event_classification_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getC1()
    {
        return $this->hasOne(C1::className(), ['sc_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getC2()
    {
        return $this->hasOne(C2::className(), ['sc_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getC3()
    {
        return $this->hasOne(C3::className(), ['sc_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getC4()
    {
        return $this->hasOne(C4::className(), ['sc_id' => 'id']);
    }
}
