<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\events\drafts\sections;

use Yii;
use app\models\events\drafts\sections\subsections\J1;
use app\models\events\drafts\sections\subsections\J2;
use app\models\events\drafts\sections\subsections\J3;
use app\models\events\drafts\sections\subsections\J4;
use app\models\events\drafts\EventDraft;


/**
 * This is the model class for table "s_j".
 *
 * @property integer $id
 * @property integer $event_classification_id
 * @property integer $status
 * @property integer $locked_by
 * @property integer $locked_at
 *
 * @property EventDraft $eventDraft
 * @property J1 $j1
 * @property J2 $j2
 * @property J3 $j3
 * @property J4 $j4
 */
class SectionJ extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 's_j';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['event_classification_id'], 'required'],
            [['event_classification_id', 'status', 'locked_by', 'locked_at'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'event_classification_id' => 'Event Classification ID',
            'status' => Yii::t('app', 'Status'),
            'locked_by' => Yii::t('app', 'Locked By'),
            'locked_at' => Yii::t('app', 'Locked At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventDraft()
    {
        return $this->hasOne(EventDraft::className(), ['id' => 'event_classification_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJ1()
    {
        return $this->hasOne(J1::className(), ['sj_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJ2()
    {
        return $this->hasOne(J2::className(), ['sj_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJ3()
    {
        return $this->hasOne(J3::className(), ['sj_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJ4()
    {
        return $this->hasOne(J4::className(), ['sj_id' => 'id']);
    }
}
