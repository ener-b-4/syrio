<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\events\drafts\sections;

use Yii;
use app\models\events\drafts\EventDraft;
use app\models\events\drafts\sections\subsections\A1;
use app\models\events\drafts\sections\subsections\A2;
use app\models\events\drafts\sections\subsections\A3;
use app\models\events\drafts\sections\subsections\A4;

/**
 * This is the model class for table "s_a".
 *
 * @property integer $id
 * @property integer $event_classification_id
 * @property integer $status
 * 
 * @property EventDraft $eventDraft
 * @property \app\models\events\drafts\sections\subsections\A1 $a1
 * @property \app\models\events\drafts\sections\subsections\A2 $a2
 * @property \app\models\events\drafts\sections\subsections\A3 $a3
 * @property \app\models\events\drafts\sections\subsections\A4 $a4
 */
class SectionA extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 's_a';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['event_classification_id'], 'required'],
            [['event_classification_id'], 'integer'],
            [['a1', 'a2'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'event_classification_id' => 'Event Classification ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventDraft()
    {
        return $this->hasOne(EventDraft::className(), ['id' => 'event_classification_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getA1()
    {
        return $this->hasOne(A1::className(), ['sa_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getA2()
    {
        return $this->hasOne(A2::className(), ['sa_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getA3()
    {
        return $this->hasOne(A3::className(), ['sa_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getA4()
    {
        return $this->hasOne(A4::className(), ['sa_id' => 'id']);
    }
    
}
