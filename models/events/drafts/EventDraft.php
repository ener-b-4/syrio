<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\events\drafts;

use Yii;
use app\models\events\EventDeclaration;
use \app\models\events\drafts\sections\SectionA;
use \app\models\events\drafts\sections\SectionB;
use \app\models\events\drafts\sections\SectionC;
use \app\models\events\drafts\sections\SectionD;
use \app\models\events\drafts\sections\SectionE;
use \app\models\events\drafts\sections\SectionF;

use \app\models\events\drafts\sections\SectionI;
use \app\models\events\drafts\sections\SectionJ;

use app\models\common\ActionMessage;

/**
 * This is the model class for table "event_classifications".
 *
 * @property integer $id
 * @property integer $event_id
 * @property integer $is_a
 * @property integer $is_b
 * @property integer $is_c
 * @property integer $is_d
 * @property integer $is_e
 * @property integer $is_f
 * @property integer $is_g
 * @property integer $is_h
 * @property integer $is_i
 * @property integer $is_j
 * @property string $created_at
 * @property integer $created_by
 * @property string $modified_at
 * @property integer $modified_by
 * @property integer $status
 * @property string $session_desc
 * @property \app\models\events\drafts\sections\SectionA $sectionA
 * @property \app\models\events\drafts\sections\SectionB $sectionB
 * @property \app\models\events\drafts\sections\SectionC $sectionC
 * @property \app\models\events\drafts\sections\SectionD $sectionD
 * @property \app\models\events\drafts\sections\SectionE $sectionE
 * @property \app\models\events\drafts\sections\SectionF $sectionF
 * @property \app\models\events\drafts\sections\SectionI $sectionI
 * @property \app\models\events\drafts\sections\SectionJ $sectionJ
 * @property EventDeclaration $event
 * @property string $ca_rejection_desc
 * @property integer $ca_status
 * @property SA[] $sAs
 */
class EventDraft extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'event_classifications';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['event_id', 'created_by', 'session_desc'], 'required'],
			[['is_a', 'is_b', 'is_c', 'is_d', 'is_e', 'is_f', 'is_g', 'is_h', 'is_i', 'is_j', 'created_by', 'modified_by', 'status'], 'integer', 'min'=>0],  //Author: cavesje Date:20151119
			[['created_at', 'modified_at', 'ca_rejection_desc', 'ca_status'], 'safe'],
			[['session_desc'], 'string', 'max' => 45]
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'event_id' => 'Event ID',
			'is_a' => 'Is A',
			'is_b' => 'Is B',
			'is_c' => 'Is C',
			'is_d' => 'Is D',
			'is_e' => 'Is E',
			'is_f' => 'Is F',
			'is_g' => 'Is G',
			'is_h' => 'Is H',
			'is_i' => 'Is I',
			'is_j' => 'Is J',
			'created_at' => ucwords(Yii::t('app', 'created at')),
			'created_by' => ucwords(Yii::t('app', 'created by')),
			'modified_at' => ucwords(Yii::t('app', 'modified at')),
			'modified_by' => ucwords(Yii::t('app', 'modified by')),
			'status' => Yii::t('app', 'Status'),
			'session_desc' => Yii::t('app', 'Description'),
			'ca_rejection_desc' => Yii::t('app', 'Reason for rejection'),
			'ca_status' => Yii::t('app', 'Submission status'),
		];
	}

        
        public function beforeSave($insert) {
            $n_sections = 0;
            
            foreach (range('a', 'j') as $letter) {
                $attr = "is_".$letter;
                if (isset($this->$attr) && $this->$attr == 1) {
                    $n_sections += 1;
                }
                
                if (!$insert) {
                    if (key_exists('session_desc', $this->dirtyAttributes)) {
                        $this->modified_at = time();
                        $this->modified_by = \Yii::$app->user->id;
                    }
                    elseif (key_exists($attr, $this->dirtyAttributes) && !$insert) {
                        $this->modified_at = time();
                        $this->modified_by = \Yii::$app->user->id;
                    }
                }
            }
            
            if (($this->is_g || $this->is_h) && $n_sections==1 && $this->status < EventDeclaration::INCIDENT_REPORT_FINALIZED_PARTIALLY) {
                $this->status = EventDeclaration::INCIDENT_REPORT_FINALIZED;
            } elseif (($this->is_g && $this->is_h) && $n_sections==2 && $this->status < EventDeclaration::INCIDENT_REPORT_FINALIZED_PARTIALLY) {
                $this->status = EventDeclaration::INCIDENT_REPORT_FINALIZED;
            } elseif (($this->is_g || $this->is_h) && $this->status < EventDeclaration::INCIDENT_REPORT_FINALIZED_PARTIALLY) {
                $this->status = EventDeclaration::INCIDENT_REPORT_FINALIZED_PARTIALLY;
            }
            return parent::beforeSave($insert);
        }
        
	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getEvent()
	{
		return $this->hasOne(EventDeclaration::className(), ['id' => 'event_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getSectionA()
	{
		return $this->hasOne(SectionA::className(), ['event_classification_id' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getSectionB()
	{
		return $this->hasOne(SectionB::className(), ['event_classification_id' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getSectionC()
	{
		return $this->hasOne(SectionC::className(), ['event_classification_id' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getSectionD()
	{
		return $this->hasOne(SectionD::className(), ['event_classification_id' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getSectionE()
	{
		return $this->hasOne(SectionE::className(), ['event_classification_id' => 'id']);
	}
	
	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getSectionF()
	{
		return $this->hasOne(SectionF::className(), ['event_classification_id' => 'id']);
	}
	
	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getSectionG()
	{
		return true;
	}
	
	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getSectionH()
	{
		return true;
	}

	
	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getSectionI()
	{
		return $this->hasOne(SectionI::className(), ['event_classification_id' => 'id']);
	}

	 /**
	 * @return \yii\db\ActiveQuery
	 */
	public function getSectionJ()
	{
		return $this->hasOne(SectionJ::className(), ['event_classification_id' => 'id']);
	}
 
	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getCreatedBy()
	{
		return $this->hasOne(\app\models\User::className(), ['id' => 'created_by']);
	}
	
	/**
	 * @return \app\models\User
	 */
	public function getModifiedBy()
	{
		return $this->hasOne(\app\models\User::className(), ['id' => 'modified_by']);
	}
		
	public function RefreshFinalizeStatus()
	{
		$finalized=  EventDeclaration::INCIDENT_REPORT_FINALIZED;
		
		$letters = ['a', 'b', 'c', 'd', 'e', 'f', 'i', 'j'];	//no G and H
		
		$n_sections = 0;
		$n_finalized = 0;
		foreach ($letters as $letter)
		{
			$is_attr = 'is_' . strtolower($letter);
			$section_attr = 'section' . strtoupper($letter);
			
			if ($this->$is_attr)
			{
				$n_sections += 1;   //increment number of sections
				$n_finalized = $this->$section_attr->status>=EventDeclaration::INCIDENT_REPORT_FINALIZED ? $n_finalized+1 : $n_finalized;	//increment the number of finalized section ONLY IF the section status is more than finalized
			}
		}
		
		/*
		 * handle the special cases (G, H)
		 */
		if ($this->is_g) {
			$n_finalized+=1;
			$n_sections+=1;
		}
		if ($this->is_h) {
			$n_finalized+=1;
			$n_sections+=1;
		}
		
		/*
		 * check the finalize status of the draft based on its sections status
		 * $n_finalized = 0			 => not finalized
		 * $n_finalized < $n_sections   => partially finalized
		 * $n_finalized = $n_sections   => finalized
		 */
		if ($n_finalized == 0) {
			$this->status = EventDeclaration::INCIDENT_REPORT_DRAFT;
			$this->save();
		}
		else if ($n_finalized < $n_sections) {
			$this->status = EventDeclaration::INCIDENT_REPORT_FINALIZED_PARTIALLY;
			$this->save();
		}
		else {
			$this->status = EventDeclaration::INCIDENT_REPORT_FINALIZED;
			$this->save();
		}
		
	}
				
	
	
	
	/**
	 * Unlocks (forced) the draft sections
	 * 
	 * @return boolean
	 */
	public function ForceUnlockSections()
	{
		$transaction = \Yii::$app->db->beginTransaction();
		$actionMessage = new ActionMessage();
		
		$letters = ['a', 'b', 'c', 'd', 'e', 'f', 'i', 'j'];
		try
		{
			foreach ($letters as $letter)
			{
				$cap_letter=strtoupper($letter);
				$is_attr = 'is_'.$letter;
				$section_attr = 'section'.$cap_letter;
				if ($this->$is_attr && $this->$section_attr->status==EventDeclaration::INCIDENT_REPORT_LOCKED) {
					$this->$section_attr->status=EventDeclaration::INCIDENT_REPORT_DRAFT;
					if (!$this->$section_attr->save())
					{
						$transaction->rollBack();
						
						$actionMessage->SetError(Yii::t('app', 'Force unlock sections'), 
								Yii::t('app', 'Unable to {evt}. Please retry later.',['evt'=>'unlock sections']));
						Yii::$app->session->set('finished_action_result', $actionMessage);
						return false;
					}
				}
			}
			$transaction->commit();

			$actionMessage->SetSuccess(Yii::t('app', 'Force unlock sections'), 
					Yii::t('app', 'Sections of draft \'{evt}\' have been unlocked. You may proceed to edit.', ['evt'=>$this->session_desc]));
			Yii::$app->session->set('finished_action_result', $actionMessage);
			return true;
			
		} catch (Exception $ex) {
			$actionMessage->SetError(Yii::t('app', 'Force unlock sections'), 
					Yii::t('app', 'Unable to {evt}. Please retry later.', ['evt'=>\Yii::t('app', 'unlock sections')]));
			Yii::$app->session->set('finished_action_result', $actionMessage);

			$transaction->rollBack();
			
			return false;
		}
	}
	
	/**
	 * @return [string] The event types as string array
	 */
	public function getEventTypes()
	{
		$retArray = [];
		$this->is_a ? $retArray[]="A" : '';
		$this->is_b ? $retArray[]="B" : '';
		$this->is_c ? $retArray[]="C" : '';
		$this->is_d ? $retArray[]="D" : '';
		$this->is_e ? $retArray[]="E" : '';
		$this->is_f ? $retArray[]="F" : '';
		$this->is_g ? $retArray[]="G" : '';
		$this->is_h ? $retArray[]="H" : '';
		$this->is_i ? $retArray[]="I" : '';
		$this->is_j ? $retArray[]="J" : '';

		return $retArray;
	}
	
	//(start) Author: vamanbo   Date: 28/08/2015	On: limit returned records function of current user's type
	/**
	 * Overrides the ActiveRecord find 
	 */
	public static function find() {
		$query = parent::find();
		
		/*
		 * If the user is:
		 * 
		 * sys_admin => return all;
		 * 
		 * operator_user => return the drafts 
		 */
		
		if (Yii::$app->user->can('sys_admin'))
		{
			return $query;
		}
		elseif (Yii::$app->user->identity->isOperatorUser)
		{
			$query = $query->join('inner join', 'event_declaration', 'event_classifications.event_id = event_declaration.id');
			$query = $query->andFilterWhere(['event_declaration.operator_id' => Yii::$app->user->identity->org_id]);
		}
		elseif (Yii::$app->user->identity->isInstallationUser)
		{
			$query = $query->join('inner join', 'event_declaration', 'event_classifications.event_id = event_declaration.id');
			$query = $query->join('inner join', 'installations', 'installations.id = event_declaration.installation_id');
			$query = $query->andFilterWhere(['event_declaration.operator_id' => Yii::$app->user->identity->org_id]);
			$query = $query->andFilterWhere(['event_declaration.installation_id' => Yii::$app->user->identity->inst_id]);
		}
                elseif (Yii::$app->user->identity->isCaUser)
                {
                    //where(['{{event_classifications}}.[[ca_status]]' => \app\models\events\EventDeclaration::INCIDENT_REPORT_ACCEPTED])                }
			$query = $query->join('inner join', 'event_declaration', 'event_classifications.event_id = event_declaration.id');
			//$query = $query->andWhere(['{{event_classifications}}.[[ca_status]]' => \app\models\events\EventDeclaration::INCIDENT_REPORT_ACCEPTED]);
                }
		return $query;
	}

	
	
	public static function getDeleted($evtID)
	{
		if (Yii::$app->user->isGuest)
		{
			return Yii::$app->error->ThrowError(Errors::RESTRICTED_AREA_ERROR);
		}
		
		$query = EventDraft::find()
				->where(['{{event_classifications}}.[[del_status]]' => EventDeclaration::STATUS_DELETED])
				->andWhere(['event_id' => $evtID]);
		
		// (start) Author: vamanbo  On: events-declaration/view - return different results depending on the user type
		// (end) Author: vamanbo  On: events-declaration/view - return different results depending on the user type
		if (Yii::$app->user->identity->isInstallationUser)
		{
			$query->andWhere(['event_declaration.installation_id' => Yii::$app->user->identity->inst_id]);
		}
		elseif (Yii::$app->user->identity->isOperatorUser)
		{
			$query->andWhere(['event_declaration.operator_id' => Yii::$app->user->identity->org_id]);
		}
		else
		{
			return Yii::$app->error->ThrowError(Errors::RESTRICTED_OPERATION_ERROR);
		}
		
		return $query;
	}
	
	public static function getNumberOfDeleted($evtID)
	{
		if (Yii::$app->user->isGuest)
		{
			return Yii::$app->error->ThrowError(Errors::RESTRICTED_AREA_ERROR);
		}
		
		$query = self::getDeleted($evtID);
		return $query->count('{{event_classifications}}.[[id]]');
	}
	
        public function getStatus()
        {
            if ($this->status<=EventDeclaration::INCIDENT_REPORT_FINALIZED_PARTIALLY) {
                foreach (range('a','j') as $letter) {
                    $attr = 'is_'.$letter;
                    if ($this->$attr && !($letter == 'g' || $letter == 'h')) {
                        $section = 'section'.strtoupper($letter);
                        if ($this->$section->status == EventDeclaration::INCIDENT_REPORT_LOCKED) {
                            return EventDeclaration::INCIDENT_REPORT_LOCKED;
                        }
                    }
                }
            }
        }
	
}
