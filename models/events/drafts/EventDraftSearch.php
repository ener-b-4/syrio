<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\events\drafts;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\events\drafts\EventDraft;

/**
 * EventDraftSearch represents the model behind the search form about `app\models\rebuilt\EventDraft`.
 */
class EventDraftSearch extends EventDraft
{
    public $session_desc;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'event_id', 'is_a', 'is_b', 'is_c', 'is_d', 'is_e', 'is_f', 'is_g', 'is_h', 'is_i', 'is_j', 'created_by', 'modified_by'], 'integer', 'min'=>0],  //Author: cavesje Date:20151119
            [['created_at', 'modified_at', 'session_desc', 'del_status'], 'safe'],
            //2017dev
            [['session_desc'], 'string', 'max'=>64]
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        //die();
        //(start) Author: vamanbo   Date: 20/08/2008    on: check search parameter type
        if (!(is_numeric($params['EventDraftSearch']['event_id'])))
        {
            throw new \yii\web\HttpException(400, Yii::t('app', 'Bad parameter'));
        }
        
        //(end) Author: vamanbo   Date: 20/08/2008    on: check search parameter type
        
        $query = EventDraft::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

/*        if (!$this->validate())
        {
            echo '<pre>';
            echo $this->errors;
            echo '</pre>';
            
        }
*/        
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }
        
        $query = $query->andFilterWhere([
            'id' => $this->id,
            'event_id' => $this->event_id,
            'is_a' => $this->is_a,
            'is_b' => $this->is_b,
            'is_c' => $this->is_c,
            'is_d' => $this->is_d,
            'is_e' => $this->is_e,
            'is_f' => $this->is_f,
            'is_g' => $this->is_g,
            'is_h' => $this->is_h,
            'is_i' => $this->is_i,
            'is_j' => $this->is_j,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'modified_at' => $this->modified_at,
            'modified_by' => $this->modified_by,
            'status' => $this->status,
        ]);

        
        $query = $query->andFilterWhere(['like', 'session_desc', $this->session_desc]);

        if (isset($this->del_status))
        {
            $query = $query->andFilterWhere(['{{event_classifications}}.[[del_status]]'=>$this->del_status]);
        }
                
        return $dataProvider;
    }
}
