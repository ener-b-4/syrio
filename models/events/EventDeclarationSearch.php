<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\events;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\events\EventDeclaration;

/* @property string event_name_ext */


/**
 * EventDeclarationSearch represents the model behind the search form about `app\models\events\EventDeclaration`.
 */
class EventDeclarationSearch extends EventDeclaration
{
    
    public $event_name_ext;
    public $reporting_dates;
    public $installation;
    
    public $event_types;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'raporteur_id', 'modified_by_id', 'status'], 'integer'],
            //[['event_date_time'], 'date', 'format'=>'Y-m-d'],
            [['event_date_time'], 'match', 'pattern' => '#(\d{4}|\*{1})\-{1}(\d{2}|\*){1}\-{1}(\d{2}|\*{1})#'],
            [['event_date_time'], 'validate_date'],
            [[
                'event_name_ext',
            ], 'string', 'length' => [0,255]],
            [[ 
                'operator', 
                'field_name', 
                'raporteur_name', 
                'raporteur_role', 
                'contact_tel', 
                'contact_email', 
                'created', 
                'updated', 
                'event_name',
                /* search model specific properties */
                'reporting_dates',
                'event_name_ext',
                'del_status',
                'installation'], 'safe'],
            
            ['reporting_dates', 'in', 'range' => [0, 1, 2, 3, 4, 5, 6, 7, 8]],
            [['event_types'], 'match', 'pattern' => '#^[a-jA-J]+(?:[\,\a-jA-J]{0,9})*$#'],
            [['installation'], 'match', 'pattern' => '#^[-\w\s\*]+(?:\/[-\w\s\*]*){0,2}$#'],
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }
    
    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        /*
         * must return the events registered on a particular Company
         * i.e.
         * 
         * 
         */
        
        $_event_classifications_joined = false;
        
        
        //echo '<pre>';
        //echo var_dump($params);
        //echo '</pre>';
        //die();
        
        $query = EventDeclaration::find();
        
        // (start) Author: vamanbo  On: events-declaration/view - return different results depending on the user type
        $query->join('left join', 'installations', 'event_declaration.installation_id = installations.id');
        //$query->where(['installations.operator_id' => Yii::$app->user->identity->organization->id]);
        // (end) Author: vamanbo  On: events-declaration/view - return different results depending on the user type
        if (Yii::$app->user->identity->isInstallationUser)
        {
            $query->andWhere(['event_declaration.installation_id' => Yii::$app->user->identity->inst_id]);
        }
        elseif (Yii::$app->user->identity->isOperatorUser)
        {
            $query->andWhere(['event_declaration.operator_id' => Yii::$app->user->identity->org_id]);
        }
        
        //$query->where(['operator' => Yii::$app->user->getIdentity()->organization]);
        //$query->orderBy([
        //    'event_date_time' => SORT_ASC, 'event_name' => SORT_ASC
        //    ]);
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'event_name_ext' => [
                    'asc' => ['event_name'=>SORT_ASC],
                    'desc' => ['event_name'=>SORT_DESC],
                ],
                'installation' => [
                    'asc' => ['installations.name'=>SORT_ASC],
                    'desc' => ['installations.name'=>SORT_DESC],
                ],
                'event_date_time',
                'field_name'
            ]
        ]);
        
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }
        
        
        /* deal with event_date_times */
        
        
        
        $query->andFilterWhere([
//            'id' => $this->id,
//            'raporteur_id' => $this->raporteur_id,
//            'created' => $this->created,
//            'updated' => $this->updated,
//            'modified_by_id' => $this->modified_by_id,
            'status' => $this->status,
        ]);

        // 'event_date_time' => $this->event_date_time,
        
        $query->andFilterWhere(['like', 'operator', $this->operator])
            ->andFilterWhere(['like', 'field_name', $this->field_name])
            //->andFilterWhere(['like', 'raporteur_name', $this->raporteur_name])
            //->andFilterWhere(['like', 'raporteur_role', $this->raporteur_role])
            //->andFilterWhere(['like', 'contact_tel', $this->contact_tel])
            //->andFilterWhere(['like', 'contact_email', $this->contact_email])
            ->andFilterWhere(['like', 'event_name', $this->event_name_ext]);
        
        
        if (isset($this->event_date_time))
        {
            /*
            $query->andFilterWhere(['and',
                'event_date_time BETWEEN "' . $this->event_date_time . '" AND "' . $this->event_date_time . ' 23:59:59"'
                ]);
             * 
             */
            
            /* '#(\d{4}|\*{1})\-{1}(\d{2}|\*){1}\-{1}(\d{2}|\*{1})#' */
            
            /* case Y-m-d */
            if (preg_match_all('#(\d{4})\-{1}(\d{2}){1}\-{1}(\d{2})#', $this->event_date_time))
            {
                /* 
                 * returns all the events in a given date 
                 * year month day set
                 */
                $query->andFilterWhere(['and',
                    'event_date_time BETWEEN "' . $this->event_date_time . '" AND "' . $this->event_date_time . ' 23:59:59"'
                    ]);
            }
            else if (preg_match_all('#(\d{4})\-{1}(\d{2}){1}\-{1}(\*{1})#', $this->event_date_time))
            {
                /* 
                 * returns all the events in a given year and month (day irrespective) 
                 * year month set
                 */
                
                $parts = explode('-', $this->event_date_time);
                
                $query->andFilterWhere(['and',
                    'year(event_date_time) = ' . $parts[0],    
                    'month(event_date_time) = ' . $parts[1]
                    ]);
            }
            elseif ((preg_match_all('#(\d{4})\-{1}(\*{1})\-{1}(\*{1})#', $this->event_date_time)))
            {
                /* 
                 * returns all the events in a given year (month and day irrespective) 
                 * year set
                 */
                
                $parts = explode('-', $this->event_date_time);
                
                $query->andFilterWhere(['and',
                    'year(event_date_time) = ' . $parts[0],    
                    ]);
            }
            elseif ((preg_match_all('#(\*{1})\-{1}(\d{2}){1}\-{1}(\*{1})#', $this->event_date_time)))
            {
                /* 
                 * returns all the events in a given month (year and day irrespective) 
                 * year set
                 */
                
                $parts = explode('-', $this->event_date_time);
                
                $query->andFilterWhere(['and',
                    'month(event_date_time) = ' . $parts[1],    
                    ]);
            }
            elseif ((preg_match_all('#(\*{1})\-{1}(\*){1}\-{1}(\d{2})#', $this->event_date_time)))
            {
                /* 
                 * returns all the events in a given day (year and month irrespective) 
                 * year set
                 */
                
                $parts = explode('-', $this->event_date_time);
                
                $query->andFilterWhere(['and',
                    'day(event_date_time) = ' . $parts[2],    
                    ]);
            }
            elseif ((preg_match_all('#(\*{1})\-{1}(\d{2}){1}\-{1}(\d{2})#', $this->event_date_time)))
            {
                /* 
                 * returns all the events in a month on a particular day (year irrespective) 
                 * month day set
                 */
                $parts = explode('-', $this->event_date_time);
                
                $query->andFilterWhere(['and',
                    'month(event_date_time) = ' . $parts[1],
                    'day(event_date_time) = ' . $parts[2]    
                    ]);
            }
            elseif ((preg_match('#(\*{1})\-{1}(\*{1})\-{1}(\*{1})#', $this->event_date_time)))
            {
                //echo 'select all';
                //die();
            }
            
        }

        
        if (isset($this->reporting_dates)) {
            if (is_numeric($this->reporting_dates))
            {
                switch ($this->reporting_dates)
                {
                    case self::TIME_STATUS_IN_TIME:
                        $query->andWhere(['and', 
                            'coalesce(event_declaration.status, 0) <> ' . self::INCIDENT_REPORT_REPORTED,
                            'TIMESTAMPDIFF(HOUR, CURDATE(), event_declaration.event_deadline) > 48']);
                        break;
                    case self::TIME_STATUS_LATE:
                        $query->andWhere(['and', 
                            'coalesce(event_declaration.status, 0) <> ' . self::INCIDENT_REPORT_REPORTED,
                            'TIMESTAMPDIFF(HOUR, CURDATE(), event_declaration.event_deadline) <= 48', 
                            'TIMESTAMPDIFF(HOUR, CURDATE(), event_declaration.event_deadline) >= 0']);
                        break;
                    case self::TIME_STATUS_OVERDUE:
                        $query->andWhere(['and', 
                            'coalesce(event_declaration.status, 0) <> ' . self::INCIDENT_REPORT_REPORTED,
                            'TIMESTAMPDIFF(HOUR, CURDATE(), event_declaration.event_deadline) < 0']);
                        break;
                    case self::TIME_STATUS_SUBMITTED:
                        $query->andWhere(['and', 
                            'event_declaration.status = ' . self::INCIDENT_REPORT_REPORTED
                            ]);
                        break;
                    case self::TIME_STATUS_SUBMITTED_OK:
                        if (!$_event_classifications_joined) {
                            $query->join('join', 'event_classifications', '{{event_classifications}}.[[event_id]] = {{event_declaration}}.[[id]]');
                            $_event_classifications_joined = true;
                        }
                        $query->andWhere(['or',
                                ['{{event_classifications}}.[[ca_status]]' => EventDeclaration::INCIDENT_REPORT_REPORTED],
                                ['{{event_classifications}}.[[ca_status]]' => EventDeclaration::INCIDENT_REPORT_REJECTED],
                                ['{{event_classifications}}.[[ca_status]]' => EventDeclaration::INCIDENT_REPORT_ACCEPTED],
                                ['{{event_classifications}}.[[ca_status]]' => EventDeclaration::INCIDENT_REPORT_RESUBMITTED],                            ]
                            )                            
                            ->andWhere(['and',
                            'unix_timestamp(event_declaration.event_deadline) - event_classifications.modified_at > 0'
                            ]);
                        break;
                    case self::TIME_STATUS_SUBMITTED_LATE:
                        if (!$_event_classifications_joined) {
                            $query->join('join', 'event_classifications', '{{event_classifications}}.[[event_id]] = {{event_declaration}}.[[id]]');
                            $_event_classifications_joined = true;
                        }
                        $query->andWhere(['or',
                                ['{{event_classifications}}.[[ca_status]]' => EventDeclaration::INCIDENT_REPORT_REPORTED],
                                ['{{event_classifications}}.[[ca_status]]' => EventDeclaration::INCIDENT_REPORT_REJECTED],
                                ['{{event_classifications}}.[[ca_status]]' => EventDeclaration::INCIDENT_REPORT_ACCEPTED],
                                ['{{event_classifications}}.[[ca_status]]' => EventDeclaration::INCIDENT_REPORT_RESUBMITTED],                            ]
                            )                            
                            ->andWhere(['and',
                            'unix_timestamp(event_declaration.event_deadline) - event_classifications.modified_at < 0'
                            ]);
                        break;
                    case self::INCIDENT_REPORT_ACCEPTED:
                        if (!$_event_classifications_joined) {
                            $query->join('join', 'event_classifications', '{{event_classifications}}.[[event_id]] = {{event_declaration}}.[[id]]');
                            $_event_classifications_joined = true;
                        }
                        $query->andWhere(['{{event_classifications}}.[[ca_status]]' => EventDeclaration::INCIDENT_REPORT_ACCEPTED]);
                        break;
                    case self::INCIDENT_REPORT_REJECTED:
                        if (!$_event_classifications_joined) {
                            $query->join('join', 'event_classifications', '{{event_classifications}}.[[event_id]] = {{event_declaration}}.[[id]]');
                            $_event_classifications_joined = true;
                        }
                        $query->andWhere(['{{event_classifications}}.[[ca_status]]' => EventDeclaration::INCIDENT_REPORT_REJECTED]);
                        break;
                    case 8:
                        if (!$_event_classifications_joined) {
                            $query->join('join', 'event_classifications', '{{event_classifications}}.[[event_id]] = {{event_declaration}}.[[id]]');
                            $_event_classifications_joined = true;
                        }
                        $query->andWhere(['or',
                                ['{{event_classifications}}.[[ca_status]]' => EventDeclaration::INCIDENT_REPORT_REPORTED],
                                ['{{event_classifications}}.[[ca_status]]' => EventDeclaration::INCIDENT_REPORT_RESUBMITTED],
                                ]);
                        break;
                }
            }
        }

        if (isset($this->event_types) && trim($this->event_types)!=='') {
            //filter the ones reported
            if (!$_event_classifications_joined) {
                $query->join('join', 'event_classifications', '{{event_classifications}}.[[event_id]] = {{event_declaration}}.[[id]]');
                $_event_classifications_joined = true;
            }
            
            $query->andWhere(['or',
                    ['{{event_classifications}}.[[ca_status]]' => EventDeclaration::INCIDENT_REPORT_REPORTED],
                    ['{{event_classifications}}.[[ca_status]]' => EventDeclaration::INCIDENT_REPORT_REJECTED],
                    ['{{event_classifications}}.[[ca_status]]' => EventDeclaration::INCIDENT_REPORT_ACCEPTED],
                    ['{{event_classifications}}.[[ca_status]]' => EventDeclaration::INCIDENT_REPORT_RESUBMITTED],
                    ]);
            
            $this->parse_event_type($query, $this->event_types);
        }

        
        if (isset($this->del_status))
        {
            $query->andFilterWhere(['{{event_declaration}}.[[del_status]]'=>$this->del_status]);
        }

        if (trim($this->installation) != '')
        {
            $split = explode('/', $this->installation);
            $inst_name = '';
            $inst_type = ''; 
            $inst_field = '';
            
            for ($i=0; $i<min([count($split), 3]); $i++) {
                if ($i===0) {
                    $inst_name = trim($split[$i]) !== '*' ? trim($split[$i]) : '';
                } elseif ($i===1) {
                    $inst_type = trim($split[$i]) !== '*' ? trim($split[$i]) : '';
                } else {
                    $inst_field = trim($split[$i]) !== '*' ? trim($split[$i]) : '';
                }
            }

            $query->andFilterWhere(['and',
                ['like', 'installations.name', $inst_name],
                ['like', 'installations.type', $inst_type],
                ['like', 'field_name', $inst_field]
                ]);
            
//            $query->andFilterWhere(['or',
//                'installations.name = \'' . $this->installation . '\'',
//                'installations.type = \'' . $this->installation . '\'',
//                ['like', 'field_name', $this->installation]
//                ]);
        }
        
        return $dataProvider;
    }
    
    
    
    public function validate_date($attribute, $params) {
        //parent::validate_date($attribute, $params);
        $init_string = $this->$attribute;
        
        $arr = explode('-', $init_string);
        
        $year = trim($arr[0]);
        $month = trim($arr[1]);
        $day = trim($arr[2]);
        
        if (is_numeric($year))
        {
            $int_year = intval($year);
            if ($int_year<1950 || $int_year>intval(date('Y')))
            {
                $this->addError($attribute, Yii::t('app', 'Bad year!'));
                return false;
            }
            
        }
        else
        {
            $int_year = 0;
        }

        if (is_numeric($month))
        {
            $int_month = intval($month);
            if ($int_month<1 || $int_month>12)
            {
                $this->addError($attribute, Yii::t('app', 'Bad month!'));
                return false;
            }
            
        }
        else
        {
            $int_month = 0;
        }

        if (is_numeric($day))
        {
            $int_day = intval($day);
            if ($int_day<1 || $int_day>$this->maxDays[$int_month])
            {
                $this->addError($attribute, Yii::t('app', 'Bad day!'));
                return false;
            }
            
        }
        else
        {
            $int_day = 0;
        }
        
        //check if there's such a day if year, month and day are all set
        if ($int_day*$int_month*$int_year != 0)
        {
            if (!strtotime($int_year . '-' . $int_month . '-' . $int_day))
            {
                $this->addError($attribute, Yii::t('app', 'Bad date!'));
                return false;
            }
        }
        
        return true;
    }
    
    private $maxDays = [
        0 => 31,
        1 => 31,
        2 => 29,
        3 => 31,
        4 => 30,
        5 => 31,
        6 => 30,
        7 => 31,
        8 => 31,
        9 => 30,
        10 => 31,
        11 => 30,
        12 => 31,
    ];

    
    public function parse_event_type($query, $val)
    {        
        if (isset($val))
        {
            //get rid of the _
            $val = str_replace('_', '', $val);
            
            $parts = [];
            $range = range('a', 'j');
            $types = explode(',', $val);
            foreach ($types as $type) {
                $t = strtolower(trim($type));
                if (in_array($t, $range))
                {
                    $parts = \yii\helpers\ArrayHelper::merge($parts,
                            ['{{event_classifications}}.[[is_' . $t . ']]' => 1]);
                }
                else
                {
                    echo $t;
                }
            }
            
            //echo print_r($parts);
            //die();
            
            $qf = ['or'];
            $qf[] = $parts;
            
            $qf2 = ['or', [
                'table.column' => 1
            ]
            ];
                    
            //echo '<pre>';
            //echo print_r($qf);
            //echo '<br/>';
            //echo print_r($qf2);
            //echo '</pre>';
            //die();
            
            $query->andFilterWhere([
                'and',
                  $qf
            ]);
            
        }
        return $query;
    }    
    
    
}
