<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\events;

use Yii;
use app\models\events\drafts\EventDraft;
use app\models\behaviors\TimingBehavior;
use app\components\Errors;
use app\models\User;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "event_declaration".
 *
 * @property integer $id
 * @property string $event_date_time
 * @property string $event_deadline
 * @property string $operator
 * @property string $field_name
 * @property string $raporteur_name
 * @property string $raporteur_role
 * @property string $contact_tel
 * @property string $contact_email
 * @property integer $raporteur_id
 * @property string $created
 * @property string $updated
 * @property integer $modified_by_id
 * @property string $event_name
 * @property integer $status
 * @property integer $del_status
 * @property string $installation_name_type
 * @property integer $reportingTimeStatus the reporting time status code (see consts)
 *
 * @property AuthUsers $raporteur
 */
class EventDeclaration extends \yii\db\ActiveRecord
{
const INCIDENT_REPORT_DRAFT = 0;
const INCIDENT_REPORT_LOCKED = 1;
const INCIDENT_REPORT_FINALIZED_PARTIALLY = 2;
const INCIDENT_REPORT_FINALIZED = 3;
const INCIDENT_REPORT_SIGNED = 4;
const INCIDENT_REPORT_REPORTED = 5;
const INCIDENT_REPORT_REJECTED = 6;
const INCIDENT_REPORT_ACCEPTED = 7;
const INCIDENT_REPORT_RESUBMITTED = 8;

const STATUS_DELETED = 0;
const STATUS_ACTIVE = 10;

const TIME_STATUS_IN_TIME = 0;
const TIME_STATUS_LATE = 1;
const TIME_STATUS_OVERDUE = 2;
const TIME_STATUS_SUBMITTED = 3;

const TIME_STATUS_SUBMITTED_LATE = 4;
const TIME_STATUS_SUBMITTED_OK = 5;



    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'event_declaration';
    }

    
    public function behaviors() {
        //parent::behaviors();
        return [
            'timing' => [
                'class' => TimingBehavior::className(),
                'TargetTimespan' => 10,
                'timeField' => 'event_date_time',
            ],
        ];
    }
    
    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            //check whether new record or update
            if ($insert)
            {
                $this->event_deadline = $this->getDeadline();
            }
            else {
                if ($this->event_date_time != $this->oldAttributes['event_date_time'])
                {
                    $this->event_deadline = $this->getDeadline();
                }
                
                $traced = ['event_name', 'field_name', 'installation_id', 'event_date_time'];
                foreach($traced as $item) {
                    if (key_exists($item, $this->dirtyAttributes)) {
                        $this->modified_by_id = \Yii::$app->user->id;
                    }
                }
            }
            return true; 
        } 
        else 
        { 
            return false; 
        }        
    }
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['event_date_time', 'created', 'updated', 'installation_id', 'event_deadline'], 'safe'],
            //Author : cavesje  Date : 20150616 - Format of datetime is wrong
            //['event_date_time', 'date', 'format' => 'yyyy-MM-dd kk:mm:ss', 'message'=>'Please provide the date in the \'yyyy-MM-dd hh:mm:ss\' format!'],            
            //['event_date_time', 'date', 'format' => 'yyyy-MM-dd hh:mm:ss', 'message'=>'Please provide the date in the \'yyyy-MM-dd hh:mm:ss\' format!'],            
            
            ['event_date_time', 'validate_date'],
            [['event_date_time'], 'required', 'on'=>['step1', 'step2', 'step3', 'submit'], 'message'=>\Yii::t('app/crf', 'Incident date and time must be provided!')],
            [['installation_id'], 'required', 'on'=>['step2', 'step3', 'submit'], 'message'=>\Yii::t('app/crf', 'Any incident must be linked to an installation!')],
            [['raporteur_name', 'contact_email', 'event_name'], 'required', 'on'=>['step3', 'submit']],
            [['raporteur_id', 'modified_by_id', 'status'], 'integer'],
            [['operator', 'field_name', 'raporteur_role', 'contact_email'], 'string', 'max' => 128],
            [['raporteur_name', 'event_name'], 'string', 'max' => 64],
            [['contact_tel'], 'string', 'max' => 32],
            [['event_name'], 'unique', 'on'=>['step3', 'submit']],
            [['operator_id'], 'required', 'on'=>['step3', 'submit']],
            [['del_status'], 'required', 'on'=>['submit']]
        ];
    }

    public function scenarios() {
        //parent::scenarios();
        return \yii\helpers\ArrayHelper::merge(parent::scenarios(), [
            'step1'=>['event_date_time'],
            'step2'=>['event_date_time'],
            'step3',
            'submit'
        ]) ;
    }
    
    
    function validate_date($attribute, $params) {
        //first check if date format is ok
        //$inputDate = new DateTime()
        
    //here your validation
        //$attr = print_r($this->$attribute);
        //echo '<script>alert(' . $attr . ')</script>';
        try
        {
            if (date_parse($this->$attribute))
            {
                $inputDate = new \DateTime($this->$attribute);
                if ($inputDate>new \DateTime())
                {
                    $errText = 'You cannot add a date from future!'; 
                    $this->addError($attribute, $errText);
                }
            }
            else
            {
                $errText = 'Please provide date and time in the yyyy/MM/dd hh:mm format!'; 
                $this->addError($attribute, $errText);
            }
        } catch (Exception $ex) {
            //$this->addError($attribute, date_format($inputDate,'D, d M Y'));  
            Yii::warning('abc');
            $errText = 'Please provide date and time in the yyyy/MM/dd hh:mm format!'; 
            $this->addError($attribute, $errText);
        }
        
    }    
    
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'event_date_time' => Yii::t('app/crf', 'Event date/time'),
            'operator' => Yii::t('app/crf', 'Operator/owner'),
            'installation_name_type' => Yii::t('app/crf', 'Name/type of the installation'),
            'field_name' => Yii::t('app/crf', 'Field name/code'),
            'raporteur_name' => Yii::t('app/crf', 'Name of the reporting person'),
            'raporteur_role' => Yii::t('app/crf', 'Role of the reporting person'),
            'contact_tel' => Yii::t('app/crf', 'Telephone number'),
            'contact_email' => Yii::t('app/crf', 'E-mail address'),
            'raporteur_id' => 'Raporteur ID',
            'created' => ucwords(Yii::t('app', 'created at')),
            'created_at' => ucwords(Yii::t('app', 'created at')),
            'created_by' => ucwords(Yii::t('app', 'created by')),
            'modified_at' => ucwords(Yii::t('app', 'modified at')),
            'modified_by_id' => 'Modified By ID',
            'event_name' => Yii::t('app/crf', 'Event Name'),
            'status' => Yii::t('app', 'Status'),
            'modifiedBy.name' => ucwords(Yii::t('app', 'modified by')),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRaporteur()
    {
        return $this->hasOne(User::className(), ['id' => 'raporteur_id']);
    }
    
    public function getDrafts()
    {
        //$drafts = new EventDraft();
        //return $drafts->find()->where(['event_id'=>$this->id]);
        return $this->hasMany(EventDraft::className(), ['event_id' => 'id']);
    }

    
    /**
     * Returns the user that created the Event declaration
     * @return app\models\User;
     */
    public function getCreatedBy()
    {
        return $this->hasOne(\app\models\User::className(), ['id' => 'created_by']);
    }
    
    
    /**
     * Returns the user that modified last the Event declaration
     * @return app\models\User;
     */
    public function getModifiedBy()
    {
        return $this->hasOne(\app\models\User::className(), ['id' => 'modified_by_id']);
    }
    
    /**
     * Returns the installation of the event
     * 
     * @return \app\models\Installations the installation
     */
    public function getInstallation()
    {
        return $this->hasOne(\app\models\Installations::className(), ['id'=>'installation_id']);
    }
    
    public function getInstallation_name_type()
    {
        if (isset($this->installation))
        {
            return $this->installation->name . ' / ' . $this->installation->type;            
        }
        else
        {
            return '(not set)/(not set)';
        }
    }
    
    /**
     * Returns the Operator/Owner organization
     */
    public function getOrganization()
    {
        return $this->hasOne(\app\modules\management\models\Organization::className(), ['id'=>'operator_id']);
    }
    
    /**
     * read-only property that returns the reporting status NOW() considering
     */
    public function getReportingTimeStatus()
    {
        $now = date_create();
        $deadline = date_create($this->event_deadline);
        
        /* @var $diff \DateInterval */            
        $diff = date_diff($now, $deadline, false);

        if ($this->status == self::INCIDENT_REPORT_REPORTED)
        {
            return self::TIME_STATUS_SUBMITTED;
        }
        if ($diff->invert == 1)
        {
            return self::TIME_STATUS_OVERDUE;
        }
        elseif ($diff->days <= 2)
        {
            return self::TIME_STATUS_LATE;
        }
        else
        {
            return self::TIME_STATUS_IN_TIME;
        }
    }
    
    public function getNumberOfDrafts()
    {
        return EventDraft::find()->where(['[[event_id]]'=>$this->id])->count('[[event_id]]');
    }
    
    public static function getDeleted()
    {
        if (Yii::$app->user->isGuest)
        {
            return Yii::$app->error->ThrowError(Errors::RESTRICTED_AREA_ERROR);
        }
        
        $query = EventDeclaration::find()
                ->where(['del_status' => self::STATUS_DELETED]);
        
        // (start) Author: vamanbo  On: events-declaration/view - return different results depending on the user type
        // (end) Author: vamanbo  On: events-declaration/view - return different results depending on the user type
        if (Yii::$app->user->identity->isInstallationUser)
        {
            $query->andWhere(['event_declaration.installation_id' => Yii::$app->user->identity->inst_id]);
        }
        elseif (Yii::$app->user->identity->isOperatorUser)
        {
            $query->andWhere(['event_declaration.operator_id' => Yii::$app->user->identity->org_id]);
        }
        else
        {
            return Yii::$app->error->ThrowError(Errors::RESTRICTED_OPERATION_ERROR);
        }
        
        return $query;
    }
    
    public static function getNumberOfDeleted()
    {
        if (Yii::$app->user->isGuest)
        {
            return Yii::$app->error->ThrowError(Errors::RESTRICTED_AREA_ERROR);
        }
        
        $query = self::getDeleted();
        return $query->count('[[id]]');
    }
    
    /**
     * Returns the event declaration that corresponds to the one reported to the competent authority
     * 
     * @return \app\models\events\drafts\EventDraft Description
     * 
     * @author Bogdan Vamanu
     * @version: 1.0.20150908
     */
    public function getReport()
    {
        $query = EventDraft::find()
                ->where(['event_id'=>$this->id])
                ->andWhere(['or',
                    '[[event_classifications]].[[ca_status]] = ' . self::INCIDENT_REPORT_REPORTED,
                    '[[event_classifications]].[[ca_status]] = ' . self::INCIDENT_REPORT_REJECTED,
                    '[[event_classifications]].[[ca_status]] = ' . self::INCIDENT_REPORT_ACCEPTED,
                    '[[event_classifications]].[[ca_status]] = ' . self::INCIDENT_REPORT_RESUBMITTED]);
        
        return $query->one();
    }
    
    
    /**
     * Returns the current status in terms of submission tasks
     * 
     * Only if the event is reported
     */
    public function getSubmissionTasks() {
        if ($this->getReport() == NULL) {
            return 'not reported yet';
        }
        
        $modif = date('Y-m-d H:i:s', $this->report->modified_at);
        
        //compute the values related to reporting dates
        $event_date = new \DateTime($this->event_date_time);
        $date_reported = date_timestamp_set(new \DateTime(), $this->report->modified_at);        
        $deadline_date = new \DateTime($this->deadline);
        
        
        $interval = date_diff($date_reported, $deadline_date, false);      //should be positive to be in time
        $deadline_interval = date_diff($deadline_date, $event_date, false);      //should be positive to be in time

        $details = [];
        $details[\Yii::t('app', 'Date of submission')] = date_format($date_reported, 'd M Y H:i:s');
        $details[\Yii::t('app', 'Deadline')] = date_format($deadline_date, 'd M Y H:i:s');

        $details[\Yii::t('app', 'Deadline interval')] = $deadline_interval->format(Yii::t('app', '{evt, plural, =1{# day} other{# days}}', ['evt'=>$deadline_interval->d]));
        //$details['Deadline interval'] = $deadline_interval->format('%a days %h hours %i minutes');
        //$details['deadline_interval'] = $deadline_interval;

        if ($interval->invert === 0) {
            $details[\Yii::t('app', 'Report submitted')] = '<span style="color: green">'. \Yii::t('app', 'in due time') . '</span>';
            $details[\Yii::t('app', 'Timing')] = $interval->format(Yii::t('app', '{evt, plural, =1{# day} other{# days}}', ['evt'=>$interval->d])
                    . ' ' . Yii::t('app', '{evt, plural, =1{# hour} other{# hours}}', ['evt'=>$interval->h])
                    . ' ' . Yii::t('app', '{evt, plural, =1{# minute} other{# minutes}}', ['evt'=>$interval->i])) . ' ' . \Yii::t('app', 'before the deadline');      //should be positive to be in time;
            $details['late_code'] = 0;
        } else {
            $details[\Yii::t('app', 'Report submitted')] = '<span style="color: red">'. \Yii::t('app', 'late') . '</span>';
            $details[\Yii::t('app', 'Timing')] = $interval->format(Yii::t('app', '{evt, plural, =1{# day} other{# days}}', ['evt'=>$interval->d])
                    . ' ' . Yii::t('app', '{evt, plural, =1{# hour} other{# hours}}', ['evt'=>$interval->h])
                    . ' ' . Yii::t('app', '{evt, plural, =1{# minute} other{# minutes}}', ['evt'=>$interval->i])) . ' ' . \Yii::t('app', 'after the deadline');      //should be positive to be in time;
            $details['late_code'] = 1;
        }
        
        return $details;
        
    }
    
    
    //vamanbo 20160422
    
    public function getReportingHistory() {
        return $this->hasMany(\app\models\ReportingHistory::className(), ['evt_id'=>'id']);
    }
    
    
    
}
