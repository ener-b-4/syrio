<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\User;
use app\components\Errors;

/**
 * UserSearch represents the model behind the search form about `app\models\User`.
 */
class UserSearch extends User
{
	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['id', 'status'], 'integer', 'min'=>0],  //Author: cavesje Date:20151119
			[['name', 'email', 'created_by', 'created_at', 'modified_at', 'org_acronym', 'inst_id'], 'safe'],
			[['full_name', 'role_in_organization'], 'safe'],
			['assignments', 'safe'],
			//['full_name', 'trim'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params)
	{
		if (Yii::$app->user->isGuest)
		{
			return Yii::$app->error->ThrowError(Errors::RESTRICTED_AREA_ERROR);
		}
		elseif (!Yii::$app->user->can('user-list'))
		{
			return Yii::$app->error->ThrowError(Errors::RESTRICTED_OPERATION_ERROR);
		}
		elseif (Yii::$app->user->can('user-list'))
		{
			if (Yii::$app->user->identity->isCaUser)
			{
				$query = User::find()
					   ->join('inner join', 'organizations', 'organizations.id = auth_users.org_id')
					   ->join('left join', 'installations', 'installations.id=auth_users.inst_id');
			}
			elseif (Yii::$app->user->identity->isOperatorUser)
			{
				/* only get the users that are in the same Organization as the current one */
				
				$query = User::find()
					->where(['like', 'org_id', 'OO'])
					->join('left join', 'organizations', 'auth_users.org_id = organizations.id')
					->join('left join', 'installations', 'installations.id=auth_users.inst_id')
					->andWhere(['organizations.id' => Yii::$app->user->identity->organization->id]);
					//->andWhere(['installations.operator_id' => Yii::$app->user->identity->organization->id]);
			}
			elseif  (Yii::$app->user->identity->isInstallationUser)
			{
				$query = User::find()
					->where(['like', 'org_id', 'OO'])
					->join('left join', 'organizations', 'auth_users.org_id = organizations.id')
					->join('left join', 'installations', 'installations.id=auth_users.inst_id')
					->andWhere(['organizations.id' => Yii::$app->user->identity->organization->id])
                                        ->andWhere([
                                            'or',
                                            ['{{installations}}.[[id]]'=>\Yii::$app->user->identity->inst_id],
                                            ['{{auth_users}}.[[inst_id]]'=>NULL]
                                        ]);
			}
			else
			{
				//is sys_admin
				$query = User::find()
						->join('left join', 'organizations', 'organizations.id = auth_users.org_id')
						->join('left join', 'installations', 'installations.id=auth_users.inst_id');
			}
		}

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

                $dataProvider->setSort([
                                'attributes' => [
                                        // aici intra coloanele in care vrei sa activezi sortarea
                                        // ‘nume_proprietate_1’…
                                    'username',
                                    'full_name' => [
                                        'asc' => ['first_names' => SORT_ASC, 'last_name' => SORT_ASC],
                                        'desc' => ['first_names' => SORT_DESC, 'last_name' => SORT_DESC],
                                        //'label' => 'Full Name',
                                        'default' => SORT_ASC
                                    ],
                                    'org_acronym' => [
                                        'asc' => ['organizations.organization_acronym' => SORT_ASC],
                                        'desc' => ['organizations.organization_acronym' => SORT_DESC],
                                        'default' => SORT_ASC
                                    ],
                                    'role_in_organization',
                            ]
                    ]);
                
                
                
		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		/* modif bv 19/07/2015 */
		/* modif bv 27/07/2015 
		 * include the installation name (if any) */
		$query->andFilterWhere(['like', 'organizations.organization_acronym', $this->org_acronym])
				->orFilterWhere(['like', 'installations.name', $this->org_acronym]);
		 
		$query->andFilterWhere([
			'or',
			['like', 'email', $this->email],
			['like', 'first_names', trim($this->_full_name)],
			['like', 'last_name', trim($this->_full_name)],
			['like', 'CONCAT_WS(" ", first_names, last_name)', trim($this->_full_name)],
		]);
		
                $query->andFilterWhere(['like', 'role_in_organization', $this->role_in_organization]);
                
		$possibleRolesArray = [];
		foreach(Yii::$app->user->identity->getPossibleRoles() as $r)
		{
			$possibleRolesArray[] = $r->name;
		}
		
		$query->join('inner join', 'auth_assignment', 'auth_assignment.user_id = auth_users.id')
				->andFilterWhere(['item_name'=>$this->_assignments]);
		
		return $dataProvider;
	}
	
	
	/*
	 * OVERRIDE SOME SEARCH PROPERTIES
	 */
	
	private $_full_name;
	public function setFull_Name($value)
	{
		$this->_full_name = $value;
	}
	public function getFull_Name()
	{
		return $this->_full_name;
	}

	
	private $_assignments;
	public function setAssignments($value)
	{
		$this->_assignments = $value;
	}
	public function getAssignments()
	{
		return $this->_assignments;
	}
	
	/*
	 * SyRIO - extends the search model
	 * by: bv
	 * created: 19 July 2014
	 */
	public $org_acronym;
	
	/*
	 * SyRIO - extends the search model for global search
	 * by: bv
	 * created: 13 July 2014
	 */
	
	private $_global;
	public function GetGlobal()
	{
		return $this->_global;
	}
	public function SetGlobal($value)
	{
		$this->_global = $value;
	}
	
}
