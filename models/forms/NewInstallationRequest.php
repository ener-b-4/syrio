<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\forms;

use Yii;
//use app\models\InstallationTypes;

/**
 * This is the model class for the form for New Installation Request form.
 *
 * @property string $operator
 * @property string $name
 * @property string $type
 * @property string $type_other
 * @property string $code
 * @property string $flag
 * @property integer $year_of_construction
 * @property integer $number_of_beds
 * @property string $op_status
 
 * @property InstallationTypes $typeObject
 * 
 * 
 * @property array[key, value] operationalStatuses (READONLY) the possible operational statuses of an installation
 */
class NewInstallationRequest extends \yii\base\Model
{
    const OP_STATUS_INOPERATIVE = 100;
    const OP_STATUS_DECOMMISSIONED = 101;
    const OP_STATUS_OPERATIVE = 110;

    
    public $operator;
    public $name;
    public $type;
    public $type_other;
    public $code;
    public $flag;
    public $year_of_construction;
    public $number_of_beds;
    public $op_status;
    
    public $contact_phone;
    public $contact_email;
    public $contact_fax;
    
    public $organization;
    public $role_in_organization;
    public $first_name;
    public $second_name;
    public $phone;
    public $email;
    public $email_repeat;  
    
    public $captcha;
    
    private static $skip_on_report = ['captcha'];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['operator', 'name', 'type', 'code', 'op_status'], 'required'],
            [['op_status'], 'string', 'max'=>64],
            [['year_of_construction'], 'integer', 'min'=>1950, 'max'=>9999],
            [['number_of_beds'], 'integer', 'min'=>0, 'max'=>150],  //should match the XML limit
            [['operator'], 'string', 'max' => 128],
            [['name'], 'string', 'max' => 64],
            [['type'], 'string', 'max' => 8],
            [['type_other'], 'string', 'max' => 45],
            [['flag'], 'string', 'length' => [2, 64]],
            [['code'], 'string', 'max' => 32],
            ['flag', 'required', 'when'=>function($model) {
                return $model->type == 'MODU';
            }, 'whenClient' => "function (attribute, value) {
                return $('#newinstallationrequest-type').val() == 'MODU';
            }", 
                    'message' =>'Registration flag is required for MODUs'],

            [['contact_phone', 'contact_fax'], 'string', 'max' => 16],
            [['contact_email'], 'string', 'max' => 45],
            [['contact_phone', 'contact_email'], 'required'],
                    
                    
            [['first_name'], 'string', 'length'=>[2,45]],
            [['email', 'email_repeat', 'contact_email'], 'email'],
            [['email_repeat'], 'compare', 'compareAttribute'=>'email', 'operator'=>'=='],

            [['phone'], 'string', 'length'=>[3,16]],
            
            [['organization', 'role_in_organization', 'first_name', 'second_name', 'phone', 'email'], 'required'],     
                    
            [['captcha'], 'captcha'],        
        ];
    }

    
    public static function staticAttributeLabels() {
        return [
            'operator' => Yii::t('app', 'Operator/owner'),
            'name' => Yii::t('app', 'Name of installation'),
            'type' => Yii::t('app/cpf', 'Type of installation'),
            'type_other' => Yii::t('app', 'Type (if other)'),
            'code' => ucfirst(Yii::t('app', 'code')),
            'flag' => Yii::t('app', 'Flag (if MODU)'),
            'year_of_construction' => Yii::t('app/cpf', 'Year of construction'),
            'number_of_beds' => Yii::t('app/cpf', 'Number of beds'),
            'op_status' => Yii::t('app', 'Operational status'),

            'contact_phone' => \Yii::t('app', 'Contact phone (installation)'),
            'contact_email' => \Yii::t('app', 'Contact email (installation)'),
            'contact_fax' => \Yii::t('app', 'Contact fax (installation)'),
            
            
            'organization' => \Yii::t('app', 'Organization'),
            'role_in_organization' => \Yii::t('app', 'Role in organization'),
            'first_name' => \Yii::t('app', 'First name'),
            'second_name' => \Yii::t('app', 'Last name'),
            'phone' => \Yii::t('app', 'Phone'),
            'email' => \Yii::t('app', 'Email'),
            'email_repeat' => \Yii::t('app', 'Repeat email'),                        
            
            'captcha' => \Yii::t('app', 'Verification code')
            ];
    }    
    
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'operator' => Yii::t('app', 'Operator/owner'),
            'name' => Yii::t('app', 'Name of installation'),
            'type' => Yii::t('app/cpf', 'Type of installation'),
            'type_other' => Yii::t('app', 'Type (if other)'),
            'code' => ucfirst(Yii::t('app', 'code')),
            'flag' => Yii::t('app', 'Flag (if MODU)'),
            'year_of_construction' => Yii::t('app/cpf', 'Year of construction'),
            'number_of_beds' => Yii::t('app/cpf', 'Number of beds'),
            'op_status' => Yii::t('app', 'Operational status'),

            'contact_phone' => \Yii::t('app', 'Contact phone (installation)'),
            'contact_email' => \Yii::t('app', 'Contact email (installation)'),
            'contact_fax' => \Yii::t('app', 'Contact fax (installation)'),
            
            'organization' => \Yii::t('app', 'Organization'),
            'role_in_organization' => \Yii::t('app', 'Role in organization'),
            'first_name' => \Yii::t('app', 'First name'),
            'second_name' => \Yii::t('app', 'Last name'),
            'phone' => \Yii::t('app', 'Phone'),
            'email' => \Yii::t('app', 'Email'),
            'email_repeat' => \Yii::t('app', 'Repeat email'),                        
            
            'captcha' => \Yii::t('app', 'Verification code')
            
            ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTypeObject()
    {
        return $this->hasOne(InstallationTypes::className(), ['type' => 'type']);
    }
    

    /**
     * Gets the possible statuses of an installation.
     * May be used directly in a dropdownlist.
     * 
     * @return array[key value] The possible operational statuses of an installation
     * @author Bogdan Vamanu <bogdan.vamanu@jrc.ec.europa.eu>
     */
    public function getOperationalStatuses()
    {
        return [
            'Inoperative' => Yii::t('app', 'Inoperative'),
            'Decommissioned' => Yii::t('app', 'Decommissioned'),
            'Operative' => Yii::t('app', 'Operative')
        ];
    }
    
    public static function SkipOnReport($key) {
        return in_array($key, self::$skip_on_report);
    }       
}
