<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\forms;

use Yii;
use yii\base\Model;

/**
 * form-model for changing the username
 *
 * @author vamanbo
 */
class ChangeUsername extends Model {

    public $new_username;
    public $captcha;

    
    /**
     *  @inheritdoc
     */
    public function rules() {
        return [
            [['new_username'], 'string', 'length' => [6, 64]],
            [['new_username'], 'unique', 'targetClass'=>'\app\models\User', 'targetAttribute' => 'username'],
            [['new_username', 'captcha'], 'required'],
            ['captcha', 'captcha'],
        ];
    }
    
    public function attributeLabels() {
        return [
            'new_username' => \Yii::t('app', 'New username'),
            'captcha' => \Yii::t('app', 'Verification code'),
        ];
    }
    
}
