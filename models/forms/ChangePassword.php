<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\forms;

use Yii;
use yii\base\Model;

/**
 * form-model for changing the password
 *
 * @author vamanbo
 */
class ChangePassword extends Model {

    public $old_password;
    public $new_password;
    public $password_repeat;
    //public $captcha;

    
    /**
     *  @inheritdoc
     */
    public function rules() {
        return [
            [['old_password', 'new_password', 'password_repeat'], 'string', 'min'=>6, 'max'=>255],
            [['old_password', 'new_password', 'password_repeat'], 'required'],
            [['old_password'], 'validateCurrentPassword'],
            ['password_repeat', 'compare', 'compareAttribute'=>'new_password'],
            //['captcha', 'captcha'],
            //[['old_pass_coded', 'new_pass_coded'], 'safe']
        ];
    }
    
    
    public function attributeLabels() {
        return [
            'old_password' => \Yii::t('app', 'Old password'),
            'new_password' => \Yii::t('app', 'New password'),
            'password_repeat' => \Yii::t('app', 'Repeat password'),
        ];
    }
    
    public function validateCurrentPassword()
    {
        /* @var $user \app\models\User */
        
        $user = Yii::$app->user->identity;
        
        if (!$user->validatePassword($this->old_password))
        {
            $this->addError('old_password', Yii::t('app', 'Value does not match your current password'));
        }
    }
}
