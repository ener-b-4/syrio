<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\forms;

use Yii;
use yii\base\Model;

/**
 * form-model for renewing the password
 *
 * @author vamanbo
 */
class NewPassword extends Model {

    public $user_id;
    public $token;
    public $new_password;
    public $password_repeat;
    public $captcha;

    
    /**
     *  @inheritdoc
     */
    public function rules() {
        return [
            [['new_password', 'password_repeat', 'token'], 'string', 'min'=>6, 'max'=>255],
            [['new_password', 'password_repeat', 'captcha'], 'required'],
            ['password_repeat', 'compare', 'compareAttribute'=>'new_password'],
            ['captcha', 'captcha'],
            //[['old_pass_coded', 'new_pass_coded'], 'safe']
        ];
    }
    
}
