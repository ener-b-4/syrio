<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\forms;

use Yii;
use yii\base\Model;

/**
 * form-model for reseting the password
 *
 * @author vamanbo
 */
class PasswordReset extends Model {

    public $username;
    public $email;
    public $captcha;
    
    /**
     *  @inheritdoc
     */
    public function rules() {
        return [
            [['username', 'email', 'captcha'], 'required'],
            [['username'], 'string', 'length' => [6, 64]],
            [['email'], 'email'],
            ['captcha', 'captcha'],
        ];
    }
    
}
