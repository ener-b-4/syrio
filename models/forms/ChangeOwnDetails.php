<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\forms;

use Yii;
use yii\base\Model;

/**
 * form-model for changing the username
 *
 * @author vamanbo
 */
class ChangeOwnDetails extends Model {

    public $phone;
    public $email;
    public $email_repeat;
    public $captcha;
    public $first_name;
    public $last_name;
    public $role;
    
    /**
     *  @inheritdoc
     */
    public function rules() {
        return [
            [['phone'], 'string', 'length' => [3, 16]],
            [['first_name', 'last_name'], 'string', 'length' => [3, 64]],

            [['phone'], 'string', 'length' => [3, 128]],

            [['email', 'email_repeat'], 'email'],
            [['email_repeat'], 'compare', 'compareAttribute'=>'email', 'operator'=>'=='],            
            
            [['first_name', 'last_name', 'phone', 'email', 'email_repeat', 'role', 'captcha'], 'required', 'on' => 'default'],
            [['first_name', 'last_name', 'email', 'email_repeat', 'role', 'captcha'], 'required', 'on'=>'default_admin'],
            ['captcha', 'captcha'],
        ];
    }
    
}
