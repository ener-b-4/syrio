<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\forms;

use yii\base\Model;

/*
 * Registration form for new operator
 */

/**
 * Description of UserRegistrationOperators
 *
 * @author vamanbo
 */
class UserRegistrationOperators extends Model {
    
    public $organization;
    public $role_in_organization;
    
    public $first_name;
    public $second_name;
    public $phone;
    public $email;
    public $email_repeat;
    public $roles;
    
    public $captcha;
    
    private static $skip_on_report = ['captcha', 'email_repeat'];
    
    /**
     *  @inheritdoc
     */
    public function rules() {
        return [
            [['organization', 'role_in_organization', 'first_name', 'second_name', 'phone', 'email', 'email_repeat', 'captcha'], 'required'],
            [['organization', 'role_in_organization', 'first_name', 'phone', 'email', 'email_repeat', 'captcha'], 'filter', 'filter'=>'trim', 'skipOnArray'=>true],
            [['organization', 'second_name', 'role_in_organization', 'email', 'email_repeat'], 'string', 'length'=>[2,128]],
            
            [['first_name'], 'string', 'length'=>[2,45]],

            [['email', 'email_repeat'], 'email'],
            [['email_repeat'], 'compare', 'compareAttribute'=>'email', 'operator'=>'=='],
            
            [['phone'], 'string', 'length'=>[3,45]],
            
            [['roles'], 'required', 'message'=>\Yii::t('app', 'At least one role must be selected.')],
            [['roles'], 'in', 'range'=>['op_admin', 'op_raporteur', 'op_user'], 'allowArray'=>true, 'message'=>\Yii::t('app', 'A valid role must be selected.')],
            
            ['captcha', 'captcha'],
        ];
    }

    
    public function AttributeLabels() {
        return [
            'organization' => \Yii::t('app', 'Organization'),
            'role_in_organization' => \Yii::t('app', 'Role in organization'),
            'first_name' => \Yii::t('app', 'First name'),
            'second_name' => \Yii::t('app', 'Last name'),
            'phone' => \Yii::t('app', 'Phone'),
            'email' => \Yii::t('app', 'Email'),
            'email_repeat' => \Yii::t('app', 'Repeat email'),
            'roles' => \Yii::t('app', 'Roles'),
            'captcha' => \Yii::t('app', 'Verification code'),
            
        ];
    }
    
    
    public static function staticAttributeLabels() {
        return [
            'organization' => \Yii::t('app', 'Organization'),
            'role_in_organization' => \Yii::t('app', 'Role in organization'),
            'first_name' => \Yii::t('app', 'First name'),
            'second_name' => \Yii::t('app', 'Last name'),
            'phone' => \Yii::t('app', 'Phone'),
            'email' => \Yii::t('app', 'Email'),
            'email_repeat' => \Yii::t('app', 'Repeat email'),
            'roles' => \Yii::t('app', 'Roles'),
            'captcha' => \Yii::t('app', 'Verification code'),
            
        ];
    }
    
    public static function SkipOnReport($key) {
        return in_array($key, self::$skip_on_report);
    }    
}
