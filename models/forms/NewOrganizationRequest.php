<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\forms;

use Yii;

/**
 * This is the model class for new organization request form.
 *
 * @property string $organization_name
 * @property string $organization_acronym
 * @property string $street1
 * @property string $street2
 * @property string $city
 * @property string $county
 * @property string $country
 * @property string $zip  
 * @property string $organization_phone
 * @property string $organization_email
 * @property string $organization_fax
 * @property string $organization_phone2
 * @property string $organization_web
 *
 */
class NewOrganizationRequest extends \yii\base\Model
{

    private static $skip_on_report = ['captcha'];
    
    public $organization_name;
    public $organization_acronym;
    public $street1;
    public $street2;
    public $city;
    public $county;
    public $country;
    public $zip;
    public $organization_phone;
    public $organization_email;
    public $organization_fax;
    public $organization_phone2;
    public $organization_web;
    public $captcha;

    public $organization;
    public $role_in_organization;
    public $first_name;
    public $second_name;
    public $phone;
    public $email;
    public $email_repeat;    
    
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['organization_name', 'organization_acronym', 'organization_phone', 'organization_email',
                'street1', 'city', 'country', 'zip', 'captcha'
                ], 'required'],
            [['organization_name', 'organization_web', 'street1', 'street2'], 'string', 'max' => 128],
            [['organization_acronym'], 'string', 'max' => 8],
            [['organization_phone', 'organization_fax', 'organization_phone2'], 'string', 'max' => 16],
            [['organization_email'], 'string', 'max' => 45],
            [['city', 'county'], 'string', 'max' => 45],
            [['country'], 'string', 'length' => [2,64]],
            [['zip'], 'string', 'max' => 16],
            [['captcha'], 'captcha'],
            [['organization_email'], 'email'],
            
            [['organization', 'role_in_organization', 'first_name', 'phone', 'email', 'email_repeat', 'captcha'], 'filter', 'filter'=>'trim', 'skipOnArray'=>true],
            [['organization', 'second_name', 'role_in_organization', 'email', 'email_repeat'], 'string', 'length'=>[2,128]],
            
            [['first_name'], 'string', 'length'=>[2,45]],
            [['email', 'email_repeat'], 'email'],
            [['email_repeat'], 'compare', 'compareAttribute'=>'email', 'operator'=>'=='],

            [['phone'], 'string', 'length'=>[3,16]],
            
            [['organization', 'role_in_organization', 'first_name', 'second_name', 'phone', 'email'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'organization_name' => Yii::t('app', 'Organization Name'),
            'organization_acronym' => Yii::t('app', 'Organization Acronym'),
            'organization_phone' => Yii::t('app', 'Organization Phone'),
            'organization_email' => Yii::t('app', 'Organization Email'),
            'organization_fax' => Yii::t('app', 'Organization Fax'),
            'organization_phone2' => Yii::t('app', 'Organization Phone2'),
            'organization_web' => Yii::t('app', 'Organization Web'),
            'street1' => Yii::t('app', 'Street'),
            'street2' => Yii::t('app', 'Street (line 2)'),
            'city' => Yii::t('app', 'City'),
            'county' => Yii::t('app', 'County'),
            'country' => Yii::t('app', 'country'),
            'zip' => Yii::t('app', 'Zip'),
            'captcha' => Yii::t('app', 'Verification code'),
            
            'organization' => \Yii::t('app', 'Organization'),
            'role_in_organization' => \Yii::t('app', 'Role in organization'),
            'first_name' => \Yii::t('app', 'First name'),
            'second_name' => \Yii::t('app', 'Last name'),
            'phone' => \Yii::t('app', 'Phone'),
            'email' => \Yii::t('app', 'Email'),
            'email_repeat' => \Yii::t('app', 'Repeat email'),            
            ];
    }

    public static function staticAttributeLabels() {
        return [
            'organization_name' => Yii::t('app', 'Organization Name'),
            'organization_acronym' => Yii::t('app', 'Organization Acronym'),
            'organization_phone' => Yii::t('app', 'Organization Phone'),
            'organization_email' => Yii::t('app', 'Organization Email'),
            'organization_fax' => Yii::t('app', 'Organization Fax'),
            'organization_phone2' => Yii::t('app', 'Organization Phone2'),
            'organization_web' => Yii::t('app', 'Organization Web'),
            'street1' => Yii::t('app', 'Street1'),
            'street2' => Yii::t('app', 'Street2'),
            'city' => Yii::t('app', 'City'),
            'county' => Yii::t('app', 'County'),
            'country' => Yii::t('app', 'Country'),
            'zip' => Yii::t('app', 'Zip'),
            'captcha' => Yii::t('app', 'Verification code'),
            
            'organization' => \Yii::t('app', 'Organization'),
            'role_in_organization' => \Yii::t('app', 'Role in organization'),
            'first_name' => \Yii::t('app', 'First name'),
            'second_name' => \Yii::t('app', 'Last name'),
            'phone' => \Yii::t('app', 'Phone'),
            'email' => \Yii::t('app', 'Email'),
            'email_repeat' => \Yii::t('app', 'Repeat email'),            
            
            ];
    }
    
    public static function SkipOnReport($key) {
        return in_array($key, self::$skip_on_report);
    }        

}
