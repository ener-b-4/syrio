<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\behaviors;

use yii\base\Behavior;

use app\models\organization\OrganizationStatistics;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TimingBehavior
 *
 * @author bogdanV
 */
class CaUserBehavior extends Behavior
{
    public function __construct() {
        parent::__construct();
    }
 
    public $_errors = [];
    
    /**
     * returns the statistics of the company events
     * 
     * return mixedArray
     */
    public function getCaIncidents()
    {
        if (!\Yii::$app->user->isGuest)
        {
                //$c = \app\models\ca\IncidentCategorization::find()->all();
                //return $c;
            $user = \Yii::$app->user;
            if ($user->can('ca_user'))
            {
                //$reported = \app\models\events\EventDeclaration::INCIDENT_REPORT_REPORTED;
                //Book::find()->where('id != :id and type != :type', ['id'=>1, 'type'=>1])->all();
                //$c = \app\models\events\EventDeclaration::find()->where('status = :reported', ['reported'=>$reported])->all();
                
                $c = \app\models\ca\IncidentCategorization::find()->where('is_done = :is_done',['is_done'=>0])->all();
                
                return $c;
                //return $stats;
            }
            else
            {
                $this->_errors[] = 'You cannot access this';
            }
        }
    }
    
    public $major;
    
    
    public function getCaIncidentsInYear($year)
    {
        if (!\Yii::$app->user->isGuest)
        {
//                $c = \app\models\ca\IncidentCategorization::find()->all();
//                return $c;
            $user = \Yii::$app->user;
            if ($user->can('ca_user') || $user->can('ca_assessor'))
            {
                
                $reported = \app\models\events\EventDeclaration::INCIDENT_REPORT_REPORTED;
                //Book::find()->where('id != :id and type != :type', ['id'=>1, 'type'=>1])->all();
                //$c = \app\models\events\EventDeclaration::find()->where('status = :reported', ['reported'=>$reported])->all();
                
                //FROM `ca_incident_cat` WHERE `op_submitted_at` like '2015%'
                $c = \app\models\ca\IncidentCategorization::findBySql('SELECT * FROM ca_incident_cat WHERE `op_submitted_at` like \'' . $year . '-%\' and is_done=1')->all();
                $count=0;
                foreach ($c as $incident)
                {
                    $incident->is_major ? $count++ : '';
                }
                $this->major = $count;
                return $c;                
                //$c = \app\models\ca\IncidentCategorization::find()->where('is_done = :is_done',['is_done'=>0])->all();

                //return $stats;
            }
            else
            {
                $this->_errors[] = 'You cannot access this';
            }
        }
    }    
    
    public function getErrors()
    {
        return $this->_errors;
    }
    
    /**
     * The method computes the time left for submitting the report
     * @return DateInterval
     */
    public function TimeLeft()
    {
        //$date1 = new \DateTime($this->owner->event_date_time);
        
        $prop = $this->timeField;
        $date1 = new \DateTime($this->owner->$prop);
        $date1->add(new \DateInterval('P' . $this->TargetTimespan . 'D'));
        //return print_r($date1);
        
        $date2 = new \DateTime(date('Y-m-d H:i:s'));
        
        //return print_r($interval);
        //$retS = '<br/>deadline: ' . print_r($date1);
        //$retS .= '<br/>current date: ' . print_r($date2);

        if (isset($retArr)) unset($retArr);
        $retArr = [];
        
        
        $interval = $date1->diff($date2);
        
        $date1 > $date2 ? $retArr['overdue'] = 0 : $retArr['overdue'] = 1;
        $retArr['deadline'] = $date1;
        $retArr['years'] = $interval->y;
        $retArr['months'] = $interval->m;
        $retArr['days'] = $interval->d;
        $retArr['hours'] = $interval->h;
        $retArr['minutes'] = $interval->i;
        $retArr['interval'] = $interval;
        
        //var_dump($date1);
        
        //$retS = '<br/>' . $msg . $interval->d . ' days, ' . $interval->h . ' hours and ' . $interval->i . ' minutes';
        return $retArr ;
    }
    
    private function dateDiff()
    {
$date1 = new DateTime("2007-03-24");
$date2 = new DateTime("2009-06-26");
$interval = $date1->diff($date2);
echo "difference " . $interval->y . " years, " . $interval->m." months, ".$interval->d." days "; 

// shows the total amount of days (not divided into years, months and days like above)
echo "difference " . $interval->days . " days ";        
        
    }
    
    public function getCanBePurged() {
        $q1 = \app\models\ca\IncidentCategorization::find()
                ->where(['a_assessed_by' => $this->owner->id])
                ->orWhere(['b_assessed_by' => $this->owner->id])
                ->orWhere(['c_assessed_by' => $this->owner->id])
                ->orWhere(['d_assessed_by' => $this->owner->id])
                ->orWhere(['e_assessed_by' => $this->owner->id])
                ->orWhere(['f_assessed_by' => $this->owner->id])
                ->orWhere(['g_assessed_by' => $this->owner->id])
                ->orWhere(['h_assessed_by' => $this->owner->id])
                ->orWhere(['i_assessed_by' => $this->owner->id])
                ->orWhere(['j_assessed_by' => $this->owner->id])
                ->count();

        $q2 = \app\modules\cpfbuilder\models\CpfSessions::find()
                ->where(['user_id' => $this->owner->id]);

        $q3 = \app\modules\cpfbuilder\models\CpfSection1::find()
                ->where(['user_id' => $this->owner->id]);
        
        $q4 = \app\modules\cpfbuilder\models\CpfSection2::find()
                ->where(['user_id' => $this->owner->id]);
        
        $q5 = \app\modules\cpfbuilder\models\CpfSection3::find()
                ->where(['user_id' => $this->owner->id]);
        
        $q6 = \app\modules\cpfbuilder\models\CpfSection4::find()
                ->where(['user_id' => $this->owner->id]);
        
        return ($q1+$q2+$q3+$q4+$q5+$q6)==0;
    }
    
}
