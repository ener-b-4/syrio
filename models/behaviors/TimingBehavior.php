<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\behaviors;

use yii\base\Behavior;
use app\models\Holidays;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TimingBehavior
 *
 * @author bogdanV
 */
class TimingBehavior extends Behavior
{
    
    public $TargetTimespan;
    
    public $timeField;
    
    
    public function getDeadline()
    {
        $prop = $this->timeField;
        $date = date_create($this->owner->$prop);
        $hols = Holidays::find()
                ->where(['repeat'=>'1'])
                ->orWhere(['year'=>  date_format($date, 'Y')])
                ->asArray()->all();
        
        $holidays = [];
        foreach ($hols as $holiday) {
            $key = date_format($date, 'Y') . '-'
                . str_pad($holiday['month'], 2, '0', STR_PAD_LEFT) . '-'
                . str_pad($holiday['day'], 2, '0', STR_PAD_LEFT);
            
            $holidays[$key] = $holiday['name'];
        }
        return $this->firstBusinessDay($this->TargetTimespan, $date, $holidays);
    }
    
    private function firstBusinessDay($numberOfBusinessDays, $fromDate, $holidays)
    {
        //$date = date_create('2015-09-01');
        //date_timestamp_set($date, time());
        //return date('N', date_timestamp_get($date));
        //return $this->isWeekend($date);
        
        $startTime = date_timestamp_get($fromDate);
        $endTime = $startTime + 86400 * $numberOfBusinessDays;  //final deadline no weekends considered;
        
        $time = $startTime;

        while($time<$endTime)
        {
            if ($this->isWeekend($time)) {
                $endTime += 86400;
//                $date = date_create();
//                date_timestamp_set($date, $time);
//                $date_string = date_format($date, 'Y-m-d');
//                echo 'weekend: ' . $date_string . '<br/>';
            }
            else
            {
                //check if not a holiday
                //putting this here ensures that a holiday that is in weekend is NOT double counted
                $date = date_create();
                date_timestamp_set($date, $time);
                $date_string = date_format($date, 'Y-m-d');
                if (key_exists($date_string, $holidays))
                {
                    $endTime += 86400;
//                    echo 'holiday: ' . $date_string . '<br/>';
                }
            }
            $time+=86400;
        }

        //check if the endtime is not a holiday
        $date = date_create();
        date_timestamp_set($date, $endTime);
        $date_string = date_format($date, 'Y-m-d');
        if (key_exists($date_string, $holidays))
        {
            $endTime += 86400;
//            echo 'holiday: ' . $date_string . '<br/>';
        }
        
        return date('Y-m-d H:i:s', $endTime);
    }
    
    
    /**
     * This function returns true if the given $date in Sunday or Saturday
     * @param timestamp $date
     */
    private function isWeekend($date)
    {
        //$dayOfWeek = date('N', date_timestamp_get($date));
        $dayOfWeek = date('N', $date);
        return ($dayOfWeek == '6' || $dayOfWeek == '7');
    }
    
    /**
     * This property gives the time left for submission in minutes, as INTEGER
     * 
     * @return DateInterval the difference between the event and current date 
     */
    public function getTimeLeft()
    {
        
        $currentDate = \DateTime::createFromFormat('Y-m-d H:i:s', date('Y-m-d H:i:s', time()));
        $timeToSubmission = \DateTime::createFromFormat('Y-m-d H:i:s', $this->getDeadline());

        return date_diff($timeToSubmission, $currentDate, false);
    }

}
    
