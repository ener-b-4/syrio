<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\behaviors;

use yii\base\Behavior;

use app\models\organization\OrganizationStatistics;
use app\components\Errors;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TimingBehavior
 *
 * @author bogdanV
 */
class InstallationUserBehavior extends Behavior
{
    public function __construct() {
        parent::__construct();
    }
 
    public $_errors = [];
    
    /**
     * returns the statistics of the company events
     * 
     * return mixedArray
     */
    public function getInstallationIncidents()
    {
        
        if (!\Yii::$app->user->isGuest)
        {
            $user = \Yii::$app->user;
            if ($user->identity->isInstallationUser)
            {
                $op = $this->owner->organization;
                $inst = $this->owner->installation;
                $c = \app\models\events\EventDeclaration::find()
                        ->where(['operator_id' => $op->id])
                        ->andWhere(['installation_id' => $inst->id])->all();
                //$c = EventDeclaration::find()->where('operator == :op',['op'=>$this->_user->organization]])->all()->count();
                
                //get current year
                $dt = new \DateTime();
                $current_year = date_format($dt, 'Y');
                
                if (isset($stats)) unset($stats);
                $stats=[];
                $stats['total'] = count($c);
                $stats['total_cy'] = 0;
                $stats['pending'] = [];
                $stats['reported'] = 0;
                $stats['pending']['total'] = 0;
                $stats['pending']['overdue'] = ['total'=>0, 'signed'=>0, 'unsigned'=>0];
                $stats['pending']['intime'] = ['total'=>0, 'signed'=>0, 'unsigned'=>0];
                $stats['yearly'] = [];

                if (count($c)>0)
                {
                    foreach($c as $incident)
                    {
                        //if (strpos($str, '.') !== FALSE)
                        //    echo 'Found it';
                        //else
                        //    echo "nope!";
                        if (strpos($incident->event_date_time, $current_year)!==FALSE)
                                $stats['total_cy'] += 1;
                        
                        //$stats->reg['total']+=1;
                        
                        //get the incident year
                        $dt = new \DateTime($incident->event_date_time);
                        $year = date_format($dt, 'Y');
                        
                        //check if the year is already
                        key_exists($year, $stats['yearly'])? $stats['yearly'][$year]['total']+=1 : $stats['yearly'][$year]['total']=1;
                        
                        if ($incident->status == \app\models\events\EventDeclaration::INCIDENT_REPORT_DRAFT)
                        {
                            key_exists('reporting', $stats['yearly'][$year]) ? $stats['yearly'][$year]['reporting']['total']+=1 : $stats['yearly'][$year]['reporting']['total'] = 1;
                            //update [pending][total]
                            $stats['pending']['total']+=1;
                            
                            //split by outdated, expiring, safe
                            if ($incident->timeLeft->invert == 0)
                            {
                                key_exists('overdue', $stats['yearly'][$year]['reporting']) ? $stats['yearly'][$year]['reporting']['overdue']['total']+=1 : $stats['yearly'][$year]['reporting']['overdue']['total']=1;
                                //update ['pending']['overdue']['total']
                                //update ['pending']['overdue']['unsigned']
                                $stats['pending']['overdue']['total']+=1;
                                $stats['pending']['overdue']['unsigned']+=1;
                            }
                            else
                            {
                                key_exists('intime', $stats['yearly'][$year]['reporting']) ? $stats['yearly'][$year]['reporting']['intime']['total']+=1 : $stats['yearly'][$year]['reporting']['intime']['total']=1;
                                //update ['pending']['intime']['total']
                                //update ['pending']['intime']['unsigned']
                                $stats['pending']['intime']['total']+=1;
                                $stats['pending']['intime']['unsigned']+=1;
                            }
                        }
                        else if ($incident->status == \app\models\events\EventDeclaration::INCIDENT_REPORT_SIGNED)
                        {
                            key_exists('signed', $stats['yearly'][$year]) ? $stats['yearly'][$year]['signed']['total']+=1 : $stats['yearly'][$year]['signed']['total'] = 1;
                            //update [pending]['total]
                            $stats['pending']['total']+=1;
                            if ($incident->timeLeft->invert)
                            {
                                key_exists('overdue', $stats['yearly'][$year]['signed']) ? $stats['yearly'][$year]['signed']['overdue']['total']+=1 : $stats['yearly'][$year]['signed']['overdue']['total']=1;
                                //update ['pending']['overdue']['total']
                                //update ['pending']['overdue']['signed']
                                $stats['pending']['overdue']['total']+=1;
                                $stats['pending']['overdue']['signed']+=1;
                            }
                            else
                            {
                                key_exists('intime', $stats['yearly'][$year]['signed']) ? $stats['yearly'][$year]['signed']['intime']['total']+=1 : $stats['yearly'][$year]['signed']['intime']['total']=1;
                                //update ['pending']['intime']['total']
                                //update ['pending']['intime']['signed']
                                $stats['pending']['intime']['total']+=1;
                                $stats['pending']['intime']['signed']+=1;
                            }
                        }
                        else if ($incident->status == \app\models\events\EventDeclaration::INCIDENT_REPORT_REPORTED)
                        {
                            key_exists('reported', $stats['yearly'][$year]) ? $stats['yearly'][$year]['reported']['total']+=1 : $stats['yearly'][$year]['reported']['total'] = 1;
                            //update ['reported']
                            $stats['reported']+=1;
                        }
                    }
                }
                
                //return $c;
                return $stats;
            }
            else
            {
                \Yii::$app->error->ThrowError(Errors::RESTRICTED_OPERATION_ERROR);
            }
        }
    }
    
    public function getErrors()
    {
        return $this->_errors;
    }
    
    /**
     * The method computes the time left for submitting the report
     * @return DateInterval
     */
    public function TimeLeft()
    {
        //$date1 = new \DateTime($this->owner->event_date_time);
        
        $prop = $this->timeField;
        $date1 = new \DateTime($this->owner->$prop);
        $date1->add(new \DateInterval('P' . $this->TargetTimespan . 'D'));
        //return print_r($date1);
        
        $date2 = new \DateTime(date('Y-m-d H:i:s'));
        
        //return print_r($interval);
        //$retS = '<br/>deadline: ' . print_r($date1);
        //$retS .= '<br/>current date: ' . print_r($date2);

        if (isset($retArr)) unset($retArr);
        $retArr = [];
        
        
        $interval = $date1->diff($date2);
        
        $date1 > $date2 ? $retArr['overdue'] = 0 : $retArr['overdue'] = 1;
        $retArr['deadline'] = $date1;
        $retArr['years'] = $interval->y;
        $retArr['months'] = $interval->m;
        $retArr['days'] = $interval->d;
        $retArr['hours'] = $interval->h;
        $retArr['minutes'] = $interval->i;
        $retArr['interval'] = $interval;
        
        //var_dump($date1);
        
        //$retS = '<br/>' . $msg . $interval->d . ' days, ' . $interval->h . ' hours and ' . $interval->i . ' minutes';
        return $retArr ;
    }
    
    private function dateDiff()
    {
$date1 = new DateTime("2007-03-24");
$date2 = new DateTime("2009-06-26");
$interval = $date1->diff($date2);
echo "difference " . $interval->y . " years, " . $interval->m." months, ".$interval->d." days "; 

// shows the total amount of days (not divided into years, months and days like above)
echo "difference " . $interval->days . " days ";        
        
    }
    
    
    public function getCanBePurged() {
        $q1 = \app\models\events\EventDeclaration::find()
                ->where(['raporteur_id'=>$this->owner->id])
                ->orWhere(['created_by'=>$this->owner->id])
                ->orWhere(['modified_by_id'=>$this->owner->id])->count();
        
        $q2 = \app\models\events\drafts\EventDraft::find()
                ->where(['created_by'=>$this->owner->id])
                ->orWhere(['modified_by'=>$this->owner->id])->count();
        
        return ($q1+$q2)==0;
    }
    
}
