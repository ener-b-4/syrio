<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\ca;

use Yii;
use app\models\events\drafts\EventDraft;
/**
 * This is the model class for table "ca_incident_cat".
 *
 * @property integer $id
 * @property integer $draft_id
 * @property app\models\events\drafts\EventDraft $draft
 * @property integer $is_major_a
 * @property integer $is_major
 * @property string $major_a_just
 * @property integer $a_assessed_by
 * @property string $a_assessed_at
 * @property integer $is_major_b
 * @property string $major_b_just
 * @property integer $b_assessed_by
 * @property string $b_assessed_at
 * @property integer $is_major_c
 * @property string $major_c_just
 * @property integer $c_assessed_by
 * @property string $c_assessed_at
 * @property integer $is_major_d
 * @property string $major_d_just
 * @property integer $d_assessed_by
 * @property string $d_assessed_at
 * @property integer $is_major_e
 * @property string $major_e_just
 * @property integer $e_assessed_by
 * @property string $e_assessed_at
 * @property integer $is_major_f
 * @property string $major_f_just
 * @property integer $f_assessed_by
 * @property string $f_assessed_at
 * @property integer $is_major_g
 * @property string $major_g_just
 * @property integer $g_assessed_by
 * @property string $g_assessed_at
 * @property integer $is_major_h
 * @property string $major_h_just
 * @property integer $h_assessed_by
 * @property string $h_assessed_at
 * @property integer $is_major_i
 * @property string $major_i_just
 * @property integer $i_assessed_by
 * @property string $i_assessed_at
 * @property integer $is_major_j
 * @property string $major_j_just
 * @property integer $j_assessed_by
 * @property string $j_assessed_at
 * @property string $op_submitted_at
 * @property string $rejection_desc
 * @property integer $is_done
 * @property EventClassifications $draft
 * 
 * @property string $name;
 * @property integer $cpf_status
 */
class IncidentCategorization extends \yii\db\ActiveRecord
{
	const INCIDENT_REPORT_UNASSESSED = 0;
	const INCIDENT_REPORT_ASSESSED = 1;
	const INCIDENT_REPORT_REJECTED = -1;
	
        public $save_as_draft;
	
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'ca_incident_cat';
	}

        
        public function beforeSave($insert) {
            if (parent::beforeSave($insert)) {
                if (!$insert) {
                    
                    if ($this->save_as_draft) {
                        //check if there's a cpf_crf_assessment
                        if (isset($this->cpfAssessment)) {
                            //set the cpf assessment as suspended if finalized
                            if ($this->cpfAssessment->assessment_status === \app\modules\cpfbuilder\models\CpfCrfAssessments::ASSESS_STATUS_FINALIZED) {
                                $this->cpfAssessment->assessment_status = \app\modules\cpfbuilder\models\CpfCrfAssessments::ASSESS_STATUS_SUSPENDED;
                                $this->cpfAssessment->update();

                                //set the message and put it in Session
                                $year = date('Y', strtotime($this->report->event->event_date_time));
                                $message = \Yii::t('app/cpf', 'The Assessment in terms of Section 4 of {evt} has been suspended.', ['evt'=>$this->name]);
                                $message .= '<br/>';
                                $message .= \Yii::t('app/cpf', 'The changes entail refreshing the CPF Annual Report (for {evt}).', ['evt'=>$year]);
                                $message .= \Yii::t('app/cpf', 'Please proceed accordingly.');

                                $session = \Yii::$app->session;
                                $session->setFlash('update_msg', $message, 'true');
                            }
                        }
                    } else {
                        //the case is finalized or re-finalized
                        if (isset($this->cpfAssessment) && 
                                ($this->cpfAssessment->assessment_status === \app\modules\cpfbuilder\models\CpfCrfAssessments::ASSESS_STATUS_SUSPENDED ||
                                $this->cpfAssessment->assessment_status === \app\modules\cpfbuilder\models\CpfCrfAssessments::ASSESS_STATUS_FINALIZED)) {
                            
                            //set the message to be passed through session
                            $message = Yii::t('app', 'The changes might require refreshing the Annual Report for the specific year. Please check.');                            

                            $session = \Yii::$app->session;
                            $session->setFlash('update_msg', $message, 'true');
                            
                            $this->cpfAssessment->assessment_status = \app\modules\cpfbuilder\models\CpfCrfAssessments::ASSESS_STATUS_FINALIZED;
                            $this->cpfAssessment->update();
                        } elseif (!isset($this->cpfAssessment)) {
                            //finalized
                            //set the message to be passed through session
                            $message = Yii::t('app', 'You may now proceed with assessing the event in terms of Section 4 of the Annual Report.') . '</br>' .
                            Yii::t('app', 'For this go to Home > Annual report preparation then click Refresh.');                            

                            $session = \Yii::$app->session;
                            $session->setFlash('update_msg', $message, 'true');
                        }
                    }
                }
            }
            
            return parent::beforeSave($insert);
        }
        
        
        public function beforeDelete() {
            if (parent::beforeDelete()) {
                if (isset($this->cpfAssessment) && 
                        ($this->cpfAssessment->assessment_status === \app\modules\cpfbuilder\models\CpfCrfAssessments::ASSESS_STATUS_SUSPENDED ||
                        $this->cpfAssessment->assessment_status === \app\modules\cpfbuilder\models\CpfCrfAssessments::ASSESS_STATUS_FINALIZED)) {
                        
                    //check if exists cpf session for the year
                    //if so, set to be refreshed
                    
                    $year = date('Y', strtotime($model->report->event->event_date_time));
                    //get the CpfSession corresponding to $year
                    //modify refresh_status is found
                    $cpf_session = \app\modules\cpfbuilder\models\CpfSessions::findOne(['year'=>$year]);
                    if (isset($cpf_session)) {
                        $cpf_session->refresh_data = 1;
                        $cpf_session->refresh_installations = 1;
                        $cpf_session->update();
                    }
                }
            }
            
            return parent::beforeDelete();
        }






        /**
	 * @inheritdoc
	 */
	public function rules()
	{
		//, 'message' => 'Please choose a username.'
		return [
			[['draft_id'], 'required', 'on'=>'submit', 'message' => 'error.'],

			[['is_major_a'], 'required', 'on'=>'submit', 'when' => function($model) {return $model->draft->is_a;},  'message' => Yii::t('app', 'Please revise Section {section}.', ['section' => 'A'])],
			[['is_major_b'], 'required', 'on'=>'submit', 'when' => function($model) {return $model->draft->is_b;},  'message' => Yii::t('app', 'Please revise Section {section}.', ['section' => 'B'])],
			[['is_major_c'], 'required', 'on'=>'submit', 'when' => function($model) {return $model->draft->is_c;},  'message' => Yii::t('app', 'Please revise Section {section}.', ['section' => 'C'])],
			[['is_major_d'], 'required', 'on'=>'submit', 'when' => function($model) {return $model->draft->is_d;},  'message' => Yii::t('app', 'Please revise Section {section}.', ['section' => 'D'])],
			[['is_major_e'], 'required', 'on'=>'submit', 'when' => function($model) {return $model->draft->is_e;},  'message' => Yii::t('app', 'Please revise Section {section}.', ['section' => 'E'])],
			[['is_major_f'], 'required', 'on'=>'submit', 'when' => function($model) {return $model->draft->is_f;},  'message' => Yii::t('app', 'Please revise Section {section}.', ['section' => 'F'])],
			[['is_major_g'], 'required', 'on'=>'submit', 'when' => function($model) {return $model->draft->is_g;},  'message' => Yii::t('app', 'Please revise Section {section}.', ['section' => 'G'])],
			[['is_major_h'], 'required', 'on'=>'submit', 'when' => function($model) {return $model->draft->is_h;},  'message' => Yii::t('app', 'Please revise Section {section}.', ['section' => 'H'])],
			[['is_major_i'], 'required', 'on'=>'submit', 'when' => function($model) {return $model->draft->is_i;},  'message' => Yii::t('app', 'Please revise Section {section}.', ['section' => 'I'])],
			[['is_major_j'], 'required', 'on'=>'submit', 'when' => function($model) {return $model->draft->is_j;},  'message' => Yii::t('app', 'Please revise Section {section}.', ['section' => 'J'])],

			[['major_a_just'], 'required', 'on'=>'submit', 'when' => function($model) {return $model->draft->is_a;}, 'message' => Yii::t('app', 'Please provide a justification for the choice for Section {evt}.', ['evt'=> 'A'])],
			[['major_b_just'], 'required', 'on'=>'submit', 'when' => function($model) {return $model->draft->is_b;}, 'message' => Yii::t('app', 'Please provide a justification for the choice for Section {evt}.', ['evt'=> 'B'])],
			[['major_c_just'], 'required', 'on'=>'submit', 'when' => function($model) {return $model->draft->is_c;}, 'message' => Yii::t('app', 'Please provide a justification for the choice for Section {evt}.', ['evt'=> 'C'])],
			[['major_d_just'], 'required', 'on'=>'submit', 'when' => function($model) {return $model->draft->is_d;}, 'message' => Yii::t('app', 'Please provide a justification for the choice for Section {evt}.', ['evt'=> 'D'])],
			[['major_e_just'], 'required', 'on'=>'submit', 'when' => function($model) {return $model->draft->is_e;}, 'message' => Yii::t('app', 'Please provide a justification for the choice for Section {evt}.', ['evt'=> 'E'])],
			[['major_f_just'], 'required', 'on'=>'submit', 'when' => function($model) {return $model->draft->is_f;}, 'message' => Yii::t('app', 'Please provide a justification for the choice for Section {evt}.', ['evt'=> 'F'])],
			[['major_g_just'], 'required', 'on'=>'submit', 'when' => function($model) {return $model->draft->is_g;}, 'message' => Yii::t('app', 'Please provide a justification for the choice for Section {evt}.', ['evt'=> 'G'])],
			[['major_h_just'], 'required', 'on'=>'submit', 'when' => function($model) {return $model->draft->is_h;}, 'message' => Yii::t('app', 'Please provide a justification for the choice for Section {evt}.', ['evt'=> 'H'])],
			[['major_i_just'], 'required', 'on'=>'submit', 'when' => function($model) {return $model->draft->is_i;}, 'message' => Yii::t('app', 'Please provide a justification for the choice for Section {evt}.', ['evt'=> 'I'])],
			[['major_j_just'], 'required', 'on'=>'submit', 'when' => function($model) {return $model->draft->is_j;}, 'message' => Yii::t('app', 'Please provide a justification for the choice for Section {evt}.', ['evt'=> 'J'])],

			['rejection_desc', 'safe'],
			['rejection_desc', 'required', 'on'=>'reject'],
			
			[['is_done'], 'safe'],		
					
			[[  'draft_id', 
				'is_major_a', 
				'is_major_b', 
				'is_major_c', 
				'is_major_d', 
				'is_major_e', 
				'is_major_f', 
				'is_major_g', 
				'is_major_h', 
				'is_major_i', 
				'is_major_j', 
				'a_assessed_by', 
				'b_assessed_by',
				], 'integer', 'min'=>0],  //Author: cavesje Date:20151119
			
			[[  'major_a_just', 
				'major_b_just',
				'major_c_just',
				'major_d_just',
				'major_e_just',
				'major_f_just',
				'major_g_just',
				'major_h_just',
				'major_i_just',
				'major_j_just',
				], 'string'],
			[[  'a_assessed_at', 
				'b_assessed_at', 
				'c_assessed_at', 
				'd_assessed_at', 
				'e_assessed_at', 
				'f_assessed_at', 
				'g_assessed_at', 
				'h_assessed_at', 
				'i_assessed_at', 
				'j_assessed_at', 
				'op_submitted_at'], 'safe'],
                                
                        //vamanbo 20160429 in [0,1]    
			[[  'is_major_a', 
				'is_major_b', 
				'is_major_c', 
				'is_major_d', 
				'is_major_e', 
				'is_major_f', 
				'is_major_g', 
				'is_major_h', 
				'is_major_i', 
				'is_major_j',
                                'is_major',
				], 'in', 'range'=>[0,1]],  //Author: vamanbo Date:20160429

                        //vamanbo 20160429 in [0,1]    
			[[  'is_major_a', 
				'is_major_b', 
				'is_major_c', 
				'is_major_d', 
				'is_major_e', 
				'is_major_f', 
				'is_major_g', 
				'is_major_h', 
				'is_major_i', 
				'is_major_j',
                                'is_major',
                                'is_done',
				], 'filter', 'filter'=>'intval', 'skipOnEmpty' => true],  //Author: vamanbo Date:20160429
                                
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'draft_id' => 'draft ID',
			'is_major_a' => Yii::t('app', 'True if report Section {evt} is categorized as major', ['evt'=>'A']),
			'is_major_b' => Yii::t('app', 'True if report Section {evt} is categorized as major', ['evt'=>'B']),
			'is_major_c' => Yii::t('app', 'True if report Section {evt} is categorized as major', ['evt'=>'C']),
			'is_major_d' => Yii::t('app', 'True if report Section {evt} is categorized as major', ['evt'=>'D']),
			'is_major_e' => Yii::t('app', 'True if report Section {evt} is categorized as major', ['evt'=>'E']),
			'is_major_f' => Yii::t('app', 'True if report Section {evt} is categorized as major', ['evt'=>'F']),
			'is_major_g' => Yii::t('app', 'True if report Section {evt} is categorized as major', ['evt'=>'G']),
			'is_major_h' => Yii::t('app', 'True if report Section {evt} is categorized as major', ['evt'=>'H']),
			'is_major_i' => Yii::t('app', 'True if report Section {evt} is categorized as major', ['evt'=>'I']),
			'is_major_j' => Yii::t('app', 'True if report Section {evt} is categorized as major', ['evt'=>'J']),
			'major_a_just' => Yii::t('app', 'Justification if Section {evt} is categorized as major', ['evt'=>'A']),
			'major_b_just' => Yii::t('app', 'Justification if Section {evt} is categorized as major', ['evt'=>'B']),
			'major_c_just' => Yii::t('app', 'Justification if Section {evt} is categorized as major', ['evt'=>'C']),
			'major_d_just' => Yii::t('app', 'Justification if Section {evt} is categorized as major', ['evt'=>'D']),
			'major_e_just' => Yii::t('app', 'Justification if Section {evt} is categorized as major', ['evt'=>'E']),
			'major_f_just' => Yii::t('app', 'Justification if Section {evt} is categorized as major', ['evt'=>'F']),
			'major_g_just' => Yii::t('app', 'Justification if Section {evt} is categorized as major', ['evt'=>'G']),
			'major_h_just' => Yii::t('app', 'Justification if Section {evt} is categorized as major', ['evt'=>'H']),
			'major_i_just' => Yii::t('app', 'Justification if Section {evt} is categorized as major', ['evt'=>'I']),
			'major_j_just' => Yii::t('app', 'Justification if Section {evt} is categorized as major', ['evt'=>'J']),
			'a_assessed_by' => Yii::t('app', '{section} Assessed By', ['section' => 'A']),
			'a_assessed_at' => Yii::t('app', '{section} Assessed At', ['section' => 'A']),
			'b_assessed_by' => Yii::t('app', '{section} Assessed By', ['section' => 'B']),
			'b_assessed_at' => Yii::t('app', '{section} Assessed At', ['section' => 'B']),
			'c_assessed_by' => Yii::t('app', '{section} Assessed By', ['section' => 'C']),
			'c_assessed_at' => Yii::t('app', '{section} Assessed At', ['section' => 'C']),
			'd_assessed_by' => Yii::t('app', '{section} Assessed By', ['section' => 'D']),
			'd_assessed_at' => Yii::t('app', '{section} Assessed At', ['section' => 'D']),
			'e_assessed_by' => Yii::t('app', '{section} Assessed By', ['section' => 'E']),
			'e_assessed_at' => Yii::t('app', '{section} Assessed At', ['section' => 'E']),
			'f_assessed_by' => Yii::t('app', '{section} Assessed By', ['section' => 'F']),
			'f_assessed_at' => Yii::t('app', '{section} Assessed At', ['section' => 'F']),
			'g_assessed_by' => Yii::t('app', '{section} Assessed By', ['section' => 'G']),
			'g_assessed_at' => Yii::t('app', '{section} Assessed At', ['section' => 'G']),
			'h_assessed_by' => Yii::t('app', '{section} Assessed By', ['section' => 'H']),
			'h_assessed_at' => Yii::t('app', '{section} Assessed At', ['section' => 'H']),
			'i_assessed_by' => Yii::t('app', '{section} Assessed By', ['section' => 'I']),
			'i_assessed_at' => Yii::t('app', '{section} Assessed At', ['section' => 'I']),
			'j_assessed_by' => Yii::t('app', '{section} Assessed By', ['section' => 'J']),
			'j_assessed_at' => Yii::t('app', '{section} Assessed At', ['section' => 'J']),
			'op_submitted_at' => Yii::t('app', 'Report Submitted At'),
			'rejection_desc' => Yii::t('app', 'Rejection justification'),
			'cpf_status' => Yii::t('app', 'CPF Assessed'),
		];
	}

	/**
	 * @return \app\models\events\drafts\EventDraft
	 */
	public function getDraft()
	{
		return $this->hasOne(EventDraft::className(), ['id' => 'draft_id']);
	}
	
        
        /**
         * The event name is formed from:
         * 
         * - op_submitted_at as     - yymmdd
         * - operator acronym       - max 8 pad X
         * - installation name      - replace ' ' with '_'
         * - field                  - XXXX
         * 
         * @return string
         */
        public function getName()
	{
            //the event name will be formed from
            
//            echo '<pre>';
//            echo var_dump($this->draft->event->organization);
//            echo '</pre>';
//            
//            die();
            
            $dt = \DateTime::createFromFormat('Y-m-d H:i:s', $this->op_submitted_at);
            $field = isset($this->draft->event->field_name) ? $this->event->field_name : 'XXXX';
            $operator = isset($this->draft->event->organization) ? $this->draft->event->organization->organization_acronym : $this->draft->event->operator;
            return date_format($dt, 'ymd') . 
                    '/' . $operator .
                    '/' . str_replace(' ', '', $this->draft->event->installation->name) .
                    '/' . $field;
                    
            
	//	return $this->draft->event->operator . '/' . $this->op_submitted_at;
	}
	

	public function getReport()
	{
		return $this->hasOne(EventDraft::className(), ['id' => 'draft_id']);
	}
        
        
	function validate_misc($attribute, $params) {
		//vezi daca draft e a
		
		if ($this->draft->is_a)
			
		
		
		//first check if date format is ok
		//$inputDate = new DateTime()
		
	//here your validation
		//$attr = print_r($this->$attribute);
		//echo '<script>alert(' . $attr . ')</script>';
		try
		{
			if (date_parse($this->$attribute))
			{
				$inputDate = new \DateTime($this->$attribute);
				if ($inputDate>new \DateTime())
				{
					$errText = 'You cannot add a date from future!'; 
					$this->addError($attribute, $errText);
				}
			}
			else
			{
				$errText = 'Please provide date and time in the yyyy/MM/dd hh:mm format!'; 
				$this->addError($attribute, $errText);
			}
		} catch (Exception $ex) {
			//$this->addError($attribute, date_format($inputDate,'D, d M Y'));  
			Yii::warning('abc');
			$errText = 'Please provide date and time in the yyyy/MM/dd hh:mm format!'; 
			$this->addError($attribute, $errText);
		}
		
	}
	
	
	public function getEvent()
	{
            return $this->draft->event;
	}
        
        public function getCpf_assessed()
        {
            if ($this->is_done == 0) {
                return null;
            } elseif ($this->is_done == -1) {
                //report rejected
                return -20;
            }
            
            $obj = \app\modules\cpfbuilder\models\CpfCrfAssessments::find()
                    ->where(['report_id' => $this->draft_id])->one();
            
            if (!isset($obj)) {
                //hard coded value!
                return -10;
            } else {
                return $obj->assessment_status;
            }
        }
        
        
        public function getCpfAssessment()
        {
            return $this->hasOne(\app\modules\cpfbuilder\models\CpfCrfAssessments::className(), ['report_id'=>'draft_id']);
        }
        
}

/* 20150920 */