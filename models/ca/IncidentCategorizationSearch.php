<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\ca;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ca\IncidentCategorization;
use app\models\events\EventDeclaration;

/**
 * IncidentCategorizationSearch represents the model behind the search form about `app\models\ca\IncidentCategorization`.
 */
class IncidentCategorizationSearch extends IncidentCategorization
{
    public $ma_assess;
    public $event_date_time;
    public $operator;
    public $installation;
    public $event;

    public $deadline_status;
    
    public $cpf_assessed;

    const MA_UNASSESSED = 0;
    const MA_MAJOR = 1;
    const MA_NOT_MAJOR = 2;
    const MA_REJECTED = 3;
    
    const CPF_NOT_NEEDED = -20;
    const CPF_NOT_AVAILABLE = -10;
    const CPF_NOT_ASSESSED = 20;
    const CPF_ASSESSED = 10;
    
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'draft_id', 'is_major_a', 'is_major_b', 'a_assessed_by', 'b_assessed_by', 'is_major'], 'integer', 'min'=>0], //Author: cavesje Date:20151119
            [['major_a_just', 'major_b_just', 'a_assessed_at', 'b_assessed_at', 'op_submitted_at'], 'safe'],
            [['ma_assess', 'event_date_time', 'operator', 'installation', 'event'], 'safe'],
            ['is_done', 'in', 'range' => [-20, -10, 0, 1, 2, 3, 10, 20]],
            ['deadline_status', 'in', 'range' => [4, 5]],
            ['cpf_assessed', 'in', 'range' => [-20, -10, 0, 10]],
            [['op_submitted_at'], 'match', 'pattern' => '#(\d{4}|\*{1})\-{1}(\d{2}|\*){1}\-{1}(\d{2}|\*{1})#'],
            [['event'], 'match', 'pattern' => '#^[a-jA-J]+(?:[\,\a-jA-J]{0,9})*$#'],
            //[['event'], 'match', 'pattern' => '#^[-\w\s]+(?:,[-\w\s]{0,9})*$#'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = IncidentCategorization::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $query->join('left join', '{{event_classifications}}', 
                '{{event_classifications}}.[[id]] = {{ca_incident_cat}}.[[draft_id]]');
        
        $query->join('inner join', '{{event_declaration}}', 
                '{{event_classifications}}.[[event_id]] = {{event_declaration}}.[[id]]');

        $query->join('inner join', '{{installations}}', 
                '{{event_declaration}}.[[installation_id]] = {{installations}}.[[id]]');

        $query->join('inner join', '{{organizations}}', 
                '{{event_declaration}}.[[operator_id]] = {{organizations}}.[[id]]');
        

        $dataProvider->setSort([
            'attributes' => [
                'operator' => [
                    'asc' => ['organizations.organization_acronym'=>SORT_ASC],
                    'desc' => ['organizations.organization_acronym'=>SORT_DESC],
                ],
                'installation' => [
                    'asc' => ['installations.name'=>SORT_ASC],
                    'desc' => ['installations.name'=>SORT_DESC],
                ],
                'event_date_time',
                'op_submitted_at',
                'is_done',
                //'field_name'
            ]
        ]);
        
        
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        if (isset($this->is_done))
        {
            switch($this->is_done)
            {
                case null:
                    break;
                case self::MA_UNASSESSED:
                    $query->andFilterWhere(['is_done'=>0])
                        ->andFilterWhere([
                            'not', [
                                '{{event_classifications}}.[[ca_status]]'=> \app\models\events\EventDeclaration::INCIDENT_REPORT_REJECTED
                                ]
                                ]);
                    break;
                case self::MA_MAJOR:
                    //major
                    $query->andFilterWhere(['or',
                        ['is_major_a' => 1],
                        ['is_major_b' => 1],
                        ['is_major_c' => 1],
                        ['is_major_d' => 1],
                        ['is_major_e' => 1],
                        ['is_major_f' => 1],
                        ['is_major_g' => 1],
                        ['is_major_h' => 1],
                        ['is_major_i' => 1],
                        ['is_major_j' => 1],
                        ]);
                    break;
                case self::MA_NOT_MAJOR:
                    //non-major
                    $query->andFilterWhere(['is_done'=>1]);
                    $query->andWhere([
                        'and',
                        ['or', 
                            ['is_major_a'=>null],
                            ['is_major_a'=>0]
                        ],
                        ['or', 
                            ['is_major_b'=>null],
                            ['is_major_b'=>0]
                        ],
                        ['or', 
                            ['is_major_c'=>null],
                            ['is_major_c'=>0]
                        ],
                        ['or', 
                            ['is_major_d'=>null],
                            ['is_major_d'=>0]
                        ],
                        ['or', 
                            ['is_major_e'=>null],
                            ['is_major_e'=>0]
                        ],
                        ['or', 
                            ['is_major_f'=>null],
                            ['is_major_f'=>0]
                        ],
                        ['or', 
                            ['is_major_g'=>null],
                            ['is_major_g'=>0]
                        ],
                        ['or', 
                            ['is_major_h'=>null],
                            ['is_major_h'=>0]
                        ],
                        ['or', 
                            ['is_major_i'=>null],
                            ['is_major_i'=>0]
                        ],
                        ['or', 
                            ['is_major_j'=>null],
                            ['is_major_j'=>0]
                        ],                        
                        ]);
                    break;
                case self::MA_REJECTED:
                case self::CPF_NOT_NEEDED:
                    //rejected
                    $query->andFilterWhere(['is_done'=>-1])
                        ->andFilterWhere([
                                '{{event_classifications}}.[[ca_status]]'=> \app\models\events\EventDeclaration::INCIDENT_REPORT_REJECTED
                                ]);
                    break;
            
                case self::CPF_NOT_AVAILABLE:
                    $query->join('left join', '{{cpf_crf_assessments}}', 
                            '{{cpf_crf_assessments}}.[[report_id]] = {{ca_incident_cat}}.[[draft_id]]')
                            ->andWhere(['{{ca_incident_cat}}.[[is_done]]' => 1])    //the report is accepted
                            ->andWhere(['{{cpf_crf_assessments}}.[[report_id]]' => NULL]);
                    
                    break;
                case self::CPF_NOT_ASSESSED:
                    $query->join('left join', '{{cpf_crf_assessments}}', 
                            '{{cpf_crf_assessments}}.[[report_id]] = {{ca_incident_cat}}.[[draft_id]]')
                            ->andWhere(['{{ca_incident_cat}}.[[is_done]]' => 1])    //the report is accepted
                            ->andWhere(['<>', '{{cpf_crf_assessments}}.[[assessment_status]]', \app\modules\cpfbuilder\models\CpfCrfAssessments::ASSESS_STATUS_FINALIZED]);
                case self::CPF_ASSESSED:
                    $query->join('left join', '{{cpf_crf_assessments}}', 
                            '{{cpf_crf_assessments}}.[[report_id]] = {{ca_incident_cat}}.[[draft_id]]')
                            ->andWhere(['{{ca_incident_cat}}.[[is_done]]' => 1])    //the report is accepted
                            ->andWhere(['=', '{{cpf_crf_assessments}}.[[assessment_status]]', \app\modules\cpfbuilder\models\CpfCrfAssessments::ASSESS_STATUS_FINALIZED]);
            }
        }
        
        
        if (isset($this->op_submitted_at))
        {
            /*
            $query->andFilterWhere(['and',
                'event_date_time BETWEEN "' . $this->event_date_time . '" AND "' . $this->event_date_time . ' 23:59:59"'
                ]);
             * 
             */
            
            /* '#(\d{4}|\*{1})\-{1}(\d{2}|\*){1}\-{1}(\d{2}|\*{1})#' */
            
            /* case Y-m-d */
            if (preg_match_all('#(\d{4})\-{1}(\d{2}){1}\-{1}(\d{2})#', $this->op_submitted_at))
            {
                /* 
                 * returns all the events in a given date 
                 * year month day set
                 */
                $query->andFilterWhere(['and',
                    'op_submitted_at BETWEEN "' . $this->op_submitted_at . '" AND "' . $this->op_submitted_at . ' 23:59:59"'
                    ]);
            }
            else if (preg_match_all('#(\d{4})\-{1}(\d{2}){1}\-{1}(\*{1})#', $this->op_submitted_at))
            {
                /* 
                 * returns all the events in a given year and month (day irrespective) 
                 * year month set
                 */
                
                $parts = explode('-', $this->op_submitted_at);
                
                $query->andFilterWhere(['and',
                    'year(op_submitted_at) = ' . $parts[0],    
                    'month(op_submitted_at) = ' . $parts[1]
                    ]);
            }
            elseif ((preg_match_all('#(\d{4})\-{1}(\*{1})\-{1}(\*{1})#', $this->op_submitted_at)))
            {
                /* 
                 * returns all the events in a given year (month and day irrespective) 
                 * year set
                 */
                
                $parts = explode('-', $this->op_submitted_at);
                
                $query->andFilterWhere(['and',
                    'year(op_submitted_at) = ' . $parts[0],    
                    ]);
            }
            elseif ((preg_match_all('#(\*{1})\-{1}(\d{2}){1}\-{1}(\*{1})#', $this->op_submitted_at)))
            {
                /* 
                 * returns all the events in a given month (year and day irrespective) 
                 * year set
                 */
                
                $parts = explode('-', $this->op_submitted_at);
                
                $query->andFilterWhere(['and',
                    'month(op_submitted_at) = ' . $parts[1],    
                    ]);
            }
            elseif ((preg_match_all('#(\*{1})\-{1}(\*){1}\-{1}(\d{2})#', $this->op_submitted_at)))
            {
                /* 
                 * returns all the events in a given day (year and month irrespective) 
                 * year set
                 */
                
                $parts = explode('-', $this->op_submitted_at);
                
                $query->andFilterWhere(['and',
                    'day(op_submitted_at) = ' . $parts[2],    
                    ]);
            }
            elseif ((preg_match_all('#(\*{1})\-{1}(\d{2}){1}\-{1}(\d{2})#', $this->op_submitted_at)))
            {
                /* 
                 * returns all the events in a month on a particular day (year irrespective) 
                 * month day set
                 */
                $parts = explode('-', $this->op_submitted_at);
                
                $query->andFilterWhere(['and',
                    'month(op_submitted_at) = ' . $parts[1],
                    'day(op_submitted_at) = ' . $parts[2]    
                    ]);
            }
            elseif ((preg_match('#(\*{1})\-{1}(\*{1})\-{1}(\*{1})#', $this->op_submitted_at)))
            {
                //echo 'select all';
                //die();
            }
            else if (preg_match_all('#(\d{4})\-{1}(\*{1}){1}\-{1}(\d{2})#', $this->op_submitted_at))
            {
                /* 
                 * returns all the events in a given year and day (month irrespective) 
                 * year day set
                 */
                
                $parts = explode('-', $this->op_submitted_at);
                
                $query->andFilterWhere(['and',
                    'year(op_submitted_at) = ' . $parts[0],    
                    'day(op_submitted_at) = ' . $parts[2]
                    ]);
            }
            
            
        }
        
        
        if (isset($this->deadline_status)) {
            switch ($this->deadline_status) {
                case \app\models\events\EventDeclaration::TIME_STATUS_SUBMITTED_OK:
                    $query->andWhere(['or',
                            ['{{event_classifications}}.[[ca_status]]' => EventDeclaration::INCIDENT_REPORT_REPORTED],
                            ['{{event_classifications}}.[[ca_status]]' => EventDeclaration::INCIDENT_REPORT_REJECTED],
                            ['{{event_classifications}}.[[ca_status]]' => EventDeclaration::INCIDENT_REPORT_ACCEPTED],
                            ['{{event_classifications}}.[[ca_status]]' => EventDeclaration::INCIDENT_REPORT_RESUBMITTED],                            ]
                        )                            
                        ->andWhere(['and',
                        'unix_timestamp(event_declaration.event_deadline) - event_classifications.modified_at > 0'
                        ]);
                    break;
                case \app\models\events\EventDeclaration::TIME_STATUS_SUBMITTED_LATE:
                    $query->andWhere(['or',
                            ['{{event_classifications}}.[[ca_status]]' => EventDeclaration::INCIDENT_REPORT_REPORTED],
                            ['{{event_classifications}}.[[ca_status]]' => EventDeclaration::INCIDENT_REPORT_REJECTED],
                            ['{{event_classifications}}.[[ca_status]]' => EventDeclaration::INCIDENT_REPORT_ACCEPTED],
                            ['{{event_classifications}}.[[ca_status]]' => EventDeclaration::INCIDENT_REPORT_RESUBMITTED],                            ]
                        )                            
                        ->andWhere(['and',
                        'unix_timestamp(event_declaration.event_deadline) - event_classifications.modified_at < 0'
                        ]);
                    break;
            }
        }
        
        
        
        $this->parse_evt_time($query, $this->event_date_time);
        
        if (isset($this->operator)) { $this->parse_operator($query, $this->operator); }
        
        $this->parse_installation($query, $this->installation);
        
        $this->parse_event_type($query, $this->event);
        
        return $dataProvider;
        
        $query->andFilterWhere([
            'id' => $this->id,
            'draft_id' => $this->draft_id,
            'is_major_a' => $this->is_major_a,
            'is_major_b' => $this->is_major_b,
            'a_assessed_by' => $this->a_assessed_by,
            'a_assessed_at' => $this->a_assessed_at,
            'b_assessed_by' => $this->b_assessed_by,
            'b_assessed_at' => $this->b_assessed_at,
            'op_submitted_at' => $this->op_submitted_at,
            'is_major' => $this->is_major,
        ]);

        $query->andFilterWhere(['like', 'major_a_just', $this->major_a_just])
            ->andFilterWhere(['like', 'major_b_just', $this->major_b_just]);

        return $dataProvider;
    }
    
    
    public function parse_evt_time($query, $event_date_time)
    {
            if (isset($this->event_date_time))
            {
                /*
                $query->andFilterWhere(['and',
                    'event_date_time BETWEEN "' . $this->event_date_time . '" AND "' . $this->event_date_time . ' 23:59:59"'
                    ]);
                 * 
                 */

                /* '#(\d{4}|\*{1})\-{1}(\d{2}|\*){1}\-{1}(\d{2}|\*{1})#' */

                /* case Y-m-d */
                if (preg_match_all('#(\d{4})\-{1}(\d{2}){1}\-{1}(\d{2})#', $this->event_date_time))
                {
                    /* 
                     * returns all the events in a given date 
                     * year month day set
                     */
                    $query->andFilterWhere(['and',
                        '{{event_declaration}}.[[event_date_time]] BETWEEN "' . $this->event_date_time . '" AND "' . $this->event_date_time . ' 23:59:59"'
                        ]);
                }
                else if (preg_match_all('#(\d{4})\-{1}(\d{2}){1}\-{1}(\*{1})#', $this->event_date_time))
                {
                    /* 
                     * returns all the events in a given year and month (day irrespective) 
                     * year month set
                     */

                    $parts = explode('-', $this->event_date_time);

                    $query->andFilterWhere(['and',
                        'year({{event_declaration}}.[[event_date_time]]) = ' . $parts[0],    
                        'month({{event_declaration}}.[[event_date_time]]) = ' . $parts[1]
                        ]);
                }
                elseif ((preg_match_all('#(\d{4})\-{1}(\*{1})\-{1}(\*{1})#', $this->event_date_time)))
                {
                    /* 
                     * returns all the events in a given year (month and day irrespective) 
                     * year set
                     */

                    $parts = explode('-', $this->event_date_time);

                    $query->andFilterWhere(['and',
                        'year({{event_declaration}}.[[event_date_time]]) = ' . $parts[0],    
                        ]);
                }
                elseif ((preg_match_all('#(\*{1})\-{1}(\d{2}){1}\-{1}(\*{1})#', $this->event_date_time)))
                {
                    /* 
                     * returns all the events in a given month (year and day irrespective) 
                     * year set
                     */

                    $parts = explode('-', $this->event_date_time);

                    $query->andFilterWhere(['and',
                        'month({{event_declaration}}.[[event_date_time]]) = ' . $parts[1],    
                        ]);
                }
                elseif ((preg_match_all('#(\*{1})\-{1}(\*){1}\-{1}(\d{2})#', $this->event_date_time)))
                {
                    /* 
                     * returns all the events in a given day (year and month irrespective) 
                     * year set
                     */

                    $parts = explode('-', $this->event_date_time);

                    $query->andFilterWhere(['and',
                        'day({{event_declaration}}.[[event_date_time]]) = ' . $parts[2],    
                        ]);
                }
                elseif ((preg_match_all('#(\*{1})\-{1}(\d{2}){1}\-{1}(\d{2})#', $this->event_date_time)))
                {
                    /* 
                     * returns all the events in a month on a particular day (year irrespective) 
                     * month day set
                     */
                    $parts = explode('-', $this->event_date_time);

                    $query->andFilterWhere(['and',
                        'month({{event_declaration}}.[[event_date_time]]) = ' . $parts[1],
                        'day({{event_declaration}}.[[event_date_time]]) = ' . $parts[2]    
                        ]);
                }
                elseif ((preg_match('#(\*{1})\-{1}(\*{1})\-{1}(\*{1})#', $this->event_date_time)))
                {
                    //echo 'select all';
                    //die();
                }
                else if (preg_match_all('#(\d{4})\-{1}(\*{1}){1}\-{1}(\d{2})#', $this->event_date_time))
                {
                    /* 
                     * returns all the events in a given year and day (month irrespective) 
                     * year day set
                     */

                    $parts = explode('-', $this->event_date_time);

                    $query->andFilterWhere(['and',
                        'year({{event_declaration}}.[[event_date_time]]) = ' . $parts[0],    
                        'day({{event_declaration}}.[[event_date_time]]) = ' . $parts[2]
                        ]);
                }

            }
        return $query;
    }    

    public function parse_operator($query, $val)
    {
        /* @var $query \yii\db\ActiveQuery */
        if (isset($val)) {
            $query->andWhere(['or',
                    ['like', '{{organizations}}.[[organization_name]]', $val],
                    ['like', '{{organizations}}.[[organization_acronym]]', $val],
                ]);
        }
        return $query;
        
    }

    public function parse_installation($query, $val)
    {
        if (trim($this->installation) != '')
        {
            $query->andFilterWhere(['or',
                ['like', 'installations.name', $this->installation],
                ['like', 'installations.type', $this->installation]
                ]);
        }
        
        return $query;
        
    }

    public function parse_event_type($query, $val)
    {
        
        if (isset($val))
        {
            //get rid of the _
            $val = str_replace('_', '', $val);
            
            $parts = [];
            $range = range('a', 'j');
            $types = explode(',', $val);
            foreach ($types as $type) {
                $t = strtolower(trim($type));
                if (in_array($t, $range))
                {
                    $parts = \yii\helpers\ArrayHelper::merge($parts,
                            ['{{event_classifications}}.[[is_' . $t . ']]' => 1]);
                }
                else
                {
                    echo $t;
                }
            }
            
            //echo print_r($parts);
            //die();
            
            $qf = ['or'];
            $qf[] = $parts;
            
            $qf2 = ['or', [
                'table.column' => 1
            ]
            ];
                    
            //echo '<pre>';
            //echo print_r($qf);
            //echo '<br/>';
            //echo print_r($qf2);
            //echo '</pre>';
            //die();
            
            $query->andFilterWhere([
                'and',
                  $qf
            ]);
            
        }
        return $query;
    }    
    
    
        }

/*20150920*/
