<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models;

use Yii;
use yii\web\IdentityInterface;
use app\models\behaviors\OperatorUserBehavior;
use app\models\behaviors\CaUserBehavior;
use app\models\behaviors\InstallationUserBehavior;
use app\models\admin\rbac\UserAssignment;
use app\models\admin\rbac\Role;

/**
 * This is the model class for table "auth_users".
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $role_in_organization
 * @property string $first_names
 * @property string $last_name
 * @property string $authKey
 * @property string $accessToken
 * @property integer $status
 * @property string $created_by
 * @property string $created_at
 * @property string $modified_at
 * @property string $login_timestamp
 * @property string $logout_timestamp
 * @property integer $org_id
 * @property string $phone
 * @property mixed $organization
 * @property string $full_name
 */
class User extends \yii\db\ActiveRecord implements IdentityInterface
{

    const STATUS_DELETED = 0;
    const STATUS_NEW = 1;
    const STATUS_ACTIVE = 10;
    const STATUS_REQUEST_CREATE = 100;
    const STATUS_REQUEST_DELETE = 101;
    
    public function behaviors() {
        //parent::behaviors();
            return [
                'operatorUser' => [
                    'class' => OperatorUserBehavior::className(),
                ],
                'installationUser' => [
                    'class' => InstallationUserBehavior::className(),
                ],
                'caUser' => [
                    'class' => CaUserBehavior::className(),
                ],
            ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'auth_users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        
            return [
                [['username', 'email', 'first_names', 'last_name', 'role_in_organization'], 'required', 'on'=>'default'],
                [['username', 'email', 'role_in_organization'], 'required', 'on'=>'default_admin'],
                [['org_id'], 'required', 'on'=>'default'],
                [['id', 'status'], 'integer', 'min'=>0],  //Author: cavesje Date:20151119
                [['role_in_organization', 'phone', 'created_at', 'modified_at', 'login_timestamp', 'org_id'], 'safe'],
                [['username', 'created_by'], 'string', 'max' => 45],
                [['first_names', 'last_name'], 'string', 'max' => 64],
                [['email'], 'string', 'max' => 128],
                [['email'], 'email'],
                [['password'], 'string', 'max' => 40],
                [['authKey', 'accessToken'], 'string', 'max' => 255],
                [['id', 'status'], 'required', 'on'=>'delete'],
            ];
        
//        if (!\Yii::$app->user->can('sys_admin')) {
//            return [
//                [['username', 'email', 'first_names', 'last_name', 'role_in_organization'], 'required'],
//                [['org_id'], 'required', 'on'=>'default'],
//                [['id', 'status'], 'integer', 'min'=>0],  //Author: cavesje Date:20151119
//                [['role_in_organization', 'phone', 'created_at', 'modified_at', 'login_timestamp', 'org_id'], 'safe'],
//                [['username', 'created_by'], 'string', 'max' => 45],
//                [['first_names', 'last_name'], 'string', 'max' => 64],
//                [['email'], 'string', 'max' => 128],
//                [['email'], 'email'],
//                [['password'], 'string', 'max' => 40],
//                [['authKey', 'accessToken'], 'string', 'max' => 255],
//                [['id', 'status'], 'required', 'on'=>'delete'],
//            ];
//        } else {
//            return [
//                [['username', 'email'], 'required'],
//                [['id', 'status'], 'integer', 'min'=>0],  //Author: cavesje Date:20151119
//                [['role_in_organization', 'phone', 'created_at', 'modified_at', 'login_timestamp', 'org_id'], 'safe'],
//                [['username', 'created_by'], 'string', 'max' => 45],
//                [['first_names', 'last_name'], 'string', 'max' => 64],
//                [['email'], 'string', 'max' => 128],
//                [['email'], 'email'],
//                [['password'], 'string', 'max' => 40],
//                [['authKey', 'accessToken'], 'string', 'max' => 255],
//                [['id', 'status'], 'required', 'on'=>'delete'],
//            ];
//        }
    }

    public function scenarios() {
        return \yii\helpers\ArrayHelper::merge(parent::scenarios(), ['delete', 'default_admin']);
    }
    
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'password' => 'Password',
            'operator' => 'Operator',
            'authKey' => 'Auth Key',
            'accessToken' => 'Access Token',
            'status' => 'Status',
            'created_by' => ucwords(Yii::t('app', 'created by')),
            'created_at' => ucwords(Yii::t('app', 'created at')),
            'modified_at' => ucwords(Yii::t('app', 'modified at')),
            
            'first_names' => \Yii::t('app', 'First name'),
            'last_name' => \Yii::t('app', 'Last name'),
            'role_in_organization' => \Yii::t('app', 'Role in organization'),
            'phone' => \Yii::t('app', 'Phone'),
            'email' => \Yii::t('app', 'Email'),
            
            
            'org_id' => Yii::t('app', 'Organization'),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        //return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
        /*
         * SyRIO modified to include the new user
         */
        return static::find()
                ->where(['id'=>$id])
                ->andWhere([
                    'or',
                    ['status' => self::STATUS_ACTIVE],
                    ['status' => self::STATUS_NEW]
                ])->one();
        
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        //return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
        
        /*
         * SyRIO modified to include the new user
         */
        return static::find()
                ->where(['username'=>$username])
                ->andWhere([
                    'or',
                    ['status' => self::STATUS_ACTIVE],
                    ['status' => self::STATUS_NEW]
                ])->one();
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int) end($parts);
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
      //Author: cavesje     Date: 2015.07.07
      //Decryption password
      //print 'Decryption ---------------------------------------';
      $decryp_password = $this->decryptPassword($password);
      //print var_dump($decryp_password);      
      $password = $decryp_password;
        
      return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    //Author: cavesje     Date: 2015.07.07
    //http://stackoverflow.com/questions/29509934/encrypt-with-cryptojs-and-decrypt-with-php
    // (vamanbo) changed to public to be accessible from change password 2015/04/09
    public function decryptPassword($password)
    {
      $key = pack('H*', "bcb04b7e103a0cd8b54763051cef08bc55abe029fdebae5e1d417e2ffb2a00a3");
      $ciphertext_dec = base64_decode($password);
      $iv_dec = pack('H*', "101112131415161718191a1b1c1d1e1f");
      $decryptedPassword = \mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, $ciphertext_dec, MCRYPT_MODE_CBC, $iv_dec);

      //http://www.rhadrix.net/?/Php/Come-rimuovere-i-caratteri-No-ASCII-da-una-stringa
      $decryptedPassword = preg_replace('/[^(\x20-\x7F)]*/','', $decryptedPassword);

      return trim($decryptedPassword);
    }
    
    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    public function getPassword() {
        return '';
    }
    
    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }
    
    
    
    
    public function getName()
    {
        return $this->username;
    }

    /**
     * returns a string with the full name of the user
     */
    public function getFull_name()
    {
        return trim($this->first_names) . ' ' . trim(strtoupper($this->last_name));
    }
    
    
    public function getEmail()
    {
        return $this->email;
    }

    public function getRoleInOrganization()
    {
        return $this->role_in_organization;
    }
    
    
    /**
     * Returns the roles of the current user
     */
    public function getRoles()
    {
        $auth = Yii::$app->getAuthManager();
        //$roles = $auth->getRolesByUser(Yii::$app->user->getId());
        $roles = $auth->getRolesByUser($this->getId());
        
        $retRoles = [];
        foreach ($roles as $role)
        {
            $retRoles[$role->name] = $role->description;
        }
        return $retRoles;
    }
    
    public function getIsCaUser()
    {
        //$n = isset($this->country2) && $this->country2!= 'XX';
        //return $n;
        
        //return Yii::$app->user->can('ca_admin');
        
        if (!isset($this->organization))
        {
            return false;
        }
        else
        {
            return $this->organization->organizationType == \app\modules\management\models\Organization::COMPETENT_AUTHORITY;
        }
    }    

    public function getIsOperatorUser()
    {
        if (!isset($this->organization))
        {
            return false;
        }
        else
        {
            return $this->organization->organizationType == \app\modules\management\models\Organization::OPERATORS_OWNERS && !isset($this->inst_id);
        }
    }    

    public function getIsInstallationUser()
    {
        if (!isset($this->organization))
        {
            return false;
        }
        else
        {
            return isset($this->inst_id);
            //return $this->organization->organizationType == \app\modules\management\models\Organization::INSTALLATION;
        }
    }    
    
    public function AssignmentNames() {
        return [];
    }
    
    //private $_assignmentNames = null;
    public function getAssignmentName($assignment_code)
    {
        //if (!isset($this->_assignmentNames)) {
        //    $this->_assignmentNames = self::AssignmentNames();
        //}
        $_assignmentNames = $this->AssignmentNames();
        if (key_exists($assignment_code, $_assignmentNames)) {
            return $_assignmentNames[$assignment_code];
        }
        else
        {
            return $assignment_code;
        }
    }
    
    /**
     * 
     * @return array[UserAssignment]
     */
    public function getAssignments() {
        return $this->hasMany(UserAssignment::className(), ['user_id'=>"id"]);
    }
    
    /**
     * get the current user's roles
     * 
     * @return array[string] the actual role names
     */
    public function getCurrentRoles()
    {   
        $current_assignments = [];
        foreach($this->assignments as $assignment) 
        {
            $current_assignments[] = $assignment->item_name;
        }
        return $current_assignments;
    }
    
    /**
     * 
     * @return mixed string the possible roles this user can be
     */
    public function getPossibleRoles()
    {
        $roleNames = [];
        $roleObj = new Role();
        if (Yii::$app->user->can('sys_admin')) {
            $roleNames = \yii\helpers\ArrayHelper::merge($roleNames, $roleObj->getCaRoles());
            $roleNames = \yii\helpers\ArrayHelper::merge($roleNames, $roleObj->getOoRoles());
            $roleNames = \yii\helpers\ArrayHelper::merge($roleNames, $roleObj->getInstallationRoles());
        }
        elseif ($this->isCaUser) {
            //return $roleObj->getCaRoles();
            $roleNames = \yii\helpers\ArrayHelper::merge($roleNames, $roleObj->getCaRoles());
            $roleNames = \yii\helpers\ArrayHelper::merge($roleNames, $roleObj->getOoRoles());
            $roleNames = \yii\helpers\ArrayHelper::merge($roleNames, $roleObj->getInstallationRoles());
        }
        elseif ($this->isOperatorUser) {
            //return $roleObj->getOoRoles();
            $roleNames = \yii\helpers\ArrayHelper::merge($roleNames, $roleObj->getOoRoles());
            $roleNames = \yii\helpers\ArrayHelper::merge($roleNames, $roleObj->getInstallationRoles());
        }
        elseif ($this->isInstallationUser) {
            $roleNames = \yii\helpers\ArrayHelper::merge($roleNames, $roleObj->getInstallationRoles());
        }
        return $roleNames;
    }
    
    /**
     * The Organization (CA, OO, etc) of the current user
     * 
     * @return mixed the Organization of the user
     */
    public function getOrganization()
    {
        if (isset($this->org_id) && strlen($this->org_id)>2)
        {
            /* TODO SyRIO: get other types of organization based on other initial digits */
            
            //get the first two characters
            $twoChars = strtoupper(substr($this->org_id, 0, 2));
            switch ($twoChars)
            {
                case 'CA':
                    return $this->hasOne(\app\modules\management\models\Organization::className(), ['id'=>'org_id']);
                    break;
                case 'OO':
                    return $this->hasOne(\app\modules\management\models\Organization::className(), ['id'=>'org_id']);
                    break;
            }
        }
        else
        {
            return NULL;
        }
    }

    /**
     * The Installation (if the user is Installation user) of the current user
     * 
     * @return app\models\Installations the Installation of the user
     */
    public function getInstallation()
    {
        if (isset($this->inst_id) && strlen($this->inst_id)>2)
        {
            //get the first two characters
            $twoChars = strtoupper(substr($this->org_id, 0, 2));
            return $this->hasOne(Installations::className(), ['id'=>'inst_id']);            
        }
        else
        {
            return NULL;
        }
    }
    
    /*
     * (vamanbo) 2015-09-04
     * Check this one out! shouldn't be here!
     */
    private $_myRoles=[];
    public function getMyRoles()
    {
        
        $this->_myRoles[] = 'ca_admin';
        return $this->_myRoles;
    }
    
    /*
     * (vamanbo) 2015-09-04
     * Check this one out! shouldn't be here!
     */
    public function setMyRoles($arr)
    {
        $this->myRoles=$arr;
    }

    /*
     * (vamanbo) 2015-09-04
     * Check this one out! shouldn't be here!
     */
    public function getAllRoles()
    {
        return [
            'ca_admin'=> 'CA Administrator',
            'ca_user' => 'CA user',
            'ca_assessor' => 'CA assessor'
        ];
    }
    
    }
