<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\cpf;

use Yii;

/**
 * This is the model class for table "cpf_section_4".
 *
 * @property integer $year
 * @property integer $s4_1_n_events_total
 * @property integer $s4_1_n_major_accidents
 * @property integer $s4_2_a_total
 * @property integer $s4_2_a_if
 * @property integer $s4_2_a_ix
 * @property integer $s4_2_a_nig
 * @property integer $s4_2_a_nio
 * @property integer $s4_2_a_haz
 * @property integer $s4_2_b_total
 * @property integer $s4_2_b_bo
 * @property integer $s4_2_b_bda
 * @property integer $s4_2_b_wbf
 * @property integer $s4_2_c_total
 * @property integer $s4_2_d_total
 * @property integer $s4_2_d_si
 * @property integer $s4_2_d_sb
 * @property integer $s4_2_d_sk
 * @property integer $s4_2_e_total
 * @property integer $s4_2_f_total
 * @property integer $s4_2_g_total
 * @property integer $s4_2_h_total
 * @property integer $s4_2_i_total
 * @property integer $s4_2_j_total
 * @property integer $s4_3_total
 * @property integer $s4_3_fatalities
 * @property integer $s4_3_inj
 * @property integer $s4_3_sinj
 * @property integer $s4_4_nsis
 * @property integer $s4_4_npcs
 * @property integer $s4_4_nics
 * @property integer $s4_4_nds
 * @property integer $s4_4_npcrs
 * @property integer $s4_4_nps
 * @property integer $s4_4_nsds
 * @property integer $s4_4_nnavaids
 * @property integer $s4_4_roteq
 * @property integer $s4_4_eere
 * @property integer $s4_4_ncoms
 * @property integer $s4_4_nother
 * @property integer $s4_5_a_total
 * @property integer $s4_5_a_df
 * @property integer $s4_5_a_co__int
 * @property integer $s4_5_a_co__ext
 * @property integer $s4_5_a_mf__f
 * @property integer $s4_5_a_mf__wo
 * @property integer $s4_5_a_mf__dm
 * @property integer $s4_5_a_mf__vh
 * @property integer $s4_5_a_if
 * @property integer $s4_5_a_csf
 * @property integer $s4_5_a_other
 * @property integer $s4_5_b_total
 * @property integer $s4_5_b_err__op
 * @property integer $s4_5_b_err__mnt
 * @property integer $s4_5_b_err__tst
 * @property integer $s4_5_b_err__insp
 * @property integer $s4_5_b_err__dsgn
 * @property integer $s4_5_b_other
 * @property integer $s4_5_c_total
 * @property integer $s4_5_c_inq__rap
 * @property integer $s4_5_c_inq__instp
 * @property integer $s4_5_c_nc__prc
 * @property integer $s4_5_c_nc__ptw
 * @property integer $s4_5_c_inq__com
 * @property integer $s4_5_c_inq__pc
 * @property integer $s4_5_c_inq__sup
 * @property integer $s4_5_c_inq__safelead
 * @property integer $s4_5_c_other
 * @property integer $s4_5_d_total
 * @property integer $s4_5_d_exc__design__wind
 * @property integer $s4_5_d_exc__design__wave
 * @property integer $s4_5_d_exc__design__lowvis
 * @property integer $s4_5_d_ice__icebergs
 * @property integer $s4_5_d_other
 * @property string $s4_6
 */
class CpfSection4 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cpf_section_4';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['year'], 'required'],
            [['year', 's4_1_n_events_total', 's4_1_n_major_accidents', 's4_2_a_total', 's4_2_a_if', 's4_2_a_ix', 's4_2_a_nig', 's4_2_a_nio', 's4_2_a_haz', 's4_2_b_total', 's4_2_b_bo', 's4_2_b_bda', 's4_2_b_wbf', 's4_2_c_total', 's4_2_d_total', 's4_2_d_si', 's4_2_d_sb', 's4_2_d_sk', 's4_2_e_total', 's4_2_f_total', 's4_2_g_total', 's4_2_h_total', 's4_2_i_total', 's4_2_j_total', 's4_3_total', 's4_3_fatalities', 's4_3_inj', 's4_3_sinj', 's4_4_nsis', 's4_4_npcs', 's4_4_nics', 's4_4_nds', 's4_4_npcrs', 's4_4_nps', 's4_4_nsds', 's4_4_nnavaids', 's4_4_roteq', 's4_4_eere', 's4_4_ncoms', 's4_4_nother', 's4_5_a_total', 's4_5_a_df', 's4_5_a_co__int', 's4_5_a_co__ext', 's4_5_a_mf__f', 's4_5_a_mf__wo', 's4_5_a_mf__dm', 's4_5_a_mf__vh', 's4_5_a_if', 's4_5_a_csf', 's4_5_a_other', 's4_5_b_total', 's4_5_b_err__op', 's4_5_b_err__mnt', 's4_5_b_err__tst', 's4_5_b_err__insp', 's4_5_b_err__dsgn', 's4_5_b_other', 's4_5_c_total', 's4_5_c_inq__rap', 's4_5_c_inq__instp', 's4_5_c_nc__prc', 's4_5_c_nc__ptw', 's4_5_c_inq__com', 's4_5_c_inq__pc', 's4_5_c_inq__sup', 's4_5_c_inq__safelead', 's4_5_c_other', 's4_5_d_total', 's4_5_d_exc__design__wind', 's4_5_d_exc__design__wave', 's4_5_d_exc__design__lowvis', 's4_5_d_ice__icebergs', 's4_5_d_other'], 'integer', 'min'=>0],  //Author: cavesje Date:20151119
            [['s4_6'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'year' => Yii::t('app/cpf', 'Year'),
            's4_1_n_events_total' => Yii::t('app/cpf', 'Number of reportable events pursuant to Annex IX'),
            's4_1_n_major_accidents' => Yii::t('app/cpf', 'Major accidents'),
            's4_2_a_total' => Yii::t('app/cpf', 'Ignited oil/gas releases - Total'),
            's4_2_a_if' => Yii::t('app/cpf', 'Ignited oil/gas releases - Fires'),
            's4_2_a_ix' => Yii::t('app/cpf', 'Ignited oil/gas releases - Explosions'),
            's4_2_a_nig' => Yii::t('app/cpf', 'Non ignited gas releases'),
            's4_2_a_nio' => Yii::t('app/cpf', 'Non-ignited oil releases'),
            's4_2_a_haz' => Yii::t('app/cpf', 'Hazardous substances released'),
            's4_2_b_total' => Yii::t('app/cpf', 'Loss of well control - Total'),
            's4_2_b_bo' => Yii::t('app/cpf', 'Blowouts'),
            's4_2_b_bda' => Yii::t('app/cpf', 'Blowout / diverter activation'),
            's4_2_b_wbf' => Yii::t('app/cpf', 'Well barrier failures'),
            's4_2_c_total' => Yii::t('app/cpf', 'Failres of SECE\'s'),
            's4_2_d_total' => Yii::t('app/cpf', 'Loss of structural integrity - total'),
            's4_2_d_si' => Yii::t('app/cpf', 'Loss of structural integrity'),
            's4_2_d_sb' => Yii::t('app/cpf', 'Loss of stability/ buoyancy'),
            's4_2_d_sk' => Yii::t('app/cpf', 'Loss of station keeping'),
            's4_2_e_total' => Yii::t('app/cpf', 'Vesel collisions - total'),
            's4_2_f_total' => Yii::t('app/cpf', 'Helicopter accidents'),
            's4_2_g_total' => Yii::t('app/cpf', 'Fatal accidents'),
            's4_2_h_total' => Yii::t('app/cpf', 'Serious injuries of 5 or more persons in the same accident'),
            's4_2_i_total' => Yii::t('app/cpf', 'Evacuation of personnel'),
            's4_2_j_total' => Yii::t('app/cpf', 'Environmental accidents'),
            's4_3_total' => Yii::t('app/cpf', 'Total number of fatalities and injuries'),
            's4_3_fatalities' => Yii::t('app/cpf', 'Number of fatalities'),
            's4_3_inj' => Yii::t('app/cpf', 'Total number of injuries'),
            's4_3_sinj' => Yii::t('app/cpf', 'Total number of serous injuries'),
            's4_4_nsis' => Yii::t('app/cpf', 'Structural integrity systems'),
            's4_4_npcs' => Yii::t('app/cpf', 'Process containment systems'),
            's4_4_nics' => Yii::t('app/cpf', 'Ignition control systems'),
            's4_4_nds' => Yii::t('app/cpf', 'Detection control systems'),
            's4_4_npcrs' => Yii::t('app/cpf', 'Process containment relief systems'),
            's4_4_nps' => Yii::t('app/cpf', 'Protection systems'),
            's4_4_nsds' => Yii::t('app/cpf', 'Shutdown systems'),
            's4_4_nnavaids' => Yii::t('app/cpf', 'Navigational aids'),
            's4_4_roteq' => Yii::t('app/cpf', 'Rotation equipment'),
            's4_4_eere' => Yii::t('app/cpf', 'Escape, evacuation and rescue equipment'),
            's4_4_ncoms' => Yii::t('app/cpf', 'Communication systems'),
            's4_4_nother' => Yii::t('app/cpf', 'other'),
            's4_5_a_total' => Yii::t('app/cpf', 'Incidents caused by Equipment failure - Total'),
            's4_5_a_df' => Yii::t('app/cpf', 'Design failures'),
            's4_5_a_co__int' => Yii::t('app/cpf', 'Internal corrosion'),
            's4_5_a_co__ext' => Yii::t('app/cpf', 'External corrosion'),
            's4_5_a_mf__f' => Yii::t('app/cpf', 'Mechanical failure due to fatigue'),
            's4_5_a_mf__wo' => Yii::t('app/cpf', 'Mechanical failures due to wear-out'),
            's4_5_a_mf__dm' => Yii::t('app/cpf', 'Mechanical failures due to defected material'),
            's4_5_a_mf__vh' => Yii::t('app/cpf', 'Mechanical failures (vessel/helicopter)'),
            's4_5_a_if' => Yii::t('app/cpf', 'Instrument failures'),
            's4_5_a_csf' => Yii::t('app/cpf', 'Control system failures'),
            's4_5_a_other' => Yii::t('app/cpf', 'other'),
            's4_5_b_total' => Yii::t('app/cpf', 'Human error-operational causes - total'),
            's4_5_b_err__op' => Yii::t('app/cpf', 'Operation error'),
            's4_5_b_err__mnt' => Yii::t('app/cpf', 'Maintenance error'),
            's4_5_b_err__tst' => Yii::t('app/cpf', 'Testing error'),
            's4_5_b_err__insp' => Yii::t('app/cpf', 'Inspection error'),
            's4_5_b_err__dsgn' => Yii::t('app/cpf', 'Design error'),
            's4_5_b_other' => Yii::t('app/cpf', 'Other'),
            's4_5_c_total' => Yii::t('app/cpf', 'Procedural / organizational error'),
            's4_5_c_inq__rap' => Yii::t('app/cpf', ' Inadequate risk assessment/perception'),
            's4_5_c_inq__instp' => Yii::t('app/cpf', 'Inadequate instruction/procedure'),
            's4_5_c_nc__prc' => Yii::t('app/cpf', 'Non-compliance with procedure'),
            's4_5_c_nc__ptw' => Yii::t('app/cpf', 'Non-compliance with permit-to-work'),
            's4_5_c_inq__com' => Yii::t('app/cpf', 'Inadequate communication'),
            's4_5_c_inq__pc' => Yii::t('app/cpf', ' Inadequate personnel competence'),
            's4_5_c_inq__sup' => Yii::t('app/cpf', ' Inadequate supervision'),
            's4_5_c_inq__safelead' => Yii::t('app/cpf', 'Inadequate safety leadership'),
            's4_5_c_other' => Yii::t('app/cpf', 'other'),
            's4_5_d_total' => Yii::t('app/cpf', 'Weather-related causes - total'),
            's4_5_d_exc__design__wind' => Yii::t('app/cpf', 'Wind in excess of limits of design'),
            's4_5_d_exc__design__wave' => Yii::t('app/cpf', 'Waves in excess of limits of design'),
            's4_5_d_exc__design__lowvis' => Yii::t('app/cpf', ' Extremely low visibility in excess of limits of design'),
            's4_5_d_ice__icebergs' => Yii::t('app/cpf', 'Presence of ice or icebergs'),
            's4_5_d_other' => Yii::t('app/cpf', 'other'),
            's4_6' => Yii::t('app/cpf', 'Most important lessons learned'),
        ];
    }

    
    /**
     * Returns the array of properties that are inferred from CRFs
     */
    public static function attributesInferred()
    {
        return [
            'year' => Yii::t('app/cpf', 'Year'),
            's4_1_n_events_total' => Yii::t('app/cpf', 'Number of reportable events pursuant to Annex IX'),
            's4_1_n_major_accidents' => Yii::t('app/cpf', 'Major accidents'),
            's4_2_a_total' => Yii::t('app/cpf', 'Ignited oil/gas releases - Total'),
            's4_2_a_if' => Yii::t('app/cpf', 'Ignited oil/gas releases - Fires'),
            's4_2_a_ix' => Yii::t('app/cpf', 'Ignited oil/gas releases - Explosions'),
            's4_2_a_nig' => Yii::t('app/cpf', 'Non ignited gas releases'),
            's4_2_a_nio' => Yii::t('app/cpf', 'Non-ignited oil releases'),
            's4_2_a_haz' => Yii::t('app/cpf', 'Hazardous substances released'),
            's4_2_b_total' => Yii::t('app/cpf', 'Loss of well control - Total'),
            //'s4_2_b_bo' => Yii::t('app/cpf', 'Blowouts'),
            's4_2_b_bda' => Yii::t('app/cpf', 'Blowout / diverter activation'),
            's4_2_b_wbf' => Yii::t('app/cpf', 'Well barrier failures'),
            's4_2_c_total' => Yii::t('app/cpf', 'Failres of SECE\'s'),
            //'s4_2_d_total' => Yii::t('app/cpf', 'Loss of structural integrity - total'),
            //'s4_2_d_si' => Yii::t('app/cpf', 'Loss of structural integrity'),
            //'s4_2_d_sb' => Yii::t('app/cpf', 'Loss of stability/ buoyancy'),
            //'s4_2_d_sk' => Yii::t('app/cpf', 'Loss of station keeping'),
            //'s4_2_e_total' => Yii::t('app/cpf', 'Vesel collisions - total'),
            //'s4_2_f_total' => Yii::t('app/cpf', 'Helicopter accidents'),
            //'s4_2_g_total' => Yii::t('app/cpf', 'Fatal accidents'),
            //'s4_2_h_total' => Yii::t('app/cpf', 'Serious injuries of 5 or more persons in the same accident'),
            's4_2_i_total' => Yii::t('app/cpf', 'Evacuation of personnel'),
            's4_2_j_total' => Yii::t('app/cpf', 'Environmental accidents'),
            
            //'s4_3_total' => Yii::t('app/cpf', 'Total number of fatalities and injuries'),
            //'s4_3_fatalities' => Yii::t('app/cpf', 'Number of fatalities'),
            //'s4_3_inj' => Yii::t('app/cpf', 'Total number of injuries'),
            //'s4_3_sinj' => Yii::t('app/cpf', 'Total number of serous injuries'),
            //'s4_4_nsis' => Yii::t('app/cpf', 'Structural integrity systems'),
            
            's4_4_npcs' => Yii::t('app/cpf', 'Process containment systems'),
            's4_4_nics' => Yii::t('app/cpf', 'Ignition control systems'),
            's4_4_nds' => Yii::t('app/cpf', 'Detection control systems'),
            's4_4_npcrs' => Yii::t('app/cpf', 'Process containment relief systems'),
            's4_4_nps' => Yii::t('app/cpf', 'Protection systems'),
            's4_4_nsds' => Yii::t('app/cpf', 'Shutdown systems'),
            's4_4_nnavaids' => Yii::t('app/cpf', 'Navigational aids'),
            's4_4_roteq' => Yii::t('app/cpf', 'Rotation equipment'),
            's4_4_eere' => Yii::t('app/cpf', 'Escape, evacuation and rescue equipment'),
            's4_4_ncoms' => Yii::t('app/cpf', 'Communication systems'),
            's4_4_nother' => Yii::t('app/cpf', 'other'),
            's4_5_a_total' => Yii::t('app/cpf', 'Incidents caused by Equipment failure - Total'),
            
            's4_5_a_df' => Yii::t('app/cpf', 'Design failures'),
            's4_5_a_co__int' => Yii::t('app/cpf', 'Internal corrosion'),
            's4_5_a_co__ext' => Yii::t('app/cpf', 'External corrosion'),
            's4_5_a_mf__f' => Yii::t('app/cpf', 'Mechanical failure due to fatigue'),
            's4_5_a_mf__wo' => Yii::t('app/cpf', 'Mechanical failures due to wear-out'),
            's4_5_a_mf__dm' => Yii::t('app/cpf', 'Mechanical failures due to defected material'),
            's4_5_a_mf__vh' => Yii::t('app/cpf', 'Mechanical failures (vessel/helicopter)'),
            's4_5_a_if' => Yii::t('app/cpf', 'Instrument failures'),
            's4_5_a_csf' => Yii::t('app/cpf', 'Control system failures'),
            's4_5_a_other' => Yii::t('app/cpf', 'other'),
            
            's4_5_b_total' => Yii::t('app/cpf', 'Human error-operational causes - total'),
            's4_5_b_err__op' => Yii::t('app/cpf', 'Operation error'),
            's4_5_b_err__mnt' => Yii::t('app/cpf', 'Maintenance error'),
            's4_5_b_err__tst' => Yii::t('app/cpf', 'Testing error'),
            's4_5_b_err__insp' => Yii::t('app/cpf', 'Inspection error'),
            //'s4_5_b_err__dsgn' => Yii::t('app/cpf', 'Design error'),
            's4_5_b_other' => Yii::t('app/cpf', 'Other'),
            
            //'s4_5_c_total' => Yii::t('app/cpf', 'Procedural / organizational error'),
            //'s4_5_c_inq__rap' => Yii::t('app/cpf', ' Inadequate risk assessment/perception'),
            //'s4_5_c_inq__instp' => Yii::t('app/cpf', 'Inadequate instruction/procedure'),
            's4_5_c_nc__prc' => Yii::t('app/cpf', 'Non-compliance with procedure'),
            's4_5_c_nc__ptw' => Yii::t('app/cpf', 'Non-compliance with permit-to-work'),
            //'s4_5_c_inq__com' => Yii::t('app/cpf', 'Inadequate communication'),
            //'s4_5_c_inq__pc' => Yii::t('app/cpf', ' Inadequate personnel competence'),
            //'s4_5_c_inq__sup' => Yii::t('app/cpf', ' Inadequate supervision'),
            //'s4_5_c_inq__safelead' => Yii::t('app/cpf', 'Inadequate safety leadership'),
            's4_5_c_other' => Yii::t('app/cpf', 'other'),
            
            //'s4_5_d_total' => Yii::t('app/cpf', 'Weather-related causes - total'),
            //'s4_5_d_exc__design__wind' => Yii::t('app/cpf', 'Wind in excess of limits of design'),
            //'s4_5_d_exc__design__wave' => Yii::t('app/cpf', 'Waves in excess of limits of design'),
            //'s4_5_d_exc__design__lowvis' => Yii::t('app/cpf', ' Extremely low visibility in excess of limits of design'),
            //'s4_5_d_ice__icebergs' => Yii::t('app/cpf', 'Presence of ice or icebergs'),
            //'s4_5_d_other' => Yii::t('app/cpf', 'other'),
            //'s4_6' => Yii::t('app/cpf', 'Most important lessons learned'),
        ];
    }
    
    /**
     * 
     * @param string $prop the name of the property
     * @return integer
     */
    public static function getIsInferred($prop) {
        return key_exists($prop, self::attributesInferred()) ? 1 : 0;
    }
    
    
}
