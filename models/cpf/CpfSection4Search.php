<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\cpf;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\cpf\CpfSection4;

/**
 * CpfSection4Search represents the model behind the search form about `app\models\cpf\CpfSection4`.
 */
class CpfSection4Search extends CpfSection4
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['year', 's4_1_n_events_total', 's4_1_n_major_accidents', 's4_2_a_total', 's4_2_a_if', 's4_2_a_ix', 's4_2_a_nig', 's4_2_a_nio', 's4_2_a_haz', 's4_2_b_total', 's4_2_b_bo', 's4_2_b_bda', 's4_2_b_wbf', 's4_2_c_total', 's4_2_d_total', 's4_2_d_si', 's4_2_d_sb', 's4_2_d_sk', 's4_2_e_total', 's4_2_f_total', 's4_2_g_total', 's4_2_h_total', 's4_2_i_total', 's4_2_j_total', 's4_3_total', 's4_3_fatalities', 's4_3_inj', 's4_3_sinj', 's4_4_nsis', 's4_4_npcs', 's4_4_nics', 's4_4_nds', 's4_4_npcrs', 's4_4_nps', 's4_4_nsds', 's4_4_nnavaids', 's4_4_roteq', 's4_4_eere', 's4_4_ncoms', 's4_4_nother', 's4_5_a_total', 's4_5_a_df', 's4_5_a_co__int', 's4_5_a_co__ext', 's4_5_a_mf__f', 's4_5_a_mf__wo', 's4_5_a_mf__dm', 's4_5_a_mf__vh', 's4_5_a_if', 's4_5_a_csf', 's4_5_a_other', 's4_5_b_total', 's4_5_b_err__op', 's4_5_b_err__mnt', 's4_5_b_err__tst', 's4_5_b_err__insp', 's4_5_b_err__dsgn', 's4_5_b_other', 's4_5_c_total', 's4_5_c_inq__rap', 's4_5_c_inq__instp', 's4_5_c_nc__prc', 's4_5_c_nc__ptw', 's4_5_c_inq__com', 's4_5_c_inq__pc', 's4_5_c_inq__sup', 's4_5_c_inq__safelead', 's4_5_c_other', 's4_5_d_total', 's4_5_d_exc__design__wind', 's4_5_d_exc__design__wave', 's4_5_d_exc__design__lowvis', 's4_5_d_ice__icebergs', 's4_5_d_other'], 'integer', 'min'=>0],   //Author: cavesje Date:20151119
            [['s4_6'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CpfSection4::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'year' => $this->year,
            's4_1_n_events_total' => $this->s4_1_n_events_total,
            's4_1_n_major_accidents' => $this->s4_1_n_major_accidents,
            's4_2_a_total' => $this->s4_2_a_total,
            's4_2_a_if' => $this->s4_2_a_if,
            's4_2_a_ix' => $this->s4_2_a_ix,
            's4_2_a_nig' => $this->s4_2_a_nig,
            's4_2_a_nio' => $this->s4_2_a_nio,
            's4_2_a_haz' => $this->s4_2_a_haz,
            's4_2_b_total' => $this->s4_2_b_total,
            's4_2_b_bo' => $this->s4_2_b_bo,
            's4_2_b_bda' => $this->s4_2_b_bda,
            's4_2_b_wbf' => $this->s4_2_b_wbf,
            's4_2_c_total' => $this->s4_2_c_total,
            's4_2_d_total' => $this->s4_2_d_total,
            's4_2_d_si' => $this->s4_2_d_si,
            's4_2_d_sb' => $this->s4_2_d_sb,
            's4_2_d_sk' => $this->s4_2_d_sk,
            's4_2_e_total' => $this->s4_2_e_total,
            's4_2_f_total' => $this->s4_2_f_total,
            's4_2_g_total' => $this->s4_2_g_total,
            's4_2_h_total' => $this->s4_2_h_total,
            's4_2_i_total' => $this->s4_2_i_total,
            's4_2_j_total' => $this->s4_2_j_total,
            's4_3_total' => $this->s4_3_total,
            's4_3_fatalities' => $this->s4_3_fatalities,
            's4_3_inj' => $this->s4_3_inj,
            's4_3_sinj' => $this->s4_3_sinj,
            's4_4_nsis' => $this->s4_4_nsis,
            's4_4_npcs' => $this->s4_4_npcs,
            's4_4_nics' => $this->s4_4_nics,
            's4_4_nds' => $this->s4_4_nds,
            's4_4_npcrs' => $this->s4_4_npcrs,
            's4_4_nps' => $this->s4_4_nps,
            's4_4_nsds' => $this->s4_4_nsds,
            's4_4_nnavaids' => $this->s4_4_nnavaids,
            's4_4_roteq' => $this->s4_4_roteq,
            's4_4_eere' => $this->s4_4_eere,
            's4_4_ncoms' => $this->s4_4_ncoms,
            's4_4_nother' => $this->s4_4_nother,
            's4_5_a_total' => $this->s4_5_a_total,
            's4_5_a_df' => $this->s4_5_a_df,
            's4_5_a_co__int' => $this->s4_5_a_co__int,
            's4_5_a_co__ext' => $this->s4_5_a_co__ext,
            's4_5_a_mf__f' => $this->s4_5_a_mf__f,
            's4_5_a_mf__wo' => $this->s4_5_a_mf__wo,
            's4_5_a_mf__dm' => $this->s4_5_a_mf__dm,
            's4_5_a_mf__vh' => $this->s4_5_a_mf__vh,
            's4_5_a_if' => $this->s4_5_a_if,
            's4_5_a_csf' => $this->s4_5_a_csf,
            's4_5_a_other' => $this->s4_5_a_other,
            's4_5_b_total' => $this->s4_5_b_total,
            's4_5_b_err__op' => $this->s4_5_b_err__op,
            's4_5_b_err__mnt' => $this->s4_5_b_err__mnt,
            's4_5_b_err__tst' => $this->s4_5_b_err__tst,
            's4_5_b_err__insp' => $this->s4_5_b_err__insp,
            's4_5_b_err__dsgn' => $this->s4_5_b_err__dsgn,
            's4_5_b_other' => $this->s4_5_b_other,
            's4_5_c_total' => $this->s4_5_c_total,
            's4_5_c_inq__rap' => $this->s4_5_c_inq__rap,
            's4_5_c_inq__instp' => $this->s4_5_c_inq__instp,
            's4_5_c_nc__prc' => $this->s4_5_c_nc__prc,
            's4_5_c_nc__ptw' => $this->s4_5_c_nc__ptw,
            's4_5_c_inq__com' => $this->s4_5_c_inq__com,
            's4_5_c_inq__pc' => $this->s4_5_c_inq__pc,
            's4_5_c_inq__sup' => $this->s4_5_c_inq__sup,
            's4_5_c_inq__safelead' => $this->s4_5_c_inq__safelead,
            's4_5_c_other' => $this->s4_5_c_other,
            's4_5_d_total' => $this->s4_5_d_total,
            's4_5_d_exc__design__wind' => $this->s4_5_d_exc__design__wind,
            's4_5_d_exc__design__wave' => $this->s4_5_d_exc__design__wave,
            's4_5_d_exc__design__lowvis' => $this->s4_5_d_exc__design__lowvis,
            's4_5_d_ice__icebergs' => $this->s4_5_d_ice__icebergs,
            's4_5_d_other' => $this->s4_5_d_other,
        ]);

        $query->andFilterWhere(['like', 's4_6', $this->s4_6]);

        return $dataProvider;
    }
}
