<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\common;

use Yii;
use yii\base\Model;

/**
 * ActionMessage
 * 
 * A simple model that allows setting error or success messages in session flash / variables
 * 
 * @property int $success the result of the action;  can be 0,1,2 (ActionMessage::constant)
 * @property string $action the name of the action performed
 * @property string $msg the message accompanying the action result
 */
class ActionMessage extends Model
{
    const SUCCESS = 0;
    const ERROR = 2;
    const WARNING = 1;

    public function __construct($config = array()) {
        parent::__construct($config);
        array_key_exists('success', $config) ? $this->success == $config['success'] : '';
        array_key_exists('msg', $config) ? $this->msg == $config['msg'] : '';
        array_key_exists('action', $config) ? $this->action == $config['action'] : '';
    }
    
    
    /*
     * @property mixed success true if action successful
     */
    public $success;
    
    public $action = '';
    
    public $msg = '';
    
    /**
     * SetSuccess
     * 
     * sets the status of this action as ActionMessage::SUCCESS
     * 
     * @param string $action
     * @param type $msg
     */
    public function SetSuccess($action, $msg)
    {
        $this->success = self::SUCCESS;
        $this->msg=$msg;
        $this->action = $action;
    }
    
    public function rules() {
        
        return [
            [['success', 'action', 'msg'], 'safe'],
        ];
        
    }
    
    /**
     * SetWarning
     * 
     * sets the status of this action as ActionMessage::WARNING
     * 
     * @param string $action
     * @param type $msg
     */
    public function SetWarning($action, $msg)
    {
        $this->success = self::WARNING;
        $this->msg=$msg;
        $this->action = $action;
    }    
    
    /**
     * SetError
     * 
     * sets the status of this action as ActionMessage::ERROR
     * 
     * @param string $action
     * @param type $msg
     */
    public function SetError($action, $msg)
    {
        $this->success = self::ERROR;
        $this->msg=$msg;
        $this->action = $action;
    }    
}
