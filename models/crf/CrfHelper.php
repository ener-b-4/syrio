<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\crf;

use Yii;



/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CrfHelper
 *
 * @author vamanbo
 */
class CrfHelper {

    const SC_NotApplicable = -9999;
    const SC_Unknown = -9998;
    
    public static function SectionsTitle($lang = null) {
        
        if (!isset($lang)) {
            $lang = \Yii::$app->language;
        }
        
        return [
            'deadline' => \Yii::t('app/crf', 'within {evt} working days from the event', ['evt'=>'10'], $lang),
            'A' => \Yii::t('app/crf', 'Unintended release of oil, gas or other hazardous substances, whether or not ignited', null, $lang),
            'A.1' => \Yii::t('app/crf', 'Was there a release of hydrocarbon substances?', null, $lang),
            'A.1.1' => \Yii::t('app/crf', 'Hydrocarbon (HC) released', null, $lang),
            'A.1.2' => \Yii::t('app/crf', 'Estimated quantity released', null, $lang),
            'A.1.3' => \Yii::t('app/crf', 'Estimated initial release rate', null, $lang),
            'A.1.4' => \Yii::t('app/crf', 'Duration of leak', null, $lang),
            'A.1.5' => \Yii::t('app/crf', 'Location of leak', null, $lang),
            'A.1.6' => \Yii::t('app/crf', 'Hazardous area classification', null, $lang),
            'A.1.7' => \Yii::t('app/crf', 'Module ventilation', null, $lang),
            'A.1.8' => \Yii::t('app/crf', 'Weather conditions', null, $lang),
            'A.1.9' => \Yii::t('app/crf', 'System pressure', null, $lang),
            'A.1.10' => \Yii::t('app/crf', 'Means of detection', null, $lang),
            'A.1.11' => \Yii::t('app/crf', 'Cause of leak', null, $lang),
            'A.1.12' => \Yii::t('app/crf', 'Did ignition occur?', null, $lang),
            'A.1.13' => \Yii::t('app/crf', 'Ignition source', null, $lang),
            'A.1.14' => \Yii::t('app/crf', 'What emergency action was taken?', null, $lang),
            'A.1.15' => \Yii::t('app/crf', 'Any additional comments', null, $lang),
            
            'A.2' => \Yii::t('app/crf', 'Description of circumstances, consequences of event and emergency response', null, $lang),
            'A.2.1' => \Yii::t('app/crf', 'Was there a release of a non-hydrocarbon hazardous substance?', null, $lang),
            'A.2.2' => \Yii::t('app/crf', 'Was there a non-carbon fire (e.g. electrical) with a significant potential to cause a major accident?', null, $lang),
            'A.2.3' => \Yii::t('app/crf', 'Is the incident likely to cause degradation to the surrounding marine environment?', null, $lang),
            
            
            'A.3' => \Yii::t('app/crf', 'Preliminary direct and underlying causes', null, $lang),
            'A.4' => \Yii::t('app/crf', 'Initial lessons learned and preliminary recommendations to prevent recurrence of similar events', null, $lang),
            
            'B' => \Yii::t('app/crf', 'Loss of well control requiring actuation of well control equipment, or failure of a well barrier requiring its replacement or repair', null, $lang),
            'B.1' => \Yii::t('app/crf', 'General information', null, $lang),
            'B.2' => \Yii::t('app/crf', 'Description of circumstances, consequences of event and emergency response', null, $lang),
            'B.3' => \Yii::t('app/crf', 'Preliminary direct and underlying causes', null, $lang),
            'B.4' => \Yii::t('app/crf', 'Initial lessons learned and preliminary recommendations to prevent recurrence of similar events', null, $lang),
            
            'C' => \Yii::t('app/crf', 'Failure of a safety and environmental critical element', null, $lang),
            'C.1' => \Yii::t('app/crf', 'General information', null, $lang),
            'C.2' => \Yii::t('app/crf', 'Description of circumstances, consequences of event and emergency response', null, $lang),
            'C.2.1' => \Yii::t('app/crf', 'Description of SECE and circumstances', null, $lang),
            'C.2.2' => \Yii::t('app/crf', 'Description of consequences', null, $lang),
            'C.3' => \Yii::t('app/crf', 'Preliminary direct and underlying causes', null, $lang),
            'C.4' => \Yii::t('app/crf', 'Initial lessons learned and preliminary recommendations to prevent recurrence of similar events', null, $lang),
            
            'D' => ucfirst(\Yii::t('app/crf', 'significant loss of structural integrity, or loss of protection against the effects of fire or explosion, or loss of station keeping in relation to a mobile installation', null, $lang)),
            'D.1' => \Yii::t('app/crf', 'General information', null, $lang),
            'D.2' => \Yii::t('app/crf', 'Description of circumstances, consequences of event and emergency response', null, $lang),
            'D.3' => \Yii::t('app/crf', 'Preliminary direct and underlying causes', null, $lang),
            'D.4' => \Yii::t('app/crf', 'Initial lessons learned and preliminary recommendations to prevent recurrence of similar events', null, $lang),
            
            'E' => \Yii::t('app/crf', 'Vessels on collision course and actual vessels collisions with an offshore installation', null, $lang),
            'E.1' => \Yii::t('app/crf', 'General information', null, $lang),
            'E.2' => \Yii::t('app/crf', 'Description of circumstances, consequences of event and emergency response', null, $lang),
            'E.3' => \Yii::t('app/crf', 'Preliminary direct and underlying causes', null, $lang),
            'E.4' => \Yii::t('app/crf', 'Initial lessons learned and preliminary recommendations to prevent recurrence of similar events', null, $lang),
            
            'F' => \Yii::t('app/crf', 'Helicopter accidents, on or near offshore installations', null, $lang),
            'F.1' => \Yii::t('app/crf', 'General information', null, $lang),
            'F.2' => \Yii::t('app/crf', 'Description of circumstances, consequences of event and emergency response', null, $lang),
            'F.3' => \Yii::t('app/crf', 'Preliminary direct and underlying causes', null, $lang),
            'F.4' => \Yii::t('app/crf', 'Initial lessons learned and preliminary recommendations to prevent recurrence of similar events', null, $lang),
            
            'G' => \Yii::t('app/crf', 'Any fatal accident to be reported under the requirements of Directive 92/91/EEC', null, $lang),
            
            'H' => \Yii::t('app/crf', 'Any serious injuries to five or more persons in the same accident to be reported under the requirements of Directive 92/91/EEC', null, $lang),
            
            'I' => \Yii::t('app/crf', 'Any evacuation of personnel', null, $lang),
            'I.1' => \Yii::t('app/crf', 'General information', null, $lang),
            'I.2' => \Yii::t('app/crf', 'Description of circumstances, consequences of event and emergency response', null, $lang),
            'I.3' => \Yii::t('app/crf', 'Preliminary direct and underlying causes', null, $lang),
            'I.4' => \Yii::t('app/crf', 'Initial lessons learned and preliminary recommendations to prevent recurrence of similar events', null, $lang),
            
            'J' => \Yii::t('app/crf', 'A major environmental incident', null, $lang),
            'J.1' => \Yii::t('app/crf', 'General information', null, $lang),
            'J.2' => \Yii::t('app/crf', 'Description of circumstances, consequences of event and emergency response', null, $lang),
            'J.3' => \Yii::t('app/crf', 'Preliminary direct and underlying causes', null, $lang),
            'J.4' => \Yii::t('app/crf', 'Initial lessons learned and preliminary recommendations to prevent recurrence of similar events', null, $lang),
        ];
    }

    public static function SectionsTitleShort($lang = null) {
        if (!isset($lang)) {
            $lang = \Yii::$app->language;
        }
        return [
            'A' => \Yii::t('app/crf', 'Unintended releases', null, $lang),
            'B' => \Yii::t('app/crf', 'Loss of well control/barrier', null, $lang),
            'C' => \Yii::t('app/crf', 'Failure of SECE', null, $lang),
            'D' => \Yii::t('app/crf', 'Loss of structural integrity', null, $lang),
            'E' => \Yii::t('app/crf', 'Vessel collision', null, $lang),
            'F' => \Yii::t('app/crf', 'Helicopter accidents', null, $lang),
            'G' => \Yii::t('app/crf', 'Fatal accidents', null, $lang),
            'H' => \Yii::t('app/crf', 'Serious injuries to five or more persons', null, $lang),
            'I' => \Yii::t('app/crf', 'Evacuation of personnel', null, $lang),
            'J' => \Yii::t('app/crf', 'Major environmental incident', null, $lang),
        ];
    }
    

    public static function EventTypes() {
        return [
            /* Section A */
            [
                'title' => 'A',
                'checked' => 'is_a',    //the attribute of the model that controlls the checked state of the checkbox
                'description' => \Yii::t('app/crf', 'Unintended release of oil, gas or other hazardous substances, whether or not ignited'),
                'description_short' => \Yii::t('app/crf', 'Unintended releases'),
                'subitems' => [
                    [
                        'title' => '1.',
                        'description' => \Yii::t('app/crf', 'Any unintentional release of ignited gas or oil on or from an offshore installation;'),
                    ],                        
                    [
                        'title' => '2.',
                        'description' => \Yii::t('app/crf', 'The unintentional release on or from an offshore installation of:'),
                        'subitems' => [
                            [
                                'title'=>'(a)',
                                'description'=> \Yii::t('app/crf', 'not ignited natural gas or evaporated associated gas if mass released >= 1 kg'),
                            ],
                            [
                                'title'=>'(b)',
                                'description'=> \Yii::t('app/crf', 'not ignited liquid of petroleum hydrocarbon if mass released >= 60 kg'),
                            ]
                        ]
                    ],                        
                    [
                        'title' => '3.',
                        'description' => \Yii::t('app/crf', 'The unintentional	release	or escape of any hazardous substance, for which the major accident risk has been assessed in the report on major hazards, on or from an offshore installation, including wells and returns of drilling additives.'),
                    ],                        
                ]
            ],
            
            /* Section B */
            [
                'title' => 'B',
                'checked' => 'is_b',    //the attribute of the model that controlls the checked state of the checkbox
                'description' => \Yii::t('app/crf', 'Loss of well control requiring actuation of well control equipment, or failure of a well barrier requiring its replacement or repair'),
                'description_short' => \Yii::t('app/crf', 'Loss of well control/barrier'),
                'subitems' => [
                    [
                        'title' => '1.',
                        'description' => \Yii::t('app/crf', 'Any blowout, regardless of the duration;'),
                    ],                        
                    [
                        'title' => '2.',
                        'description' => \Yii::t('app/crf', 'The coming into operation of a blowout prevention or diverter system to control flow of well-fluids;'),
                    ],                        
                    [
                        'title' => '3.',
                        'description' => \Yii::t('app/crf', 'The mechanical failure of any part of a well, whose purpose is to prevent or limit the effect of the unintentional release of fluids from a well or a reservoir being drawn on by a well, or whose failure would cause or contribute to such a release') . ';',
                    ],                        
                    [
                        'title' => '4.',
                        'description' => \Yii::t('app/crf', 'The taking of precautionary measures additional to any already contained in the original drilling programme where a planned minimum separation distance between adjacent wells was not maintained.'),
                    ],                        
                ]
            ],
            
            /* Section C */
            [
                'title' => 'C',
                'checked' => 'is_c',    //the attribute of the model that controlls the checked state of the checkbox
                'description' => \Yii::t('app/crf', 'Failure of a safety and environmental critical element'),
                'description_short' => \Yii::t('app/crf', 'Failure of SECE'),
                'subitems' => [
                    [
                        'title' => null,
                        'description' => \Yii::t('app/crf', 'Any loss or non-availability of a SECE requiring immediate remedial action.'),
                    ],                        
                ]
            ],
            
            /* Section D */
            [
                'title' => 'D',
                'checked' => 'is_d',    //the attribute of the model that controlls the checked state of the checkbox
                'description' => ucfirst(\Yii::t('app/crf', 'significant loss of structural integrity, or loss of protection against the effects of fire or explosion, or loss of station keeping in relation to a mobile installation')),
                'description_short' => \Yii::t('app/crf', 'Loss of structural integrity'),
                'subitems' => [
                    [
                        'title' => null,
                        'description' => \Yii::t('app/crf', 'Any detected condition that reduces the designed structural integrity of the installation, including stability, buoyancy and station keeping, to the extent that it requires '
                                . 'immediate remedial action.'),
                    ],                        
                ]
            ],
            
            /* Section E */
            [
                'title' => 'E',
                'checked' => 'is_e',    //the attribute of the model that controlls the checked state of the checkbox
                'description' => \Yii::t('app/crf', 'Vessels on collision course and actual vessel collisions with an offshore installation'),
                'description_short' => \Yii::t('app/crf', 'Vessel collision'),
                'subitems' => [
                    [
                        'title' => null,
                        'description' => \Yii::t('app/crf', 'Any collision, or potential collision, between a vessel and an offshore installation which has, or would have, enough energy to cause sufficient damage to the installation and/or vessel, to jeopardise the overall structural or process integrity.'),
                    ],                        
                ]
            ],
            
            /* Section F */
            [
                'title' => 'F',
                'checked' => 'is_f',    //the attribute of the model that controlls the checked state of the checkbox
                'description' => \Yii::t('app/crf', 'Helicopter accidents, on or near offshore installations'),
                'description_short' => \Yii::t('app/crf', 'Helicopter accidents'),
                'subitems' => [
                    [
                        'title' => null,
                        'description' => \Yii::t('app/crf', 'Any collision, or potential collision, between a helicopter and an offshore installation.'),
                    ],                        
                ]
            ],
            
            /* Section G */
            [
                'title' => 'G',
                'checked' => 'is_g',    //the attribute of the model that controlls the checked state of the checkbox
                'description' => \Yii::t('app/crf', 'Any fatal accident to be reported under the requirements of Directive 92/91/EEC'),
                'description_short' => \Yii::t('app/crf', 'Fatal accidents'),
            ],
            
            /* Section H */
            [
                'title' => 'H',
                'checked' => 'is_h',    //the attribute of the model that controlls the checked state of the checkbox
                'description' => \Yii::t('app/crf', 'Any serious injuries to five or more persons in the same accident to be reported under the requirements of Directive 92/91/EEC'),
                'description_short' => \Yii::t('app/crf', 'Serious injuries to five or more persons'),
            ],
            
            /* Section I */
            [
                'title' => 'I',
                'checked' => 'is_i',    //the attribute of the model that controlls the checked state of the checkbox
                'description' => \Yii::t('app/crf', 'Any evacuation of personnel'),
                'description_short' => \Yii::t('app/crf', 'Evacuation of personnel'),
                'subitems' => [
                    [
                        'title' => null,
                        'description' => \Yii::t('app/crf', 'Any unplanned emergency evacuation of part of or all personnel as a result of, or where there is a significant risk of a major accident.'),
                    ],                        
                ]
            ],
            
            /* Section J */
            [
                'title' => 'J',
                'checked' => 'is_j',    //the attribute of the model that controlls the checked state of the checkbox
                'description' => \Yii::t('app/crf', 'A major environmental incident'),
                'description_short' => \Yii::t('app/crf', 'Major environmental incident'),
                'subitems' => [
                    [
                        'title' => null,
                        'description' => \Yii::t('app/crf', 'Any major environmental incident as defined in Article 2.1.d and Article 2.37 of Directive 2013/30/EU.'),
                    ],                        
                ]
            ],
            
        ];
    }
    
    /**
     * This function returns the appropriate CRF model based on the code (e.g. A.1 returns SectionA).
     * The codes must be one in the SectionsTitle
     * 
     * @param type $code
     */
    public static function model_factory($code) {
        if (!key_exists($code, self::SectionsTitle())) {
            return null;
        }
        
        /*
         * get the depth of the model from code
         */
        $split = explode('.', $code);
        
        //echo '<pre>';
        //echo print_r($split);
        //echo '</pre>';
        
        switch (count($split)) {
            case 1:
                //e.g. A
                $model_name = '\app\models\events\drafts\sections\Section'.strtoupper($split[0]);
                $model = new $model_name();
                
                return $model;
                
//                echo '<pre>';
//                echo print_r($model);
//                echo '</pre>';
//                die();
                break;
            case 2:
                //\app\models\events\drafts\sections\subsections\B1
                $model_name = '\app\models\events\drafts\sections\subsections\\'.strtoupper($split[0]).$split[1];
                $model = new $model_name();
                
                return $model;
                
//                echo '<pre>';
//                echo var_dump($model);
//                echo '</pre>';
//                die();
                break;
            case 3:
                //e.g. A.1.1
                //the same as for 2 since there's no specific model for blocks
                $model_name = '\app\models\events\drafts\sections\subsections\\'.strtoupper($split[0]).$split[1];
                $model = new $model_name();
                
                return $model;
                
//                echo '<pre>';
//                echo var_dump($model);
//                echo '</pre>';
//                die();
                break;
        }
        
    }
    
    
    
    public static function getLevel($code) {
        if (!key_exists($code, self::SectionsTitle())) {
            return -1;
        }
        
        /*
         * get the depth of the model from code
         */
        $split = explode('.', $code);
        return count($split);
    }
    
    
    
    public static function SpecialValues($which = null) {
        if (!isset($which)) {
            return [
                -9999 => [\Yii::t('app', 'NA'), \Yii::t('app', 'not applicable')],
                -9998 => [\Yii::t('app', 'NN'), \Yii::t('app', 'unknown')],
            ];
        } elseif ($which === 0) {
            return [-9999, -9998];
        } elseif ($which === 1) {
            return [\Yii::t('app', 'NA'), \Yii::t('app', 'NN')];
        } elseif ($which === 2) {
            return [\Yii::t('app', 'not applicable'), \Yii::t('app', 'unknown')];
        }
    }

    
    public static function SpecialValueLiteral($code, $which) {
        if (!is_numeric($code)) {
            return $code;
        }
        $code = intval($code);
        
        if (in_array($code, self::SpecialValues(0))) {
            return self::SpecialValues()[$code][$which];
        } else {
            return $code;
        }
    }
    
    public static function IsSpecialValue($code) {
        return in_array($code, self::SpecialValues(0));
    }

    public static function SpecialValueCodeByLiteral($lit) {
        foreach (self::SpecialValues() as $key=>$value) {
            if (in_array(strtoupper($lit), $value)) {
                return $key;
            }
        }
        return null;
    }
    
}
