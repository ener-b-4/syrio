<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\crf;

/**
 * This class contains the settings for displaying the CRF
 * 
 * Main use - indicate whether or not to include the MA assessment in the views;
 * 
 * The object will be set in the controllers' actions.
 * The object is to be passed to the redering views.
 *
 * @property bool $include_ma_assessment whether or not to include the MA assessment block in the section view; default FALSE.
 * @property bool $as_pdf format of the report; default FALSE.
 * 
 * @author vamanbo
 */
class CrfDisplaySettings {
    
    public function __construct() {
        $this->include_ma_assessment = FALSE;
    }
    
    
    public $include_ma_assessment;
    
    public $as_pdf;
}
