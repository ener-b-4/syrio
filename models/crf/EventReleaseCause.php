<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\crf;

use Yii;
use app\models\events\drafts\sections\subsections\A1;
use app\models\crf\LeakCause;

/**
 * This is the model class for table "a1_release_causes".
 *
 * @property integer $a1_id
 * @property integer $leak_cause_id
 *
 * @property A1 $a1
 * @property LeakCause $leakCause
 */
class EventReleaseCause extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'a1_release_causes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['a1_id', 'leak_cause_id'], 'required'],
            [['a1_id', 'leak_cause_id'], 'integer'],
            [['a1_id'], 'exist', 'skipOnError' => true, 'targetClass' => SA1::className(), 'targetAttribute' => ['a1_id' => 'id']],
            [['leak_cause_id'], 'exist', 'skipOnError' => true, 'targetClass' => LeakCause::className(), 'targetAttribute' => ['leak_cause_id' => 'num_code']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'a1_id' => 'A1 ID',
            'leak_cause_id' => 'Leak Cause ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getA1()
    {
        return $this->hasOne(A1::className(), ['id' => 'a1_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLeakCause()
    {
        return $this->hasOne(LeakCause::className(), ['num_code' => 'leak_cause_id']);
    }
}
