<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\crf;

use Yii;
use app\models\crf\LeakCause;

/**
 * This is the model class for table "leak_cause_categories".
 *
 * @property integer $id
 * @property string $name
 *
 * @property LeakCause[] $leakCauses
 */
class LeakCauseCategories extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'leak_cause_categories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id'], 'integer'],
            [['name'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLeakCauses()
    {
        return $this->hasMany(LeakCause::className(), ['category_id' => 'id']);
    }
}
