<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\crf;

use Yii;

/**
 * This is the model class for table "sece".
 *
 * @property integer $id
 * @property string $name
 *
 * @property SC21Sece[] $sC21Seces
 * @property SC21[] $sC21s
 */
class Sece extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sece';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id'], 'integer'],
            [['name'], 'string', 'max' => 64],
            [['name'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => Yii::t('app', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSC21Seces()
    {
        return $this->hasMany(SC21Sece::className(), ['sece_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSC21s()
    {
        return $this->hasMany(SC21::className(), ['id' => 's_c_2_1_id'])->viaTable('s_c_2_1_sece', ['sece_id' => 'id']);
    }
}
