<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\crf\constants;

use Yii;

/**
 * This is the model class for table "leak_cause".
 *
 * @property integer $num_code
 * @property string $cause
 * @property integer $category_id
 *
 * @property LeakCauseCategories $category
 */
class LeakCause extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'leak_cause';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['num_code', 'cause', 'category_id'], 'required'],
            [['num_code', 'category_id'], 'integer', 'min'=>0],  //Author: cavesje Date:20151119
            [['cause'], 'string', 'max' => 128]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'num_code' => Yii::t('app', 'Num Code'),
            'cause' => Yii::t('app', 'Cause'),
            'category_id' => Yii::t('app', 'Category ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(LeakCauseCategories::className(), ['id' => 'category_id']);
    }
}
