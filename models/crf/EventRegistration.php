<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\crf;

use yii\base\Model;

class EventRegistration extends Model
{
    public $id;
    
    public $event_date_year;
    public $event_date_month;
    public $event_date_day;
    public $event_time_hour;
    public $event_time_minute;
    public $event_time_seconds;
    
    
    public $operator;
    public $installation_name;
    public $field_name;
    
    public $reporter_name;
    public $reporter_role;
    
    public $telephone;
    public $address;
    
    public $created_by;
    public $created_at;
    public $modified_at;
    
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'event_date_year', 
                    'event_date_month', 
                    'event_date_day', 
                    'event_time_hour', 
                    'event_time_minute', 
                    'event_time_seconds', 
                    'operator',
                    'created_by',
                    'installation_name'
                ],
                'required'],            
            [['created_at', 'modified_at'], 'safe'],
            [['operator', 'installation_name'], 'string', 'max' => 45],
        ];
    }    
    

    public function attributeLabels()
    {
        return [
            'event_time_hour' => 'hh',
            'event_time_minute' => 'min',
            'event_time_second' => 'ss',
            'operator' => 'Operator/owner',
            'installation_name' => 'Name/type of the installation',
            'field_name' => 'Field name/code (if relevant)',
            'reporter_name' => 'Name of the reporting person',
            'reporter_role' => 'Role of the reporting person',
            'telephone' => 'Telephone number',
            'address' => 'Email',
            'created_by' => ucwords(Yii::t('app', 'created by')),
            'created_at' => ucwords(Yii::t('app', 'created at')),
            'modified_at' => ucwords(Yii::t('app', 'modified at')),
        ];
    }
    
    
    public function getEventDateTime()
    {
        return $this->_event_date_time;
    }
    
    public function setEventDateTime($date, $time)
    {
        //$this->_event_date_time = DateTime::createFromFormat('m-d-Y', '10-16-2003')->format('Y-m-d');
        $this->_event_date_time = DateTime::createFromFormat('m-d-Y', $date);
    }
    
    public function getIsNewRecord()
    {
        return $this->id === null ? true : false;
    }
    
}