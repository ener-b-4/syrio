<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ReportingHistory;

use yii\data\Sort;

/**
 * ReportingHistorySearch represents the model behind the search form about `app\models\ReportingHistory`.
 */
class ReportingHistorySearch extends ReportingHistory
{
    
    public $edt;        //event_date_time
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'evt_id', 'draft_id', 'event', 'visibility'], 'integer'],
            [['description', 'edt'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        
        $sort = new Sort([
            'attributes' => [
                'id',
                'edt' => [
                    'asc' => ['{{event_declaration}}.[[id]]' => SORT_ASC],
                    'desc' => ['{{event_declaration}}.[[id]]' => SORT_DESC],
                    'default' => SORT_DESC,
                    'label' => \Yii::t('app/crf', 'Event date/time'),
                ],
            ],
        ]);
        
        
        $query = ReportingHistory::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);


        //I need to join the model with the event declaration and stuff IF the user is sys_admin
        //otherwise it is already joined
        if (\Yii::$app->user->can('sys_admin')) {
            $query->join('inner join', '{{event_declaration}}', '{{event_declaration}}.[[id]] = {{reporting_history}}.[[evt_id]]');
        }
        
        $dataProvider->setSort($sort);
        $dataProvider->pagination->pageSize = 50;
        
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            //'id' => $this->id,
            'evt_id' => $this->evt_id,
            'draft_id' => $this->draft_id,
            'event' => $this->event,
            'visibility' => $this->visibility,
        ]);

        $query->andFilterWhere(['like', 'description', $this->description]);

        
        return $dataProvider;
    }
}
