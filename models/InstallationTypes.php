<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "installation_types".
 *
 * @property string $type
 * @property string $name
 * @property integer $mobile
 *
 * @property Installations[] $installations
 */
class InstallationTypes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'installation_types';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'name', 'mobile'], 'required'],
            [['mobile'], 'integer', 'min'=>0],  //Author: cavesje Date:20151119
            [['type'], 'string', 'max' => 8],
            [['name'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'type' => ucfirst(Yii::t('app', 'type')),
            'name' => Yii::t('app', 'Name'),
            'mobile' => Yii::t('app', 'Mobile'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInstallations()
    {
        return $this->hasMany(Installations::className(), ['type' => 'type']);
    }
}
