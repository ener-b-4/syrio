<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Holidays;

/**
 * HolidaysSearch represents the model behind the search form about `app\models\Holidays`.
 */
class HolidaysSearch extends Holidays
{
    public $date;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'day', 'month', 'year', 'repeat', 'created_at', 'updated_at'], 'integer'],
            [['name', 'date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Holidays::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'day' => $this->day,
            'month' => $this->month,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        if (isset($this->year) && trim($this->year)!== '') {
            //die();
            //also add the null condition
            $query->andWhere([
                'or',
                ['year'=>$this->year],
                ['year'=>null, 'year'=>0 ]
            ]);
        } 
        
        if (isset($this->repeat) && trim($this->repeat)!== '' && $this->repeat == 0) {
            //die();
            //also add the null condition
            $query->andWhere([
                'or',
                ['repeat'=>$this->repeat],
                ['repeat'=>null ]
            ]);
        } 
        
        $query->andFilterWhere(['like', 'name', $this->name]);

        $query->orderBy([
            'repeat' => SORT_DESC,
            'concat_ws("-",year, month, day)' => SORT_ASC,
            'name'=>SORT_ASC
        ]);
        
        return $dataProvider;
    }
}
