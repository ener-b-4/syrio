<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\admin\users;

use app\models\User;
use \Yii;

/**
 * Behavior that adds the FirstLogin property to the User class
 *
 * @author bogdanV
 */
class UserFirstLoginBehavior extends \yii\base\Behavior {
    

    /**
     * 
     * @return boolean 'true' if this is the first time the current user logs in
     * @throws Exception
     */
    public function getFirstLogin()
    {
        if (!Yii::$app->user->isGuest)
        {
            /*
            echo '<pre>';
            echo var_dump(Yii::$app->user->identity);
            echo '</pre>';
            echo Yii::$app->user->identity->status == User::STATUS_NEW;
            die();
             * 
             */
            return Yii::$app->user->identity->status == User::STATUS_NEW;
        }
        else
        {
            echo 'error';
            die();
            
            throw new Exception('not allowed');
        }
    }
    
}
