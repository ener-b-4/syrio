<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\admin\users;

/**
 * Description of CreateUserModel
 *
 * @author vamanbo
 */
class CreateUserModel extends \yii\base\Model
{
    
    public function rules() {
        return [
            ['user_type', 'required'],
            ['target', 'required', 'when' => function ($model, $attribute) {
                return isset($model->user_type);
            }],
        ];
    }
    
    public $user_type;

    public $target;
    
    public function getTargets($user_type)
    {
        switch ($user_type)
        {
            case 'ca':
                //get the competent authorities
                return \app\modules\management\models\CompetentAuthority::find()
                    ->join('inner join', 'c_country', 'iso2 = country')
                    ->asArray()
                    ->orderBy('country')
                    ->select(['country', 'c_country.short_name'])
                    ->all();
                break;
            case 'oo':
                return \app\modules\management\models\Organization::findOperatorOrganization()
                    ->asArray()
                    ->orderBy('organization_acronym')
                    ->select(['id', 'concat(organization_acronym, " - " , organization_name) as full_name'])
                    ->all();
                break;
            case 'inst':
                return \app\models\Installations::find()
                    ->asArray()
                    ->orderBy('name')
                    ->select(['id', 'concat(name, " - " , type) as full_name'])
                    ->all();
                break;
        }
    }
    
}
