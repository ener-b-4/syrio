<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\admin\users;

/**
 * Model of the FirstLoginForm
 *
 * 
 * 
 * @author bogdanV
 */
class FirstLoginForm extends \yii\base\Model {
    
    public $new_password;
    public $new_password_repeat;
    
    public $captcha;
    
    public function rules() {
        return [
            [['new_password', 'new_password_repeat'], 'required'],
            [['new_password'], 'string', 'min'=>6, 'max'=>32],
            ['new_password_repeat', 'compare', 'compareAttribute'=>'new_password'],
            ['captcha', 'captcha'],
        ];
    }

    public function attributeLabels() {
        return [
            'new_password' => \Yii::t('app', 'Password'),
            'new_password_repeat' => \Yii::t('app', 'Repeat password')
        ];
    }
}
