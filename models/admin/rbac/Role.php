<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\admin\rbac;

use Yii;

/**
 * This is the model class for table "auth_item".
 *
 * @property string $name
 * @property integer $type
 * @property string $description
 * @property string $rule_name
 * @property string $data
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property AuthAssignment[] $authAssignments
 * @property AuthRule $ruleName
 * @property AuthItemChild[] $authItemChildren
 * 
 * @property app\models\admin\rbac\Role[] $caRoles
 */
class Role extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'auth_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'type'], 'required'],
            [['type', 'created_at', 'updated_at'], 'integer'],
            [['description', 'data'], 'string'],
            [['name', 'rule_name'], 'string', 'max' => 64],
            [['type'], 'integer', 'min'=>0],                 //Author: cavesje Date:20151119
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Name'),
            'type' => ucfirst(Yii::t('app', 'type')),
            'description' => Yii::t('app', 'Description'),
            'rule_name' => Yii::t('app', 'Rule Name'),
            'data' => 'Data',
            'created_at' => ucwords(Yii::t('app', 'created at')),
            'updated_at' => ucwords(Yii::t('app', 'modified at')),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthAssignments()
    {
        return $this->hasMany(AuthAssignment::className(), ['item_name' => 'name']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRuleName()
    {
        return $this->hasOne(AuthRule::className(), ['name' => 'rule_name']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthItemChildren()
    {
        return $this->hasMany(AuthItemChild::className(), ['child' => 'name']);
    }
    
    /**
     * Returns the CA related roles
     * 
     * @return \app\models\admin\rbac\Roles
     */
    public function getCaRoles()
    {
        if (Yii::$app->user->can('ca_admin') || Yii::$app->user->can('sys_admin') || Yii::$app->user->can('ca-user-list'))
        {
            if (Yii::$app->user->can('sys_admin')) {
            $query = $this->find()
                    ->where(['type' => 1])
                    ->andWhere(['like', 'name', 'ca_'])
                    ->andWhere('name <> "ca_"')
                    ->orWhere(['name'=>'sys_admin']);
            } else {
            $query = $this->find()
                    ->where(['type' => 1])
                    ->andWhere(['like', 'name', 'ca_'])
                    ->andWhere('name <> "ca_"');
            }
            
            //return $query->asArray()->all();
            return $query->all();
        }
        else
        {
            return [];
        }
    }
    
    /**
     * Returns the OO related roles
     * 
     * @return \app\models\admin\rbac\Roles
     */
    public function getOoRoles()
    {
        if (Yii::$app->user->can('ca_admin') || Yii::$app->user->can('op_admin') || Yii::$app->user->can('sys_admin') || Yii::$app->user->can('ca-user-list') || Yii::$app->user->can('oo-user-list'))
        {
            $query = $this->find()
                    ->where(['type' => 1])
                    ->andWhere(['like', 'name', 'op_'])
                    ->andWhere('name <> "op_"');
            
            //return $query->asArray()->all();
            return $query->all();
        }
        else
        {
            return [];
        }
    }
    
    /**
     * Returns the Installation related roles
     * 
     * @return \app\models\admin\rbac\Roles
     */
    public function getInstallationRoles()
    {
        if (Yii::$app->user->can('ca_admin') || Yii::$app->user->can('op_admin') || Yii::$app->user->can('inst_admin') || Yii::$app->user->can('sys_admin') || Yii::$app->user->can('ca-user-list') || Yii::$app->user->can('oo-user-list'))
        {
            $query = $this->find()
                    ->where(['type' => 1])
                    ->andWhere(['like', 'name', 'inst_'])
                    ->andWhere('name <> "inst_"');
            
            //return $query->asArray()->all();
            return $query->all();
        }
        else
        {
            return [];
        }
    }
    
}
