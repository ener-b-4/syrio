<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Installations;

use yii\helpers\HtmlPurifier;

/**
 * InstallationsSearch represents the model behind the search form about `app\models\Installations`.
 */
class InstallationsSearch extends Installations
{
    public $typeObject;
    public $operator;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'operator_id', 'name', 'type', 'type_other', 'code', 'flag', 'modified_by', 'typeObject', 'operatorAcronym'], 'safe'],
            [['status', 'created_at', 'created_by', 'modified_at'], 'integer'],
            [['typeObject', 'operator'], 'safe'],
            [['year_of_construction'], 'integer', 'min'=>1950, 'max'=>9999],
            [['number_of_beds'], 'integer', 'min'=>0, 'max'=>500],  //should match the XML limit
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        /*
         * SANITIZE PARAMS
         */
        
        /*
        if (isset($_GET['InstallationsSearch']))
        {
            echo 'true';
            die();
        }
        else
        {
            echo 'false';
            die();
        }
        */
        
        $query = Installations::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'id',
                'code',
                'name',
                'year_of_construction',
                'typeObject' => [
                    'asc' => ['type' => SORT_ASC],
                    'desc' => ['type' => SORT_DESC],
                    'label' => Yii::t('app', 'Operation type'),
                    'default' => SORT_ASC
                ],
                'number_of_beds',
                'operator' => [
                    'asc' => ['organizations.organization_acronym' => SORT_ASC],
                    'desc' => ['organizations.organization_acronym' => SORT_DESC],
                    'label' => Yii::t('app', 'Operator'),
                    'default' => SORT_ASC
                ],
                'flag'
                //'operator'
            ]
        ]);
        
        if (!($this->load($params) && $this->validate())) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->join('inner join', 'organizations', 'installations.operator_id = organizations.id');
        
        $query->andFilterWhere([
            'year_of_construction' => $this->year_of_construction,
            'number_of_beds' => $this->number_of_beds,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'modified_at' => $this->modified_at,
        ]);

        $query->andFilterWhere([
            'organizations.organization_acronym' => $this->_operator_acronym,
        ]);
        
        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'installations.name', $this->name])
            ->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'type_other', $this->type_other])
            ->andFilterWhere(['like', 'code', $this->code])
            ->andFilterWhere(['like', 'flag', $this->flag])
            ->andFilterWhere(['like', 'modified_by', $this->modified_by]);

        if (isset($this->typeObject))
        {
            $query->join('left join', 'installation_types', 'installations.type = installation_types.type');
            $query->andFilterWhere(['installations.type' => $this->typeObject]);
        }
        
        if (isset($this->operator))
        {
            $query->andFilterWhere(['or', 
                ['like', 'organizations.organization_acronym', $this->operator],
                ['like', 'organizations.organization_name', $this->operator],
            ]);
        }
        return $dataProvider;
    }
    
    
    
    private $_operator_acronym;
    public function GetOperatorAcronym()
    {
        return $this->_operator_acronym;        
    }
    public function SetOperatorAcronym($value)
    {
        $this->_operator_acronym = $value;
    }
    
    
}
