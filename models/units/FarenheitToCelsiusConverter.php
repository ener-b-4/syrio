<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\units;

class FarenheitToCelsiusConverter implements \app\models\units\IUnitConverter
{
    
    /**
     * Function to call for direct transformation (i.e. to reference unit)
     * 
     * @param double $value the value in F
     * @return double the value in C
     */
    public function toNorm($value) {
        return ($value - 32) / 1.8;
    }
    
    /**
     * Function to call for the inverse transformation (i.e. from reference unit)
     * 
     * @param double $value the value in F
     * @return double the value in C
     */
    public function fromNorm($value) {
        return $value * 1.8 + 32;
    }
    
    /**
     * 
     * @return string
     */
    public function getName() {
        return 'Farenheit to Celsius converter';
    }
}
