<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\units;

use Yii;

/**
 * This is the model class for table "units".
 *
 * @property integer $id
 * @property string $unit
 * @property double $c_factor
 * @property double $s_unit
 * @property string $expression the jquery representation of the conversion equation
 * @property string $conversion_class the fully qualified classname (implementing IUnitConverter)
 * @property integer $scope
 * @property integer $us
 */
class Units extends \yii\db\ActiveRecord
{
    const UNIT_MASS = 1;
    const UNIT_TIME = 2;
    const UNIT_MASS_RATE = 3;
    const UNIT_SPEED = 4;
    const UNIT_PRESSURE = 5;
    const UNIT_LENGTH = 6;
    const UNIT_TEMPERATURE = 7;
    const UNIT_METRIC_VOLUME = 8;
    const UNIT_OF_ENERGY_GO = 9;
    
    /**
     * @author Bogdan Vamanu <bogdan.vamanu@jrc.ec.europa.eu>
     * 
     * @param double $value the value to be ransformed
     * @param int $fromUnit the unit id
     */
    public static function Normalize($value, $fromUnit)
    {
        /* @var $unit Units */
                
        $unit = Units::find()
                ->where(['id'=>$fromUnit])
                ->one();
        if (!$unit) {
            throw new \InvalidArgumentException('Unit not found.');
        }
        
        /* first check if the converter class exists - priority 0 */
        $conversion_class = $unit->conversion_class;
        if (isset($conversion_class))
        {
            //create the class
            $converter = new $conversion_class();
            return $converter->toNorm($value);
        }
        
        /* secondly check if expression is set */
        if (isset($unit->expression))
        {
            /* get the json as array */
            $transformArray = json_decode($unit->expression, true);
            $expression = $transformArray['ex'];
            $params = [];
            foreach(range('a', 'z') as $param)
            {
                if (strpos($expression, $param) !== false)
                {
                    $params[] = $param;
                }
            }
            foreach ($params as $param)
            {
                $expression = $param === 'x' ? str_replace($param, $value, $expression) : str_replace($param, $transformArray[$param], $expression);
            }
            $calc = new \app\models\units\Calculator();
            return $calc->calculate($expression);            
        }
        
        return $value * $unit->c_factor + (isset($unit->s_unit) ? $unit->s_unit : 0);
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'units';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['unit', 'c_factor', 'scope'], 'required'],
            [['c_factor'], 'number', 'min'=>0],  //Author: cavesje Date:20151119
            [['scope', 'us'], 'integer', 'min'=>0],  //Author: cavesje Date:20151119
            [['unit'], 'string', 'max' => 45],
            [['unit'], 'unique'],
            [['expression'], 'string'],
            [['expression'], 'safe'],
            [['c_factor', 'scope', 'us'], 'unique', 'targetAttribute' => ['c_factor', 'scope', 'us'], 'message' => 'The combination of Multiplication Factor, Scope and Imperial already exists.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'unit'                  => Yii::t('app/units', 'Unit'),
            'c_factor'              => Yii::t('app/units', 'Multiplication Factor'),
            's_unit'                => Yii::t('app/units', 'Sum Factor'),
            'scope'                 => Yii::t('app/units', 'Scope'),
            'us'                    => ucfirst(Yii::t('app/units', 'imperial')),
            'expression'            => Yii::t('app/units', 'Expression'),
            'conversion_class'      => Yii::t('app/units', 'Conversion Class'),
        ];
    }
    
    
    public static function EnergyUnitGo($id)
    {
        return Units::find()
                ->where(['scope' => self::UNIT_OF_ENERGY_GO])
                ->andWhere(['id' => $id])->one();
                
    }
    
    public static function getUnitsArray($type) {
        return \yii\helpers\ArrayHelper::map(Units::find()->where(['scope'=>$type])->all(), 'id', 'unit');
    }
    
}

/*20120920*/