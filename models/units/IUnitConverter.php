<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\units;

interface IUnitConverter
{
    /**
     * Name of the converer
     */
    public function getName();
    
    public function toNorm($value);
    
    public function fromNorm($value);
}
