<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\units;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\units\Units;

/**
 * UnitsSearch represents the model behind the search form about `app\models\units\Units`.
 */
class UnitsSearch extends Units
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'scope', 'us'], 'integer'],
            [['unit', 'expression', 'conversion_class'], 'safe'],
            [['c_factor', 's_unit'], 'number', 'min'=>0],   //Author: cavesje Date:20151119
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Units::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'c_factor' => $this->c_factor,
            'scope' => $this->scope,
            'us' => $this->us,
            's_unit' => $this->s_unit,
        ]);

        $query->andFilterWhere(['like', 'unit', $this->unit])
            ->andFilterWhere(['like', 'expression', $this->expression])
            ->andFilterWhere(['like', 'conversion_class', $this->conversion_class]);

        return $dataProvider;
    }
}
