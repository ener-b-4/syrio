<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models;

use Yii;
use app\models\events\drafts\EventDraft;
use app\models\events\EventDeclaration;
use yii\db\Exception;

/**
 * This is the model class for table "reporting_history".
 *
 * @property integer $id
 * @property integer $evt_id
 * @property integer $draft_id
 * @property \app\models\events\EventDeclaration $eventObject
 * @property \app\models\events\drafts\EventDraft $draft
 * @property \yii\db\ActiveQuery eventHistory static 
 * @property string $description
 * @property integer $visibility
 *
 * @property EventClassifications $draft
 * @property EventDeclaration $evt
 */
class ReportingHistory extends \yii\db\ActiveRecord
{
    
    const EVENT_REGISTERED = 1;
    const EVENT_FINALIZED = 2;
    const EVENT_SIGNED = 3;
    const EVENT_REOPENED = 4;
    const EVENT_SUBMITTED = 5;
    const EVENT_RESUBMITTED = 6;
    const EVENT_REJECTED = 7;
    const EVENT_ACCEPTED = 8;
    const EVENT_ASSESSED = 9;
    const EVENT_ASSESSED_FOR_CPF = 10;
    const EVENT_SECTION_FINALIZED = 12;
    const EVENT_SECTION_REOPENED = 11;
    const EVENT_DRAFT_CREATED = 13;
    const EVENT_DRAFT_DELETED = 14;
    const EVENT_DRAFT_RESTORED = 15;
    
    const EVENT_CPF_REOPENED = 16;
    const EVENT_CPF_SUSPENDED = 17;
    const EVENT_CPF_RESUMED = 18;
    const EVENT_MA_REOPENED = 19;
    
    const VISIBLE_OO = 1;
    const VISIBLE_CA = 2;
    const VISIBLE_BOTH = 3;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reporting_history';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['evt_id', 'event', 'visibility'], 'required'],
            [['evt_id', 'draft_id', 'event', 'visibility'], 'integer'],
            [['description'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'evt_id' => 'Report ID',
            'draft_id' => 'Draft ID',
            'event' => Yii::t('app', 'Event'),
            'description' => Yii::t('app', 'Description'),
            'visibility' => Yii::t('app', 'Visibility'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDraft()
    {
        return $this->hasOne(EventClassifications::className(), ['id' => 'draft_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIncident()
    {
        return $this->hasOne(EventDeclaration::className(), ['id' => 'evt_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventObject()
    {
        return $this->hasOne(EventDraft::className(), ['id' => 'draft_id']);
    }
    
    
    /**
     * 
     * @param type $evtID
     * @param integer $draftID can be null
     * @throws \yii\web\NotFoundHttpException
     */
    public static function getEventHistory($evtID, $draftID)
    {
        $events = EventDeclaration::find()->where(['evt_id' => $evtID]);
        if (!$event) {
            return null;
        }
        
        if (!isset($draftID))
        {
            //get all the history of a given event reporting
            return $events;
        }
        else
        {
            $events->andWhere(['draft_id'=>$draftID]);
            return $events;
        }
    }
    
    /**
     * 
     * @param integer $evtID
     * @param integer $draftID
     * @param self::constant $event
     * @param type $description
     */
    public static function addHistoryItem($evtID, $draftID, $event, $description, $shift_time = null)
    {
        $newItem = new ReportingHistory();
        $newItem->evt_id = $evtID;
        isset($draftID) ? $newItem->draft_id = $draftID : '';
        $newItem->event = $event;
        $newItem->description = $description;
        
        switch ($event)
        {
            case self::EVENT_REGISTERED:
            case self::EVENT_FINALIZED:
            case self::EVENT_SIGNED:
            case self::EVENT_REOPENED:
            case self::EVENT_SECTION_FINALIZED:
            case self::EVENT_SECTION_REOPENED:
            case self::EVENT_DRAFT_CREATED:
            case self::EVENT_DRAFT_DELETED:
            case self::EVENT_DRAFT_RESTORED:
                $newItem->visibility = self::VISIBLE_OO;
                break;
            case self::EVENT_SUBMITTED:
            case self::EVENT_RESUBMITTED:
            case self::EVENT_REJECTED:
            case self::EVENT_ACCEPTED:
                $newItem->visibility = self::VISIBLE_BOTH;
                break;
            case self::EVENT_ASSESSED:
            case self::EVENT_ASSESSED_FOR_CPF:
            case self::EVENT_CPF_REOPENED:
            case self::EVENT_CPF_RESUMED:
            case self::EVENT_CPF_SUSPENDED:
            case self::EVENT_MA_REOPENED:
                $newItem->visibility = self::VISIBLE_CA;
                break;
        }
        
        if (!isset($shift_time)) {
            $p_time = \time();
        } else {
            $p_time = \time() + $shift_time;;
        }

        //get a free time to set as id
        $ok = 0;
        while ($ok = 0) {
            if (ReportingHistory::findOne(['id'=>$p_time]) !== NULL) {
                $newItem->id = $p_time;
            }
        }
        
        !$description ? '' : $newItem->description = $description;
        
        if (!$newItem->validate())
        {
            if (YII_DEBUG)
            {
                echo '<pre>';
                echo var_dump($newItem->errors);
                echo '</pre>';
                die();
                throw new Exception('validation error in history_item');
            }
            return false;
        }
        
        if ($newItem->save())
        {
            return true;
        } else {
            return false;
        }
        
    }
    
    /**
     * returns the (default) literal description of the evt
     */
    public static function getLogDescription()
    {
        return [
            1 => Yii::t('app', 'event registered to SyRIO'),
            2 => Yii::t('app', 'event finalized'),
            3 => Yii::t('app', 'event signed'),
            4 => Yii::t('app', 'event reopened'),
            5 => Yii::t('app', 'event submitted'),
            6 => Yii::t('app', 'event re-submitted'),
            7 => Yii::t('app', 'event rejected'),
            8 => Yii::t('app', 'event accepted'),
            9 => Yii::t('app', 'event assessed'),
            10 => Yii::t('app', 'event assessed for CPF'),
            11 => Yii::t('app', 'section reopened'),
            12 => Yii::t('app', 'section finalized'),
            13 => Yii::t('app', 'draft created'),
            14 => Yii::t('app', 'draft deleted'),
            15 => Yii::t('app', 'draft restored'),
            16 => Yii::t('app', 'report\'s CPF Assessment reopened'),
            17 => Yii::t('app', 'report\'s CPF Assessment suspended'),
            18 => Yii::t('app', 'report\'s CPF Assessment resumed'),
            19 => Yii::t('app', 'report\'s MA Assessment reopened'),
        ];
    }
    
    
    public function getOffshoreIncident() {
        return $this->hasOne(EventDeclaration::className(), ['id'=>'evt_id']);
    }
    
    
    public static function find() {
        if (\Yii::$app->user->can('sys_admin')) {
            //return all
            return parent::find();
        }
        elseif (\Yii::$app->user->identity->isCaUser) {
            return parent::find()
                    ->where(['visibility'=>self::VISIBLE_CA])
                    ->orWhere(['visibility'=>self::VISIBLE_BOTH]);
        }
        elseif (\Yii::$app->user->identity->isOperatorUser) {
            return parent::find()
                    ->where(['visibility'=>self::VISIBLE_OO])
                    ->orWhere(['visibility'=>self::VISIBLE_BOTH])
                    ->join('inner join', '{{event_declaration}}', '{{event_declaration}}.[[id]] = {{reporting_history}}.[[evt_id]]')
                    ->andWhere(['{{event_declaration}}.[[operator_id]]'=>\Yii::$app->user->identity->org_id]);
                ;
        }
        elseif (\Yii::$app->user->identity->isInstallationUser) {
            return parent::find()
                    ->where(['visibility'=>self::VISIBLE_OO])
                    ->orWhere(['visibility'=>self::VISIBLE_BOTH])
                    ->join('inner join', '{{event_declaration}}', '{{event_declaration}}.[[id]] = {{reporting_history}}.[[evt_id]]')
                    ->join('inner join', '{{installations}}', '{{event_declaration}}.[[installation_id]] = {{installations}}.[[id]]')
                    ->andWhere(['{{event_declaration}}.[[operator_id]]'=>\Yii::$app->user->identity->org_id])
                    ->andWhere(['{{event_declaration}}.[[installation_id]]'=>\Yii::$app->user->identity->inst_id]);
        }
        
        $this->throw(new \yii\web\HttpException(404, 'Page not found'));
    }
    
}
