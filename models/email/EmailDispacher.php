<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\models\email;

use yii\base\Model;
use \Yii;

/**
 * Description of EmailDispacher
 *
 * @author bogdanV
 */
class EmailDispacher extends Model
{
	/**
	 * sendCaUserRegistrationConfirmation
	 * 
	 * @param \app\models\admin\users\SignupCaUserForm $model
	 * @return boolean
	 * 
	 * Sends the emails for user registration
	 */
	public static function sendCaUserRegistrationConfirmation($model, $tempPassword)
	{
		if (Yii::$app->user->can('sys_admin') || Yii::$app->user->can('ca_admin'))
		{
                    
                    //get the mail_language and check if there's a translation view of the email available
                    $lang = isset(\Yii::$app->params['mail_language']) ? \Yii::$app->params['mail_language'] : \Yii::$app->language;
                    $lang_view_path = isset(\Yii::$app->params['mail_language']) ? \Yii::$app->params['mail_language'] . '/' : '';
                    $lang_view_path = '/email-templates/' . $lang_view_path;

                    /*
                     * force email language to match the params value
                     * @since 1.3
                     */
                    $c_lang = \Yii::$app->language;
                    if (isset($lang)) {
                        \Yii::$app->language = $lang;
                    }
                    
                    $message_subject = 'SyRIO - ' . \Yii::t('app', '[SyRIO] - User registration confirmed', null, $lang);
                    
                    $user_view = 'ca_new_user_confirm';
                    
                    //check if there's a translation view of the email available
                    if (file_exists( $_SERVER['DOCUMENT_ROOT'] . '/../views' . $lang_view_path . $user_view . '.php')) {
                        $user_view = $lang_view_path . $user_view;
                    } else {
                        $user_view = '/email-templates/' . $user_view;
                    }
                    
                    
			$emailValidator = new \yii\validators\EmailValidator();
			if (!$emailValidator->validate($model->email, $err))
			{
				$error = $err;
                                /*
                                 * restore the app language
                                 * @since: 1.3
                                 */
                                \Yii::$app->language = $c_lang;
                                
				return false;
			}
			else
			{
				$senderUser = Yii::$app->user->identity->full_name;

				//create and send emails
				$sendResult = Yii::$app->mailer->compose($user_view, ['model' => $model, 'tempPassword' => $tempPassword])
						->setFrom([Yii::$app->user->identity->email => $senderUser])
						->setTo($model->email)
						->setSubject($message_subject)
						->send();

				if (!$sendResult) {
					$error = 'Unable to send confirmation email';
					//$this->addError('send_email', $error);
                                        /*
                                         * restore the app language
                                         * @since: 1.3
                                         */
                                        \Yii::$app->language = $c_lang;
                                        
					return false;
				}			
			}
		}   //check credentials if
		else
		{
			echo 'forbidden to send email';
			die;
		}
		//echo 'email sent';
                /*
                 * restore the app language
                 * @since: 1.3
                 */
                \Yii::$app->language = $c_lang;
		return true;
	}
	
	/**
	 * sendOoUserRegistrationConfirmation
	 * 
	 * @param app\models\User $model
	 * @return boolean
	 * 
	 * Sends the emails for user registration
	 */
	public static function sendOoUserRegistrationConfirmation($model, $tempPassword)
	{
		if (Yii::$app->user->can('sys_admin') || Yii::$app->user->can('oo_admin'))
		{

                    //get the mail_language and check if there's a translation view of the email available
                    $lang = isset(\Yii::$app->params['mail_language']) ? \Yii::$app->params['mail_language'] : \Yii::$app->language;
                    $lang_view_path = isset(\Yii::$app->params['mail_language']) ? \Yii::$app->params['mail_language'] . '/' : '';
                    $lang_view_path = '/email-templates/' . $lang_view_path;

                    /*
                     * force email language to match the params value
                     * @since 1.3
                     */
                    $c_lang = \Yii::$app->language;
                    if (isset($lang)) {
                        \Yii::$app->language = $lang;
                    }
                    
                    $message_subject = 'SyRIO - ' . \Yii::t('app', '[SyRIO] - User registration confirmed', null, $lang);
                    
                    $user_view = 'oo_new_user_confirm';
                    
                    //check if there's a translation view of the email available
                    if (file_exists( $_SERVER['DOCUMENT_ROOT'] . '/../views' . $lang_view_path . $user_view . '.php')) {
                        $user_view = $lang_view_path . $user_view;
                    } else {
                        $user_view = '/email-templates/' . $user_view;
                    }
                    
                    
                    
                        $emailValidator = new \yii\validators\EmailValidator();
			if (!$emailValidator->validate($model->email, $err))
			{
				$error = $err;
                                /*
                                 * restore the app language
                                 * @since: 1.3
                                 */
                                \Yii::$app->language = $c_lang;
				return false;
			}
			else
			{
				$senderUser = Yii::$app->user->identity->full_name;

				//create and send emails
				$sendResult = Yii::$app->mailer->compose($user_view, ['model' => $model, 'tempPassword' => $tempPassword])
						->setFrom([Yii::$app->user->identity->email => $senderUser])
						->setTo($model->email)
						->setSubject($message_subject)
						->send();

				if (!$sendResult) {
					$error = 'Unable to send confirmation email';
					//$this->addError('send_email', $error);
                                        /*
                                         * restore the app language
                                         * @since: 1.3
                                         */
                                        \Yii::$app->language = $c_lang;
					return false;
				}			
			}
		}   //check credentials if
		else
		{
			echo 'forbidden to send email';
			die;
		}
		//echo 'email sent';
                /*
                 * restore the app language
                 * @since: 1.3
                 */
                \Yii::$app->language = $c_lang;
                
		return true;
	}
	
	
	/**
	 * sendInstallationUserRegistrationConfirmation
	 * 
	 * @param app\models\User $model
	 * @return boolean
	 * 
	 * Sends the emails for user registration
	 */
	public static function sendInstallationUserRegistrationConfirmation($model, $tempPassword)
	{
		if (Yii::$app->user->can('sys_admin') || Yii::$app->user->can('oo_admin'))
		{
                    
                    //get the mail_language and check if there's a translation view of the email available
                    $lang = isset(\Yii::$app->params['mail_language']) ? \Yii::$app->params['mail_language'] : \Yii::$app->language;
                    $lang_view_path = isset(\Yii::$app->params['mail_language']) ? \Yii::$app->params['mail_language'] . '/' : '';
                    $lang_view_path = '/email-templates/' . $lang_view_path;
                    /*
                     * force email language to match the params value
                     * @since 1.3
                     */
                    $c_lang = \Yii::$app->language;
                    if (isset($lang)) {
                        \Yii::$app->language = $lang;
                    }

                    $message_subject = 'SyRIO - ' . \Yii::t('app', '[SyRIO] - User registration confirmed', null, $lang);
                    
                    $user_view = 'inst_new_user_confirm';
                    
                    //check if there's a translation view of the email available
                    if (file_exists( $_SERVER['DOCUMENT_ROOT'] . '/../views' . $lang_view_path . $user_view . '.php')) {
                        $user_view = $lang_view_path . $user_view;
                    } else {
                        $user_view = '/email-templates/' . $user_view;
                    }
                    
                    
			$emailValidator = new \yii\validators\EmailValidator();
			if (!$emailValidator->validate($model->email, $err))
			{
				$error = $err;
                                /*
                                 * restore the app language
                                 * @since: 1.3
                                 */
                                \Yii::$app->language = $c_lang;
				return false;
			}
			else
			{
				$senderUser = Yii::$app->user->identity->full_name;

				//create and send emails
				$sendResult = Yii::$app->mailer->compose($user_view, ['model' => $model, 'tempPassword' => $tempPassword])
						->setFrom([Yii::$app->user->identity->email => $senderUser])
						->setTo($model->email)
						->setSubject($message_subject)
						->send();

				if (!$sendResult) {
					$error = 'Unable to send confirmation email';
					//$this->addError('send_email', $error);
                                        /*
                                         * restore the app language
                                         * @since: 1.3
                                         */
                                        \Yii::$app->language = $c_lang;
					return false;
				}			
			}
		}   //check credentials if
		else
		{
			echo 'forbidden to send email';
			die;
		}
		//echo 'email sent';
                /*
                 * restore the app language
                 * @since: 1.3
                 */
                \Yii::$app->language = $c_lang;
		return true;
	}
	
	
	/**
	 * sendPasswordResetInformation
	 * 
	 * @param string $email
	 * @param string $token
	 * @return boolean
	 * 
	 * Sends the emails for password rest
	 */
	public static function sendPasswordResetInformation($email, $token)
	{
            
            //get the mail_language and check if there's a translation view of the email available
            $lang = isset(\Yii::$app->params['mail_language']) ? \Yii::$app->params['mail_language'] : \Yii::$app->language;
            $lang_view_path = isset(\Yii::$app->params['mail_language']) ? \Yii::$app->params['mail_language'] . '/' : '';
            $lang_view_path = '/email-templates/' . $lang_view_path;
            
            /*
             * force email language to match the params value
             * @since 1.3
             */
            $c_lang = \Yii::$app->language;
            if (isset($lang)) {
                \Yii::$app->language = $lang;
            }
                
                $user_view = 'password_reset';
                
                //check if there's a translation view of the email available
                if (file_exists( $_SERVER['DOCUMENT_ROOT'] . '/../views' . $lang_view_path . $user_view . '.php')) {
                    $user_view = $lang_view_path . $user_view;
                } else {
                    $user_view = '/email-templates/' . $user_view;
                }
            
		$emailValidator = new \yii\validators\EmailValidator();
		if (!$emailValidator->validate($email, $err))
		{
			$error = $err;
                        /*
                         * restore the app language
                         * @since: 1.3
                         */
                        \Yii::$app->language = $c_lang;
			return false;
		}

		$senderUser = Yii::$app->params['adminEmail'];
		//$url = \yii\helpers\Url::to(['site/verify-password-reset-token', 'token' => $token]);
		
		$url=Yii::$app->urlManager->createAbsoluteUrl(['site/verify-password-reset-token', 'token' => $token]);

		//create and send emails
		$sendResult = Yii::$app->mailer->compose($user_view, ['url'=>$url])
				//->setFrom([Yii::$app->user->identity->email => $senderUser])
				->setFrom([$senderUser])
				->setTo($email)
				->setSubject('[SyRIO] - ' . \Yii::t('app', 'password reset', null, $lang))
				->send();

		if (!$sendResult) {
			$error = 'Unable to send confirmation email';
			//$this->addError('send_email', $error);
                        /*
                         * restore the app language
                         * @since: 1.3
                         */
                        \Yii::$app->language = $c_lang;
			return false;
		}			
		
                /*
                 * restore the app language
                 * @since: 1.3
                 */
                \Yii::$app->language = $c_lang;
                
		return true;
	}

	/**
	 * 
	 * @param \app\models\requests\Request $model
	 */
	public static function sendUserRegistrationRequest($model)
	{
            //get the mail_language and check if there's a translation view of the email available
            $lang = isset(\Yii::$app->params['mail_language']) ? \Yii::$app->params['mail_language'] : \Yii::$app->language;
            $lang_view_path = isset(\Yii::$app->params['mail_language']) ? \Yii::$app->params['mail_language'] . '/' : '';
            $lang_view_path = '/email-templates/' . $lang_view_path;
            /*
             * force email language to match the params value
             * @since 1.3
             */
            $c_lang = \Yii::$app->language;
            if (isset($lang)) {
                \Yii::$app->language = $lang;
            }
            
		$message_subject = '';
		//get the request type
		switch ($model->request_type)
		{
			case \app\models\requests\RequestTypes::NEW_OO_USER:
				$message_subject = 'SyRIO - ' . \Yii::t('app', 'Operators/Owners user registration request', null, $lang);
				break;
			case \app\models\requests\RequestTypes::NEW_CA_USER:
				$message_subject = 'SyRIO - ' . \Yii::t('app', 'Competent Authority user registration request', null, $lang);
				break;
			case \app\models\requests\RequestTypes::NEW_INST_USER:
				$message_subject = 'SyRIO - ' . \Yii::t('app', 'Installation user registration request', null, $lang);
				break;
		}
		
		$admin_view = 'user-registration-request-admin';
		$user_view = 'user-registration-request-user';
		$request_array = json_decode($model->content, true);

                //check if there's a translation view of the email available
                if (file_exists( $_SERVER['DOCUMENT_ROOT'] . '/../views' . $lang_view_path . $admin_view . '.php')) {
                    $admin_view = $lang_view_path . $admin_view;
                } else {
                    $admin_view = '/email-templates/' . $admin_view;
                }

                if (file_exists( $_SERVER['DOCUMENT_ROOT'] . '/../views' . $lang_view_path . $user_view . '.php')) {
                    $user_view = $lang_view_path . $user_view;
                } else {
                    $user_view = '/email-templates/' . $user_view;
                }
                
                
		$to_admin = Yii::$app->params['adminEmail'];
		$to_user = $request_array['email']; 
		$emailValidator = new \yii\validators\EmailValidator();
		if (!$emailValidator->validate($to_user, $err))
		{
			$error = $err;
                        /*
                         * restore the app language
                         * @since: 1.3
                         */
                        \Yii::$app->language = $c_lang;
			return false;
		}

		$from = Yii::$app->params['adminEmail'];
		
		//create and send emails
		$url=Yii::$app->urlManager->createAbsoluteUrl(['requests/request/view', 'id' => $model->id]);
		$sendResult = Yii::$app->mailer->compose($admin_view, ['model'=>$model, 'url'=>$url])
				//->setFrom([Yii::$app->user->identity->email => $senderUser])
				->setFrom([$from])
				->setTo($to_admin)
				->setSubject($message_subject)
				->send();

		$sendResult2 = Yii::$app->mailer->compose($user_view, ['model'=>$model])
				//->setFrom([Yii::$app->user->identity->email => $senderUser])
				->setFrom([$from])
				->setTo($to_user)
				->setSubject($message_subject)
				->send();
		
		if (!$sendResult || !$sendResult2) {
			$error = 'Unable to send confirmation emails';
			//$this->addError('send_email', $error);
                        /*
                         * restore the app language
                         * @since: 1.3
                         */
                        \Yii::$app->language = $c_lang;
			return false;
		}			
		
                /*
                 * restore the app language
                 * @since: 1.3
                 */
                \Yii::$app->language = $c_lang;
                
		return true;
	}
	
	/**
	* 
	* @param \app\models\requests\Request $model
	*/
	public static function sendRequestProcessed($model)
	{
            //get the mail_language and check if there's a translation view of the email available
            $lang = isset(\Yii::$app->params['mail_language']) ? \Yii::$app->params['mail_language'] : \Yii::$app->language;
            $lang_view_path = isset(\Yii::$app->params['mail_language']) ? \Yii::$app->params['mail_language'] . '/' : '';
            $lang_view_path = '/email-templates/' . $lang_view_path;
            
            /*
             * force email language to match the params value
             * @since 1.3
             */
            $c_lang = \Yii::$app->language;
            if (isset($lang)) {
                \Yii::$app->language = $lang;
            }
            
		$message_subject = '';
		//get the request type
		switch ($model->request_type)
		{
			case \app\models\requests\RequestTypes::NEW_OO_USER:
				$message_subject = \Yii::t('app', '[SyRIO] Request #{evt1} - {evt2} user registration request', [
                                    'evt1'=>$model->id,
                                    'evt2'=>\Yii::t('app/crf', 'Operator/owner', null, $lang)
                                ], $lang);
				break;
			case \app\models\requests\RequestTypes::NEW_CA_USER:
				$message_subject = \Yii::t('app', '[SyRIO] Request #{evt1} - {evt2} user registration request', [
                                    'evt1'=>$model->id,
                                    'evt2'=>\Yii::t('app', 'Competent Authority', null, $lang)
                                ], $lang);
				break;
			case \app\models\requests\RequestTypes::NEW_INST_USER:
				$message_subject = \Yii::t('app', '[SyRIO] Request #{evt1} - {evt2} user registration request', [
                                    'evt1'=>$model->id,
                                    'evt2'=>ucfirst(\Yii::t('app', 'installation'), null, $lang)
                                ], $lang);
				break;			
		}
		
		$user_view = 'request_processed_view';
		$request_array = json_decode($model->content, true);
		
                //check if there's a translation view of the email available
				
                if (file_exists( $_SERVER['DOCUMENT_ROOT'] . '/../views' . $lang_view_path . $user_view . '.php')) {
                    $user_view = $lang_view_path . $user_view;
                } else {
                    $user_view = '/email-templates/' . $user_view;
                }
                
                
		$from = Yii::$app->params['adminEmail'];
		
		$to_user = $request_array['email']; 
		$emailValidator = new \yii\validators\EmailValidator();
		if (!$emailValidator->validate($to_user, $err))
		{
			$error = $err;
                        /*
                         * restore the app language
                         * @since: 1.3
                         */
                        \Yii::$app->language = $c_lang;
			return false;
		}
		
		//create and send emails
		$sendResult = Yii::$app->mailer->compose($user_view, ['model'=>$model])
				//->setFrom([Yii::$app->user->identity->email => $senderUser])
				->setFrom([$from])
				->setTo($to_user)
				->setSubject($message_subject)
				->send();
		
		if (!$sendResult) {
			$error = 'Unable to send confirmation emails';
			//$this->addError('send_email', $error);
                        /*
                         * restore the app language
                         * @since: 1.3
                         */
                        \Yii::$app->language = $c_lang;
			return false;
		}			
		
                /*
                 * restore the app language
                 * @since: 1.3
                 */
                \Yii::$app->language = $c_lang;
		return true;		
	}
		
	/**
	* 
	* @param \app\models\requests\Request $model
	*/
	public static function sendRequestReopened($model)
	{
            //get the mail_language and check if there's a translation view of the email available
            $lang = isset(\Yii::$app->params['mail_language']) ? \Yii::$app->params['mail_language'] : \Yii::$app->language;
            $lang_view_path = isset(\Yii::$app->params['mail_language']) ? \Yii::$app->params['mail_language'] . '/' : '';
            $lang_view_path = '/email-templates/' . $lang_view_path;
            
                /*
                 * force email language to match the params value
                 * @since 1.3
                 */
                $c_lang = \Yii::$app->language;
                if (isset($lang)) {
                    \Yii::$app->language = $lang;
                }
            
                $message_subject = \Yii::t('app', '[SyRIO] Request #{evt1} - re-opened', [
                    'evt1'=>$model->id,
                ], $lang);
                
		$user_view = 'request_reopened_view';
		$request_array = json_decode($model->content, true);

                //check if there's a translation view of the email available
                if (file_exists( $_SERVER['DOCUMENT_ROOT'] . '/../views' . $lang_view_path . $user_view . '.php')) {
                    $user_view = $lang_view_path . $user_view;
                } else {
                    $user_view = '/email-templates/' . $user_view;
                }
                
                
		$from = Yii::$app->params['adminEmail'];
		
		$to_user = $request_array['email']; 
		$emailValidator = new \yii\validators\EmailValidator();
		if (!$emailValidator->validate($to_user, $err))
		{
			$error = $err;
                        /*
                         * restore the app language
                         * @since: 1.3
                         */
                        \Yii::$app->language = $c_lang;
			return false;
		}
		
		//create and send emails
		$sendResult = Yii::$app->mailer->compose($user_view, ['model'=>$model])
				//->setFrom([Yii::$app->user->identity->email => $senderUser])
				->setFrom([$from])
				->setTo($to_user)
				->setSubject($message_subject)
				->send();
		
		if (!$sendResult) {
			$error = 'Unable to send confirmation emails';
			//$this->addError('send_email', $error);
                        /*
                         * restore the app language
                         * @since: 1.3
                         */
                        \Yii::$app->language = $c_lang;
			return false;
		}			
		
                /*
                 * restore the app language
                 * @since: 1.3
                 */
                \Yii::$app->language = $c_lang;
		return true;		
	}
	
	/**
	 * 
	 * @param \app\models\requests\Request $model
	 */
	public static function sendOrganizationRegistrationRequest($model)
	{
            //get the mail_language and check if there's a translation view of the email available
            $lang = isset(\Yii::$app->params['mail_language']) ? \Yii::$app->params['mail_language'] : \Yii::$app->language;
            $lang_view_path = isset(\Yii::$app->params['mail_language']) ? \Yii::$app->params['mail_language'] . '/' : '';
            $lang_view_path = '/email-templates/' . $lang_view_path;
            
            /*
             * force email language to match the params value
             * @since 1.3
             */
            $c_lang = \Yii::$app->language;
            if (isset($lang)) {
                \Yii::$app->language = $lang;
            }
            
            
		$message_subject = '';
		//get the request type
		switch ($model->request_type)
		{
			case \app\models\requests\RequestTypes::NEW_CA_ORG:
				$message_subject = \Yii::t('app', '[SyRIO] Request #{evt1} - {evt2} registration request', [
                                    'evt1'=>$model->id,
                                    'evt2'=>\Yii::t('app', 'Competent Authority Organization', null, $lang)
                                ], $lang);
				break;
			case \app\models\requests\RequestTypes::NEW_OO_ORG:
				$message_subject = \Yii::t('app', '[SyRIO] Request #{evt1} - {evt2} registration request', [
                                    'evt1'=>$model->id,
                                    'evt2'=>\Yii::t('app', 'Operators / Owners Organization', null, $lang)
                                ], $lang);
				break;
			default:
                                /*
                                 * restore the app language
                                 * @since: 1.3
                                 */
                                \Yii::$app->language = $c_lang;
				return false;
				break;
		}
		
		/*
		 * (vamanbo) 20150913
		 * 
		 * NB for this version ALL the messages are standard
		 * This is why this points to 'user-registration...'
		 * 
		 */
		$admin_view = 'user-registration-request-admin';
		$user_view = 'user-registration-request-user';
		$request_array = json_decode($model->content, true);
		
                //check if there's a translation view of the email available
                if (file_exists( $_SERVER['DOCUMENT_ROOT'] . '/../views' . $lang_view_path . $admin_view . '.php')) {
                    $admin_view = $lang_view_path . $admin_view;
                } else {
                    $admin_view = '/email-templates/' . $admin_view;
                }

                if (file_exists( $_SERVER['DOCUMENT_ROOT'] . '/../views' . $lang_view_path . $user_view . '.php')) {
                    $user_view = $lang_view_path . $user_view;
                } else {
                    $user_view = '/email-templates/' . $user_view;
                }
                
		$to_admin = Yii::$app->params['adminEmail'];
		$to_user = $request_array['email']; 
		$emailValidator = new \yii\validators\EmailValidator();
		if (!$emailValidator->validate($to_user, $err))
		{
			$error = $err;
                        /*
                         * restore the app language
                         * @since: 1.3
                         */
                        \Yii::$app->language = $c_lang;
			return false;
		}

		$from = Yii::$app->params['adminEmail'];
		
		//create and send emails
		$url=Yii::$app->urlManager->createAbsoluteUrl(['requests/request/view', 'id' => $model->id]);
		$sendResult = Yii::$app->mailer->compose($admin_view, ['model'=>$model, 'url'=>$url])
				//->setFrom([Yii::$app->user->identity->email => $senderUser])
				->setFrom([$from])
				->setTo($to_admin)
				->setSubject($message_subject)
				->send();

		$sendResult2 = Yii::$app->mailer->compose($user_view, ['model'=>$model])
				//->setFrom([Yii::$app->user->identity->email => $senderUser])
				->setFrom([$from])
				->setTo($to_user)
				->setSubject($message_subject)
				->send();
		
		if (!$sendResult || !$sendResult2) {
			$error = 'Unable to send confirmation emails';
			//$this->addError('send_email', $error);
                        /*
                         * restore the app language
                         * @since: 1.3
                         */
                        \Yii::$app->language = $c_lang;
                        
			return false;
		}			
		
                /*
                 * restore the app language
                 * @since: 1.3
                 */
                \Yii::$app->language = $c_lang;
		return true;
	}
		
	
	
	/**
	 * 
	 * @param \app\models\requests\Request $model
	 */
	public static function sendInstallationRegistrationRequest($model)
	{
                //get the mail_language and check if there's a translation view of the email available
                $lang = isset(\Yii::$app->params['mail_language']) ? \Yii::$app->params['mail_language'] : \Yii::$app->language;
                $lang_view_path = isset(\Yii::$app->params['mail_language']) ? \Yii::$app->params['mail_language'] . '/' : '';
                $lang_view_path = '/email-templates/' . $lang_view_path;

                /*
                 * force email language to match the params value
                 * @since 1.3
                 */
                $c_lang = \Yii::$app->language;
                if (isset($lang)) {
                    \Yii::$app->language = $lang;
                }
                
                $message_subject = \Yii::t('app', '[SyRIO] Request #{evt1} - {evt2} registration request', [
                    'evt1'=>$model->id,
                    'evt2'=>\Yii::t('app', 'installation', null, $lang)
                ], $lang);
		
		/*
		 * (vamanbo) 20150913
		 * 
		 * NB for this version ALL the messages are standard
		 * This is why this points to 'user-registration...'
		 * 
		 */
		$admin_view = 'user-registration-request-admin';
		$user_view = 'user-registration-request-user';
		$request_array = json_decode($model->content, true);
		
                //check if there's a translation view of the email available
				//the file to check:
				//	$_SERVER['DOCUMENT_ROOT'] . '/../views' . $lang_view_path . $admin_view . '.php'
				
                if (file_exists( $_SERVER['DOCUMENT_ROOT'] . '/../views' . $lang_view_path . $admin_view . '.php')) {
                    $admin_view = $lang_view_path . $admin_view;
                } else {
                    $admin_view = '/email-templates/' . $admin_view;
                }

                if (file_exists( $_SERVER['DOCUMENT_ROOT'] . '/../views' . $lang_view_path . $user_view . '.php')) {
                    $user_view = $lang_view_path . $user_view;
                } else {
                    $user_view = '/email-templates/' . $user_view;
                }
                
		$to_admin = Yii::$app->params['adminEmail'];
		$to_user = $request_array['email']; 
		$emailValidator = new \yii\validators\EmailValidator();
		if (!$emailValidator->validate($to_user, $err))
		{
			$error = $err;
                        /*
                         * restore the app language
                         * @since: 1.3
                         */
                        \Yii::$app->language = $c_lang;
			return false;
		}

		$from = Yii::$app->params['adminEmail'];
		
		//create and send emails
		$url=Yii::$app->urlManager->createAbsoluteUrl(['requests/request/view', 'id' => $model->id]);
		$sendResult = Yii::$app->mailer->compose($admin_view, ['model'=>$model, 'url'=>$url])
				//->setFrom([Yii::$app->user->identity->email => $senderUser])
				->setFrom([$from])
				->setTo($to_admin)
				->setSubject($message_subject)
				->send();

		$sendResult2 = Yii::$app->mailer->compose($user_view, ['model'=>$model])
				//->setFrom([Yii::$app->user->identity->email => $senderUser])
				->setFrom([$from])
				->setTo($to_user)
				->setSubject($message_subject)
				->send();
		
		if (!$sendResult || !$sendResult2) {
			$error = 'Unable to send confirmation emails';
			//$this->addError('send_email', $error);
                        /*
                         * restore the app language
                         * @since: 1.3
                         */
                        \Yii::$app->language = $c_lang;
			return false;
		}			
		
                /*
                 * restore the app language
                 * @since: 1.3
                 */
                \Yii::$app->language = $c_lang;
		return true;
	}	
        
        
	/**
	 * 
	 * @param \app\models\ca\IncidentCategorization $model the the ca report that has been submitted
	 */
	public static function sendOoSubmitsReport($model)
	{
            
            //get the mail_language and check if there's a translation view of the email available
            $lang = isset(\Yii::$app->params['mail_language']) ? \Yii::$app->params['mail_language'] : \Yii::$app->language;
            $lang_view_path = isset(\Yii::$app->params['mail_language']) ? \Yii::$app->params['mail_language'] . '/' : '';
            $lang_view_path = '/email-templates/' . $lang_view_path;
            
            /*
             * force email language to match the params value
             * @since 1.3
             */
            $c_lang = \Yii::$app->language;
            if (isset($lang)) {
                \Yii::$app->language = $lang;
            }
            
            
                //get the stuff from incident
                $event = $model->draft->event;
                $draft = $model->draft;
                
                //compute the values related to reporting dates
                $event_date = new \DateTime($event->event_date_time);
                $date_reported = new \DateTime($model->op_submitted_at);
                $deadline_date = new \DateTime($event->deadline);
                
                $interval = date_diff($date_reported, $deadline_date, false);      //should be positive to be in time
                $deadline_interval = date_diff($deadline_date, $event_date, false);      //should be positive to be in time
                
                $details = [];
                $details['date_of_submission'] = date_format($date_reported, 'd M Y H:i:s');
                $details['deadline'] = date_format($deadline_date, 'd M Y H:i:s');
                
                $details['deadline_interval'] = $deadline_interval->format('%a days %h hours %i minutes');
                //$details['deadline_interval'] = $deadline_interval;
                
                if ($deadline_interval->invert = 1) {
                    $details['late'] = '<span style="color: green">'. \Yii::t('app', 'in due time', null, $lang) . '</span>';
                    $details['to_deadline'] = $interval->format('%a days %h hours %i minutes') . ' ' . \Yii::t('app', 'to deadline', null, $lang);      //should be positive to be in time;
                } else {
                    $details['late'] = '<span style="color: red">'. \Yii::t('app', 'late', null, $lang) . '</span>';
                    $details['to_deadline'] = $interval->format('%a days %h hours %i minutes') . ' ' . \Yii::t('app', 'over the deadline', null, $lang);      //should be positive to be in time;
                }

                //get the type of the event and populate the array
                $event_types = [];
                foreach(range('a', 'j') as $letter) {
                    $is_attr = 'is_'.$letter;
                    if ($draft->$is_attr) {
                        $upper = strtoupper($letter);
                        $event_types[] = $upper . '. ' . \app\models\crf\CrfHelper::SectionsTitle($lang)[$upper];
                    }
                }
                
                $details['event_types'] = $event_types;
                $details['installation'] = $event->installation->name;

		$message_subject = \Yii::t('app', '[SyRIO] OFFSHORE INCIDENT REPORTED - {evt1}/{evt2}/{evt3}', [
                    'evt1' => $event->organization->organization_acronym,
                    'evt2' => $event->installation->name,
                    'evt3' => date_format($event_date, 'Y-m-d H:i:s')
                ], null, $lang);
                
		
		/*
		 * (vamanbo) 20160225
		 */
                
                $mail_view = 'oo_reports_incident';
                
                //check if there's a translation view of the email available
                if (file_exists( $_SERVER['DOCUMENT_ROOT'] . '/../views' . $lang_view_path . $mail_view . '.php')) {
                    $mail_view = $lang_view_path . $mail_view;
                } else {
                    $mail_view = '/email-templates/' . $mail_view;
                }
                
                
                
                
                
                //get the list of the people to send this information to (ca_assessors)
                $tos = \app\models\User::find()
                        ->join('join', 'auth_assignment', '{{auth_users}}.[[id]] = {{auth_assignment}}.[[user_id]]')
                        ->where(['{{auth_users}}.[[status]]' => 10])
                        ->andWhere(['{{auth_assignment}}.[[item_name]]' => 'ca_assessor'])
                        ->select(['concat_ws(" ", first_names, last_name) as name', 'email'])
                        ->asArray()->all();
                
                
		$emailValidator = new \yii\validators\EmailValidator();
                $to_users = [];
                
                foreach ($tos as $to) {
                    if ($emailValidator->validate($to['email'], $err)) {
                        $to_users[$to['email']] = $to['name'];
                    }
                }
                
		$from = Yii::$app->params['adminEmail'];
		
		$sendResult = Yii::$app->mailer->compose($mail_view, ['model'=>$model, 'details'=>$details])
				//->setFrom([Yii::$app->user->identity->email => $senderUser])
				->setFrom([$from => 'SyRIO'])
				->setTo($to_users)
				->setSubject($message_subject)
                                ->setTextBody('')
				->send();
                
		if (!$sendResult) {
			$error = 'Unable to send confirmation emails';
			//$this->addError('send_email', $error);
                        /*
                         * restore the app language
                         * @since: 1.3
                         */
                        \Yii::$app->language = $c_lang;
                        
			return false;
		}			
		
                /*
                 * restore the app language
                 * @since: 1.3
                 */
                \Yii::$app->language = $c_lang;
		return true;
	}	
        
	/**
	 * 
	 * @param \app\models\ca\IncidentCategorization $model the the ca report that has been submitted
	 */
	public static function sendOoReSubmitsReport($model)
	{

            //get the mail_language and check if there's a translation view of the email available
            $lang = isset(\Yii::$app->params['mail_language']) ? \Yii::$app->params['mail_language'] : \Yii::$app->language;
            $lang_view_path = isset(\Yii::$app->params['mail_language']) ? \Yii::$app->params['mail_language'] . '/' : '';
            $lang_view_path = '/email-templates/' . $lang_view_path;

            /*
             * force email language to match the params value
             * @since 1.3
             */
            $c_lang = \Yii::$app->language;
            if (isset($lang)) {
                \Yii::$app->language = $lang;
            }
            
            
                //get the stuff from incident
                $event = $model->draft->event;
                $draft = $model->draft;
                
                //compute the values related to reporting dates
                $event_date = new \DateTime($event->event_date_time);
                $date_reported = new \DateTime($model->op_submitted_at);
                $deadline_date = new \DateTime($event->deadline);
                
                $interval = date_diff($date_reported, $deadline_date, false);      //should be positive to be in time
                $deadline_interval = date_diff($deadline_date, $event_date, false);      //should be positive to be in time
                
                $details = [];
                
                //only at resubmission
                $details['title'] = $event->organization->organization_acronym . '/' .
                        $event->installation->name . '/' . date_format($event_date, 'Y-m-d H:i:s');

                $details['justification'] = $model->rejection_desc;
                
                $details['date_of_submission'] = date_format($date_reported, 'd M Y H:i:s');
                $details['deadline'] = date_format($deadline_date, 'd M Y H:i:s');
                
                $details['deadline_interval'] = $deadline_interval->format('%a days %h hours %i minutes');
                //$details['deadline_interval'] = $deadline_interval;
                
                if ($deadline_interval->invert = 1) {
                    $details['late'] = '<span style="color: green">'. \Yii::t('app', 'in due time', null, $lang) . '</span>';
                    $details['to_deadline'] = $interval->format('%a days %h hours %i minutes') . ' ' . \Yii::t('app', 'to deadline', null, $lang);      //should be positive to be in time;
                } else {
                    $details['late'] = '<span style="color: red">'. \Yii::t('app', 'late', null, $lang) . '</span>';
                    $details['to_deadline'] = $interval->format('%a days %h hours %i minutes') . ' ' . \Yii::t('app', 'over the deadline', null, $lang);      //should be positive to be in time;
                }

                //get the type of the event and populate the array
                $event_types = [];
                foreach(range('a', 'j') as $letter) {
                    $is_attr = 'is_'.$letter;
                    if ($draft->$is_attr) {
                        $upper = strtoupper($letter);
                        $event_types[] = $upper . '. ' . \app\models\crf\CrfHelper::SectionsTitle($lang)[$upper];
                    }
                }
                
                $details['event_types'] = $event_types;
                $details['installation'] = $event->installation->name;
                
		$message_subject = \Yii::t('app', '[SyRIO] OFFSHORE INCIDENT RESUBMITTED - {evt1}/{evt2}/{evt3}', [
                    'evt1' => $event->organization->organization_acronym,
                    'evt2' => $event->installation->name,
                    'evt3' => date_format($event_date, 'Y-m-d H:i:s')
                ], null, $lang);
                
		/*
		 * (vamanbo) 20160225
		 */
                
                $mail_view = 'oo_resubmits_incident';
                
                //check if there's a translation view of the email available
                if (file_exists( $_SERVER['DOCUMENT_ROOT'] . '/../views' . $lang_view_path . $mail_view . '.php')) {
                    $mail_view = $lang_view_path . $mail_view;
                } else {
                    $mail_view = '/email-templates/' . $mail_view;
                }
                
                //get the list of the people to send this information to (ca_assessors)
                $tos = \app\models\User::find()
                        ->join('join', 'auth_assignment', '{{auth_users}}.[[id]] = {{auth_assignment}}.[[user_id]]')
                        ->where(['{{auth_users}}.[[status]]' => 10])
                        ->andWhere(['{{auth_assignment}}.[[item_name]]' => 'ca_assessor'])
                        ->select(['concat_ws(" ", first_names, last_name) as name', 'email'])
                        ->asArray()->all();
                
                
		$emailValidator = new \yii\validators\EmailValidator();
                $to_users = [];
                
                foreach ($tos as $to) {
                    if ($emailValidator->validate($to['email'], $err)) {
                        $to_users[$to['email']] = $to['name'];
                    }
                }
                
		$from = Yii::$app->params['adminEmail'];
		
		$sendResult = Yii::$app->mailer->compose($mail_view, ['model'=>$model, 'details'=>$details])
				//->setFrom([Yii::$app->user->identity->email => $senderUser])
				->setFrom([$from => 'SyRIO'])
				->setTo($to_users)
				->setSubject($message_subject)
                                ->setTextBody('')
				->send();
                
		if (!$sendResult) {
			$error = 'Unable to send confirmation emails';
			//$this->addError('send_email', $error);
                        /*
                         * restore the app language
                         * @since: 1.3
                         */
                        \Yii::$app->language = $c_lang;
			return false;
		}			
		
                /*
                 * restore the app language
                 * @since: 1.3
                 */
                \Yii::$app->language = $c_lang;
		return true;
	}	
        
	/**
	 * 
	 * @param \app\models\ca\IncidentCategorization $model the the ca report that has been submitted
         * @since 1.3
	 */
	public static function sendCaRejectsReport($model)
	{
            
            //get the mail_language and check if there's a translation view of the email available
            $lang = isset(\Yii::$app->params['mail_language']) ? \Yii::$app->params['mail_language'] : \Yii::$app->language;
            $lang_view_path = isset(\Yii::$app->params['mail_language']) ? \Yii::$app->params['mail_language'] . '/' : '';
            $lang_view_path = '/email-templates/' . $lang_view_path;

            /*
             * force email language to match the params value
             * @since 1.3
             */
            $c_lang = \Yii::$app->language;
            if (isset($lang)) {
                \Yii::$app->language = $lang;
            }
            
            //get the stuff from incident
            $event = $model->draft->event;
            $draft = $model->draft;
            $event_date = new \DateTime($event->event_date_time);
            $date = date('Y-m-d H:i:s');    //the date to sync the reject message date
                        
            /*
             * THE CA PART
             */
                
		$message_subject = \Yii::t('app', '[SyRIO] OFFSHORE INCIDENT REJECTED - {evt1}', [
                    'evt1' => $model->name,
                ], null, $lang);
                
                $mail_view = 'ca_rejects_report_ca';
                
                //check if there's a translation view of the email available
                if (file_exists( $_SERVER['DOCUMENT_ROOT'] . '/../views' . $lang_view_path . $mail_view . '.php')) {
                    $mail_view = $lang_view_path . $mail_view;
                } else {
                    $mail_view = '/email-templates/' . $mail_view;
                }
                
                //get the list of the people to send this information to (ca_assessors)
                $tos = \app\models\User::find()
                        ->join('join', 'auth_assignment', '{{auth_users}}.[[id]] = {{auth_assignment}}.[[user_id]]')
                        ->where(['{{auth_users}}.[[status]]' => 10])
                        ->andWhere(['{{auth_assignment}}.[[item_name]]' => 'ca_assessor'])
                        ->select(['concat_ws(" ", first_names, last_name) as name', 'email'])
                        ->asArray()->all();
                
		$emailValidator = new \yii\validators\EmailValidator();
                $to_users = [];
                
                foreach ($tos as $to) {
                    if ($emailValidator->validate($to['email'], $err)) {
                        $to_users[$to['email']] = $to['name'];
                    }
                }
                
		$from = Yii::$app->params['adminEmail'];
	
                $details = [];
                $details['assessor_name'] = \Yii::$app->user->identity->full_name;
                $details['date'] = $date;
                
                //get the type of the event and populate the array
                $event_types = [];
                foreach(range('a', 'j') as $letter) {
                    $is_attr = 'is_'.$letter;
                    if ($draft->$is_attr) {
                        $upper = strtoupper($letter);
                        $event_types[] = $upper . '. ' . \app\models\crf\CrfHelper::SectionsTitle($lang)[$upper];
                    }
                }
                
                $details['event_types'] = $event_types;
                $details['installation'] = $event->installation->name;
                
		$sendResult = Yii::$app->mailer->compose($mail_view, ['model'=>$model, 'details'=>$details])
				//->setFrom([Yii::$app->user->identity->email => $senderUser])
				->setFrom([$from => 'SyRIO'])
				->setTo($to_users)
				->setSubject($message_subject)
                                ->setTextBody('')
				->send();
                
		if (!$sendResult) {
			$error = 'Unable to send the notification emails';
			//$this->addError('send_email', $error);
                        /*
                         * restore the app language
                         * @since: 1.3
                         */
                        \Yii::$app->language = $c_lang;
			return false;
		}			

                /*
                 * PREPARE THE MESSAGE TO THE OO
                 */
		$message_subject = \Yii::t('app', '[SyRIO] OFFSHORE INCIDENT REJECTED - {evt1}', [
                    'evt1' => $event->event_name,
                ], null, $lang);
                
                $mail_view = 'ca_rejects_report_oo';
                
                //check if there's a translation view of the email available
                if (file_exists( $_SERVER['DOCUMENT_ROOT'] . '/../views' . $lang_view_path . $mail_view . '.php')) {
                    $mail_view = $lang_view_path . $mail_view;
                } else {
                    $mail_view = '/email-templates/' . $mail_view;
                }
                
                //get the email of the guy submitting the report
                //the guy that submitted the report is the one who last modified the submitted draft!
                $tos = \app\models\User::find()
                        ->where(['{{auth_users}}.[[status]]' => 10])
                        ->andWhere(['{{auth_users}}.[[id]]' => $model->draft->modified_by])
                        ->select(['concat_ws(" ", first_names, last_name) as name', 'email'])
                        ->asArray()->all();
                
		$emailValidator = new \yii\validators\EmailValidator();
                $to_users = [];
                
                foreach ($tos as $to) {
                    if ($emailValidator->validate($to['email'], $err)) {
                        $to_users[$to['email']] = $to['name'];
                    }
                }
                
                if (count($to) === 0) //maybe the rapporteur got inactivated since report has been submitted
                {
                    /*
                     * restore the app language
                     * @since: 1.3
                     */
                    \Yii::$app->language = $c_lang;
                    return true;
                }
                
		$from = Yii::$app->params['adminEmail'];
	
                $details = [];
                $details['name'] = $event->event_name;
                $details['date'] = $date;            
                
		$sendResult = Yii::$app->mailer->compose($mail_view, ['model'=>$model, 'details'=>$details])
				//->setFrom([Yii::$app->user->identity->email => $senderUser])
				->setFrom([$from => 'SyRIO'])
				->setTo($to_users)
				->setSubject($message_subject)
                                ->setTextBody('')
				->send();
                
		if (!$sendResult) {
			$error = 'Unable to send the notification emails';
			//$this->addError('send_email', $error);
                        /*
                         * restore the app language
                         * @since: 1.3
                         */
                        \Yii::$app->language = $c_lang;
			return false;
		}			
                
                /*
                 * restore the app language
                 * @since: 1.3
                 */
                \Yii::$app->language = $c_lang;
                
		return true;
	}	

	/**
	 * 
	 * @param \app\models\ca\IncidentCategorization $model the the ca report that has been submitted
         * @since 1.3
	 */
	public static function sendCaAcceptsReport($model)
	{
            
            //get the mail_language and check if there's a translation view of the email available
            $lang = isset(\Yii::$app->params['mail_language']) ? \Yii::$app->params['mail_language'] : \Yii::$app->language;
            $lang_view_path = isset(\Yii::$app->params['mail_language']) ? \Yii::$app->params['mail_language'] . '/' : '';
            $lang_view_path = '/email-templates/' . $lang_view_path;

            /*
             * force email language to match the params value
             * @since 1.3
             */
            $c_lang = \Yii::$app->language;
            if (isset($lang)) {
                \Yii::$app->language = $lang;
            }
            
            //get the stuff from incident
            $event = $model->draft->event;
            $draft = $model->draft;
            $event_date = new \DateTime($event->event_date_time);
            $date = date('Y-m-d H:i:s');    //the date to sync the reject message date
                        
            /*
             * THE CA PART
             */
                
		$message_subject = \Yii::t('app', '[SyRIO] OFFSHORE INCIDENT ACCEPTED - {evt1}', [
                    'evt1' => $model->name,
                ], null, $lang);
                
                $mail_view = 'ca_accepts_report_ca';
                
                //check if there's a translation view of the email available
                if (file_exists( $_SERVER['DOCUMENT_ROOT'] . '/../views' . $lang_view_path . $mail_view . '.php')) {
                    $mail_view = $lang_view_path . $mail_view;
                } else {
                    $mail_view = '/email-templates/' . $mail_view;
                }
                
                //get the list of the people to send this information to (ca_assessors)
                $tos = \app\models\User::find()
                        ->join('join', 'auth_assignment', '{{auth_users}}.[[id]] = {{auth_assignment}}.[[user_id]]')
                        ->where(['{{auth_users}}.[[status]]' => 10])
                        ->andWhere(['{{auth_assignment}}.[[item_name]]' => 'ca_assessor'])
                        ->select(['concat_ws(" ", first_names, last_name) as name', 'email'])
                        ->asArray()->all();
                
		$emailValidator = new \yii\validators\EmailValidator();
                $to_users = [];
                
                foreach ($tos as $to) {
                    if ($emailValidator->validate($to['email'], $err)) {
                        $to_users[$to['email']] = $to['name'];
                    }
                }
                
		$from = Yii::$app->params['adminEmail'];
	
                $details = [];
                $details['assessor_name'] = \Yii::$app->user->identity->full_name;
                $details['date'] = $date;
                $details['major'] = $model->is_major;
                
                //get the type of the event and populate the array
                $event_types = [];
                foreach(range('a', 'j') as $letter) {
                    $is_attr = 'is_'.$letter;
                    if ($draft->$is_attr) {
                        $upper = strtoupper($letter);
                        $event_types[] = $upper . '. ' . \app\models\crf\CrfHelper::SectionsTitle($lang)[$upper];
                    }
                }
                
                $details['event_types'] = $event_types;
                $details['installation'] = $event->installation->name;
                
		$sendResult = Yii::$app->mailer->compose($mail_view, ['model'=>$model, 'details'=>$details])
				//->setFrom([Yii::$app->user->identity->email => $senderUser])
				->setFrom([$from => 'SyRIO'])
				->setTo($to_users)
				->setSubject($message_subject)
                                ->setTextBody('')
				->send();
                
		if (!$sendResult) {
			$error = 'Unable to send the notification emails';
			//$this->addError('send_email', $error);
                        /*
                         * restore the app language
                         * @since: 1.3
                         */
                        \Yii::$app->language = $c_lang;
			return false;
		}			

                /*
                 * PREPARE THE MESSAGE TO THE OO
                 */
		$message_subject = \Yii::t('app', '[SyRIO] OFFSHORE INCIDENT ACCEPTED - {evt1}', [
                    'evt1' => $event->event_name,
                ], null, $lang);
                
                $mail_view = 'ca_accepts_report_oo';
                
                //check if there's a translation view of the email available
                if (file_exists( $_SERVER['DOCUMENT_ROOT'] . '/../views' . $lang_view_path . $mail_view . '.php')) {
                    $mail_view = $lang_view_path . $mail_view;
                } else {
                    $mail_view = '/email-templates/' . $mail_view;
                }
                
                //get the email of the guy submitting the report
                //the guy that submitted the report is the one who last modified the submitted draft!
                $tos = \app\models\User::find()
                        ->where(['{{auth_users}}.[[status]]' => 10])
                        ->andWhere(['{{auth_users}}.[[id]]' => $model->draft->modified_by])
                        ->select(['concat_ws(" ", first_names, last_name) as name', 'email'])
                        ->asArray()->all();
                
		$emailValidator = new \yii\validators\EmailValidator();
                $to_users = [];
                
                foreach ($tos as $to) {
                    if ($emailValidator->validate($to['email'], $err)) {
                        $to_users[$to['email']] = $to['name'];
                    }
                }
                
                if (count($to) === 0) //maybe the rapporteur got inactivated since report has been submitted
                {
                    /*
                     * restore the app language
                     * @since: 1.3
                     */
                    \Yii::$app->language = $c_lang;
                    return true;
                }
                
		$from = Yii::$app->params['adminEmail'];
	
                $details = [];
                $details['name'] = $event->event_name;
                $details['date'] = $date;            
                
		$sendResult = Yii::$app->mailer->compose($mail_view, ['model'=>$model, 'details'=>$details])
				//->setFrom([Yii::$app->user->identity->email => $senderUser])
				->setFrom([$from => 'SyRIO'])
				->setTo($to_users)
				->setSubject($message_subject)
                                ->setTextBody('')
				->send();
                
		if (!$sendResult) {
			$error = 'Unable to send the notification emails';
			//$this->addError('send_email', $error);
                        /*
                         * restore the app language
                         * @since: 1.3
                         */
                        \Yii::$app->language = $c_lang;
			return false;
		}			
                
                /*
                 * restore the app language
                 * @since: 1.3
                 */
                \Yii::$app->language = $c_lang;
                
		return true;
	}	
}
