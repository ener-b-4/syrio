<?php
use yii\helpers\ArrayHelper;

include_once(__DIR__.'/version.php');

return ArrayHelper::merge($version,[
    'adminEmail' => '<<admin email address>>',                  // default admin email
    'developersEmail' => 'syrio@jrc.ec.europa.eu',              // developers' email - do not change!!!
    'competentAuthority' => '<<name of the competent auth.>>',  // name of the Competent Authority
    'competentAuthorityIso2' => 'AA',                           // country ISO 3166-1 alpha-2 code of the country hosting SyRIO 
    'languages' => [
        'en' => 'English',
        'it' => 'Italiano',
        'ro' => 'Româneşte'
    ],
    'language' => 'en',                                         // the default application language
                                                                // country ISO 3166-1 alpha-2 code of the country
                                                                // one in 'languages'
                                                                // CASE SENSITIVE!
    'mail_language' => 'en',                                    // the language used for the automatic emails
]);
