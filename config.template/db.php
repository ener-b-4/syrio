<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host={insert_host};port={insert_port};dbname=syrio_b',  //change database name if required    
    'username' => '{insert_syrio_db_user}',
    'password' => '{insert_syrio_db_user_password}',
    'charset' => 'utf8',
];
