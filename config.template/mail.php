<?php

return [
    'class'=>'yii\swiftmailer\Mailer',
    'useFileTransport' => true,
    'transport' => [
        'class' => 'Swift_SmtpTransport',
        'host' => 'email.no.mail.com',
        'username' => 'username',
        'password' => 'password',
        //'encryption' => 'tls',
        'port' => '25',
    ]
];