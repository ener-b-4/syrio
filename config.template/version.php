<?php
$version = [
    'previous_version' => '1.3.20170226',                       // previous version         
    'previous_build' => '1258',                                 // previous build no.        
    'version' => '1.3.20170427',                                // version
    'build' => '1616'                                           // build
];