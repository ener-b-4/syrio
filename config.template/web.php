<?php

$params = require(__DIR__ . '/params.php');
$params = array_merge($params, require(__DIR__ . '/core.php'));

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => [
        'log',
        'app\base\settings',
    ],
    'language' => $params['language'],
    'sourceLanguage' => 'en',
    
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],    
	
    'on beforeAction' => function($event) {
        if (Yii::$app->params['terms_of_use'] != 1) {
            //set the catchall to the terms_of_use page
            $actionId = $event->action->id;
            $controllerId = $event->action->controller->id;
            $moduleId = $event->action->controller->module->id;
            
            if (!(($controllerId === 'terms') || ($controllerId === 'site' && $actionId === 'login'))) {
                return Yii::$app->response->redirect(['/setup/terms'], 301);
            }
            
            if ($controllerId === 'site' && $actionId === 'index') {
                return Yii::$app->response->redirect(['/setup/terms'], 301);
            }
        }
        
        if ( !Yii::$app->user->isGuest)  {
            if (Yii::$app->session['userSessionTimeout'] < time()) {

                //unset the login time as session variable
                \Yii::$app->user->logout();

                $actionMessage = new app\models\common\ActionMessage();
                $actionMessage->SetWarning(
                        Yii::t('app', 'Your session has timed-out!', []), 
                        Yii::t('app', 'You must login again to continue.'));
                Yii::$app->session->set('finished_action_result', $actionMessage);
                
                //Yii::$app->response->redirect(['site/logout']);
                return \Yii::$app->user->loginRequired();
                //return false;
            } else {
                Yii::$app->session->set('userSessionTimeout', time() + Yii::$app->params['sessionTimeoutSeconds']);
                return true; 
            }
        } else {
            return true;
        }
    
    },
    
    'as beforeRequest' => [
        'class' => 'app\behaviors\SyrioSessionBehavior',
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '75tLc8ckIFCcLgdiSOUofBldF9ZRCicP',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => require(__DIR__ . '/mail.php'),        
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        'authManager' => [
            'class'=>'yii\rbac\DbManager',
            'db' => 'db',
        ],
        'charset' => 'utf-8',
        'urlManager' => [
            // here is the url manager config
            //
            //'rules' => [],
        ],
        'error' => [
            'class' => 'app\components\Errors',
        ],
        'i18n' => [
            'translations' => [
                'app*' => [
                    'class' => 'yii\i18n\DbMessageSource',
                    'messageTable' => '{{t_message}}',
                    'sourceMessageTable' => '{{t_source_message}}',
                    'forceTranslation'=>false,       //Whether to force message translation when the source and target languages are the same.
                    'on missingTranslation' => ['app\components\TranslationEventHandler', 'handleMissingTranslation']
                ]
            ]
        ],
        
        //this one is needed to enable hashing the password on the client side
        'security' => [
          'passwordHashStrategy' => 'password_hash'
        ],
    ],
    'modules' => [
        'mdm_admin' => [
            'class' => 'mdm\admin\Module',
            'layout' => 'top-menu', // defaults to null, using the application's layout without the menu
                                     // other avaliable values are 'right-menu' and 'top-menu'            
            'controllerMap' => [
                 'assignment' => [
                    'class' => 'mdm\admin\controllers\AssignmentController',
                    /* 'userClassName' => 'app\models\User', */ // fully qualified class name of your User model
                    // Usually you don't need to specify it explicitly, since the module will detect it automatically
                    'idField' => 'id',        // id field of your User model that corresponds to Yii::$app->user->id
                    'usernameField' => 'username', // username field of your User model
                    'searchClass' => 'app\models\UserSearch'    // fully qualified class name of your User model for searching
                ]
            ],            
        ],  
        'gridview' => [
            'class' => '\kartik\grid\Module',
            // 'downloadAction' => 'gridview/export/download',
            'i18n' => [
                'class' => 'yii\i18n\PhpMessageSource',
                'basePath' => '@app/messages',
                'forceTranslation' => true                
                //'forceTranslation' => 'false'
            ],
        ],
        'management' => [
            'class' => 'app\modules\management\Management',
        ],        
        'pdfgenerator' => [
          'class' => 'app\modules\pdfgenerator\pdfgenerator',
        ],
        'cpfbuilder' => [
          'class' => 'app\modules\cpfbuilder\cpf_builder',
        ],
        'translation' => [
          'class' => 'app\modules\translations\translations',
        ],
        'setup' => [
          'class' => 'app\modules\setup\setup',
        ]
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = 'yii\debug\Module';

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = 'yii\gii\Module';
}

return $config;
