<?php

return [
    'sessionTimeoutSeconds' => '1800',                          // duration in seconds after which the user is automatically logged out
    'user.passwordResetTokenExpire' => 3600,                    // duration in seconds while the reset password link is available  
];
