<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\widgets;

use \yii\base\Widget;
use yii\helpers\Html;

use app\models\common\ActionMessage;

/**
 * ActionMessageWidget
 * 
 * Creates an action result div
 *
 * @property app\models\common\ActionMessage $actionMessage The ActionMessage object to be rendered
 * 
 * @author bogdanV
 */
class ActionMessageWidget extends Widget {
    
    public $actionMessage;
    
    public function init()
    {
        parent::init();
        
        if ($this->actionMessage === null)
        {
            //$this->actionMessage = 'Hello Bre n\'auzi? ';
        }
        else
        {
            //do nothing
        }
    }
    
    
    public function run()
    {
        //return $this->render('show-message', ['model' => $model]);
        //return Html::encode('MY WIDGET');
        if (isset($this->actionMessage))
        {
            //return Html::encode('MY WIDGET');
            return $this->render('show-message', ['model' => $this->actionMessage]);
        }
    }
    
}
