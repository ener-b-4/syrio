<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\widgets;

use app\models\shared\NullValueFormatter;
use app\models\crf\CrfHelper;

/**
 * Description of UnitValueViewWidget
 *
 * @author vamanbo
 */
class UnitValueViewWidget extends \yii\base\Widget {
    
    public $model;
    public $attr_root;
    public $units;
    
    public function init($options = null)
    {
        parent::init();
        
        if (isset($options)) {
            $this->model = $options['model'];
            $this->attr_root = $options['attr_root'];
            $this->units = $options['units'];
        }
    }
    
    public function run()
    {
        if (!isset($this->model) || !isset($this->attr_root) || !isset($this->units)) {
            return '{either the model and / or att_root missing}';
        }

        $quantity_attr = $this->attr_root . '_q';
        $unit_attr = $this->attr_root . '_u';
        
        if (!$this->model->hasAttribute($quantity_attr) || !$this->model->hasAttribute($unit_attr)) {
            return '{the model doesn\'t have one of the _u or _q properies}';
        }
        
        $unit_str = CrfHelper::IsSpecialValue($this->model->$quantity_attr) ? '' : NullValueFormatter::FormatDropdown($this->model->$unit_attr, $this->units);
        
        $ret = '<span class="crf-item-label sy_descriptive_text">'
                . NullValueFormatter::format($this->model->$quantity_attr) . ' ' 
                . $unit_str
                . '</span>';
        
        //$ret = 'aaaa';
        //echo 'ret =' . $ret;
        //die();
        return $ret;
        
    }
        
    
}
