<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\widgets;

use \yii\base\Widget;
use yii\helpers\Html;


/**
 * ValidationReportWidget
 * 
 * Creates a report based on an array of errors
 *
 * @property mixed $errors the array of errors (given from $model->errors)
 * 
 * @author bogdanV
 */
class ValidationReportWidget extends Widget {
    
    public $title;
    public $errors_message = 'Please review the following:';
    public $no_errors_message = 'No errors found';
    public $errors;
    
    public function init()
    {
        parent::init();
    }
    
    
    public function run()
    {
        //return Html::encode('MY WIDGET');
        if (isset($this->errors) && count($this->errors)>0)
        {
            return $this->render('ValidationReportWidget_hasErrors', [
                'title' => $this->title,
                'message' => $this->errors_message,
                'errors' => $this->errors
            ]);
        }
        else
        {
            return $this->render('ValidationReportWidget_noErrors', [
                'title' => $this->title,
                'message' => $this->no_errors_message,
            ]);
        }
        
        /*
        if (isset($this->actionMessage))
        {
            //return Html::encode('MY WIDGET');
            return $this->render('show-message', ['model' => $this->actionMessage]);
        }
         * 
         */
    }
    
}
