<?php

namespace app\widgets\cookies;

use yii\base\Widget;
use yii\helpers\Json;

/**
 */
class Cookies extends Widget
{

    /**
     * Runs the widget.
     * @return string
     */
    public function run()
    {
        return $this->render('default');
    }
    
}
