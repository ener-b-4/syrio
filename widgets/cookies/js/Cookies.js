/*!
 * Cookie v1.0.1
 * author: vamanbo
 */

var c = readCookie("SyRIO_cookies_accept");
if (!c) {
    showCookies();
} else if (c=="true") {
    $('.cookie_container').addClass('hidden');
} else {
    showCookies();
}

$('.CookieOk').click(function(e) {
    e.preventDefault();
    
    //create the cookie
    createCookie('SyRIO_cookies_accept', true, 30);
    hideCookies();
})

function showCookies() {
    $('.cookie_container').removeClass('hidden');
}

function hideCookies() {
    $('.cookie_container').slideUp({
        duration: 500,
        complete: function() {
            $(this).addClass('hidden');
        }
    });
}


function createCookie(name,value,days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        var expires = "; expires="+date.toGMTString();
    }
    else var expires = "";
    document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name,"",-1);
}

