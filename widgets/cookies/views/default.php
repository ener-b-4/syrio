<?php

app\widgets\cookies\assets\CookieAsset::register($this);

$default_text = Yii::t('app', "This site uses cookies to offer the best online experience. By continuing to use this website, you are agreeing to our use of cookies. Alternatively, you can manage them in your browser settings.");

yii\bootstrap\BootstrapAsset::register($this);

?>

<div class='container-fluid cookie_container hidden' style='position: relative; top:51px; '>
    <div class='row' style="margin-left: -15px; margin-right: -15px; border: 1px solid black; padding: 12px; background-color: #c4e3f3">
        <div class='col-lg-12'>
            <div class='container'>
                <div class='row'>
                    <div class='col-md-11 col-sm-10 col-xs-12'>
                        <span>
                            <?= $default_text ?>
                        </span>
                    </div>
                    <div class='col-md-1 col-sm-2 col-xs-12'>
                        <div class='pull-right'>
                            <button type="button" class="CookieOk btn btn-default" style='width:100%'>OK</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
