<?php

namespace app\widgets\cookies\assets;

use yii\web\AssetBundle;

/**
 * Asset bundle for the Cookie javascript file.
 * 
 * @author vamanbo
 * @version 1.0.0
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */
class CookieAsset extends AssetBundle
{
    public $sourcePath = '@app/widgets/cookies/js';
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',        
        ];
    
    /**
     * Registers js file.
     * @since 1.0.1
     */
    public function init()
    {
        $this->js[] = 'Cookies' . (YII_DEBUG ? '' : '.js') . '.js';
        parent::init();
    }
}
