<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

/// <summary>
/// ============================================================================================
/// Details: This file contains the widget of CA Justifications
/// ============================================================================================
/// </summary>

namespace app\widgets;

use yii\base\Widget;
use app\models\ca\IncidentCategorization;

/**
 * Description of assess_ma
 *
 * @author vamanbo
 */
class assess_ma extends Widget{
    
    /**
     *
     * @var app\models\ca\IncidentCategorization $model 
     */
    public $model;
    
    /**
     *
     * @var string $section 
     */
    public $section;
    public $is_pdf;
    public $displayForm;
    
    public function init()
    {
        parent::init();
        
        $this->displayForm = false;    //view by default
    }
    
    
    public function run() {
        
        if ($this->displayForm === false) {
            if ($this->is_pdf) {
                return $this->render('assess-ma-view-pdf', ['model'=>$this->model, 'section'=>$this->section]);
            } else {
                return $this->render('assess-ma-view', ['model'=>$this->model, 'section'=>$this->section]);
            }
        }
        else {
            return $this->render('assess-ma-form', ['model'=>$this->model, 'section'=>$this->section]);
        }
    }    
}
