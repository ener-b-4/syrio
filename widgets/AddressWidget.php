<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\widgets;

use \yii\base\Widget;
use yii\helpers\Html;

use app\modules\management\models\Address;

/**
 * AddressWidget
 * 
 * Renders an address
 *
 * @property app\modules\management\models\Address $address The Address object to be rendered
 * @property integer style The style of the rendering (0 - full, 1 - one line)
 * 
 * @author bogdanV
 */
class AddressWidget extends Widget {
    
    public $address;
    public $style;
    
    public function init()
    {
        parent::init();

        if (!isset($this->style))
        {
            $this->style=0;
        }
    }
    
    
    public function run()
    {
        //return $this->render('show-message', ['model' => $model]);
        //return Html::encode('MY WIDGET');
        if (isset($this->address))
        {
            if ($this->style==0)
            {
                return $this->render('full-address', ['model' => $this->address]);
            }
            elseif ($this->style==1)
            {
                return $this->render('one-line-address', ['model' => $this->address]);
            }
            else
            {
                return $this->render('uknown-address-style');
            }
        }
    }
    
}
