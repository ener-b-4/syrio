<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

?>
<div class='alert alert-success' role='alert'>
    <h3 style="margin-top:10px;"><?= $title ?></h3>
    <p><?= $message ?></p>
</div>

