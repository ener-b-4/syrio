<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;

/* 
 * The view for LanguageSelectorWidget
 */

?>

<?= Html::beginForm(); ?>

<div id="language-select">
    <?php
                    echo Html::dropDownList('_lang', $currentLang, $languages,
                            array(
                                'onchange' => 'language_change(this)',
                                'csrf' => true,
                            ));
    ?>
    
    <script type="text/javascript">
    </script>
</div>

<?= Html::endForm() ?>

<?php

$script = 'function language_change(selected) {'
        . "     var param = '_lang='+selected.value+'&YII_CSRF_TOKEN=' + '".Yii::$app->request->csrfToken."+' ;". PHP_EOL
        . '     $.ajax({' . PHP_EOL
        . '         url: "' . Yii::$app->getUrlManager()->createUrl('site/language') . '",' . PHP_EOL
        . '         type: "post", ' . PHP_EOL
        . '         data: param' . PHP_EOL
        . '     }).success(function(data) {' . PHP_EOL
        . '         window.location.reload();' . PHP_EOL
        . '     });' . PHP_EOL
        . '     }' . PHP_EOL;

$this->registerJs($script, \yii\web\View::POS_END);

?>
