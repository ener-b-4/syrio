<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

/// <summary>
/// ============================================================================================
/// Details: This file contains the widget for Reporting Status widget
/// ============================================================================================
/// </summary>

use yii\helpers\Html;

/* @var integer $n_events */
/* @var integer $n_events_year */
/* @var mixed[] $submitted */
/* @var mixed[] $in_progress */
/* @var int $scope 1-Operators 2-Installation */

//total of the submitted reports
$submitted_total = 0;
foreach($submitted as $item) {
    $submitted_total+= intval($item);
}

$progress_total = 0;
foreach($in_progress as $item) {
    $progress_total+= intval($item);
}

if ($scope == 1) {
    $help_text_1 = \Yii::t('app', 'Overview of the incidents registered by your organization.');
} else {
    $help_text_1 = \Yii::t('app', 'Overview of the incidents registered by your installation.');
}

?>


<div class="crf-base-font">
<div class='row hidden-xs'>
    <div class='col-lg-12'>
        <div class='well'>
            <div class="row">
                <div class='col-lg-12' style='font-size: 125%'>
                    <strong>
                    <?= \Yii::t('app', 'Incidents overview') ?>                    
                    </strong>
                    <small>
                        <div class='text-muted' style='padding-bottom: 6px;'>
                            <?= $help_text_1 ?>
                        </div>
                    </small>
                </div>
                
                <div class='col-lg-12 stat_block'>
                    <div class='row'>

                        <div class="col-lg-12">
                            <strong class="text-info"><?= \Yii::t('app', 'Registered incidents') ?></strong>
                            <small>
                                <div class='text-muted' style='padding-bottom: 6px;'>
                                    <?= \Yii::t('app', 'the number of incidents registered in SyRIO') ?>
                                </div>
                            </small>
                        </div>
                        <div class="col-sm-10">
                            <span style='padding-right: 10px' class='glyphicon glyphicon-plus-sign'></span>
                            <strong><?= ucfirst(\Yii::t('app', 'total')).':' ?></strong>
                        </div>
                        <div class="col-sm-2 pull-right text-right">
                            <span class="badge"><?= $n_events ?></span>
                        </div>
                        <div class="col-sm-10">
                            <span style='padding-right: 10px' class='glyphicon glyphicon-calendar'></span>
                            <?= \Yii::t('app', 'Current year ({evt}):', ['evt'=>date('Y')]) ?>
                        </div>
                        <div class="col-sm-2 pull-right text-right">
                            <span class="badge"><?= $n_events_year ?></span>
                        </div>

                    </div>
                </div>   <!-- end stat_block -->
                    
                    
                    
                <div class='col-lg-12 stat_block'>
                    <div class='row'>
                        
                        <div class="col-lg-12">
                            <strong class="text-info"><?= \Yii::t('app', 'Submitted reports') ?></strong>
                            <small>
                                <div class='text-muted' style='padding-bottom: 6px;'>
                                    <?= \Yii::t('app', 'incident reports that have been submitted to the Competent Authority') ?>
                                </div>
                            </small>
                        </div>
                        <div class="col-sm-10">
                            <span style='padding-right: 10px' class='glyphicon glyphicon-plus-sign'></span>
                            <strong><?= ucfirst(\Yii::t('app', 'total').':') ?></strong>
                        </div>
                        <div class="col-sm-2 pull-right text-right">
                            <span class="badge"><?= $submitted_total ?></span>
                        </div>

                        <div class="col-sm-10">
                            <i class="fa fa-check fa-lg" title="<?= \Yii::t('app', 'Accepted') ?>" style='padding-right: 10px'></i>                    
                            <?= \Yii::t('app', 'Accepted') ?>
                        </div>
                        <div class="col-sm-2 pull-right text-right">
                            <span class="badge"><?= key_exists(1, $submitted) ? $submitted[1] : 0 ?></span>
                        </div>
                        <div class="col-sm-10">
                            <i class="fa fa-arrow-right fa-sm" title="<?= \Yii::t('app', 'Pending for acceptance') ?>"></i>
                            <i class="fa fa-building-o fa-sm" title="<?= \Yii::t('app', 'Pending for acceptance') ?>" style='padding-right: 0px'></i>                    
                            <?= \Yii::t('app', 'Pending') ?>
                        </div>
                        <div class="col-sm-2 pull-right text-right">
                            <span class="badge"><?= key_exists(0, $submitted) ? $submitted[0] : 0 ?></span>
                        </div>
                        <div class="col-sm-10">
                            <?php $warn_class = key_exists(-1, $submitted) && $submitted[-1]>0 ? 'sy_error' : '' ?>
                            <i class="fa fa-exclamation-triangle fa-lg <?= $warn_class ?>" title="<?= \Yii::t('app', 'Rejected') ?>" style='padding-right: 10px'></i>                    
                            <?= \Yii::t('app', 'Rejected') ?>
                        </div>
                        <div class="col-sm-2 pull-right text-right">
                            <span class="badge"><?= key_exists(-1, $submitted) ? $submitted[-1] : 0 ?></span>
                        </div>
                    </div>
                </div>   <!-- end stat_block -->
                
                <div class="col-lg-12">
                    <strong class="text-info"><?= \Yii::t('app', 'Reporting') ?></strong>
                    <small>
                        <div class='text-muted' style='padding-bottom: 6px;'>
                            <?= \Yii::t('app', 'the incidents registered in SyRIO, yet not reported to the CA') ?>
                        </div>
                    </small>
                </div>
                <div class="col-sm-10">
                    <span style='padding-right: 10px' class='glyphicon glyphicon-plus-sign'></span>
                    <strong><?= ucfirst(\Yii::t('app', 'total') . ':') ?></strong>
                </div>
                <div class="col-sm-2 pull-right text-right">
                    <span class="badge"><?= $progress_total ?></span>
                </div>
                
                <div class="col-sm-10">
                    <i class="fa fa-star-o fa-lg" title="<?= \Yii::t('app', 'Draft') ?>" style='padding-right: 10px'></i>                    
                    <?= \Yii::t('app', 'Draft') ?>
                </div>
                <div class="col-sm-2 pull-right text-right">
                    <span class="badge"><?= key_exists(0, $in_progress) ? $in_progress[0] : 0 ?></span>
                </div>
                
                <div class="col-sm-10">
                    <i class="fa fa-star-half-o fa-lg" title="<?= \Yii::t('app', 'Finalized (partially)') ?>" style='padding-right: 10px'></i>                    
                    <?= \Yii::t('app', 'Finalized (partially)') ?>
                </div>
                <div class="col-sm-2 pull-right text-right">
                    <span class="badge"><?= key_exists(2, $in_progress) ? $in_progress[2] : 0 ?></span>
                </div>

                <div class="col-sm-10">
                    <i class="fa fa-star fa-lg" title="<?= \Yii::t('app', 'Finalized') ?>" style='padding-right: 10px'></i>                    
                    <?= \Yii::t('app', 'Finalized') ?>
                </div>
                <div class="col-sm-2 pull-right text-right">
                    <span class="badge"><?= key_exists(3, $in_progress) ? $in_progress[3] : 0 ?></span>
                </div>

                <div class="col-sm-10">
                    <i class="fa fa-check-square-o fa-lg" title="<?= \Yii::t('app', 'Signed') ?>" style='padding-right: 10px'></i>                    
                    <?= \Yii::t('app', 'Signed') ?>
                </div>
                <div class="col-sm-2 pull-right text-right">
                    <span class="badge"><?= key_exists(4, $in_progress) ? $in_progress[4] : 0 ?></span>
                </div>
                
            </div>
        </div>
    </div>
</div>

<div class="row visible-xs" style='margin-left: 0px; border: 1px #e3e3e3 solid; border-radius: 6px; background-color: #f5f5f5; padding: 6px 0 6px 0'>
    <div class='col-xs-4 col-xs-4-landscape'>
        <div class='row' style=''>
            <div class='col-xs-6' style='border-right: 1px #aaa solid; min-height: 80px' >
                <div style="font-size: 80%">
                    <strong class="text-info"><?= \Yii::t('app', 'Registered incidents') ?></strong>
                </div>
                <div style="font-size: 180%">
                    <?= $n_events ?>
                </div>  
            </div>
            <div class='col-xs-6'>
                <div class='row'>
                    <div class='col-lg-12'>
                        <span style='padding-right: 3px' class='glyphicon glyphicon-calendar'></span>
                        <?= date('Y') . ':' ?>
                        <span><?= $n_events_year ?></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class='col-xs-4 col-xs-4-landscape'>
        <div class='row'>
            <div class='col-sm-6 col-xs-6' style='border-right: 1px #aaa solid; background-color: <?= strlen($warn_class) > 0 ? "#f2dede" : 'transparent' ?> ; min-height:80px ' >
                <div style="font-size: 80% ">
                    <strong class="text-info"><?= \Yii::t('app', 'Submitted reports') ?></strong>
                </div>
                <div style="font-size: 180%">
                    <?= $submitted_total ?>
                </div>  
            </div>
            <div class='col-sm-6 col-xs-6'>
                <div class='row'>
                    <div class='col-lg-12'>
                        <i class="fa fa-check fa-sm" title="<?= \Yii::t('app', 'Accepted') ?>" style='padding-right: 13px'></i>
                        <span><?= key_exists(1, $submitted) ? $submitted[1] : 0 ?></span>
                    </div>
                    <div class='col-lg-12'>
                        <i class="fa fa-arrow-right fa-sm" title="<?= \Yii::t('app', 'Pending for acceptance') ?>"></i>
                        <i class="fa fa-building-o fa-sm" title="<?= \Yii::t('app', 'Pending for acceptance') ?>" style='padding-right: 0px'></i>                    
                        <span><?= key_exists(0, $submitted) ? $submitted[0] : 0 ?></span>
                    </div>
                    <div class='col-lg-12'>
                        <i class="fa fa-exclamation-triangle fa-sm <?= $warn_class ?>" title="<?= \Yii::t('app', 'Rejected') ?>" style='padding-right: 13px'></i>                    
                        <span><?= key_exists(-1, $submitted) ? $submitted[-1] : 0 ?></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class='col-xs-4 col-xs-4-landscape'>
        <div class='row'>
            <div class='col-sm-6 col-xs-6' style='border-right: 1px #aaa solid; min-height: 80px' >
                <div style="font-size: 80% ">
                    <strong class="text-info"><?= \Yii::t('app', 'Reporting') ?></strong>
                </div>
                <div style="font-size: 180%">
                    <?= $progress_total ?>
                </div>  
            </div>
            <div class='col-sm-6 col-xs-6'>
                <div class='row'>
                    <div class='col-lg-12'>
                        <i class="fa fa-star-o fa-sm" title="<?= \Yii::t('app', 'Draft') ?>" style='padding-right: 13px'></i>                    
                        <span><?= key_exists(0, $in_progress) ? $in_progress[0] : 0 ?></span>
                    </div>
                    <div class='col-lg-12'>
                        <i class="fa fa-star-half-o fa-sm" title="<?= \Yii::t('app', 'Finalized (partially)') ?>" style='padding-right: 13px'></i>                    
                        <span><?= key_exists(2, $in_progress) ? $in_progress[2] : 0 ?></span>
                    </div>
                    <div class='col-lg-12'>
                        <i class="fa fa-star fa-sm" title="<?= \Yii::t('app', 'Finalized') ?>" style='padding-right: 13px'></i>                    
                        <span><?= key_exists(3, $in_progress) ? $in_progress[3] : 0 ?></span>
                    </div>
                    <div class='col-lg-12'>
                        <i class="fa fa-check-square-o fa-sm" title="<?= \Yii::t('app', 'Signed') ?>" style='padding-right: 13px'></i>                    
                        <span><?= key_exists(4, $in_progress) ? $in_progress[4] : 0 ?></span>
                    </div>
                </div>
            </div>
        </div>        
    </div>
</div>

</div>

