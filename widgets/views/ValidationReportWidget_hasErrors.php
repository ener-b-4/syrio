<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

?>
<div class='alert alert-danger' role='alert'>
    <h3 style="margin-top:10px;"><?= $title ?></h3>
    <p><?= $message ?></p>
    <div style="margin-left:-50px; margin-top:12px ">
        <?php vrw_RecursiveBuild($errors); ?>        
    </div>
</div>

<?php
function vrw_RecursiveBuild($errorsArray)
{
    echo '<ul>';
    foreach($errorsArray as $key => $error)
    {
        if (is_array($error))
        {
            vrw_RecursiveBuild($error);
        }
        else
        {
            echo "<li>" . $error . '</li>';
        }
    }
    echo '</ul>';                    
}
?>