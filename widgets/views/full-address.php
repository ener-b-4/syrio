<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;


/**
 * Renders the address in 'full' mode
 */

/* @var $this yii\web\View */
/* @var $model app\modules\management\models\Address */

$streets = Html::encode($model->street1);

if (!isset($model->street2) || $model->street2=='') {
    $streets .= '<br/>' . Html::encode($model->street2);
}

?>

<div class="col-lg-12">
    <p><?= $streets  ?></p>
    <p>
        <em><?= Html::encode($model->city) ?></em>
    </p>
    <p>
        <?= Html::encode($model->zip) . ' ' . Html::encode($model->county) ?>
    </p>
    <p>
        <?= strtoupper(Html::encode($model->countryObject->short_name)) ?>
    </p>
</div>