<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

/// <summary>
/// ============================================================================================
/// Details: This file contains the widget of CA Justifications (PDF page)
/// ============================================================================================
/// </summary>

use yii\helpers\Html;
use app\models\shared\NullValueFormatter;
use app\models\shared\TextFormatter;

/* 
 * The view for LanguageSelectorWidget
 */

$isMajorAttr = 'is_major_' . strtolower($section);
$majorJust = 'major_' . strtolower($section) . '_just';

$value_just = $model->$majorJust;

?>

<div class="crf-base-font">
<div class='row'>
    <div class='col-lg-12'>
            <p>
                <?= Html::encode(\Yii::t('app/crf', 'The competent authority shall further complete this section')) ?>
            </p>
            <p>
                <?= Html::encode(\Yii::t('app/crf', 'Is this considered to be a major incident?')) ?>
            </p>
            <p>
                  <?php 
                //pdf
                    $checked_value = $model->$isMajorAttr;
                    $checked_text_first = \Yii::t('app', 'Yes');
                    $checked_text_second = \Yii::t('app', 'No');
                    $checkbox_checked = '<img src="css/images/checkbox-selected.png" width="16" height="16" style="margin:0 10 0 0;" />';
                    $checkbox_unchecked = '<img src="css/images/checkbox.png" width="16" height="16" style="margin:0 10 0 0;" />';

                    $s='';
                    if (!isset($checked_value)) {
                        //both unchecked
                        $s = $checkbox_unchecked . $checked_text_first . '<br/>'. $checkbox_unchecked. $checked_text_second;
                    } elseif ($checked_value != 0) {
                        //first (Yes) checked
                        //second (No) unchecked
                        $s = $checkbox_checked . $checked_text_first . '<br/>'. $checkbox_unchecked. $checked_text_second;
                    } else {
                        //first (Yes) unchecked
                        //second (No) checked
                        $s = $checkbox_unchecked . $checked_text_first .'<br/>'. $checkbox_checked. $checked_text_second;
                    }
                    echo $s;
                  ?>
                
            </p>
            <br/>
            <p>
                <?= Html::encode(\Yii::t('app/crf', 'Give justification')) ?>
            </p>
            <p class="text-info">
                <?php echo NullValueFormatter::Format(TextFormatter::TextToHtml($value_just)) ?>
            </p>
    </div>
</div>
</div>