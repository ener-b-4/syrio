<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

/// <summary>
/// ============================================================================================
/// Details: This file contains the widget of CA Justifications (view page)
/// ============================================================================================
/// </summary>

use yii\helpers\Html;
use app\models\shared\NullValueFormatter;
use app\models\shared\TextFormatter;

/* @var app\models\ca\IncidentCategorization $model */


/* 
 * not used in this version!
 */

$isMajorAttr = 'is_major_' . strtolower($section);
$majorJust = 'major_' . strtolower($section) . '_just';

$items = [
    1 => \Yii::t('app', 'Yes'),
    0 => \Yii::t('app', 'No'),
];

?>


<div class="crf-base-font">
<div class='row'>
    <div class='col-lg-12'>
        <div class='well well-lg'>
            <p>
                <?= Html::encode(\Yii::t('app/crf', 'The competent authority shall further complete this section')) ?>
            </p>
            <p>
                <?= Html::encode(\Yii::t('app/crf', 'Is this considered to be a major incident?')) ?>
            </p>
            <p>
                <?php
                echo Html::checkboxList($isMajorAttr, 
                        $model->$isMajorAttr, 
                        $items,
                        [
                            'label'=>'',
                            'separator'=>'<br/>',
                            'itemOptions'=>[
                                'disabled' => true
                            ]
                        ]);
                    
                  ?>
            </p>
            <br/>
            <p>
                <?= Html::encode(\Yii::t('app/crf', 'Give justification')) ?>
            </p>
            <p class="text-info">
                <?php echo NullValueFormatter::Format(TextFormatter::TextToHtml($model->$majorJust)) ?>
            </p>
        </div>
    </div>
</div>
</div>