<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;


/**
 * Renders the address in 'one-line' mode
 */

/* @var $this yii\web\View */
/* @var $model app\modules\management\models\Address */

?>

<span>
    <span>
    <?= Html::encode($model->street1) .
            ((!isset($model->street2) || $model->street2=='') ? '' : '<br/>' . Html::encode($model->street2)) . ', ' ?>        
    </span>
    <span>
        <em><?= Html::encode($model->city) . ', ' ?></em>
    </span>
    <span>
        <?= Html::encode($model->zip) . ' ' . Html::encode($model->county) . ', '?>
    </span>
    <span>
        <?= strtoupper(Html::encode($model->countryObject->short_name)) ?>
    </span>
</span>