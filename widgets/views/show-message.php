<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

//use kartik\icons\Icon;
use app\models\common\ActionMessage;
use yii\web\View;
use app\assets\FontAwesomeAsset;
use \app\widgets\ActionMessageWidget;

/**
 * show-message
 *
 * The view for ActionMessageWidget
 * 
 * 
 * @author bogdanV
 */

/* @var $this yii\web\View */
/* @var $model \app\models\common\ActionMessage */

    //initialize kartik icons
    //Icon::map($this, Icon::FA);

    FontAwesomeAsset::register($this);

    switch ($model->success)
    {
        case ActionMessage::ERROR:
            $class = 'sy-action-result-error';
            
            break;
        case ActionMessage::WARNING:
            $class = 'sy-action-result-warning';

            break;
        default:
            $class = 'sy-action-result-success';
            
            break;
            //success
    }
    
?>

<?php

    $session_msg = Yii::$app->session->get('finished_action_result');     //$session_msg is of type ActionMessage
    $s = "<div class='row'>"
            . " <div class='col-lg-12 sy-action $class sy-action-title'>"
            . "     <span class='sy-action-title'>$model->action</span>"
            . "     <div class='col-lg-12 sy-action-message' style='font-size: 16px;'>"
            . nl2br($model->msg)
            . "     </div>"
            . " </div>"
            . "</div>";
    
    
    echo $s;

?>

<?php
    $this->registerCssFile('@web/css/action-message-widget.css', [View::POS_HEAD]);
?>

