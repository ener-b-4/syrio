<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

/* 
 * This is the widget for language selection
 * 
 * Author: Bogdan Vamanu
 * 
 */

namespace app\widgets;

use \yii\base\Widget;
use yii\helpers\Html;

/**
 * LanguageSelectorWidget
 * 
 * Creates an action result div
 *
 * @property app\models\common\ActionMessage $actionMessage The ActionMessage object to be rendered
 * 
 * @author bogdanV
 */
class LanguageSelectorWidget extends Widget {
    
    public function init()
    {
        parent::init();
    }
    
    
    public function run()
    {
        $currentLang = \Yii::$app->language;
        $languages = \Yii::$app->params['languages'];
        return $this->render('language-selector', array('currentLang'=>$currentLang, 'languages'=>$languages));
    }
    
}
