<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

/// <summary>
/// ============================================================================================
/// Details: This file contains the widget of OO reporting status
/// ============================================================================================
/// </summary>

namespace app\widgets;

use Yii;
use yii\base\Widget;
use app\models\ca\IncidentCategorization;
use app\models\events\EventDeclaration;

use app\components\helpers\TArrayHelper;

/**
 * Description of assess_ma
 *
 * @author vamanbo
 */
class OperatorsReportingStatusWidget extends Widget{
    
    private $template_submitted_query;
    private $template_pending_query;
    
    private $and_where_template;
    private $and_where_template_2;
    
    private $scope;         //1 if user is OO
                            //2 if user is Inst
    
    public function init()
    {
        parent::init();
        
        /* initialize the query blocks */
        $submitted_select_block = 'SELECT '
                . 'count(event_declaration.event_name) as number_of,'
                . 'is_done'
                . '   FROM '
		. '     event_declaration'
                . '     join event_classifications on event_classifications.event_id = event_declaration.id '
                . '     left join ca_incident_cat on ca_incident_cat.draft_id = event_classifications.id';
        
        $this->template_submitted_query = $submitted_select_block . ' WHERE (NOT isnull(is_done)) AND (event_declaration.operator_id = "%s" %s) GROUP BY is_done';
        
        /* set the scope and in_and_where_template */
        if (Yii::$app->user->identity->isOperatorUser) {
            $this->scope = 1;
            $this->and_where_template = "";
            $this->and_where_template_2 = "";
        } elseif (Yii::$app->user->identity->isInstallationUser) {
            $this->scope = 2;
            $this->and_where_template = ' and event_declaration.installation_id = "'. Yii::$app->user->identity->inst_id . '"';
            $this->and_where_template_2 = ' and ed.installation_id = "'. Yii::$app->user->identity->inst_id . '"';
        }
        
        /* build final sql */
        $this->template_submitted_query = sprintf($this->template_submitted_query, Yii::$app->user->identity->org_id, $this->and_where_template);
        //echo $this->template_submitted_query;
        
        // fix 2024-05-16.004
        $drafting_block = 'select '
                . 'count(ed.id),'
                . 'ec.status as report_draft_status '
		. ' from '
                . '     event_declaration ed '
                . ' inner join ('
		. '     select id, session_desc, event_id, MAX(status) as status from event_classifications where del_status = 10 '
                . '     group by event_id, status, session_desc, id order by event_id'
                . ' ) ec on ec.event_id = ed.id '
                . ' left join ca_incident_cat ca on ca.draft_id = ec.id '
                . ' where '
                . '     isnull(is_done) and (isnull(ed.status) or ed.status<5) AND (ed.operator_id = "%s" %s)'
                . ' group by report_draft_status;';
        
        /* build final sql */
        $this->template_pending_query = sprintf($drafting_block, Yii::$app->user->identity->org_id, $this->and_where_template_2);
        //echo '<br/>' . '<br/>' . $this->template_pending_query;
        
        
    }
    
    
    public function run() {
        
        $n_events = count(Yii::$app->user->identity->organization->getEvents());
        $n_events_year = count(Yii::$app->user->identity->organization->getEvents(date('Y')));
        
        $command_submitted = Yii::$app->db->createCommand($this->template_submitted_query);
        
        $results_submitted = $command_submitted->queryAll();
        $results_submitted = TArrayHelper::map($results_submitted, 'is_done', 'number_of');

        
        $command_pending = Yii::$app->db->createCommand($this->template_pending_query);
        
        $results_pending = $command_pending->queryAll();
        $results_pending = TArrayHelper::map($results_pending, 'report_draft_status', 'count(ed.id)');
        
//        echo '<pre>';
//        var_dump($results_submitted);
//        echo '</pre>';
//
//        echo '<pre>';
//        var_dump($results_pending);
//        echo '</pre>';
        
        return $this->render('reporting_status', [
            'n_events'=>$n_events,
            'n_events_year'=>$n_events_year,
            'submitted'=>$results_submitted, 
            'in_progress'=>$results_pending, 
            'scope' => $this->scope]);
    }    
}
