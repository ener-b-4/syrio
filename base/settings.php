<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\base;

use Yii;
use yii\base\BootstrapInterface;

/**
 * Description of settings
 *
 * @author vamanbo
 */
class settings implements BootstrapInterface {
    private $db;
    
    public function __construct() {
        $this->db = Yii::$app->db;
    }
    
    /**
    * Bootstrap method to be called during application bootstrap stage.
    * Loads all the settings into the Yii::$app->params array
    * @param Application $app the application currently running
    */

    public function bootstrap($app) {

        // Get settings from database
        $sql = $this->db->createCommand("SELECT setting_name,setting_value FROM settings");
        $settings = $sql->queryAll();

        // Now let's load the settings into the global params array

        foreach ($settings as $key => $val) {
            Yii::$app->params[$val['setting_name']] = $val['setting_value'];
        }
    }    
}
