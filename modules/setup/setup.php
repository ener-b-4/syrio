<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\modules\setup;

use yii\base\Module;

/**
 * Description of Setup
 *
 * @author vamanbo
 */
class Setup extends Module
{
    public $controllerNamespace = 'app\modules\setup\controllers';

    public function init()
    {
        parent::init();
    }
    
    
}
