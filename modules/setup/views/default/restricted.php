<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;


/* @var $this yii\web\View */

$this->title = ucfirst(Yii::t('app', 'SyRIO Setup'));

?>

<div class="row">
    <div class="col-lg-12">
        <h1><?= Html::encode($this->title) ?></h1>

        <div class="alert alert-warning" role="alert">
            The application is not setup. <br/>
            Please login as <em>system administrator</em> and setup the application.
        </div>
    </div>
</div>