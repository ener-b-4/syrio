<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\web\View;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = "Terms and Conditions";

?>

<div class="row">
    <div class="col-lg-12">
        <h1><?= Html::encode($this->title) ?></h1>

        <div class="alert alert-danger" role="alert">
            This version of SyRIO must be activated by agreeing with the Terms and Conditions below.
        </div>
    </div>
    <div class="col-lg-12">
        <div class="alert alert-info" role="alert">
            Please navigate at the bottom of this page and agree (tick the checkbox).
        </div>
    </div>
    
    <div class="col-lg-12">
        <div class="form-control" style="
             max-height: 300px; 
             height: 300px; 
             padding: 15px; 
             overflow-y: auto;
             border: 1px solid #ccc;
             border-radius: 6px;
             width: 100%;
             ">
            <?= $this->render('tou_text') ?>
        </div>
    </div>
    
    
    <div class="col-lg-12" style="padding-top: 10px;">
        <form action="<?= Url::to(['/setup/terms/agree']) ?>" method='post' >
            <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />            
            <input type="checkbox" id="agree" name="agree">
            <label for="agree">Agree with Terms and Conditions</label>
            <br/>
            <input type="submit" value="Submit" id="btnSubmit" class="btn btn-success" disabled>
        </form>
    </div>
</div>


<?php
$js = <<<JS
    $("#agree").click(function(e) {
            if ($(this).is(":checked")) {
                $("#btnSubmit").prop("disabled", false);
            } else {
                $("#btnSubmit").prop("disabled", true);
            }
        }
        );
JS;

$this->registerJs($js, View::POS_READY);
?>                
