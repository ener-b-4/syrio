<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = "Terms and Conditions";
?>

<div class="row">
    <div class="col-lg-12">
        
        <h1><?= Html::encode($this->title) ?></h1>

        <div class="alert alert-danger" role="alert">
            This version of SyRIO is not activated.
            
            <p>
                Please login as <em>system administrator</em> and 'agree' with the Terms and Conditions below.
            </p>
            <p>                
                <br/>
                The default <em>system administrator</em> username/password combination is: <i>admin/syrio_admin</i>.
            </p>
        </div>
    </div>
    
    <div class="col-lg-12">
        <div class="form-control" style="
             max-height: 300px; 
             height: 300px; 
             padding: 15px; 
             overflow-y: auto;
             border: 1px solid #ccc;
             border-radius: 6px;
             width: 100%;
             ">
            <?= $this->render('tou_text') ?>
        </div>
    </div>
    
</div>

