<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */
use yii\helpers;

?>

<small class='text-muted'>
    Copyright 2016 DG ENER
</small>

<div style='padding-top: 16px'>
    <p>
    Licensed under the EUPL, Version 1.1 or – as soon they
    will be approved by the European Commission - subsequent
    versions of the EUPL (the "Licence");
    </p>
    <p>
    You may not use this work except in compliance with the <?= helpers\Html::a('License', ['display-license']) ?>.
    </p>
    <p>
    You may obtain the latest online version of the Licence (if any) <?= helpers\Html::a('here', 'https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11') ?>.
    </p>
    
    <p>
    Unless required by applicable law or agreed to in
    writing, software distributed under the Licence is
    distributed on an "AS IS" basis,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, 
    either express or implied.

    </p>
    <p>
    See the License for specific language governing
    permissions and limitations <?= helpers\Html::a('here', 'https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11') ?>.        
    </p>
</div>



