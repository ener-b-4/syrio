<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\modules\setup\controllers;

use Yii;
use yii\web\Controller;

/**
 * Controller class for Terms and Conditions
 *
 * @author vamanbo
 */
class TermsController extends Controller {
    
    //displayes the terms and condition page   
    public function actionIndex() {
        if (!Yii::$app->user->can('sys_admin')) {
            return $this->render('terms_general');
        } else {
            return $this->render('terms_form');
        }
    }
    
    public function actionDisplayLicense() {
        return $this->render('eupl1.1');
    }
    
    public function actionAgree() {
        if (!Yii::$app->user->can('sys_admin')) {
            return $this->redirect(['/setup/terms/forbidden']);
        }
        else {
            //mark the terms_of_use as 1
            $setting = \app\modules\setup\models\Settings::findOne(['setting_name'=>'terms_of_use']);
            $setting->setting_value = '1';
            
            if (!$setting->validate()) {
                var_dump($setting->errors);
                die();
            }
            
            $setting->update();
            
            return $this->goHome();
        }
    }
}
