<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\modules\setup\controllers;
use yii\web\Controller;
use Yii;

class DefaultController extends Controller
{
    /**
     * Add this to ensure that only the authenticated users have access to this
     * @param type $action
     * @return boolean
     * 
     * @version 1.0.20160616
     * @author vamanbo
     */
    public function beforeAction($action) {
        //echo var_dump($action);
        //die();
        if (!\Yii::$app->user->can('sys_admin') && $action->id !== 'forbidden')
        {
            Yii::$app->response->redirect(['setup/default/forbidden'], 302);
            return false;
        }
        return parent::beforeAction($action);        
    }
    
    
    public function behaviors()
    {
        
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                //only authenticated users
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ]
            ],
        ];
    }
    
    public function actionIndex()
    {
        return $this->render('setup');
    }
    
    public function actionForbidden() {
        return $this->render('restricted');
    }
}
