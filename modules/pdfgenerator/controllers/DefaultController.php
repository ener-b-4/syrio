<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\modules\pdfgenerator\controllers;

use Yii;
use yii\web\Controller;
use app\models\events\drafts\EventDraft;
use app\models\events\EventDeclaration;

//use app\modules\pdfgenerator\models\EventDraft;
use app\modules\pdfgenerator\models\printPDFOptions;
use app\modules\pdfgenerator\models\printPDFHeader;

use app\modules\pdfgenerator\behaviors\PrintableSection;
use yii\helpers\HtmlPurifier;

use app\models\ca\IncidentCategorization;

//use mPDF;                                                             //We will include the pdf library installed by composer
use kartik\mpdf\Pdf;

class DefaultController extends Controller
{
    public $layout = '@app/views/layouts/main_pdf';
    
    const REPORT_FULL = 0;
    const REPORT_SECTION = 1;
    const REPORT_SUBSECTION = 2;
    
    
   /**
    * Finds the EventDraft model based on its primary key value.
    * If the model is not found, a 404 HTTP exception will be thrown.
    * @param integer $id
    * @return EventDraft the loaded model
    * @throws NotFoundHttpException if the model cannot be found
    */
  protected function findModel($id)
  {
    if (($model = EventDraft::findOne($id)) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  /**
   * @param integer $id the id of the Draft
   * @param string $section 
   * either NULL - full report
   * section.subsection.subsubsection e.g. A.2.1
   * @return type
   */
  public function actionIndex($id, $section=NULL)
      //[A..J] -> entire section A or B or C or D or E of F or I ot J
      // subsection [A.1..J.4] -> sub-section 1 or 2 or 3 or 4         
  {
      //increase php execution time to 2 minutes
      $exec_time = ini_get('max_execution_time');
      if ($exec_time < 120 ) {
          ini_set('max_execution_time', 120);
      }
      
    $draft = $this->findModel($id);

    /* handle the section part */
    $purifier = new HtmlPurifier();
    
    /* create the CrfDisplaySettings object to be passed to the view */
    $display_settings = new \app\models\crf\CrfDisplaySettings();
    /* if the user can incident-report-view-ma-assessment set the settings appropriately */
    $display_settings->as_pdf = true;
    if (\Yii::$app->user->identity->isCaUser) {
        $display_settings->include_ma_assessment = TRUE;
    }
    
    
    if (isset($section)) {
        $section = $purifier->process($section);

        $split = explode('.', $section);

        switch (count($split)) {
            case 1:
                if (!in_array(strtoupper($split[0]), range('A', 'J')))
                {
                    throw new \yii\web\HttpException('Unexpected parameter!');
                }
                $report_type = self::REPORT_SECTION;
                $sectionAttr = 'section'.strtoupper($split[0]);
                $model = $draft->$sectionAttr;
                $model->attachBehavior('printable', new PrintableSection);

                $url = '@app/modules/pdfgenerator/views/default/view-section';
                $content = $this->renderPartial($url, 
                    [
                        'model' => $draft,
                        'v' => 0,
                        'section' => strtolower($split[0]), 
                        'is_pdf' => 1,
                        'display_settings' => $display_settings
                    ]);

                $title = 'SECTION ' . strtoupper($split[0]) . '.';
                $link = '/sections/section-view';
                
                //return $content;
                if (Yii::$app->user->identity->isOperatorUser) {
                    $pdf_title = ''; //$draft->event->event_name;
                } else {
                    $pdf_title = '';
                }
                $this->funGenerateSectionPdf($id, 
                        $content, 
                        $pdf_title);
                break;
            case 3:
                
                if (!in_array(strtoupper($split[0]), range('A', 'J')) || 
                        !in_array(intval($split[1]), range(1,4)))
                {
                    throw new \yii\web\HttpException('Unexpected parameter!');
                }
                $report_type = self::REPORT_SUBSECTION;
                $sectionAttr = 'section'.strtoupper($split[0]);
                $subsection_attr = 's'.strtolower($split[1]);

                $model = $draft->$sectionAttr;
                $model->attachBehavior('printable', new PrintableSection);
                
                 $url = '@app/modules/pdfgenerator/views/default/view-section';
                
                 $content = $this->renderPartial($url, 
                    [
                        'model' => $draft,
                        'v' => intval($split[1]),
                        'section' => strtoupper($split[0]. '.' . $split[1] . '.'), 
                        'is_pdf' => 1,
                        'display_settings' => $display_settings                        
                    ]);
                
                 //return $content;
                 
                //$title = $draft->event->event_name . '>' . $draft->session_desc . '> Section ' . strtoupper($section) . $split[1];
                $title = 'Section ' . strtoupper($section) . $split[1];                  

                // (start) Bug 62 - Generate PDF : Subsection to be changed the title of page in EVENT NAME > SESSION DESC > SECTION NAME
                //$new_model->event_classification_id = $id;
                //list( $event_name, $session_desc ) = $this->getEntireTitle($id);
                //$new_model->event_name = $event_name;
                //$new_model->session_desc = $session_desc;
                // (end) Bug 62 - Generate PDF : Subsection to be changed the title of page in EVENT NAME > SESSION DESC > SECTION NAME

                $this->funGenerateSectionPdf($id, 
                        $content, 
                        $title);
                break;
        }
    } else {
        
        
        /* render the entire report */
        $report_type = self::REPORT_FULL;
        $draft->attachBehavior('printable', new PrintableSection);
        $url = '@app/modules/pdfgenerator/views/sections/report-view';
        $title = $draft->event->event_name;
        
        $incident_cat = \app\models\ca\IncidentCategorization::findOne(['draft_id' => $draft->id]);
        $content = $this->renderPartial($url, [
            'model' => $draft, 
            'incident_cat' =>  $incident_cat,
            'is_pdf' => 1,
            'display_settings' => $display_settings            
        ]);

        //return $content;
        
        $this->funGenerateSectionPdf($id, $content, '');
    }
  }
        
  private function getEntireTitle($event_classification_id)
  {
    $model_event_class = EventDraft::find()->where(['id' => $event_classification_id])->all();

    $event_id = null;
    $session_desc = null;
    $event_name = null;
    
    if (count($model_event_class) > 0)
    {
      foreach($model_event_class as $class)
      {
        $event_id = $class->event_id;
        $session_desc = $class->session_desc;
        
        $model_event_decla = EventDeclaration::find()->where(['id' => $event_id])->all();
        if (count($model_event_decla) > 0)
        {
          foreach($model_event_decla as $decla)
          {
            $event_name = $decla->event_name;
          }
        }
      }
    }
    
    $session_desc = ($session_desc !== NULL) ? $session_desc : '(not set)';
    $event_name = ($event_name !== NULL) ? $event_name : '(not set)';
   
    return array( $event_name, $session_desc);
  }

  function getEventName ($id)
  {
    $model_event = EventDeclaration::find()->where(['id' => $id])->all();
    $event_name = null;
    
    if (count($model_event) > 0)
    {
      foreach($model_event as $event)
      {
        $event_name = $event->event_name;
      }
    }
    
    $event_name = ($event_name !== NULL) ? $event_name : '(not set)';
   
    return $event_name;
  }

  private function funGenerateSectionPdf($id,
          $content,
          $title = NULL, 
          $new_model = NULL)
  {
    //$this->layout = '@app/views/layouts/main_pdf';
        
    if ($new_model === NULL) {
      $model = $this->findModel($id);
      
      //$model->isCampactPrint = $isCampactPrint;
      //$model->isCompilationPrint = $isCompilationPrint;
    }
    else {
      $model = $new_model;
    }
    
    /* Management the partial view for Header */
    $model_header = new printPDFHeader();

    // (start) Bug 62 - Generate PDF : Subsection to be changed the title of page in EVENT NAME > SESSION DESC > SECTION NAME
    $section_name = ($title !== NULL) ? $title : '';
    //$sLeftString = $section_name;

    if (isset($new_model)) {
      $event_name = ($model->event_name !== NULL) ? $model->event_name : '(not set)';
      $session_desc = ($model->session_desc !== NULL) ? $model->session_desc : '(not set)';
    }
    else {
      $event_name = $this->getEventName($model->event_id);
      $session_desc = ($model->session_desc !== NULL) ? $model->session_desc : '(not set)';
      $section_name = ($title !== NULL) ? $title : '';
    }
    
    if (Yii::$app->user->identity->isCaUser || Yii::$app->user->can('sys_admin')) {
        $ca_report = IncidentCategorization::findOne(['draft_id'=>$model->id]);
        $sLeftString = $ca_report->name;
    } else {
        $sLeftString = $event_name 
            . ' / ' 
            . $session_desc 
            . (($section_name !== '') ? ' / ' : '')
            . $section_name;
    }
    
    
    $model_header->sLeftString = $sLeftString;
    $model_header->sRightString = \Yii::t('app', 'SyRIO report') .' '. date('Y-m-d H:i:s', time());
    $heading = $this->renderPartial('_pdf-heading', ['model' => $model_header]);

    $mpdf = new Pdf([
        // set to use core fonts only
        'mode' => Pdf::MODE_UTF8, 
        // A4 paper format
        'format' => Pdf::FORMAT_A4,
        // portrait orientation
        'orientation' => Pdf::ORIENT_PORTRAIT, 
        // stream to browser inline
        'destination' => Pdf::DEST_DOWNLOAD, 
        //filename => the default filename
        'filename' => $sLeftString.'.pdf',
        // your html content input
        'content' => $content,
        // format content from your own css file if needed or use the
        // enhanced bootstrap css built by Krajee for mPDF formatting 
        //'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
        'cssFile' => '@app/web/css/syrio_pdf.css',
        // any css to be embedded if required
        'cssInline' => '.kv-heading-1{font-size:18px}', 
         // set mPDF properties on the fly
        'options' => [
            'title' => \Yii::t('app', 'SyRIO report'),
            'shrink_tables_to_fit' => 0
            ],
        // set the page top margin for the new document (in millimetres).
        'marginTop' => 25,
         // call mPDF methods on the fly
        'methods' => [ 
            'SetHeader'=>[$heading], 
            'SetFooter'=>['{PAGENO}'],
            ]
    ]);  // new mPDF('utf-8', 'A4');

    //echo $content;
    //die();
   
    if (isset(Yii::$app->request->referrer)) {
        echo $mpdf->render();
        return Yii::$app->request->referrer;
    } else {
        return $mpdf->render();
    }
  }
  
  
}
