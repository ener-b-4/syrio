<?php

namespace app\modules\pdfgenerator\behaviors;

use yii\base\Behavior;

/**
 * Description of PrintableSection
 *
 * @author vamanbo
 */
class PrintableSection extends Behavior{
    
    public $isCampactPrint;
    public $event_name;
    public $session_desc;
    public $isCompilationPrint;
    
}
