<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Renders the 'Details of the location and of the person reporting the event' section of the CRF
 *
 * @author vamanbo
 */

use app\models\events\EventDeclaration;

/* @var $this yii\web\View */
/* @var $model \app\models\events\EventDeclaration */

?>

    <div class="sy_margin_bottom_2">
        <strong><?= \Yii::t('app/crf', 'Details of the location and of the person reporting the event'); ?></strong>
    </div>
    
    <table class='pdf_table_full_width'>
        <tr>
            <td><?= $model->attributeLabels()['operator'] ?>:</td>
            <td><?= $model->operator ?></td>
        </tr>
        <tr>
            <td><?= $model->attributeLabels()['installation_name_type'] ?>:</td>
            <td><?= $model->installation->name . '/' . $model->installation->type ?></td>
        </tr>
        <tr>
            <td><?= $model->attributeLabels()['field_name'] . ' ' . \Yii::t('app/crf', '(if relevant)') ?>:</td>
            <td><?= $model->field_name ?></td>
        </tr>
        <tr>
            <td colspan='2'><br /></td>
        </tr>
        <tr>
            <td><?= $model->attributeLabels()['raporteur_name'] ?>:</td>
            <td><?= $model->raporteur_name ?></td>
        </tr>
        <tr>
            <td><?= $model->attributeLabels()['raporteur_role'] ?>:</td>
            <td><?= $model->raporteur_role ?></td>
        </tr>
        <tr>
            <td colspan='2'><br /></td>
        </tr>
        <tr>
            <td colspan='2'><strong><?= Yii::t('app/crf','Contact details') ?>:</strong></td>
        </tr>
        <tr>
            <td><?= $model->attributeLabels()['contact_tel'] ?>:</td>
            <td><?= $model->contact_tel ?></td>
        </tr>
        <tr>
            <td><?= $model->attributeLabels()['contact_email'] ?>:</td>
            <td><?= $model->contact_email ?></td>
        </tr>
    </table>
