<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

    use yii\helpers\Html;
    use app\models\crf\CrfHelper;
    use app\models\shared\NullValueFormatter;
    use app\models\shared\TextFormatter;
    use app\assets\AppAssetPdf;

    /* @var $this yii\web\View */
    /* @var $model app\models\events\drafts\sections\subsections\C3 */

    $is_pdf = (isset($is_pdf) ? $is_pdf : 0);
    //Compose the request si/no
    if ($is_pdf == 1) { 
        //Registrtion different ASSETS for PDF
        AppAssetPdf::register($this);
    }

?>

<div class="sectionC-3 sy_pad_bottom_18">
    <div class="row">
        <div class="crf-h1 col-lg-12">
            <div class="sy_pad_bottom_18">
                <span class='title-number crf-h1-title-number'>
                    <strong><?= \Yii::t('app', 'C').'.3.'; ?> </strong>
                </span>
                <span class='title-text crf-h1-title-text'>
                    <strong>
                        <?= CrfHelper::SectionsTitle()['C.3'] ?>
                        <?= '(' . \Yii::t('app/crf', 'within 10 working days from the event') . ')' ?>
                    </strong>
                </span>
            </div>
            <div class="crf-h2">
                <div class='row'>
                    <div class="sy_margin_bottom_10">
                        <div class="sy_descriptive_text">
                            <?= NullValueFormatter::Format(
                                TextFormatter::TextToHtml($model->sc3_1)) ?>
                        </div>
                    </div>
                </div>
            </div>            
        </div>
    </div>
</div>

