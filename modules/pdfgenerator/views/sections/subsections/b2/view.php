<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

    use yii\helpers\Html;
    use app\models\units\Units;
    use app\models\crf\CrfHelper;
    use app\models\shared\NullValueFormatter;
    use app\models\shared\TextFormatter;
    use app\assets\AppAssetPdf;

    $units = [
        0 => Units::getUnitsArray(Units::UNIT_TIME),
        1 => Units::getUnitsArray(Units::UNIT_MASS_RATE),
        2 => Units::getUnitsArray(Units::UNIT_METRIC_VOLUME),
        3 => Units::getUnitsArray(Units::UNIT_METRIC_VOLUME),
    ] ;

    //Registrtion different ASSETS for PDF
    AppAssetPdf::register($this);
    
?>


<div class="sectionB-2 sy_pad_bottom_18">
    <div class="row">
        <div class="crf-h1 col-lg-12">
            <div class="sy_pad_bottom_18">
                <span class='title-number crf-h1-title-number'>
                    <strong><?= \Yii::t('app', 'B').'.2.'; ?> </strong>
                </span>
                <span class='title-text crf-h1-title-text'>
                    <strong>
                        <?= CrfHelper::SectionsTitle()['B.2'] ?>
                    </strong>
                </span>
            </div>
            <div class="crf-h2 col-lg-12">
                <div class='row'>
                    <div class="col-lg-12 sy_margin_bottom_10">
                        <span class="crf-item-label"><?= $model->attributeLabels()['b2_1'] . ': ' ?></span>
                        <div class="sy_pad_top_6">
                            <ul class="list-inline sy_pdf_indent_less16">
                                <li>
                                  <?php 
                                //pdf
                                    $checked_value = $model->b2_1;
                                    $checked_text_first = \Yii::t('app', 'Yes');
                                    $checked_text_second = \Yii::t('app', 'No');
                                    $checkbox_checked = '<img src="css/images/checkbox-selected.png" width="16" height="16" style="margin:0 10 0 0;" />';
                                    $checkbox_unchecked = '<img src="css/images/checkbox.png" width="16" height="16" style="margin:0 10 0 0;" />';

                                    $s='';
                                    if (!isset($checked_value)) {
                                        //both unchecked
                                        $s = $checkbox_unchecked . $checked_text_first . '<br/>'. $checkbox_unchecked. $checked_text_second;
                                    } elseif ($checked_value != 0) {
                                        //first (Yes) checked
                                        //second (No) unchecked
                                        $s = $checkbox_checked . $checked_text_first . '<br/>'. $checkbox_unchecked. $checked_text_second;
                                    } else {
                                        //first (Yes) unchecked
                                        //second (No) checked
                                        $s = $checkbox_unchecked . $checked_text_first .'<br/>'. $checkbox_checked. $checked_text_second;
                                    }
                                    echo $s;
                                  ?>
                                </li>
                            </ul>
                        </div>
                    </div> <!-- end column -->
                    
                    <div class="col-lg-12 sy_margin_bottom_10">
                        <span class="crf-item-label"><?= $model->attributeLabels()['b2_2'] . ': ' ?></span>
                        <div class="sy_pad_top_6">
                              <?php 
                            //pdf
                                $checked_value = $model->b2_2;
                                $checked_text_first = \Yii::t('app', 'Yes');
                                $checked_text_second = \Yii::t('app', 'No');
                                $checkbox_checked = '<img src="css/images/checkbox-selected.png" width="16" height="16" style="margin:0 10 0 0;" />';
                                $checkbox_unchecked = '<img src="css/images/checkbox.png" width="16" height="16" style="margin:0 10 0 0;" />';

                                $s='';
                                if (!isset($checked_value)) {
                                    //both unchecked
                                    $s = $checkbox_unchecked . $checked_text_first . '<br/>'. $checkbox_unchecked. $checked_text_second;
                                } elseif ($checked_value != 0) {
                                    //first (Yes) checked
                                    //second (No) unchecked
                                    $s = $checkbox_checked . $checked_text_first . '<br/>'. $checkbox_unchecked. $checked_text_second;
                                } else {
                                    //first (Yes) unchecked
                                    //second (No) checked
                                    $s = $checkbox_unchecked . $checked_text_first .'<br/>'. $checkbox_checked. $checked_text_second;
                                }
                                echo $s;
                              ?>
                        </div>
                    </div> <!-- end column -->

                    <div class="col-lg-12 sy_margin_bottom_10">
                        <span class="crf-item-label"><?= $model->attributeLabels()['b2_3'] . ': ' ?></span>
                        <div class="sy_pad_top_6">
                              <?php 
                            //pdf
                                $checked_value = $model->b2_3;
                                $checked_text_first = \Yii::t('app', 'Yes');
                                $checked_text_second = \Yii::t('app', 'No');
                                $checkbox_checked = '<img src="css/images/checkbox-selected.png" width="16" height="16" style="margin:0 10 0 0;" />';
                                $checkbox_unchecked = '<img src="css/images/checkbox.png" width="16" height="16" style="margin:0 10 0 0;" />';

                                $s='';
                                if (!isset($checked_value)) {
                                    //both unchecked
                                    $s = $checkbox_unchecked . $checked_text_first . '<br/>'. $checkbox_unchecked. $checked_text_second;
                                } elseif ($checked_value != 0) {
                                    //first (Yes) checked
                                    //second (No) unchecked
                                    $s = $checkbox_checked . $checked_text_first . '<br/>'. $checkbox_unchecked. $checked_text_second;
                                } else {
                                    //first (Yes) unchecked
                                    //second (No) checked
                                    $s = $checkbox_unchecked . $checked_text_first .'<br/>'. $checkbox_checked. $checked_text_second;
                                }
                                echo $s;
                              ?>
                        </div>
                    </div> <!-- end column -->

                    <div class="col-lg-12 sy_margin_bottom_10">
                        <span class="crf-item-label"><?= $model->attributeLabels()['b2_4'] . ': ' ?></span>
                        <div class="sy_pad_top_6">
                            <ul class="list-inline sy_pdf_indent_less16">
                                <li>
                                  <!-- (a) -->
                                  <?php 
                                    if ($model->b2_4_a == true)  { echo '<img src="css/images/checkbox-selected.png" width="16" height="16" />'; }
                                    else { echo '<img src="css/images/checkbox.png" width="16" height="16" />'; }
                                  ?>
                                  (a)
                                  <span class="sy_descriptive_text">
                                    <?php echo $model->b2_4_a_desc ?> <br />
                                  </span>
                                  <!-- (b) -->
                                  <?php 
                                    if ($model->b2_4_b == true)  { echo '<img src="css/images/checkbox-selected.png" width="16" height="16" />'; }
                                    else { echo '<img src="css/images/checkbox.png" width="16" height="16" />'; }
                                  ?>
                                  (b)
                                  <span class="sy_descriptive_text">
                                    <?php print $model->b2_4_b_desc ?>  <br />
                                  </span>
                                  <!-- (c) -->
                                  <?php 
                                    if ($model->b2_4_c == true)  { echo '<img src="css/images/checkbox-selected.png" width="16" height="16" />'; }
                                    else { echo '<img src="css/images/checkbox.png" width="16" height="16" />'; }
                                  ?>
                                  (c) 
                                  <span class="sy_descriptive_text">
                                      <?php echo $model->b2_4_c_desc ?>  <br />
                                  </span>
                                </li>
                            </ul>
                        </div>
                    </div> <!-- end column -->
                    
                    <div class="col-lg-12 sy_margin_bottom_10">
                                    <div class="sy_pad_top_18 title-text sy_margin_bottom_10">
                                        <strong>
                                            <?= \Yii::t('app/crf', 'Description of circumstances') ?>
                                        </strong>
                                    </div>
                                    <div class="row section-content sy_margin_bottom_10">
                                        <div class="col-lg-12 sy_descriptive_text sy_margin_bottom_10 text-info' ?>">
                                            <?= NullValueFormatter::Format(
                                                        TextFormatter::TextToHtml($model->b2_5)) ?>
                                        </div>
                                        
                                        <br />
                                            
                                        <div class="col-lg-12">
                                            <?= \Yii::t('app/crf', 'Further details') ?> 
                                        </div>
                                        
                                        <div class="col-lg-12 sy_pad_top_18">
                                            <?php
                                            $index = 0;
                                            foreach(range('a','d') as $letter) {
                                                $attr_q = 'b2_6_'.$letter.'_q';
                                                $attr_u = 'b2_6_'.$letter.'_u';

                                                ?>
                                            <div class="sy-indent-1">
                                                <span class="crf-item-label sy_pad_right_6">
                                                    <?= $model->attributeLabels()[$attr_q] ?>
                                                </span>
                                                <span class="crf-item-label sy_pad_right_6 sy_descriptive_text <?php echo json_encode($is_pdf) == 1 ? 'text-info' : '' ?>">
                                                    <?= 
                                                    NullValueFormatter::format($model->$attr_q) . ' ' .
                                                    NullValueFormatter::FormatDropdown($model->$attr_u, $units[$index]);
                                                    ?>
                                                </span>
                                            </div>
                                            <?php
                                                $index++;
                                            }?>
                                            
                                        </div>
                                    </div>          <!-- end row section-content 2-->
                    </div>   <!-- end column -->     
                    
                    <div class="col-lg-12 sy_margin_bottom_10">
                        <div class="sy_pad_top_18 title-text sy_margin_bottom_10">
                            <br/>   <!-- just to jump on the next page -->
                            <strong><?= \Yii::t('app/crf', 'Consequences of event and emergency response') ?></strong>
                        </div>
                        <div class="row section-content">
                            <div class="col-lg-12 sy_descriptive_text sy_margin_bottom_10 <?php echo json_encode($is_pdf) == 1 ? 'text-info' : '' ?>">
                                <?= NullValueFormatter::Format(
                                            TextFormatter::TextToHtml($model->b2_7)) ?>
                            </div>
                        </div>          <!-- end row section-content 3-->
                    </div>                    
                </div>
            </div>            
        </div>
    </div>
</div>

