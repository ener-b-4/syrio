<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use app\models\crf\CrfHelper;
use app\models\shared\NullValueFormatter;
use app\models\shared\TextFormatter;
use app\assets\AppAssetPdf;

    //Registrtion different ASSETS for PDF
    AppAssetPdf::register($this);
?>

<div class="sectionA-4 sy_pad_bottom_18">
    <div class="row">
        <div class="crf-h1 col-lg-12">
            <div style="margin-bottom: 10px">
                <span class='title-number crf-h1-title-number'>
                    <strong><?= \Yii::t('app', 'A').'.4.'; ?> </strong>
                </span>
                <span class='title-text crf-h1-title-text'>
                    <strong>
                        <?= CrfHelper::SectionsTitle()['A.4'] ?>
                        <?= '(' . \Yii::t('app/crf', 'within 10 working days from the event') . ')' ?>
                    </strong>
                </span>
            </div>
            <div class="crf-h2 col-lg-12">
                <div class='row'>
                    <div class="col-lg-12 sy_margin_bottom_10">
                        <div class="sy_descriptive_text">
                            <?= NullValueFormatter::Format(TextFormatter::TextToHtml($model->a4_1)) ?>
                        </div>
                    </div>
                </div>
            </div>            
        </div>
    </div>
</div>

