<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

    use yii\helpers\Html;
    use app\models\shared\Constants;
    use app\models\units\Units;
    use app\assets\AppAssetPdf;

    use app\models\crf\CrfHelper;
    use app\models\shared\NullValueFormatter;

    use yii\helpers\ArrayHelper;

    $liquidTypes=ArrayHelper::map(\app\models\shared\Constants::find()->where(['type'=>10])->asArray()->all(), 'num_code', 'name');
    $wellheadTypes=ArrayHelper::map(\app\models\shared\Constants::find()->where(['type'=>11])->asArray()->all(), 'num_code', 'name');
    $activityTypes = ArrayHelper::map(\app\models\shared\Constants::find()->where(['type'=>8])->asArray()->all(), 'num_code', 'name');
    $servicesTypes = ArrayHelper::map(\app\models\shared\Constants::find()->where(['type'=>9])->asArray()->all(), 'num_code', 'name');
    $pressureUnits=ArrayHelper::map(\app\models\units\Units::find()->where(['scope'=>5])->asArray()->all(), 'id', 'unit');
    $tempUnits=ArrayHelper::map(\app\models\units\Units::find()->where(['scope'=>7])->asArray()->all(), 'id', 'unit');
    $distUnits=ArrayHelper::map(\app\models\units\Units::find()->where(['scope'=>6])->asArray()->all(), 'id', 'unit');

    //Registrtion different ASSETS for PDF
    AppAssetPdf::register($this);
?>


<div class="sectionB-1 sy_pad_bottom_18">
    <div class="row">
        <div class="crf-h1 col-lg-12">
            <div class="sy_pad_bottom_18">
                <span class='title-number crf-h1-title-number'>
                    <strong><?= \Yii::t('app', 'B').'.1.'; ?> </strong>
                </span>
                <span class='title-text crf-h1-title-text'>
                    <strong>
                        <?= CrfHelper::SectionsTitle()['B.1'] ?>
                    </strong>
                </span>
            </div>            
            <div class="crf-h2 col-lg-12">
                <div class='row'>
                    <div class="col-lg-12 sy_margin_bottom_10">
                        <span class="crf-item-label">(a) </span>
                        <span class="crf-item-label"><?= $model->attributeLabels()['b1_1'] . ': ' ?></span>
                        <span class="crf-item-label sy_descriptive_text">
                            <?= NullValueFormatter::format($model->b1_1) ?>
                        </span>
                    </div>
                    <div class="col-lg-12 sy_margin_bottom_10">
                        <span class="crf-item-label">(b) </span>
                        <span class="crf-item-label"><?= $model->attributeLabels()['b1_2'] . ': ' ?></span>
                        <span class="crf-item-label sy_descriptive_text"><?= 
                            NullValueFormatter::format($model->b1_2)
                        ?></span>
                    </div>
                    <div class="col-lg-12 sy_margin_bottom_10">
                        <span class="crf-item-label">(c) </span>
                        <span class="crf-item-label"><?= $model->attributeLabels()['b1_3'] . ': ' ?></span>
                        <span class="crf-item-label sy_descriptive_text"><?= 
                            NullValueFormatter::format($model->b1_3)
                        ?></span>
                    </div>
                    <div class="col-lg-12 sy_margin_bottom_10">
                        <span class="crf-item-label">(d) </span>
                        <span class="crf-item-label"><?= $model->attributeLabels()['b1_4'] . ': ' ?></span>
                        <div style='margin-left: 24px;'>
                            <span class="crf-item-label sy_descriptive_text"><?= 
                                NullValueFormatter::format($model->b1_4_s)
                            ?></span>
                            <span class="glyphicon glyphicon-arrow-right"></span>
                            ->
                            <span class="crf-item-label sy_descriptive_text"><?= 
                                NullValueFormatter::format($model->b1_4_e)
                            ?></span>
                        </div>
                    </div>
                    <div class="col-lg-12 sy_margin_bottom_10">
                        <span class="crf-item-label">(e) </span>
                        <span class="crf-item-label"><?= $model->attributeLabels()['b1_5'] . ': ' ?></span>
                        <span class="crf-item-label sy_descriptive_text"><?= 
                            NullValueFormatter::FormatDropdown($model->b1_5, $liquidTypes,null, 'app/data');
                            //NullValueFormatter::format($model->b1_5)
                        ?></span>
                    </div>
                    <div class="col-lg-12 sy_margin_bottom_10">
                        <span class="crf-item-label">(f) </span>
                        <span class="crf-item-label"><?= $model->attributeLabels()['b1_6'] . ': ' ?></span>
                        <span class="crf-item-label sy_descriptive_text"><?= 
                            NullValueFormatter::FormatDropdown($model->b1_6, $wellheadTypes,null, 'app/data');
                            //NullValueFormatter::format($model->b1_6)
                        ?></span>
                    </div>
                    <div class="col-lg-12 sy_margin_bottom_10">
                        <span class="crf-item-label">(g) </span>
                        <span class="crf-item-label"><?= $model->attributeLabels()['b1_7'] . ': ' ?></span>
                        <span class="crf-item-label sy_descriptive_text"><?= 
                            NullValueFormatter::format($model->b1_7)
                        ?></span>
                    </div>
                    <div class="col-lg-12 sy_margin_bottom_10">
                        <span class="crf-item-label">(h) </span>
                        <span class="crf-item-label"><?= $model->attributeLabels()['b1_8'] . ': ' ?></span>
                        <span class="crf-item-label sy_descriptive_text">
                            <?=
                            NullValueFormatter::format($model->b1_8_1_q) . ' ' .
                            NullValueFormatter::FormatDropdown($model->b1_8_1_u, $pressureUnits) . ' / ';
                            ?>
                        </span>
                        <span class="crf-item-label sy_descriptive_text">
                            <?=
                            NullValueFormatter::format($model->b1_8_2_q) . ' ' .
                            NullValueFormatter::FormatDropdown($model->b1_8_2_u, $tempUnits) . ' / ';
                            ?>
                        </span>
                        <span class="crf-item-label sy_descriptive_text">
                            <?=
                            NullValueFormatter::format($model->b1_8_3_q) . ' ' .
                            NullValueFormatter::FormatDropdown($model->b1_8_3_u, $distUnits);
                            ?>
                        </span>
                    </div>
                    <div class="col-lg-12 sy_margin_bottom_10">
                        <span class="crf-item-label">(i) </span>
                        <span class="crf-item-label"><?= $model->attributeLabels()['b1_9'] . ': ' ?></span>
                        <span class="crf-item-label sy_descriptive_text "><?= 
                            NullValueFormatter::FormatDropdown($model->b1_9, $activityTypes, null, 'app/data');
                            //NullValueFormatter::format($model->b1_9)
                        ?></span>
                    </div>
                    <div class="col-lg-12 sy_margin_bottom_10">
                        <span class="crf-item-label">(j) </span>
                        <span class="crf-item-label"><?= $model->attributeLabels()['b1_10'] . ' ' .
                                \Yii::t('app/crf', '(if applicable)') . ': ' ?></span>
                        <span class="crf-item-label sy_descriptive_text"><?php
                            if (isset($model->b1_9) && $model->b1_9 == 704)
                            {
                                if (isset($model->b1_10)) 
                                { 
                                    echo \Yii::t('app/data', Constants::findOne(['num_code'=>$model->b1_10])->name);
                                }
                                else
                                {
                                    echo NullValueFormatter::Format($model->b1_10);
                                }
                            }
                            else
                            {
                                echo NullValueFormatter::Format($model->b1_10);
                                //$null_txt = 'NA';
                                //echo $null_txt;
                            }
                        ?></span>
                    </div>
                </div>
            </div>            
        </div>
    </div>
</div>


