<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

    use yii\helpers\Html;

    use app\models\crf\CrfHelper;
    use app\models\shared\TextFormatter;
    use app\models\shared\NullValueFormatter;
    use app\assets\AppAssetPdf;

    /* @var $this yii\web\View */
    /* @var $model app\models\events\drafts\sections\subsections\F1 */
   
        //Registrtion different ASSETS for PDF
        AppAssetPdf::register($this);
?>

<div class="sectionF-1 sy_pad_bottom_18">
    <div class="row">
        <div class="crf-h1 col-lg-12">
            <div class='sy_pad_bottom_18'>
                <span class='title-number crf-h1-title-number'>
                    <strong><?= \Yii::t('app', 'F').'.1.'; ?> </strong>
                </span>
                <span class='title-text crf-h1-title-text'>
                    <strong>
                        <?= CrfHelper::SectionsTitle()['F.1'] ?>
                    </strong>
                </span>
            </div>
            
            <div class="crf-h2">
                <div class='row'>
                    <div class="col-lg-12 sy_margin_bottom_10">
                        <span class="crf-item-label"><?= \Yii::t('app', '(a)'); ?> </span>
                        <span class="crf-item-label"><?= $model->attributeLabels()['f1_1'] . ': ' ?></span>

                        <span class="crf-item-label sy_descriptive_text">
                            <?= NullValueFormatter::Format(
                                TextFormatter::TextToHtml($model->f1_1)) ?>
                        </span>
                    </div>
                    <div class="col-lg-12 sy_margin_bottom_10">
                        <span class="crf-item-label"><?= \Yii::t('app', '(b)'); ?> </span>
                        <span class="crf-item-label"><?= $model->attributeLabels()['f1_2'] . ': ' ?></span>

                        <span class="crf-item-label sy_descriptive_text">
                            <?= NullValueFormatter::Format(
                                TextFormatter::TextToHtml($model->f1_2)) ?>
                        </span>
                    </div>
                    <div class="col-lg-12 sy_margin_bottom_10">
                            <span class="crf-item-label"><?= \Yii::t('app', '(c)'); ?> </span>
                            <span class="crf-item-label"><?= $model->attributeLabels()['f1_3'] . ': ' ?></span>

                            <span class="crf-item-label sy_descriptive_text">
                                <?= NullValueFormatter::Format(
                                    TextFormatter::TextToHtml($model->f1_3)) ?>
                            </span>
                    </div>
                </div>
            </div>            
        </div>
    </div>
</div>

