<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

    use yii\helpers\Html;
    use yii\widgets\DetailView;

    use yii\helpers\ArrayHelper;

    use app\models\crf\Sece;
    use app\widgets\ActionMessageWidget;
    use app\assets\AppAssetPdf;

    use app\models\crf\CrfHelper;
    use app\models\shared\TextFormatter;
    use app\models\shared\NullValueFormatter;


    /* @var $this yii\web\View */
    /* @var $model app\models\events\drafts\sections\subsections\C2 */
    /* @var $section2_1 app\models\events\drafts\sections\subsections\C2_1 */
    /* @var $section2_2 app\models\events\drafts\sections\subsections\C2_2 */


    $section2_1 = $model->sC2_1;
    $section2_2 = $model->sC2_2;
  
    //Registrtion different ASSETS for PDF
    AppAssetPdf::register($this);
?>

<?php

/*
$available_seces_a = ArrayHelper::map(
        Sece::find()->where(['and', 'id>=100', 'id<200'])
        ->orderBy('id')->asArray()->all(), 'id', 'name');
*/

$available_seces = ArrayHelper::map(
        Sece::find()->orderBy('id')->asArray()->all(), 'id', 'name');
$model_seces = ArrayHelper::map($section2_1->sC21Seces,'sece_id','sece_id');
$last_group='';
$group_letters = [
    100 => ['a'=>Yii::t('app', 'Structural integrity systems')], 
    200 => ['b'=>Yii::t('app', 'Process containment systems')], 
    300 => ['c'=>Yii::t('app', 'Ignition control systems')], 
    400 => ['d'=>Yii::t('app', 'Detection systems')], 
    500 => ['e'=>Yii::t('app', 'Process containment release systems')],  
    600 => ['f'=>Yii::t('app', 'Protection systems')], 
    700 => ['g'=>Yii::t('app', 'Shutdown systems')], 
    800 => ['h'=>Yii::t('app', 'Navigational aids')], 
    900 => ['i'=>Yii::t('app', 'Rotating equipment - power supply')], 
    1000 => ['j'=>Yii::t('app', 'Escape, evacuation and rescue equipment')], 
    1100 => ['k'=>Yii::t('app', 'Communication system')], 
    1200 => ['l'=>Yii::t('app', 'Other, specify')],];

$UIitems=[];
$current_key = 'a';
$group_seed = 100;

$s='';
foreach ($available_seces as $key=>$value){
    
    //get group letter
    $offset = $key - $group_seed;

    if ($offset > 0 && $offset < 100) {
        //echo 'item in' . $group_seed . ': ' . $key . '<br/>';
        
        $s.='<div class=\'col-sm-4 sy_table_cell\'>';
        $s.=Html::checkbox('sel_sece[]', in_array($key, $model_seces), [
            'label' => $value,
            'value' => $key,
            'disabled' => true,
        ]);
        $s.='</div>';
        
        //$UIitems[$current_key] .= $s;
        //echo $s;
    }
    else
    {
        $attr = 'c21_3_' . key($group_letters[$group_seed]);
        
        //add the 'other section and close the string for current subgroup
        $s.='<div class=\'col-sm-4 sy_table_cell\'>';
        $s.='   <div class=\'other_container\'>';
        $s.='       <div class=\'other_label\'>';
        $s.= Html::checkbox($attr, $section2_1->getAttribute($attr), ['label'=>'other', 'disabled' => true]);
        $s.='       </div>';
        $s.='       <div class=\'other_text\'>';
        $s.= Html::textInput($attr . '_desc', $section2_1->getAttribute($attr . '_desc'), ['readonly'=>true]);
        $s.='       </div>';
        $s.='   </div>';
        $s.='</div>';
        
        //echo $s;
        //die();
        
        $UIitems[$current_key] = $s;
        
        //set all for the new subgroup
        $s='';
        $group_seed += 100;
        $current_key = key($group_letters[$group_seed]);
        //echo '<p>' . $group_letters[$group_seed][$current_key] . '</p>';
        //echo 'item in' . $group_seed . ': ' . $key . '<br/>';

        $s.='<div class=\'col-sm-4 sy_table_cell\'>';
        $s.=Html::checkbox('sel_sece[]', in_array($key, $model_seces), [
            'label' => $value,
            'value' => $key,
        ]);
        $s.='</div>';

        //create a checkbox with the following:
        //echo $s;
    }
}
//add the k(other) manually
$attr = 'c21_3_' . 'k' ;//key($group_letters[$group_seed]);
//add the 'other section and close the string for current subgroup
$s.='<div class=\'col-sm-4 sy_table_cell\'>';
$s.='   <div class=\'other_container\'>';
$s.='       <div class=\'other_label\'>';
$s.= Html::checkbox($attr, $section2_1->getAttribute($attr), ['label'=>'other', 'disabled' => true]);
$s.='       </div>';
$s.='       <div class=\'other_text\'>';
$s.= Html::textInput($attr . '_desc', $section2_1->getAttribute($attr . '_desc'), ['readonly'=>true]);
$s.='       </div>';
$s.='   </div>';
$s.='</div>';
$UIitems[$current_key] = $s;

//add the l(other) other mannualy
$attr = 'c21_3_' . 'l' ;//key($group_letters[$group_seed]);
$current_key = 'l';
//add the 'other section and close the string for current subgroup
$s='<div class=\'col-sm-4 sy_table_cell\'>';
$s.='   <div class=\'other_container\'>';
$s.='       <div class=\'other_label\'>';
$s.= Html::checkbox($attr, $section2_1->getAttribute($attr), ['label'=>'other', 'disabled' => true]);
$s.='       </div>';
$s.='       <div class=\'other_text\'>';
$s.= Html::textInput($attr . '_desc', $section2_1->getAttribute($attr . '_desc'), ['readonly'=>true]);
$s.='       </div>';
$s.='   </div>';
$s.='</div>';
$UIitems[$current_key] = $s;


//echo '<pre>';
//echo var_dump($UIitems);
//echo '</pre>';
//die();


?>

<div class="sectionC-2 sy_pad_bottom_18">
    <div class="row">
        <div class="crf-h1 col-lg-12">
            <span class='title-number crf-h1-title-number'>
                <strong><?= \Yii::t('app', 'C').'.2.'; ?> </strong>
            </span>
            <span class='title-text crf-h1-title-text'>
                <strong>
                    <?= CrfHelper::SectionsTitle()['C.2'] ?>
                </strong>
            </span>
            <div class="section-content crf-h1-section-content">
                <br/>
            </div>            
            <div class='row'>
                <div class="col-lg-12 sy_margin_bottom_10">
                <?= $this->render('view-parts/c_2_1', [
                    'model' => $model, 
                    'is_pdf' => $is_pdf
                ]) ?>
                </div>
                <div class="col-lg-12 sy_margin_bottom_10">
                <?= $this->render('view-parts/c_2_2', [
                    'model' => $model,
                    'is_pdf' => $is_pdf
                ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>

