<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

  use yii\helpers\Html;

  use app\models\crf\CrfHelper;
  use app\models\shared\TextFormatter;
  use app\models\shared\NullValueFormatter;
  use app\assets\AppAssetPdf;
  
  /* @var $this yii\web\View */
  /* @var $model app\models\events\drafts\sections\subsections\C2 */

  //Registrtion different ASSETS for PDF
  AppAssetPdf::register($this);

?>

<div class="row" id="content-c2_2">
    

<div class='sy_margin_bottom_20 col-lg-12'>
        
        <div class='row form-inline'>
            
            <div class="col-lg-12 sy_margin_bottom_10 crf-h1-font">
                <span class='title-number'><strong><?= \Yii::t('app', 'C').'.2.2.' ?></strong></span> 
                <span class='title-text'><strong><?= CrfHelper::SectionsTitle()['C.2.2'] ?></strong></span>
            </div>

            <div class="crf-h1 col-lg-12">
                <div class="section-content crf-h1-section-content">
                    <div class='row'>
                        <div class="col-lg-12">
                            <div class='sy_margin_bottom_10'>
                                <strong><i><?php
                                    $text = Yii::t('app/crf', 'Is the incident likely to cause degradation to the surrounding marine environment?');
                                    echo $text;
                                ?></i></strong>
                            </div>
                        </div>
                        <div class="col-lg-12 sy_margin_bottom_10">
                            <?php
                            
                                //pdf
                                $checked_value = $model->sC2_2->c22_1;
                                $checked_text_first = \Yii::t('app', 'Yes');
                                $checked_text_second = \Yii::t('app', 'No');
                                $checkbox_checked = '<img src="css/images/checkbox-selected.png" width="16" height="16" style="margin:10 10 10 10;" />';
                                $checkbox_unchecked = '<img src="css/images/checkbox.png" width="16" height="16" style="margin:10 10 10 10;" />';

                                $s='';
                                if (!isset($checked_value)) {
                                    //both unchecked
                                    $s = $checked_text_first . $checkbox_unchecked . ' '. $checked_text_second . $checkbox_unchecked;
                                } elseif ($checked_value != 0) {
                                    //first (Yes) checked
                                    //second (No) unchecked
                                    $s = $checked_text_first . $checkbox_checked . ' '. $checked_text_second . $checkbox_unchecked;
                                } else {
                                    //first (Yes) unchecked
                                    //second (No) checked
                                    $s = $checked_text_first . $checkbox_unchecked . ' '. $checked_text_second . $checkbox_checked;
                                }
                                echo $s;
                            ?>
                        </div> <!-- end col -->
                        <div class="col-lg-12 sy_margin_bottom_10">
                            <div class='sy_margin_bottom_10'>
                            <?php
                                $txt = 'If yes, outline the environmental impacts which already have been observed or are likely to result from the incident';
                                echo \Yii::t('app/crf',  $txt).'.';
                            ?>
                            </div>

                            <div class="sy_descriptive_text">
                                <?= NullValueFormatter::Format(
                                            TextFormatter::TextToHtml($model->sC2_2->c22_desc)) ?>
                            </div>
                        </div> <!-- end col -->
                    </div>
                </div>            
            </div>
        </div>
</div>

</div>
