<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;

use app\models\crf\CrfHelper;
use app\models\shared\TextFormatter;
use app\models\shared\NullValueFormatter;


use yii\widgets\DetailView;

use yii\helpers\ArrayHelper;

use app\models\crf\Sece;
use app\widgets\ActionMessageWidget;
use app\assets\AppAssetPdf;

/* @var $this yii\web\View */
/* @var $model app\models\events\drafts\sections\subsections\C2 */
/* @var $section2_1 app\models\events\drafts\sections\subsections\C2_1 */

$section2_1 = $model->sC2_1;

/* @var $this yii\web\View */
/* @var $model app\models\events\drafts\sections\subsections\C1 */

//Registrtion different ASSETS for PDF
AppAssetPdf::register($this);

?>

<?php

/*
$available_seces_a = ArrayHelper::map(
        Sece::find()->where(['and', 'id>=100', 'id<200'])
        ->orderBy('id')->asArray()->all(), 'id', 'name');
*/

$available_seces = ArrayHelper::map(
        Sece::find()->orderBy('id')->asArray()->all(), 'id', 'name');
$model_seces = ArrayHelper::map($section2_1->sC21Seces,'sece_id','sece_id');
$last_group='';
$group_letters = [
    100 => ['a'=>Yii::t('app/crf', 'Structural integrity systems')], 
    200 => ['b'=>Yii::t('app/crf', 'Process containment systems')], 
    300 => ['c'=>Yii::t('app/crf', 'Ignition control systems')], 
    400 => ['d'=>Yii::t('app/crf', 'Detection systems')], 
    500 => ['e'=>Yii::t('app/crf', 'Process containment release systems')],  
    600 => ['f'=>Yii::t('app/crf', 'Protection systems')], 
    700 => ['g'=>Yii::t('app/crf', 'Shutdown systems')], 
    800 => ['h'=>Yii::t('app/crf', 'Navigational aids')], 
    900 => ['i'=>Yii::t('app/crf', 'Rotating equipment - power supply')], 
    1000 => ['j'=>Yii::t('app/crf', 'Escape, evacuation and rescue equipment')], 
    1100 => ['k'=>Yii::t('app/crf', 'Communication system')], 
    1200 => ['l'=>Yii::t('app/crf', 'Other, specify')],];

?>


<div class="row" id="content-a1_1">
    <div class='sy_margin_bottom_20 col-lg-12'>
        
        <div class='row form-inline'>
            
            <div class="col-lg-12 sy_margin_bottom_10 crf-h1-font">
                <span class='title-number'><strong><?= \Yii::t('app', 'C').'.2.1.' ?></strong></span> 
                <span class='title-text'><strong><?= CrfHelper::SectionsTitle()['C.2.1'] ?></strong></span>
            </div>

                <div class='col-lg-12 crf-base-font'>
                    <!-- Which Safety and Environmental Critical Systems were reported by the independent verifier -->
                    <div class='row'>
                        <div class="col-lg-12 section-content ">
                            <p>
                                <?php
                                    $text = Yii::t('app/crf', 'Which Safety and Environmental Critical systems were reported by the independent verifier as lost or unavailable, requiring immediate remedial action, or have failed during an incident?');
                                    echo $text;
                                ?>
                            </p>
                        </div> 
                    </div>

                    <!-- Origin -->
                    <div class='row'>
                        <div class="crf-h2 col-lg-12">
                            <div class="row form-inline">                            
                                <div class="col-lg-12 crf-base-font">

                                    <table class="pdf_table_no_border pdf_table_inline">
                                        <tbody>
                                            <tr>
                                                <td><strong><em><?= \Yii::t('app/crf', 'Origin') . ': ' ?></em></strong></td>
                                                <td class="sy_pad_left_30"><?= $section2_1->attributeLabels()['c21_1'] ?></td>
                                            <tr>
                                                <td></td>
                                                <td class="sy_pad_left_30">
                                                    <span class="sy_descriptive_text text-info">
                                                        <?= NullValueFormatter::format($section2_1->c21_1) ?>
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td class="sy_pad_left_30"><?= $section2_1->attributeLabels()['c21_2'] ?></td>
                                            <tr>
                                                <td></td>
                                                <td class="sy_pad_left_30">
                                                    <span class="sy_descriptive_text text-info">
                                                        <?= NullValueFormatter::format($section2_1->c21_2)?>
                                                    </span>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    
                                </div>
                            </div>
                        </div>
                    </div> 

            </div>
            
        </div>
    </div>

    <br />
        
    <!-- Which Safety and Environmental Critical Systems were reported by the independent verifier -->
    <div class='sy_margin_bottom_20'>
        
        <div class='row form-inline'>

            <div class="col-lg-12 crf-h2">
                <div class='col-lg-12 section-content'>
                    <!-- Which Safety and Environmental Critical Systems were reported by the independent verifier -->
                    <div class='row'>
                        <div class="col-lg-12">
                            <strong><?php echo Yii::t('app/crf', 'Safety and Environmental Critical Elements concerned'); ?> </strong>
                        </div> 
                    </div>

                    <div class='row'>
                        <div class="col-lg-12">
                            <div class="row form-inline">                            
                                <div class="col-lg-12">
                                    <table class="table crf-table">
                                            <tbody>
                                                    <?php
                                                    reset($group_letters);
                                                    foreach ($group_letters as $key=>$value) {
                                                            //  key             = 100
                                                            //  value           = ['a', 'Structural integrity']
                                                            //  key($value)     = 'a'
                                                            //  current($value) = 'Structural integrity'

                                                            $current_letter = key($value);

                                                            $current_seces = ArrayHelper::map( 
                                                                            Sece::find()
                                                                            ->where(['between', 'id', intval($key), intval($key+99)])->all(),
                                                                            'id', 'name');

                                                            //create the sece type header row
                                                            $row = "<tr class='sy_table_sece_header'>"
                                                                            . " <th colspan='3'>"
                                                                            . " <b>($current_letter)" . ' ' . current($value) . "</b>"
                                                                            . " </th>"
                                                                            . "</tr>";
                                                            echo $row;

                                                            //populate the seces cells in a three cell layout
                                                            $attr = 'sece_'.$current_letter;

                                                                echo Html::activeCheckboxList($section2_1, $attr, $current_seces, [
                                                                    'item' => function ($index, $label, $name, $checked, $value) {
                                                                            $openRow = $index % 3 === 0 ? '<tr>' : '';
                                                                            $closeRow = $index % 3 === 2 ? '</tr>' : '';

                                                                            $cell = '<td style="width:33%">';
                                                                            
                                                                            if ($checked == true)  { $check_img = '<img src="css/images/checkbox-selected.png" width="16" height="16" style="padding-top: 3px;" />'; } 
                                                                            else { $check_img = '<img src="css/images/checkbox.png" width="16" height="16" style="padding-top: 3px;" />';          }
                                                                            
                                                                            $cell_t = '<table border="0" cellspacing="0" cellpadding="0">'
                                                                                  . '<tr>'
                                                                                  . '  <td style = "border-bottom:0px;border-left:0px;border-top:0px;border-right:0px; padding:0px; padding-right:3px; ">' . $check_img . '</td>'
                                                                                  . '  <td style = "border-bottom:0px;border-left:0px;border-top:0px;border-right:0px; padding:0px; padding-right:3px; padding-top:2px">' . Yii::t('app/crf', $label) . '</td>'
                                                                                  . '</tr>'
                                                                                  . '</table>';
                                                                            
                                                                            //$cell = $cell . $label . '</td>';
                                                                            $cell .= $cell_t . '</td>';

                                                                            return $openRow . $cell . $closeRow;
                                                                    }
                                                                ]);
                                                            
                                                            $last_row_cells_count = count($current_seces) % 3;

                                                            //create the other cell
                                                            $other_attr = 'c21_3_'.$current_letter;
                                                            $other_text_attr = 'c21_3_'.$current_letter.'_desc';
                                                            $other_text_div_id = $other_text_attr . '_div';

                                                            $other_cell = '';
                                                            
                                                                    $check_img = null;
                                                                    if ($section2_1->$other_attr == 1)  { $check_img = '<img src="css/images/checkbox-selected.png" width="16" height="16" style="padding-top: 3px; float: left;" />'; }; 
                                                                    if ($section2_1->$other_attr == 0) { $check_img = '<img src="css/images/checkbox.png" width="16" height="16" style="padding-top: 3px; float: left;" />';          };   
                                                                    $other_cell = $other_cell . '<td>';
                                                                    
                                                                    $other_html = isset($section2_1->$other_text_attr) && 
                                                                            $section2_1->$other_text_attr !== '' ? NullValueFormatter::Format($section2_1->$other_text_attr) : '';
                                                                    
                                                                    $cell_t = '<table border="0" cellspacing="0" cellpadding="0">'
                                                                          . '<tr>'
                                                                          . '  <td style = "border-bottom:0px;border-left:0px;border-top:0px;border-right:0px; padding:0px; padding-right:3px; ">' . $check_img . '</td>'
                                                                          . '  <td style = "border-bottom:0px;border-left:0px;border-top:0px;border-right:0px; padding:0px; padding-right:3px; padding-top:2px">' . 
                                                                            $section2_1->getAttributeLabel($other_attr) 
                                                                            . '</td>'
                                                                            . '<td style = "border-bottom:0px;border-left:0px;border-top:0px;border-right:0px; padding:0px; padding-right:3px; padding-top:2px">'
                                                                                    . '<span id = "' . $other_text_div_id . '" class="sy_descriptive_text' . (json_encode($is_pdf) == 1 ? ' text-info ' : ' ') . '">' 
                                                                                    . ' ' . $other_html
                                                                                    . '</span>'
                                                                            . '</td>'                                                                            
                                                                          . '</tr>'
                                                                          . '</table>';
                                                                    $other_cell = $other_cell . $cell_t                                                                            
                                                                                    . '</td>';
                                                            
                                                            $openRow = $last_row_cells_count % 3 === 0 ? '<tr>' : '';
                                                            //$closeRow = $last_row_cells_count === 2 ? '</tr>' : '';

                                                            echo $openRow . $other_cell;
                                                            $last_row_cells_count++;

                                                            //create the additional cells (if needed)
                                                            $missing_cells = 3 - ($last_row_cells_count !== 0 ? $last_row_cells_count : 3) ; // intval(count($current_seces) / 3);

                                                            if ($missing_cells !== 0 && $missing_cells < 3) {
                                                                    for($i = 1; $i<=$missing_cells; $i++) {
                                                                            echo '<td></td>';
                                                                    }
                                                                    echo '</tr>';
                                                            }
                                                    }
                                                    ?>
                                            </tbody>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div> 

                </div>
            </div>
            
        </div>
    </div>    
    
</div>

