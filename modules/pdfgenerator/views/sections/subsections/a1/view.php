<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

  use yii\helpers\Html;

  use app\models\AppConstants;
  
  /* @var $this yii\web\View */
  /* @var $model app\models\crf\Section1 */
  /* @var $event_id integer */


  $section_name = 'A';
  //$this->title = 'Section '. $section_name;
 
  $is_pdf = (isset($is_pdf) ? $is_pdf : 0);
?>

<div class="a1-view sy_pad_top_36">

    <?php
        $is_pdf = 1;
        include_once 'view_parts/_review.php';
    ?>  

</div>

