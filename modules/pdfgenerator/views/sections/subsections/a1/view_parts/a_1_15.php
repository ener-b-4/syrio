<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\assets\AppAssetPdf;
use app\models\shared\NullValueFormatter;
use app\models\shared\TextFormatter;

/**
 * Performs a validation and rendering logic for section a_1_xv
 * Render block in $a1xii_block
 *
 * @var $model app\models\crf\Section1
 * 
 * @author bogdanV
 */

/* @var $model app\models\crf\Section1 */

//Compose the request si/no
//Registrtion different ASSETS for PDF
AppAssetPdf::register($this);

?>

<!--
SECTION A.1.XV
-->

<div class="row" id="content-a1_xv">
    <div class='col-lg-12 sy_margin_bottom_20'>
        <div class='row form-inline crf-h2-box'>
            <div class="col-lg-12 crf-h2 sy_margin_bottom_10">
                <span class='title-number'>XV.</span> 
                <span class='title-text'><strong><?= $model->attributeLabels()['a1xv'] ?></strong></span> 
            </div>
            
            <div class="col-lg-12 crf-h2">
                <div class='col-lg-12 section-content'>
                    <div class='row'>
                        <div class="col-lg-6">
                            <div class="row form-inline">                            
                                <div class="col-lg-12">
                                    <ul class="list-inline sy_margin_bottom_0 clear-margin-left">
                   
                                        <li class='sy_vert_top text-info'>
                                            <?= NullValueFormatter::Format(TextFormatter::TextToHtml($model->a1xv)) ?>
                                        </li>
                                        
                                    </ul>
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
         </div>
    </div>
</div>        <!-- close content-a1_xv -->


