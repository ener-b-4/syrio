<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use app\models\shared\NullValueFormatter;
use app\assets\AppAssetPdf;

/**
 * Performs a validation and rendering logic for section a_1_xii
 * Render block in $a1xii_block
 *
 * @var $model app\models\crf\Section1
 * 
 * @author bogdanV
 */

/* @var $model app\models\crf\Section1 */

$yesno = [1=>'Yes', 0=>'No'];
$checkbox_checked = '<img src="css/images/checkbox-selected.png" width="16" height="16" style="margin:10 10 10 10;" />';
$checkbox_unchecked = '<img src="css/images/checkbox.png" width="16" height="16" style="margin:10 10 10 10;" />';

//Registrtion different ASSETS for PDF
AppAssetPdf::register($this);

?>

<div class="row" id="content-a1_xii">
    <div class='col-lg-12 sy_margin_bottom_26'>
        <div class='content'>
            <div class="row form-inline crf-h2-box">
                <div class='col-lg-12 crf-h2'>
                    <span class='title-number'>XII.</span> 
                    <span class='title-text'><strong><?= $model->attributeLabels()['axii'] ?></strong></span> 
                    <span class='text-muted'>
                        <em><?= '(' . \Yii::t('app', 'Please tick appropriate box') . ')' ?></em>
                    </span> 

                    <div class='col-lg-12 section-content'>
                        <div class='row'>   <!-- content row -->
                            <div class='col-lg-12'>     <!-- yesno col12 -->
                                <?php
                                if (!isset($model->axii)) {
                                    //yes is unchecked
                                    //no is unchecked
                                    $s = \Yii::t('app', 'Yes').$checkbox_unchecked.\Yii::t('app', 'No').$checkbox_unchecked;
                                }
                                elseif ($model->axii!=0) {
                                    //yes is checked
                                    //no is unchecked
                                    $s = \Yii::t('app', 'Yes').$checkbox_checked.\Yii::t('app', 'No').$checkbox_unchecked;
                                } else {
                                    //no is checked
                                    //yes is unchecked
                                    $s = \Yii::t('app', 'Yes').$checkbox_unchecked.\Yii::t('app', 'No').$checkbox_checked;
                                }
                                
                                echo $s;
                                ?>
                                
                                <?php
                                if (isset($model->axii) && $model->axii !== 0) {
                                    // the ignition block container //
                                    ?>
                                
                                    <div class='row' id="ignition_block"> <!-- ignition block container div -->
                                        <div class='col-lg-12'>
                                            
                                            <table class="crf_table full-width">
                                                <tr>
                                                    <td>
                                                        <?= \Yii::t('app/crf', 'If yes, was it:') ?>
                                                    </td>
                                                    <td>
                                                        <?php 
                                                        if (!isset($model->axii_code)) {
                                                            //yes is unchecked
                                                            //no is unchecked
                                                            $s = \Yii::t('app/crf', 'Immediate').$checkbox_unchecked.\Yii::t('app/crf', 'Delayed').$checkbox_unchecked;
                                                        }
                                                        elseif ($model->axii_code!=0) {
                                                            //yes is checked
                                                            //no is unchecked
                                                            $s = \Yii::t('app/crf', 'Immediate').$checkbox_checked.\Yii::t('app/crf', 'Delayed').$checkbox_unchecked;
                                                        } else {
                                                            //no is checked
                                                            //yes is unchecked
                                                            $s = \Yii::t('app/crf', 'Immediate').$checkbox_unchecked.\Yii::t('app/crf', 'Delayed').$checkbox_checked;
                                                            echo $s;
                                                            $s='';
                                                            echo '</td>';
                                                            echo '<td>';
                                                            //put the delayed block
                                                            ?>
                                                                <span><?= \Yii::t('app/crf', 'Delay time'). ': ' ?></span>
                                                                <span class="text-info"><?= NullValueFormatter::Format($model->axii_code_time) . ' ' ?></span>
                                                                <em><?= Yii::t('app/crf', '(sec)') ?></em>
                                                            <?php
                                                        }
                                                        echo $s;
                                                        ?>
                                                    </td>
                                                    <td>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        
                                        <div class="col-lg-12" id="a1_xii_table_div">
                                            <p>
                                                <?= \Yii::t('app/crf', 'Was there').':' ?> 
                                                <em class='text-muted'>
                                                    <?= 
                                                    \Yii::t('app/crf', '(add sequence of events by numbering appropriate boxes in order of occurrence)')
                                                    ?>
                                                </em>
                                            </p>
                                            <table class="crf-table full-width">
                                                <tr>
                                                    <td class='half'>
                                                        <span class='crf-item-label'>
                                                            <?= \Yii::t('app/crf', 'A flash fire') ?>
                                                        </span>
                                                        <span class="text-info">
                                                            <?= NullValueFormatter::Format($model->axii_flash_index, lcfirst(Yii::t('app', 'No'))) ?> 
                                                        </span>
                                                    </td>
                                                    <td>
                                                        <span class='crf-item-label'>
                                                            <?= \Yii::t('app/crf', 'An explosion') ?>
                                                        </span>
                                                        <span class="text-info">
                                                            <?= NullValueFormatter::Format($model->axii_exp_index, lcfirst(Yii::t('app', 'No'))) ?> 
                                                        </span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class='half'>
                                                        <span class='crf-item-label'>
                                                            <?= \Yii::t('app/crf', 'A jet fire') ?>
                                                        </span>
                                                        <span class="text-info">
                                                            <?= NullValueFormatter::Format($model->axii_jet_index, lcfirst(Yii::t('app', 'No'))) ?> 
                                                        </span>
                                                    </td>
                                                    <td>
                                                        <span class='crf-item-label'>
                                                            <?= \Yii::t('app/crf', 'A pool fire') ?>
                                                        </span>
                                                        <span class="text-info">
                                                            <?= NullValueFormatter::Format($model->axii_pool_index, lcfirst(Yii::t('app', 'No'))) ?> 
                                                        </span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div> <!-- ignition block container div (row) -->
                                
                                <?php
                                }   // end if ignition occurs block container
                                ?>
                            </div> <!-- end yesno col12 -->
                        </div>  <!-- end content row -->
                    </div> <!-- end section content -->
                </div> <!-- end crf-h2 col -->
            </div>  <!-- end crf-h2-box row -->
        </div>  <!-- end 'content' -->
    </div>  <!-- end col-12 -->

</div>
