<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\assets\AppAssetPdf;

use app\models\shared\NullValueFormatter;
use app\models\shared\TextFormatter;
use app\models\crf\CrfHelper;

/**
 * Performs a validation and rendering logic for section a_1_xiii
 * Render block in $a1xii_block
 *
 * @var $model app\models\crf\Section1
 * 
 * @author bogdanV
 */

/* @var $model app\models\crf\Section1 */

//Registrtion different ASSETS for PDF
AppAssetPdf::register($this);
?>

<!--
SECTION A.1.XIII
-->

<div class="row" id="content-a1_xiii">
    <div class='col-lg-12 sy_margin_bottom_20'>
        <div class='row form-inline crf-h2-box'>
            <div class="col-lg-12 crf-h2 sy_margin_bottom_10">
                <span class='title-number'>XIII</span> 
                <span class='title-text'><strong><?= $model->attributeLabels()['axiii'] ?></strong></span> 
                <span class='text-muted'> <?= '(' . \Yii::t('app', 'if known') . ')' ?></span>
            </div>
            
            <div class="col-lg-12 crf-h2">
                <div class='col-lg-12 section-content'>
                    <div class='row'>
                        <div class="col-lg-6">
                            <div class="row form-inline">                            
                                <div class="col-lg-12">
                                    <span class='text-muted'><?= \Yii::t('app/crf', 'Provide a description of the ignition source') ?></span>
                                    
                                    <ul class="list-inline sy_margin_bottom_0 clear-margin-left">
                   
                                        <li class='sy_vert_top '>
                                            <span class="sy_descriptive_text text-info">
                                                <?= NullValueFormatter::Format(TextFormatter::TextToHtml($model->axiii)) ?>
                                            </span>
                                        </li>
                    
                                    </ul>

                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
         </div>
    </div>
</div>        <!-- close content-a1_xiii -->



