<?php

/// <summary>
/// ============================================================================================
/// Repository path        = $HeadURL: http://stus-test04:8080/svn/SyRIO_beta/branches/OneViewCrf/views/events/drafts/sections/subsections/a1/view_parts/a_1_1_copied.php $
/// File                   = $Id: a_1_1_copied.php 20 2015-11-13 11:24:12Z Jessica $
/// Last changed by        = $Author: Jessica $
/// Last changed date      = $Date: 2015-11-13 12:24:12 +0100 (Fri, 13 Nov 2015) $
/// Last committed         = $Revision: 20 $
/// ============================================================================================
/// Details: View file for section A.1.1
/// ============================================================================================
/// </summary>

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\assets\AppAssetPdf;

/* @var $this yii\web\View */
/* @var $model app\models\crf\Section1 */
/* @var $form yii\widgets\ActiveForm */

//echo 'here: ' . $is_pdf;
//die();

//Compose the request si/no
if (json_encode($is_pdf) == 1) { 
    //Registrtion different ASSETS for PDF
    AppAssetPdf::register($this);
}

?>

<div class="row" id='content-a1_1'>
    <div class="col-lg-12">
        <div class="content">
            <div class="row crf-h2-box">                            
                <div class="col-lg-12 crf-h2">
                    <span class='title-number'>I.</span> 
                    <span class='title-text'><?= \Yii::t('app/crf', 'Hydrocarbon (HC) released') ?></span> 
                    <span class="small em">(<?= \Yii::t('app/crf', 'Tick appropriate box') ?>)</span>
                    <div class="row">
                        <div class="col-lg-12 form-inline section-content">
                            <br/>
                            <div class='sy_pad_bottom_18'>
                                <span class='crf-item-label'>
                                    <?= \Yii::t('app/crf', 'NON-PROCESS') ?>
                                </span>
                                <span class='crf-item-label'>
                                    <?= Html::activeCheckbox($model, 'a1i_np', ['id'=>'chkNp', 'disabled'=>true, 'value'=>1, 'uncheck'=>0, 'label'=>'',
                                        'class' => 'sy_data_toggle',
                                        'sy_data_toggle_dest' => 'a1i_specify_container',
                                        'sy_data_toggle_src_type' => 'checkbox',
                                        'sy_data_toggle_visible_value' => 'true',
                                        'sy_data_toggle_enabled' => 'true'
                                        ]) ?>
                                </span>
                                <span id='a1i_specify_container'>
                                    <?php //$form->field($model, 'a1i_np_specify')->textInput(['maxlength' => 255])->label('') ?>
                                    <span><em>(<?= \Yii::t('app/crf', 'please specify') ?>)</em></span>
                                    <?= app\models\shared\NullValueFormatter::Format($model->a1i_np_specify) ?>
                                </span>
                            </div>
                            <ul class="list-inline clear-margin-left">
                                <li>
                                    <?= \Yii::t('app/crf', 'PROCESS') ?>
                                </li>
                                <li>
                                    <?= Html::activeCheckbox($model, 'a1i_o', [
                                        'id'=>'chkO', 'disabled'=>true, 'value'=>1, 'uncheck'=>0, 'label'=>'Oil'
                                        ]) ?>
                                </li>
                                <li>
                                    <?= Html::activeCheckbox($model, 'a1i_co', [
                                        'id'=>'chkCo', 'disabled'=>true, 'value'=>1, 'uncheck'=>0, 'label'=>'Condensate'
                                        ]) ?>
                                </li>
                                <li>
                                    <?= Html::activeCheckbox($model, 'a1i_g', [
                                        'id'=>'chkG', 'disabled'=>true, 'value'=>1, 'uncheck'=>0, 'label'=>'Gas',
                                        ]) ?>
                                </li>
                                <li>
                                    <?= Html::activeCheckbox($model, 'a1i_2p', [
                                        'id'=>'chk2p', 'disabled'=>true, 'value'=>1, 'uncheck'=>0, 'label'=>'2-Phase',
                                        ]) ?>
                                </li>
                            </ul>

                            <div class='row'>
                                <div class="col-lg-12 sy_toggle_dest" id='h2s_container'
                                            sy_toggle_src_dependent_on = 'chkG chk2p'>
                                    <ul class="list-inline clear-margin-left">
                                        <li>
                                            <?= \Yii::t('app/crf', 'Level of H') ?><sub>2</sub><?= \Yii::t('app/crf', 'S') ?>
                                        </li>
                                        <li>
                                            <?php //$form->field($model, 'a1i_2p_h2s_q')->textInput()->label('') ?>
                                            <?= app\models\shared\NullValueFormatter::Format($model->a1i_2p_h2s_q) ?>
                                        </li>
                                        <li>
                                            <em>(<?= \Yii::t('app/crf', 'estimated ppm') ?>)</em>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>                                    
                    </div>
                </div> <!-- col crf-h2 -->
            </div> <!-- row crf-h2-box -->
        </div>
    </div>
</div>
