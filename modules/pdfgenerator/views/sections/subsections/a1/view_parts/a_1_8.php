<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\assets\AppAssetPdf;
use app\models\shared\NullValueFormatter;
use app\models\shared\TextFormatter;
use app\models\crf\CrfHelper;

/*
 * SyRIO beta
 */

/* @var $this yii\web\View */
/* @var $model app\models\crf\Section1 */

$speedUnits = app\models\units\Units::getUnitsArray(app\models\units\Units::UNIT_SPEED);


    //Registrtion different ASSETS for PDF
    AppAssetPdf::register($this);
?>

<!--
SECTION A.1.VIII
-->

<div class="row" id="content-a1_viii">
    <div class='col-lg-12 sy_margin_bottom_20'>
        <div class='row form-inline crf-h2-box'>
            <div class="col-lg-12 crf-h2 sy_margin_bottom_10">
                <span class='title-number'>VIII.</span> 
                <span class='title-text'><strong><?= $model->attributeLabels()['a1viii'] ?></strong></span> 
            </div>
            
            <div class="col-lg-12 crf-h2">
                <div class='col-lg-12 section-content'>
                    <div class='row'>
                        <div class="col-lg-10">
                            <div class="row form-inline">                            
                                <div class="col-lg-12">
                                    <span><?= $model->attributeLabels()['a1viii_uw_q'] ?></span>
                                    <span class="text-info"><?= NullValueFormatter::Format($model->a1viii_uw_q) ?></span>
                                    <span><?= NullValueFormatter::FormatDropdown($model->a1viii_uw_u, $speedUnits) ?></span>
                                    
                                    <div class='text-muted sy_margin_bottom_20'>
                                        <em>
                                            <?= \Yii::t('app/crf', 'Specify units') . ' ('
                                                . \Yii::t('app', 'e.g.') . ' mph, m/s, ft/s)' ?>
                                        </em>
                                    </div>
                                        
                                    <br/>
                                    
                                </div> 
                            </div>
                        </div>
                    </div>
                    
                    <div class='row'>
                        <div class="col-lg-10">
                            <div class="row form-inline">                            
                                <div class="col-lg-12">
                                    
                                    <span><?= $model->attributeLabels()['a1viii_dw_q'] ?></span>
                                    <span class="text-info"><?= NullValueFormatter::Format($model->a1viii_dw_q) ?></span>
                                        
                                    <div class='text-muted sy_margin_bottom_20'>
                                        <em>
                                           <?= \Yii::t('app/crf', 'Specify heading in degrees') ?>
                                        </em>
                                    </div>
                                        
                                    <br/>
                                </div> 
                            </div>
                        </div>
                    </div>
                    
                    <div class='row'>
                        <div class="col-lg-10">
                            <div class="row form-inline">                            
                                <div class="col-lg-12">
                                    
                                    <div class='text-muted'>
                                        <p>
                                            <?= \Yii::t('app/crf', 'Provide a description of other relevant weather conditions') ?>
                                        </p>
                                        <div class='text-info'>
                                        <?= NullValueFormatter::Format(
                                                    TextFormatter::TextToHtml($model->a1viii_c)) ?>
                                        </div>
                                    </div>
                                </div> 
                            </div>
                        </div>
                    </div>
                    
                    <br>
                </div>
            </div>
         </div>
    </div>

</div>
            

