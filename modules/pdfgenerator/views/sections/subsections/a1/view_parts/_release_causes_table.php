<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

/* @var $this yii\web\View */
/* @var $model app\models\crf\Section1 */
/* @var $all_causes app\models\crf\EventReleaseCause[] */
/* @var $sel_causes app\models\crf\EventReleaseCause[] */
/* @var $other_attr mixed key=>value pair with key the category_id and value the model root attribute (should be appended with _code or _specify) */

/* get the leak types available */
$leak_categories = \app\models\crf\LeakCauseCategories::find()->where(['<>', 'id', 0])->all();

//<img src="css/images/checkbox-selected.png" width="16" height="16" />
//<img src="css/images/checkbox.png" width="16" height="16" />
$template = '<img src="{img_src}" width="16" height="16" /> <span class="inline-label-right">{label}</span>';


$template_other = '<img src="{img_src}" width="16" height="16" /> <span class="inline-label-right">' . Yii::t('app/crf', 'Other, specify') . '</span>'
        . ' <span class="specify-span left-pad {other_visible}">{other_desc}</span>';

?>

<!--<table class="pdf_table pdf_table_full_width pdf_table-bordered">-->
<table class="release_causes">
    <tbody>
        <?php 
        foreach($leak_categories as $category) {
            /* render header */
            ?>
        <tr class="p-head">
            <td colspan="2"><b><?= Yii::t('app/crf', $category->name) ?></b></td>
        </tr>
        <?php
            /* get the causes */
            $index = 0;
            foreach ($all_causes[$category->id] as $cause) {
                $checked = in_array($cause['num_code'], $sel_causes) ? true : false;
                
                $img_src = $checked ? 'css/images/checkbox-selected.png':'css/images/checkbox.png';

                $cinput = str_replace('{label}', Yii::t('app/crf', $cause['cause']), $template);
                $cinput = str_replace('{img_src}', $img_src, $cinput);
                
                if ($index % 2 == 0) {
                    echo '<tr>';
                    echo '<td>'
                    . $cinput
                    //. $cause['cause'] . '('.$cause['num_code'].') '.$checked
                    . '</td>';
                } else {
                    echo '<td>'
                    . $cinput
                    //. $cause['cause'] . '('.$cause['num_code'].') '.$checked
                    . '</td>';
                    
                    echo '</tr>';
                }
                $index+=1;
            }
            
            /* check if other exists */
            if (array_key_exists($category->id, $other_attr)) {
                $attr1 = $other_attr[$category->id].'_code';
                $attr2 = $other_attr[$category->id].'_specify';
                
                //<img src="css/images/checkbox-selected.png" width="16" height="16" />
                //<img src="css/images/checkbox.png" width="16" height="16" />
                $checked = (isset($model->$attr1) && $model->$attr1 == 2) ? true : false;
                
                $img_src = $checked ? 'css/images/checkbox-selected.png':'css/images/checkbox.png';
                
                $cinput = str_replace('{img_src}', $img_src, $template_other);
                
                if (!$checked) {
                    $other_visible = "hidden";
                } else {
                    $other_visible = "";
                    $other_text = app\models\shared\NullValueFormatter::Format($model->$attr2);
                    $cinput = str_replace('{other_desc}', $other_text, $cinput);
                }
                $cinput = str_replace('{other_visible}', $other_visible, $cinput);
                
                /* if index % 2 == 1 create an empty cell */
                if ($index % 2 == 1) {
                    echo '<td>'.$cinput.'</td>'.'</tr>';
                } else {
                    //create a new row
                    echo '<tr><td>'.$cinput.'</td>'.'<td></td></tr>';
                    $index+=1;
                }
                
                $index+=1;
            }
            
            
            /* if index % 2 == 1 create an empty cell */
            if ($index % 2 == 1) {
                echo '<td></td></tr>';
            }
            
        }
        ?>
        
    </tbody>
</table>