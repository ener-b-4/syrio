<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\assets\AppAssetPdf;
use app\models\shared\NullValueFormatter;
use app\models\shared\TextFormatter;

/*
 * SyRIO beta
 */

/* @var $this yii\web\View */
/* @var $model app\models\crf\Section1 */

//Registrtion different ASSETS for PDF
AppAssetPdf::register($this);

?>

<!--
SECTION A.1.VI
-->
<div class="row" id="content-a1_vi">
    
    <div class='col-lg-12 sy_margin_bottom_20'>
        <div class='row form-inline crf-h2-box'>
            <div class="col-lg-12 crf-h2 sy_margin_bottom_10">
                <span class='title-number'>VI.</span> 
                <span class='title-text'><strong><?= $model->attributeLabels()['a1vi_code'] ?></strong></span> 
                <em class='text-muted'><?= ' ' . \Yii::t('app/crf', '(i.e. zone at location of incident)'); ?></em>
            </div>
            
            <div class="col-lg-12 crf-h2">
                <div class='col-lg-12 section-content'>
                    <div class='row'>
                        <div class="col-lg-6">
                            <div class="row form-inline">                            
                                <div class="col-lg-12">
                                    <ul class="list-inline sy_margin_bottom_0 clear-margin-left">
                   
                                        <?php
                                            if ($model->a1vi_code == 0) { $model->a1vi_code = NULL; }
                                            if (!isset($model->a1vi_code)) {
                                                echo '<li class="sy_vert_top"><div class="text-info">' . NullValueFormatter::Format($model->a1vi_code) . '</div></li>';
                                            }
                                        ?>
                                    </ul>
                                    <div class='text-muted sy_margin_bottom_20'>
                                    </div>
                                </div> 
                            </div>
                        </div>
                    </div>
                    
                    <?php if (isset($model->a1vi_code)) { ?>
                    
                    <div class='row'>
                        <div class="col-lg-6">
                            <div class="row form-inline">                            
                                <div class="col-lg-12">
                                        <div class=""> 
                                            <table autosize="1.2" class="pdf_table pdf_table_no_border">
                                              <tbody>
                                                <tr>
                                                  <td style="vertical-align: central"><em>(<?= \Yii::t('app/crf', 'Tick appropriate box') ?>)</em></td>
                                                  <td>
                                                    <?php 
                                                      // a1i_o
                                                      if ($model->a1vi_code == 1)  { echo '1<img src="css/images/checkbox-selected.png" width="16" height="16" style="margin:10 10 10 10;" />'; }; 
                                                      if ($model->a1vi_code !== 1) { echo '1<img src="css/images/checkbox.png" width="16" height="16" style="margin:10 10 10 10;" />';          };   
                                                    ?>
                                                  </td>
                                                  <td>
                                                    <?php 
                                                      // a1i_co
                                                      if ($model->a1vi_code == 2)  { echo '2<img src="css/images/checkbox-selected.png" width="16" height="16" style="margin:10 10 10 10;" />'; }; 
                                                      if ($model->a1vi_code !== 2) { echo '2<img src="css/images/checkbox.png" width="16" height="16" style="margin:10 10 10 10;" />';          };   
                                                    ?>
                                                  </td>
                                                  <td>
                                                    <?php 
                                                      // a1i_g
                                                      if ($model->a1vi_code == 3)  { echo \Yii::t('app/crf', 'Unclassified').'<img src="css/images/checkbox-selected.png" width="16" height="16" style="margin:10 10 10 10;" />'; }; 
                                                      if ($model->a1vi_code !== 3) { echo \Yii::t('app/crf', 'Unclassified').'<img src="css/images/checkbox.png" width="16" height="16" style="margin:10 10 10 10;" />';          };   
                                                    ?>
                                                  </td>
                                                </tr>
                                              </tbody>
                                            </table>
                                        </div>    <!--class="table-responsive"-->
                                    
                                    <div class='text-muted sy_margin_bottom_20'>
                                    </div>
                                </div> 
                            </div>
                        </div>
                    </div>
                    
                    <?php } ?>
                    
                    <!--GENERATED BY PDF-->
                    <?php if (json_encode($is_pdf) == 1) { ?> <br> <?php } ?>
                </div>
            </div>
         </div>
    </div>


</div>

