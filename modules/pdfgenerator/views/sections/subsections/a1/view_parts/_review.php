<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use kartik\form\ActiveForm;
use app\assets\AppAssetPdf;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\crf\Section1 */
/* @var $form yii\widgets\ActiveForm */

//Compose the request si/no
    //Registrtion different ASSETS for PDF
    AppAssetPdf::register($this);

    $isSectionA1 = $model->a1;

    //pdf
    $checked_value = $model->a1;
    $checked_text_first = \Yii::t('app', 'Yes');
    $checked_text_second = \Yii::t('app', 'No');
    $checkbox_checked = '<img src="css/images/checkbox-selected.png" width="16" height="16" style="margin:10 10 10 10;" />';
    $checkbox_unchecked = '<img src="css/images/checkbox.png" width="16" height="16" style="margin:10 10 10 10;" />';

    $s='';
    if (!isset($checked_value)) {
        //both unchecked
        $s = $checked_text_first . $checkbox_unchecked . ' '. $checked_text_second . $checkbox_unchecked;
    } elseif ($checked_value != 0) {
        //first (Yes) checked
        //second (No) unchecked
        $s = $checked_text_first . $checkbox_checked . ' '. $checked_text_second . $checkbox_unchecked;
    } else {
        //first (Yes) unchecked
        //second (No) checked
        $s = $checked_text_first . $checkbox_unchecked . ' '. $checked_text_second . $checkbox_checked;
    }
?>

<div class="sectionA-1 sy_pad_bottom_18">
    
    <div class="row">

        <div class='col-lg-10 crf-h1'>
            <span class='title-number'>
                <strong><?= \Yii::t('app', 'A').'.1.' ?></strong>
            </span>
            <span class='title-text'>
                <strong>
                    <?= \Yii::t('app/crf', 'Was there a release of hydrocarbon substances?') ?>
                </strong>
            </span>
            <?php echo $s ?>
            <div class='section-content'>
                <strong><?= \Yii::t('app/crf', 'If yes, fill in the following sections') ?></strong>
            </div>
        </div>
        
        <div class="col-lg-12 hiddenBlock crf-h1" id="a1_content">
            <div class='section-content'>
                <?php
                    foreach(range(1,15) as $index) {
                        //$view = 'view_parts/a_1_'.$index;
                        $view = '@app/modules/pdfgenerator/views/sections/subsections/a1/view_parts/a_1_'.$index;
                        echo $this->render($view, 
                            [
                                'model'=>$model, 
                                'is_pdf' => 1
                            ]);
                    }
                ?>
            </div>
        </div>
        
    </div>  <!-- end first row block -->
    
</div>
