<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\assets\AppAssetPdf;
use app\models\shared\NullValueFormatter;
use app\models\shared\TextFormatter;

/*
 * SyRIO beta
 */

/* @var $this yii\web\View */
/* @var $model app\models\crf\Section1 */

//Registrtion different ASSETS for PDF
AppAssetPdf::register($this);

?>

<!--
SECTION A.1.VII
-->
<div class="row" id="content-a1_vii">
    <div class='col-lg-12 sy_margin_bottom_20'>
        <div class='row form-inline crf-h2-box'>
            <div class="col-lg-12 crf-h2 sy_margin_bottom_10">
                <span class='title-number'>VII.</span> 
                <span class='title-text'><strong><?= $model->attributeLabels()['a1vii_code'] ?></strong></span> 
            </div>
            
            <div class="col-lg-12 crf-h2">
                <div class='col-lg-12 section-content'>
                    <div class='row'>
                        <div class="col-lg-6">
                            <div class="row form-inline">                            
                                <div class="col-lg-12">
                                    <ul class="list-inline sy_margin_bottom_0 clear-margin-left">
                   
                                        <!-- GENREATED BY PDF -->
                                        <li>
                                            <div class="table-responsive"> 
                                                <table autosize="1.6" class="pdf_table_no_border pdf_table_inline">
                                                    <tbody>
                                                        <tr>
                                                            <td><?= \Yii::t('app/crf', 'Natural'); ?></td>
                                                            <td>
                                                                <?php 
                                                                // Natural
                                                                if ($model->a1vii_code == 300) { echo '<img src="css/images/checkbox-selected.png" width="16" height="16" />'; }
                                                                else { echo '<img src="css/images/checkbox.png" width="16" height="16" />';          }
                                                              ?>
                                                            </td>
                                                            <td><?= \Yii::t('app/crf', 'Forced'); ?></td>
                                                            <td>
                                                                <?php 
                                                                  // Forced
                                                                  if ($model->a1vii_code == 301) { echo '<img src="css/images/checkbox-selected.png" width="16" height="16" />'; }
                                                                  else { echo '<img src="css/images/checkbox.png" width="16" height="16" />';          }
                                                                ?>
                                                            </td>
                                                            <td><?= \Yii::t('app/crf', 'Not applicable'); ?></td>
                                                            <td>
                                                                <?php 
                                                                  // Not applicable
                                                                  if ($model->a1vii_code == 2) { echo '<img src="css/images/checkbox-selected.png" width="16" height="16" />'; }
                                                                  else { echo '<img src="css/images/checkbox.png" width="16" height="16" />';          }    
                                                                ?>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </li>
                                    </ul>
                                    <div class='text-muted sy_margin_bottom_20'>
                                    </div>
                                </div> 
                            </div>
                        </div>
                    </div>
                    
                    <div class='row'>
                        <div class="col-lg-6">
                            <div class="row form-inline">                            
                                <div class="col-lg-12">
                                    <span><?= $model->attributeLabels()['a1viia'] ?></span> 
                                    
                                    <span class='text-info'> <?= NullValueFormatter::Format($model->a1viia) ?> </span>
                                    
                                    <div class='text-muted sy_margin_bottom_20'>
                                        <em>
                                            <?= '(' . \Yii::t('app/crf', 'Insert the number of walls, including floor and ceiling') .')' ?>
                                        </em>
                                    </div>
                                    
                                </div> 
                            </div>
                        </div>
                    </div>
                    
                    <br/>
            
                    <div class='row'>
                        <div class="col-lg-6">
                            <div class="row form-inline">                            
                                <div class="col-lg-12">
                                    <span><?= $model->attributeLabels()['a1viib'] ?></span> 
                                    
                                    <span class='text-info'> <?= NullValueFormatter::Format($model->a1viib) ?> </span>
                                    
                                    <span><em>(m<sup>3</sup>)</em></span>
                                    <div class='text-muted sy_margin_bottom_20'>
                                    </div>
                                </div> 
                            </div>
                        </div>
                    </div>
            
                    <br/>
                    
                    <div class='row'>
                        <div class="col-lg-6">
                            <div class="row form-inline">                            
                                <div class="col-lg-12">
                                    <span><?= $model->attributeLabels()['a1viic'] ?></span> 
                                    
                                    <span class='text-info'> <?= NullValueFormatter::Format($model->a1viic) ?> </span>
                                    
                                    <div class='text-muted sy_margin_bottom_20'></div>
                                </div> 
                            </div>
                        </div>
                    </div>
                    
                    <br/>
                    
                    <div class='row'>
                        <div class="col-lg-6">
                            <div class="row form-inline">                            
                                <div class="col-lg-12">
                                    <span><?= $model->attributeLabels()['a1viid'] ?></span> 
                                    
                                    <span class='text-info'> <?= NullValueFormatter::Format($model->a1viid) ?> </span>
                                </div> 
                            </div>
                        </div>
                    </div>
                    
                    <br/>
                    
                </div>
            </div>
         </div>
    </div>
</div>       
