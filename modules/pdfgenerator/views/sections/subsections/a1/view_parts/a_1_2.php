<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\assets\AppAssetPdf;
use app\models\shared\TextFormatter;    
use app\models\shared\NullValueFormatter;

/*
 * SyRIO beta
 */

/* @var $this yii\web\View */
/* @var $model app\models\crf\Section1 */


$massUnits = app\models\units\Units::getUnitsArray(app\models\units\Units::UNIT_MASS);

    //Registrtion different ASSETS for PDF
    AppAssetPdf::register($this);
?>

<!--
SECTION A.1.II
-->
<div class="row" id="content-a1_ii">
    <div class='col-lg-12 sy_margin_bottom_20'>
        <div class='row form-inline crf-h2-box'>
            <div class="col-lg-12 crf-h2 sy_margin_bottom_10">
                <span class='title-number'>II.</span> 
                <span class='title-text'><strong><?= $model->attributeLabels()['a1ii_q'] ?></strong></span> 
                    <span class="sy_descriptive_text text-info">
                        <?= NullValueFormatter::Format(TextFormatter::TextToHtml($model->a1ii_q)) ?>
                    </span>
                    <span >
                        <?= NullValueFormatter::FormatDropdown($model->a1ii_u, $massUnits) ?>
                    </span>
            
                <div class='col-lg-12 section-content'>
                    <div class='row'>

                        <div class="col-lg-12 ">
                            <div class="text-muted">
                                <em>
                                    <?= \Yii::t('app/crf', 'Specify units') . ' ('
                                        . 'e.g. '                                
                                        . \Yii::t('app/crf', 'tonnes, kg, Nm3') . ')' ?>
                                </em>
                            </div>

                        </div>
                    </div>
                </div>

            </div>  <!-- end of h2 -->
         </div>
    </div>

</div>

