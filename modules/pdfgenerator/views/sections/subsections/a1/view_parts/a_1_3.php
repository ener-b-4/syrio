<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\assets\AppAssetPdf;
use app\models\shared\TextFormatter;    
use app\models\shared\NullValueFormatter;

/* @var $this yii\web\View */
/* @var $model app\models\crf\Section1 */

$massRateUnits = app\models\units\Units::getUnitsArray(app\models\units\Units::UNIT_MASS_RATE);

//Registrtion different ASSETS for PDF
AppAssetPdf::register($this);
?>

<!--
SECTION A.1.III
-->

<div class="row" id="content-a1_iii">
    <div class='col-lg-12 sy_margin_bottom_20'>
        <div class='row form-inline crf-h2-box'>
            <div class="col-lg-12 crf-h2 sy_margin_bottom_10">
                <span class='title-number'>III.</span> 
                <span class='title-text'><strong><?= $model->attributeLabels()['a1iii_q'] ?></strong></span> 

                    <span class="sy_descriptive_text text-info">
                        <?= NullValueFormatter::Format(TextFormatter::TextToHtml($model->a1iii_q)) ?>
                    </span>
                    <span >
                        <?= NullValueFormatter::FormatDropdown($model->a1iii_u, $massRateUnits) ?>
                    </span>

                <div class='col-lg-12 section-content'>
                    <div class='row'>

                        <div class="col-lg-12 ">
                            <div class="text-muted">
                                <em>
                                    <?= \Yii::t('app/crf', 'Specify units') . ' ('
                                        . 'e.g. '                                
                                        . \Yii::t('app/crf', 'tonnes/day, kg/s, Nm3/s') . ')' ?>
                                </em>
                            </div>

                        </div>
                    </div>
                </div>  <!-- end section content (1 of 2) -->
                
            </div>  <!-- end h2 -->
        </div>      <!-- end h2-box -->
    </div>          <!-- end col-lg-12 -->
</div>              <!-- end row -->


