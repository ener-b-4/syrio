<?php

/* @var $this yii\web\View */
/* @var $model app\models\crf\Section1 */
/* @var $form yii\widgets\ActiveForm */

use yii\helpers\Html;
use app\models\shared\TextFormatter;    
use app\models\shared\NullValueFormatter;
use app\assets\AppAssetPdf;

/* @var $this yii\web\View */
/* @var $model app\models\crf\Section1 */
/* @var $form yii\widgets\ActiveForm */


//Compose the request si/no
//Registrtion different ASSETS for PDF
AppAssetPdf::register($this);
?>


<div class="row" id="content-a1_i">
    <div class='col-lg-12 sy_margin_bottom_20'>
        <div class='row form-inline crf-h2-box'>
            <div class="col-lg-12 crf-h2 sy_margin_bottom_10">
                <span class='title-number'>I.</span> 
                <span class='title-text'><strong><?= \Yii::t('app/crf', 'Hydrocarbon (HC) released') ?></strong></span> 
                <span class="small em"><?= \Yii::t('app/crf', '(Tick appropriate box)') ?></span>

                <div class='col-lg-12 section-content'>
                    <?= ''?>
                    
                    <!-- NON-PROCESS -->
                    <div class='row form-inline'>
                        <div class="col-lg-12">
                            <ul class="list-inline sy_margin_bottom_0 clear-margin-left">
                                <!--GENERATED BY VIEW-->
                                <li class='sy_vert_top'>
                                    <?= \Yii::t('app/crf', 'NON-PROCESS') ?> 
                                    <?php 
                                        $is_a1i_np = $model->a1i_np;
                                        $l_a1i_np = '';
                                        if ($is_a1i_np == true)  { 
                                            $l_a1i_np = $l_a1i_np . '<img src="css/images/checkbox-selected.png" width="16" height="16" style="margin:10 10 10 10;" />'; 
                                        } else {
                                            $l_a1i_np = $l_a1i_np . '<img src="css/images/checkbox.png" width="16" height="16" style="margin:10 10 10 10;" />'; 
                                        }; 
                                        echo $l_a1i_np;
                                    ?> 
                                    <span><em>(<?= \Yii::t('app/crf', 'please specify') ?>)</em></span>
                                    <span class="sy_descriptive_text text-info">
                                        <?= NullValueFormatter::Format(TextFormatter::TextToHtml($model->a1i_np_specify)) ?>
                                    </span>
                                </li>
                            </ul>
                        </div>
                    </div>
                    
                    <!-- PROCESS -->
                    <div class='row form-inline'>
                        <div class="col-lg-12">
                                    <table class="pdf_table_no_border pdf_table_inline">
                                      <tbody>
                                        <tr>
                                          <td><?= \Yii::t('app/crf', 'PROCESS'); ?>:</td>
                                          <td>
                                              <span>
                                            <?= \Yii::t('app/crf', 'Oil'); ?>
                                              </span>
                                              <span>
                                            <?php 
                                              // a1i_o
                                              if ($model->a1i_o == true)  {
                                                  echo '<img src="css/images/checkbox-selected.png" width="16" height="16" />'; 
                                              } else {
                                                  echo '<img src="css/images/checkbox.png" width="16" height="16" />';
                                              }
                                            ?>
                                              </span>
                                          </td>
                                          <td>
                                              <span>
                                            <?= \Yii::t('app/crf', 'Gas'); ?>
                                              </span>
                                              <span>
                                            <?php 
                                              // a1i_g
                                              if ($model->a1i_g == true)  { 
                                                  echo '<img src="css/images/checkbox-selected.png" width="16" height="16"  />'; 
                                              } else {
                                                  echo '<img src="css/images/checkbox.png" width="16" height="16"  />';
                                              }   
                                            ?>
                                              </span>
                                          </td>
                                          <td>
                                              <span>
                                                   <?= \Yii::t('app/crf', 'Condensate') ?>
                                              </span>
                                              <span>
                                            <?php
                                              // a1i_o
                                              if ($model->a1i_o == true)  {
                                                  echo '<img src="css/images/checkbox-selected.png" width="16" height="16" />';
                                              } else {
                                                  echo '<img src="css/images/checkbox.png" width="16" height="16" />';          
                                              }
                                            ?>
                                              </span>
                                          </td>

                                          <td>
                                              <span>
                                            <?= \Yii::t('app/crf', '2-Phase'); ?>
                                              </span>
                                              <span>
                                            <?php 
                                              // a1i_g
                                              if ($model->a1i_2p == true)  {
                                                  echo '<img src="css/images/checkbox-selected.png" width="16" height="16"  />'; 

                                              } else {
                                                  echo '<img src="css/images/checkbox.png" width="16" height="16"  />';          
                                              }
                                            ?>
                                              </span>
                                          </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <span><?= \Yii::t('app/crf', 'Level of H') ?><sub>2</sub>S:</span>
                                                <span class="text-info"><?= app\models\shared\NullValueFormatter::Format($model->a1i_2p_h2s_q) ?></span>
                                                <span><em>(<?= \Yii::t('app/crf', 'estimated ppm') ?>)</em></span>
                                            </td>
                                        </tr>
                                      </tbody>
                                    </table>
                        </div> 
                    </div>
                </div>
            </div>
         </div>
        
    </div>
    
</div>  <!-- end row div content-a1_1 -->
