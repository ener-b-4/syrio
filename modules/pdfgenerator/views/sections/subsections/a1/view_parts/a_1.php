<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\form\ActiveForm;

include_once Yii::getAlias('@app') . '/views/events/drafts/sections/subsections/a1/load_units.php';

/* @var $this yii\web\View */
/* @var $model app\models\crf\Section1 */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="section1-form">
    
    <?php $form = ActiveForm::begin(); ?>

    <div class='row'>
        <div class="col-lg-10">
            <h4><?= Yii::t('app', 'A').'.1.'?> <?= \Yii::t('app/crf', 'Was there a release of hydrocarbon substances?') ?></h4>
        </div>
        <div class="col-lg-2">
            <?= $form->field($model, 'a1')
                ->radioList(
                    ['1' => 'Yes', '0' =>'No'],
                    [
                      'unselect' => null,
                      'item' => function($index, $label, $name, $checked, $value) {
                            $showPanel = $index == 0 ? "true" : "false";
                            $ischecked = $checked ? 'checked="checked"' : ''; // $value == '1' ? "checked " : '';
                            $setting = 'class="sy_data_toggle" '
                                    . 'sy_data_toggle_dest = "a1_content"'
                                    . 'sy_data_toggle_src_type = "radio"'
                                    . 'sy_data_toggle_visible_value = "' . $showPanel . '"' 
                                    . 'sy_data_toggle_enabled = "true"';
                    
                            $return = '<label>'
                                    . ' <input type="radio" disabled name="' . $name . '" value="'. $value .'" ' 
                                    . $ischecked 
                                    . ' '
                                    . $setting
                                    . '>'
                                    . ' ' . $label
                                    . '</input>'
                                    . '</label>';
                            return $return;
                      }
                    ]    
                )->label(false) ?>
            
        </div>
    </div>
    
    <div class="row hiddenBlock" id="a1_content">
        <div class="col-lg-12">
            <div class="text-muted">
                <?= \Yii::t('app/crf', 'If yes, fill in the following sections') ?>
            </div>
            
            <?php
            
            foreach(range(1,13) as $index) {
                $view = 'view_parts/blocks/a1_'.$index;
                echo $this->render($view, ['model'=>$model, 'form'=>$form]);
            }
            
            ?>
        </div>
    </div>
    
    <?php ActiveForm::end(); ?>

</div>
