<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

/**
 * Performs a validation and rendering logic for section a_1_x
 * Render block in $a1xii_block
 *
 * @var $model \app\models\events\drafts\sections\subsections\A1
 * 
 * @author bogdanV
 */

/* @var $model app\models\crf\Section1 */

use app\models\crf\constants\LeakCause;
use app\models\constants\Constants;
use yii\helpers\Html;
use app\models\shared\NullValueFormatter;
use app\models\shared\TextFormatter;
use yii\helpers\ArrayHelper;
use app\assets\AppAssetPdf;
use app\components\helpers\TArrayHelper;

//Registrtion different ASSETS for PDF
AppAssetPdf::register($this);

$leak_causes = app\models\crf\LeakCauseCategories::find()->where(['not', ['id'=>0]])->all();

$failureEquipment = ArrayHelper::map(\app\models\crf\LeakCause::find()->where(['category_id'=>2])->asArray()->all(), 'num_code', 'cause');
$failureDesign = ArrayHelper::map(\app\models\crf\LeakCause::find()->where(['category_id'=>1])->asArray()->all(), 'num_code', 'cause');
$failureOperation = ArrayHelper::map(\app\models\crf\LeakCause::find()->where(['category_id'=>3])->asArray()->all(), 'num_code', 'cause');
$failureProcedural = ArrayHelper::map(\app\models\crf\LeakCause::find()->where(['category_id'=>4])->asArray()->all(), 'num_code', 'cause');

$operationalModes = ArrayHelper::map(Constants::getOperationalModes(), 'num_code', 'name');

$section_leaks_q = $model->releaseCauses;

$items = [
    'design' => $failureDesign,
    'equipment' => $failureEquipment,
    'operation' => $failureOperation,
    'procedural' => $failureProcedural,
];

$checkbox_checked   = '<img src="css/images/checkbox-selected.png" width="16" height="16" />';
$checkbox_unchecked = '<img src="css/images/checkbox.png" width="16" height="16" />';

?>

<!--
SECTION A.1.XI
-->
<div class="row sy_margin_bottom_26" id="content-a1_xi">
    
    <div class='col-lg-12 sy_margin_bottom_20'>
        <div class='row form-inline crf-h2-box'>
            <div class="col-lg-12 crf-h2 sy_margin_bottom_10">
                <span class='title-number'>XI.</span> 
                <span class='title-text'><strong><?= $model->attributeLabels()['a1xi'] ?></strong></span>
                <span class='text-muted'>
                    <em>(
                        <?= \Yii::t('app/crf', 'Please give a short description and complete the \'{evt}\' checklist below', 
                                [
                                    'evt'=>Html::a(
                                        \Yii::t('app/crf', 'Cause'), '#causes'
                                        )
                                ]) 
                        ?>
                        )
                    </em>
                </span>
            </div>
            
            <div class="col-lg-12 crf-h2">
                <div class='col-lg-12 section-content'>
                    <div class='section-content'>
                        <div class='row'>
                            <div class="col-lg-10">
                                <div class='sy_margin_bottom_10 <?php echo json_decode($is_pdf) == 1 ? 'text-info' : '' ?> '>
                                    <?= NullValueFormatter::Format(
                                            TextFormatter::TextToHtml($model->axi_description)) ?>
                                </div>

                                    
                                <!-- http://mpdf1.com/manual/index.php?tid=108 -->
                                <p style="page-break-after:always"></p>
                                <div id = 'causes'>
                                    <div>
                                        <p>
                                            <strong><?= strtoupper(\Yii::t('app/crf', 'cause of leak checklist')) ?></strong>
                                            <br/>
                                            <em class='text-muted'>
                                                <?= \Yii::t('app/crf', '(Please indicate those items which come nearest to pinpointing the cause of the leak)') ?>
                                            </em>
                                        </p>

                                    </div>

                                    <div class='sy_pad_bottom_18'> <!-- Indicate causes of the release div -->
                                        <strong><?= \Yii::t('app/crf', 'Indicate the cause(s) of the release') ?></strong>
                                        <br/>
                                        <span class='text-muted'>
                                            <span style='text-decoration: underline'><?= \Yii::t('app/crf', 'From each of the following categories')?></span>
                                            <?= ' ' . \Yii::t('app/crf', 'tick the appropriate boxes') ?>
                                        </span>
                                                
                                        <!-- GENERATED BY PDF -->
                                        <!-- TABLE -->
                                        <?php
                                        $all_causes = \app\models\crf\LeakCause::find()
                                                ->where(['<>', 'category_id', 0])
                                                ->asArray()
                                                ->all();
                                        $all_causes =  TArrayHelper::tIndex($all_causes, 'category_id');

                                        $sel_causes = TArrayHelper::getColumn($model->releaseCauses, 'num_code');
                                                //->asArray()
                                                //->all();
                                        ?>
                                        <?= $this->render('_release_causes_table', [
                                            'model'=>$model,
                                            'all_causes'=>$all_causes, 
                                            'sel_causes'=>$sel_causes,
                                            'other_attr'=>[
                                                "2"=>"a1_fail_equipment",
                                                "3"=>"a1_fail_operation",
                                                "4"=>"a1_fail_procedural"
                                            ]]) ?>
                                        <br />
                                        <!-- TABLE -->
                                        
                                        
                                    </div> <!-- end div Indicate causes of the release -->

                                    <div> <!-- div Operational mode -->
                                        <p>
                                            <strong><?= \Yii::t('app/crf', 'Indicate the operational mode in the area at the time of release') ?></strong>
                                            <br/>
                                            <span class='text-muted'>
                                                <?= \Yii::t('app/crf', 'Choose one parameter from the following categories, and tick the appropriate boxes') ?>
                                            </span>
                                        </p>
                                        
                                        <!-- GENERATED BY PDF -->
                                        <!-- TABLE -->
                                        <div class=""> 
                                            <?php 
                                            $op_modes = ArrayHelper::map(Constants::getOperationalModes(), 
                                                    'num_code', 'name');
                                            
                                            ?>
                                          <table class="op_mode_table">
                                              <thead>
                                                  <tr>
                                                      <th>
                                                          <strong><?= \Yii::t('app/crf', 'Operation mode in the area at the time of release'); ?></strong>:    
                                                      </th>
                                                  </tr>
                                              </thead>
                                              <tbody>
                                                  <?php
                                                  foreach ($op_modes as $key=>$value) {
                                                  ?>
                                                  <tr>
                                                      <td>
                                                          <?php
                                                          echo $model->a1_op == $key ? $checkbox_checked : $checkbox_unchecked;
                                                          echo ' ' . \Yii::t('app/crf',$value);
                                                          
                                                          //601 - well operations, requires other
                                                          if ($key==601 && $model->a1_op==601) {
                                                              ?>
                                                              <i> <?= \Yii::t('app/crf', '(Specify actual operation, e.g. wire line, well test, etc.)'); ?>:</i><br />                                                          
                                                              <span class="sy_descriptive_text" style="margin-left: 16px;">
                                                              <?php echo NullValueFormatter::Format($model->a1_op_specify) ?>
                                                              </span>
                                                          
                                                          <?php
                                                          }
                                                          ?>
                                                      </td>
                                                  </tr>
                                                  <?php
                                                  }
                                                  ?>
                                              </tbody>
                                          </table>                                            
                                        </div>
                                        <br />
                                        <!-- TABLE -->
                                        
                                    </div> <!-- end div Operational mode -->
                                </div>
                                
                                <!-- http://mpdf1.com/manual/index.php?tid=108 -->
                                <p style="page-break-after:always"></p>
                                
                            </div>
                        </div>
                    </div>
                </div> 
            </div>
            
        </div>
    </div>
</div>    
