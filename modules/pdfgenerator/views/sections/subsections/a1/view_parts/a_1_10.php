<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use app\assets\AppAssetPdf;
use app\models\shared\NullValueFormatter;
use app\models\shared\TextFormatter;

/*
 * SyRIO beta
 */

/* @var $this yii\web\View */
/* @var $model app\models\crf\Section1 */
/* @var $form yii\widgets\ActiveForm */

//Registrtion different ASSETS for PDF
AppAssetPdf::register($this);

?>

            <!--
            SECTION A.1.X
            -->
            
            <div class="row" id="content-a1_x">
                <div class="col-lg-12">
                    <div class='content'>
                        <div class="row form-inline crf-h2-box">
                            <div class="col-lg-12 crf-h2">
                                <span class='title-number'>X.</span> 
                                <span class='title-text'><strong><?= $model->attributeLabels()['a1x'] ?></strong></span> 
                                <span class='text-muted'>
                                    <em><?= '(' . \Yii::t('app', 'Please tick type of detector ot specify as appropriate') . ')' ?></em>
                                </span> 
                                
                                <div class="col-lg-12 section-content">
                                    
                                    <!-- GENREATED BY PDF -->
                                        <ul class="list-inline">
                                            <li>
                                              <?php 
                                                // Fire
                                                if ($model->a1x_a == true) { echo '<img src="css/images/checkbox-selected.png" width="16" height="16" />'; }; 
                                                if ($model->a1x_a == false) { echo '<img src="css/images/checkbox.png" width="16" height="16" />';          };   
                                              ?>
                                              <strong><?= \Yii::t('app/crf', 'Fire'); ?></strong>
                                            </li>
                                            <li>
                                              <?php 
                                                // Gas
                                                if ($model->a1x_b == true) { echo '<img src="css/images/checkbox-selected.png" width="16" height="16" />'; }; 
                                                if ($model->a1x_b == false) { echo '<img src="css/images/checkbox.png" width="16" height="16" />';          };    
                                              ?>
                                              <strong><?= \Yii::t('app/crf', 'Gas'); ?></strong>
                                            </li>
                                            <li>
                                              <?php 
                                                // Smoke
                                                if ($model->a1x_c == true) { echo '<img src="css/images/checkbox-selected.png" width="16" height="16" />'; }; 
                                                if ($model->a1x_c == false) { echo '<img src="css/images/checkbox.png" width="16" height="16" />';          };    
                                              ?>
                                              <strong><?= \Yii::t('app/crf', 'Smoke'); ?></strong>
                                            </li>
                                            <li>
                                              <?php 
                                                // Other
                                                if ($model->a1x_d == true) { echo '<img src="css/images/checkbox-selected.png" width="16" height="16" />'; }; 
                                                if ($model->a1x_d == false) { echo '<img src="css/images/checkbox.png" width="16" height="16" />';          };    
                                              ?>
                                              <strong><?= \Yii::t('app/crf', 'Other'); ?></strong>
                                              <i><?= \Yii::t('app/crf', '(Please specify)'); ?></i>
                                              <span class='text-info'>
                                              <?php print $model->a1x_d_specify; ?>   
                                              </span>
                                            </li>
                                        </ul>
                                </div>
                            </div>
                        </div>                        
                    </div>
                </div>
                
            </div>                
            

