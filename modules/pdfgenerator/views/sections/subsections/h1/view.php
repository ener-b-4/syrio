<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

    use yii\helpers\Html;
    use app\assets\AppAssetPdf;

    /* @var $this yii\web\View */
    /* @var $model app\models\events\drafts\sections\subsections\I4 */
    
    //Registrtion different ASSETS for PDF
    AppAssetPdf::register($this);
?>

<div class="sectionH-1 sy_pad_bottom_18">
    <div class="row">
        <div class='col-lg-12 sy_pad_bottom_18'>
            <span>
                <?= Yii::t('app/crf', 'Section H shall be reported under the requirements of Directive 92/91/EEC') . '.' ?>
            </span>
        </div>
        
        <div class='col-lg-12 text-muted'>
            <?= Yii::t('app', 'Page intentionally left blank') ?>
        </div>
    </div>
</div>

