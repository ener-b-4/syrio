<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

    use yii\helpers\Html;

    use app\models\crf\CrfHelper;
    use app\models\shared\TextFormatter;
    use app\models\shared\NullValueFormatter;
    use app\assets\AppAssetPdf;

    /* @var $this yii\web\View */
    /* @var $model app\models\events\drafts\sections\subsections\I2 */
 
    //Registrtion different ASSETS for PDF
    AppAssetPdf::register($this);
?>


<div class="sectionI-2 sy_pad_bottom_18">
    <div class="row">
        <div class="crf-h1 col-lg-12">
            <div class="">
                <span class='title-number crf-h1-title-number'>
                    <strong><?= \Yii::t('app', 'I').'.2.'; ?> </strong>
                </span>
                <span class='title-text crf-h1-title-text'>
                    <strong>
                        <?= CrfHelper::SectionsTitle()['I.2'] ?>
                    </strong>
                </span>
            </div>
            <div class="crf-h2">
                <div class="row">
                    <br/>
                    <div class="col-lg-12 sy_margin_bottom_10">
                        <strong><?= $model->attributeLabels()['i2_1'] ?> </strong>
                    </div>
                    <div class="col-lg-12 sy_margin_bottom_10">
                                <div class=""> 
                                    <table class="pdf_table_no_border">
                                      <tbody>
                                        <tr>
                                          <td>
                                            <?php 
                                              // Natural
                                              if ($model->i2_1 == 1) 
                                                  { echo '<img src="css/images/checkbox-selected.png" width="16" height="16" />'; }
                                              else
                                                  { echo '<img src="css/images/checkbox.png" width="16" height="16" />';};   
                                            ?>
                                            <?= \Yii::t('app/crf', 'Precautionary'); ?>
                                          </td>
                                          <td>
                                            <?php 
                                              // Forced
                                              if ($model->i2_1 == 2) 
                                                  { echo '<img src="css/images/checkbox-selected.png" width="16" height="16" />'; }
                                              else
                                                  { echo '<img src="css/images/checkbox.png" width="16" height="16" />';          };    
                                            ?>
                                            <?= \Yii::t('app/crf', 'Emergency'); ?>
                                          </td>
                                            <td>
                                              <?php 
                                                // Not applicable
                                                if ($model->i2_1 == 3) 
                                                    { echo '<img src="css/images/checkbox-selected.png" width="16" height="16" />'; }
                                                else
                                                    { echo '<img src="css/images/checkbox.png" width="16" height="16" />';          };    
                                              ?>
                                            <?= \Yii::t('app/crf', 'Both'); ?>
                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                </div>    <!-- end measures div-->
                    </div>
                
                    <div class="col-lg-12 sy_margin_bottom_10">
                        <span class="crf-item-label"><strong><?= $model->attributeLabels()['i2_2'] . ': ' ?></strong></span>

                        <span class="crf-item-label sy_descriptive_text">
                            <?= NullValueFormatter::Format(
                                TextFormatter::TextToHtml($model->i2_2)) ?>
                        </span>
                    </div>
                    
                    <div class="col-lg-12 sy_margin_bottom_10">
                        <span class="crf-item-label"><strong><?= $model->attributeLabels()['i2_3'] . ': ' ?></strong></span>

                        <span class="crf-item-label sy_descriptive_text">
                            <?= NullValueFormatter::Format(
                                TextFormatter::TextToHtml($model->i2_3)) ?>
                        </span>

                        <span class='text-muted'>
                            <?= \Yii::t('app/crf', '(e.g. helicopter)') ?>
                        </span>
                    </div>                    
                    
                    <div class="col-lg-12 sy_margin_bottom_10">
                        <div class="sy_margin_bottom_10">
                            <br/>
                            <?php
                                $s1 = 'Indicate the system that failed and provide a description of the circumstances of the event / describe what has happened ';
                                $s2 = ', ' . 'unless already reported in a previous section of this report';
                                echo Yii::t('app/crf', $s1) . ' ' . Yii::t('app/crf', $s2) . '.';
                            ?>
                        </div>
                        <div class="sy_descriptive_text sy_pad_top_6">
                            <?= NullValueFormatter::Format(
                                TextFormatter::TextToHtml($model->i2_4)) ?>
                        </div> <!-- end descriptive text -->       
                    </div>
                    
                </div>
            </div>                  <!-- end 'section-content' -->
        </div>                      <!-- end h1 -->
    </div>
</div>                              <!-- end section-I2 -->
