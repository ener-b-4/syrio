<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use app\models\crf\CrfHelper;
use app\models\shared\NullValueFormatter;
use app\models\shared\TextFormatter;
use app\assets\AppAssetPdf;

//Registrtion different ASSETS for PDF
AppAssetPdf::register($this);
?>

<div class="sectionB-3 sy_pad_bottom_18">
    <div class="row">
        <div class="crf-h1 col-lg-12">
            <div class="sy_pad_bottom_18">
                <span class='title-number crf-h1-title-number'>
                    <strong><?= \Yii::t('app', 'B').'.3.'; ?> </strong>
                </span>
                <span class='title-text crf-h1-title-text'>
                    <strong>
                        <?= CrfHelper::SectionsTitle()['B.3'] ?>
                        <?= '(' . \Yii::t('app/crf', 'within 10 working days from the event') . ')' ?>
                    </strong>
                </span>
            </div>
            <div class="crf-h2 col-lg-12">
                <div class='row'>
                    <div class="col-lg-12 sy_margin_bottom_10">
                        <div class="sy_descriptive_text">
                            <?= NullValueFormatter::Format(
                                TextFormatter::TextToHtml($model->b3_1)) ?>
                        </div>
                    </div>
                </div>
            </div>            
        </div>
    </div>
</div>

