<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

    use yii\helpers\Html;
    use app\models\crf\CrfHelper;
    use app\models\shared\NullValueFormatter;
    use app\models\shared\TextFormatter;
    use app\assets\AppAssetPdf;

    /* @var $this yii\web\View */
    /* @var $model app\models\events\drafts\sections\subsections\D4 */
        
    //Registrtion different ASSETS for PDF
    AppAssetPdf::register($this);

?>

<div class="sectionD-4 sy_pad_bottom_18">
    <div class="row">
        <div class="crf-h1 col-lg-12">
            <div class='sy_pad_bottom_18'>
                <span class='title-number crf-h1-title-number'>
                    <strong><?= \Yii::t('app', 'D').'.4.'; ?> </strong>
                </span>
                <span class='title-text crf-h1-title-text'>
                    <strong>
                        <?= CrfHelper::SectionsTitle()['D.4'] ?>
                        <?= '(' . \Yii::t('app/crf', 'within 10 working days from the event') . ')' ?>
                    </strong>
                </span>
            </div>
            <div class="crf-h2">
                <div class='row'>
                    <div class="col-lg-12 sy_margin_bottom_10">
                        <div class="sy_descriptive_text">
                            <?= NullValueFormatter::Format(
                                TextFormatter::TextToHtml($model->d4_1)) ?>
                        </div>
                    </div>
                </div>
            </div>            
        </div>
    </div>
</div>
