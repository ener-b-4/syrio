<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */


use app\models\shared\NullValueFormatter;
use app\models\shared\TextFormatter;
use app\assets\AppAssetPdf;

/**
 * Performs a validation and rendering logic for section a_2_0
 *
 * @var $model app\models\crf\Section1
 * 
 * @author bogdanV
 */

/* @var $model app\models\crf\Section1 */

//Registrtion different ASSETS for PDF
AppAssetPdf::register($this);
?>

<!--
SECTION A.2.0
-->
<div class="col-lg-12 sy_margin_bottom_10"> 
    <div class="sy_descriptive_text">
        <?= NullValueFormatter::Format(
                    TextFormatter::TextToHtml($model->a2_desc)) ?>
    </div>
    <br/>
</div>
