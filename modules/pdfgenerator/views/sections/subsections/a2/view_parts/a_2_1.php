<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use app\models\shared\NullValueFormatter;
use app\models\shared\TextFormatter;
use yii\helpers\Html;
use app\assets\AppAssetPdf;

use app\widgets\UnitValueViewWidget;

/**
 * @var $model app\models\crf\Section1
 * 
 * @author bogdanV
 */

/* @var $model app\models\crf\Section1 */

if (!isset($massUnits)) { $massUnits = app\models\units\Units::getUnitsArray(app\models\units\Units::UNIT_MASS); }

    //Registrtion different ASSETS for PDF
    AppAssetPdf::register($this);
if (json_encode($is_pdf) == 0) { $col_lg_number = 'col-lg-12'; };    
if (json_encode($is_pdf) == 1) { $col_lg_number = 'col-lg-10'; };    
?>

<!--
SECTION A.2.1
-->
    <div class="col-lg-12 sy_margin_bottom_26">
            <div class="row form-inline">
                <div class='col-lg-12 crf-h2p'>
                    <span class='title-number'><?= \Yii::t('app', 'A').'.2.1.' ?></span> 
                    <span class='title-text'><strong><?= $model->attributeLabels()['a2_1'] . ' ' ?></strong></span> 
                    <div class='col-lg-12 section-content'>
                            <div class='col-lg-12'>
                                <?php
                                //pdf
                                $checked_value = $model->a2_1;
                                $checked_text_first = \Yii::t('app', 'Yes');
                                $checked_text_second = \Yii::t('app', 'No');
                                $checkbox_checked = '<img src="css/images/checkbox-selected.png" width="16" height="16" style="margin:10 10 10 10;" />';
                                $checkbox_unchecked = '<img src="css/images/checkbox.png" width="16" height="16" style="margin:10 10 10 10;" />';

                                $s='';
                                if (!isset($checked_value)) {
                                    //both unchecked
                                    $s = $checked_text_first . $checkbox_unchecked . ' '. $checked_text_second . $checkbox_unchecked;
                                } elseif ($checked_value != 0) {
                                    //first (Yes) checked
                                    //second (No) unchecked
                                    $s = $checked_text_first . $checkbox_checked . ' '. $checked_text_second . $checkbox_unchecked;
                                } else {
                                    //first (Yes) unchecked
                                    //second (No) checked
                                    $s = $checked_text_first . $checkbox_unchecked . ' '. $checked_text_second . $checkbox_checked;
                                }
                                echo $s;
                                ?>
                            </div>
                            <div id="divA21" class="hiddenBlock col-lg-12">
                                <div>
                                    <?= \Yii::t('app/crf', 'If yes, please specify the type and quantity of released substance:') ?>
                                </div>
                                <div>
                                    <span>
                                        <?= ucfirst(\Yii::t('app', 'type')) . ':' ?> 
                                    </span>
                                    <span class='crf-item-label sy_pad_right_18 text-info'>
                                        <?= NullValueFormatter::format($model->a2_1_t) ?>
                                    </span>
                                    <span>
                                        <?= ucfirst(\Yii::t('app', 'quantity')) . ':' ?> 
                                    </span>
                                    <span class="text-info">
                                        <?= UnitValueViewWidget::widget([
                                            'model' => $model,
                                            'attr_root' => 'a2_1',
                                            'units' => $massUnits
                                        ]) ?>
                                    </span>
                                </div>
                            </div>
                    </div>      <!-- close content row -->
                </div>          <!-- close col-lg-12 crf-h2 -->
            </div>              <!-- close col-lg-12 crf-h2-box -->
    </div>                      <!-- close col-12-top -->

