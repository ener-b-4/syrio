<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use app\models\shared\NullValueFormatter;
use app\models\shared\TextFormatter;
use yii\helpers\Html;
use app\assets\AppAssetPdf;

/**
 * Performs a validation and rendering logic for section a_2_1
 *
 * @var $model app\models\crf\Section1
 * 
 * @author bogdanV
 */

/* @var $model app\models\crf\Section1 */

//Registrtion different ASSETS for PDF
AppAssetPdf::register($this);
?>

<!--
SECTION A.2.2
-->
    <div class="col-lg-12 sy_margin_bottom_26">
            <div class="row form-inline">
                <div class='col-lg-12 crf-h2p'>
                    <span class='title-number'><?= \Yii::t('app', 'A').'.2.2.' ?></span> 
                    <span class='title-text'><strong><?= $model->attributeLabels()['a2_2'] . ' ' ?></strong></span> 
                    <div class='col-lg-12 section-content'>
                            <div class='col-lg-12'>
                                <?php
                                //pdf
                                $checked_value = $model->a2_2;
                                $checked_text_first = \Yii::t('app', 'Yes');
                                $checked_text_second = \Yii::t('app', 'No');
                                $checkbox_checked = '<img src="css/images/checkbox-selected.png" width="16" height="16" style="margin:10 10 10 10;" />';
                                $checkbox_unchecked = '<img src="css/images/checkbox.png" width="16" height="16" style="margin:10 10 10 10;" />';

                                $s='';
                                if (!isset($checked_value)) {
                                    //both unchecked
                                    $s = $checked_text_first . $checkbox_unchecked . ' ' . $checkbox_unchecked . $checked_text_second;
                                } elseif ($checked_value != 0) {
                                    //first (Yes) checked
                                    //second (No) unchecked
                                    $s = $checked_text_first . $checkbox_checked . ' ' . $checkbox_unchecked . $checked_text_second;
                                } else {
                                    //first (Yes) unchecked
                                    //second (No) checked
                                    $s = $checked_text_first . $checkbox_unchecked . ' ' . $checkbox_checked . $checked_text_second;
                                }
                                echo $s;
                                ?>
                            </div>
                            <div id="divA22" class="hiddenBlock col-lg-12">
                                <div class="sy_margin_bottom_10 sy_pad_top_18">
                                    <?= \Yii::t('app/crf', 'Describe circumstances') . ':' ?>
                                </div>
                                <div class="sy_descriptive_text text-info">
                                    <?= NullValueFormatter::Format(TextFormatter::TextToHtml($model->a2_2_desc)) ?>
                                </div>
                            </div>
                    </div>      <!-- close content row -->
                </div>          <!-- close col-lg-12 crf-h2 -->
            </div>              <!-- close col-lg-12 crf-h2-box -->
    </div>                      <!-- close col-12-top -->

