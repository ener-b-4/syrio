<?php

use app\models\shared\NullValueFormatter;
use app\models\shared\TextFormatter;
use yii\helpers\Html;
use app\assets\AppAssetPdf;


/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

/* @var $model app\models\crf\Section1 */

//Registrtion different ASSETS for PDF
AppAssetPdf::register($this);
?>

<!--
SECTION A.2.3
-->

    <div class="col-lg-12 sy_margin_bottom_26">
            <div class="row form-inline">
                <div class='col-lg-12 crf-h2p'>
                    <span class='title-number'><?= \Yii::t('app', 'A').'.2.3.' ?></span> 
                    <span class='title-text'><strong><?= $model->attributeLabels()['a2_3'] . ' ' ?></strong></span> 
                    <div class='col-lg-12 section-content'>
                            <div class='col-lg-12'>
                                <?php
                                //pdf
                                $checked_value = $model->a2_3;
                                $checked_text_first = \Yii::t('app', 'Yes');
                                $checked_text_second = \Yii::t('app', 'No');
                                $checkbox_checked = '<img src="css/images/checkbox-selected.png" width="16" height="16" style="margin:10 10 10 10;" />';
                                $checkbox_unchecked = '<img src="css/images/checkbox.png" width="16" height="16" style="margin:10 10 10 10;" />';

                                $s='';
                                if (!isset($checked_value)) {
                                    //both unchecked
                                    $s = $checked_text_first . $checkbox_unchecked . ' '. $checked_text_second . $checkbox_unchecked;
                                } elseif ($checked_value != 0) {
                                    //first (Yes) checked
                                    //second (No) unchecked
                                    $s = $checked_text_first . $checkbox_checked . ' '. $checked_text_second . $checkbox_unchecked;
                                } else {
                                    //first (Yes) unchecked
                                    //second (No) checked
                                    $s = $checked_text_first . $checkbox_unchecked . ' '. $checked_text_second . $checkbox_checked;
                                }
                                echo $s;
                                ?>
                            </div>
                            <div id="divA23" class="hiddenBlock col-lg-12">
                                <div class="sy_margin_bottom_10 sy_pad_top_18">
                                    <?= \Yii::t('app/crf', 'If yes, outline the environmental impacts which already have been observed or are likely to result from the incident') . ':' ?>
                                </div>
                                <div class="sy_descriptive_text text-info">
                                    <?= NullValueFormatter::Format(TextFormatter::TextToHtml($model->a2_3_desc)) ?>
                                </div>
                            </div>
                    </div>      <!-- close content row -->
                </div>          <!-- close col-lg-12 crf-h2 -->
            </div>              <!-- close col-lg-12 crf-h2-box -->
    </div>                      <!-- close col-12-top -->



