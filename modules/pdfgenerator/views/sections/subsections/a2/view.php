<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

  use yii\helpers\Html;
  use app\models\units\Units;
  use app\models\crf\CrfHelper;
  use app\assets\AppAssetPdf;
  
  //Registrtion different ASSETS for PDF
  AppAssetPdf::register($this);
?>

<div class="sectionA-2 sy_pad_bottom_18">
    <div class="row">
        <div class="crf-h1 col-lg-12">
            <div style="margin-bottom: 10px">
                <span class='title-number crf-h1-title-number'>
                    <strong><?= \Yii::t('app', 'A').'.2.'; ?> </strong>
                </span>
                <span class='title-text crf-h1-title-text'>
                    <strong>
                        <?= CrfHelper::SectionsTitle()['A.2'] ?>
                    </strong>
                </span>
            </div>
            <div class="crf-h2 col-lg-12">
                <div class='row'>
                    <!-- this goes into the individual parts <div class="col-lg-12 sy_margin_bottom_10"> -->
                    <?php
                        $view = 'view_parts/a_2_0';
                        echo $this->render($view, ['model'=>$model, 'is_pdf' => 1]);
                    ?>
                </div>
            </div>
            <div class="crf-crf-base-font"><!-- no h2 --> 
                <div class='row'>
                    <!-- this goes into the individual parts <div class="col-lg-12 sy_margin_bottom_10"> -->
                    <?php

                    foreach(range(1,3) as $index) {
                        $view = 'view_parts/a_2_'.$index;
                        echo $this->render($view, ['model'=>$model, 'is_pdf' => 1]);
                    }

                    ?>
                </div>
            </div>
        </div>
    </div>
</div>

