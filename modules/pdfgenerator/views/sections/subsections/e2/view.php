<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

    use yii\helpers\Html;

    use app\models\crf\CrfHelper;
    use app\models\shared\TextFormatter;
    use app\models\shared\NullValueFormatter;
    use app\assets\AppAssetPdf;

    /* @var $this yii\web\View */
    /* @var $model app\models\events\drafts\sections\subsections\E2 */

        //Registrtion different ASSETS for PDF
        AppAssetPdf::register($this);

?>

<div class="sectionF-2 sy_pad_bottom_18">
    <div class="row">
        <div class="crf-h1 col-lg-12">
            <div class='sy_pad_bottom_18'>
                <span class='title-number crf-h1-title-number'>
                    <strong><?= \Yii::t('app', 'E').'.2.'; ?> </strong>
                </span>
                <span class='title-text crf-h1-title-text'>
                    <strong>
                        <?= CrfHelper::SectionsTitle()['E.2'] ?>
                    </strong>
                </span>
            </div>
            <div class="crf-h2">
                <div class='row'>
                    <div class="col-lg-12 sy_margin_bottom_10">
                        <div class='sy_margin_bottom_10'>
                            <?php 
                                $s1 = 'Indicate the system that failed and provide a description of the circumstances of the event / describe what has happened ';
                                $s2 = '(minimum distance between vessel and installation, '
                                    . 'course and speed of vessel, weather condition)';
                                echo Yii::t('app/crf', $s1) . ' ' . Yii::t('app/crf', $s2) . '.';
                            ?>
                        </div>
                        <div class="sy_descriptive_text">
                            <?= NullValueFormatter::Format(
                                TextFormatter::TextToHtml($model->e2_1)) ?>
                        </div>
                    </div>
                </div>
            </div>            
        </div>
    </div>
</div>

