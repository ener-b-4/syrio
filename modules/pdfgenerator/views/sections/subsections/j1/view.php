<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

    use yii\helpers\Html;

    use app\models\crf\CrfHelper;
    use app\models\shared\TextFormatter;
    use app\models\shared\NullValueFormatter;
    use app\assets\AppAssetPdf;

    /* @var $this yii\web\View */
    /* @var $model app\models\events\drafts\sections\subsections\J1 */

    //Registrtion different ASSETS for PDF
    AppAssetPdf::register($this);
?>

<div class="sectionJ-1 sy_pad_bottom_18">
    <div class="row">
        <div class="crf-h1 col-lg-12">
            <div class="sy_pad_bottom_18">
                <span class='title-number crf-h1-title-number'>
                    <strong><?= \Yii::t('app', 'J').'.1.'; ?> </strong>
                </span>
                <span class='title-text crf-h1-title-text'>
                    <strong>
                        <?= CrfHelper::SectionsTitle()['J.1'] ?>
                    </strong>
                </span>
            </div>
            <div class="crf-h2">
                <div class='row'>
                    <div class="col-lg-12 sy_margin_bottom_10">
                        <span class="crf-item-label"><?= \Yii::t('app', '(a)'); ?> </span>
                        <span class="crf-item-label"><?= $model->attributeLabels()['j1_1'] . ' ' . \Yii::t('app/crf', '(if applicable)')  . ': ' ?></span>

                        <span class="crf-item-label sy_descriptive_text <?php echo json_encode($is_pdf) == 1 ? 'text-info' : '' ?>">
                            <?= NullValueFormatter::Format(
                                TextFormatter::TextToHtml($model->j1_1)) ?>
                        </span>
                    </div>
                </div>
            </div>            
        </div>
    </div>
</div>

