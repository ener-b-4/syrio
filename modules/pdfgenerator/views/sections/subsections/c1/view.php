<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

    use yii\helpers\Html;

    use app\assets\AppAssetPdf;
    use app\models\crf\CrfHelper;
    use app\models\shared\TextFormatter;
    use app\models\shared\NullValueFormatter;

    /* @var $this yii\web\View */
    /* @var $model app\models\events\drafts\sections\subsections\C1 */

        //Registrtion different ASSETS for PDF
        AppAssetPdf::register($this);
?>

<div class="sectionC-1 sy_pad_bottom_18">
    <div class="row">
        <div class="crf-h1 col-lg-12">
            <div class="sy_pad_bottom_18">
                <span class='title-number crf-h1-title-number'>
                    <strong><?= \Yii::t('app', 'C').'.1.'; ?> </strong>
                </span>
                <span class='title-text crf-h1-title-text'>
                    <strong>
                        <?= CrfHelper::SectionsTitle()['C.1'] ?>
                    </strong>
                </span>
            </div>
            <div class="crf-h2 col-lg-12">
                <div class='row'>
                    <div class="col-lg-12 sy_margin_bottom_10">
                        <span class="crf-item-label"><?= \Yii::t('app', '(a)'); ?> </span>
                        <span class="crf-item-label"><?= $model->attributeLabels()['c1_1'] . ': ' ?></span>

                        <span class="crf-item-label sy_descriptive_text">
                            <?= NullValueFormatter::Format(
                                TextFormatter::TextToHtml($model->c1_1)) ?>
                        </span>
                    </div>
                </div>
            </div>            
        </div>
    </div>
</div>

