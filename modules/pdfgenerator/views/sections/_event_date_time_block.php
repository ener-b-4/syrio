<?php

/**
 * Renders the event time block as in the CPF
 *
 * @author vamanbo
 * 
 * 
 */

use app\models\events\EventDeclaration;

/* @var $this yii\web\View */
/* @var $model \app\models\events\EventDeclaration */

$dt = new DateTime($model->event_date_time);
?>

<!-- render the event details -->
<strong><?= Yii::t('app/crf' , 'Event date and time') ?></strong>
<br />
(a) <?= Yii::t('app/crf' , 'Event date') ?>: <?= date_format($dt, 'd-m-Y') ?> <i>(dd/mm/yyyy)</i>
<br/>
(b) <?= Yii::t('app/crf' , 'Event time') ?>: <?= date_format($dt, 'H:i') ?> <i>(hh:mm)</i>
