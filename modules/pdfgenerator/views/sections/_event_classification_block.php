<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

/// <summary>
/// ============================================================================================
/// Details: This file contains the event classification section acc. to CRF
/// ============================================================================================
/// </summary>

use app\models\ca\IncidentCategorization;

/* @var $this yii\web\View */
/* @var $model \app\models\events\EventDeclaration */

$event = $model->event;
$isSectionA1 = $model->is_a;
use app\models\crf\CrfHelper;

/* @var $this yii\web\View */
/* @var $model \app\models\ca\IncidentCategorization */

?>

<?php

/*
 * o functie care sa primeasca ca parmetri
 * - un item din event types
 * - sa creeze un table row deschis/inchis
 * - sa se acceseze singura daca 'subitems' exista
 */

function recursiveRowGenerator($item, $model) {
        $substract_subs = key_exists('subitems', $item) ? 1 : 0;
        
        echo '<table class="pdf_table_no_border_full_width">' . "\n";
        echo '<tr>' . "\n";
        
        //if (count($item)-$substract_subs === 3) {
        if (key_exists('checked', $item)) {
            //'checkbox' three columns row
            
            /* set the icon for the checkbox 
             * - get the attribute of the model that controlls the checked state (i.e. $item[$checked])
             * - set the checkbox image depending on the attribute's value
             */
            $attr = $item['checked'];
            if ($model->$attr) {
                $src = 'css/images/checkbox-selected.png';
            }
            else {
                $src = 'css/images/checkbox.png';
            }
            
            echo '<td class="ident">' . "\n"
            . '<img src="'. $src .'" width="16" height="16" />' . "\n"
            . '</td>' . "\n";
            
            echo '<td class="ident">' . "\n"
            . '<strong>' . $item['title'] . '</strong>' . "\n"
            . '</td>' . "\n";
            
            echo '<td>' . "\n";
            echo '<table class="pdf_table_no_border_full_width">' . "\n";
            echo    '<tr>' . "\n"
                    . '<td><div style="padding-bottom: 6px">' . "\n"
                        . $item['description'] . "\n"
                    . '</div></td>' . "\n"
                    . '</tr>' . "\n";
            echo    '<tr>' . "\n";
            echo        '<td>' . "\n";
            /*
             * if subitems exist and count(subitems>0) recursively call this function for each of the sub-items
             */
            if (key_exists('subitems', $item) && count($item['subitems']>0)) {
                foreach($item['subitems'] as $subitem) {
                    recursiveRowGenerator($subitem, $model);
                }
            }
            echo '</td>' . "\n";
            echo '</tr>' . "\n";
            echo '</table>' . "\n";
            echo '</td>' . "\n";
            
        } else {
            //if 'title' is null or empty make one column; else 2
            
            if (is_null($item['title']) || trim($item['title'])==='') {
                //'regular' two column row
                //echo '<td>'
                //. '</td>';
                echo '<td>' . "\n";
                echo '<table class="pdf_table_no_border_full_width">' . "\n";
                echo '<tr>' . "\n"
                . '<td><div style="padding-bottom: 6px">' . "\n"
                        . $item['description'] . "\n"
                        . '</div></td>' . "\n"
                . '</tr>' . "\n";
                echo '<tr>' . "\n";
                echo '<td>' . "\n";
                /*
                 * if subitems exist and count(subitems>0) recursively call this function for each of the sub-items
                 */
                if (key_exists('subitems', $item) && count($item['subitems']>0)) {
                    foreach($item['subitems'] as $subitem) {
                        recursiveRowGenerator($subitem, $model);
                    }
                }
                echo '</td>' . "\n";
                echo '</tr>' . "\n";
                echo '</table>' . "\n";
                echo '</td>' . "\n";
            }
            else {
                //'regular' two column row
                //echo '<td>'
                //. '</td>';

                echo '<td class="ident">' . "\n"
                . '<strong>' . $item['title'] . '</strong>' . "\n"
                . '</td>' . "\n";

                echo '<td>' . "\n";
                echo '<table class="pdf_table_no_border_full_width">' . "\n";
                echo '<tr>' . "\n"
                . '<td><div style="padding-bottom: 6px">' . "\n"
                        . $item['description'] . "\n"
                        . '</div></td>' . "\n"
                . '</tr>' . "\n";
                echo '<tr>' . "\n";
                echo '<td>' . "\n";
                /*
                 * if subitems exist and count(subitems>0) recursively call this function for each of the sub-items
                 */
                if (key_exists('subitems', $item) && count($item['subitems']>0)) {
                    foreach($item['subitems'] as $subitem) {
                        recursiveRowGenerator($subitem, $model);
                    }
                }
                echo '</td>' . "\n";
                echo '</tr>' . "\n";
                echo '</table>' . "\n";
                echo '</td>' . "\n";
            }   //end two-columns
        }
        echo '</tr>' . "\n";
        echo '</table>' . "\n";
        
}


?>

<div>
    <div class="sy_margin_top_2 sy_margin_bottom_2 crf-base-font">
        <strong><?= \Yii::t('app/crf', 'Event categorization'); ?></strong>
        <p>
            <small>
                <?= \Yii::t('app/crf', 'According to Annex IX of Directive 2013/30/EU'); ?>            
            </small>
        </p>
    </div>
    
    <p><em><strong><?= \Yii::t('app/crf', 'What type of event is being reported?'); ?></strong> <?= \Yii::t('app/crf', '(More than one option might be chosen)'); ?></em></p>
    
    <div class="sy_pad_top_36">
        <?php

        foreach (CrfHelper::EventTypes() as $item) {
            $depth = 1;
            recursiveRowGenerator($item, $model);
        }

        ?>
    </div>
</div>

<!-- REWRITE TITLE OF FULL REPORT -->
<?php $this->title = $model->event->event_name; ?>

