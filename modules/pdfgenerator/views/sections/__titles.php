<?php

/**
 * This contains the titles of each section (as in the prited version of the CRF)
 * 
 * The returned string is the HTML! version
 *
 * @author vamanbo
 */

$titles = [
    'A'=>   '<div class="col-lg-12 text-center" style=" padding-bottom: 24px">'. \Yii::t('app', 'SECTION {evt}', ['evt'=>\Yii::t('app', 'A')]) . '<br/>'
            . '<strong>' . \Yii::t('app/crf', 'UNINTENDED RELEASE OF OIL, GAS OR OTHER HAZARDOUS') . '<br/>'
            . ' ' . \Yii::t('app/crf', 'SUBSTANCES, WHETHER OR NOT IGNITED') . '</strong>'
            . '</div>',
    
    'B'=>   '<div class="col-lg-12 text-center" style=" padding-bottom: 24px">'. \Yii::t('app', 'SECTION {evt}', ['evt'=>\Yii::t('app', 'B')]) . '<br/>'
            . '<strong>' . \Yii::t('app/crf', 'LOSS OF WELL CONTROL REQUIRING ACTUATION OF WELL CONTROL EQUIPMENT, OR') . '<br/>'
            . \Yii::t('app/crf', 'FAILURE OF A WELL BARRIER REQUIRING ITS REPLACEMENT OR REPAIR') . '</strong>'
            . '</div>',
    
    'C'=>   '<div class="col-lg-12 text-center" style=" padding-bottom: 24px">'. \Yii::t('app', 'SECTION {evt}', ['evt'=>\Yii::t('app', 'C')]) . '<br/>'
            . '<strong>' . \Yii::t('app/crf', 'FAILURE OF A SAFETY AND ENVIRONMENTAL CRITICAL ELEMENT') . '</strong>'
            . '</div>',
    
    'D'=>   '<div class="col-lg-12 text-center" style=" padding-bottom: 24px">'. \Yii::t('app', 'SECTION {evt}', ['evt'=>\Yii::t('app', 'D')]) . '<br/>'
            . '<strong>' . \Yii::t('app/crf', 'SIGNIFICANT LOSS OF STRUCTURAL INTEGRITY, OR LOSS OF PROTECTION AGAINST ') 
            . \Yii::t('app/crf', 'THE EFFECTS OF FIRE OR EXPLOSION, OR LOSS OF STATION KEEPING IN RELATION TO ') 
            . \Yii::t('app/crf', 'A MOBILE INSTALLATION ') . '</strong>'
            . '</div>',
    
    'E'=>   '<div class="col-lg-12 text-center" style=" padding-bottom: 24px">'. \Yii::t('app', 'SECTION {evt}', ['evt'=>\Yii::t('app', 'E')]) . '<br/>'
            . '<strong>' . \Yii::t('app/crf', 'VESSELS ON COLLISION COURSE AND ACTUAL VESSEL COLLISIONS WITH ') .  '<br/>'
            . \Yii::t('app/crf', 'AN OFFSHORE INSTALLATION') . '</strong>'
            . '</div>',
    
    'F'=>   '<div class="col-lg-12 text-center" style=" padding-bottom: 24px">'. \Yii::t('app', 'SECTION {evt}', ['evt'=>\Yii::t('app', 'F')]) . '<br/>'
            . '<strong>' . \Yii::t('app/crf', 'HELICOPTER ACCIDENTS, ON OR NEAR OFFSHORE INSTALLATIONS') . '</strong>'
            . '</div>',
    
    'G'=>   '<div class="col-lg-12 text-center" style=" padding-bottom: 24px">'. \Yii::t('app', 'SECTION {evt}', ['evt'=>\Yii::t('app', 'G')]) . '<br/>'
            . '<strong>' . \app\models\crf\CrfHelper::SectionsTitle()['G'] . '</strong>'
            . '</div>',
    
    'H'=>   '<div class="col-lg-12 text-center" style=" padding-bottom: 24px">'. \Yii::t('app', 'SECTION {evt}', ['evt'=>\Yii::t('app', 'H')]) . '<br/>'
            . '<strong>' . \app\models\crf\CrfHelper::SectionsTitle()['H'] . '</strong>'
            . '</div>',
    
    'I'=>   '<div class="col-lg-12 text-center" style=" padding-bottom: 24px">'. \Yii::t('app', 'SECTION {evt}', ['evt'=>\Yii::t('app', 'I')]) . '<br/>'
            . '<strong>' . \Yii::t('app/crf', 'ANY EVACUATION OF PERSONNEL') . '</strong>'
            . '</div>',
    
    'J'=>   '<div class="col-lg-12 text-center" style=" padding-bottom: 24px">'. \Yii::t('app', 'SECTION {evt}', ['evt'=>\Yii::t('app', 'J')]) . '<br/>'
            . '<strong>' . \Yii::t('app/crf', 'A MAJOR ENVIRONMENTAL INCIDENT') . '</strong>'
            . '</div>'

];