<?php

/// <summary>
/// ============================================================================================
/// Repository path        = $HeadURL: http://stus-test04:8080/svn/SyRIO_beta/branches/OneViewCrf/modules/pdfgenerator/views/sections/view_all_1.php $
/// File                   = $Id: view_all_1.php 44 2015-12-18 16:06:16Z Bogdan $
/// Last changed by        = $Author: Bogdan $
/// Last changed date      = $Date: 2015-12-18 17:06:16 +0100 (Fri, 18 Dec 2015) $
/// Last committed         = $Revision: 44 $
/// ============================================================================================
/// Details: This file contains the review of event draft (generate PDF)
/// ============================================================================================
/// </summary>

use app\models\crf\CrfHelper;

$isCompilationPrint = $model->isCompilationPrint;

?>

<?php

/*
 * o functie care sa primeasca ca parmetrii
 * - un item din event types
 * - sa creeze un table row deschis/inchis
 * - sa se acceseze singura daca 'subitems' exista
 */

function recursiveRowGenerator_a($item, $max_depth, $depth) {
        $substract_subs = key_exists('subitems', $item) ? 1 : 0;
        $col_span = $max_depth-$depth+1;
        
        if (count($item)-$substract_subs === 3) {
            //'checkbox' three columns row
            /* add the eventual empty columns */
            for ($i=1; $i<$col_span-1; $i++) {
                echo '<td></td>';
            }
            
            echo '<td>'
            . '<img src="css/images/checkbox-selected.png" width="16" height="16" />'
            . '</td>';
            
            echo '<td>'
            . '<strong>' . $item['title'] . '</strong>'
            . '</td>';
            
            echo '<td colspan="' . $col_span .'">'
            . '<strong>' . $item['description'] . '</strong>'
            . '</td>';
            
        } else {
            //'regular' two column row
            /* add the eventual empty columns */
            for ($i=1; $i<$col_span; $i++) {
                echo '<td></td>';
            }
            
            echo '<td>'
            . '</td>';
            
            echo '<td>'
            . '<strong>' . $item['title'] . '</strong>'
            . '</td>';
            
            echo '<td colspan="' . $col_span .'">'
            . '<strong>' . $item['description'] . '</strong>'
            . '</td>';
            
        }
        echo '</tr>';
        
        /*
         * if subitems exist and count(subitems>0) recursively call this function for each of the sub-items
         */
        if (key_exists('subitems', $item) && count($item['subitems']>0)) {
            $depth++;
            foreach($item['subitems'] as $subitem) {
                recursiveRowGenerator($subitem, $max_depth, $depth);
            }
        }
        
        //$depth--;
}


function recursiveRowGenerator($item, $max_depth, $depth) {
        $substract_subs = key_exists('subitems', $item) ? 1 : 0;
        $col_span = $max_depth-$depth+1;
        
        echo '<table class="pdf_table_no_border_full_width">';
//        echo '<tr>';
        
        if (count($item)-$substract_subs === 3) {
            //'checkbox' three columns row
            /* add the eventual empty columns */
            echo '<td>'
            . '<img src="css/images/checkbox-selected.png" width="16" height="16" />'
            . '</td>';
            
            echo '<td>'
            . '<strong>' . $item['title'] . '</strong>'
            . '</td>';
            
            echo '<td>';
            echo '<table>';
            echo '<tr>'
            . $item['description']
            . '</tr>';
            echo '<tr>';
            echo '<td>';
            /*
             * if subitems exist and count(subitems>0) recursively call this function for each of the sub-items
             */
            if (key_exists('subitems', $item) && count($item['subitems']>0)) {
                $depth++;
                foreach($item['subitems'] as $subitem) {
                    recursiveRowGenerator($subitem, $max_depth, $depth);
                }
            }
            echo '</td>';
            echo '</tr>';
            echo '</table>';
            echo '</td>';
            
        } else {
            //'regular' two column row
            //echo '<td>'
            //. '</td>';
            
            echo '<td>'
            . '<strong>' . $item['title'] . '</strong>'
            . '</td>';
            
            echo '<td>';
            echo '<table>';
            echo '<tr>'
            . $item['description']
            . '</tr>';
            echo '<tr>';
            echo '<td>';
            /*
             * if subitems exist and count(subitems>0) recursively call this function for each of the sub-items
             */
            if (key_exists('subitems', $item) && count($item['subitems']>0)) {
                $depth++;
                foreach($item['subitems'] as $subitem) {
                    recursiveRowGenerator($subitem, $max_depth, $depth);
                }
            }
            echo '</td>';
            echo '</tr>';
            echo '</table>';
            echo '</td>';
            
        }
//        echo '</tr>';
        echo '</table>';
        
}


?>

<table class="pdf_table_no_border_full_width" style="border: 1px solid black">
    <?php
    
    foreach (CrfHelper::EventTypes() as $item) {
        $depth = 1;
        recursiveRowGenerator($item, 3, $depth);
    }
    
    echo 'sd';
    //die();
    ?>
</table>


<div crf-base-font>
  <?php 
    if ($model->is_a == true)  { echo '<img src="css/images/checkbox-selected.png" width="16" height="16" />'; }; 
    if ($model->is_a == false) { echo '<img src="css/images/checkbox.png" width="16" height="16" />'; }; 
  ?>
  <strong style="padding-left: 10px; padding-right: 10px">A</strong> 
  <?= Yii::t('app/crf', 'Unintended release of oil, gas or other hazardous substances, whether or not ignited') ?>:
</div>
<div class="crf-base-font">
    <!-- the description goes here -->
    <?php

        $selGuidance = '<ol>';
        $selGuidance .= '   <li class="sy_pdf_small">';
        $selGuidance .= \Yii::t('app/crf', 'Any unintentional release of ignited gas or oil on or from an offshore installation;');
        $selGuidance .= '   </li>';
        $selGuidance .= '   <li class="sy_pdf_small">';
        $selGuidance .=        \Yii::t('app/crf', 'The unintentional release on or from an offshore installation of:');
        $selGuidance .= '       <ol type="a">';
        $selGuidance .= '          <li class="sy_pdf_small">';
        $selGuidance .=                \Yii::t('app/crf', 'not ignited natural gas or evaporated associated gas if mass released >= 1 kg') . ';';
        $selGuidance .= '          </li>';
        $selGuidance .= '          <li class="sy_pdf_small">';
        $selGuidance .=                \Yii::t('app/crf', 'not ignited liquid of petroleum hydrocarbon if mass released >= 60 kg') . ';';
        $selGuidance .= '          </li>';
        $selGuidance .= '       </ol>';
        $selGuidance .= '   </li>';
        $selGuidance .= '   <li class="sy_pdf_small">';
        $selGuidance .= \Yii::t('app/crf', 'The unintentional	release	or escape of any hazardous substance, '
    . 'for which the major accident risk has been assessed in the report on major hazards, '
    . 'on or from an offshore installation, including wells and returns of drilling additives.');
        $selGuidance .= '   </li>';
        $selGuidance .= '</ol>';
    ?>
    <?= $selGuidance ?>
</div>

<h5>
  <?php 
    if ($model->is_b == true)  { echo '<img src="css/images/checkbox-selected.png" width="16" height="16" />'; }; 
    if ($model->is_b == false) { echo '<img src="css/images/checkbox.png" width="16" height="16" />'; }; 
  ?>
  <strong style="padding-left: 10px; padding-right: 10px">B</strong> 
  <?= \Yii::t('app/crf', 'Loss of well control requiring actuation of well control equipment, or failure of a well barrier requiring its replacement or repair') ?>:
</h5>

<div class="col-lg-10">
<?php

    $selGuidance = '<ol>';
    $selGuidance .= '   <li class="sy_pdf_small">';
    $selGuidance .=        \Yii::t('app/crf', 'Any blowout, regardless of the duration;');
    $selGuidance .= '   </li>';
    $selGuidance .= '   <li class="sy_pdf_small">';
    $selGuidance .=        \Yii::t('app/crf', 'The coming into operation of a blowout prevention or diverter system to control flow of well-fluids;');
    $selGuidance .= '   </li>';
    $selGuidance .= '   <li class="sy_pdf_small">';
    $selGuidance .=        \Yii::t('app/crf', 'The mechanical failure of any part of a well, whose purpose is to prevent or limit the effect of the unintentional release of fluids from a well or a reservoir being drawn on by a well, or whose failure would cause or contribute to such a release') . ';';
    $selGuidance .= '   </li>';
    $selGuidance .= '   <li class="sy_pdf_small">';
    $selGuidance .=        \Yii::t('app/crf', 'The taking of precautionary measures additional to any already contained in the original drilling programme where a planned minimum separation distance between adjacent wells was not maintained') . ';';
    $selGuidance .= '   </li>';
    $selGuidance .= '</ol>';
                        
?>
    <?= $selGuidance ?>
</div>

<!-- Section D  -->
<h5>
  <?php 
    if ($model->is_c == true)  { echo '<img src="css/images/checkbox-selected.png" width="16" height="16" />'; }; 
    if ($model->is_c == false) { echo '<img src="css/images/checkbox.png" width="16" height="16" />'; }; 
  ?>
  <strong style="padding-left: 10px; padding-right: 10px">C</strong> 
  <?= \Yii::t('app/crf', 'Failure of a safety and environmental critical element') ?>:
</h5>
<div class="col-lg-10">
    <!-- the description goes here -->
    <?php

        $selGuidance = '<ol class="list-unstyled">';
        $selGuidance .= '   <li class="sy_pdf_small">';
        $selGuidance .=        \Yii::t('app/crf', 'Any loss or non-availability of a SECE requiring immediate remedial action.');
        $selGuidance .= '   </li>';
        $selGuidance .= '</ol>';

        echo $selGuidance;
    ?>
</div>

<!-- http://mpdf1.com/manual/index.php?tid=108 -->
<p style="page-break-after:always">
    <!-- Section D  -->
    <h5>
      <?php 
        if ($model->is_d == true)  { echo '<img src="css/images/checkbox-selected.png" width="16" height="16" />'; }; 
        if ($model->is_d == false) { echo '<img src="css/images/checkbox.png" width="16" height="16" />'; }; 
      ?>
      <strong style="padding-left: 10px; padding-right: 10px">D</strong> 
      <?= \Yii::t('app/crf', 'Significant loss of structural integrity, or loss of protection against the effects of fire or explosion, or loss of station keeping in relation to a mobile installation') ?>:
    </h5>
    <div>
        <!-- the description goes here -->
            <?php

                $selGuidance = '<ol class="list-unstyled">';
                $selGuidance .= '   <li class="sy_pdf_small">';
                $selGuidance .=        \Yii::t('app/crf', 'Any detected condition that reduces the designed structural integrity of the installation, including stability, buoyancy and station keeping, to the extent that it requires '
                                . 'immediate remedial action.');
                $selGuidance .= '   </li>';
                $selGuidance .= '</ol>';

                echo $selGuidance;
            ?>
    </div>

    <h5>
      <?php 
        if ($model->is_e == true)  { echo '<img src="css/images/checkbox-selected.png" width="16" height="16" />'; }; 
        if ($model->is_e == false) { echo '<img src="css/images/checkbox.png" width="16" height="16" />'; }; 
      ?>
      <strong style="padding-left: 10px; padding-right: 10px">E</strong> 
      <?= \Yii::t('app/crf', 'Vessels on collision course and actual vessel collisions with an offshore installation') ?>:
    </h5>
    <div>
    <!-- the description goes here -->
        <?php

            $selGuidance = '<ol class="list-unstyled">';
            $selGuidance .= '   <li class="sy_pdf_small">';
            $selGuidance .=        \Yii::t('app/crf', 'Any collision, or potential collision, between a vessel and an offshore installation which has, or would have, enough energy to cause sufficient damage to the installation and/or vessel, to jeopardise the overall structural or process integrity.');
            $selGuidance .= '   </li>';
            $selGuidance .= '</ol>';

            echo $selGuidance;                 
        ?>                            
    </div>

    <h5>
      <?php 
        if ($model->is_f == true)  { echo '<img src="css/images/checkbox-selected.png" width="16" height="16" />'; }; 
        if ($model->is_f == false) { echo '<img src="css/images/checkbox.png" width="16" height="16" />'; }; 
      ?>
      <strong style="padding-left: 10px; padding-right: 10px">F</strong> 
      <?= \Yii::t('app/crf', 'Helicopter accidents, on or near offshore installations') ?>:
    </h5>
    <div>
    <?php

        $selGuidance = '<ol class="list-unstyled">';
        $selGuidance .= '   <li class="sy_pdf_small">';
        $selGuidance .=        \Yii::t('app/crf', 'Any collision, or potential collision, between a helicopter and an offshore installation.');
        $selGuidance .= '   </li>';
        $selGuidance .= '</ol>';

        echo $selGuidance;
    ?>
    </div>

    <h5>
      <?php 
        if ($model->is_g == true)  { echo '<img src="css/images/checkbox-selected.png" width="16" height="16" />'; }; 
        if ($model->is_g == false) { echo '<img src="css/images/checkbox.png" width="16" height="16" />'; }; 
      ?>
      <strong style="padding-left: 10px; padding-right: 10px">G</strong> 
      <?= \Yii::t('app/crf', 'Any fatal accident to be reported under the requirements of Directive 92/91/EEC') ?>.
    </h5>

    <h5>
      <?php 
        if ($model->is_h == true)  { echo '<img src="css/images/checkbox-selected.png" width="16" height="16" />'; }; 
        if ($model->is_h == false) { echo '<img src="css/images/checkbox.png" width="16" height="16" />'; }; 
      ?>
      <strong style="padding-left: 10px; padding-right: 10px">H</strong> 
      <?= \Yii::t('app/crf', 'Any serious injuries to five or more persons in the same accident to be reported under the requirements of Directive 92/91/EEC') ?>.
    </h5>

    <h5>
      <?php 
        if ($model->is_i == true)  { echo '<img src="css/images/checkbox-selected.png" width="16" height="16" />'; }; 
        if ($model->is_i == false) { echo '<img src="css/images/checkbox.png" width="16" height="16" />'; }; 
      ?>
      <strong style="padding-left: 10px; padding-right: 10px">I</strong> 
      <?= \Yii::t('app/crf', 'Any evacuation of personnel') ?>:
    </h5>

    <div class="col-lg-10">
    <!-- the description goes here -->
    <?php

        $selGuidance = '<ol class="list-unstyled">';
        $selGuidance .= '   <li class="sy_pdf_small">';
        $selGuidance .=        \Yii::t('app/crf', 'Any unplanned emergency evacuation of part of or all personnel as a result of, or where there is a significant risk of a major accident.');
        $selGuidance .= '   </li>';
        $selGuidance .= '</ol>';

        echo $selGuidance;
    ?>                          
    </div>

    <h5>
      <?php 
        if ($model->is_j == true)  { echo '<img src="css/images/checkbox-selected.png" width="16" height="16" />'; }; 
        if ($model->is_j == false) { echo '<img src="css/images/checkbox.png" width="16" height="16" />'; }; 
      ?>
      <strong style="padding-left: 10px; padding-right: 10px">J</strong> 
      <?= \Yii::t('app/crf', 'A major environmental incident') ?>:
    </h5>
    <div class="col-lg-10">
    <!-- the description goes here -->
    <?php

        $selGuidance = '<ol class="list-unstyled">';
        $selGuidance .= '   <li class="sy_pdf_small">';
        $selGuidance .=        \Yii::t('app/crf', 'Any major environmental incident as defined in Article 2.1.d and Article 2.37 of Directive 2013/30/EU.');
        $selGuidance .= '   </li>';
        $selGuidance .= '</ol>';

        echo $selGuidance;
    ?>         
    </div>

    <?php if ($isCompilationPrint == true) {

      $selRemarks = '<p class = "sy_pdf_block_v_spacer_16"  ><i>' . \Yii::t('app/crf', 'Remarks') . ': </i></p>';
      $selRemarks .= '<ol class="list-unstyled">';
      $selRemarks .= '  <li class = "sy_pdf_small sy_pdf_block_v_spacer_16">';
      $selRemarks .=       \Yii::t('app/crf', 'If the incident falls into one of the above mentioned categories, the operator/owner shall proceed to the relevant section(s), hence a single incident could result in completing multiple sections.  The operator/owner shall submit the filled in sections to the Competent Authority within 15 working days of the event, using the best information available at that time. If the event reported is a major accident, the Member State shall initiate a thorough investigation in accordance with Article 26 of Directive 2013/30/EU') ;
      $selRemarks .= '  </li>';
      $selRemarks .= '  <li class = "sy_pdf_small sy_pdf_block_v_spacer_16">';
      $selRemarks .=       \Yii::t('app/crf', 'Fatalities and serious injuries are reported under the requirements of Directive 92/91/EEC') ;
      $selRemarks .= '  </li>';
      $selRemarks .= '  <li class = "sy_pdf_small sy_pdf_block_v_spacer_16">';
      $selRemarks .=       \Yii::t('app/crf', 'Helicopter incidents are reported under CAA regulations. If a helicopter accident occurs in relation to Directive 2013/30/EU, section F shall be completed') ;
      $selRemarks .= '  </li>';
      $selRemarks .= '  <li class = "sy_pdf_small sy_pdf_block_v_spacer_16">';
      $selRemarks .=       \Yii::t('app/crf', 'Taking into account Member States\' obligations to maintain or achieve Good Environmental Status under Directive 2008/56/EC , if an unintended release of oil, gas or other hazardous substance, or the failure of a safety and environmental critical element results in or is likely to result in degradation of  the environment, such impacts should be reported to the competent authorities') ;
      $selRemarks .= '</ol>';

       echo $selRemarks;
    }
    ?>
</p>