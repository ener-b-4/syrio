<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

/// <summary>
/// ============================================================================================
/// Details: This file contains the review of event draft (generate PDF)
/// ============================================================================================
/// </summary>

use app\modules\pdfgenerator\behaviors\PrintableSection;
use app\models\ca\IncidentCategorization;
use app\models\crf\CrfDisplaySettings;

/* @var $this yii\web\View */
/* @var $model \app\models\events\EventDeclaration */
/* @var $incident_cat app\models\ca\IncidentCategorization */
/* @var bool $display_status */
/* @var $display_settings app\models\crf\CrfDisplaySettings */
/* @var string $task */

$event = $model->event;
$dt = new DateTime($event->event_date_time);

$isCampactPrint = $model->isCampactPrint;
$isCompilationPrint = $model->isCompilationPrint;
$isSectionA1 = $model->is_a;


if (Yii::$app->user->identity->isCaUser || Yii::$app->user->can('sys_admin')) {
    $ca_report = IncidentCategorization::findOne(['draft_id'=>$model->id]);
    $this->title = $ca_report->name; 
} else {
    $this->title = $model->event->event_name; 
}


?>




<div class="sy_pdf_block_v_spacer crf-base-font sy_margin_bottom_2">
    <?= $this->render('_event_date_time_block', ['model' => $model->event]) ?>
</div>
    
<div class="sy_pdf_block_v_spacer crf-base-font ">
    <?= $this->render('_event_details_block', ['model' => $model->event]) ?>
</div>


<div class="sy_pdf_block_v_spacer crf-base-font">
    <?= $this->render('_event_classification_block', ['model'=>$model]) ?>    
</div>

<?php
include_once '__titles.php';

$url = '@app/modules/pdfgenerator/views/default/view-section';        

foreach (range('a','j') as $letter) {
    $is_attr = 'is_'.$letter;
    
    if ($model->$is_attr) {
        echo '<pagebreak />';
        echo '<div class="sy_block_v_spacer">';     //open section's div
        //render the section's title DOM from $titles array (included above)
        echo $titles[strtoupper($letter)];
        
        //prepare the attributes for rendering the body of the section's report
        $section = strtoupper($letter);
        echo $this->render($url, 
            [
                'model' => $model,
                'v' => 0,
                'section' => $section, 
                'is_pdf' => 1,
                'display_settings' => $display_settings,
                'incident_cat' => $incident_cat,
                'title' => $this->title
            ]);
        
        echo '</div>';                              //close section's div
    }
}
?>

<!-- REWRITE TITLE OF FULL REPORT -->
<?php 
if (Yii::$app->user->identity->isCaUser) {
    $ca_report = IncidentCategorization::findOne(['draft_id'=>$model->id]);
    $this->title = $ca_report->name; 
} else {
    $this->title = $model->event->event_name; 
}

?>

