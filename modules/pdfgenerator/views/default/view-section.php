<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

/* @var $this yii\web\View */
/* @var $model \app\models\events\drafts\EventDraft */
/* @var $incident_cat app\models\ca\IncidentCategorization */
/* @var $section string ['a'-'j'] */
/* @var $title string */

$this->title = isset($title) ? $title . '/'. 'Section ' . strtoupper($section):'Section ' . strtoupper($section);
$event = $model->event;

//get the number of points
//create the SA accordingly
$split = explode('.', $section);

$sectionn = $split[0];                           //['a'-'j']
$section_upper = strtoupper($sectionn);          //['A'-'J']
$sectionAttr = 'section' . $section_upper;      //section[A-J]
$sa = $model->$sectionAttr;                     //section object

switch (count($split))
{
    case 3:
        $subsec = strtolower($split[0]) . $split[1];          //a1
        break;
}
?>

<div class="row">
    <div class="col-lg-12" id="subsection-content">
        
        <?php
            if (isset($subsec))
            {
                echo $this->render('../sections/subsections/' . $subsec . '/view', 
                    [
                        'event_id' => $sa->event_classification_id, 
                        'model'=>$sa->$subsec,
                        'is_pdf'=>$is_pdf
                    ]);
                
                
            }
            elseif ($section_upper==='G') {
                echo $this->render('../sections/subsections/g1/view');
            }
            elseif ($section_upper==='H') {
                echo $this->render('../sections/subsections/h1/view');
            }
            else
            {
                //display all the subsections
                for ($i = 1; $i<5; $i++)
                {
                    $subsec = strtolower($split[0]) . $i;
                    
//                    echo $this->render('@app/views/events/drafts/sections/subsections/' . $subsec . '/view', 
                    echo $this->render('../sections/subsections/' . $subsec . '/view',                     
                        [
                            'event_id' => $sa->event_classification_id, 
                            'model'=>$sa->$subsec,
                            'is_pdf'=>$is_pdf
                        ]);
                }
            }
        ?>
    </div>
    <div class="col-lg-12">
        <?php
            /* check if include_ma_assessment is true and render it for the proper Crf Section */
            if ($display_settings->include_ma_assessment) {
                //echo var_dump($display_settings->include_ma_assessment); 
                ?>
                <div class="sy_margin_top_2">     <!-- open assessment div -->
                <?= \app\widgets\assess_ma::widget([
                    'model'=>$incident_cat,
                    'section'=> strtolower($section),
                    'is_pdf' => 1
                ]) ?>
                </div>
                <?php
            }
        ?>
    </div>
</div>
