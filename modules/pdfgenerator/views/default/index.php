<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model app\modules\pdfgenerator\models\printPDFOptions */

$this->title = 'Exporting PDF';

echo $this->render('@app/views/shared_parts/_breadcrumbs', [
  'title_page' => $this->title, 
  'model' => $model, 
  'module_name' => 'EVENT_SECTION_PDF'
]);
?>

<div class="pdfgenerator-default-index">
  
  <?php $form = ActiveForm::begin(['id' => 'generate-pdf-form',
    'options' => ['class' => 'form-horizontal'],
  ]); ?>
    
  <div class="col-lg-12 sy_block_v_spacer ">
    <div class="row">
      <h1>
        <?= 
          //$this->context->action->uniqueId 
          //$model->event->event_name;
          $model->event_name;
        ?>
      </h1> 
    </div>

    <!--<p>
      This is the view content for action "<?= $this->context->action->id ?>".
      The action belongs to the controller "<?= get_class($this->context) ?>"
      in the "<?= $this->context->module->id ?>" module.
    </p>
    <p>
      You may customize this page by editing the following file:<br>
      <code><?php // ptint __FILE__ ?></code>
    </p>
    -->
  
    <div class="row sy_block_v_spacer">
      <div class="col-lg-10 col-lg-offset-1 form-inline">  
        <p>
            The "Compact Option" is used for not print all the sections not filled.
        </p>
        <ul class="list-inline sy_pdf_indent_less16">
          <li>
            <?= Html::activeRadio($model, 'isCampactPrint', ['onclick'=>'clicked(this)', 'id'=>'radio1', 'value'=>1, 'uncheck'=>null, 'label'=>'Compact Option']) ?>
          </li>
          <br />
          <li>
            <?= Html::activeRadio($model, 'isCampactPrint', ['onclick'=>'clicked(this)', 'id'=>'radio2', 'value'=>0, 'uncheck'=>null, 'label'=>'Full Option']) ?>
          </li>
        </ul>
      </div>
    </div>
    
    <div class="row sy_block_v_spacer">
      <div class="col-lg-10 col-lg-offset-1 form-inline">  
        <p>
            The "Compilation Option" is used to print all the section concerning the information compiled.
        </p>
        <ul class="list-inline sy_pdf_indent_less16">
          <li>
            <?= Html::activeRadio($model, 'isCompilationPrint', ['onclick'=>'clicked(this)', 'id'=>'radio3', 'value'=>1, 'uncheck'=>null, 'label'=>'Include Information Compiled']) ?>
          </li>
          <br />
          <li>
            <?= Html::activeRadio($model, 'isCompilationPrint', ['onclick'=>'clicked(this)', 'id'=>'radio4', 'value'=>0, 'uncheck'=>null, 'label'=>'Exclude Information Compiled']) ?>
          </li>
        </ul>
      </div>
    </div>
  </div>

  <?= 
    //Html::a(Yii::t('app', 'Generate PDF'), ['generate-pdf', 'id' => $model->id], ['class' => 'btn btn-success pull-right']) 
    Html::submitButton(Yii::t('app', 'Generate PDF'), ['class' => 'btn btn-primary']);
  ?>

  <?php ActiveForm::end(); ?>
</div>




