<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model app\modules\pdfgenerator\models\printPDFHeader */

?>

<div class="table-responsive"> 
  <!--<table autosize="1.6" class="pdf_table_yes_no pdf_table-bordered">-->
    <table class="table" style="border: none; margin-bottom: 0px; font-size: 10px;">  
    <tbody>
      <tr>
          <td style="border: none; margin-bottom: 0px; font-size: 10px; text-align: left;"><strong><?= $model->sLeftString ?></strong></td>
          <td style="border: none; margin-bottom: 0px; font-size: 10px; text-align: right;"><strong><?= $model->sRightString ?></strong></td>
      </tr>
    </tbody>
  </table>
</div>    <!--class="table-responsive"-->