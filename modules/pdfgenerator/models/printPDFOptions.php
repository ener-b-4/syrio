<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\modules\pdfgenerator\models;

use Yii;
use yii\base\Model;

/*
 * @property integer $id
 * @property integer $isCampactPrint
 * @property integer $isCompilationPrint
 * @property string $event_name
 */

class printPDFOptions extends \yii\db\ActiveRecord
{
  public $isCampactPrint;
  public $isCompilationPrint;
  public $id;                   //event_classification_id
  public $event_name;
  public $session_desc;         //used for breadcrumbs for print single section
  public $event_id;             //used for breadcrumbs for print single section
  
  /**
    * @return array the validation rules.
    */
   public function rules()
   {
      return [
          // username and password are both required
          //[['id', 'event_name'], 'required'],

        // set "level" to be 1 if it is empty
        ['isCampactPrint', 'default', 'value' => 1],
        ['isCompilationPrint', 'default', 'value' => 0],
      ];
   }
}
