<?php

/// <summary>
/// ============================================================================================
/// Repository path        = $HeadURL: http://stus-test04:8080/svn/SyRIO_beta/branches/OneViewCrf/modules/pdfgenerator/models/Constants.php $
/// File                   = $Id: Constants.php 14 2015-10-21 09:11:09Z Jessica $
/// Last changed by        = $Author: Jessica $
/// Last changed date      = $Date: 2015-10-21 11:11:09 +0200 (Wed, 21 Oct 2015) $
/// Last committed         = $Revision: 14 $
/// ============================================================================================
/// Details: This file contains the module of table event_classifications of 
//  module 'pdfgenerator'
/// ============================================================================================
/// </summary>

namespace app\modules\pdfgenerator\models;

use Yii;

/**
 * This is the model class for table "constants".
 *
 * @property integer $num_code
 * @property string $name
 * @property integer $type
 */
class Constants extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'constants';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['num_code', 'name', 'type'], 'required'],
            [['num_code', 'type'], 'integer'],
            [['name'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'num_code' => Yii::t('app', 'Num Code'),
            'name' => Yii::t('app', 'Name'),
            'type' => Yii::t('app', 'Type'),
        ];
    }
}
