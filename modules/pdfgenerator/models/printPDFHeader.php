<?php

/// <summary>
/// ============================================================================================
/// Repository path        = $HeadURL: http://stus-test04:8080/svn/SyRIO_beta/branches/OneViewCrf/modules/pdfgenerator/models/printPDFHeader.php $
/// File                              = $Id: printPDFHeader.php 14 2015-10-21 09:11:09Z Jessica $
/// Last changed by        = $Author: Jessica $
/// Last changed date    = $Date: 2015-10-21 11:11:09 +0200 (Wed, 21 Oct 2015) $
/// Last committed         = $Revision: 14 $
/// ============================================================================================
/// Details: Model header of PDF page generated
/// ============================================================================================
/// </summary>

namespace app\modules\pdfgenerator\models;

use Yii;
use yii\base\Model;

/*
 * @property string $sLeftString
 * @property string $sRightString
 */

class printPDFHeader extends \yii\db\ActiveRecord
{
  public $sLeftString;
  public $sRightString;
  
  /**
    * @return array the validation rules.
    */
  public function rules()
  {
    return [
      // set "level" to be 1 if it is empty
      ['sLeftString', 'default', 'value' => ''],
      ['sRightString', 'default', 'value' => ''],
    ];
  }
}
