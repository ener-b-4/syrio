<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\translations\models\TMessage */
/* @var $messages [id, message] */
/* @var $countries [iso2, country] */

$this->title = Yii::t('app', 'register new {evt}', [
    'evt'=>\Yii::t('app', 'language')
    ]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Management'), 'url' => ['/management']];
$this->params['breadcrumbs'][] = ['label' => ucfirst(Yii::t('app/translations', 'translations')), 'url' => ['/translation']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tmessage-new-language">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form_new_language', [
        'model' => $model,
        'messages'=>$messages,
        'countries'=>$countries
    ]) ?>

</div>
