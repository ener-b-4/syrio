<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use kartik\form\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\translations\models\TMessage */
/* @var $messages app\modules\translations\models\TMessage */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tmessage-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ref', ['showLabels'=>false])->hiddenInput() ?>
    
    <?= $form->field($model, 'language')->dropDownList($countries, [
        'prompt' => '- ' . \Yii::t('app', 'language') . ' -'
    ]) ?>

    <?= $form->field($model, 'id')->dropDownList($messages, [
        'prompt' => '- ' . \Yii::t('app/translations', 'message') . ' -'
    ])->hint(\Yii::t('app/translations', 'At least one source message must be translated to register a new language.')) ?>

    
    <?= $form->field($model, 'translation')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app/commands', 'Submit'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
