<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use kartik\form\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\translations\models\TMessage */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tmessage-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ref', ['showLabels'=>false])->hiddenInput() ?>
    
    <?= $form->field($model, 'id', ['showLabels'=>false])->hiddenInput() ?>

    <?= $form->field($model, 'language', ['showLabels'=>false])->hiddenInput() ?>

    <div class="row">
        <div class="col-lg-12">
            <p class="text-info">
                <?= \Yii::t('app/translations', 'Please provide the translation for the source message in the language of choice ({evt}).', ['evt'=>  strtoupper($model->language)]) ?>
                <br>
                
                <span class='text-warning'>
                    <?= \Yii::t('app/translations', 'Pay special attention to (preserve!) the eventual placeholders in the source message (curly bracelets).') ?>
                </span>
            </p>
            <div class='sy_pad_top_18'>
                <strong>
                    <?= ucfirst(\Yii::t('app/translations', 'source message')) ?>
                </strong>
            </div>
            <p>
                <?= $model->sourceMessage->message ?>
            </p>
        </div>
        <div class='col-lg-12 sy_pad_top_18'>
            <?= $form->field($model, 'translation')->textarea(['rows' => 6]) ?>
        </div>
    </div>
    
    

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app/commands', 'Submit'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
