<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\translations\models\TMessage */

$this->title = Yii::t('app/translations', 'Translate');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Management'), 'url' => ['/management']];
$this->params['breadcrumbs'][] = ['label' => ucfirst(Yii::t('app/translations', 'translations')), 'url' => ['/translation']];
$this->params['breadcrumbs'][] = $this->title . '(' . strtoupper($model->language) . ')';
?>
<div class="tmessage-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
