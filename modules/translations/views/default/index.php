<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $searchModel app\modules\translations\models\TMessageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

/* @var $stats \app\modules\translations\models\statistics\LanguagesStatistics */


$this->title = ucfirst(Yii::t('app/translations', 'translations'));

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Management'), 'url' => ['/management']];
$this->params['breadcrumbs'][] = $this->title;



$languages = $stats->languages->all();

?>
<div class="tmessage-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php  // Html::a(Yii::t('app/translations', 'Create Tmessage'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <div class="content">
        <div class="row">
            <div class="col-lg-12">
               <?php include Yii::getAlias('@app') . '/views/shared_parts/message_snippet.php'; ?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span class="sy_text_panel_title"><?= ucfirst(Yii::t('app/translations', 'registered languages')) ?></span>
                    </div>
                    <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <table class="table table-condensed">
                                            <thead>
                                                <tr>
                                                    <th colspan="4">
                                                        <?= ucfirst(Html::encode(\Yii::t('app', 'total'))) . ': ' ?>
                                                        <?= $stats->languages_count ?>
                                                    </th>
                                                    <th>
                                                        <div style="text-align: right">
                                                            <?= Html::a('<span class="glyphicon glyphicon-plus"></span>', Url::to(['/translation/t-message/new_language']), [
                                                                'class' => 'btn btn-success',
                                                                'title' => ucfirst(\Yii::t('app', 'register new {evt}', [
                                                                    'evt'=>\Yii::t('app', 'language')
                                                                ]))
                                                                ]) ?>
                                                        </div>
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <th>
                                                        <?= ucfirst(Html::encode(\Yii::t('app', 'country'))) ?>
                                                    </th>
                                                    <th>
                                                        <?= ucfirst(Html::encode(\Yii::t('app', 'code'))) ?>
                                                    </th>
                                                    <th>
                                                        <?= ucfirst(Html::encode(\Yii::t('app', 'translated'))) ?>
                                                    </th>
                                                    <th colspan="2">
                                                        <?= ucfirst(Html::encode(\Yii::t('app', 'completion'))) ?>
                                                    </th>
                                                </tr>
                                            </thead>
                                        <?php
                                        foreach ($languages as $language) {
                                        ?>
                                            <tr>
                                                <td>
                                                    <?= Html::encode($language["country"]) ?>
                                                </td>
                                                <td>
                                                    <?= Html::encode($language["language"]) ?>
                                                </td>
                                                <td>
                                                    <?= Html::encode($language["items"]) ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    $comp = $stats->languages_count === 0 ? '-' : 
                                                            number_format(($language['items']/$stats->total_messages)*100, 2);
                                                    
                                                    echo $comp . '%';
                                                    ?>
                                                </td>
                                                <td style="text-align: right">
                                                    <?php
                                                        $url = Url::to(['/translation/t-source-message/translations', 'lang'=>$language["language"]]);
                                                        echo Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                                            'title' => \Yii::t('app', 'View')
                                                        ])
                                                    ?>
                                                    
                                                    <?php
                                                        $url = Url::to(['/translation/t-source-message/del_translation', 'lang'=>$language["language"]]);
                                                        echo Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                                            'title' => \Yii::t('app', 'Delete'),
                                                            'data-method' => 'post',
                                                            'data-confirm' => \Yii::t('app/translation', ''
                                                                    . 'This will remove language \'{evt}\' from SyRIO.', [
                                                                        'evt'=>$language['language']
                                                                    ])
                                                                    . "\n\n" .
                                                                    \Yii::t('app', 'The operation CANNOT be undone!')
                                                                    . "\n" .
                                                                    \Yii::t('app', 'Are you sure?')
                                                        ])
                                                    ?>
                                                </td>
                                            </tr>
                                        <?php 
                                        }   //enf foreach
                                            
                                        ?>
                                        </table>
                                    </div>
                                </div>
                        
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span class="sy_text_panel_title"><?= ucfirst(Yii::t('app/translations', 'source messages')) ?></span>
                    </div>
                    <div class="panel-body">
                        <?php
                        
                        $msg_l1 = \Yii::t('app', "The source messages are the ones that must be translated in other languages. "
                                . "The messages are grouped in Categories and address the application messages (UI) and the Common Reporting Format (app/crf) and Common Publication Format (app/cpf) messages and text.");
                        $msg_l2 = \Yii::t('app', "Currently, there are {evt} messages registered with SyRIO." , [
                            'evt'=>$stats->total_messages
                        ]);

                        ?>
                        
                        <div class="row">
                            <div class="col-xs-11">
                                <?= $msg_l1 ?>
                                <br/>
                                <br/>
                                <?= $msg_l2 ?>
                            </div>
                            <div class="col-xs-1 pull-right">
                                <?= Html::a('<span class="glyphicon glyphicon-menu-right"></span>...'
                                        , Url::to(['/translation/t-source-message/index']), [
                                            'class' => 'btn btn-info',
                                            'title' => ucfirst(\Yii::t('app', 'more')) . '...'
                                            ]) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</div>
