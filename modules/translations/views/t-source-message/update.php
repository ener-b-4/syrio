<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\modules\translations\models\TSourceMessage */

$this->title = ucfirst(Yii::t('app', 'modify {evt}', [
    'evt'=>yii::t('app/translations', 'source message')
]));
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Management'), 'url' => ['/management']];
$this->params['breadcrumbs'][] = ['label' => ucfirst(Yii::t('app/translations', 'translations')), 'url' => ['/translation']];
$this->params['breadcrumbs'][] = ['label' => ucfirst(Yii::t('app/translations', 'source messages')), 'url' => [
    Url::to('/translation/t-source-message/index')
    ]];
$this->params['breadcrumbs'][] = ucfirst(Yii::t('app', 'modify {evt}', [
    'evt'=>''
]));

?>
<div class="tsource-message-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
