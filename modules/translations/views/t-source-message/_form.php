<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\translations\models\TSourceMessage */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tsource-message-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class='row'>
        <div class='col-lg-12 sy_pad_top_18'>
            <?= $form->field($model, 'category')->textInput([
                'maxlength' => true,
                'placeholder' => 'e.g. app, app/crf, ...' 
                ]) ?>

            <?= $form->field($model, 'message')->textarea(['rows' => 6]) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app/commands', 'Submit'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
