<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use yii\helpers\Html;

/* get the app categories */
$categories = ArrayHelper::map(
        app\modules\translations\models\TSourceMessage::find()
        ->select(['category'])
        ->asArray()        
        ->distinct(true)
        ->all(), 
        'category', 'category');

$tfilter = [
    '*' => '('.\Yii::t('app','all').')',
    '+' => '('.\Yii::t('app','existing').')',
    '-' => '('.\Yii::t('app','missing').')',
];

$reset_url = \yii\helpers\Url::to([
                '/translation/t-source-message/translations', 'lang'=>$searchModel->language                        
            ]);

$grid_toolbar = [
        [
            'content'=>
                Html::a('<i class="glyphicon glyphicon-repeat"></i>', $reset_url, [
                    'class' => 'btn btn-default', 
                    'title' => Yii::t('app', 'Reset Grid')
                ]),
            'options' => ['class' => 'btn-group']
        ],
        '{export}',
        '{toggleData}'
        //'{toggle-data}'
    ];   //end grid toolbar




$columns = [
    [
        'class' => 'yii\grid\SerialColumn'
    ],

    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'id',
        'hidden' => true,
        'hiddenFromExport' => true
    ],
    
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'category',
        'label' => ucfirst(Yii::t('app/translations', 'category')),
        'filterType' => GridView::FILTER_SELECT2,   // '\kartik\widgets\Select2',
        'filter' => $categories,
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear'=>true],
        ],
        'filterInputOptions' => ['placeholder'=>\Yii::t('app/translations' ,'category')],
    ],

    [
        'class' => '\kartik\grid\DataColumn',
        'label' => ucfirst(Yii::t('app/translations', 'source message')),
        'attribute' => 'message',
        'format' => 'ntext'
    ],
    
    
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'language',
        'hidden' => true,
        'hiddenFromExport' => true
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'language_db',
        'hidden' => true,
        'hiddenFromExport' => true
    ],
    
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'translation',
        'label' => ucfirst(Yii::t('app/translations', 'translation')),
            'filterType' => GridView::FILTER_SELECT2,   // '\kartik\widgets\Select2',
            'filter' => $tfilter,
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear'=>true],
            ],
            'filterInputOptions' => ['placeholder'=> '- ' . \Yii::t('app/translations' ,'translation') . ' -'],      
    ],
    
    
//    [
//        'class'=>'kartik\grid\EditableColumn',
//        'attribute'=>'translation',
//        //'pageSummary'=>'Total',
//        'vAlign'=>'middle',
//        'width'=>'210px',
//        'readonly'=>function($model, $key, $index, $widget) {
//            //return (!$model->status); // do not allow editing of inactive records
//            return false;
//        },
//        'editableOptions'=> function ($model, $key, $index, $widget) {
//            return [
//                'header'=>'Name', 
//                'size'=>'md',
//                'inputType'=> kartik\editable\Editable::INPUT_TEXT
////                'afterInput'=>function ($form, $widget) use ($model, $index, $colorPluginOptions) {
////                    return $form->field($model, "color")->widget(\kartik\widgets\ColorInput::classname(), [
////                        'showDefaultPalette'=>false,
////                        'options'=>['id'=>"color-{$index}"],
////                        'pluginOptions'=>$colorPluginOptions,
////                    ]);
////                }
//            ];
//        }
//    ],    
    
    [
        'class' => kartik\grid\ActionColumn::className(),
        'header' => ucfirst(Yii::t('app', 'actions')),        
        'buttons' => [
            'edit_translation' => function($url, $model, $key) {
                $url = yii\helpers\Url::to(['/translation/t-source-message/edit_translation', 'lang'=>$model['language'], 'src_id'=>$model['id']]);
                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                    'title' => \Yii::t('app', 'Edit')
                ]);
            },
            'reset_translation' => function($url, $model, $key) {
                $url = yii\helpers\Url::to(['/translation/t-source-message/reset_translation', 'lang'=>$model['language'], 'src_id'=>$model['id']]);
                return Html::a('<span class="glyphicon glyphicon-remove"></span>', $url, [
                    'title' => ucfirst(\Yii::t('app', 'reset'))
                ]);
            }
        ],
        'template' => '{edit_translation}'
    ],
];
        
        
$isFa = true;   
$pdfHeader = '';
$pdfFooter = '';
$title = \Yii::t('app', 'List of translated messages for {evt}', [
    'evt' => $searchModel->language
]);

$exportConfig = [
    GridView::HTML => [
        'label' => 'HTML',
        'icon' => $isFa ? 'file-text' : 'floppy-saved',
        'iconOptions' => ['class' => 'text-info'],
        'showHeader' => true,
        'showPageSummary' => true,
        'showFooter' => true,
        'showCaption' => true,
        'filename' => 'grid-export',
        //'alertMsg' => Yii::t('kvgrid', 'The HTML export file will be generated for download.'),
        'options' => ['title' => Yii::t('app', 'Hyper Text Markup Language')],
        'mime' => 'text/html',
        'config' => [
            'cssFile' => 'http://netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css'
        ]
    ],
    
    GridView::CSV => [
        'label' => 'CSV',
        'icon' => $isFa ? 'file-code-o' : 'floppy-open', 
        'iconOptions' => ['class' => 'text-primary'],
        'showHeader' => true,
        'showPageSummary' => true,
        'showFooter' => true,
        'showCaption' => true,
        'filename' => 'grid-export',
        //'alertMsg' => Yii::t('kvgrid', 'The CSV export file will be generated for download.'),
        'options' => ['title' => Yii::t('app', 'Comma Separated Values')],
        'mime' => 'application/csv',
        'config' => [
            'colDelimiter' => ",",
            'rowDelimiter' => "\r\n",
        ]
    ],    
    
    
    GridView::TEXT => [
        'label' => Yii::t('app', 'Text'),
        'icon' => $isFa ? 'file-text-o' : 'floppy-save',
        'iconOptions' => ['class' => 'text-muted'],
        'showHeader' => true,
        'showPageSummary' => true,
        'showFooter' => true,
        'showCaption' => true,
        'filename' => 'grid-export',
        'alertMsg' => Yii::t('app', 'The TEXT export file will be generated for download.'),
        'options' => ['title' => Yii::t('app', 'Tab Delimited Text')],
        'mime' => 'text/plain',
        'config' => [
            'colDelimiter' => "\t",
            'rowDelimiter' => "\r\n",
        ]
    ],
    GridView::EXCEL => [
        'label' => 'Excel',
        'icon' => $isFa ? 'file-excel-o' : 'floppy-remove',
        'iconOptions' => ['class' => 'text-success'],
        'showHeader' => true,
        'showPageSummary' => true,
        'showFooter' => true,
        'showCaption' => true,
        'filename' => 'grid-export',
        'alertMsg' => Yii::t('app', 'The EXCEL export file will be generated for download.'),
        'options' => ['title' => Yii::t('app', 'Microsoft Excel 95+')],
        'mime' => 'application/vnd.ms-excel',
        'config' => [
            'worksheet' => Yii::t('app', 'ExportWorksheet'),
            'cssFile' => ''
        ]
    ],
    GridView::PDF => [
        'label' => 'PDF',
        'icon' => $isFa ? 'file-pdf-o' : 'floppy-disk',
        'iconOptions' => ['class' => 'text-danger'],
        'showHeader' => true,
        'showPageSummary' => true,
        'showFooter' => true,
        'showCaption' => true,
        'filename' => 'grid-export',
        'alertMsg' => Yii::t('app', 'The PDF export file will be generated for download.'),
        'options' => ['title' => Yii::t('app', 'Portable Document Format')],
        'mime' => 'application/pdf',
        'config' => [
            'mode' => 'ro',
            'format' => 'A4-L',
            'destination' => 'D',
            'marginTop' => 20,
            'marginBottom' => 20,
            'cssInline' => '.kv-wrap{padding:20px;}' .
                '.kv-align-center{text-align:center;}' .
                '.kv-align-left{text-align:left;}' .
                '.kv-align-right{text-align:right;}' .
                '.kv-align-top{vertical-align:top!important;}' .
                '.kv-align-bottom{vertical-align:bottom!important;}' .
                '.kv-align-middle{vertical-align:middle!important;}' .
                '.kv-page-summary{border-top:4px double #ddd;font-weight: bold;}' .
                '.kv-table-footer{border-top:4px double #ddd;font-weight: bold;}' .
                '.kv-table-caption{font-size:1.5em;padding:8px;border:1px solid #ddd;border-bottom:none;}',
            'methods' => [
                'SetHeader' => [
                    ['odd' => $pdfHeader, 'even' => $pdfHeader]
                ],
                'SetFooter' => [
                    ['odd' => $pdfFooter, 'even' => $pdfFooter]
                ],
            ],
            'options' => [
                'title' => $title,
                'subject' => Yii::t('app', 'PDF export generated by SyRIO'),
                //'keywords' => Yii::t('kvgrid', 'krajee, grid, export, yii2-grid, pdf')
            ],
            'contentBefore'=>'',
            'contentAfter'=>''
        ]
    ],    
];        