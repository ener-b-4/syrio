<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
//use yii\grid\GridView;
//use app\components\helpers\DataGrid\CustomGridView;
use kartik\grid\GridView;


/* @var $this yii\web\View */
/* @var $searchModel app\modules\translations\models\TTranslationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = ucfirst(\Yii::t('app/translations', 'translations')) . ' (' . strtoupper($searchModel->language) . ')';

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Management'), 'url' => ['/management']];
$this->params['breadcrumbs'][] = ['label' => ucfirst(Yii::t('app/translations', 'translations')), 'url' => ['/translation']];
$this->params['breadcrumbs'][] = strtoupper($searchModel->language);

include_once 'parts/gridview_columns_translations.php';

?>
<div class="tsource-message-index">

    <div class="row">
        <div class="col-lg-12">
            <?php include Yii::getAlias('@app') . '/views/shared_parts/message_snippet.php'; ?>
        </div>
    </div>
    
    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php
    //echo '<pre>';
    //echo var_dump($dataProvider->getModels());
    //echo '</pre>';
    
    
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $columns,
        
        'toolbar' => $grid_toolbar,  
        
        'panel'=>[
            'type'=>GridView::TYPE_PRIMARY,
            'heading'=>'',
        ],        
        
        // set export properties
        'export' => [
          'icon' => 'share-square-o',
          'encoding' => 'utf-8',
          'fontAwesome' => true,
          'target' => GridView::TARGET_SELF,   //target: string, the target for submitting the export form, which will trigger the download of the exported file. 
          'showConfirmAlert' => TRUE,
          'header' => '<li role="presentation" class="dropdown-header">' . \Yii::t('app', 'Grid Export') . '</li>'
          //'header' => 'Export Page Data',
  	],
        
        'persistResize'=>false,
        'exportConfig'=>$exportConfig,        
        
    ]); ?>

</div>
