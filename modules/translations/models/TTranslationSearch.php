<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\modules\translations\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\translations\models\TTranslation;

/**
 * TSourceMessageSearch represents the model behind the search form about `app\modules\translations\models\TSourceMessage`.
 */
class TTranslationSearch extends TTranslation
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['category', 'message', 'language', 'translation'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->load($params);
        
        if (isset($this->translation)) {
            switch ($this->translation) {
                case '+':
                    //$query = TTranslation::find();
                    $query = TTranslation::find($this->language, '+');
                    //$query->andWhere([
                    //    '[[translation]]' => null
                    //]);
                    break;
                case '-':
                    //$query = TTranslation::find();
                    $query = TTranslation::find($this->language, '-');
                    //$query->andWhere([
                    //    '[[translation]]' => null
                    //]);
                    break;
                default:
                    $query = TTranslation::find($this->language);
                    break;
            }
        }
        else {
            $query = TTranslation::find($this->language);
        }

        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes'=>[
                'category',
                'message',
                'translation'
            ]
        ]);
        
//        echo var_dump($this->translation);
//        die();

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'category', $this->category])
            ->andFilterWhere(['like', 'message', $this->message]);

        return $dataProvider;
    }
    
    private $_translation;
    public function setTranslation($value) {
        $this->_translation = $value;
    }
    public function getTranslation() {
        return $this->_translation;
    }
}
