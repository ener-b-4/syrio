<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use \app\modules\translations\models\TMessage;
use \app\modules\translations\models\TSourceMessage;

namespace app\modules\translations\models\statistics;

/**
 * Description of LanguagesStatistics
 *
 * @author vamanbo
 */
class LanguagesStatistics extends \yii\base\Model {
    
    
    public $total_messages;
    
    public $languages_count;
    public $languages;
    
    public function __construct($config = array()) {
        parent::__construct($config);
        
        /* get the total number of source messages */
        $this->total_messages = \app\modules\translations\models\TSourceMessage::find()->count();

        /* get the list of the registered languages together with the number of records in that language*/
        //select distinct language, count(language) as number from t_message group by language order by language;
        
        $this->languages = \app\modules\translations\models\TMessage::find()
                ->leftJoin('c_country', 'UPPER({{t_message}}.[[language]]) = {{c_country}}.[[iso2]]')
                ->asArray()
                ->select([
                    'language', 
                    '{{c_country}}.[[short_name]] as country', 
                    'count(language) as items'])
                ->orderBy('language')
                ->groupBy('language');
        
        $this->languages_count = $this->languages->count();
    }
}
