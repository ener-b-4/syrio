<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\db\ActiveRecord;

namespace app\modules\translations\models;

/**
 * Description of TTranslation
 *
 * @author vamanbo
 */
class TTranslation extends TSourceMessage {
    //specific language
    public $language;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['message'], 'string'],
            [['category'], 'string', 'max' => 32],
            [['translation', 'language'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            //'id' => ucfirst(Yii::t('app/translations', 'source message')),
            'category' => ucfirst(Yii::t('app', 'category')),
            'message' => ucfirst(Yii::t('app', 'message')),
            'translation' => ucfirst(Yii::t('app/translations', 'translation')),
        ];
    }
    
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslation()
    {
        //return 'abc';
        //return TMessage::find()
        //        -> where(['id'=>$this->id])
        //        -> andWhere( ['language' => $this->language] )->one();
        
        //return $this->hasOne(TMessage::className(), ['id' => 'id', 'language' => $this->language]);
        return $this->hasOne(TMessage::className(), ['id' => 'id']);        
    }
    
    
    public static function find($lang = null, $special = null) {
        
        /*
         * 
         * -- merge!
(SELECT DISTINCT `t_source_message`.`id`, `t_source_message`.`message`, `t_message`.`translation` FROM `t_source_message` LEFT JOIN `t_message` ON  `t_source_message`.`id` = `t_message`.`id` WHERE (`t_message`.`language` IS NULL))
UNION
(SELECT DISTINCT `t_source_message`.`id`, `t_source_message`.`message`, `t_message`.`translation` FROM `t_source_message` LEFT JOIN `t_message` ON  `t_source_message`.`id` = `t_message`.`id` WHERE (`t_message`.`language` = 'it'))
union 
(SELECT DISTINCT `t_source_message`.`id`, `t_source_message`.`message`, NULL FROM `t_source_message` LEFT JOIN `t_message` ON  `t_source_message`.`id` = `t_message`.`id` WHERE (SELECT count(id) from t_message where `t_message`.`language` <> 'it' > 0 )) 
ORDER BY `id` LIMIT 20
;
*/

        $query1 = (new \yii\db\Query)
                ->select(['{{t_source_message}}.[[id]] as id', '{{t_source_message}}.[[category]]', '{{t_source_message}}.[[message]]', '"' . $lang .  '"' . ' as [[language]]', 
                    '{{t_message}}.[[language]] as language_db',
                    '{{t_message}}.[[translation]]'])
                ->distinct()
                ->from('{{t_source_message}}')
                ->leftJoin('{{t_message}}', '{{t_source_message}}.[[id]] = {{t_message}}.[[id]]')
                ->where(['{{t_message}}.[[language]]' => null]);
        
        $query2 = (new \yii\db\Query)
                ->select(['{{t_source_message}}.[[id]]', '{{t_source_message}}.[[category]]', '{{t_source_message}}.[[message]]', '{{t_message}}.[[language]] as language', 
                            '{{t_message}}.[[language]] as language_db',
                            '{{t_message}}.[[translation]]'])
                ->distinct()
                ->from('{{t_source_message}}')
                ->leftJoin('{{t_message}}', '{{t_source_message}}.[[id]] = {{t_message}}.[[id]]')
                ->where(['{{t_message}}.[[language]]' => $lang]);

        $query3 = (new \yii\db\Query)
                ->select(['{{t_source_message}}.[[id]]', '{{t_source_message}}.[[category]]', '{{t_source_message}}.[[message]]', '"' . $lang .  '"' . ' as [[language]]', 
                            '{{t_message}}.[[language]] as language_db',
                            'NULL as [[translation]]'])
                ->distinct()
                ->from('{{t_source_message}}')
                ->leftJoin('{{t_message}}', '{{t_source_message}}.[[id]] = {{t_message}}.[[id]]')
                ->where('(SELECT count(id) from t_message where `t_message`.`language` <> "' . $lang . '" > 0 )');
        
        //$unionQuery = (new ActiveQ)

        

        if (isset($special) && $special === '-') {

/*
SELECT DISTINCT T.`id` AS `id`, T.`category`, T.`message`, "ro" as `language`, `t_message`.`language` AS `language_db`, `t_message`.`translation`		
FROM 
    `t_source_message` T
LEFT JOIN `t_message` ON T.`id` = `t_message`.`id`
WHERE 
(
    (`t_message`.`language` <> "ro" or `t_message`.`language`is NULL)
    AND NOT EXISTS (
	SELECT 1 FROM `t_message` WHERE `t_message`.`language` = "ro" AND `t_message`.`id` = T.`id`
    )
or (
    `t_message`.`language` = "ro" AND `t_message`.`translation` = ''
)
or (
    `t_message`.`translation` = '' IS NULL
);                
 */            
            //echo 'here';
            //die();
            $sub = (new \yii\db\Query)
                                ->select('id')
                                ->from('{{t_message}}')
                                ->where('{{t_message}}.[[language]] = :lang', [':lang' => $lang])
                                ->andWhere('{{t_message}}.[[id]] = T.id');
            
            $query1 = (new \yii\db\Query)
                    ->select(['T.`id` AS `id`, T.`category`, T.`message`, "' . $lang . '" as `language`, `t_message`.`language` AS `language_db`, NULL as [[translation]]'])
                    ->distinct()
                    ->from('{{t_source_message}} T')
                    ->leftJoin('{{t_message}}', 'T.[[id]] = {{t_message}}.[[id]]')
                    ->where('{{t_message}}.[[language]]<>:lang', [':lang'=>$lang])
                    ->andWhere(['not exists', $sub]);
            
            $query1->orWhere([
                'and',
                ['=', '{{t_message}}.[[language]]', $lang],
                ['=', '{{t_message}}.[[translation]]', ''],
                ]);
            
            $query1->orWhere(['{{t_message}}.[[translation]]'=>null]);

            $unionQuery = (new \yii\db\Query())
                ->from(['dummy_name' => $query1])
                ->groupBy(['id']);
        }
        else if (isset($special) && $special === '+') {
        /* asta merge pentru filtrarea alora existente */
            $unionQuery = (new \yii\db\Query())
                ->from(['dummy_name' => $query1->union($query2)->union($query3)])
                ->where('[[language]]=[[language_db]]')    
                ->groupBy(['id']);
            //->orderBy(['{{t_source_message}}.[[id]]' => SORT_ASC]);
        }
        else {
            //return all
            $unionQuery = (new \yii\db\Query())
                ->from(['dummy_name' => $query1->union($query2)->union($query3)])
                ->groupBy(['id']);
        }
        
        
        return $unionQuery;
        
    }
}
