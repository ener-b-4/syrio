<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\modules\translations\models;

/**
 * This is the ActiveQuery class for [[TSourceMessage]].
 *
 * @see TSourceMessage
 */
class TSourceMessageQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    public function lang($lang) {
        return $this->andWhere(['language' => $lang]);
    }
    
    /**
     * @inheritdoc
     * @return TSourceMessage[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return TSourceMessage|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}