<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\modules\translations\models;

use Yii;

/**
 * This is the model class for table "t_source_message".
 *
 * @property integer $id
 * @property string $category
 * @property string $message
 *
 * @property TMessage[] $translations
 */
class TSourceMessage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 't_source_message';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['message', 'category'], 'required'],
            //[['message', 'category'], 'unique', 'targetAttribute' => ['message', 'category']],
            [['message'], 'string'],
            [['category'], 'string', 'max' => 32]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category' => ucfirst(Yii::t('app/translations', 'category')),
            'message' => ucfirst(Yii::t('app/translations', 'message')),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(TMessage::className(), ['id' => 'id']);
    }

    
    /**
     * @inheritdoc
     * @return TSourceMessageQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TSourceMessageQuery(get_called_class());
    }
}
