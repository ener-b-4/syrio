<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\modules\translations\models;

use Yii;

/**
 * This is the model class for table "t_message".
 *
 * @property integer $id
 * @property string $language
 * @property string $translation
 *
 * @property TSourceMessage $sourceMessage
 */
class TMessage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 't_message';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'language', 'translation'], 'required'],
            [['id'], 'integer'],
            [['translation'], 'string'],
            [['language'], 'string', 'max' => 16],
            [['ref'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => ucfirst(Yii::t('app/translations', 'source message')),
            'language' => ucfirst(Yii::t('app', 'language')),
            'translation' => ucfirst(Yii::t('app/translations', 'translation')),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSourceMessage()
    {
        return $this->hasOne(TSourceMessage::className(), ['id' => 'id']);
    }

    
    public $ref;
    
    /**
     * @inheritdoc
     * @return TMessageQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TMessageQuery(get_called_class());
    }
}
