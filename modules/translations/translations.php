<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\modules\translations;

use yii\base\Module;

/**
 * Description of CpfBuilder
 *
 * @author vamanbo
 */
class Translations extends Module
{
    public $controllerNamespace = 'app\modules\translations\controllers';

    public function init()
    {
        parent::init();
    }
    
    
}
