<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\modules\translations\controllers;

use Yii;
use app\modules\translations\models\TSourceMessage;
use app\modules\translations\models\TSourceMessageSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use app\models\common\ActionMessage;

/**
 * TSourceMessageController implements the CRUD actions for TSourceMessage model.
 */
class TSourceMessageController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'del_translation' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all TSourceMessage models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TSourceMessageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TSourceMessage model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TSourceMessage model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TSourceMessage();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $actionMessage = new ActionMessage();
            $actionMessage->SetSuccess(
                    Yii::t('app', 'Add {evt}', ['evt' => \Yii::t('app/translations', 'source message')]), 
                    Yii::t('app', '\'{evt}\' has been successfully {evt2}.', ['evt'=>$model->message, 'evt2'=>\Yii::t('app', 'added')]));
            Yii::$app->session->set('finished_action_result', $actionMessage);
            
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing TSourceMessage model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $actionMessage = new ActionMessage();
            $actionMessage->SetSuccess(
                    ucfirst(Yii::t('app', 'modify {evt}', ['evt' => \Yii::t('app/translations', 'source message')])), 
                    Yii::t('app', '{evt} has been successfully modified.', ['evt'=>$model->message]));
            Yii::$app->session->set('finished_action_result', $actionMessage);
            
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TSourceMessage model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->delete();

        $actionMessage = new ActionMessage();
        $actionMessage->SetWarning(
                Yii::t('app', 'Delete {evt}', ['evt' => \Yii::t('app/translations', 'source message')]), 
                Yii::t('app', '\'{evt}\' has been successfully {evt2}.', ['evt'=>$model->message, 'evt2'=>\Yii::t('app', 'deleted')]));
        Yii::$app->session->set('finished_action_result', $actionMessage);
        
        return $this->redirect(['index']);
    }

    /**
     * Finds the TSourceMessage model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TSourceMessage the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TSourceMessage::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(\Yii::t('app', 'The requested page does not exist.'));
        }
    }
    
    
    public function actionTranslations($lang) {
        if (!isset($lang)) {
            throw new NotFoundHttpException(\Yii::t('app', 'The requested page does not exist.'));
        }
        
        /* get the registered languages */
        $reg_langs = \app\modules\translations\models\TMessage::find()
                ->asArray()
                ->distinct(true)
                ->select('language')
                ->all();
        
        if (!\app\models\shared\GenericHelpers::recursiveInArray($reg_langs, $lang)) {
            throw new NotFoundHttpException(\Yii::t('app', 'The requested language is not registered'));
        }
        
        
        $searchModel = new \app\modules\translations\models\TTranslationSearch();
        $searchModel->language = $lang;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('translations', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    
    
    public function actionEdit_translation($lang, $src_id) {
        if (!isset($lang) || !isset($src_id)) {
            throw new NotFoundHttpException(\Yii::t('app', 'The requested page does not exist.'));
        }
        
        /* get the registered languages */
        $reg_langs = \app\modules\translations\models\TMessage::find()
                ->asArray()
                ->distinct(true)
                ->select('language')
                ->all();
        
        if (!\app\models\shared\GenericHelpers::recursiveInArray($reg_langs, $lang)) {
            throw new NotFoundHttpException(\Yii::t('app', 'The requested language is not registered'));
        }
        
        /*
         * try to get the TMessage model
         */
        $model = \app\modules\translations\models\TMessage::find()
                ->where(['id'=>$src_id])
                ->andWhere(['language'=>$lang])
                ->one();
        
        if (\Yii::$app->request->post())
        {
            echo 'post';
        }
        
        if (!isset($model)) {
            $url = \yii\helpers\Url::to(['/translation/t-message/create', 'language'=>$lang, 'id'=>$src_id]);
        } else {
            $url = \yii\helpers\Url::to(['/translation/t-message/update', 'language'=>$lang, 'id'=>$src_id]);
        }
        
        return $this->redirect($url);
        
        //return $this->render('edit_translation', ['model'=>$model]);
    }
    

    public function actionReset_translation($lang, $src_id) {
        if (!isset($lang) || !isset($src_id)) {
            throw new NotFoundHttpException(\Yii::t('app', 'The requested page does not exist.'));
        }
        
        /* get the registered languages */
        $reg_langs = \app\modules\translations\models\TMessage::find()
                ->asArray()
                ->distinct(true)
                ->select('language')
                ->all();
        
        if (!\app\models\shared\GenericHelpers::recursiveInArray($reg_langs, $lang)) {
            throw new NotFoundHttpException(\Yii::t('app', 'The requested language is not registered'));
        }
        
        /*
         * try to get the TMessage model
         */
        $model = \app\modules\translations\models\TMessage::find()
                ->where(['id'=>$src_id])
                ->andWhere(['language'=>$lang])
                ->one();

        if (!isset($model)) {
            throw new NotFoundHttpException(\Yii::t('app', 'The requested page does not exist.'));
        }
        $text = $model->sourceMessage->message;
        
        \app\modules\translations\models\TMessage::deleteAll([
            'id'=>$src_id,
            'language'=>$lang
        ]);
        
        $actionMessage = new ActionMessage();
        $actionMessage->SetSuccess(
                Yii::t('app', 'Reset {evt}', ['evt' => \Yii::t('app/translations', 'translation')]), 
                Yii::t('app', 'Translation of \'{evt}\' has been reset.', [
                    'evt' => $text
                ]));
        Yii::$app->session->set('finished_action_result', $actionMessage);
        
        if (isset($model->ref)) {
            return $this->redirect($model->ref);
        }
        return $this->redirect(['translations', 'lang'=>$model->language]);
        
    }
    
    
    public function actionDel_translation($lang) {
        if (!isset($lang)) {
            throw new NotFoundHttpException(\Yii::t('app', 'The requested page does not exist.'));
        }
        
        /* get the registered languages */
        $reg_langs = \app\modules\translations\models\TMessage::find()
                ->asArray()
                ->distinct(true)
                ->select('language')
                ->all();
        
        if (!\app\models\shared\GenericHelpers::recursiveInArray($reg_langs, $lang)) {
            throw new \yii\web\BadRequestHttpException(\Yii::t('app', 'The requested language could not be found'));
        }
        
        
        \app\modules\translations\models\TMessage::deleteAll('language = :lang', [':lang'=>$lang]);
        
        $actionMessage = new ActionMessage();
        $actionMessage->SetSuccess(
                Yii::t('app', 'Remove {evt}!', ['evt' => \Yii::t('app', 'Language')]), 
                Yii::t('app', 'Language \'{evt}\' removed.', [
                    'evt' => $lang
                ]));
        Yii::$app->session->set('finished_action_result', $actionMessage);
        
        return $this->redirect(['/translation']);
        
    }
    
}
