<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\modules\translations\controllers;
use app\components\Errors;

use yii\web\Controller;
use Yii;

class DefaultController extends Controller
{
    /**
     * Add this to ensure that only the authenticated users have access to this
     * @param type $action
     * @return boolean
     * 
     * @version 1.0.20151009
     * @author vamanbo <john.doe@example.com>
     */
    public function beforeAction($action) {
        
        if (!\Yii::$app->user->can('translate_app'))
        {
            return \Yii::$app->error->ThrowError(Errors::RESTRICTED_AREA_ERROR);
            return FALSE;
        }
        return parent::beforeAction($action);        
    }
    
    
    public function behaviors()
    {
        
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                //only authenticated users
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ]
            ],
        ];
    }
    
    public function actionIndex()
    {
        
        //echo 'here';
        //die();
        
        $stats = new \app\modules\translations\models\statistics\LanguagesStatistics();
        
        return $this->render('index', ['stats'=>$stats]);

        //$url = Yii::$app->urlManager->createUrl(['cpfbuilder/cpf-sessions']);
        //return $this->redirect($url);
        //return $this->render('index');
    }
    
}
