<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\modules\translations\controllers;

use Yii;
use app\modules\translations\models\TMessage;
use app\modules\translations\models\TMessageSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TMessageController implements the CRUD actions for TMessage model.
 */
class TMessageController extends Controller
{

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all TMessage models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TMessageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TMessage model.
     * @param integer $id
     * @param string $language
     * @return mixed
     */
    public function actionView($id, $language)
    {
        return $this->render('view', [
            'model' => $this->findModel($id, $language),
        ]);
    }

    /**
     * Creates a new TMessage model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id, $language)
    {
        $model = new TMessage();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if (isset($model->ref)) {
                return $this->redirect($model->ref);
            }
            return $this->redirect(['view', 'id' => $model->id, 'language' => $model->language]);
        } else {
            $model->id = $id;
            $model->language = $language;
            $model->ref = Yii::$app->request->referrer;
            
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing TMessage model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @param string $language
     * @return mixed
     */
    public function actionUpdate($id, $language)
    {
        $model = $this->findModel($id, $language);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if (isset($model->ref)) {
                return $this->redirect($model->ref);
            }
            return $this->redirect(['view', 'id' => $model->id, 'language' => $model->language]);
        } else {
            $model->ref = Yii::$app->request->referrer;
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TMessage model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @param string $language
     * @return mixed
     */
    public function actionDelete($id, $language)
    {
        $this->findModel($id, $language)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TMessage model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @param string $language
     * @return TMessage the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id, $language)
    {
        if (($model = TMessage::findOne(['id' => $id, 'language' => $language])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    
    /**
     * This registers a new language
     */
    public function actionNew_language()
    {
        $model = new TMessage;
        
        if ($model->load(\Yii::$app->request->post())) {
            if (!$model->validate()) {
                echo 'not validated';
                die();
            }
            
            //set the langeage as small caps
            $model->language = strtolower($model->language);
            
            $model->save();
            if (isset($model->ref)) {
                return $this->redirect($model->ref);
            }
            return $this->redirect(['view', 'id' => $model->id, 'language' => $model->language]);
        } else {
            
            //get the registered messages
            $messages = \yii\helpers\ArrayHelper::map(\app\modules\translations\models\TSourceMessage::find()
                    ->orderBy('message')
                    ->all(), 'id', 'message');
            
            $countries = \yii\helpers\ArrayHelper::map(\app\modules\management\models\Country::find()
                    ->orderBy('short_name')->all(), 'iso2', 'short_name');
            
            $model->ref = Yii::$app->request->referrer;
            return $this->render('new_language', [
                'model' => $model,
                'messages' => $messages,
                'countries' => $countries
            ]);
        }
    }
    
}
