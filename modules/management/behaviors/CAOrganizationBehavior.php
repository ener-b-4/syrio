<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\modules\management\behaviors;


use Yii;
use app\modules\management\models\CompetentAuthority;
use app\modules\management\models\IOrganization;
use app\models\Installations;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CAOrganizationBehavior
 *
 * @author vamanbo
 */
class CAOrganizationBehavior extends    \yii\base\Behavior 
                             implements IOrganization {
    //put your code here
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompetentAuthority()
    {
        return $this->hasOne(CompetentAuthority::className(), ['country' => 'country']);
    }
    
    public function setOrganizationType($value)
    {
        //return \app\modules\management\models\Organization::COMPETENT_AUTHORITY;
    }

    public function getOrganizationType()
    {
        return \app\modules\management\models\Organization::COMPETENT_AUTHORITY;
    }

    
    /**
     * Counts the installations in this Competent Authority
     * 
     * @param array $filter should contain ['status'=> AND/OR 'op_status'=>]
     * @return integer the number of Installations registered in this Competent Authority
     * @throws \yii\web\NotAcceptableHttpException if $filer is scalar
     * 
     * @author Bogdan Vamanu <bogdan.vamanu@jrc.ec.europa.eu>
     */
    public function getNumberOfInstallations($filter)
    {
        $query = Installations::find();
        
        if (isset($filter))
        {
            if (!is_array($filter))
            {
                throw new \yii\web\NotAcceptableHttpException('$filter must be array. Scalar provided');
            }
            
            $query = Installations::find();
            
            if (key_exists('status', $filter)) {
                $query->andFilterWhere(['status' => $filter['status']]);
            }
            if (key_exists('op_status', $filter)) {
                $query->andFilterWhere(['op_status' => $filter['op_status']]);
            }
        }
        return $query->count();
    }
    
}
