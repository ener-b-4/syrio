<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\modules\management\behaviors;


use Yii;
use \app\models\Installations;
use app\models\events\EventDeclaration;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of OOOrganizationBehavior
 *
 * @author vamanbo
 */
class OOOrganizationBehavior extends \yii\base\Behavior {
    //put your code here
    
    public function getOrganizationType()
    {
        return \app\modules\management\models\Organization::OPERATORS_OWNERS;
    }
    
    /**
     * Counts the installations in this Organization
     * 
     * @param array $filter should contain ['status'=> AND/OR 'op_status'=>]
     * @return integer the number of Installations of this Operators/Owners organization
     * @throws \yii\web\NotAcceptableHttpException if $filer is scalar
     * 
     * @author Bogdan Vamanu <bogdan.vamanu@jrc.ec.europa.eu>
     */
    public function getNumberOfInstallations($filter)
    {
        $query = Installations::find();
        $query->where(['operator_id'=> $this->owner->id]);
        
        if (isset($filter))
        {
            if (!is_array($filter))
            {
                throw new \yii\web\NotAcceptableHttpException('$filter must be array. Scalar provided');
            }
            
            $query = Installations::find();
            
            if (key_exists('status', $filter)) {
                $query->andFilterWhere(['status' => $filter['status']]);
            }
            if (key_exists('op_status', $filter)) {
                $query->andFilterWhere(['op_status' => $filter['op_status']]);
            }
        }
        return $query->count();
    }
    
    public function getEvents($year = null, $all = NULL) {
        $query = EventDeclaration::find()
                ->where(['operator_id'=>$this->owner->id]);
        
        if (isset($year) && intval($year)>0) {
            $query->andWhere(['=', 'year(event_date_time)', $year]);
        }
        
        if (isset($all) || $all>0) {
            $query->andWhere(['del_status'=>10]);
        }
        
        if (Yii::$app->user->identity->isInstallationUser) {
            $query->andWhere(['installation_id'=>Yii::$app->user->identity->inst_id]);
        }
        
        
        return $query->all();
    }
    
}
