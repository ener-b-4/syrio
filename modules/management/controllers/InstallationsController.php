<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\modules\management\controllers;

use Yii;
use app\models\Installations;
use app\models\InstallationsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\components\Errors;
use app\modules\management\models\Organization;
use \app\models\InstallationTypes;
use \app\modules\management\models\Country;
use \app\models\common\ActionMessage;

/**
 * InstallationsController implements the CRUD actions for Installations model.
 */
class InstallationsController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                //only authenticated users
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Installations models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (!Yii::$app->user->can('oo-inst-list'))
        {
            Yii::$app->error->ThrowError(Errors::RESTRICTED_AREA_ERROR);
        }

        $searchModel = new InstallationsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Installations model.
     * @param string $id
     * @param string $operator_id
     * @return mixed
     */
    public function actionView($id, $operator_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id, $operator_id),
        ]);
    }

    /**
     * Creates a new Installations model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * 
     * 'sys_admin'  can create installations for ANY registered company
     * 'ca_admin'   can create installations for ANY registered company
     * 'op_admin'   can create installations ONLY for his Company (Organization)
     * 
     * @author Bogdan Vamanu <bogdan.vamanu@jrc.ec.europa.eu>
     * @return mixed
     */
    public function actionCreate()
    {
        if (Yii::$app->user->isGuest)
        {
            return Yii::$app->error->ThrowError(Errors::RESTRICTED_AREA_ERROR);
        }
        if (Yii::$app->user->can('sys_admin') || Yii::$app->user->can('ca_admin'))
        {
            $model = new Installations();
        }
        elseif (Yii::$app->user->can('op_admin'))
        {
            $model = new Installations();
            $model->operator_id = Yii::$app->user->identity->organization->id;
        }
        else
        {
            return Yii::$app->error->ThrowError(Errors::RESTRICTED_OPERATION_ERROR);
        }

        $organizations = Organization::getOperatorOrganizations()->all();
        $installation_types = \yii\helpers\ArrayHelper::map(InstallationTypes::find()->all(), 'type', 
                function($data) { 
                    return $data->type . ' - ' . $data->name; 
                }); 
        $countries = \yii\helpers\ArrayHelper::map(Country::find()->all(), 'iso2', 'short_name');

        if ($model->load(Yii::$app->request->post())) {
            
            /* create an id */
            $model->id = Installations::GenerateId();
            $model->created_by = Yii::$app->user->id;
            $model->created_at = time();
            $model->status = Installations::STATUS_ACVTIVE;
            if ($model->validate())
            {
                $model->save();
                return $this->redirect(['view', 'id' => $model->id, 'operator_id' => $model->operator_id]);
            }
            else
            {
                return $this->render('create', [
                    'model' => $model,
                    'organizations' => \yii\helpers\ArrayHelper::map($organizations, 'id', 'organization_acronym'),
                    'installation_types' => $installation_types,
                    'countries' => $countries
                ]);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
                'organizations' => \yii\helpers\ArrayHelper::map($organizations, 'id', 'organization_acronym'),
                'installation_types' => $installation_types,
                'countries' => $countries
            ]);
        }
    }

    /**
     * Creates a new Installations model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * 
     * 'sys_admin'  can create installations for ANY registered company
     * 'ca_admin'   can create installations for ANY registered company
     * 'op_admin'   can create installations ONLY for his Company (Organization)
     * 
     * @author Bogdan Vamanu <bogdan.vamanu@jrc.ec.europa.eu>
     * @return mixed
     */
    public function actionCreateFor($org_id)
    {
        if (Yii::$app->user->isGuest)
        {
            return Yii::$app->error->ThrowError(Errors::RESTRICTED_AREA_ERROR);
        }

        $organizations = Organization::getOperatorOrganizations()
                ->where(['id'=>$org_id])->all();
        if (!isset($organizations))
        {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        if (Yii::$app->user->can('sys_admin') || Yii::$app->user->can('ca_admin'))
        {
            $model = new Installations();
            $model->operator_id = $org_id;
        }
        elseif (Yii::$app->user->can('op_admin') && $org_id == Yii::$app->user->identity->organization->id)
        {
            $model = new Installations();
            $model->operator_id = Yii::$app->user->identity->organization->id;
        }
        else
        {
            return Yii::$app->error->ThrowError(Errors::RESTRICTED_OPERATION_ERROR);
        }

        
        
        $installation_types = \yii\helpers\ArrayHelper::map(InstallationTypes::find()->all(), 'type', 
                function($data) { 
                    return $data->type . ' - ' . $data->name; 
                }); 
        $countries = \yii\helpers\ArrayHelper::map(Country::find()->all(), 'iso2', 'short_name');

        if ($model->load(Yii::$app->request->post())) {
            
            /* create an id */
            $model->id = Installations::GenerateId();
            $model->created_by = Yii::$app->user->id;
            $model->created_at = time();
            $model->status = Installations::STATUS_ACVTIVE;
            if ($model->validate())
            {
                $model->save();
                return $this->redirect(['view', 'id' => $model->id, 'operator_id' => $model->operator_id]);
            }
            else
            {
                return $this->render('create', [
                    'model' => $model,
                    'organizations' => \yii\helpers\ArrayHelper::map($organizations, 'id', 'organization_acronym'),
                    'installation_types' => $installation_types,
                    'countries' => $countries
                ]);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
                'organizations' => \yii\helpers\ArrayHelper::map($organizations, 'id', 'organization_acronym'),
                'installation_types' => $installation_types,
                'countries' => $countries
            ]);
        }
    }
    
    
    
    /**
     * Updates an existing Installations model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @param string $operator_id
     * @return mixed
     */
    public function actionUpdate($id, $operator_id)
    {
        if (Yii::$app->user->isGuest)
        {
            return Yii::$app->error->ThrowError(Errors::RESTRICTED_AREA_ERROR);
        }
        if (Yii::$app->user->can('sys_admin') || Yii::$app->user->can('ca_admin'))
        {
            $model = new Installations();
        }
        elseif (Yii::$app->user->can('op_admin'))
        {
            $model = new Installations();
            $model->operator_id = Yii::$app->user->identity->organization->id;
        }
        else
        {
            return Yii::$app->error->ThrowError(Errors::RESTRICTED_OPERATION_ERROR);
        }
        
        $model = $this->findModel($id, $operator_id);

        $organizations = Organization::getOperatorOrganizations()->all();
        $installation_types = \yii\helpers\ArrayHelper::map(InstallationTypes::find()->all(), 'type', 
                function($data) { 
                    return $data->type . ' - ' . $data->name; 
                }); 
        $countries = \yii\helpers\ArrayHelper::map(Country::find()->all(), 'iso2', 'short_name');
        if ($model->load(Yii::$app->request->post()))
        {
            $model->modified_by = Yii::$app->user->id;
            $model->modified_at = time();
            if ($model->validate())
            {
                $model->save();
                return $this->redirect(['view', 'id' => $model->id, 'operator_id' => $model->operator_id]);
            }
            else
            {
                return $this->render('update', [
                    'model' => $model,
                    'organizations' => \yii\helpers\ArrayHelper::map($organizations, 'id', 'organization_acronym'),
                    'installation_types' => $installation_types,
                    'countries' => $countries
                ]);
            }
            return $this->redirect(['view', 'id' => $model->id, 'operator_id' => $model->operator_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'organizations' => \yii\helpers\ArrayHelper::map($organizations, 'id', 'organization_acronym'),
                'installation_types' => $installation_types,
                'countries' => $countries
            ]);
        }
    }

    
    public function actionDelete($id, $operator_id)
    {
        if (Yii::$app->user->isGuest)
        {
            return Yii::$app->error->ThrowError(Errors::RESTRICTED_AREA_ERROR);
        }
        if (Yii::$app->user->can('sys_admin') || Yii::$app->user->can('ca_admin') || Yii::$app->user->can('op_admin'))
        {
            $inst = $this->findModel($id, $operator_id);
            $inst->status = Installations::STATUS_DELETED;
            $inst->save();

            $actionMessage = new ActionMessage();
            $actionMessage->SetSuccess(
                    Yii::t('app', 'Installation {evt} has been deleted!', ['evt' => $inst->name]), 
                    Yii::t('app', 'You may undo the operation or purge if you want it completely removed from SyRIO.'));
            Yii::$app->session->set('finished_action_result', $actionMessage);

            return $this->redirect(['index']);
        }
        else
        {
            return Yii::$app->error->ThrowError(Errors::RESTRICTED_OPERATION_ERROR);
        }
    }
    
    
    /**
     * Reactivates an installation
     * 
     * @param integer $id
     * @param string $operator_id
     * @return Installations index if succeeded
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionReactivate($id, $operator_id)
    {
        if (Yii::$app->user->isGuest)
        {
            return Yii::$app->error->ThrowError(Errors::RESTRICTED_AREA_ERROR);
        }
        elseif (Yii::$app->user->can('sys_admin') || Yii::$app->user->can('installation-purge'))
        {
            $model = $this->findModel($id, $operator_id);
            if (!isset($model)) {
                throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
            }
            $model->status = Installations::STATUS_ACVTIVE;
            $model->save();
            $msg = Yii::t('app', 'Intallation {evt} has been successfully reactivated.', [
                'evt' => $model->name,
            ]); 

            $actionMessage = new ActionMessage();
            
            $actionMessage->SetSuccess(Yii::t('app', 'Reactivate installation'), $msg);
            Yii::$app->session->set('finished_action_result', $actionMessage);
            
            $searchModel = new InstallationsSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
        else
        {
            return Yii::$app->error->ThrowError(Errors::RESTRICTED_OPERATION_ERROR);
        }
    }
    
    /**
     * Purges (permanently deletes) an existing Installations model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @param string $operator_id
     * @return mixed
     */
    public function actionPurge($id, $operator_id)
    {
        if (Yii::$app->user->isGuest)
        {
            return Yii::$app->error->ThrowError(Errors::RESTRICTED_AREA_ERROR);
        }
        elseif (Yii::$app->user->can('sys_admin') || Yii::$app->user->can('installation-purge'))
        {        
            $this->findModel($id, $operator_id)->delete();
            return $this->redirect(['index']);
        }
        else
        {
            return Yii::$app->error->ThrowError(Errors::RESTRICTED_OPERATION_ERROR);
        }
        
    }

    /**
     * Finds the Installations model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @param string $operator_id
     * @return Installations the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id, $operator_id)
    {
        if (($model = Installations::findOne(['id' => $id, 'operator_id' => $operator_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
