<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\modules\management\controllers;

use Yii;
use yii\filters\VerbFilter;
use app\modules\management\models\Organization;
use app\modules\management\models\Address;
use kartik\form\ActiveForm;
use yii\web\NotFoundHttpException;
use app\components\Errors;

/**
 * Description of OperatorsOwnersOrganizationController
 *
 * @author vamanbo
 */
class OperatorsOwnersOrganizationController extends \yii\web\Controller {
    
    
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                //only authenticated users
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'create-user' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Creates a new Operators/OwnersOrganization model.
     * If creation is successful, the browser will be redirected to the 'organizations' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (!Yii::$app->user->can('oo-org-create'))
        {
            return Yii::$app->error->ThrowError(Errors::RESTRICTED_OPERATION_ERROR);
        }
        
        
        $model = new Organization([
            'organizationType' => 'oo'
        ]);
        $address = new Address();
        
        if (Yii::$app->request->isAjax && 
                $model->load(Yii::$app->request->post()) && 
                $address->load(Yii::$app->request->post())) {
            
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            //return ActiveForm::validateMultiple([$model, $address]);
            ActiveForm::validate($model);
            ActiveForm::validate($address);
            
            foreach($address->errors as $key=>$value)
            {
                $model->addError($key, $value);
            }
            
            $result = [];
            foreach ($model->getErrors() as $attribute => $errors) {
                $result[\yii\helpers\Html::getInputId($model, $attribute)] = $errors;
            }            
            return $result;
        }
        else if (
                !Yii::$app->request->isAjax &&
                $model->load(Yii::$app->request->post()) && 
                $address->load(Yii::$app->request->post())) {

            $flag = 0;
            $transaction = Yii::$app->db->beginTransaction();
            
            try {
                
                //second save the address
                //get the organization country from the post variable Organization[country]
                $org_country = \kartik\helpers\Html::encode(Yii::$app->request->post()['Organization']['country']);
                $address->country = $org_country;
                if (!$address->validate())
                {
                    $flag = 1;                    
                }
                else
                {
                    $address->save();   //here I have the address id
                }
                
                //third the Organization
                
                /* create the id for the OO */
                $date = new \DateTime();
                $ca_id = 'OO' . $date->getTimestamp();
                
                $model->id = $ca_id;
                $model->organization_address = $address->id;
                $model->created_by = Yii::$app->user->id;
                $model->created_at = date('Y-m-h H:i:s');
                
                //(start) Author: vamanbo   Date: 24.08.2015  - on BUG 14: set the status as STATUS_ACTIVE
                $model->status = Organization::STATUS_ACTIVE;
                //(end) Author: vamanbo   Date: 24.08.2015  - on BUG 14: set the status as STATUS_ACTIVE
                
                if (!$model->validate())
                {
                    $flag = 1;                    
                }
                else
                {
                    $model->save();   //here I have the organization id
                }
                
            } catch (Exception $ex) {
                $flag = 1;
            }
            
            if ($flag==1) {
                $transaction->rollBack();
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ActiveForm::validateMultiple([$model, $address]);
            }
            else
            {
                $transaction->commit();
                //return $this->redirect(['view', 'id' => $model->id]);
                return $this->redirect(Yii::$app->urlManager->createUrl(['/management/organization/index']));
                
            }        
        } else {
            return $this->render('create', [
                'model' => $model,
                'address' => $address
            ]);
        }
    }
    
    
    /**
     * 
     * @param app\modules\management\models\Organization $model
     */
    public function actionView($id)
    {
        $model=$this->findModel($id);
        return $this->render('view', ['model'=>$model]);
        die();
    }
    
    
    public function actionUpdate($id)
    {
        if (Yii::$app->user->isGuest)
        {
            return Yii::$app->error->ThrowError(\app\components\Errors::RESTRICTED_AREA_ERROR);
        }
        elseif (!Yii::$app->user->can('oo-org-update'))
        {
            return Yii::$app->error->ThrowError(\app\components\Errors::RESTRICTED_OPERATION_ERROR);
        }
        
        $model=$this->findModel($id);
        $address = $model->address;
        
        
        if (Yii::$app->request->isAjax && 
                $model->load(Yii::$app->request->post()) && 
                $address->load(Yii::$app->request->post())) {
            
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            //return ActiveForm::validateMultiple([$model, $address]);
            ActiveForm::validate($model);
            ActiveForm::validate($address);
            
            foreach($address->errors as $key=>$value)
            {
                $model->addError($key, $value);
            }
            
            $result = [];
            foreach ($model->getErrors() as $attribute => $errors) {
                $result[\yii\helpers\Html::getInputId($model, $attribute)] = $errors;
            }            
            return $result;
        }
        else if (!Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()) && 
                $address->load(Yii::$app->request->post())) {

            $flag = 0;
            $transaction = Yii::$app->db->beginTransaction();
            
            try {
                
                //second save the address
                //get the organization country from the post variable Organization[country]
                $org_country = \kartik\helpers\Html::encode(Yii::$app->request->post()['Organization']['country']);
                $address->country = $org_country;
                if (!$address->validate())
                {
                    $flag = 1;                    
                }
                else
                {
                    $address->save();   //here I have the address id
                }
                
                //third the Organization
                
                /* create the id for the OO */
                //(start) Author: vamanbo   Date: 11.11.2015  - on BUG 83: unable to update Organization
                //$date = new \DateTime();
                //$ca_id = 'OO' . $date->getTimestamp();
                //$model->id = $ca_id;
                //$model->organization_address = $address->id;
                //(end) Author: vamanbo   Date: 11.11.2015  - on BUG 83: unable to update Organization

                $model->modified_by = Yii::$app->user->id;
                $model->modified_at = date('Y-m-h H:i:s');
                
                //(start) Author: vamanbo   Date: 24.08.2015  - on BUG 14: set the status as STATUS_ACTIVE
                //$model->status = Organization::STATUS_ACTIVE;
                //(end) Author: vamanbo   Date: 24.08.2015  - on BUG 14: set the status as STATUS_ACTIVE
                
                if (!$model->validate())
                {
                    $flag = 1;                    
                }
                else
                {
                    $model->save();   //here I have the organization id
                }
                
            } catch (Exception $ex) {
                $flag = 1;
            }
            
            if ($flag==1) {
                $transaction->rollBack();
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ActiveForm::validateMultiple([$model, $address]);
            }
            else
            {
                $transaction->commit();
                //return $this->redirect(['view', 'id' => $model->id]);
                //return $this->redirect(Yii::$app->urlManager->createUrl(['/management/organization/index']));
                return $this->render('view', ['model'=>$model]);
            }        
        }
        else 
        {
            return $this->render('update', ['model'=>$model]);
        }
    }
    
    
    
    /**
     * Finds the OOOrganization model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Organizations the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Organization::findOne(['id' => $id])) !== null) {
            if ($model->organizationType!==Organization::OPERATORS_OWNERS)
            {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
}
