<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\modules\management\controllers;

use Yii;
use \app\modules\management\models\OrganizationSearch;
use \app\components\Errors;
use \app\modules\management\models\Organization;
use \app\models\common\ActionMessage;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of OrganizationController
 *
 * @author vamanbo
 */
class OrganizationController extends \yii\web\Controller {
    
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                //only authenticated users
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
    
    public function actionIndex()
    {
        if (Yii::$app->user->can('organization-list'))
        {
            $searchModel = new OrganizationSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
        else
        {
            Yii::$app->error->ThrowError(Errors::RESTRICTED_AREA_ERROR);
        }
    }
    
    public function actionCreate()
    {
        //(start) Author: vamanbo   Date: 24.08.2015  - restricted action redirrect
        if (Yii::$app->user->isGuest) {
            return Yii::$app->error->ThrowError(Errors::RESTRICTED_AREA_ERROR);
        }
        if (!Yii::$app->user->can('organization-create'))
        {
            return Yii::$app->error->ThrowError(Errors::RESTRICTED_OPERATION_ERROR);
        }
        //(end) Author: vamanbo   Date: 24.08.2015  - restricted action redirrect

        if (Yii::$app->user->can('sys_admin'))
        {
            //$model=new \app\modules\management\models\Organization();
            $model = new \app\modules\management\models\CreateOrganizationModel();
            if ($model->load(Yii::$app->request->post())) {
                if ($model->organization_type == 1) {
                    //ca organization
                    $model=new \app\modules\management\models\Organization([
                        'organizationType'=>'ca',
                    ]);
                    $address = new \app\modules\management\models\Address();
                    return $this->redirect(Yii::$app->urlManager->createUrl(['/management/competent-authority-organization/create']));
                }
                elseif ($model->organization_type == 2) {
                    //oo organization
                    $model=new \app\modules\management\models\Organization([
                        'organizationType'=>'ca',
                    ]);
                    $address = new \app\modules\management\models\Address();
                    return $this->redirect(Yii::$app->urlManager->createUrl(['/management/operators-owners-organization/create']));
                }
            }
            return $this->render('create', ['model'=>$model]);
        }
        elseif (Yii::$app->user->can('ca-org-create'))
        {
            $model=new \app\modules\management\models\Organization([
                'organizationType'=>'ca',
            ]);
            $address = new \app\modules\management\models\Address();
            return $this->redirect(Yii::$app->urlManager->createUrl(['management/competent-authority-organization/create']));
        }
        elseif (Yii::$app->user->can('oo-org-create'))
        {
            $model=new \app\modules\management\models\Organization([
                'organizationType'=>'oo',
            ]);
            $address = new \app\modules\management\models\Address();
            return $this->redirect(Yii::$app->urlManager->createUrl(['management/operators-owners-organization/create']));
        }
    }
    

    public function actionDelete($id)
    {
        if (Yii::$app->user->isGuest)
        {
            return Yii::$app->error->ThrowError(Errors::RESTRICTED_AREA_ERROR);
        }
        if (
                Yii::$app->user->can('sys_admin') || 
                Yii::$app->user->can('ca_admin') ||
                Yii::$app->user->can('op_admin'))
        {
            $model = $this->findModel($id);
            
            if ($model->organizationType == Organization::COMPETENT_AUTHORITY) {
                //die('delete org');
                $ncao = Organization::find()
                    ->where(['like', 'id', 'CA'])
                    ->andWhere(['status'=>10])
                    ->count();

                if ($ncao == 1) {
                    //throw error
                    return Yii::$app->error->ThrowWarning(Errors::WARN_LAST_CAO);
                }
            } else {
                if (!Yii::$app->user->can('oo-org-delete')) {
                    return Yii::$app->error->ThrowError(Errors::RESTRICTED_OPERATION_ERROR);
                }
            }
            
            
            
            $model->status = \app\modules\management\models\Organization::STATUS_DELETED;
            $model->modified_by = \Yii::$app->user->id;
            $model->modified_at = time();
            
            if ($model->save()){
                $actionMessage = new ActionMessage();
                $actionMessage->SetSuccess(
                        Yii::t('app', 'Organization {evt} has been deleted!', [
                            'evt' => $model->organization_name . '(' . $model->organization_acronym . ')',
                        ]), 
                        Yii::t('app', 'You may undo the operation or purge if you want it completely removed from SyRIO.'));
                Yii::$app->session->set('finished_action_result', $actionMessage);
            }
            else
            {
                $actionMessage = new ActionMessage();
                $actionMessage->SetSuccess(
                        Yii::t('app', 'Organization {evt} could not be deleted!', [
                            'evt' => $model->organization_name . '(' . $model->organization_acronym . ')',
                        ]), 
                        Yii::t('app', 'There\'s been an error while processing your request.') . ' '
                        . Yii::t('app', 'Please retry later.')
                        );
                Yii::$app->session->set('finished_action_result', $actionMessage);
            }


            return $this->redirect(['index']);
        }
        else
        {
            return Yii::$app->error->ThrowError(Errors::RESTRICTED_OPERATION_ERROR);
        }
    }
    
    
    /**
     * Reactivates an organization
     * 
     * @param integer $id
     * @param string $operator_id
     * @return Organization index if succeeded
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionReactivate($id)
    {
        if (Yii::$app->user->isGuest)
        {
            return Yii::$app->error->ThrowError(Errors::RESTRICTED_AREA_ERROR);
        }
        if (Yii::$app->user->can('sys_admin') || Yii::$app->user->can('organization-purge'))
        {
            $model = $this->findModel($id);
            if (!isset($model)) {
                throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
            }
            $model->status = Organization::STATUS_ACTIVE;
            $model->modified_by = \Yii::$app->user->id;
            $model->modified_at = time();
            $model->save();
            $msg = Yii::t('app', 'Organization {evt} has been successfully reactivated.', [
                'evt' => $model->organization_name . '(' . $model->organization_acronym . ')',
            ]); 

            $actionMessage = new ActionMessage();
            
            $actionMessage->SetSuccess(Yii::t('app', 'Reactivate organization'), $msg);
            Yii::$app->session->set('finished_action_result', $actionMessage);
            
            //$searchModel = new InstallationsSearch();
            //$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->redirect(['index']);
            //return $this->render('index', [
            //    'searchModel' => $searchModel,
            //    'dataProvider' => $dataProvider,
            //]);
        }
        else
        {
            return Yii::$app->error->ThrowError(Errors::RESTRICTED_OPERATION_ERROR);
        }
    }
    
    /**
     * Purges (permanently deletes) an existing Organization model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @param string $operator_id
     * @return mixed
     */
    //(start) Author: vamanbo   Date: 24.08.2015  - on BUG 22: remove the $operator_id parameter (end)
    public function actionPurge($id)
    {
        if (Yii::$app->user->isGuest)
        {
            return Yii::$app->error->ThrowError(Errors::RESTRICTED_AREA_ERROR);
        }
        if (Yii::$app->user->can('sys_admin') || Yii::$app->user->can('organization-purge'))
        {
            $model = $this->findModel($id);
            
            //(start) Author: vamanbo   Date: 24.08.2015  - on BUG 23: inform the user that the organization has been purged
            $actionMessage = new ActionMessage();
            $actionMessage->SetSuccess('Purge',
                    Yii::t('app', 'Organization {evt} has been purged (permanently deleted)!', [
                        'evt' => $model->organization_name . '(' . $model->organization_acronym . ')',
                    ]));
            Yii::$app->session->set('finished_action_result', $actionMessage);
            //(end) Author: vamanbo   Date: 24.08.2015  - on BUG 23: inform the user that the organization has been purged

            $model->delete();
             
            return $this->redirect(['index']);
        }
        else
        {
            return Yii::$app->error->ThrowError(Errors::RESTRICTED_OPERATION_ERROR);
        }
    }
    
    
    /**
     * Redirects the browser to the appropriate organization description page.
     * 
     * @author Bogdan Vamanu <bogdan.vamanu@jrc.ec.europa.eu>
     * @version 24/08/2015, 25/08/2015
     * 
     * @param type $id
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        if (Yii::$app->user->isGuest) {
            return Yii::$app->error->ThrowError(Errors::RESTRICTED_AREA_ERROR);
        }
        
        $model=$this->findModel($id);
        if ($model->organizationType == Organization::COMPETENT_AUTHORITY) {
            return $this->redirect(Yii::$app->urlManager->createUrl(['management/competent-authority-organization/view', 'id'=>$id]));
        } elseif ($model->organizationType == Organization::OPERATORS_OWNERS)
        {
            return $this->redirect(Yii::$app->urlManager->createUrl(['management/operators-owners-organization/view', 'id'=>$id]));
        }
    }
    
    
    
    /**
     * Finds the Organization model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Organizations the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Organization::findOne(['id' => $id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
}

/*20150922*/