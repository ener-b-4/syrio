<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\modules\management\controllers;

use Yii;

use app\modules\management\models\Organization;
use \app\modules\management\models\Address;
use app\modules\management\models\CaoSearch;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use \app\components\Errors;
use yii\helpers\Url;

use kartik\form\ActiveForm;

/**
 * CompetentAuthorityOrganizationController implements the CRUD actions for CompetentAuthorityOrganization model.
 */
class CompetentAuthorityOrganizationController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                //only authenticated users
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'create-user' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all CompetentAuthorityOrganization models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CaoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    
    /**
     * Lists CompetentAuthorityOrganization models for a given Country.
     * SyRIO
     * @return mixed
     */
    public function actionListOrganizations($country)
    {
        $searchModel = new CaoSearch();
        $qParams = Yii::$app->request->queryParams;
        
        
        if (key_exists('CaoSearch', $qParams))
        {
            $qParams['CaoSearch']['country'] = $country;
        }
        else
        {
            //enforce country
            $qParams['CaoSearch'] = ['country' => $country];
        }
        
        /* test purposes */
        /*
        echo '<pre>';
        echo var_dump($qParams);
        echo '</pre>';
        die();
        */
        
        $dataProvider = $searchModel->search($qParams);

        return $this->render('index_country', [
            'country' => $country,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    /**
     * Displays a single CompetentAuthorityOrganization model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CompetentAuthorityOrganization model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (!(Yii::$app->user->can('sys_admin') || Yii::$app->user->can('ca_admin'))) {
            return Yii::$app->error->ThrowError(Errors::RESTRICTED_AREA_ERROR);
        }
        
        $model = new Organization();
        $address = new Address();
        $address->country = Yii::$app->params['competentAuthorityIso2'];
        
        if (Yii::$app->request->isAjax && 
                $model->load(Yii::$app->request->post()) && 
                $address->load(Yii::$app->request->post())) {
            
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            //return ActiveForm::validateMultiple([$model, $address]);
            ActiveForm::validate($model);
            ActiveForm::validate($address);
            
            foreach($address->errors as $key=>$value)
            {
                $model->addError($key, $value);
            }
            
            $result = [];
            foreach ($model->getErrors() as $attribute => $errors) {
                $result[\yii\helpers\Html::getInputId($model, $attribute)] = $errors;
            }            
            return $result;
        }
        else if ($model->load(Yii::$app->request->post()) && 
                $address->load(Yii::$app->request->post())) {

            $flag = 0;
            $transaction = Yii::$app->db->beginTransaction();
            
            try {
                
                //second save the address
                if (!$address->validate())
                {
                    $flag = 1;                    
                }
                else
                {
                    $address->save();   //here I have the address id
                }
                
                //third the Organization
                
                /* create the id for the CA */
                $date = new \DateTime();
                $ca_id = 'CA' . $date->getTimestamp();
                
                $model->id = $ca_id;
                $model->organization_address = $address->id;
                //(start) Author: vamanbo   Date: 25.08.2015  - on CA Organization CRUD: I need this next line because the validation rules of Organization (country cannot be blank!)
                $model->country = $address->country;
                //(end) Author: vamanbo   Date: 25.08.2015  - on CA Organization CRUD: I need this next line because the validation rules of Organization (country cannot be blank!)
                $model->created_by = Yii::$app->user->id;
                $model->created_at = date('Y-m-h H:i:s');

                //(start) Author: vamanbo   Date: 24.08.2015  - on BUG 14: set the status as STATUS_ACTIVE
                $model->status = Organization::STATUS_ACTIVE;
                //(end) Author: vamanbo   Date: 24.08.2015  - on BUG 14: set the status as STATUS_ACTIVE
                
                if (!$model->validate())
                {
                    $flag = 1;                    
                }
                else
                {
                    $model->save();   //here I have the organization id
                }
                
            } catch (Exception $ex) {
                $flag = 1;
            }
            
            if ($flag==1) {
                $transaction->rollBack();
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ActiveForm::validateMultiple([$model, $address]);
            }
            else
            {
                $transaction->commit();
                //return $this->redirect(['view', 'id' => $model->id]);
                return $this->redirect(Yii::$app->urlManager->createUrl(['/management/organization/index']));
                
            }        
        } else {
            $country = \app\modules\management\models\Country::findOne(['iso2'=>Yii::$app->params['competentAuthorityIso2']]);
            
            return $this->render('create', [
                'model' => $model,
                'address' => $address,
                'country_phone_call' => $country->calling_code,
                'country_domain' => $country->cctld
            ]);
        }
    }

    /**
     * Updates an existing CompetentAuthorityOrganization model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if (!(Yii::$app->user->can('sys_admin') || Yii::$app->user->can('ca_admin'))) {
            return Yii::$app->error->ThrowError(Errors::RESTRICTED_AREA_ERROR);
        }
        
        //(start) Author: vamanbo   Date: 25.08.2015  - on CA Organization CRUD: re-write findModel
        $model = $this->findModel($id);
        //(end) Author: vamanbo   Date: 25.08.2015  - on CA Organization CRUD: re-write findModel
        $address = $model->address;

        //(start) Author: vamanbo   Date: 25.08.2015  - on CA Organization CRUD: I need this next line because the validation rules of Organization (country cannot be blank!)
        $model->country = $address->country;
        //(end) Author: vamanbo   Date: 25.08.2015  - on CA Organization CRUD: I need this next line because the validation rules of Organization (country cannot be blank!)
        
        if (Yii::$app->request->isAjax && 
                $model->load(Yii::$app->request->post()) && 
                $address->load(Yii::$app->request->post())) {
            
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            //return ActiveForm::validateMultiple([$model, $address]);
            ActiveForm::validate($model);
            ActiveForm::validate($address);
            
            foreach($address->errors as $key=>$value)
            {
                $model->addError($key, $value);
            }
            
            $result = [];
            foreach ($model->getErrors() as $attribute => $errors) {
                $result[\yii\helpers\Html::getInputId($model, $attribute)] = $errors;
            }            
            return $result;
        }
        elseif ($model->load(Yii::$app->request->post()) && 
                $address->load(Yii::$app->request->post())) {

            $flag = 0;
            $transaction = Yii::$app->db->beginTransaction();
            
            try {
                
                //second save the address
                //(start) Author: vamanbo   Date: 25.08.2015  - on CA Organization CRUD: comment next line
                //$address->country = $model->country;
                //(end) Author: vamanbo   Date: 25.08.2015  - on CA Organization CRUD: comment next line

                if (!$address->validate())
                {
                    $flag = 1;                    
                }
                else
                {
                    $address->save();   //here I have the address id
                }
                
                //third the Organization                

                //(start) Author: vamanbo   Date: 25.08.2015  - on CA Organization CRUD: changes in this block
                //$model->organization_address = $address->id;
                $model->modified_by = Yii::$app->user->id;
                $model->modified_at = date('Y-m-h H:i:s');
                if (!$model->validate())
                {
                    $flag = 1;                    
                }
                else
                {
                    $model->save();   //here I have the organization id
                }
                //(end) Author: vamanbo   Date: 25.08.2015  - on CA Organization CRUD: changes in this block
            } catch (Exception $ex) {
                $flag = 1;
            }
            
            if ($flag==1) {
                $transaction->rollBack();
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ActiveForm::validateMultiple([$model, $address]);
            }
            else
            {
                $transaction->commit();
                return $this->redirect(['view', 'id' => $model->id]);
            }                        
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Finds the CompetentAuthorityOrganization model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return CompetentAuthorityOrganization the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Organization::findOne(['id' => $id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    
    /*
     * Additional SyRIO actions
     * 
     */
    
}
