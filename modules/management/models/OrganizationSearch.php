<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\modules\management\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\management\models\Organization;
use app\components\Errors;

/**
 * CaoSearch represents the model behind the search form about `app\modules\management\models\CompetentAuthorityOrganization`.
 */
class OrganizationSearch extends Organization
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'created_by', 'modified_by'], 'integer'],
            [['organization_name', 
                'organization_acronym', 
                'organization_phone', 
                'organization_email', 
                'organization_fax', 
                'organization_phone2', 
                'organization_web', 
                'created_at', 
                'modified_at',
                'global',
                'addressText',
                'organizationType',
                ], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        if (Yii::$app->user->can('organization-list'))
        {
            $query = Organization::find();
            if (Yii::$app->user->identity->isOperatorUser)
            {
                $query->where([
                                'or',
                                ['like', 'organizations.id', 'ca'],
                                ['organizations.id' => Yii::$app->user->identity->org_id],
                    ]);
            }
        }
        else
        {
            Yii::$app->error->ThrowError(Errors::RESTRICTED_AREA_ERROR);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'organization_acronym',
                'organization_name',
                'country' => [
                    'asc' => ['country' => SORT_ASC, 'street1' => SORT_ASC],
                    'desc' => ['country' => SORT_DESC, 'street1' => SORT_ASC],
                    'label' => Yii::t('app', 'Competent Authority')
                ],
                'addressText' => [
                    'asc' => ['country' => SORT_ASC, 'street1' => SORT_ASC],
                    'desc' => ['country' => SORT_DESC, 'street1' => SORT_ASC],
                ]
            ]
        ]);
        
        
        /* delete all the other params if global exists */
        if (key_exists('CaoSearch', $params)) {
            if (!key_exists('global', $params['CaoSearch'])) {
                $params['CaoSearch']['global'] = '';
            }
            if ($params['CaoSearch']['global'] !== '')
            {
                $array = $params['CaoSearch'];
                foreach($array as $key=>$value) {
                    if ($key !== 'global' && $key!== 'id' && $key!== 'search_mode') {$params['CaoSearch'][$key] = '';}
                }
            }
        }
        
        
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'modified_at' => $this->modified_at,
            'modified_by' => $this->modified_by,
        ]);        

        if (isset($this->global) && $this->global != '') {
            $query->join('left join', 'c_address', 'organizations.organization_address = c_address.id')
                ->join('left join', 'c_country', 'c_address.country = c_country.iso2');
            
            $query->andFilterWhere([
                'or',
                ['like', 'organization_name', $this->global],
                ['like', 'organization_acronym', $this->global],
                ['like', 'organization_phone', $this->global],
                ['like', 'organization_email', $this->global],
                ['like', 'organization_fax', $this->global],
                ['like', 'organization_phone2', $this->global],
                ['like', 'organization_web', $this->global],

                ['like', 'c_country.short_name', $this->global],
                ['like', 'c_address.street1', $this->global],
                ['like', 'c_address.street2', $this->global],
                ['like', 'c_address.zip', $this->global],
                ['like', 'c_address.city', $this->global],
                ['like', 'c_address.county', $this->global],
                ['like', 'c_address.country', $this->global],
                
                ['like', 'organizations.id', $this->organizationType]                
            ]);
        }
        else
        {
            $query->andFilterWhere(['like', 'organization_name', $this->organization_name])
                ->andFilterWhere(['like', 'organization_acronym', $this->organization_acronym])
                ->andFilterWhere(['like', 'organization_phone', $this->organization_phone])
                ->andFilterWhere(['like', 'organization_email', $this->organization_email])
                ->andFilterWhere(['like', 'organization_fax', $this->organization_fax])
                ->andFilterWhere(['like', 'organization_phone2', $this->organization_phone2])
                ->andFilterWhere(['like', 'organization_web', $this->organization_web]);
            
            $query->join('left join', 'c_address', 'organizations.organization_address = c_address.id')
                ->join('left join', 'c_country', 'c_address.country = c_country.iso2')
                ->andFilterWhere([
                    'or',
                    ['like', 'c_country.short_name', $this->addressText],
                    ['like', 'c_address.street1', $this->addressText],
                    ['like', 'c_address.street2', $this->addressText],
                    ['like', 'c_address.zip', $this->addressText],
                    ['like', 'c_address.city', $this->addressText],
                    ['like', 'c_address.county', $this->addressText],
                    ['like', 'c_address.country', $this->addressText],
                ]);
            
            $query->andFilterWhere(['like', 'organizations.id', $this->organizationType]);
        }

        
        return $dataProvider;
    }

    
    /*
     * SyRIO - extends the search model for global search
     * by: bv
     * created: 02 June 2014
     */
    
    private $_global;
    public function GetGlobal()
    {
        return $this->_global;
    }
    public function SetGlobal($value)
    {
        $this->_global = $value;
    }
    
    /**
     *
     * @var integer 
     */
    //public $search_mode;
    
    private $_address;  //address as string for the search object
    public function GetAddressText()
    {
        return $this->_address;
    }
    public function SetAddressText($value)
    {
        $this->_address = $value;
    }
    
    
    private $_organizationType;
    public function GetOrganizationType()
    {
        return $this->_organizationType;        
    }
    public function SetOrganizationType($value)
    {
        $this->_organizationType = $value;
    }
    
    
}
