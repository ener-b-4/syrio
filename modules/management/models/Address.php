<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\modules\management\models;

use Yii;
use app\modules\management\models\Country;

/**
 * This is the model class for table "c_address".
 *
 * @property integer $id
 * @property string $street1
 * @property string $street2
 * @property string $city
 * @property string $county
 * @property string $country
 * @property string $zip
 * @property app\modules\management\models\Country $countryObject
 */
class Address extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'c_address';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['street1', 'city', 'country', 'zip'], 'required'],
            [['street1', 'street2'], 'string', 'max' => 128],
            [['city', 'county'], 'string', 'max' => 45],
            [['country'], 'string', 'max' => 2],
            [['zip'], 'string', 'max' => 16]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'street1' => Yii::t('app', 'Street1'),
            'street2' => Yii::t('app', 'Street2'),
            'city' => Yii::t('app', 'City'),
            'county' => Yii::t('app', 'County'),
            'country' => Yii::t('app', 'Country'),
            'zip' => Yii::t('app', 'Zip'),
        ];
    }
    
    public function getCountryObject()
    {
        return $this->hasOne(Country::className(), ['iso2' => 'country']);
    }
}
