<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\modules\management\models;

use Yii;

/**
 * This is the model class for table "c_country".
 *
 * @property integer $country_id
 * @property string $short_name
 * @property string $long_name
 * @property string $iso2
 * @property string $iso3
 * @property string $numcode
 * @property integer $un_member
 * @property string $calling_code
 * @property string $cctld
 * @property integer $eu_member
 * @property integer $has_shoreline
 */
class Country extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'c_country';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['iso2', 'iso3'], 'required'],
            [['un_member', 'eu_member', 'has_shoreline'], 'integer'],
            [['short_name', 'long_name'], 'string', 'max' => 80],
            [['iso2'], 'string', 'max' => 2],
            [['iso3'], 'string', 'max' => 3],
            [['numcode'], 'string', 'max' => 6],
            [['calling_code'], 'string', 'max' => 8],
            [['cctld'], 'string', 'max' => 5],
            [['iso2'], 'unique'],
            [['iso3'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'country_id' => Yii::t('app', 'Country ID'),
            'short_name' => Yii::t('app', 'Short Name'),
            'long_name' => Yii::t('app', 'Long Name'),
            'iso2' => Yii::t('app', 'Iso2'),
            'iso3' => Yii::t('app', 'Iso3'),
            'numcode' => Yii::t('app', 'Numcode'),
            'un_member' => Yii::t('app', 'Un Member'),
            'calling_code' => Yii::t('app', 'Calling Code'),
            'cctld' => Yii::t('app', 'Cctld'),
            'eu_member' => Yii::t('app', 'Eu Member'),
            'has_shoreline' => Yii::t('app', 'Has Shoreline'),
        ];
    }
}
