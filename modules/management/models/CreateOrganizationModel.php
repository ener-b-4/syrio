<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\modules\management\models;

use Yii;

/**
 * Model used for creating an Organization
 *
 * @author vamanbo
 */
class CreateOrganizationModel extends \yii\base\Model {
    //put your code here
    
    public function rules() {
        return [
            ['organization_type', 'required'],
        ];
    }
    
    public function attributeLabels() {
        return [
            'organization_type'=>Yii::t('app', 'Organization type')
        ];
    }
    
    public $organization_type;
    
    public $organization;       //organization model
    
    public function getOrganizationTypes()
    {
        return [
            1 => Yii::t('app', '{evt} Organization', ['evt' => Yii::t('app', 'Competent Authority')]),
            2 => Yii::t('app', '{evt} Organization', ['evt' => Yii::t('app', 'Operators/Owners')]),
        ];
    }
    
}
