<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\modules\management\models;

use Yii;

use app\modules\management\models\Address;
use app\modules\management\models\Country;
use app\modules\management\models\CompetentAuthority;

use app\models\events\EventDeclaration;

/**
 * This is the model class for table "ca_organizations".
 *
 * @property integer $id
 * @property string $organization_name
 * @property string $organization_acronym
 * @property string $organization_phone
 * @property string $organization_email
 * @property string $organization_fax
 * @property string $organization_phone2
 * @property integer $organization_address
 * @property string $organization_web
 * @property string $created_at
 * @property integer $created_by
 * @property string $modified_at
 * @property integer $modified_by
 *
 * @property CompetentAuthorities $competentAuthority
 * @property Address $address
 * 
 */
class Organization extends \yii\db\ActiveRecord
{
    const COMPETENT_AUTHORITY = 100;
    const OPERATORS_OWNERS = 200;
    const INSTALLATION = 300;
    
    const STATUS_DELETED = 0;
    const STATUS_REQUEST = 1;
    const STATUS_ACTIVE = 10;
    
    
    public function __construct($config = array()) {
        parent::__construct();
        if (key_exists('organizationType', $config))
        {
            $orgCode = strtoupper($config['organizationType']);
            switch ($orgCode)
            {
                case 'CA':
                    $this->attachBehavior('organizationBehavior', \app\modules\management\behaviors\CAOrganizationBehavior::className());
                    break;
                case 'OO':
                    $this->attachBehavior('organizationBehavior', \app\modules\management\behaviors\OOOrganizationBehavior::className());
                    break;
            }
        }
    }
    
    public function afterFind() {
        parent::afterFind();
        if (isset($this->id) && strlen($this->id)>2)
        {
            $orgCode = strtoupper(substr($this->id, 0, 2));
            switch ($orgCode)
            {
                case 'CA':
                    $this->attachBehavior('organizationBehavior', \app\modules\management\behaviors\CAOrganizationBehavior::className());
                    break;
                case 'OO':
                    $this->attachBehavior('organizationBehavior', \app\modules\management\behaviors\OOOrganizationBehavior::className());
                    break;
            }
        }
    }

    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'organizations';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['organization_name', 'organization_acronym', 'organization_phone', 'organization_email', 'organization_address', 'created_by', 'status'], 'required'],
            [['organization_address', 'created_by', 'modified_by', 'status'], 'integer'],
            ['status', 'in', 'range' => [self::STATUS_DELETED,self::STATUS_REQUEST,self::STATUS_ACTIVE]],
            [['created_at', 'modified_at'], 'safe'],
            [['organization_name', 'organization_web'], 'string', 'max' => 128],
            [['organization_acronym'], 'string', 'max' => 8],
            [['organization_phone', 'organization_fax', 'organization_phone2'], 'string', 'max' => 16],
            [['organization_email'], 'string', 'max' => 45],
            [['organization_acronym'], 'checkAcronym']
            //[['organization_acronym', 'country'], 'unique', 'targetAttribute' => ['organization_acronym', 'country'], 'message' => 'The combination of Organization Acronym and Country already exists.'],            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'organization_name' => Yii::t('app', 'Name'),
            'organization_acronym' => ucfirst(Yii::t('app', 'acronym')),
            'country' => ucfirst(Yii::t('app', 'country')),
            'organization_phone' => Yii::t('app', 'Phone'),
            'organization_email' => Yii::t('app', 'Email'),
            'organization_fax' => Yii::t('app', 'Fax'),
            'organization_phone2' => Yii::t('app', 'Phone (alt.)'),
            'organization_address' => ucfirst(Yii::t('app', 'address')),
            'organization_web' => Yii::t('app', 'Web'),
            'created_at' => ucwords(Yii::t('app', 'created at')),
            'created_by' => ucwords(Yii::t('app', 'created by')),
            'modified_at' => ucwords(Yii::t('app', 'modified at')),
            'modified_by' => ucwords(Yii::t('app', 'modified by')),
        ];
    }

    /**
     * @return string(2) the Country iso2 of the Competent Authority
     */
    public function getCompetentAuthority()
    {
        return Yii::$app->params['competentAuthority'];
    }

    
    //(start) Author: vamanbo   Date: 25.08.2015  - on Edit Organization: add the write part of the property and make it 'internal'
    private $_country;
    
    public function setCountry($value)
    {
        $this->_country = $value;
    }

    public function getCountry()
    {
        return $this->_country;
    }
    //(end) Author: vamanbo   Date: 25.08.2015  - on Edit Organization: add the write part of the property and make it 'internal'
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddress()
    {
        return $this->hasOne(Address::className(), ['id' => 'organization_address']);
    }

    /**
     * 
     * @return \yii\db\ActiveQuery
     */
    public static function findOperatorOrganization()
    {
        return parent::find()
                ->where(['like', 'id', 'oo']);
    }
    

    /**
     * Gets the Operator/Owners organization models registered with SyRIO
     * 
     * @version 1.0.0 (20150724)
     * @author Bogdan Vamanu <bogdan.vamanu@jrc.ec.europa.eu>
     * 
     * @return \yii\db\ActiveQuery 
     */
    public static function getOperatorOrganizations()
    {
        if (Yii::$app->user->can('sys_admin') || Yii::$app->user->can('ca_admin'))
        {
            return parent::find()
                    ->where(['like', 'id', 'oo'])
                    ->orderBy('organization_name');
        }
        elseif (Yii::$app->user->identity->isOperatorUser)
        {
            //only return his organization
            return parent::find()
                    ->where(['id' => Yii::$app->user->identity->organization->id]);
        }
    }
    
    
    /*
     * Returns the number of active users in a given Organization
     */
    public function getNumberOfUsers()
    {
        return \app\models\User::find()
                ->where(['org_id' => $this->id])
                ->andWhere(['status' => \app\models\User::STATUS_ACTIVE])
                ->count();
    }
    
    public function checkAcronym($attribute, $params)
    {
        $org = Organization::find()
                ->where(['organization_acronym' => $this->$attribute])
                ->andWhere(['status' => Organization::STATUS_ACTIVE])
                ->andWhere(['not', ['id' => $this->id]])
                ->one();
        
        if (isset($org))
        {
            $this->addError($attribute, \Yii::t('app', 'There\'s already an active installation having this acronym. Please reconsider.'));
        }
    }
    
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(\app\models\User::className(), ['id' => 'created_by']);
    }
    
    public function getModifiedBy()
    {
        return $this->hasOne(\app\models\User::className(), ['id' => 'modified_by']);
    }
    
    public function getEvents() {
        return EventDeclaration::find()
                ->where(['operator_id'=>$this->id])
                ->all();
    }
    
    public function getHasEvents() {
        $query = EventDeclaration::find()
                ->where(['operator_id'=>$this->id])
                ->count();
        return $query > 0;
    }
    
    
}
