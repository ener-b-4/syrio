<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\modules\management;

class Management extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\management\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
