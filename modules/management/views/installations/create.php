<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\helpers\Url;
use app\widgets\ValidationReportWidget;

/* @var $this yii\web\View */
/* @var $model app\models\Installations */
/* @var $organizations the dropdownlist ready array of possible organizations */
/* @var $installation_types the dropdownlist ready array of possible types of installations */
/* @var $countries the dropdownlist ready array of countries */

/*
 * Breadcrumbs stuff
 */
$breadcrumbs = [
    [
        'label' => Yii::t('app', 'Management'),
        'url' => Url::to(['/management/']),
        //'template' =>"<li>{link}</li>
    ],
    [
        'label' => Yii::t('app', 'Installations'),
        'url' => Url::to(['/management/installations']),
        //'template' =>"<li>{link}</li>
    ],
];
$this->params['breadcrumbs'] = $breadcrumbs;

$this->title = Yii::t('app', 'Create');
$this->params['breadcrumbs'][] = $this->title;
?>

<?php
if ($model->hasErrors())
{
    echo ValidationReportWidget::widget([
        'title' => \Yii::t('app','Register {evt}', ['evt'=>ucfirst(Yii::t('app', 'installation'))]),
        'errors_message' => \Yii::t('app','Unable to register the installation! Please review the information below and try again.'),
        'errors'=>$model->errors,
        'no_errors_message' => \Yii::t('app','Installation {evt} has been created.', ['evt'=>$model->name]),
    ]);
}                            
?>

<h1><?= \Yii::t('app','Installations') ?></h1>
<p><?= \Yii::t('app', 'Create Installation', []) ?></p>

<?= $this->render('_form', [
    'model' => $model,
    'organizations' => $organizations,
    'installation_types'=> $installation_types,
    'countries' => $countries
]) ?>

