<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use kartik\grid\GridView;
use app\components\helpers\DataGrid\CustomGridView;
use app\assets\FontAwesomeAsset;
use app\assets\SyrioGlyphsAsset;
use yii\helpers\Url;
use app\models\Installations;
use app\widgets\ActionMessageWidget;
use app\components\helpers\DataGrid\funDataGridClass;

SyrioGlyphsAsset::register($this);
FontAwesomeAsset::register($this);

/* @var $this yii\web\View */
/* @var $searchModel app\models\InstallationsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

/*
 * Breadcrumbs stuff
 */
$breadcrumbs = [
    [
        'label' => Yii::t('app', 'Management'),
        'url' => Url::to(['/management/']),
        //'template' =>"<li>{link}</li>
    ],
];
$this->params['breadcrumbs'] = $breadcrumbs;

$this->title = ucfirst(Yii::t('app', 'installations'));
$this->params['breadcrumbs'][] = $this->title;
?>

<?php

$title_h_l =  'SyRIO ' . \Yii::t('app', 'Export');
$title_h_c =  $this->title; //'Custom C';
$title_h_r = 'Generated' . ': ' . date("D, d-M-Y g:i a T");

$ourPdfHeader = funDataGridClass::getOurPdfHeader($title_h_l, $title_h_c, $title_h_r);

$title_f = '';
$ourPdfFooter = funDataGridClass::getOurPdfFooter($title_f);
$exportConfig = funDataGridClass::getExportConfiguration($ourPdfHeader, $ourPdfFooter);


/* setup the grid columns */
$columns = [];
$columns[] = ['class' => 'yii\grid\SerialColumn'];

Yii::$app->user->can('sys_admin') ? $columns[] = 'id' : '';

$columns[] = 'code';
$columns[] = [
    'attribute' => 'name',
    'label' => Yii::t('app', 'Name'),
];

$columns[] = [
    'class' => kartik\grid\DataColumn::className(),
    'attribute' => 'flag',
    'filterType' => \kartik\grid\GridView::FILTER_SELECT2,
    'filter' => yii\helpers\ArrayHelper::map(app\modules\management\models\Country::find()->orderBy('short_name')->all(), 'iso2', 'short_name'),
    'filterWidgetOptions'=>[
            'pluginOptions'=>['allowClear'=>true],
        ],
    'filterInputOptions'=>['placeholder'=>Yii::t('app', 'country')],
    
    'content' => function($model, $key, $index, $column) {
        
        /* @var $model \app\models\Installations */
        if ($model->type == 'MODU')
        {
            $flags_url = \Yii::getAlias('@web') . '/css/images/flags/' . strtoupper($model->country->iso2) . '.png';
            return Html::img($flags_url) . ' '
            . $model->country->short_name . ' (' . $model->country->iso2 . ')';
        }
        else
        {
            return 'N/A';
        }
    }
];

$columns[] = [
    'class' => kartik\grid\DataColumn::className(),
    'label' => ucfirst(Yii::t('app', 'installation type')),
    'attribute' => 'typeObject',
    'filterType' => \kartik\grid\GridView::FILTER_SELECT2,
    'filter' => yii\helpers\ArrayHelper::map(app\models\InstallationTypes::find()->all(), 'type', 'type'),
    'filterWidgetOptions'=>[
            'pluginOptions'=>['allowClear'=>true],
        ],
    'filterInputOptions'=>['placeholder'=>Yii::t('app', 'type')],
    'content' => function($model, $key, $index, $column) {
        return $model->typeObject->type . ' - ' .
                $model->typeObject->name . 
                ' <em>(' . 
                ($model->typeObject->mobile == 1 ? strtolower(Yii::t('app', 'Mobile')) : strtolower(Yii::t('app', 'Fixed'))) . 
                ')</em>';        
    }
];

$columns[] = [
    'attribute' => 'year_of_construction',
    'label' => Yii::t('app/cpf', 'Year of construction'),
];

$columns[] = [
    'class' => kartik\grid\DataColumn::className(),
    'attribute' => 'number_of_beds',
    'label' => Yii::t('app', 'Beds'),
    'filterWidgetOptions'=>[
            'pluginOptions'=>['allowClear'=>true],
        ],
    'content' => function($model, $key, $index, $column) {
        return isset($model->number_of_beds) ? $model->number_of_beds : '-';
    }
];

$columns[] = [
    'class' => kartik\grid\DataColumn::className(),
    'attribute' => 'operator',  //to be created in Search
    'label' => Yii::t('app/crf', 'Operator/owner'),
    'content' => function($model, $key, $index, $column) {
        if (isset($model->operator)) {
            return Html::a($model->operator->organization_acronym, 
                    Url::to(['/management/organization/view', 'id'=>$model->operator->id]), []);
        }
    }
];


/* ACTION COLUMNS SECTION */
$editColumn = [
    'class' => 'kartik\grid\ActionColumn',
    'dropdown' => false,
    'dropdownOptions' => [
        'class' => 'pull-right',
    ],
    'buttons' => [
        'delete' => function ($url, $model, $key) {
            if (!Yii::$app->user->can('oo-inst-delete')) {
                return '';
            }
            $msg = Yii::t('app', 'Are you sure you want to delete {data}?', [
                'data'=>$model->name]);
            $msg .= ' ';
            $msg .= Yii::t('app', 'Operation CAN be undone.');
            return $model->status === 10 || $model->status === 1 ? 
                    Html::a('<i class="glyphicon glyphicon-trash"></i>', $url, [
                        'title' => Yii::t('app', 'Remove'),
                        'data-confirm' => $msg,
                        'data-method' => 'POST']) : '';
            
        },
        'purge' => function ($url, $model, $key) {
                if (!Yii::$app->user->can('sys_admin') || $model->hasEvents)
                {
                    return '';
                }

                $msg = Yii::t('app', 'Are you sure you want to PERMANENTLY delete {data}?', [
                    'data'=>$model->name]);
                $msg .= ' ';
                $msg .= Yii::t('app', 'Operation CAN NOT be undone.');
                return $model->status === 0 ? 
                        Html::a('<i class="glyphicon glyphicon-remove"></i>', $url, [
                            'title' => ucfirst(Yii::t('app/commands', 'purge')),
                            'data-confirm' => $msg,
                            'data-method' => 'POST']) : '';
            },
        'reactivate' => function ($url, $model, $key) {
                if (!Yii::$app->user->can('sys_admin'))
                {
                    return '';
                }
                $msg = Yii::t('app', 'This will reactivate {data}.', [
                    'data'=>$model->name]);
                $msg .= ' ';
                $msg .= Yii::t('app', 'Are you sure?');
                return $model->status === 0 ? 
                        Html::a('<i class="glyphicon glyphicon-repeat"></i>', $url, [
                            'title' => ucfirst(Yii::t('app', 'reactivate')), 
                            'data-confirm' => $msg,
                            'data-method'=>'POST']) : '';
            },
    ],      //end of buttons section
    'template' => '{view} {update} {delete} {purge} {reactivate}'
];
$columns[] = $editColumn;


$actionColumn = [
    'class' => 'kartik\grid\ActionColumn',
    'header' => ucfirst(Yii::t('app', 'users')),
    'width' => '200px',
    'headerOptions' => [
        
    ],
    'dropdown' => false,
    'dropdownOptions' => [
        'class' => 'pull-right',
    ],
    'buttons' => [
        'static-users-badge' => function($url,$model) {
            return '<span class="badge" '
                    . 'style="margin-right: 6px; cursor: context-menu">'
                    . '' . $model->numberOfUsers. '</span>';
        },
        'create-user' => function($url, $model) {
            if (Yii::$app->user->can('user-create', []))
            {
                $title = Yii::t('app', 'Add user');
                $options = []; // initialize this
                $icon = '<i class="fa fa-user-plus"></i>';
                $label = $icon; // . ' ' . $title;
                //$url = Url::toRoute(['create-user','id'=>$model->id, 'country'=>$model->country]);
                $url = Url::toRoute(['/admin/users/user/create-installation-user','installation_id'=>$model->id]);
                $options['title'] = $title;
                $options['tabindex'] = '-1';
                $options['data-method'] = 'post';
                $options['data-toggle'] = 'tooltip';
                return Html::a($label, $url, $options) . PHP_EOL;            
            }
        },
        'list-users' => function($url, $model) {
            if (Yii::$app->user->can('user-list'))
            {
                $title = Yii::t('app', 'List {evt}', ['evt'=>Yii::t('app', 'users')]);
                $options = []; // this must be initialized
                $icon = '<i class="fa fa-th"></i>';
                $label = $icon; // . ' ' . $title;
                $url = Url::toRoute([
                    '/admin/users/user/index', 
                    'UserSearch[org_acronym]'=> $model->name]);
                $options['title'] = $title;
                $options['tabindex'] = '-1';
                $options['data-method'] = 'post';
                $options['data-toggle'] = 'tooltip';
                return Html::a($label, $url, $options) . PHP_EOL;            
            }
            else
            {
                return null;
            }
        }
                
    ],      //end of buttons section
    'template' => '{static-users-badge} {list-users} {create-user}'
];

$columns[] = $actionColumn;

/* Grid toolbar section */
$grid_toolbar = [
        [
            'content'=>
                Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], [
                    'class' => 'btn btn-success',
                    'title' => Yii::t('app', 'Add {evt}', ['evt' => ucfirst(Yii::t('app', 'installation'))])
                ]) .
                Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], [
                    'class' => 'btn btn-default', 
                    'title' => Yii::t('app', 'Reset Grid')
                ]),
            'options' => ['class' => 'btn-group']
        ],
        '{export}',
        //'{toggle-data}'
    ];   //end grid toolbar


?>


<div class="installations-index">

    <?php
        $session_msg = Yii::$app->session->get('finished_action_result');     //$session_msg is of type ActionMessage
        if (isset($session_msg))
        {
            echo ActionMessageWidget::widget(['actionMessage' => $session_msg,]);

            //disable the next line if you want the message to persist
            unset(Yii::$app->session['finished_action_result']);
        }
    ?>    
    
    <h1><span class="icon-syrio_glyphs"></span><?= Html::encode(' ' . \Yii::t('app', 'List {evt}', ['evt'=>$this->title])) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
    </p>

    <?= CustomGridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'pjax' => true,
        
        'rowOptions' => function($model) {
            return $model->status == Installations::STATUS_DELETED ? ['class' => 'danger'] : [];
        },
        
        'columns' => $columns,  // $grid_columns,
        'toolbar' => $grid_toolbar,
        
        'responsive'=>'true',
        'hover'=>true,
                
        // set export properties
        'export' => [
          'icon' => 'share-square-o',
          'encoding' => 'utf-8',
          'fontAwesome' => true,
          'target' => CustomGridView::TARGET_SELF,   //target: string, the target for submitting the export form, which will trigger the download of the exported file. 
          'showConfirmAlert' => TRUE,
          'header' => '<li role="presentation" class="dropdown-header">' . \Yii::t('app', 'Grid Export') . '</li>'
          //'header' => 'Export Page Data',
  	],

          //(start) Author: cavesje   Date: 14.08.2015  - extra configuration PDF export
          'exportConfig'     => $exportConfig,
          //(end) Author: cavesje   Date: 14.08.2015  - extra configuration PDF export

        'panel' => [
                'type' => GridView::TYPE_PRIMARY,
                'heading' => '<span class="icon-syrio_glyphs"></span>' . ucfirst(Yii::t('app', 'installations')),
                'before'=>''
                ,     //end before
                'after'=>'<span></span>',            
            ],                        
        
    ]); ?>

</div>
