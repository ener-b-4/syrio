<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Installations */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => ucfirst(Yii::t('app', 'installations')), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$flag_url = \Yii::getAlias('@web') . '/css/images/flags/' . $model->flag;
$org_url = \Yii::$app->urlManager->createUrl(['/management/operators-owners-organization/view', 'id'=>$model->operator_id]);
?>
<div class="installations-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
    <?php
    $columns = [];
    
    if (\Yii::$app->user->can('oo-inst-update')) {
        echo Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id, 'operator_id' => $model->operator_id], ['class' => 'btn btn-primary']);
    };
    
    $columns[] = 'name';
    $columns[] = [
        'label' => ucfirst(\Yii::t('app', 'type')),
        'value' => $model->type . ' (' . $model->typeObject->name . ')',        
    ];
    $columns[] = 'type_other';
    $columns[] = 'year_of_construction';
    $columns[] = 'number_of_beds';
    $columns[] = 'code';
    
    //$model->flag = 'ro';
    if (isset($model->flag) && strlen($model->flag) == 2 ) {
        
        $country = app\modules\management\models\Country::find()->where(['iso2' => $model->flag])->one();
        
        $columns[] = [
            'label' => ucfirst(\Yii::t('app', 'Flag (if MODU)')),
            //'attribute' => 'flag',
            'value' => '<img src="' . $flag_url . '.png' . '" />' 
                . '<span style="padding-left: 6px">' . $country->short_name . ' (' . strtoupper($model->flag) . ')</span>',
            'format' => 'raw',
        ];
        
    }
    
    $columns[] = [
        'label' => \Yii::t('app/crf', 'Operator/owner'),
        'value' => Html::a($model->operator->organization_acronym, $org_url),
        'format' => 'raw'
    ];

    $sec_columns = [];
    $sec_columns[] = [
        'label' => ucfirst(\Yii::t('app', 'created by')),
        'value' => $model->createdBy->full_name
    ];
    $sec_columns[] = [
        'label' => ucfirst(\Yii::t('app', 'created at')),
        'value' => date('Y-m-d H:i:s', $model->created_at)
    ];

    if (isset($model->modified_by) && strlen($model->modified_by) > 0) {
        $sec_columns[] = [
            'label' => ucfirst(\Yii::t('app', 'modified by')),
            'value' => $model->modifiedBy->full_name
        ];
        $sec_columns[] = [
            'label' => ucfirst(\Yii::t('app', 'modified at')),
            'value' => date('Y-m-d H:i:s', $model->modified_at)
        ];
    } else {
        $sec_columns[] = [
            'label' => \Yii::t('app', 'Modified'),
            'value' => null
        ];
    }
    
    
    ?>

        
        
        
    </p>

    <div class="row">
        <div class="col-sm-9">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => $columns
            ]) ?>
        </div>
        <div class="col-sm-3">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => $sec_columns
            ]) ?>
        </div>
    </div>
    

    
    
</div>
