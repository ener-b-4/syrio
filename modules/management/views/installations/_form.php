<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use kartik\form\ActiveForm;
use yii\helpers\ArrayHelper;
use app\modules\management\models\Organization;


/* @var $this yii\web\View */
/* @var $model app\models\Installations */
/* @var $organizations the dropdownlist ready array of possible organizations */
/* @var $installation_types the dropdownlist ready array of possible types of installations */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="installations-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'operator_id')
            ->dropDownList(
                    $organizations, 
                    [
                        'prompt'=>Yii::t('app', '(none)'),
                    ])
            ->label(Yii::t('app/crf', 'Operator/owner'))?>
    
    <?= $form->field($model, 'type')
            ->dropDownList(
                    $installation_types, 
                    [
                        'prompt'=>Yii::t('app', '(none)'),
                    ])?>

    <?= $form->field($model, 'type_other')->textInput(['maxlength' => true, 'placeholder'=>Yii::t('app', 'e.g. jack-up')])->hint(\Yii::t('app', 'optional')) ?>
    
    <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder'=>Yii::t('app', 'add name')]) ?>

    <?= $form->field($model, 'code')->textInput(['maxlength' => true, 'placeholder'=>Yii::t('app', 'add the installation code')])
            ->hint(Yii::t('app', 'Please use the IMO code (if available)'))?>

    <?= $form->field($model, 'flag')
            ->dropDownList(
                    $countries, 
                    [
                        'prompt'=>' - ' .Yii::t('app', 'country'). ' - ',
                    ])
            ?>

    <?= $form->field($model, 'year_of_construction')->textInput() ?>

    <?= $form->field($model, 'number_of_beds')->textInput() ?>

    <?= $form->field($model, 'op_status')
            ->dropDownList(
                    $model->operationalStatuses, 
                    [
                        'prompt'=>Yii::t('app', '(not set)'),
                    ])->label(\Yii::t('app', 'Operational status')) ?>

    <hr>
    
    <small>
        <?php
            if (!$model->isNewRecord)
            {
                echo ucfirst(Yii::t('app', 'created by')) . ': ' .$model->createdBy->full_name
                        . ' ' . Yii::t('app', 'at') . ' '
                        . date('Y-m-d H:i:s', $model->created_at);
                echo '<br/>';
                if (isset($model->modified_at)) {
                echo ucfirst(Yii::t('app', 'modified by')) . ': ' . $model->modifiedBy->full_name
                        . ' ' . Yii::t('app', 'at') . ' '
                        . date('Y-m-d H:i:s', $model->modified_at);
                }
            }
        ?>
    </small>
    
    <div class="form-group" style="margin-top: 16px">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
