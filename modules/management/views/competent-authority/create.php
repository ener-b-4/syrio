<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use app\components\Errors;

/* @var $this yii\web\View */
/* @var $model app\modules\management\models\CompetentAuthority */
/* @var $organization app\modules\management\models\Organization */
/* @var $address app\modules\management\models\Address */

$this->title = Yii::t('app', 'Create {evt}', [
    'evt' => Yii::t('app', 'Competent Authority'),
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Competent Authorities'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="competent-authority-create">

    <?php
        if (!Yii::$app->user->can('sys_admin'))
        {
            Yii::$app->error->ThrowError(Errors::RESTRICTED_AREA_ERROR);
        }
    ?>
    
    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'organization' => $organization,
        'address' => $address,
    ]) ?>

</div>
