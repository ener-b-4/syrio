<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
//use yii\widgets\DetailView;
use yii\grid\GridView;
//use kartik\tabs\TabsX;
use yii\widgets\Pjax;
use kartik\nav\NavX;

use app\modules\management\models\Country;

/* @var $this yii\web\View */
/* @var $model app\modules\management\models\CompetentAuthority */

$this->title = $model->country;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app\cpf', 'Management'), 'url' => ['/management']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app\cpf', 'Competent Authorities'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?php

$_country = Country::find()
        ->where(['iso2'=>$model->country])
        ->select('short_name')->asArray();

if (count($_country)==0) {
    throw new ErrorException();
}
else
{
    $_countryLong = $_country->one()["short_name"];
}


$items = [
    [
        'label' => '<i class="fa fa-building-o"></i>' . ' ' . Yii::t('app', 'Organizations'),
        'active' => 'true',
        'url' => '#'
    ],
    [
        'label' => '<i class="fa fa-user"></i>' . ' ' . ucfirst(Yii::t('app', 'users')),
        'url' => '#'
    ]
]

?>

<div class="competent-authority-view">

    <h1><?= Yii::t('app', 'Competent Authority') . ': ' . Html::encode($_countryLong) ?></h1>

    <p>
        <?php
            // Usage with bootstrap nav pills.
            echo NavX::widget([
                'options'=>['class'=>'nav nav-pills'],
                'items' => $items,
                'encodeLabels' => false
            ]);
        ?>
    </p>
    
    
    
    <p>
        <?php // Html::a(Yii::t('app\cpf', 'Update'), ['update', 'id' => $model->country], ['class' => 'btn btn-primary']) ?>
        <?php
            /*Html::a(Yii::t('app\cpf', 'Delete'), ['delete', 'id' => $model->country], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app\cpf', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) */?>
    </p>

    <?php
        $columns =[
            'organization_acronym',
            'attribute' => 'organization_name',           
        ];
    ?>

    <div>
        <?php
        
        Pjax::begin(['id'=>'bv_pj']);
        echo GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                //'pjax' => true,
                /*
                'toolbar' =>  [
                    ['content'=>
                        Html::a('<i class="glyphicon glyphicon-plus"></i>', Yii::$app->getUrlManager()->createUrl('/admin/users/user/create-ca-user'), ['class'=>'btn btn-success', 'title'=>Yii::t('app', 'Add organization')]),
                        Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['grid-demo'], ['data-pjax'=>0, 'class' => 'btn btn-default', 'title'=>Yii::t('app', 'Reset Grid')])
                    ],
                    '{toggleData}',
                ],
                 * 
                 */
                'columns' => $columns,
                
                /*
                'responsive'=>true,
                'resizableColumns'=>true,            
                'hover'=>true,
                'export'=>false,
                'panel' => [
                        //'type' => GridView::TYPE_PRIMARY,
                        'heading' => '<i class="fa fa-building-o"></i> ' . Yii::t('app', 'Organizations'),
                    ],
                 * 
                 */
            ]);
        Pjax::end();
        ?>
        
    </div>

</div>
