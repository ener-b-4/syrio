<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use kartik\grid\GridView;
use kartik\helpers\Html;
use \yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\management\models\CompetentAuthorityOrganizationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$columns =[
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'acronym',
        'value' => 'organization_acronym',
        'options' => [
            'width'=> '50px',
        ]
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'organization_name',
        'value' => 'organization_name',
        'options' => [
            //'width'=> '50px',
        ]
    ],
];
        
?>

        <?php
        
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'pjax' => true,
            'toolbar' =>  [
                ['content'=>
                    Html::a('<i class="glyphicon glyphicon-plus"></i>', Yii::$app->getUrlManager()->createUrl('/admin/users/user/create-ca-user'), ['class'=>'btn btn-success', 'title'=>Yii::t('app', 'Add organization')]),
                    Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['grid-demo'], ['data-pjax'=>0, 'class' => 'btn btn-default', 'title'=>Yii::t('app', 'Reset Grid')])
                ],
                '{toggleData}',
            ],
            'columns' => $columns,
            'responsive'=>true,
            'hover'=>true,
            'export'=>false,
            'panel' => [
                    'type' => GridView::TYPE_PRIMARY,
                    'heading' => '<i class="fa fa-building-o"></i> ' . Yii::t('app', 'Organizations'),
                ],                        
        ]);

        ?>

<?php

$js = <<<JS

      
$( '.form-control' ).each(function(index){
    //alert('here');
});        

        
        
$(document).on('submit', function(e){
        //alert('submit');
        //console.log($( this ).attr('action'));

    $( '.gridview-filter-form' ).each(function(index){
        //alert('here');
        \$that = $( this );
        
        $.get(
            \$that.attr("action"),
            \$that.serialize()
        )
        .done(function(result) {
            //console.log(result);
            $( '#w0-tab0' ).html(result);        
            //$( '#w0-tab0' ).html(JSON.stringify(result));
            //$.pjax.reload({container:'#w0-tab0'});
        });
        
        console.log($(this).attr('action'));
    });        
   
   
    //console.log(e);
    e.preventDefault();
});
        
    
JS;

$this->registerJs($js);

?>