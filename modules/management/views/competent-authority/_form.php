<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use kartik\widgets\ActiveForm;
use kartik\helpers\Html;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\modules\management\models\CompetentAuthority */
/* @var $organization app\modules\management\models\Organization */
/* @var $address app\modules\management\models\address */
/* @var $form yii\widgets\ActiveForm */


$countries = ArrayHelper::map(\app\modules\management\models\Country::find()
        ->where('eu_member=1 AND has_shoreline=1')
        ->all(), 'iso2', 'short_name');

$step2_div_class  = isset($model->country) ? '' : 'hidden';
?>

<div class="competent-authority-form">

    
    <?php $form = ActiveForm::begin([
        'enableAjaxValidation' => true,
        'type' => ActiveForm::TYPE_HORIZONTAL,                
    ]); ?>

    <div id="step1-title" class="sy_step_container" style="padding-top:24px;">
        <table>
            <tbody>
                <tr>
                    <td style='vertical-align: top;'><span class="sy_step_id">1</span></td>
                    <td>
                        <span class="sy_step_title"><?= Yii::t('app', 'Register one Competent Authority Organization') ?></span>
                        <span class="text-info">
                            <?php
                            ?>
                        </span>
                    </td>
                </tr>
            </tbody>
        </table>
        <div id="step1" class='row'>
            <div class='form-group'>
                <?= Html::activeLabel($model, 'country', ['label'=>'Competent Authority Country', 'class'=>'col-sm-3 control-label']) ?>
                <div class="col-md-3 col-sm-4">
                    <?= $form->field($model, 'country', ['showLabels'=>false])->dropDownList($countries, [
                        'prompt' => '-- select country --',
                    ]) ?>
                </div>
            </div>
        </div>
    </div>

    <div id="step2-title" class="sy_step_container <?= $step2_div_class ?> ">
        <table>
            <tbody>
                <tr>
                    <td style='vertical-align: top;'><span class="sy_step_id">2</span></td>
                    <td>
                        <span class="sy_step_title">Register one Competent Authority Organization</span>
                        <span class="text-info">
                            <?php
                                $country = isset($model->country) ? 'abc' : $model->country;
                                $s = '<br/>';
                                $s .= Yii::t('app', 'According to Directive 2013/30/EU MS Competent Authorities may have <strong>at least</strong> one Organization.');
                                $s .= '<br/>';
                                $s .= Yii::t('app', 'To complete the registration of the Competent Authority of {rev}, please provide the details of one of the component Organizations.', [
                                    'rev' => $country,
                                ]);
                                $s .= '<br/>';
                                echo $s;
                            ?>
                        </span>
                    </td>
                </tr>
            </tbody>
        </table>
        
        <div id="step2" class='row' style="padding-top:24px;">
            <div class='form-group'>
                <?= Html::activeLabel($organization, 'organization_name', ['label'=>'Organization name', 'class'=>'col-sm-2 control-label']) ?>
                <div class='col-sm-6'>
                    <?= $form->field($organization, 'organization_name', ['showLabels'=>false, 'enableAjaxValidation' => true])->textInput(['maxlength' => true, 'placeholder' => Yii::t('app', 'Full name')]) ?>
                </div>
                <div class='col-sm-3'>
                    <?= $form->field($organization, 'organization_acronym', ['showLabels'=>false])->textInput(['maxlength' => true, 'placeholder' => ucfirst(Yii::t('app', 'acronym'))]) ?>
                </div>
            </div>

            <div class='form-group'>
                <?= Html::activeLabel($organization, 'organization_phone', ['label'=>'Contact', 'class'=>'col-sm-2 control-label']) ?>
                <div class='col-lg-2 col-sm-4 col-xs-12'>
                    <?= $form->field($organization, 'organization_phone', ['showLabels' => false])->textInput(['maxlength' => true, 'placeholder' => 'e.g. (+4)021 555-55-55'])->hint(Yii::t('app', 'Enter phone number')) ?>
                </div>
                <div class='col-lg-2 col-sm-4 col-xs-12'>
                    <?= $form->field($organization, 'organization_phone2', ['showLabels' => false])->textInput(['maxlength' => true, 'placeholder' => 'e.g. (+4)021 555-55-55'])->hint(Yii::t('app', 'Alternate phone number (optional)')) ?>
                </div>
                <div class='col-lg-2 col-sm-4 col-xs-12 col-lg-offset-0 col-sm-offset-2'>
                    <?= $form->field($organization, 'organization_fax', ['showLabels' => false])->textInput(['maxlength' => true, 'placeholder' => 'e.g. (+4)021 555-55-55'])->hint(Yii::t('app', 'Enter fax number (optional)')) ?>
                </div>
                <div class='col-lg-6 col-sm-8 col-xs-12 col-sm-offset-2'>
                    <?= $form->field($organization, 'organization_email', ['showLabels' => false])->textInput(['maxlength' => true, 'placeholder' => 'noone@nomail.com'])->hint(Yii::t('app', 'Enter email address')) ?>
                </div>
                <div class='col-lg-6 col-sm-8 col-xs-12 col-sm-offset-2'>
                    <?= $form->field($organization, 'organization_web', ['showLabels' => false])->textInput(['maxlength' => true, 'placeholder' => 'http://www.caorganization.com'])->hint(Yii::t('app', 'Enter Organization homepage (optional)')) ?>
                </div>
            </div>


            <div class='form-group'>
                <?= Html::activeLabel($address, 'street1', ['label'=>ucfirst(Yii::t('app', 'address')), 'class'=>'col-sm-2 control-label']) ?>
                <div class='col-sm-9'>
                    <?= $form->field($address, 'street1', ['showLabels' => false])->textInput(['maxlength' => true, 'placeholder' => \Yii::t('app', 'please enter street name, nr...')])->hint(Yii::t('app', 'Enter street name')) ?>
                </div>
                <div class='col-sm-9 col-sm-offset-2'>
                    <?= $form->field($address, 'street2', ['showLabels' => false])->textInput(['maxlength' => true, 'placeholder' => ''])->hint(Yii::t('app', 'Enter street name (line 2, optional)')) ?>
                </div>
                <div class='col-sm-3 col-sm-offset-2'>
                    <?= $form->field($address, 'city', ['showLabels' => false])->textInput(['maxlength' => true, 'placeholder' => strtolower(\Yii::t('app', 'City'))])->hint(Yii::t('app', 'Enter city')) ?>
                </div>
                <div class='col-sm-2'>
                    <?= $form->field($address, 'county', ['showLabels' => false])->textInput(['maxlength' => true, 'placeholder' => strtolower(\Yii::t('app', 'County'))])->hint(Yii::t('app', 'Enter county (optional)')) ?>
                </div>
                <div class='col-sm-2'>
                    <?= $form->field($address, 'zip', ['showLabels' => false])->textInput(['maxlength' => true, 'placeholder' => strtolower(Yii::t('app', 'Zip'))])->hint(Yii::t('app', 'Enter zip (optional)')) ?>
                </div>
            </div>    

        </div>  <!-- end step 2 row -->
    </div>
    
    <div id="fg" class="form-group  <?= $step2_div_class ?>">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app\cpf', 'Create') : Yii::t('app\cpf', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$js = <<<EOF

$('#competentauthority-country').change(function(evt){
        //alert($( this ).val());
        
        \$selCountry = $( this ).val();
        
        $.get('check-exists', { country: \$selCountry }, function(data) {
            alert(data);
        
            var result = JSON.parse(data);
        
            console.log(result);
        
            if (result.result==0) {
                $( '#step2-title' ).removeClass('hidden');
                $( '.form-group' ).removeClass('hidden');
            }
            else {
                $( '#step2-title' ).removeClass('hidden');
                $( '.form-group' ).removeClass('hidden');
                $( '#step2-title' ).addClass('hidden');
                $( '.form-group' ).addClass('hidden');
        
                //display the info that exists
                
            }
            //$( '#step2' ).html(data);
        })
        .fail(function() {
            alert('error');
        });
   });        
EOF;

$this->registerJs($js, \yii\web\View::POS_END);
?>