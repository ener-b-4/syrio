<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */


use kartik\widgets\ActiveForm;
use kartik\helpers\Html;

/**
 * Description of _create_organization
 *
 * @author vamanbo
 */

/* @var $country string */
/* @var $organization app\modules\management\models\Organization */

?>

<h1 style='padding-bottom: 32px;'>Create organization for <?= $country ?></h1>

<?php ob_start(); 
$form = ActiveForm::begin([
        //'id' => 'w0',
        'type' => ActiveForm::TYPE_HORIZONTAL,        
]); 
ob_get_clean(); ?>




<div class='row sy_padding_clear'>
    
    <div class='form-group'>
        <?= Html::activeLabel($organization, 'organization_name', ['label'=>'Organization name', 'class'=>'col-sm-2 control-label']) ?>
        <div class='col-sm-6'>
            <?= $form->field($organization, 'organization_name', ['showLabels'=>false, 'enableAjaxValidation' => true])->textInput(['maxlength' => true, 'placeholder' => Yii::t('app', 'Full name')]) ?>
        </div>
        <div class='col-sm-3'>
            <?= $form->field($organization, 'organization_acronym', ['showLabels'=>false])->textInput(['maxlength' => true, 'placeholder' => ucfirst(Yii::t('app', 'acronym'))]) ?>
        </div>
    </div>
    
    <div class='form-group'>
        <?= Html::activeLabel($organization, 'organization_phone', ['label'=>'Contact', 'class'=>'col-sm-2 control-label']) ?>
        <div class='col-lg-2 col-sm-4 col-xs-12'>
            <?= $form->field($organization, 'organization_phone', ['showLabels' => false])->textInput(['maxlength' => true, 'placeholder' => 'e.g. (+4)021 555-55-55'])->hint(Yii::t('app', 'Enter phone number')) ?>
        </div>
        <div class='col-lg-2 col-sm-4 col-xs-12'>
            <?= $form->field($organization, 'organization_phone2', ['showLabels' => false])->textInput(['maxlength' => true, 'placeholder' => 'e.g. (+4)021 555-55-55'])->hint(Yii::t('app', 'Alternate phone number (optional)')) ?>
        </div>
        <div class='col-lg-2 col-sm-4 col-xs-12 col-lg-offset-0 col-sm-offset-2'>
            <?= $form->field($organization, 'organization_fax', ['showLabels' => false])->textInput(['maxlength' => true, 'placeholder' => 'e.g. (+4)021 555-55-55'])->hint(Yii::t('app', 'Enter fax number (optional)')) ?>
        </div>
        <div class='col-lg-6 col-sm-8 col-xs-12 col-sm-offset-2'>
            <?= $form->field($organization, 'organization_email', ['showLabels' => false])->textInput(['maxlength' => true, 'placeholder' => 'noone@nomail.com'])->hint(Yii::t('app', 'Enter email address')) ?>
        </div>
        <div class='col-lg-6 col-sm-8 col-xs-12 col-sm-offset-2'>
            <?= $form->field($organization, 'organization_web', ['showLabels' => false])->textInput(['maxlength' => true, 'placeholder' => 'http://www.caorganization.com'])->hint(Yii::t('app', 'Enter Organization homepage (optional)')) ?>
        </div>
    </div>


    <div class='form-group'>
        <?= Html::activeLabel($address, 'street1', ['label'=>ucfirst(Yii::t('app', 'address')), 'class'=>'col-sm-2 control-label']) ?>
        <div class='col-sm-9'>
            <?= $form->field($address, 'street1', ['showLabels' => false])->textInput(['maxlength' => true, 'placeholder' => \Yii::t('app', 'please enter street name, nr...')])->hint(Yii::t('app', 'Enter street name')) ?>
        </div>
        <div class='col-sm-9 col-sm-offset-2'>
            <?= $form->field($address, 'street2', ['showLabels' => false])->textInput(['maxlength' => true, 'placeholder' => ''])->hint(Yii::t('app', 'Enter street name (line 2, optional)')) ?>
        </div>
        <div class='col-sm-3 col-sm-offset-2'>
            <?= $form->field($address, 'city', ['showLabels' => false])->textInput(['maxlength' => true, 'placeholder' => strtolower(\Yii::t('app', 'City'))])->hint(Yii::t('app', 'Enter city')) ?>
        </div>
        <div class='col-sm-2'>
            <?= $form->field($address, 'county', ['showLabels' => false])->textInput(['maxlength' => true, 'placeholder' => strtolower(\Yii::t('app', 'County'))])->hint(Yii::t('app', 'Enter county (optional)')) ?>
        </div>
        <div class='col-sm-2'>
            <?= $form->field($address, 'zip', ['showLabels' => false])->textInput(['maxlength' => true, 'placeholder' => strtolower(Yii::t('app', 'Zip'))])->hint(Yii::t('app', 'Enter zip (optional)')) ?>
        </div>
    </div>    
    
</div>

<?php ob_start(); 
ActiveForm::end(); 
ob_get_clean(); ?>

