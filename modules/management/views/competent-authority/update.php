<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\management\models\CompetentAuthority */
/* @var $organization app\modules\management\models\Organization */
/* @var $address app\modules\management\models\Address */

$this->title = Yii::t('app/cpf', 'Update {modelClass}: ', [
    'modelClass' => 'Competent Authority',
]) . ' ' . $model->country;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/cpf', 'Competent Authorities'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->country, 'url' => ['view', 'id' => $model->country]];
$this->params['breadcrumbs'][] = Yii::t('app/cpf', 'Update');
?>
<div class="competent-authority-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'organization' => $organization,
        'address' => $address,
    ]) ?>

</div>
