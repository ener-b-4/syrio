<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\management\models\Organization */
/* @var $address \app\modules\management\models\Address */
/* @var $country_phone_call string */
/* @var $country_domain string */

/* breadcrumbs section */
use yii\helpers\Url;
$breadcrumbs = [
    [
        'label' => Yii::t('app', 'Management'),
        'url' => Url::to(['/management/']),
        //'template' =>"<li>{link}</li>
    ],
    [
        'label' => Yii::t('app', 'Organizations'),
        'url' => Url::to(['/management/organization']),
        //'template' =>"<li>{link}</li>
    ],
    [
        'label' => Yii::t('app', 'Create'),
        'url' => Url::to(['/management/organization/create']),
    ],
    [
        'label' => Yii::t('app', 'Competent Authority Organizations'),
    ]
];
$this->params['breadcrumbs'] = $breadcrumbs;
$this->title = Yii::t('app', 'Create {evt}' , ['evt' => Yii::t('app', 'Competent Authority Organization')]);

?>
<div class="competent-authority-organization-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'address' => $address,
        'country_phone_call' => $country_phone_call,
        'country_domain' => $country_domain
    ]) ?>

</div>
