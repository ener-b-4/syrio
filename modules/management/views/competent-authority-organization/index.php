<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

//use yii\helpers\Html;
use kartik\helpers\Html;
use app\assets\FontAwesomeAsset;
use kartik\grid\GridView;
use app\widgets\AddressWidget;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\management\models\CompetentAuthorityOrganizationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->registerAssetBundle(FontAwesomeAsset::className());

$this->title = Yii::t('app', 'Competent Authority Organizations');
$this->params['breadcrumbs'][] = $this->title;
?>

<?php

$allOrganizations = Yii::$app->user->can('sys_admin') ? 1 : 0;

/* SET UP GRIDVIEW */    
$grid_columns = [
    ['class' => 'yii\grid\SerialColumn'],
];

if ($allOrganizations == 1) {
    $grid_columns[] = [
        'class' => kartik\grid\DataColumn::className(),
        'attribute' => 'country',
        //'label' => Yii::t('app', 'Name'),
        'value' => 'country',
    ];
}


$grid_columns[] = 'organization_acronym';
$grid_columns[] = [
        'class' => kartik\grid\DataColumn::className(),
        'attribute' => 'organization_name',
        'label' => Yii::t('app', 'Name'),
        'value' => 'organization_name',
        ];
$grid_columns[] = [
        'class' => kartik\grid\DataColumn::className(),
        'attribute' => 'addressText',
        'label' => ucfirst(Yii::t('app', 'address')),
        'format' => 'html',
        'content' => function ($model, $key, $index, $column) {
            return AddressWidget::widget([
                    'style' => 1,
                    'address' => $model->address
                ]);    
            },
        ];

$grid_columns[] = [
    'class' => 'kartik\grid\ActionColumn',
    'dropdown' => true,
    'dropdownOptions' => [
        'class' => 'pull-right',
    ],
    'buttons' => [
        'sep1' => function ($url, $model) {
            return '<small class="text-muted" style="margin-left: 6px; margin-top:6px;">' . Yii::t('app', 'Organization') . '</small>';
        },
        'sep2' => function ($url, $model) {
            return '<small class="text-muted" style="margin-left: 6px; margin-top:6px;">' . ucfirst(Yii::t('app', 'users')) . '</small>';
        },
        'create-user' => function($url, $model) {
            $title = Yii::t('app', 'Create user');
            $options = []; // you forgot to initialize this
            $icon = '<i class="fa fa-user-plus"></i>';
            $label = $icon . ' ' . $title;
            //$url = Url::toRoute(['create-user','id'=>$model->id, 'country'=>$model->country]);
            $url = Url::toRoute(['/admin/users/user/create-ca-for','id'=>$model->id, 'country'=>$model->country]);
            $options['tabindex'] = '-1';
            $options['data-method'] = 'post';
            return '<li>' . Html::a($label, $url, $options) . '</li>' . PHP_EOL;            
        }
    ],
    'template' => '{sep1}{view}{edit}{delete}{sep2}{create-user}'
];




$grid_toolbar = [
        [
            'content'=>
                Html::button('<i class="glyphicon glyphicon-plus"></i>', [
                    'type'=>'button', 
                    'title'=>Yii::t('app', 'Add'), 
                    'class'=>'btn btn-success'
                ]) . ' '.
                Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['grid-demo'], [
                    'class' => 'btn btn-default', 
                    'title' => Yii::t('app', 'Reset Grid')
                ]),
            'options' => ['class' => 'btn-group-sm']
        ],
    ];   //end grid toolbar

?>



<div class="competent-authority-organization-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php //echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Competent Authority Organization'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php
        $sbox = Html::activeInput('text', $searchModel, 'global', [
           'class' => 'form-control' 
        ]);
        $search_mode_box = Html::input('text', 'search_mode', "0", ['id'=>'search_mode']);

//        $search_mode_box = Html::activeInput('text', $searchModel, 'search_mode', [
//           'class' => 'form-control' 
//        ]);
        echo     $search_mode_box;
    ?>
    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'pjax' => true,
        
        'columns' => $grid_columns,
        'toolbar' => $grid_toolbar,
        
        'responsive'=>true,
        'hover'=>true,
        'export'=>false,
        'panel' => [
                'type' => GridView::TYPE_PRIMARY,
                'heading' => '<i class="fa fa-building-o"></i> ' . Yii::t('app', 'Organizations'),
                'before'=>''
                            . '<div class=" col-lg-5 col-sm-7 col-xs-10">'
                            .   '<i class="fa fa-search" style="padding-right:6px; vertical-align:sub" ></i> '
                            .   '<span id="show_global_search">'
                            .   '     <a href="#" id="change_search" style="vertical-align:sub">field</a>'
                            .   '</span>'
                                    . '<div class="sy_search_container")>'
                                    . '<table>'
                                    . '<tr id="w1-filters" class="filters skip-export global-filter">'
                                    . '<td style="width:80%">'
                                    . $sbox
                                    . '</td>'
                                    . '</tr>'
                                    . '</table>'
                                . '</div>'
                    . '</div>'
,     //end before
                'after'=>'<span></span>',            
            ],                        
        
    ]); ?>

</div>

<?php
$js = <<<JS
        (function($) {
            $('#submitSearchButton').click(function(event){
                event.preventDefault();                
                
                var ser = $('#w0').serialize();
                //call pjax
                $.pjax({
                  url: 'index?' + ser,
                  container: '#w1-pjax'
                })        
            });
        
            /* check which search_mode is enabled */
            //var search_mode = $('#caosearch-search_mode').val();
            var search_mode = parseInt($('#search_mode').val());
            displaySearch(search_mode);
        })(jQuery);

   
        $( document ).ajaxSuccess(function() {
            //redraw the current search
            var search_mode = parseInt($('#search_mode').val());

            displaySearch(search_mode);
        
            updateFields();
        
            //re-register the show_global_search click event to the newly returned element
            $( '#show_global_search' ).click(function(event){
                    event.preventDefault();
                    switchSearch();
                });
        });
        
        
        $( '#show_global_search' ).click(function(event){
                //$('#w1-filters:not(.global-filter)').toggle('hidden');
                //$('.sy_search_container').removeClass('hidden');

                event.preventDefault();
                switchSearch();
            });
        
        
        function switchSearch() {
                var search_mode = $('#search_mode').val();
                if(typeof search_mode === 'undefined' || search_mode == ""){
                   search_mode = 1;
                 };
                search_mode = parseInt(search_mode);
                search_mode = (search_mode + 1) % 3;
                $('#search_mode').val(search_mode);
        
                displaySearch(search_mode);
        
                //alert($('#caosearch-search_mode').val());
        }
        
        function displaySearch(search_mode){
                switch(search_mode) 
                {
                    case 0:
                        //local search
                        $('#w1-filters:not(.global-filter)').removeClass('hidden'); //.toggle('hidden');
                        $('.sy_search_container').addClass('hidden');
                        $('.competent-authority-organization-search').addClass('hidden');
                        
                        $('#change_search').text('field');
                        break;
                    case 1:
                        //global search
                        $('#w1-filters:not(.global-filter)').addClass('hidden'); //.toggle('hidden');
                        $('.sy_search_container').removeClass('hidden');
                        $('.competent-authority-organization-search').addClass('hidden');

                        $('#change_search').text('global');
                        break;
                    case 2:
                        //advanced search
                        $('#w1-filters:not(.global-filter)').addClass('hidden'); //.toggle('hidden');
                        $('.sy_search_container').addClass('hidden');
                        $('.competent-authority-organization-search').removeClass('hidden');
        
                        $('#change_search').text('advanced');
                        break;
                    default:
                        //local search
                        $('#w1-filters:not(.global-filter)').removeClass('hidden'); //.toggle('hidden');
                        $('.sy_search_container').addClass('hidden');
                        $('.competent-authority-organization-search').addClass('hidden');
                        break;
                }
        }
        
        function updateFields()
        {
            var sPageURL = window.location.search.substring(1);
            sPageURL = fixedEncodeURI(sPageURL);
        
            var sURLVariables = sPageURL.split('&');
            for (var i = 0; i < sURLVariables.length; i++) 
            {
                var sParameterName = sURLVariables[i].split('=');
                //alert(sParameterName[0]);
                $('#'+sParameterName[0]).each(function(){
                    $(this).val(sParameterName[1]);
                }); 
            }        
        }
        
        /*
        This function replaces the %5B and %5D with [ and ]
        */
        function fixedEncodeURI (str) {
            //return encodeURI(str).replace(/%5B/g, '[').replace(/%5D/g, ']');
            return str.replace(/%5B/g, '[').replace(/%5D/g, ']');        
        }        
JS;

$this->registerJs($js, yii\web\View::POS_READY);
?>