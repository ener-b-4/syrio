<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\management\models\Organization */

$this->title = Yii::t('app/cpf', 'Update {modelClass}: ', [
    'modelClass' => 'Competent Authority Organization',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Competent Authority Organizations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id, 'country' => $model->country]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="competent-authority-organization-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'address' => $model->address,
    ]) ?>

</div>
