<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use app\assets\FontAwesomeAsset;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $country char2 of the CA country */
/* @var $searchModel app\modules\management\models\CompetentAuthorityOrganizationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->registerAssetBundle(FontAwesomeAsset::className());

$this->title = Yii::t('app', 'Competent Authority Organizations');
unset($this->params['breadcrumbs']);

$rootUrl = Yii::getAlias('@web');
$this->params['breadcrumbs'] = [
    'managementLink' => [
        'label' => Yii::t('app', 'Management'),
        'url' => $rootUrl . '/index.php/management'
    ],
    'caLink' => [
        'label' => Yii::t('app', 'Competent Authorities'),
        'url' => $rootUrl . '/index.php/management/competent-authority?country=' . $country,
    ]
];
$this->params['breadcrumbs'][] = Yii::t('app', 'Organizations');


$grid_columns = [
    ['class' => 'yii\grid\SerialColumn'],

    [
        'class' => kartik\grid\DataColumn::className(),
        'attribute' => 'organization_name',
        'label' => Yii::t('app', 'Name'),
        'value' => 'organization_name',
    ],
    'organization_acronym',

    ['class' => 'yii\grid\ActionColumn'],
];

$grid_toolbar = [
        [
            'content'=>
                Html::button('<i class="glyphicon glyphicon-plus"></i>', [
                    'type'=>'button', 
                    'title'=>Yii::t('app', 'Add'), 
                    'class'=>'btn btn-success'
                ]) . ' '.
                Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['grid-demo'], [
                    'class' => 'btn btn-default', 
                    'title' => Yii::t('app', 'Reset Grid')
                ]),
            'options' => ['class' => 'btn-group-sm']
        ],
    ];   //end grid toolbar

?>
<div class="competent-authority-organization-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php //echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Competent Authority Organization'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'pjax' => true,
        
        'columns' => $grid_columns,
        'toolbar' => $grid_toolbar,
        
        'responsive'=>true,
        'hover'=>true,
        'export'=>false,
        'panel' => [
                'type' => GridView::TYPE_PRIMARY,
                'heading' => '<i class="fa fa-building-o"></i> ' . Yii::t('app', 'Organizations'),
                'before'=>'<div class="sy_search_container")>'
            . '<i class="fa fa-search"></i> '
            . '<input type="text" class="searchAll sy_search_textbox" />'
            . '</div>',
                'after'=>'<span></span>',            
            ],                        
        
    ]); ?>

</div>
