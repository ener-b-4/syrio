<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\management\models\Organization */
/* "var $address \app\modules\management\models\Address */

/* breadcrumbs section */
use yii\helpers\Url;
$breadcrumbs = [
    [
        'label' => Yii::t('app', 'Management'),
        'url' => Url::to(['/management/']),
        //'template' =>"<li>{link}</li>
    ],
    [
        'label' => Yii::t('app', 'Organizations'),
        'url' => Url::to(['/management/organization']),
        //'template' =>"<li>{link}</li>
    ],
    [
        'label' => Yii::t('app', 'Create'),
        'url' => Url::to(['/management/organization/create']),
    ],
    [
        'label' => Yii::t('app', '{evt} Organization', ['evt'=>Yii::t('app', 'Operators/Owners')]),
    ]
];
$this->params['breadcrumbs'] = $breadcrumbs;

$this->title = Yii::t('app', 'Create {evt} Organization' , ['evt' => Yii::t('app', 'Operators/Owners')]);
?>
<div class="oo-organization-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'address' => $address,
    ]) ?>

</div>
