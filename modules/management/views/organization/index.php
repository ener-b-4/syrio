<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

//use yii\helpers\Html;
use kartik\helpers\Html;
use app\assets\FontAwesomeAsset;
use kartik\grid\GridView;
use app\widgets\AddressWidget;
use yii\helpers\Url;
use app\widgets\ActionMessageWidget;
use app\assets\SyrioGlyphsAsset;
use \app\modules\management\models\Organization;

/* @var $this yii\web\View */
/* @var $model app\modules\management\models\Organization */
/* @var $searchModel app\modules\management\models\OrganizationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->registerAssetBundle(FontAwesomeAsset::className());
SyrioGlyphsAsset::register($this);

/*
 * Breadcrumbs stuff
 */
$breadcrumbs = [
	[
		'label' => Yii::t('app', 'Management'),
		'url' => Url::to(['/management/']),
		//'template' =>"<li>{link}</li>
	],
];
$this->params['breadcrumbs'] = $breadcrumbs;

$this->title = Yii::t('app', 'Organizations');
$this->params['breadcrumbs'][] = $this->title;
?>

<?php

$allOrganizations = Yii::$app->user->can('sys_admin') || Yii::$app->user->identity->isCaUser ? 1 : 0;

$columns = [
	['class' => 'yii\grid\SerialColumn'],	
];

if ($allOrganizations == 1) {
	$temp = [
		[
			'class' => kartik\grid\DataColumn::className(),
			'attribute' => 'organizationType',
			'label' => ucfirst(Yii::t('app', 'type')),
			'format' => 'html',
			'width' => '200px',
			'content' => function ($model, $key, $index, $column) {
				//return 'type: ' . $model->organizationType;
				if ($model->organizationType == app\modules\management\models\Organization::COMPETENT_AUTHORITY) {
					return 'Competent Authority';
				}
				elseif ($model->organizationType == app\modules\management\models\Organization::OPERATORS_OWNERS) {
					return 'Operators / Owners';
				}
				},
			'filterType' => GridView::FILTER_SELECT2,   // '\kartik\widgets\Select2',
			'filter' => [
				'CA' => 'Competent Authority',
				'OO' => 'Operators / Owners'
			],
			'filterWidgetOptions' => [
				'pluginOptions' => ['allowClear'=>true],
			],
			'filterInputOptions' => ['placeholder'=>'Any type'],
			'format' => 'raw',
		]		
	];
				
	$columns = yii\helpers\ArrayHelper::merge($columns, $temp);
				
//	echo '<pre>';
//	echo var_dump($columns);
//	echo '</pre>';
//	die();
}

$temp = [
	'organization_acronym',
	[
		'class' => kartik\grid\DataColumn::className(),
		'attribute' => 'organization_name',
		'label' => Yii::t('app', 'Name'),
		'value' => 'organization_name',
	],
	[
		'class' => kartik\grid\DataColumn::className(),
		'attribute' => 'addressText',
		'label' => ucfirst(Yii::t('app', 'address')),
		'format' => 'html',
		'content' => function ($model, $key, $index, $column) {
			return AddressWidget::widget([
					'style' => 1,
					'address' => $model->address
				]);	
			},
	]	
];

$columns = yii\helpers\ArrayHelper::merge($columns, $temp);

/* ACTION COLUMNS SECTION */
$editColumn = [
	'class' => 'kartik\grid\ActionColumn',
        'header' => ucfirst(\Yii::t('app', 'actions')),
	'dropdown' => false,
	'dropdownOptions' => [
		'class' => 'pull-right',
	],
	'buttons' => [
		'delete' => function ($url, $model, $key) {
                        if ($model->organizationType == Organization::OPERATORS_OWNERS) {
                            if (!Yii::$app->user->can('oo-org-delete')) {
                                    return '';
                            }                            
                        } else {
                            if (!(Yii::$app->user->can('ca-org-delete'))) {
                                    return '';
                            }                            
                        }
    
                        
			$msg = Yii::t('app', 'Are you sure you want to delete {data}?', [
				'data'=>$model->organization_name]);
			$msg .= ' ';
			$msg .= Yii::t('app', 'Operation CAN be undone.');
			return $model->status === 10 || $model->status === 1 ? 
					Html::a('<i class="glyphicon glyphicon-trash"></i>', $url, [
						'title' => Yii::t('app', 'Remove'),
						'data-confirm' => $msg,
						'data-method' => 'POST']) : '';
			
		},

				/*20150923*/
		'update' => function ($url, $model, $key) {

			if ($model->organizationType == app\modules\management\models\Organization::COMPETENT_AUTHORITY 
					&& \Yii::$app->user->can('ca-org-update')) {
				$url = \Yii::$app->urlManager->createUrl([
					'management/competent-authority-organization/update', 'id'=>$model->id]);
                                
				return Html::a('<i class="glyphicon glyphicon-pencil"></i>', $url, [
					'title' => \Yii::t('app', 'Update')]);
				}
			elseif ($model->organizationType == app\modules\management\models\Organization::OPERATORS_OWNERS && \Yii::$app->user->can('oo-org-update')) {
				$url = \Yii::$app->urlManager->createUrl([
					'management/operators-owners-organization/update', 'id'=>$model->id]);
				return Html::a('<i class="glyphicon glyphicon-pencil"></i>', $url, [
					'title' => \Yii::t('app', 'Update')]);
			}
		},
				/* end 20150923*/


		'purge' => function ($url, $model, $key) {
				if (!Yii::$app->user->can('sys_admin') || $model->hasEvents)
				{
					return '';
				}

				$msg = Yii::t('app', 'Are you sure you want to PERMANENTLY delete {data}?', [
					'data'=>$model->organization_name]);
				$msg .= ' ';
				$msg .= Yii::t('app', 'Operation CAN NOT be undone.');
				return $model->status === 0 ? 
						Html::a('<i class="glyphicon glyphicon-remove"></i>', $url, [
							'title' => Yii::t('app/commands', 'Purge'),
							'data-confirm' => $msg,
							'data-method' => 'POST']) : '';
			},
		'reactivate' => function ($url, $model, $key) {
				if (!Yii::$app->user->can('sys_admin'))
				{
					return '';
				}
				$msg = Yii::t('app', 'This will reactivate {data}.', [
					'data'=>$model->organization_name]);
				$msg .= ' ';
				$msg .= Yii::t('app', 'Are you sure?');
				return $model->status === 0 ? 
						Html::a('<i class="glyphicon glyphicon-repeat"></i>', $url, [
							'title' => ucfirst(Yii::t('app', 'reactivate')), 
							'data-confirm' => $msg,
							'data-method'=>'POST']) : '';
			},
	],	  //end of buttons section
	'template' => '{view} {update} {delete} {reactivate} {purge}'
];

$actionColumn = [
	'class' => 'kartik\grid\ActionColumn',
	'header' => ucfirst(Yii::t('app', 'users')),
	'width' => '200px',
	'headerOptions' => [
		
	],
	'dropdown' => false,
	'dropdownOptions' => [
		'class' => 'pull-right',
	],
	'buttons' => [
		'static-users-badge' => function($url,$model) {
			return '<span class="badge" '
					. 'style="margin-right: 6px; cursor: context-menu">'
					. '' . $model->numberOfUsers. '</span>';
		},
		
		'create-user' => function($url, $model) {
			if ($model->organizationType == app\modules\management\models\Organization::COMPETENT_AUTHORITY)
			{
				if (Yii::$app->user->can('ca-user-create', []))
				{
					$title = Yii::t('app', 'Create {evt}', ['evt'=>Yii::t('app', 'CA user')]);
					$options = []; // initialize this
					$icon = '<i class="fa fa-user-plus"></i>';
					$label = $icon; // . ' ' . $title;
					//$url = Url::toRoute(['create-user','id'=>$model->id, 'country'=>$model->country]);
					$url = Url::toRoute(['/admin/users/user/create-ca-user','ca_org_id'=>$model->id]);
					$options['title'] = $title;
					$options['tabindex'] = '-1';
					$options['data-method'] = 'post';
					$options['data-toggle'] = 'tooltip';
					return Html::a($label, $url, $options) . PHP_EOL;			
				}
			}
			elseif ($model->organizationType == app\modules\management\models\Organization::OPERATORS_OWNERS)
			{
				if (Yii::$app->user->can('oo-user-create'))
				{
					$title = Yii::t('app', 'Create {evt}', ['evt'=>Yii::t('app', 'OO user')]);
					$options = []; // initialize this
					$icon = '<i class="fa fa-user-plus"></i>';
					$label = $icon; // . ' ' . $title;
					$url = Url::toRoute(['/admin/users/user/create-oo-user','org_id'=>$model->id]);
					$options['title'] = $title;
					$options['tabindex'] = '-1';
					$options['data-method'] = 'post';
					$options['data-toggle'] = 'tooltip';
					return Html::a($label, $url, $options) . PHP_EOL;			
				}
			}
		},
				
		'list-users' => function($url, $model) {
			if ($model->organizationType == app\modules\management\models\Organization::COMPETENT_AUTHORITY)
			{
				if (Yii::$app->user->can('ca-user-list'))
				{
					$title = Yii::t('app', 'List {evt}', ['evt'=>Yii::t('app', 'users')]);
					$options = []; // this must be initialized
					$icon = '<i class="fa fa-th"></i>';
					$label = $icon; // . ' ' . $title;
					$url = Url::toRoute(['/admin/users/user/index', 'UserSearch[org_acronym]'=>
						$model->organization_acronym]);
					$options['title'] = $title;
					$options['tabindex'] = '-1';
					$options['data-method'] = 'post';
					$options['data-toggle'] = 'tooltip';
					return Html::a($label, $url, $options) . PHP_EOL;			
				}
				else
				{
					return null;
				}
			}
			elseif ($model->organizationType == app\modules\management\models\Organization::OPERATORS_OWNERS)
			{
				if (Yii::$app->user->can('user-list'))
				{
					$title = Yii::t('app', 'List {evt}', ['evt'=>Yii::t('app', 'users')]);
					$options = []; // this must be initialized
					$icon = '</span><i class="fa fa-th"></i>';
					$label = $icon; // . ' ' . $title;
					$url = Url::toRoute(['/admin/users/user/index', 'UserSearch[org_acronym]'=>
						$model->organization_acronym]);
					$options['title'] = $title;
					$options['tabindex'] = '-1';
					$options['data-method'] = 'post';
					$options['data-toggle'] = 'tooltip';
					return Html::a($label, $url, $options) . PHP_EOL;			
				}
				else
				{
					return null;
				}
			}
		}
				
	],	  //end of buttons section
	'template' => '{static-users-badge} {list-users} {create-user}'
];
	

	
$actionInstallationsColumn = [
	'class' => 'kartik\grid\ActionColumn',
	'header' => Yii::t('app', 'Active Installations'),
	'width' => '200px',
	'headerOptions' => [
		
	],
	'dropdown' => false,
	'dropdownOptions' => [
		'class' => 'pull-right',
	],
	'buttons' => [
		'static-installations-badge' => function($url,$model) {
			return '<span class="badge" '
					. 'style="margin-right: 6px; cursor: context-menu">'
					. '' . $model->getNumberOfInstallations(null) . '</span>';
		},
		
		'create-installation' => function($url, $model) {
			if (Yii::$app->user->can('oo-inst-create'))
			{
				$title = Yii::t('app', 'Create {evt}', ['evt'=>\Yii::t('app', 'installation')]);
				$options = []; // initialize this
				$icon = '<span class="icon-syrio_glyphs"></span>' . '<sup style="margin-left: -3px"><i class="fa fa-plus"></i></sup>';
				$label = $icon; // . ' ' . $title;
				if ($model->organizationType == Organization::OPERATORS_OWNERS)
				{
					$url = Url::toRoute(['/management/installations/create-for', 'org_id'=>$model->id]);
				}
				else
				{
					$url = Url::toRoute(['/management/installations/create']);
				}
				$options['title'] = $title;
				$options['tabindex'] = '-1';
				$options['data-method'] = 'post';
				$options['data-toggle'] = 'tooltip';
				return Html::a($label, $url, $options) . PHP_EOL;			
			}
		},
				
		'list-installations' => function($url, $model) {
			if (Yii::$app->user->can('oo-inst-list'))
			{
				$options = []; // this must be initialized
				$icon = '</span><i class="fa fa-th"></i>';
				$label = $icon; // . ' ' . $title;

                                if ($model->organizationType == Organization::COMPETENT_AUTHORITY) {
                                    $org_acronym = null;
                                    $title = Yii::t('app', 'Installations in jurisdiction');
                                } else {
                                    $org_acronym = $model->organization_acronym;
                                    $title = $model->organization_acronym . ' ' . Yii::t('app', 'installations');
                                }
                                
				$url = (\Yii::$app->user->can('sys_admin') || \Yii::$app->user->identity->isCaUser || \Yii::$app->user->can('oo-inst-list')) ?
                                        //Url::toRoute(['/management/installations/index'])
                                        Url::toRoute(['/management/installations/index', 'InstallationsSearch[operatorAcronym]'=> $org_acronym]) : '#';
				$options['title'] = $title;
				$options['tabindex'] = '-1';
				$options['data-method'] = 'post';
				$options['data-toggle'] = 'tooltip';
				return Html::a($label, $url, $options) . PHP_EOL;			
			}
			else
			{
				return null;
			}
		}
				
	],	  //end of buttons section
	'template' => '{static-installations-badge} {list-installations} {create-installation}'
];
		
	
	
$columns[] = $editColumn;
$columns[] = $actionInstallationsColumn;
$columns[] = $actionColumn;

	
/* SET UP GRIDVIEW */	
			
$grid_columns[] = [
	'class' => 'kartik\grid\ActionColumn',
	'dropdown' => true,
	'dropdownOptions' => [
		'class' => 'pull-right',
	],
	'buttons' => [
		'sep1' => function ($url, $model) {
			return '<small class="text-muted" style="margin-left: 6px; margin-top:6px;">' . Yii::t('app', 'Organization') . '</small>';
		},
		'sep2' => function ($url, $model) {
			return '<small class="text-muted" style="margin-left: 6px; margin-top:6px;">' . ucfirst(Yii::t('app', 'users')) . '</small>';
		},
		'create-user' => function($url, $model) {
			$title = Yii::t('app', 'Create user');
			$options = []; // you forgot to initialize this
			$icon = '<i class="fa fa-user-plus"></i>';
			$label = $icon . ' ' . $title;
			//$url = Url::toRoute(['create-user','id'=>$model->id, 'country'=>$model->country]);
			$url = Url::toRoute(['/admin/users/user/create-ca-for','id'=>$model->id, 'country'=>$model->country]);
			$options['tabindex'] = '-1';
			$options['data-method'] = 'post';
			return '<li>' . Html::a($label, $url, $options) . '</li>' . PHP_EOL;			
		},
		'purge' => function ($url, $model, $key) {
				if (!Yii::$app->user->can('sys_admin'))
				{
					return '';
				}

				$msg = Yii::t('app', 'Are you sure you want to PERMANENTLY delete {data}?', [
					'data'=>$model->full_name]);
				$msg .= ' ';
				$msg .= Yii::t('app', 'Operation CAN NOT be undone.');
				return $model->status === 0 ? 
						Html::a('<i class="glyphicon glyphicon-remove"></i>', $url, [
							'title' => Yii::t('app/commands', 'Purge'),
							'data-confirm' => $msg,
							'data-method' => 'POST']) : '';
			},
		'reactivate' => function ($url, $model, $key) {
				if (!Yii::$app->user->can('sys_admin'))
				{
					return '';
				}
				return $model->status === 0 ? 
						Html::a('<i class="glyphicon glyphicon-repeat"></i>', $url, [
							'title' => Yii::t('app', 'Reactivate'), 
							'data-method'=>'POST']) : '';
			},
				
	],
	'template' => '{sep1} {view} {edit} {delete} {purge} {reactivate} {sep2} {create-user}'
];

$grid_toolbar = [
		[
			'content'=>
				Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], [
					'class' => 'btn btn-success',
				]) .
				//Html::button('<i class="glyphicon glyphicon-plus"></i>', [
				//	'type'=>'button', 
				//	'title'=>Yii::t('app', 'Create Organization'), 
				//	'class'=>'btn btn-success',
				//]) . ' '.
				Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['grid-demo'], [
					'class' => 'btn btn-default', 
					'title' => Yii::t('app', 'Reset Grid')
				]),
			'options' => ['class' => 'btn-group-sm']
		],
	];   //end grid toolbar

?>



<div class="competent-authority-organization-index">

	<?php
		$session_msg = Yii::$app->session->get('finished_action_result');	 //$session_msg is of type ActionMessage
		if (isset($session_msg))
		{
			echo ActionMessageWidget::widget(['actionMessage' => $session_msg,]);

			//disable the next line if you want the message to persist
			unset(Yii::$app->session['finished_action_result']);
		}
	?>	

	<h1><?= Html::encode($this->title) ?></h1>
	<?php //echo $this->render('_search', ['model' => $searchModel]); ?>

	<?php
		$sbox = Html::activeInput('text', $searchModel, 'global', [
		   'class' => 'form-control collapsed' 
		]);
		
		$search_mode_box = Html::input('text', 'search_mode', "0", ['id'=>'search_mode', 'style'=>'display: none']);
		echo $search_mode_box;
	?>
	
	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'pjax' => true,
		'rowOptions' => function($model) {
			return $model->status == app\modules\management\models\Organization::STATUS_DELETED ? ['class' => 'danger'] : [];
		},
		'columns' => $columns,  // $grid_columns,
		'toolbar' => $grid_toolbar,
		
		'responsive'=>true,
		'hover'=>true,
		'export'=>false,
		'panel' => [
				'type' => GridView::TYPE_PRIMARY,
				'heading' => '<i class="fa fa-building-o"></i> ' . Yii::t('app', 'Organizations'),
				'after'=>'<span></span>',			
			],						
		
	]); ?>

</div>

