<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

/* var app\modules\management\models\Organization $model */

use kartik\widgets\ActiveForm;
use kartik\helpers\Html;

/* breadcrumbs section */
use yii\helpers\Url;
$breadcrumbs = [
    [
        'label' => Yii::t('app', 'Management'),
        'url' => Url::to(['/management/']),
        //'template' =>"<li>{link}</li>
    ],
    [
        'label' => Yii::t('app', 'Organizations'),
        'url' => Url::to(['/management/organization']),
        //'template' =>"<li>{link}</li>
    ],
    [
        'label' => Yii::t('app', 'Create'),
    ]
];
$this->params['breadcrumbs'] = $breadcrumbs;



if (property_exists(get_class($model), 'organizationType'))
{
    $_org_type = Yii::t('app', 'no');
}
else
{
    try {
        if ($model->organizationType == \app\modules\management\models\Organization::COMPETENT_AUTHORITY)
        {
            $_org_type = Yii::t('app', 'Competent Authority');
        }
        elseif ($model->organizationType == \app\modules\management\models\Organization::OPERATORS_OWNERS)
        {
            $_org_type = Yii::t('app', 'Operators / Owners');
        }
    } catch (Exception $ex) {
        $_org_type = '';
    }
    
    
    
}
?>

<div class="syrio_create_organization">
    <h1><?= \Yii::t('app','Organizations') ?></h1>
    <p><?= \Yii::t('app', 'Create {evt} Organization', ['evt'=>$_org_type]) ?></p>
    
    <?php $form = ActiveForm::begin() ?>
    
    <?= $form->field($model, 'organization_type')->dropDownList($model->getOrganizationTypes()); ?>
    
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Next'), ['class' => 'btn btn-primary']) ?>
    </div>

    <?php $form->end() ?>
    
</div>