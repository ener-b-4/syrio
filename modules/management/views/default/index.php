<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */


    use yii\web\View;
    use app\modules\management\assets\ManagementAsset;
    use kartik\helpers\Html;
    use app\assets\FontAwesomeAsset;
    use app\assets\SyrioGlyphsAsset;
    
    ManagementAsset::register($this);
    FontAwesomeAsset::register($this);
    SyrioGlyphsAsset::register($this);
    
    $this->title = \Yii::t('app', 'Management Hub');
    
?>

<div class="management-default-index">
</div>

<div class="row">
    <div class="col-lg-12">
        <div class='management-container' class='col-lg-12'>
            <div class='col-lg-2 col-sm-3 col-xs-12'>
                <div class='sy_click_panel' data-link="<?= Yii::$app->urlManager->createUrl(['management/organization/index']) ?>">
                    <div class="sy_click_content">
                        <i class="fa fa-building-o" style='font-size: 36px;'></i>
                        <?= ucfirst(Yii::t('app', 'organizations')) ?>
                    </div>
                </div>
            </div>
            <div class='col-lg-2 col-sm-3 col-xs-12'>
                <div class='sy_click_panel' data-link="<?= Yii::$app->urlManager->createUrl(['management/installations/index']) ?>">
                    <div class="sy_click_content">
                        <span class="icon-syrio_glyphs" style='font-size: 36px;'></span>
                        <?= ucfirst(Yii::t('app', 'installations')) ?>
                    </div>
                </div>
            </div>
            <div class='col-lg-2 col-sm-3 col-xs-12'>
                <div class='sy_click_panel' data-link="<?= Yii::$app->urlManager->createUrl(['admin/users/user']) ?>">
                    <div class="sy_click_content">
                        <i class="fa fa-users" style='font-size: 36px;'></i>
                        <?= ucfirst(Yii::t('app', 'users')) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row" style="margin-top: 24px; border-top: 1px solid rgb(200,200,200); padding-top:24px;">
    <div class="col-lg-12">
        <div class='management-container' class='col-lg-12'>
            <div class='col-lg-2 col-sm-3 col-xs-12'>
                <div class='sy_click_panel' data-link="<?= Yii::$app->urlManager->createUrl(['/units']) ?>">
                    <div class="sy_click_content">
                        <i class="fa fa-percent" style='font-size: 36px;'></i>
                        <?= ucfirst(\Yii::t('app', 'units')) ?>
                    </div>
                </div>
            </div>
            <div class='col-lg-2 col-sm-3 col-xs-12'>
                <div class='sy_click_panel' data-link="<?= Yii::$app->urlManager->createUrl(['/admin/holidays']) ?>">
                    <div class="sy_click_content">
                        <i class="fa fa-calendar" style='font-size: 36px;'></i>
                        <?= ucfirst(\Yii::t('app', '{n, plural, =1{Holiday} other{Holidays}}', ['n'=>2])) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php if (\Yii::$app->user->can('sys_admin') || \Yii::$app->user->can('ca_admin')) { ?>

<div class="row" style="margin-top: 24px; border-top: 1px solid rgb(200,200,200); padding-top:24px;">
    <div class="col-lg-12">
        <div class='management-container' class='col-lg-12'>
            <div class='col-lg-2 col-sm-3 col-xs-12'>
                <div class='sy_click_panel' data-link="<?= Yii::$app->urlManager->createUrl(['/translation']) ?>">
                    <div class="sy_click_content">
                        <i class="glyphicon glyphicon-text-size" style='font-size: 36px;'></i>
                        <?= ucfirst(\Yii::t('app/translations', 'translations')) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php } ?>

<?php
    //$this->registerJsFile( 'ui_interaction.js',['depends' => [\yii\web\JqueryAsset::className()]]); 
?>