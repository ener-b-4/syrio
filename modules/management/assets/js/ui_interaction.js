/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

(function() {
    $('.sy_click_panel').each(function(){
       //console.log($(this).attr('data-link')) ;
       //alert($(this).attr('data-link'));
       $that = $(this);
       
       $that.click(function(evt){
          window.location.href = $(this).attr('data-link');
       });
       
       $that.mouseenter(function() {
           $(this).addClass('hover');
       });

        $that.mouseleave(function() {
           $(this).removeClass('hover');
           $(this).removeClass('mouse-down');
       });
       
       $that.mousedown(function() {
           $(this).removeClass('hover');
           $(this).addClass('mouse-down');
       })
       
    });
})();