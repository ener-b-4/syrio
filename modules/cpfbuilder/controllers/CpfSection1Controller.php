<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\modules\cpfbuilder\controllers;

use Yii;
use app\modules\cpfbuilder\models\CpfSection1;
use app\modules\cpfbuilder\models\CpfSection1Search;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CpfSection1Controller implements the CRUD actions for CpfSection1 model.
 */
class CpfSection1Controller extends Controller
{
    /**
     * Add this to ensure that only the CAusers (actually CA rapporteurs) have access to this
     * @param type $action
     * @return boolean
     * 
     * @version 1.0.20151009
     * @author vamanbo <john.doe@example.com>
     */
    public function beforeAction($action) {
        //if (!Yii::$app->user->can('ca_generate_annual_report'))
        if (!(Yii::$app->user->can('ca_user') || Yii::$app->user->can('sys_admin')))
        {
            return Yii::$app->error->ThrowError(Errors::RESTRICTED_AREA_ERROR);
        }
        return parent::beforeAction($action);        
    }
    
    
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all CpfSection1 models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CpfSection1Search();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CpfSection1 model.
     * @param integer $year
     * @param integer $user_id
     * @return mixed
     */
    public function actionView($year, $user_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($year, $user_id),
        ]);
    }


    /**
     * Finds the CpfSection1 model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $year
     * @param integer $user_id
     * @return CpfSection1 the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($year, $user_id)
    {
        if (($model = CpfSection1::findOne(['year' => $year])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
