<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\modules\cpfbuilder\controllers;

use Yii;
use app\modules\cpfbuilder\models\CpfReferences;
use app\modules\cpfbuilder\models\CpfReferencesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use app\components\Errors;

/**
 * CpfReferencesController implements the CRUD actions for CpfReferences model.
 */
class CpfReferencesController extends Controller
{
    
    /**
     * Add this to ensure that only the CAusers (actually CA rapporteurs) have access to this
     * @param type $action
     * @return boolean
     * 
     * @version 1.0.20160218
     * @author vamanbo <john.doe@example.com>
     */
    public function beforeAction($action) {
        if (!Yii::$app->user->can('ca_generate_annual_report'))
        {
            return Yii::$app->error->ThrowError(Errors::RESTRICTED_AREA_ERROR);
            return FALSE;
        }
        return parent::beforeAction($action);        
    }
    
    
    
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    
    /**
     * Lists all CpfReferences models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CpfReferencesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new CpfReferences model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CpfReferences();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if (isset($model->_ref)) {
                return $this->redirect($model->_ref);
            } else {
                return $this->redirect(['index']);
            }
        } else {
            
            if ($model->hasProperty('_ref')) {
                if (\Yii::$app->request->referrer!== $model->_ref) {
                    $model->_ref = \Yii::$app->request->referrer;
                }
            }
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing CpfReferences model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing CpfReferences model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        //only delete a title that is not referenced
        $model = $this->findModel($id);
        
        if (isset($model->cpfs) && $model->getCpfs()->count()>0) {
            return Yii::$app->error->ThrowError(Errors::RESTRICTED_OPERATION_ERROR);
        }
        
        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CpfReferences model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CpfReferences the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CpfReferences::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
