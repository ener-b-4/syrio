<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\modules\cpfbuilder\controllers;

use Yii;
use app\modules\cpfbuilder\models\CpfInstOpArea;
use app\modules\cpfbuilder\models\CpfInstOpAreaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CpfInstOpAreaController implements the CRUD actions for CpfInstOpArea model.
 */
class CpfInstOpAreaController extends Controller
{
    /**
     * Add this to ensure that only the CAusers (actually CA rapporteurs) have access to this
     * @param type $action
     * @return boolean
     * 
     * @version 1.0.20151009
     * @author vamanbo <john.doe@example.com>
     */
    public function beforeAction($action) {
        if (!Yii::$app->user->can('ca_generate_annual_report'))
        {
            return Yii::$app->error->ThrowError(Errors::RESTRICTED_AREA_ERROR);
            return FALSE;
        }
        return parent::beforeAction($action);        
    }
    
    
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all CpfInstOpArea models.
     * @return mixed
     */
    public function actionIndex()
    {
        return new NotFoundHttpException();
        $searchModel = new CpfInstOpAreaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CpfInstOpArea model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CpfInstOpArea model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($year, $user_id, $installation_id)
    {
        $model = new CpfInstOpArea();
        $model->installation_id = $installation_id;
        $model->user_id = $user_id;
        $model->year = $year;
        
        //$installation = \app\modules\cpfbuilder\models\CpfInstallations::find(['id'=>$installation_id])->one();
        //$model->year = $installation->year;
        //$model->user_id = $installation->user_id;
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            $url =\Yii::$app->urlManager->createUrl([ '/cpfbuilder/cpf-sessions/view',                 
                'year' => $model->year, 'user_id'=>$model->user_id]);
            return $this->redirect($url);
            
            
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }


    /**
     * Deletes an existing CpfInstOpArea model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->delete();

            $url =\Yii::$app->urlManager->createUrl([ '/cpfbuilder/cpf-sessions/view',                 
                'year' => $model->year, 'user_id'=>$model->user_id]);
            return $this->redirect($url);
    }

    /**
     * Finds the CpfInstOpArea model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CpfInstOpArea the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CpfInstOpArea::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
