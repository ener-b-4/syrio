<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\modules\cpfbuilder\controllers;

use Yii;
use app\modules\cpfbuilder\models\CpfSection3;
use app\modules\cpfbuilder\models\CpfSection3Search;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CpfSection3Controller implements the CRUD actions for CpfSection3 model.
 */
class CpfSection3Controller extends Controller
{
    /**
     * Add this to ensure that only the CAusers (actually CA rapporteurs) have access to this
     * @param type $action
     * @return boolean
     * 
     * @version 1.0.20151009
     * @author vamanbo <john.doe@example.com>
     */
    public function beforeAction($action) {
        //if (!Yii::$app->user->can('ca_generate_annual_report'))
        if (!(Yii::$app->user->can('ca_user') || Yii::$app->user->can('sys_admin')))
        {
            return Yii::$app->error->ThrowError(Errors::RESTRICTED_AREA_ERROR);
        }
        return parent::beforeAction($action);        
    }
    
    
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all CpfSection3 models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CpfSection3Search();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CpfSection3 model.
     * @param integer $year
     * @param integer $user_id
     * @return mixed
     */
    public function actionView($year, $user_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($year, $user_id),
        ]);
    }

    /**
     * Updates an existing CpfSection3 model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $year
     * @param integer $user_id
     * @return mixed
     */
    public function actionUpdate($year, $user_id)
    {
        if (!Yii::$app->user->can('ca_generate_annual_report'))
        {
            return Yii::$app->error->ThrowError(Errors::RESTRICTED_AREA_ERROR);
        }
        
        $model = $this->findModel($year, $user_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $url =\Yii::$app->urlManager->createUrl([ '/cpfbuilder/cpf-sessions/view',                 
                'year' => $model->year, 'user_id'=>$model->user_id]);
            return $this->redirect($url);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
    

    /**
     * Finds the CpfSection3 model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $year
     * @param integer $user_id
     * @return CpfSection3 the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($year, $user_id)
    {
        if (($model = CpfSection3::findOne(['year' => $year])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    
    public function actionRemoveReference() {
        if (!key_exists('id', $_POST)) {
            throw new NotFoundHttpException(\Yii::t('app', 'Page not found'));
        }
        
        $id = $_POST['id'];
        
        if (!filter_var($id, FILTER_VALIDATE_INT) === false) {
            //remove the link reference
            \Yii::$app->db->createCommand()->delete('{{cpf_report_refs}}', '[[ref_id]] = :x', ['x'=>intval($id)])->execute();
            return "{'success': $id }";
        }
        else {
            return "failed";
        }
        return 'remove ';// . $id;
    }
    
    
    public function actionReferences($year, $user_id) {
        $model = $this->findModel($year, $user_id);
        
        if (\Yii::$app->request->post()) {
            //update references
            
            if (!key_exists('ids', $_POST)) {
                throw new NotFoundHttpException(\Yii::t('app', 'Page not found'));
            }
            
            $ids = $_POST['ids'];
            
            $transaction = \Yii::$app->db->beginTransaction();
            
            foreach($ids as $id) {
                if (!filter_var($id, FILTER_VALIDATE_INT) === false) {
                    //create a new link
                    \Yii::$app->db->createCommand()->insert('{{cpf_report_refs}}', ['year'=>$year, 'user_id'=>$user_id, 'ref_id'=>$id])->execute();
                }
                else {
                    throw new \InvalidArgumentException('bad id');
                }
            }
            
            $transaction->commit();
            
            $url =\Yii::$app->urlManager->createUrl([ '/cpfbuilder/cpf-sessions/view',                 
                'year' => $model->year, 'user_id'=>$model->user_id]);            
            return $this->redirect($url, ['year'=>$year, 'user_id'=>$user_id]);
        } else {
            //$url =\Yii::$app->urlManager->createUrl([ '/cpfbuilder/cpf-sessions/addReferences',                 
            //    'model'=>$this]);            
            
            $searchModel = new \app\modules\cpfbuilder\models\CpfReferencesSearchEx();
            $searchModel->year=$year;
            $searchModel->user_id=$user_id;
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            
            return $this->render('addReferences', ['model'=>$model, 'searchModel' => $searchModel, 'dataProvider'=>$dataProvider]);
        }
    }
}
