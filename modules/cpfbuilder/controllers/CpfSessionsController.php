<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\modules\cpfbuilder\controllers;

use Yii;
use app\modules\cpfbuilder\models\CpfSessions;
use app\modules\cpfbuilder\models\CpfSessionsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \app\components\Errors;
use app\models\common\ActionMessage;

use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * CpfSessionsController implements the CRUD actions for CpfSessions model.
 */
class CpfSessionsController extends Controller
{
    /**
     * Add this to ensure that only the CAusers (actually CA rapporteurs) have access to this
     * @param type $action
     * @return boolean
     * 
     * @version 1.0.20151009
     * @author vamanbo <john.doe@example.com>
     */
    public function beforeAction($action) {
        if (!(Yii::$app->user->can('ca_user') || Yii::$app->user->can('sys_admin')))
        {
            return Yii::$app->error->ThrowError(Errors::RESTRICTED_AREA_ERROR);
            return FALSE;
        }
        return parent::beforeAction($action);        
    }
    
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'reset' => ['post'],
                    'generate_xml' => ['post']
                ],
            ],
        ];
    }

    /**
     * Lists all CpfSessions models.
     * @return mixed
     */
    public function actionIndex()
    {
        //the access is allowed to all of the CA users (dealt with in the beforeAction)
        
        $searchModel = new CpfSessionsSearch();     //this one already returns the cpf sessions of the current user
        
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CpfSessions model.
     * @param integer $year
     * @param integer $user_id
     * @return mixed
     */
    public function actionView($year)
    {
        //check if user can create the report and pass the parameter to the view
        $readonly = !(Yii::$app->user->can('ca_generate_annual_report'));
        
        return $this->render('view', [
            'model' => $this->findModel($year),
            //'readonly' => $readonly
        ]);
    }

    /**
     * Creates a new CpfSessions model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (!\Yii::$app->user->can('ca_generate_annual_report'))
        {
            return \Yii::$app->error->ThrowError(Errors::RESTRICTED_AREA_ERROR);
        }
        
        $model = new CpfSessions();
        $model->user_id = \Yii::$app->user->id;
        
        if (\Yii::$app->request->isAjax && $model->load(\Yii::$app->request->post())) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        
        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate())
            {
                $transaction = \Yii::$app->db->beginTransaction();
                $ok = 1;
                
                /*20150923*/
                $section2 = \app\modules\cpfbuilder\models\CpfSection2::findOne(
                        ['year'=>$model->year, 'user_id'=>$model->user_id]);
                if (isset($section2))
                {
                    $section2->delete();
                }

                
                $model->refresh_data = 1;
                $model->refresh_installations = 1;
                
                //save the model
                if ($model->save())
                {
                    //create all the sections
                    $section1 = new \app\modules\cpfbuilder\models\CpfSection1();
                    $section1->year = $model->year;
                    $section1->user_id = $model->user_id;
                    $section1->ms = \Yii::$app->params['competentAuthorityIso2'];
                    $section1->ca = \Yii::$app->params['competentAuthority'];
                    $section1->dra = \Yii::$app->user->identity->organization->organization_name;
                    $section1->phone = !\Yii::$app->user->identity->phone ? '-' : \Yii::$app->user->identity->phone;
                    $section1->email = \Yii::$app->user->identity->email;
                    
                    $section2 = new \app\modules\cpfbuilder\models\CpfSection2();
                    $section2->year = $model->year;
                    $section2->user_id = $model->user_id;
                    

                    $section3 = new \app\modules\cpfbuilder\models\CpfSection3();
                    $section3->year = $model->year;
                    $section3->user_id = $model->user_id;

                    $section4 = new \app\modules\cpfbuilder\models\CpfSection4();
                    $section4->year = $model->year;
                    $section4->user_id = $model->user_id;

                    
                    if ($section1->save() && $section2->save() && $section3->save() && $section4->save())
                    {
                        $transaction->commit();
                    }
                    else
                    {
                        $transaction->rollBack();
                        $ok=0;
                    }
                }
                else
                {
                    $transaction->rollBack();
                    $ok=0;
                }

                
                if (!$ok) {
                    echo 'unable to save.';
                    die();
                }
            }
            
            return $this->redirect(['index']);
            //return $this->redirect(['view', 'year' => $model->year, 'user_id' => $model->user_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing CpfSessions model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $year
     * @param integer $user_id
     * @return mixed
     */
    public function actionUpdate($year)
    {
        if (!\Yii::$app->user->can('ca_generate_annual_report'))
        {
            return \Yii::$app->error->ThrowError(Errors::RESTRICTED_AREA_ERROR);
        }
        
        $model = $this->findModel($year);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'year' => $model->year, 'user_id' => $model->user_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing CpfSessions model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $year
     * @param integer $user_id
     * @return mixed
     */
    public function actionDelete($year)
    {
        $actionMessage = new ActionMessage();
        
        if (!\Yii::$app->user->can('ca_generate_annual_report'))
        {
            return \Yii::$app->error->ThrowError(Errors::RESTRICTED_AREA_ERROR);
        }
        
        $this->findModel($year)->delete();

        $msg = \Yii::t('app/messages', 'The {evt} Annual Report has been deleted (the report AND individual incident preparation files have been deleted)', ['evt'=>$year]);
        
        $actionMessage->SetSuccess(
                Yii::t('app', 'Annual report').' '.'deleted', $msg); 

        Yii::$app->session->set('finished_action_result', $actionMessage);
        return $this->redirect(['index']);
    }

    /**
     * Resets an annual report WITHOUT deleting the individual case assessments!
     * 
     * @param type $year
     * 
     * @since 1.3
     */
    public function actionReset($year) {
        
        $actionMessage = new ActionMessage();
        
        $hostInfo = Yii::$app->request->hostInfo;   //e.g. http://localhost:8080
        $referrer = Yii::$app->request->referrer;   //e.g. http://localhost:8080/index.php?r=cpfbuilder%2Fcpf-sessions

        if ((strpos($referrer, $hostInfo) > -1) && $referrer!==Yii::$app->request->absoluteUrl) {
            $return_url = $referrer;
        } 
        
        if (!\Yii::$app->user->can('ca_generate_annual_report'))
        {
            return \Yii::$app->error->ThrowError(Errors::RESTRICTED_AREA_ERROR);
        }
        
        $current_report = $this->findModel($year);
        
        /*
         * set current reports' year to 9999
         * save the current report to cascade the new year to the individual crf_cpf_assessments
         * 
         */
        $current_report->year = 9999;
        $current_report->save();

        //create session
        $cpf_session = new \app\modules\cpfbuilder\models\CpfSessions();
        $cpf_session->year = $year;
        $cpf_session->user_id = Yii::$app->user->id;

        $transaction = Yii::$app->db->beginTransaction();
        
        if (!$this->createCPF($cpf_session)) {
            $transaction->rollBack();

            $msg = \Yii::t('app/messages/errors', "There's been an error while processing your request.");
            $msg+= '<br/>'.'ERR: '.'error while creating the empty cpf_session';

            $actionMessage->SetError(
                    Yii::t('app', 'Annual Report Reset'), $msg); 

            Yii::$app->session->set('finished_action_result', $actionMessage);
            return $this->redirect(['index']);
        }

        if (!$cpf_session->save()) {
            $transaction->rollBack();
            
            $msg = \Yii::t('app/messages/errors', "There's been an error while processing your request.");
            $msg+= '<br/>'.'ERR: '.'unable to create new cpf_session';

            $actionMessage->SetError(
                    Yii::t('app', 'Annual Report Reset'), $msg); 

            Yii::$app->session->set('finished_action_result', $actionMessage);
            return $this->redirect(['index']);
            
        }
        
        /*
         * get all the crf_cpf assessments from $current report and set the year and user_id as for the new one
         */

        foreach($current_report->assessedReports as $crf_cpf_report) {
            $crf_cpf_report->year = $year;
            $crf_cpf_report->user_id = Yii::$app->user->id;
            if (!$crf_cpf_report->save()) {
                $transaction->rollBack();
                
                $msg = \Yii::t('app/messages/errors', "There's been an error while processing your request.");
                $msg+= '<br/>'.'ERR: '.'unable to transfer individual report assessments';
                
                $actionMessage->SetError(
                        Yii::t('app', 'Annual Report Reset'), $msg); 

                Yii::$app->session->set('finished_action_result', $actionMessage);
                return $this->redirect(['index']);
            }
        }
        
        /*
         * delete the 9999
         */
        $current_report->delete();
        $transaction->commit();
        
        $msg = \Yii::t('app/messages', 'The {evt} Annual Report has been reset (all the information has been cleared)', ['evt'=>$year]);
        
        $actionMessage->SetSuccess(
                Yii::t('app', 'Annual Report Reset'), $msg); 

        Yii::$app->session->set('finished_action_result', $actionMessage);
        
        
        if (isset($return_url)) {
            $this->redirect($return_url);
        } else {
            $this->redirect(['index']);
        }
    }


    private function createCPF($model) {
        
        //die ('cpfmodel');
            if ($model->validate())
            {
                $transaction = \Yii::$app->db->beginTransaction();
                $ok = 1;
                
                /*20150923*/
                $section2 = \app\modules\cpfbuilder\models\CpfSection2::findOne(
                        ['year'=>$model->year]);
                if (isset($section2))
                {
                    $section2->delete();
                }

                //save the model
                if ($model->save())
                {
                    //create all the sections
                    $section1 = new \app\modules\cpfbuilder\models\CpfSection1();
                    $section1->year = $model->year;
                    $section1->user_id = $model->user_id;
                    $section1->ms = \Yii::$app->params['competentAuthorityIso2'];
                    $section1->ca = \Yii::$app->params['competentAuthority'];
                    $section1->dra = \Yii::$app->user->identity->organization->organization_name;
                    $section1->phone = !\Yii::$app->user->identity->phone ? '-' : \Yii::$app->user->identity->phone;
                    $section1->email = \Yii::$app->user->identity->email;
                    
                    $section2 = new \app\modules\cpfbuilder\models\CpfSection2();
                    $section2->year = $model->year;
                    $section2->user_id = $model->user_id;
                    

                    $section3 = new \app\modules\cpfbuilder\models\CpfSection3();
                    $section3->year = $model->year;
                    $section3->user_id = $model->user_id;

                    $section4 = new \app\modules\cpfbuilder\models\CpfSection4();
                    $section4->year = $model->year;
                    $section4->user_id = $model->user_id;

                    
                    if ($section1->save() && $section2->save() && $section3->save() && $section4->save())
                    {
                        $transaction->commit();
                    }
                    else
                    {
                        $transaction->rollBack();
                        $ok=0;
                    }
                }
                else
                {
                    $transaction->rollBack();
                    $ok=0;
                }

                
                if (!$ok) {
                    echo 'unable to save.';
                    return false;
                }
                return true;
            } else {
                echo 'validation error.';
                return false;
            }
        
    }




    /**
     * Finds the CpfSessions model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $year
     * @param integer $user_id
     * @return CpfSessions the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($year)
    {
        if (($model = CpfSessions::findOne(['year' => $year])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    
    public function actionRefreshInferred($year, $section = NULL)
    {
        if (!Yii::$app->user->can('ca_generate_annual_report'))
        {
            return Yii::$app->error->ThrowError(Errors::RESTRICTED_AREA_ERROR);
        }
        
        $model = $this->findModel($year);
        $sections = range(1,4);
        
        if (isset($section))
        {
            if (!in_array($section, $sections))
            {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
            

        }
        
        $model->cpfSection4->RefreshInferred();
        
        //(vamanbo 20160429)
        /* set refreshed message
         * 
         */
        $msg = \Yii::t('app/messages', 'Data in Section 4 has been successfuly refreshed from the incidents list');
        
        $actionMessage = new ActionMessage();
        $actionMessage->SetSuccess(
                Yii::t('app', '{evt} updated', ['evt'=>\Yii::t('app/messages', 'Data')]), $msg); 

        Yii::$app->session->set('finished_action_result', $actionMessage);
        
        /* change the refresh_installations status of the CPF report */
        $model->refresh_data = 0;
        $model->update();
        
        
        
        //return $this->render('view', ['model'=>$model, 'readonly'=>FALSE]);
        return $this->redirect(['view', 'model'=>$model, 'year'=>$year, 'readonly'=>FALSE]);
    }
    
    
    public function actionRefreshInstallations($year)
    {
        if (!Yii::$app->user->can('ca_generate_annual_report'))
        {
            return Yii::$app->error->ThrowError(Errors::RESTRICTED_AREA_ERROR);
        }

        $model= $this->findModel($year);
        $section2 = $model->cpfSection2;
        
        $retarray = $section2->refreshInferred();
        if ($retarray !== null)
        {
            $msg = \Yii::t('app/messages', 'Installations list has been successfuly refreshed from the incidents list')
                    . '<br/>' . \Yii::t('app/messages', 'The following have occured:')
                    . '<p>'
                    . \Yii::t('app/messages', 'New installations') . ': ' . count($retarray['new']) . '<br/>'
                    . \Yii::t('app/messages', 'Skipped') . ': ' . count($retarray['skipped']) . '<br/>'
                    . \Yii::t('app/messages', 'Removed') . ': ' . count($retarray['removed']) . '<br/>'
                    . '</p>';
            
            $actionMessage = new ActionMessage();
            $actionMessage->SetSuccess(
                    Yii::t('app', '{evt} updated', ['evt'=>\Yii::t('app/messages', 'Installations list')]), $msg); 
                    
            Yii::$app->session->set('finished_action_result', $actionMessage);
            
            //(vamanbo 20160429)
            /* change the refresh_installations status of the CPF report */
            $model->refresh_installations = 0;
            $model->update();
            
        }
        else
        {
            $actionMessage = new ActionMessage();
            $actionMessage->SetError(
                    Yii::t('app/messages/errors', 'Unable to execute request!', []), 
                    Yii::t('app/messages/errors', 'Check the information below and try again.'));
            Yii::$app->session->set('finished_action_result', $actionMessage);
        }
        
        //check if user can create the report and pass the parameter to the view
        $readonly = !(Yii::$app->user->can('ca_generate_annual_report'));
        
        return $this->render('view', ['model'=>$model, 'readonly'=>$readonly]);
    }
    
    
    public function actionGenerate_xml($year, $user_id)
    {
        /* @var $cpfSession CpfSessions */
        $cpfSession = \app\modules\cpfbuilder\models\CpfSessions::findOne(['year' => $year]);
        
        $cpfSession->scenario = 'xml';
        $cpfSession->cpfSection3->scenario = 'xml';
        
        foreach (range(2,3) as $index) {
            $sectionAttr = 'cpfSection'.$index;
            $cpfSession->$sectionAttr->scenario = 'xml';
            if (!$cpfSession->$sectionAttr->validate()) {
                foreach($cpfSession->$sectionAttr->errors as $error) {
                    $cpfSession->addError(\Yii::t('app/crf', 'Section {evt}', ['evt'=>$index]), $error);
                }
            }
        }
        
        //do the validation here
        //A. there must be at least one operation area for a MODU
        foreach ($cpfSession->cpfSection2->cpfInstallations as $installation)
        {
            if ($installation->type == 'MODU' || $installation->type == 'MOA')
            {
                if (count($installation->cpfInstOpAreas) == 0)
                {
                    $cpfSession->addError('installations', \Yii::t('app/messages/errors', 'At least one operational area must be provided for each mobile installation')
                            . '(' . $installation->ident . ')');
                }
            }
            elseif (!isset($installation->lat) || !(isset($installation->lon)))
            {
                $cpfSession->addError('installations', \Yii::t('app/messages/errors', 'Please provide the coordinates for each fixed installation')
                        . '(' . $installation->ident . ')');
            }
            
        }
        
        //data norm
        //'norm_oil_u', 'norm_gas_u'
        if (!isset($cpfSession->cpfSection2->norm_oil_u)) {
            $cpfSession->cpfSection2->norm_oil_u = 26;   //ktoe
        }
        if (!isset($cpfSession->cpfSection2->norm_gas_u)) {
            $cpfSession->cpfSection2->norm_gas_u = 26;   //ktoe
        }
        
        if ($cpfSession->hasErrors())
        {
            //check if user can create the report and pass the parameter to the view
            $readonly = !(Yii::$app->user->can('ca_generate_annual_report'));

            return $this->render('view', ['model'=>$cpfSession, 'readonly'=>$readonly]);
        }
        
        $xml = \app\modules\cpfbuilder\models\CpfXmlGenerator::ConvertToXml($cpfSession);
        
        $filename = 'syrio_ospis_'. date('Ymd-his', time()) . '.xml';
        //echo '<xmp>' . $xml->saveXML() . '</xmp>';
        header('Content-type: text/xml');
        header('Content-Disposition: attachment; filename="' . $filename . '"');

        echo $xml;
    }
}

/*20160928*/