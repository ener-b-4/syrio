<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\modules\cpfbuilder\controllers;

use Yii;
use app\modules\cpfbuilder\models\CpfInstallations;
use app\modules\cpfbuilder\models\CpfInstallationsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CpfInstallationsController implements the CRUD actions for CpfInstallations model.
 */
class CpfInstallationsController extends Controller
{
    
    /**
     * Add this to ensure that only the CAusers (actually CA rapporteurs) have access to this
     * @param type $action
     * @return boolean
     * 
     * @version 1.0.20151009
     * @author vamanbo <john.doe@example.com>
     */
    public function beforeAction($action) {
        if (!Yii::$app->user->can('ca_generate_annual_report'))
        {
            return Yii::$app->error->ThrowError(Errors::RESTRICTED_AREA_ERROR);
            return FALSE;
        }
        return parent::beforeAction($action);        
    }
            
    
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all CpfInstallations models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CpfInstallationsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CpfInstallations model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CpfInstallations model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CpfInstallations();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing CpfInstallations model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $url =\Yii::$app->urlManager->createUrl([ '/cpfbuilder/cpf-sessions/view',                 
                'year' => $model->year, 'user_id'=>$model->user_id]);
            return $this->redirect($url);

            
            
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing CpfInstallations model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        $url =\Yii::$app->urlManager->createUrl([ '/cpfbuilder/cpf-sessions/view',                 
            'year' => $model->year, 'user_id'=>$model->user_id]);
        return $this->redirect($url);

    }

    /**
     * Finds the CpfInstallations model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CpfInstallations the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CpfInstallations::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
