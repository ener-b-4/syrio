<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\modules\cpfbuilder\controllers;

use Yii;
use app\modules\cpfbuilder\models\CpfCrfAssessments;
use app\modules\cpfbuilder\models\CpfCrfAssessmentsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \app\components\Errors;
use app\models\common\ActionMessage;

/**
 * CpfCrfAssessmentsController implements the CRUD actions for CpfCrfAssessments model.
 */
class CpfCrfAssessmentsController extends Controller
{
    
    /**
     * Add this to ensure that only the CAusers (actually CA rapporteurs) have access to this
     * @param type $action
     * @return boolean
     * 
     * @version 1.0.20160218
     * @author vamanbo <john.doe@example.com>
     */
    public function beforeAction($action) {
        if (!(Yii::$app->user->can('ca_user') || Yii::$app->user->can('sys_admin')))
        {
            return Yii::$app->error->ThrowError(Errors::RESTRICTED_AREA_ERROR);
            return FALSE;
        }
        return parent::beforeAction($action);        
    }
        
    
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'finalize' => ['post'],
                    'reopen' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all CpfCrfAssessments models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CpfCrfAssessmentsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CpfCrfAssessments model.
     * @param integer $year
     * @param integer $user_id
     * @param integer $report_id
     * @return mixed
     */
    public function actionView($year, $report_id)
    {
        //$model = $this->findModel($year, $user_id, $report_id);
        $model = $this->findModel($year, $report_id);        
        $model->InferData();
        
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new CpfCrfAssessments model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (!Yii::$app->user->can('ca_generate_annual_report'))
        {
            return Yii::$app->error->ThrowError(Errors::RESTRICTED_AREA_ERROR);
            return FALSE;
        }
        
        $model = new CpfCrfAssessments();
        $model->InferData();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'year' => $model->year, 'user_id' => $model->user_id, 'report_id' => $model->report_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing CpfCrfAssessments model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $year
     * @param integer $user_id
     * @param integer $report_id
     * @return mixed
     */
    public function actionUpdate($year, $report_id)
    {
        
        if (!Yii::$app->user->can('ca_generate_annual_report'))
        {
            return Yii::$app->error->ThrowError(Errors::RESTRICTED_AREA_ERROR);
            return FALSE;
        }
        
        //$model = $this->findModel($year, $user_id, $report_id);
        $model = $this->findModel($year, $report_id);
        
        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                if (count($model->dirtyAttributes) > 0)
                {
                    $model->modified_at = time();
                    $model->modified_by = \Yii::$app->user->id;
                    $model->save();
                }
            }
            else {
                die('not valid');
            }
            
            return $this->redirect(['view', 'year' => $model->year, 'report_id' => $model->report_id]);
        } else {
            $model->InferData();
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    
    public function actionCancel($year, $report_id) {
        //$model = $this->findModel($year, $user_id, $report_id);
        $model = $this->findModel($year, $report_id);
        return $this->redirect(['view', 'year' => $model->year, 'user_id' => $model->user_id, 'report_id' => $model->report_id]);
    }
    
    /**
     * Deletes an existing CpfCrfAssessments model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $year
     * @param integer $user_id
     * @param integer $report_id
     * @return mixed
     */
    public function actionDelete($year, $report_id)
    {
        if (!Yii::$app->user->can('ca_generate_annual_report'))
        {
            return Yii::$app->error->ThrowError(Errors::RESTRICTED_AREA_ERROR);
            return FALSE;
        }
        
        //$this->findModel($year, $user_id, $report_id)->delete();
        $this->findModel($year, $report_id)->delete();
        
        return $this->redirect(['index']);
    }

    /**
     * Finds the CpfCrfAssessments model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $year
     * @param integer $user_id
     * @param integer $report_id
     * @return CpfCrfAssessments the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    //protected function findModel($year, $user_id, $report_id)
    protected function findModel($year, $report_id)
    {
        //if (($model = CpfCrfAssessments::findOne(['year' => $year, 'user_id' => $user_id, 'report_id' => $report_id])) !== null) {
        if (($model = CpfCrfAssessments::findOne(['year' => $year, 'report_id' => $report_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
     * Finds in the reports (ca_incident_cat) the ones that
     * are assessed (event_categorization.ca_status == assessed) and 
     * are NOT in the current user's list of cpf assessed.
     * 
     * Populates the database with these reports.
     * 
     * @param bool $own if true (default) only the reports signed by himself are taken into account
     */
    public function actionRefreshList($own = FALSE)
    {
        if (!Yii::$app->user->can('ca_generate_annual_report'))
        {
            return Yii::$app->error->ThrowError(Errors::RESTRICTED_AREA_ERROR);
            return FALSE;
        }
        
        
        $query = \app\models\events\drafts\EventDraft::find()
                -> where(['{{event_classifications}}.[[ca_status]]' => \app\models\events\EventDeclaration::INCIDENT_REPORT_ACCEPTED])
                ;
        
        $query->join('left join', '{{cpf_crf_assessments}}', '{{cpf_crf_assessments}}.[[report_id]] = {{event_classifications}}.[[id]]');
        //$query->join('join', '{{ca_incident_cat}}', '{{ca_incident_cat}}.[[draft_id]] = {{event_classifications}}.[[id]]');
        
        $query->andWhere(['{{cpf_crf_assessments}}.[[report_id]]' => NULL]);
        
        $transaction = Yii::$app->db->beginTransaction();
        
        $ok = 1;
        $counter = 0;
        foreach($query->all() as $report)
        {
            /* @var \app\models\events\drafts\EventDraft $report */
            /* @var CpfCrfAssessments() $new_record */

            //echo '<pre>';
            //echo var_dump($report);
            //echo '</pre>';
            
            $date = date_create($report->event->event_date_time);
            $new_record = new CpfCrfAssessments();
            $new_record->year = intval(date_format($date, 'Y'));
            $new_record->user_id = \Yii::$app->user->id;
            $new_record->report_id = $report->id;
            $new_record->created_at = time();
            $new_record->created_by = \Yii::$app->user->id;
            $new_record->status = \app\models\events\EventDeclaration::STATUS_ACTIVE;
            $new_record->assessment_status = CpfCrfAssessments::ASSESS_STATUS_NEW;

            //check if there's a session for user and year 
            $session = \app\modules\cpfbuilder\models\CpfSessions::findOne(['year'=>$new_record->year]);
            if (is_null($session)) {
                //create session
                $cpf_session = new \app\modules\cpfbuilder\models\CpfSessions();
                $cpf_session->year = $new_record->year;
                $cpf_session->user_id = $new_record->user_id;
                
                //aici
                $this->createCPF($cpf_session);
                
                if (!$cpf_session->save()) {
                    die('unable to create new cpf_session');
                }
            }
//            {
                if (!$new_record->save())
                {
                    $new_record->validate();
                    //echo '<pre>';
                    //echo var_dump($new_record->errors);
                    //echo '</pre>';                
                    $transaction->rollBack();
                    $ok = 0;
                }
                $counter+=1;
//            }
        }

        //die('display reports');
        
        if ($ok === 1)
        {
            $transaction->commit();
            $actionMessage = new ActionMessage();
            $actionMessage->SetSuccess(
                    Yii::t('app', 'Success', []), 
                    Yii::t('app', 'You have {evt} new records found.', ['evt' => $counter]));
            Yii::$app->session->set('finished_action_result', $actionMessage);
        }
        else
        {
            $actionMessage = new ActionMessage();
            $actionMessage->SetError(
                    Yii::t('app', 'Unable to {evt}!', ['evt' => 'refresh data']), 
                    Yii::t('app', 'Please try again later.'));
            Yii::$app->session->set('finished_action_result', $actionMessage);
        }
        
        return $this->redirect(['index']);
    }
    
    public function actionFinalize($year, $report_id)
    {
        if (!Yii::$app->user->can('ca_generate_annual_report'))
        {
            return Yii::$app->error->ThrowError(Errors::RESTRICTED_AREA_ERROR);
            return FALSE;
        }
        
        //don't need the authorization
        //done in beforeaction
        
        //! make it as post in the data-method request!
        
        
        //$model=$this->findModel($year, $user_id, $report_id);
        $model=$this->findModel($year, $report_id);
        $model->assessment_status = CpfCrfAssessments::ASSESS_STATUS_FINALIZED;
        $model->modified_at = time();
        $model->modified_by = \Yii::$app->user->id;
        
        if ($model->validate() && $model->save())
        {
            $actionMessage = new ActionMessage();
            $actionMessage->SetSuccess(
                    Yii::t('app', 'Success', []), 
                    Yii::t('app', 'You have marked \'{evt}\' as finalized. The report will be accounted in the Annual Report Builder', ['evt'=>$model->report->name]));
            Yii::$app->session->set('finished_action_result', $actionMessage);
            
            
            $description = [
                'by' => \Yii::$app->user->identity->full_name,
                'at' => date('Y-m-d H:i:s', \time()),
            ];

            //log the event history
            if (!\app\models\ReportingHistory::addHistoryItem(
                    $model->report->event->id, $model->report->draft_id, 
                    \app\models\ReportingHistory::EVENT_ASSESSED_FOR_CPF, 
                    json_encode($description)))
            {
                    $ok=0;
            }
            
            
        }
        else
        {
            $actionMessage = new ActionMessage();
            $actionMessage->SetError(
                    Yii::t('app', 'Unable to {evt}!', ['evt' => 'mark this event as final']), 
                    Yii::t('app', 'Please try again later.'));
            Yii::$app->session->set('finished_action_result', $actionMessage);
        }
        
        return $this->redirect(['index']);
    }
    
    public function actionReopen($year, $report_id)
    {
        if (!Yii::$app->user->can('ca_generate_annual_report'))
        {
            return Yii::$app->error->ThrowError(Errors::RESTRICTED_AREA_ERROR);
            return FALSE;
        }
        
        //! make it as post in the data-method request!
        
        
        $model=$this->findModel($year, $report_id);
        $model->assessment_status = CpfCrfAssessments::ASSESS_STATUS_IN_PROGRESS;
        $model->modified_at = time();
        $model->modified_by = \Yii::$app->user->id;
        
        if ($model->validate() && $model->save())
        {
            $actionMessage = new ActionMessage();
            $actionMessage->SetSuccess(
                    Yii::t('app', 'Success', []), 
                    Yii::t('app', 'You have reopened \'{evt}\'. The event WILL NOT be accounted in the Annual Report Builder.', ['evt'=>$model->report->name]));
            Yii::$app->session->set('finished_action_result', $actionMessage);
            
            
            $description = [
                'by' => \Yii::$app->user->identity->full_name,
                'at' => date('Y-m-d H:i:s', \time()),
            ];

            //log the event history
            if (!\app\models\ReportingHistory::addHistoryItem(
                    $model->report->event->id, $model->report->draft_id, 
                    \app\models\ReportingHistory::EVENT_CPF_REOPENED, 
                    json_encode($description)))
            {
                    $ok=0;
            }
            
            
        }
        else
        {
            $actionMessage = new ActionMessage();
            $actionMessage->SetError(
                    Yii::t('app', 'Unable to {evt}!', ['evt' => 'reopen this event as final']), 
                    Yii::t('app', 'Please try again later.'));
            Yii::$app->session->set('finished_action_result', $actionMessage);
        }
        
        return $this->redirect(['index']);
    }
    

    
    private function createCPF($model) {
        
        //die ('cpfmodel');
            if ($model->validate())
            {
                $transaction = \Yii::$app->db->beginTransaction();
                $ok = 1;
                
                /*20150923*/
                $section2 = \app\modules\cpfbuilder\models\CpfSection2::findOne(
                        ['year'=>$model->year]);
                if (isset($section2))
                {
                    $section2->delete();
                }

                //save the model
                if ($model->save())
                {
                    //create all the sections
                    $section1 = new \app\modules\cpfbuilder\models\CpfSection1();
                    $section1->year = $model->year;
                    $section1->user_id = $model->user_id;
                    $section1->ms = \Yii::$app->params['competentAuthorityIso2'];
                    $section1->ca = \Yii::$app->params['competentAuthority'];
                    $section1->dra = \Yii::$app->user->identity->organization->organization_name;
                    $section1->phone = !\Yii::$app->user->identity->phone ? '-' : \Yii::$app->user->identity->phone;
                    $section1->email = \Yii::$app->user->identity->email;
                    
                    $section2 = new \app\modules\cpfbuilder\models\CpfSection2();
                    $section2->year = $model->year;
                    $section2->user_id = $model->user_id;
                    

                    $section3 = new \app\modules\cpfbuilder\models\CpfSection3();
                    $section3->year = $model->year;
                    $section3->user_id = $model->user_id;

                    $section4 = new \app\modules\cpfbuilder\models\CpfSection4();
                    $section4->year = $model->year;
                    $section4->user_id = $model->user_id;

                    
                    if ($section1->save() && $section2->save() && $section3->save() && $section4->save())
                    {
                        $transaction->commit();
                    }
                    else
                    {
                        $transaction->rollBack();
                        $ok=0;
                    }
                }
                else
                {
                    $transaction->rollBack();
                    $ok=0;
                }

                
                if (!$ok) {
                    echo 'unable to save.';
                    die();
                }
            }
        
    }
    
}


