<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\modules\cpfbuilder\models;

use Yii;
use app\models\ca\IncidentCategorization;
/**
 * This is the model class for table "cpf_crf_assessments".
 *
 * @property integer $year
 * @property integer $user_id
 * @property integer $report_id
 * @property integer $s_s4_2_a_total
 * @property integer $s_s4_2_a_if
 * @property integer $s_s4_2_a_ix
 * @property integer $s_s4_2_a_nig
 * @property integer $s_s4_2_a_nio
 * @property integer $s_s4_2_a_haz
 * @property integer $s_s4_2_b_total
 * @property integer $s_s4_2_b_bo
 * @property integer $s_s4_2_b_bda
 * @property integer $s_s4_2_b_wbf
 * @property integer $s_s4_2_c_total
 * @property integer $s_s4_2_d_total
 * @property integer $s_s4_2_d_si
 * @property integer $s_s4_2_d_sb
 * @property integer $s_s4_2_d_sk
 * @property integer $s_s4_2_e_total
 * @property integer $s_s4_2_f_total
 * @property integer $s_s4_2_g_total
 * @property integer $s_s4_2_h_total
 * @property integer $s_s4_2_i_total
 * @property integer $s_s4_2_j_total
 * @property integer $s_s4_3_total
 * @property integer $s_s4_3_fatalities
 * @property integer $s_s4_3_inj
 * @property integer $s_s4_3_sinj
 * @property integer $s_s4_4_nsis
 * @property integer $s_s4_4_npcs
 * @property integer $s_s4_4_nics
 * @property integer $s_s4_4_nds
 * @property integer $s_s4_4_npcrs
 * @property integer $s_s4_4_nps
 * @property integer $s_s4_4_nsds
 * @property integer $s_s4_4_nnavaids
 * @property integer $s_s4_4_roteq
 * @property integer $s_s4_4_eere
 * @property integer $s_s4_4_ncoms
 * @property integer $s_s4_4_nother
 * @property integer $s_s4_5_a_total
 * @property integer $s_s4_5_a_df
 * @property integer $s_s4_5_a_co__int
 * @property integer $s_s4_5_a_co__ext
 * @property integer $s_s4_5_a_mf__f
 * @property integer $s_s4_5_a_mf__wo
 * @property integer $s_s4_5_a_mf__dm
 * @property integer $s_s4_5_a_mf__vh
 * @property integer $s_s4_5_a_if
 * @property integer $s_s4_5_a_csf
 * @property integer $s_s4_5_a_other
 * @property integer $s_s4_5_b_total
 * @property integer $s_s4_5_b_err__op
 * @property integer $s_s4_5_b_err__mnt
 * @property integer $s_s4_5_b_err__tst
 * @property integer $s_s4_5_b_err__insp
 * @property integer $s_s4_5_b_err__dsgn
 * @property integer $s_s4_5_b_other
 * @property integer $s_s4_5_c_total
 * @property integer $s_s4_5_c_inq__rap
 * @property integer $s_s4_5_c_inq__instp
 * @property integer $s_s4_5_c_nc__prc
 * @property integer $s_s4_5_c_nc__ptw
 * @property integer $s_s4_5_c_inq__com
 * @property integer $s_s4_5_c_inq__pc
 * @property integer $s_s4_5_c_inq__sup
 * @property integer $s_s4_5_c_inq__safelead
 * @property integer $s_s4_5_c_other
 * @property string $s_s4_6
 * @property integer $u_s4_2_a_total
 * @property integer $u_s4_2_a_if
 * @property integer $u_s4_2_a_ix
 * @property integer $u_s4_2_a_nig
 * @property integer $u_s4_2_a_nio
 * @property integer $u_s4_2_a_haz
 * @property integer $u_s4_2_b_total
 * @property integer $u_s4_2_b_bo
 * @property integer $u_s4_2_b_bda
 * @property integer $u_s4_2_b_wbf
 * @property integer $u_s4_2_c_total
 * @property integer $u_s4_2_d_total
 * @property integer $u_s4_2_d_si
 * @property integer $u_s4_2_d_sb
 * @property integer $u_s4_2_d_sk
 * @property integer $u_s4_2_e_total
 * @property integer $u_s4_2_f_total
 * @property integer $u_s4_2_g_total
 * @property integer $u_s4_2_h_total
 * @property integer $u_s4_2_i_total
 * @property integer $u_s4_2_j_total
 * @property integer $u_s4_3_total
 * @property integer $u_s4_3_fatalities
 * @property integer $u_s4_3_inj
 * @property integer $u_s4_3_sinj
 * @property integer $u_s4_4_nsis
 * @property integer $u_s4_4_npcs
 * @property integer $u_s4_4_nics
 * @property integer $u_s4_4_nds
 * @property integer $u_s4_4_npcrs
 * @property integer $u_s4_4_nps
 * @property integer $u_s4_4_nsds
 * @property integer $u_s4_4_nnavaids
 * @property integer $u_s4_4_roteq
 * @property integer $u_s4_4_eere
 * @property integer $u_s4_4_ncoms
 * @property integer $u_s4_4_nother
 * @property integer $u_s4_5_a_total
 * @property integer $u_s4_5_a_df
 * @property integer $u_s4_5_a_co__int
 * @property integer $u_s4_5_a_co__ext
 * @property integer $u_s4_5_a_mf__f
 * @property integer $u_s4_5_a_mf__wo
 * @property integer $u_s4_5_a_mf__dm
 * @property integer $u_s4_5_a_mf__vh
 * @property integer $u_s4_5_a_if
 * @property integer $u_s4_5_a_csf
 * @property integer $u_s4_5_a_other
 * @property integer $u_s4_5_b_total
 * @property integer $u_s4_5_b_err__op
 * @property integer $u_s4_5_b_err__mnt
 * @property integer $u_s4_5_b_err__tst
 * @property integer $u_s4_5_b_err__insp
 * @property integer $u_s4_5_b_err__dsgn
 * @property integer $u_s4_5_b_other
 * @property integer $u_s4_5_c_total
 * @property integer $u_s4_5_c_inq__rap
 * @property integer $u_s4_5_c_inq__instp
 * @property integer $u_s4_5_c_nc__prc
 * @property integer $u_s4_5_c_nc__ptw
 * @property integer $u_s4_5_c_inq__com
 * @property integer $u_s4_5_c_inq__pc
 * @property integer $u_s4_5_c_inq__sup
 * @property integer $u_s4_5_c_inq__safelead
 * @property integer $u_s4_5_c_other
 * @property string $u_s4_6
 * @property integer $status
 * @property integer $assessment_status
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $modified_at
 * @property integer $modified_by
 * 
 * @property integer $s_s4_5_d_total
 * @property integer $s_s4_5_d_exc__design__wind
 * @property integer $s_s4_5_d_exc__design__wave
 * @property integer $s_s4_5_d_exc__design__lowvis
 * @property integer $s_s4_5_d_ice__icebergs
 * @property integer $s_s4_5_d_other
 * @property integer $u_s4_5_d_total
 * @property integer $u_s4_5_d_exc__design__wind
 * @property integer $u_s4_5_d_exc__design__wave
 * @property integer $u_s4_5_d_exc__design__lowvis
 * @property integer $u_s4_5_d_ice__icebergs
 * @property integer $u_s4_5_d_other
 * @property CaIncidentCat $report
 * @property CpfSessions $year0
 */
class CpfCrfAssessments extends \yii\db\ActiveRecord
{
    const ASSESS_STATUS_NEW = 0;
    const ASSESS_STATUS_SUSPENDED = 5;
    const ASSESS_STATUS_IN_PROGRESS = 10;
    const ASSESS_STATUS_FINALIZED = 20;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cpf_crf_assessments';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['year', 'user_id', 'report_id', 'status', 'assessment_status', 'created_at', 'created_by'], 'required'],
            [['year', 'user_id', 'report_id', 's_s4_2_a_total', 's_s4_2_a_if', 's_s4_2_a_ix', 's_s4_2_a_nig', 's_s4_2_a_nio', 's_s4_2_a_haz', 's_s4_2_b_total', 's_s4_2_b_bo', 's_s4_2_b_bda', 's_s4_2_b_wbf', 's_s4_2_c_total', 's_s4_2_d_total', 's_s4_2_d_si', 's_s4_2_d_sb', 's_s4_2_d_sk', 's_s4_2_e_total', 's_s4_2_f_total', 's_s4_2_g_total', 's_s4_2_h_total', 's_s4_2_i_total', 's_s4_2_j_total','s_s4_4_nsis', 's_s4_4_npcs', 's_s4_4_nics', 's_s4_4_nds', 's_s4_4_npcrs', 's_s4_4_nps', 's_s4_4_nsds', 's_s4_4_nnavaids', 's_s4_4_roteq', 's_s4_4_eere', 's_s4_4_ncoms', 's_s4_4_nother', 's_s4_5_a_total', 's_s4_5_a_df', 's_s4_5_a_co__int', 's_s4_5_a_co__ext', 's_s4_5_a_mf__f', 's_s4_5_a_mf__wo', 's_s4_5_a_mf__dm', 's_s4_5_a_mf__vh', 's_s4_5_a_if', 's_s4_5_a_csf', 's_s4_5_a_other', 's_s4_5_b_total', 's_s4_5_b_err__op', 's_s4_5_b_err__mnt', 's_s4_5_b_err__tst', 's_s4_5_b_err__insp', 's_s4_5_b_err__dsgn', 's_s4_5_b_other', 's_s4_5_c_total', 's_s4_5_c_inq__rap', 's_s4_5_c_inq__instp', 's_s4_5_c_nc__prc', 's_s4_5_c_nc__ptw', 's_s4_5_c_inq__com', 's_s4_5_c_inq__pc', 's_s4_5_c_inq__sup', 's_s4_5_c_inq__safelead', 's_s4_5_c_other', 'u_s4_2_a_total', 'u_s4_2_a_if', 'u_s4_2_a_ix', 'u_s4_2_a_nig', 'u_s4_2_a_nio', 'u_s4_2_a_haz', 'u_s4_2_b_total', 'u_s4_2_b_bo', 'u_s4_2_b_bda', 'u_s4_2_b_wbf', 'u_s4_2_c_total', 'u_s4_2_d_total', 'u_s4_2_d_si', 'u_s4_2_d_sb', 'u_s4_2_d_sk', 'u_s4_2_e_total', 'u_s4_2_f_total', 'u_s4_2_g_total', 'u_s4_2_h_total', 'u_s4_2_i_total', 'u_s4_2_j_total', 'u_s4_4_nsis', 'u_s4_4_npcs', 'u_s4_4_nics', 'u_s4_4_nds', 'u_s4_4_npcrs', 'u_s4_4_nps', 'u_s4_4_nsds', 'u_s4_4_nnavaids', 'u_s4_4_roteq', 'u_s4_4_eere', 'u_s4_4_ncoms', 'u_s4_4_nother', 'u_s4_5_a_total', 'u_s4_5_a_df', 'u_s4_5_a_co__int', 'u_s4_5_a_co__ext', 'u_s4_5_a_mf__f', 'u_s4_5_a_mf__wo', 'u_s4_5_a_mf__dm', 'u_s4_5_a_mf__vh', 'u_s4_5_a_if', 'u_s4_5_a_csf', 'u_s4_5_a_other', 'u_s4_5_b_total', 'u_s4_5_b_err__op', 'u_s4_5_b_err__mnt', 'u_s4_5_b_err__tst', 'u_s4_5_b_err__insp', 'u_s4_5_b_err__dsgn', 'u_s4_5_b_other', 'u_s4_5_c_total', 'u_s4_5_c_inq__rap', 'u_s4_5_c_inq__instp', 'u_s4_5_c_nc__prc', 'u_s4_5_c_nc__ptw', 'u_s4_5_c_inq__com', 'u_s4_5_c_inq__pc', 'u_s4_5_c_inq__sup', 'u_s4_5_c_inq__safelead', 'u_s4_5_c_other', 'status', 'assessment_status', 'created_at', 'created_by', 'modified_at', 'modified_by', 's_s4_5_d_total', 's_s4_5_d_exc__design__wind', 's_s4_5_d_exc__design__wave', 's_s4_5_d_exc__design__lowvis', 's_s4_5_d_ice__icebergs', 's_s4_5_d_other', 'u_s4_5_d_total', 'u_s4_5_d_exc__design__wind', 'u_s4_5_d_exc__design__wave', 'u_s4_5_d_exc__design__lowvis', 'u_s4_5_d_ice__icebergs', 'u_s4_5_d_other'], 'integer'],
            [['year', 'user_id', 'report_id', 's_s4_2_a_total', 's_s4_2_a_if', 's_s4_2_a_ix', 's_s4_2_a_nig', 's_s4_2_a_nio', 's_s4_2_a_haz', 's_s4_2_b_total', 's_s4_2_b_bo', 's_s4_2_b_bda', 's_s4_2_b_wbf', 's_s4_2_c_total', 's_s4_2_d_total', 's_s4_2_d_si', 's_s4_2_d_sb', 's_s4_2_d_sk', 's_s4_2_e_total', 's_s4_2_f_total', 's_s4_2_g_total', 's_s4_2_h_total', 's_s4_2_i_total', 's_s4_2_j_total', 's_s4_3_total', 's_s4_3_fatalities', 's_s4_3_inj', 's_s4_3_sinj', 's_s4_4_nsis', 's_s4_4_npcs', 's_s4_4_nics', 's_s4_4_nds', 's_s4_4_npcrs', 's_s4_4_nps', 's_s4_4_nsds', 's_s4_4_nnavaids', 's_s4_4_roteq', 's_s4_4_eere', 's_s4_4_ncoms', 's_s4_4_nother', 's_s4_5_a_total', 's_s4_5_a_df', 's_s4_5_a_co__int', 's_s4_5_a_co__ext', 's_s4_5_a_mf__f', 's_s4_5_a_mf__wo', 's_s4_5_a_mf__dm', 's_s4_5_a_mf__vh', 's_s4_5_a_if', 's_s4_5_a_csf', 's_s4_5_a_other', 's_s4_5_b_total', 's_s4_5_b_err__op', 's_s4_5_b_err__mnt', 's_s4_5_b_err__tst', 's_s4_5_b_err__insp', 's_s4_5_b_err__dsgn', 's_s4_5_b_other', 's_s4_5_c_total', 's_s4_5_c_inq__rap', 's_s4_5_c_inq__instp', 's_s4_5_c_nc__prc', 's_s4_5_c_nc__ptw', 's_s4_5_c_inq__com', 's_s4_5_c_inq__pc', 's_s4_5_c_inq__sup', 's_s4_5_c_inq__safelead', 's_s4_5_c_other', 'u_s4_2_a_total', 'u_s4_2_a_if', 'u_s4_2_a_ix', 'u_s4_2_a_nig', 'u_s4_2_a_nio', 'u_s4_2_a_haz', 'u_s4_2_b_total', 'u_s4_2_b_bo', 'u_s4_2_b_bda', 'u_s4_2_b_wbf', 'u_s4_2_c_total', 'u_s4_2_d_total', 'u_s4_2_d_si', 'u_s4_2_d_sb', 'u_s4_2_d_sk', 'u_s4_2_e_total', 'u_s4_2_f_total', 'u_s4_2_g_total', 'u_s4_2_h_total', 'u_s4_2_i_total', 'u_s4_2_j_total', 'u_s4_3_total', 'u_s4_3_fatalities', 'u_s4_3_inj', 'u_s4_3_sinj', 'u_s4_4_nsis', 'u_s4_4_npcs', 'u_s4_4_nics', 'u_s4_4_nds', 'u_s4_4_npcrs', 'u_s4_4_nps', 'u_s4_4_nsds', 'u_s4_4_nnavaids', 'u_s4_4_roteq', 'u_s4_4_eere', 'u_s4_4_ncoms', 'u_s4_4_nother', 'u_s4_5_a_total', 'u_s4_5_a_df', 'u_s4_5_a_co__int', 'u_s4_5_a_co__ext', 'u_s4_5_a_mf__f', 'u_s4_5_a_mf__wo', 'u_s4_5_a_mf__dm', 'u_s4_5_a_mf__vh', 'u_s4_5_a_if', 'u_s4_5_a_csf', 'u_s4_5_a_other', 'u_s4_5_b_total', 'u_s4_5_b_err__op', 'u_s4_5_b_err__mnt', 'u_s4_5_b_err__tst', 'u_s4_5_b_err__insp', 'u_s4_5_b_err__dsgn', 'u_s4_5_b_other', 'u_s4_5_c_total', 'u_s4_5_c_inq__rap', 'u_s4_5_c_inq__instp', 'u_s4_5_c_nc__prc', 'u_s4_5_c_nc__ptw', 'u_s4_5_c_inq__com', 'u_s4_5_c_inq__pc', 'u_s4_5_c_inq__sup', 'u_s4_5_c_inq__safelead', 'u_s4_5_c_other', 'status', 'assessment_status', 'created_at', 'created_by', 'modified_at', 'modified_by', 's_s4_5_d_total', 's_s4_5_d_exc__design__wind', 's_s4_5_d_exc__design__wave', 's_s4_5_d_exc__design__lowvis', 's_s4_5_d_ice__icebergs', 's_s4_5_d_other', 'u_s4_5_d_total', 'u_s4_5_d_exc__design__wind', 'u_s4_5_d_exc__design__wave', 'u_s4_5_d_exc__design__lowvis', 'u_s4_5_d_ice__icebergs', 'u_s4_5_d_other'], 'safe'],
            [[ 's_s4_3_total', 's_s4_3_fatalities', 's_s4_3_inj', 's_s4_3_sinj', 'u_s4_3_total', 'u_s4_3_fatalities', 'u_s4_3_inj', 'u_s4_3_sinj'], 'integer'],
            [['s_s4_6', 'u_s4_6'], 'string'],
            [['u_s4_3_total', 'u_s4_3_fatalities', 'u_s4_3_inj', 'u_s4_3_sinj'], 'integer', 'min'=>0],
            
            [['assessment_status'], 'in', 'range' => [0, 5, 10, 20]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'year' => Yii::t('app/cpf', 'Year'),
            'user_id' => 'User ID',
            'report_id' => 'Report ID',
            's_s4_2_a_total' => Yii::t('app/cpf', 'Unintended releases - Total - {evt}', ['evt'=>'SyRIO']),
            's_s4_2_a_if' => Yii::t('app/cpf', 'Ignited oil/gas releases - Fires - {evt}', ['evt'=>'SyRIO']),
            's_s4_2_a_ix' => Yii::t('app/cpf', 'Ignited oil/gas releases - Explosions - {evt}', ['evt'=>'SyRIO']),
            's_s4_2_a_nig' => Yii::t('app/cpf', 'Not ignited gas releases - {evt}', ['evt'=>'SyRIO']),
            's_s4_2_a_nio' => Yii::t('app/cpf', 'Not ignited oil releases - {evt}', ['evt'=>'SyRIO']),
            's_s4_2_a_haz' => Yii::t('app/cpf', 'Hazardous substances releases - {evt}', ['evt'=>'SyRIO']),
            's_s4_2_b_total' => Yii::t('app/cpf', 'Loss of well - Total - {evt}', ['evt'=>'SyRIO']),
            's_s4_2_b_bo' => Yii::t('app/cpf', 'Blowouts - {evt}', ['evt'=>'SyRIO']),
            's_s4_2_b_bda' => Yii::t('app/cpf', 'Blowout / diverter activation - {evt}', ['evt'=>'SyRIO']),
            's_s4_2_b_wbf' => Yii::t('app/cpf', 'Well barrier failure - {evt}', ['evt'=>'SyRIO']),
            's_s4_2_c_total' => Yii::t('app/cpf', 'Failures of SECE - {evt}', ['evt'=>'SyRIO']),
            's_s4_2_d_total' => Yii::t('app/cpf', 'Loss of structural integrity - total - {evt}', ['evt'=>'SyRIO']),
            's_s4_2_d_si' => Yii::t('app/cpf', 'Loss of structural integrity - {evt}', ['evt'=>'SyRIO']),
            's_s4_2_d_sb' => Yii::t('app/cpf', 'Loss of stability/buoyancy - {evt}', ['evt'=>'SyRIO']),
            's_s4_2_d_sk' => Yii::t('app/cpf', 'Loss of station keeping - {evt}', ['evt'=>'SyRIO']),
            's_s4_2_e_total' => Yii::t('app/cpf', 'Vessel collisions - total - {evt}', ['evt'=>'SyRIO']),
            's_s4_2_f_total' => Yii::t('app/cpf', 'Helicopter accidents - {evt}', ['evt'=>'SyRIO']),
            's_s4_2_g_total' => Yii::t('app/cpf', 'Fatal accidents - {evt}', ['evt'=>'SyRIO']),
            's_s4_2_h_total' => Yii::t('app/cpf', 'Serious injuries of 5 or more persons in the same accident - {evt}', ['evt'=>'SyRIO']),
            's_s4_2_i_total' => Yii::t('app/cpf', 'Evacuation of personnel - {evt}', ['evt'=>'SyRIO']),
            's_s4_2_j_total' => Yii::t('app/cpf', 'Environmental accidents - {evt}', ['evt'=>'SyRIO']),
            's_s4_3_total' => Yii::t('app/cpf', 'Total number of fatalities and injuries - {evt}', ['evt'=>'SyRIO']),
            's_s4_3_fatalities' => Yii::t('app/cpf', 'Number of fatalities - {evt}', ['evt'=>'SyRIO']),
            's_s4_3_inj' => Yii::t('app/cpf', 'Total number of injuries - {evt}', ['evt'=>'SyRIO']),
            's_s4_3_sinj' => Yii::t('app/cpf', 'Total number of serious injuries - {evt}', ['evt'=>'SyRIO']),
            's_s4_4_nsis' => Yii::t('app/cpf', 'Structural integrity systems - {evt}', ['evt'=>'SyRIO']),
            's_s4_4_npcs' => Yii::t('app/cpf', 'Process containment systems - {evt}', ['evt'=>'SyRIO']),
            's_s4_4_nics' => Yii::t('app/cpf', 'Ignition control systems - {evt}', ['evt'=>'SyRIO']),
            's_s4_4_nds' => Yii::t('app/cpf', 'Detection control systems - {evt}', ['evt'=>'SyRIO']),
            's_s4_4_npcrs' => Yii::t('app/cpf', 'Process containment relief systems - {evt}', ['evt'=>'SyRIO']),
            's_s4_4_nps' => Yii::t('app/cpf', 'Protection systems - {evt}', ['evt'=>'SyRIO']),
            's_s4_4_nsds' => Yii::t('app/cpf', 'Shutdown systems - {evt}', ['evt'=>'SyRIO']),
            's_s4_4_nnavaids' => Yii::t('app/cpf', 'Navigational aids - {evt}', ['evt'=>'SyRIO']),
            's_s4_4_roteq' => Yii::t('app/cpf', 'Rotating equipment - {evt}', ['evt'=>'SyRIO']),
            's_s4_4_eere' => Yii::t('app/cpf', 'Escape, evacuation and rescue equipment - {evt}', ['evt'=>'SyRIO']),
            's_s4_4_ncoms' => Yii::t('app/cpf', 'Communication systems - {evt}', ['evt'=>'SyRIO']),
            's_s4_4_nother' => Yii::t('app/cpf', 'other - {evt}', ['evt'=>'SyRIO']),
            's_s4_5_a_total' => Yii::t('app/cpf', 'Incidents caused by Equipment failure - Total - {evt}', ['evt'=>'SyRIO']),
            's_s4_5_a_df' => Yii::t('app/cpf', 'Design failures - {evt}', ['evt'=>'SyRIO']),
            's_s4_5_a_co__int' => Yii::t('app/cpf', 'Internal corrosion - {evt}', ['evt'=>'SyRIO']),
            's_s4_5_a_co__ext' => Yii::t('app/cpf', 'External corrosion - {evt}', ['evt'=>'SyRIO']),
            's_s4_5_a_mf__f' => Yii::t('app/cpf', 'Mechanical failures due to fatigue - {evt}', ['evt'=>'SyRIO']),
            's_s4_5_a_mf__wo' => Yii::t('app/cpf', 'Mechanical failures due to wear-out - {evt}', ['evt'=>'SyRIO']),
            's_s4_5_a_mf__dm' => Yii::t('app/cpf', 'Mechanical failures due to defected material - {evt}', ['evt'=>'SyRIO']),
            's_s4_5_a_mf__vh' => Yii::t('app/cpf', 'Mechanical failures (vessel/helicopter) - {evt}', ['evt'=>'SyRIO']),
            's_s4_5_a_if' => Yii::t('app/cpf', 'Instrument failures - {evt}', ['evt'=>'SyRIO']),
            's_s4_5_a_csf' => Yii::t('app/cpf', 'Control system failures - {evt}', ['evt'=>'SyRIO']),
            's_s4_5_a_other' => Yii::t('app/cpf', 'other - {evt}', ['evt'=>'SyRIO']),
            's_s4_5_b_total' => Yii::t('app/cpf', 'Human error-operational causes - total - {evt}', ['evt'=>'SyRIO']),
            's_s4_5_b_err__op' => Yii::t('app/cpf', 'Operation error - {evt}', ['evt'=>'SyRIO']),
            's_s4_5_b_err__mnt' => Yii::t('app/cpf', 'Maintenance error - {evt}', ['evt'=>'SyRIO']),
            's_s4_5_b_err__tst' => Yii::t('app/cpf', 'Testing error - {evt}', ['evt'=>'SyRIO']),
            's_s4_5_b_err__insp' => Yii::t('app/cpf', 'Inspection error - {evt}', ['evt'=>'SyRIO']),
            's_s4_5_b_err__dsgn' => Yii::t('app/cpf', 'Design error - {evt}', ['evt'=>'SyRIO']),
            's_s4_5_b_other' => Yii::t('app/cpf', 'other - {evt}', ['evt'=>'SyRIO']),
            's_s4_5_c_total' => Yii::t('app/cpf', 'Procedural / organizational error - {evt}', ['evt'=>'SyRIO']),
            's_s4_5_c_inq__rap' => Yii::t('app/cpf', 'Inadequate risk assessment/perception - {evt}', ['evt'=>'SyRIO']),
            's_s4_5_c_inq__instp' => Yii::t('app/cpf', 'Inadequate instruction/procedure - {evt}', ['evt'=>'SyRIO']),
            's_s4_5_c_nc__prc' => Yii::t('app/cpf', 'Non-compliance with procedure - {evt}', ['evt'=>'SyRIO']),
            's_s4_5_c_nc__ptw' => Yii::t('app/cpf', 'Non-compliance with permit-to-work - {evt}', ['evt'=>'SyRIO']),
            's_s4_5_c_inq__com' => Yii::t('app/cpf', 'Inadequate communication - {evt}', ['evt'=>'SyRIO']),
            's_s4_5_c_inq__pc' => Yii::t('app/cpf', 'Inadequate personnel competence - {evt}', ['evt'=>'SyRIO']),
            's_s4_5_c_inq__sup' => Yii::t('app/cpf', 'Inadequate supervision - {evt}', ['evt'=>'SyRIO']),
            's_s4_5_c_inq__safelead' => Yii::t('app/cpf', 'Inadequate safety leadership - {evt}', ['evt'=>'SyRIO']),
            's_s4_5_c_other' => Yii::t('app/cpf', 'other - {evt}', ['evt'=>'SyRIO']),
            's_s4_6' => Yii::t('app/cpf', 'Most important lessons learned - {evt}', ['evt'=>'SyRIO']),
            'u_s4_2_a_total' => Yii::t('app/cpf', 'Unintended releases - Total - {evt}', ['evt'=>\Yii::t('app', 'user')]),
            'u_s4_2_a_if' => Yii::t('app/cpf', 'Ignited oil/gas releases - Fires - {evt}', ['evt'=>\Yii::t('app', 'user')]),
            'u_s4_2_a_ix' => Yii::t('app/cpf', 'Ignited oil/gas releases - Explosions - {evt}', ['evt'=>\Yii::t('app', 'user')]),
            'u_s4_2_a_nig' => Yii::t('app/cpf', 'Not ignited gas releases - {evt}', ['evt'=>\Yii::t('app', 'user')]),
            'u_s4_2_a_nio' => Yii::t('app/cpf', 'Not ignited oil releases - {evt}', ['evt'=>\Yii::t('app', 'user')]),
            'u_s4_2_a_haz' => Yii::t('app/cpf', 'Hazardous substances releases - {evt}', ['evt'=>\Yii::t('app', 'user')]),
            'u_s4_2_b_total' => Yii::t('app/cpf', 'Loss of well - Total - {evt}', ['evt'=>\Yii::t('app', 'user')]),
            'u_s4_2_b_bo' => Yii::t('app/cpf', 'Blowouts - {evt}', ['evt'=>\Yii::t('app', 'user')]),
            'u_s4_2_b_bda' => Yii::t('app/cpf', 'Blowout / diverter activation - {evt}', ['evt'=>\Yii::t('app', 'user')]),
            'u_s4_2_b_wbf' => Yii::t('app/cpf', 'Well barrier failure - {evt}', ['evt'=>\Yii::t('app', 'user')]),
            'u_s4_2_c_total' => Yii::t('app/cpf', 'Failures of SECE - {evt}', ['evt'=>\Yii::t('app', 'user')]),
            'u_s4_2_d_total' => Yii::t('app/cpf', 'Loss of structural integrity - total - {evt}', ['evt'=>\Yii::t('app', 'user')]),
            'u_s4_2_d_si' => Yii::t('app/cpf', 'Loss of structural integrity - {evt}', ['evt'=>\Yii::t('app', 'user')]),
            'u_s4_2_d_sb' => Yii::t('app/cpf', 'Loss of stability/buoyancy - {evt}', ['evt'=>\Yii::t('app', 'user')]),
            'u_s4_2_d_sk' => Yii::t('app/cpf', 'Loss of station keeping - {evt}', ['evt'=>\Yii::t('app', 'user')]),
            'u_s4_2_e_total' => Yii::t('app/cpf', 'Vessel collisions - total - {evt}', ['evt'=>\Yii::t('app', 'user')]),
            'u_s4_2_f_total' => Yii::t('app/cpf', 'Helicopter accidents - {evt}', ['evt'=>\Yii::t('app', 'user')]),
            'u_s4_2_g_total' => Yii::t('app/cpf', 'Fatal accidents - {evt}', ['evt'=>\Yii::t('app', 'user')]),
            'u_s4_2_h_total' => Yii::t('app/cpf', 'Serious injuries of 5 or more persons in the same accident - {evt}', ['evt'=>\Yii::t('app', 'user')]),
            'u_s4_2_i_total' => Yii::t('app/cpf', 'Evacuation of personnel - {evt}', ['evt'=>\Yii::t('app', 'user')]),
            'u_s4_2_j_total' => Yii::t('app/cpf', 'Environmental accidents - {evt}', ['evt'=>\Yii::t('app', 'user')]),
            'u_s4_3_total' => Yii::t('app/cpf', 'Total number of fatalities and injuries - {evt}', ['evt'=>\Yii::t('app', 'user')]),
            'u_s4_3_fatalities' => Yii::t('app/cpf', 'Number of fatalities - {evt}', ['evt'=>\Yii::t('app', 'user')]),
            'u_s4_3_inj' => Yii::t('app/cpf', 'Total number of injuries - {evt}', ['evt'=>\Yii::t('app', 'user')]),
            'u_s4_3_sinj' => Yii::t('app/cpf', 'Total number of serious injuries - {evt}', ['evt'=>\Yii::t('app', 'user')]),
            'u_s4_4_nsis' => Yii::t('app/cpf', 'Structural integrity systems - {evt}', ['evt'=>\Yii::t('app', 'user')]),
            'u_s4_4_npcs' => Yii::t('app/cpf', 'Process containment systems - {evt}', ['evt'=>\Yii::t('app', 'user')]),
            'u_s4_4_nics' => Yii::t('app/cpf', 'Ignition control systems - {evt}', ['evt'=>\Yii::t('app', 'user')]),
            'u_s4_4_nds' => Yii::t('app/cpf', 'Detection control systems - {evt}', ['evt'=>\Yii::t('app', 'user')]),
            'u_s4_4_npcrs' => Yii::t('app/cpf', 'Process containment relief systems - {evt}', ['evt'=>\Yii::t('app', 'user')]),
            'u_s4_4_nps' => Yii::t('app/cpf', 'Protection systems - {evt}', ['evt'=>\Yii::t('app', 'user')]),
            'u_s4_4_nsds' => Yii::t('app/cpf', 'Shutdown systems - {evt}', ['evt'=>\Yii::t('app', 'user')]),
            'u_s4_4_nnavaids' => Yii::t('app/cpf', 'Navigational aids - {evt}', ['evt'=>\Yii::t('app', 'user')]),
            'u_s4_4_roteq' => Yii::t('app/cpf', 'Rotating equipment - {evt}', ['evt'=>\Yii::t('app', 'user')]),
            'u_s4_4_eere' => Yii::t('app/cpf', 'Escape, evacuation and rescue equipment - {evt}', ['evt'=>\Yii::t('app', 'user')]),
            'u_s4_4_ncoms' => Yii::t('app/cpf', 'Communication systems - {evt}', ['evt'=>\Yii::t('app', 'user')]),
            'u_s4_4_nother' => Yii::t('app/cpf', 'other - {evt}', ['evt'=>\Yii::t('app', 'user')]),
            'u_s4_5_a_total' => Yii::t('app/cpf', 'Incidents caused by Equipment failure - Total - {evt}', ['evt'=>\Yii::t('app', 'user')]),
            'u_s4_5_a_df' => Yii::t('app/cpf', 'Design failures - {evt}', ['evt'=>\Yii::t('app', 'user')]),
            'u_s4_5_a_co__int' => Yii::t('app/cpf', 'Internal corrosion - {evt}', ['evt'=>\Yii::t('app', 'user')]),
            'u_s4_5_a_co__ext' => Yii::t('app/cpf', 'External corrosion - {evt}', ['evt'=>\Yii::t('app', 'user')]),
            'u_s4_5_a_mf__f' => Yii::t('app/cpf', 'Mechanical failures due to fatigue - {evt}', ['evt'=>\Yii::t('app', 'user')]),
            'u_s4_5_a_mf__wo' => Yii::t('app/cpf', 'Mechanical failures due to wear-out - {evt}', ['evt'=>\Yii::t('app', 'user')]),
            'u_s4_5_a_mf__dm' => Yii::t('app/cpf', 'Mechanical failures due to defected material - {evt}', ['evt'=>\Yii::t('app', 'user')]),
            'u_s4_5_a_mf__vh' => Yii::t('app/cpf', 'Mechanical failures (vessel/helicopter) - {evt}', ['evt'=>\Yii::t('app', 'user')]),
            'u_s4_5_a_if' => Yii::t('app/cpf', 'Instrument failures - {evt}', ['evt'=>\Yii::t('app', 'user')]),
            'u_s4_5_a_csf' => Yii::t('app/cpf', 'Control system failures - {evt}', ['evt'=>\Yii::t('app', 'user')]),
            'u_s4_5_a_other' => Yii::t('app/cpf', 'other - {evt}', ['evt'=>\Yii::t('app', 'user')]),
            'u_s4_5_b_total' => Yii::t('app/cpf', 'Human error-operational causes - total - {evt}', ['evt'=>\Yii::t('app', 'user')]),
            'u_s4_5_b_err__op' => Yii::t('app/cpf', 'Operation error - {evt}', ['evt'=>\Yii::t('app', 'user')]),
            'u_s4_5_b_err__mnt' => Yii::t('app/cpf', 'Maintenance error - {evt}', ['evt'=>\Yii::t('app', 'user')]),
            'u_s4_5_b_err__tst' => Yii::t('app/cpf', 'Testing error - {evt}', ['evt'=>\Yii::t('app', 'user')]),
            'u_s4_5_b_err__insp' => Yii::t('app/cpf', 'Inspection error - {evt}', ['evt'=>\Yii::t('app', 'user')]),
            'u_s4_5_b_err__dsgn' => Yii::t('app/cpf', 'Design error - {evt}', ['evt'=>\Yii::t('app', 'user')]),
            'u_s4_5_b_other' => Yii::t('app/cpf', 'other - {evt}', ['evt'=>\Yii::t('app', 'user')]),
            'u_s4_5_c_total' => Yii::t('app/cpf', 'Procedural / Organizational error - {evt}', ['evt'=>\Yii::t('app', 'user')]),
            'u_s4_5_c_inq__rap' => Yii::t('app/cpf', 'Inadequate risk assessment/perception - {evt}', ['evt'=>\Yii::t('app', 'user')]),
            'u_s4_5_c_inq__instp' => Yii::t('app/cpf', 'Inadequate instruction/procedure - {evt}', ['evt'=>\Yii::t('app', 'user')]),
            'u_s4_5_c_nc__prc' => Yii::t('app/cpf', 'Non-compliance with procedure - {evt}', ['evt'=>\Yii::t('app', 'user')]),
            'u_s4_5_c_nc__ptw' => Yii::t('app/cpf', 'Non-compliance with permit-to-work - {evt}', ['evt'=>\Yii::t('app', 'user')]),
            'u_s4_5_c_inq__com' => Yii::t('app/cpf', 'Inadequate communication - {evt}', ['evt'=>\Yii::t('app', 'user')]),
            'u_s4_5_c_inq__pc' => Yii::t('app/cpf', 'Inadequate personnel competence - {evt}', ['evt'=>\Yii::t('app', 'user')]),
            'u_s4_5_c_inq__sup' => Yii::t('app/cpf', 'Inadequate supervision - {evt}', ['evt'=>\Yii::t('app', 'user')]),
            'u_s4_5_c_inq__safelead' => Yii::t('app/cpf', 'Inadequate safety leadership - {evt}', ['evt'=>\Yii::t('app', 'user')]),
            'u_s4_5_c_other' => Yii::t('app/cpf', 'other - {evt}', ['evt'=>\Yii::t('app', 'user')]),
            'u_s4_6' => Yii::t('app/cpf', 'Most important lessons learned - {evt}', ['evt'=>\Yii::t('app', 'user')]),
            'status' => Yii::t('app', 'Status'),
            'assessment_status' => Yii::t('app', 'Assessment Status'),
            'created_at' => ucwords(Yii::t('app', 'created at')),
            'created_by' => ucwords(Yii::t('app', 'created by')),
            'modified_at' => ucwords(Yii::t('app', 'modified at')),
            'modified_by' => ucwords(Yii::t('app', 'modified by')),
            's_s4_5_d_total' => Yii::t('app/cpf', 'Weather-related causes - total - {evt}', ['evt'=>\Yii::t('app', 'user')]),
            's_s4_5_d_exc__design__wind' => Yii::t('app/cpf', 'Wind in excess of limits of design - {evt}', ['evt'=>\Yii::t('app', 'user')]),
            's_s4_5_d_exc__design__wave' => Yii::t('app/cpf', 'Waves in excess of limits of design - {evt}', ['evt'=>\Yii::t('app', 'user')]),
            's_s4_5_d_exc__design__lowvis' => Yii::t('app/cpf', 'Extremely low visibility in excess of limits of design - {evt}', ['evt'=>\Yii::t('app', 'user')]),
            's_s4_5_d_ice__icebergs' => Yii::t('app/cpf', 'Presence of ice or icebergs - {evt}', ['evt'=>\Yii::t('app', 'user')]),
            's_s4_5_d_other' => Yii::t('app/cpf', 'other - {evt}', ['evt'=>\Yii::t('app', 'user')]),
            'u_s4_5_d_total' => Yii::t('app/cpf', 'Weather-related causes - total - {evt}', ['evt'=>'SyRIO']),
            'u_s4_5_d_exc__design__wind' => Yii::t('app/cpf', 'Wind in excess of limits of design - {evt}', ['evt'=>'SyRIO']),
            'u_s4_5_d_exc__design__wave' => Yii::t('app/cpf', 'Waves in excess of limits of design - {evt}', ['evt'=>'SyRIO']),
            'u_s4_5_d_exc__design__lowvis' => Yii::t('app/cpf', 'Extremely low visibility in excess of limits of design - {evt}', ['evt'=>'SyRIO']),
            'u_s4_5_d_ice__icebergs' => Yii::t('app/cpf', 'Presence of ice or icebergs - {evt}', ['evt'=>'SyRIO']),
            'u_s4_5_d_other' => Yii::t('app/cpf', 'other - {evt}', ['evt'=>'SyRIO']),
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReport()
    {
        return $this->hasOne(IncidentCategorization::className(), ['draft_id' => 'report_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getYear0()
    {
        return $this->hasOne(CpfSessions::className(), ['year' => 'year', 'user_id' => 'user_id']);
    }
    
    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            //check whether new record or update
            //update the user's totals
            
            //Section 4.2
            foreach (range('a','j') as $letter) {
                $_s42_attr = CpfCore::cpf_core_attr_s4_x('2.'.$letter);
                if (count($_s42_attr) > 1) {
                    //i.e. total and children
                    $total_attr = 'u_s4_2_'.$letter.'_total';
                    $this->$total_attr = 0;
                    
                    foreach($_s42_attr as $attr) {
                        $c_attr = 'u_'.$attr;
                        if ($attr !== $total_attr) {
                            $usr_attr = 'u_'.$attr;
                            if (isset($this->$usr_attr) && $this->$usr_attr > 0) {
                                //set the total 1
                                $this->$total_attr = 1;
                                //uncomment next line for incremental version
                                //comment-out the break for incremental version
                                //$this->$total_attr += 1;

                                break;
                            }
                        }
                    }
                }
                
//                echo $letter.': <br/>';
//                echo '<pre>';
//                print_r($_s42_attr);
//                echo '</pre>';
//                echo '<br/>';
            }
            
//            die();
            
            //Section 4.5
            foreach(range('a','d') as $letter) {
                $_s45_attr = CpfCore::cpf_core_attr_s4_x('5.'.$letter);
                $total_attr = 'u_s4_5_'.$letter.'_total';
                $this->$total_attr = 0;

                foreach($_s45_attr as $attr) {
                    if (!(strpos($attr, 'total') > -1)) {
                        $usr_attr = 'u_'.$attr;
                        if (isset($this->$usr_attr) && $this->$usr_attr > 0) {
                            //set the total 1
                            $this->$total_attr = 1;
                            //uncomment next line for incremental version
                            //comment-out the break for incremental version
                            //$this->$total_attr += 1;

                            break;
                        }
                    }
                }
            }
            
            
            if ($insert)
            {
                $this->assessment_status = CpfCrfAssessments::ASSESS_STATUS_NEW;
            }
            else {
                $skip = ['year', 'user_id', 'assessment_status', 'modified_at', 'modified_by'];
                
                $dirtyAttr = $this->dirtyAttributes;
                $oldAttr = $this->oldAttributes;

                if (key_exists('assessment_status', $this->oldAttributes)) {
                    //check if the new or old attributes if FINALIZED
                    //if so, set cpf session must refresh to true
                    //get the eventual CpfSession
                    $cpfSession = \app\components\helpers\CaHelper::getCpfSession($this->year);
                    if ($cpfSession !== null) {
                        if ($this->oldAttributes['assessment_status'] === self::ASSESS_STATUS_SUSPENDED) {
                            $cpfSession->refresh_data = 1;
                            //$cpfSession->refresh_data = 1;
                            $cpfSession->update();
                        } else if ($this->assessment_status === self::ASSESS_STATUS_FINALIZED || $this->assessment_status === self::ASSESS_STATUS_SUSPENDED) {
                            $cpfSession->refresh_data = 1;
                            $cpfSession->refresh_installations = 1;
                            $cpfSession->update();
                        } else if ($this->oldAttributes['assessment_status'] === self::ASSESS_STATUS_FINALIZED) {
                            $cpfSession->refresh_data = 1;
                            $cpfSession->refresh_installations = 1;
                            $cpfSession->update();
                        }
                    }
                    
                    if ($this->assessment_status === self::ASSESS_STATUS_FINALIZED && $this->oldAttributes['assessment_status'] === self::ASSESS_STATUS_SUSPENDED) {
                        $description = [
                            'at' => date('Y-m-d H:i:s', \time()),
                        ];

                        //log the event history
                        \app\models\ReportingHistory::addHistoryItem(
                                $this->report->event->id, $this->report->draft_id, 
                                \app\models\ReportingHistory::EVENT_CPF_RESUMED, 
                                json_encode($description), 10);
                    } else if ($this->assessment_status === self::ASSESS_STATUS_SUSPENDED){
                        $description = [
                            'at' => date('Y-m-d H:i:s', \time()),
                        ];
                        //log the event history
                        \app\models\ReportingHistory::addHistoryItem(
                                $this->report->event->id, $this->report->draft_id, 
                                \app\models\ReportingHistory::EVENT_CPF_SUSPENDED, 
                                json_encode($description), 10);
                    }
                    
                }
                
                
                
                $hasChanges = 0;
                foreach($dirtyAttr as $dirty => $val)
                {
                    if ($val != $oldAttr[$dirty] && !in_array($dirty, $skip))
                    {
                        //echo $dirty . '    ' . $val . '    ' . $oldAttr[$dirty] . '<br/>';
                        $hasChanges = 1;                        
                        $this->assessment_status = self::ASSESS_STATUS_IN_PROGRESS;
                    }
                    else
                    {
                        //echo $dirty;
                    }
                    
                }
            }
            return true; 
        } 
        else 
        { 
            return false; 
        }        
    }

    
    public function beforeDelete() {
        if (parent::beforeDelete()) {
            $cpfSession = \app\components\helpers\CaHelper::getCpfSession($this->year);
            if ($cpfSession !== null && $this->assessment_status === self::ASSESS_STATUS_FINALIZED) {
                $cpfSession->refresh_data = 1;
                $cpfSession->refresh_installations = 1;
                $cpfSession->update();                
            }
            
        }
        return parent::beforeDelete();
    }
    
    
    public function InferData()
    {
        /* @var $ca_event \app\models\ca\IncidentCategorization */
        $ca_event = $this->report;

        $is_major = $ca_event->is_major;
        $letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j'];        
        
        /*
         * Compute 4.2.{a-j}.totals - for ALL kind of accidents
         */
        $draft = $ca_event->draft;
        $event = $draft->event;
        foreach ($letters as $letter)
        {
            $is_attr = 'is_'.$letter;                   //event.is_a
            $total_attr = 's_s4_2_'.$letter.'_total';     //s4_2_a_total

            if ($draft->$is_attr == 1) {
                $this->$total_attr = 1;
                //augment the specific 4.2.{letter}
                $this->classify_4_2_x($letter, $draft, $cpf_section4);
            }
        }

        if ($is_major)
        {
            foreach ($letters as $letter)
            {
                $is_attr = 'is_'.$letter;                   //event.is_a
                $total_attr = 's_s4_2_'.$letter.'_total';     //s4_2_a_total
                
                if ($draft->$is_attr == 1) {
                    $this->$total_attr = 1;
                    //augment the specific 4.2.{letter}
                    $this->classify_4_2_x($letter, $draft, $this);
                    if ($letter == 'c')
                    {
                        $this->classify_4_4_x($draft);
                    }
                    if ($letter == 'a')
                    {
                        $this->infer_cpf_4_5($draft);
                    }
                }
            }
        }

    }
    
    /**
     * 
     * @param string $letter {a-j}
     * @param \app\models\events\drafts\EventDraft $draft
     * @param byref \app\models\cpf\CpfSection4 $cpf_section4
     */
    private function classify_4_2_x($letter, $draft, &$cpf_section4)
    {
        //only a, b, d sections must be evaluated here!
        switch ($letter)
        {
            case 'a':
                /* @var $sectionA \app\models\events\drafts\sections\SectionA */        
                //get SectionA
                $sectionA = $draft->sectionA;
                
                //echo '<pre>';
                //echo var_dump($sectionA);
                //echo '</pre>';
                //die();
                
                if ($sectionA->a1->axii_flash_index ||
                        $sectionA->a1->axii_jet_index ||
                        $sectionA->a1->axii_pool_index)
                {
                    $this->s_s4_2_a_if += 1;
                }
                if ($sectionA->a1->axii_exp_index)
                {
                    $this->s_s4_2_a_ix += 1;
                }

                //check for $cpf_section4->s4_2_a_nig
                //the release MUST NOT be IGNITED!
                if ($sectionA->a1->a1i_g == 1 &&
                        $sectionA->a1->axii_exp_index == 0 &&
                        $sectionA->a1->axii_flash_index == 0 &&
                        $sectionA->a1->axii_jet_index == 0 &&
                        $sectionA->a1->axii_pool_index == 0
                        )
                {
                        $this->s_s4_2_a_nig += 1;
                }

                //check for $cpf_section4->s4_2_a_nio
                //the release MUST NOT be IGNITED!
                if ($sectionA->a1->a1i_o == 1 &&
                        $sectionA->a1->axii_exp_index == 0 &&
                        $sectionA->a1->axii_flash_index == 0 &&
                        $sectionA->a1->axii_jet_index == 0 &&
                        $sectionA->a1->axii_pool_index == 0
                        )
                {
                        $this->s_s4_2_a_nio += 1;
                }
                
                //check for $cpf_section4->s4_2_a_haz
                //H2S is considered hazmat (=> gas)
                // + numarul de evenimente in care este declarat release od non-hc hazmat (A.2.1_1 = 1)
                
                if ($sectionA->a1->a1i_g == 1 ||
                        $sectionA->a1->a1i_2p == 1 ||
                        $sectionA->a2->a2_1 == 1
                        )
                {
                        $this->s_s4_2_a_haz += 1;
                }
                
                break;
            case 'b':
                /*
                 * to compute:
                 * - $cpf_section4->s4_2_b_total;    //previously computed
                 * - $cpf_section4->s4_2_b_bo;       //CANNOT be computed; the only information on blow-outs is given in CRF B.2.1_1
                 *                                   //However, CRF B.2.1_1 is also required by CPF s4_2_b_bda
                 * - $cpf_section4->s4_2_b_bda;      //activation of BOP/Diverter systems
                 *                                   // = number of SectionB>b2_1 == 1 OR SectionB>b2_2 == 1
                 * - $cpf_section4->s4_2_b_wbf;      //well barrier failures
                 *                                   // = number of SectionB>b2_4 == 1
                 */
                
                /* @var $sectionB \app\models\events\drafts\sections\SectionA */        
                $sectionB = $draft->sectionB;
                
                if ($sectionB->b2->b2_1 == 1 || $sectionB->b2->b2_2 == 1)
                {
                    $this->s_s4_2_b_bda += 1;
                }
                
                if ($sectionB->b2->b2_4_a == 1 
                        || $sectionB->b2->b2_4_b == 1
                        || $sectionB->b2->b2_4_c == 1)
                {
                    $this->s_s4_2_b_wbf += 1;
                }
                break;
            case 'd':
                /*
                 * to compute:
                 * - $cpf_section4->s4_2_d_si;   //structural integrity                                            
                 * - $cpf_section4->s4_2_d_sb;   //stability/buoyancy
                 * - $cpf_section4->s4_2_d_sk;   //station keeping
                 * 
                 * NONE of these can be computed from CRF
                 */
                break;
        }
    }
    
    
    /**
     * 
     * @param string $letter {a-j}
     * @param \app\models\events\drafts\EventDraft $draft
     * @param \app\models\cpf\CpfSection4 $cpf_section4
     */
    private function classify_4_4_x($draft)
    {
        $classes = [
            'a'=>['attr'=>'s_s4_4_nsis'     ,'value'=>0],
            'b'=>['attr'=>'s_s4_4_npcs'     ,'value'=>0],
            'c'=>['attr'=>'s_s4_4_nics'     ,'value'=>0],
            'd'=>['attr'=>'s_s4_4_nds'      ,'value'=>0],
            'e'=>['attr'=>'s_s4_4_npcrs'    ,'value'=>0],
            'f'=>['attr'=>'s_s4_4_nps'      ,'value'=>0],
            'g'=>['attr'=>'s_s4_4_nsds'     ,'value'=>0],
            'h'=>['attr'=>'s_s4_4_nnavaids' ,'value'=>0],
            'i'=>['attr'=>'s_s4_4_roteq'    ,'value'=>0],
            'j'=>['attr'=>'s_s4_4_eere'     ,'value'=>0],
            'k'=>['attr'=>'s_s4_4_ncoms'    ,'value'=>0],
            'l'=>['attr'=>'s_s4_4_nother'   ,'value'=>0],
        ];
        
        //var_dump($draft->sectionC->c2->sC2_1->seces);
        foreach($draft->sectionC->c2->sC2_1->seces as $sece) 
        {
            if ($sece->id>100 && $sece->id<200)         {$classes['a']['value']=1;}    //{ $add_a = 1; }
            else if ($sece->id>200 && $sece->id<300)    {$classes['b']['value']=1;}    //{ $add_b = 1; }
            else if ($sece->id>300 && $sece->id<400)    {$classes['c']['value']=1;}    //{ $add_c = 1; }
            else if ($sece->id>400 && $sece->id<500)    {$classes['d']['value']=1;}    //{ $add_d = 1; }
            else if ($sece->id>500 && $sece->id<600)    {$classes['e']['value']=1;}    //{ $add_e = 1; }
            else if ($sece->id>600 && $sece->id<700)    {$classes['f']['value']=1;}    //{ $add_f = 1; }
            else if ($sece->id>700 && $sece->id<800)    {$classes['g']['value']=1;}    //{ $add_g = 1; }
            else if ($sece->id>800 && $sece->id<900)    {$classes['h']['value']=1;}    //{ $add_h = 1; }
            else if ($sece->id>900 && $sece->id<1000)   {$classes['i']['value']=1;}    //{ $add_i = 1; }
            else if ($sece->id>1000 && $sece->id<1100)  {$classes['j']['value']=1;}    //{ $add_j = 1; }
            else if ($sece->id>1100 && $sece->id<1200)  {$classes['k']['value']=1;}    //{ $add_k = 1; }
            //else if ($sece->id>1200 && $sece->id<1300) { $add_l = 1; }
            
            //echo $sece->id . '<br/>';
        }
        
        //check the 'other' for each of the a-l sections
        foreach (range('a','l') as $lttr) {
            $attr = 'c21_3_'.$lttr;
            if (isset($draft->sectionC->c2->sC2_1->$attr) && $draft->sectionC->c2->sC2_1->$attr == 1) {
                $classes[$lttr]['value'] = 1;
            }
            
            $this->$classes[$lttr]['attr'] = $classes[$lttr]['value'];
        }
        
    }
        
    /**
     * @param \app\models\events\drafts\EventDraft $draft
     * @param \app\models\cpf\CpfSection4 $cpf_section4
     * @since 1.3.
     */
    private function infer_cpf_4_5($draft)
    {
        /* @var $sectionA \app\models\events\drafts\sections\SectionA */        
        
        $sectionA = $draft->sectionA;

        $_causes = array_keys(\app\components\helpers\TArrayHelper::tIndex($sectionA->a1->releaseCauses, 'num_code'));  //e.g. [1104, 1206]
        $_causes_categories = array_keys(\app\components\helpers\TArrayHelper::tIndex($sectionA->a1->releaseCauses, 'category_id'));  //e.g. [1104, 1206]
        
//        echo '<pre>';
//        echo var_dump($_causes);
//        echo var_dump($_causes_categories);
//        echo '<pre>';
        
        //echo 'INFERENCE RULES: <br/>';
        
//        echo '<pre>';
//        echo var_dump(CpfCore::InferenceRules());
//        echo '<pre>';

        foreach (CpfCore::InferenceRules() as $key => $item) {
            $syrio_attr = 's_'.$key;        //e.g. 's_s4_5_a_total'
            foreach ($item['elements'] as $element=>$rules) {
                /* e.g.
                 *  's4_5_b_err__op' => [         //event
                        'if_any' => [
                            1202,       //left open
                            1207,       //dropped object
                            1209,       //opened when containing HC
                            1205,       //improper operation
                            ], //
                        'other' => [],
                        'value' => 0
                    ],
                 */
                $syrio_c_attr = 's_'.$element;
                /*
                 * check the boolean rule
                 */
                
                $_selected = 0;
                foreach($rules['if_any'] as $condition) {
                    //check in the current draft's $_causes if the code exists
                    if (in_array($condition, $_causes)) {
                        $_selected = 1;
                        /* also set the parent as 1
                         * NOTE:    in this version, the total is maximum 1;
                         *          this basically corresponds to the following assumption:
                         *          - the case (incident) is accounted in the final report
                         *          - one incident can have more than one causes.
                         * 
                         *          For the second possible approach, the _total (parent) should be incremented
                         */
                        $this->$syrio_attr = 1;
                        //uncomment this line if the incremental version is adopted
                        //$this->$syrio_attr+=1;
                    }
                }
                //check if 'other' values should be checked
                foreach($rules['other'] as $key=>$value) {
//                    echo $key.'=>'.$value.'<br/>';
                    
                    if (isset($sectionA->a1->$key) && $sectionA->a1->$key == $value) {
                        $_selected = 1;
                        $this->$syrio_attr = 1;
                    }
                }

                /* write the current element */
                $this->$syrio_c_attr = $_selected;
                //echo '> '. $element .'<br/>';
            }
        }       //end inference rules loop
        
//        echo '<pre>';
//        echo var_dump($this);
//        echo '<pre>';

    }        
    
}
