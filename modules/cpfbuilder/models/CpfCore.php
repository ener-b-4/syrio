<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\modules\cpfbuilder\models;

class CpfCore {
    public static function cpf_core_attr_s4() {
        return [
        /**
         * This is the model class for table "cpf_section4".
         */
        's4_1_n_events_total',
        's4_1_n_major_accidents',
        's4_2_a_total',
        's4_2_a_if',
        's4_2_a_ix',
        's4_2_a_nig',
        's4_2_a_nio',
        's4_2_a_haz',
        's4_2_b_total',
        's4_2_b_bo',
        's4_2_b_bda',
        's4_2_b_wbf',
        's4_2_c_total',
        's4_2_d_total',
        's4_2_d_si',
        's4_2_d_sb',
        's4_2_d_sk',
        's4_2_e_total',
        's4_2_f_total',
        's4_2_g_total',
        's4_2_h_total',
        's4_2_i_total',
        's4_2_j_total',
        //'s4_3_total',
        's4_3_fatalities',
        's4_3_inj',
        's4_3_sinj',
        's4_4_nsis',
        's4_4_npcs',
        's4_4_nics',
        's4_4_nds',
        's4_4_npcrs',
        's4_4_nps',
        's4_4_nsds',
        's4_4_nnavaids',
        's4_4_roteq',
        's4_4_eere',
        's4_4_ncoms',
        's4_4_nother',
            
        's4_5_a_total',
        's4_5_a_df',
        's4_5_a_co__int',
        's4_5_a_co__ext',
        's4_5_a_mf__f',
        's4_5_a_mf__wo',
        's4_5_a_mf__dm',
        's4_5_a_mf__vh',
        's4_5_a_if',
        's4_5_a_csf',
        's4_5_a_other',
        's4_5_b_total',
        's4_5_b_err__op',
        's4_5_b_err__mnt',
        's4_5_b_err__tst',
        's4_5_b_err__insp',
        's4_5_b_err__dsgn',
        's4_5_b_other',
            
        's4_5_c_total',
        's4_5_c_inq__rap',
        's4_5_c_inq__instp',
        's4_5_c_nc__prc',
        's4_5_c_nc__ptw',
        's4_5_c_inq__com',
        's4_5_c_inq__pc',
        's4_5_c_inq__sup',
        's4_5_c_inq__safelead',
        's4_5_c_other',
            
        's4_5_d_total',
        's4_5_d_exc__design__wind',
        's4_5_d_exc__design__wave',
        's4_5_d_exc__design__lowvis',
        's4_5_d_ice__icebergs',
        's4_5_d_other',

        '4_6',            
        ];
    }    

    public static function InferenceRules() {       
        $a_part = [
            's4_5_a_total' => [
                'value' => 0,
                'elements' => [             //11 elements
                    's4_5_a_df' => [        //design failure
                        'if_any' => [1001], //
                        'other' => [],
                        'value' => 0
                    ],
                    's4_5_a_co__int' => [        //corrosion / internal
                        'if_any' => [1101], //
                        'other' => [],
                        'value' => 0
                    ],
                    's4_5_a_co__ext' => [        //corrosion / external
                        'if_any' => [1102], //
                        'other' => [],
                        'value' => 0
                    ],
                    's4_5_a_mf__f' => [        //mechanical failure / fatigue
                        'if_any' => [1103], //
                        'other' => [],
                        'value' => 0
                    ],
                    's4_5_a_mf__wo' => [       //mechanical failure / worn-out
                        'if_any' => [1104], //
                        'other' => [],
                        'value' => 0
                    ],
                    's4_5_a_mf__dm' => [        //mechanical failure / material defect
                        'if_any' => [1106], //
                        'other' => [],
                        'value' => 0
                    ],
                    's4_5_a_mf__vh' => [        //mechanical failure / vessel/helicopter
                        'if_any' => [], //
                        'other' => [],
                        'value' => 0
                    ],
                    's4_5_a_if' => [            //instrument failure
                        'if_any' => [], //
                        'other' => [],
                        'value' => 0
                    ],
                    's4_5_a_csf' => [           //control system failure
                        'if_any' => [], //
                        'other' => [],
                        'value' => 0
                    ],
                    's4_5_a_other' => [         //equipment related / other
                        'if_any' => [1105],     //erosion
                        'other' => ['a1_fail_equipment_code'=>2],
                        'value' => 0
                    ],
                ]
            ],
        ];
        
        //B Human error / operational failure
        $b_part = [
            's4_5_b_total' => [         //event
                'value' => 0,
                'elements' => [         // 6 children
                    's4_5_b_err__op' => [         //event
                        'if_any' => [
                            1202,       //left open
                            1207,       //dropped object
                            1209,       //opened when containing HC
                            1205,       //improper operation
                            ], //
                        'other' => [],
                        'value' => 0
                    ],
                    's4_5_b_err__mnt' => [
                        'if_any' => [
                            1206,       //improper maintenance
                            ], //
                        'other' => [],
                        'value' => 0
                    ],
                    's4_5_b_err__tst' => [         //event
                        'if_any' => [
                            1204,       //improper testing
                            ], //
                        'other' => [],
                        'value' => 0
                    ],
                    's4_5_b_err__insp' => [         //event
                        'if_any' => [
                            1203,       //improper inspection
                            ], //
                        'other' => [],
                        'value' => 0
                    ],
                    's4_5_b_err__dsgn' => [         //event
                        'if_any' => [], //
                        'other' => [],
                        'value' => 0
                    ],
                    's4_5_b_other' => [         //event
                        'if_any' => [
                            1201,   //incorectly fitted
                            1208,   //other impact
                            ], //
                        'other' => ['a1_fail_operation_code'=>2],
                        'value' => 0
                    ],
                ]
            ],
        ];
        
        $c_part = [     //PROCEDURAL / ORGANIZATION ERROR
            's4_5_c_total' => [
                'value'=>0,
                'elements' => [     //9 CHILDREN
                    's4_5_c_inq__rap' => [         //CPF INADEQUATE RISK ASSESSMENT/PERCEPTION
                        'if_any' => [                            
                            ], //
                        'other' => [],
                        'value' => 0
                    ],
                    's4_5_c_inq__instp' => [         //CPF INADEQUATE INSTRUCTION/PROCEDURE
                        'if_any' => [
                            1303    //DEFICIENT PROCEDURE
                            ], //
                        'other' => [],
                        'value' => 0
                    ],
                    's4_5_c_nc__prc' => [         //CPF NON-COMPLIENCE WITH PROCEDURE
                        'if_any' => [
                            1301    //NON-COMPLIANCE WITH PROCEDURE
                            ], //
                        'other' => [],
                        'value' => 0
                    ],
                    's4_5_c_nc__ptw' => [         //CPF NON-COMPLIENCE WITH PERMIT TO WORK
                        'if_any' => [
                            1302    //NON-COMPLIANCE WITH PERMIT TO WORK
                            ], //
                        'other' => [],
                        'value' => 0
                    ],
                    's4_5_c_inq__com' => [         //CPF INNADEQUATE COMMUNICATION
                        'if_any' => [
                            ], //
                        'other' => [],
                        'value' => 0
                    ],
                    's4_5_c_inq__pc' => [         //CPF INNADEQUATE PERSONNEL COMPETENCE
                        'if_any' => [
                            ], //
                        'other' => [],
                        'value' => 0
                    ],
                    's4_5_c_inq__sup' => [         //CPF INNADEQUATE SUPERVISION
                        'if_any' => [
                            ], //
                        'other' => [],
                        'value' => 0
                    ],
                    's4_5_c_inq__safelead' => [    //CPF INNADEQUATE SAFETY LEADERSHIP
                        'if_any' => [
                            ], //
                        'other' => [],
                        'value' => 0
                    ],
                    's4_5_c_other' => [    //CPF INNADEQUATE SAFETY LEADERSHIP
                        'if_any' => [
                            ], //
                        'other' => ['a1_fail_procedural_code' => 2],
                        'value' => 0
                    ],
                ]
            ]
        ];
        
        
        $d_part = [
            's4_5_d_total' => [
                'value'=>0,
                'elements' => [
                    's4_5_d_exc__design__wind' => [    //CPF WIND IN EXCESS
                        'if_any' => [
                            ], //
                        'other' => [],
                        'value' => 0
                    ],
                    's4_5_d_exc__design__wave' => [    //CPF WAVE IN EXCESS
                        'if_any' => [
                            ], //
                        'other' => [],
                        'value' => 0
                    ],
                    's4_5_d_exc__design__lowvis' => [    //CPF LOW VISIBILITY
                        'if_any' => [
                            ], //
                        'other' => [],
                        'value' => 0
                    ],
                    's4_5_d_ice__icebergs' => [    //CPF PRESENCE OF ICEBERGS
                        'if_any' => [
                            ], //
                        'other' => [],
                        'value' => 0
                    ],
                    's4_5_d_other' => [    //CPF OTHER EXTREME WEATHER
                        'if_any' => [
                            ], //
                        'other' => [],
                        'value' => 0
                    ],
                ]
            ]
        ];

        $ret = [];
        $ret = \yii\helpers\ArrayHelper::merge($ret, $a_part);
        $ret = \yii\helpers\ArrayHelper::merge($ret, $b_part);
        $ret = \yii\helpers\ArrayHelper::merge($ret, $c_part);
        $ret = \yii\helpers\ArrayHelper::merge($ret, $d_part);
        
        return $ret;
    }
    
    
    public static function cpf_core_attr_s4_1() {
        return preg_grep('~^s4_1~', self::cpf_core_attr_s4());
    }    
    
    public static function cpf_core_attr_s4_x($x) {
        $pattern = '~^s4_'.$x.'~';
        return preg_grep($pattern, self::cpf_core_attr_s4());
    }    
    
    
    public static function cpf_inferrable_attr() {
        return [
        /**
         * This is the model class for table "cpf_section4".
         */
        's4_1_n_events_total' => 'all the events reported over the current year',
        's4_1_n_major_accidents' => 'The event has been classified as Major Accident',
        's4_2_a_total' => 'The event has been classified as type A. Unintended releases...',    //new 1.3
        's4_2_a_if' => 'Either ignited oil or ignited gas (flash, jet, pool) has been declared in the report. See Section A.1.XII',
        's4_2_a_ix' => 'Explosion of hydrocarbons has been declared in the report. See Section A.1.XII',
        's4_2_a_nig' => 'No gas release (see Section A.1.I) or No ignition has been declared (see Section A.1.XII)',
        's4_2_a_nio' => 'No oil release (see Section A.1.I) or No ignition has been declared (see Section A.1.XII)',
        's4_2_a_haz' => 'There has been a release of H2S (see Section A.1.I) or of another non-hydrocarbon hazardous substance (see Section A.2.1)',
        's4_2_b_total' => 'The event has been classified as type B. Loss of well control...',   //new 1.3
        's4_2_b_bda' => 'Blowout prevention equipment has been activated and/or Diverter system in operation has been declared (see Section B.2)',
        's4_2_b_wbf' => 'There has been at least one well barrier failure declared (see Section B.2.)',
        's4_2_c_total' => 'The event has been classified as type C. Failure of a Safety and Environmental Critical Element',
        's4_2_d_total' => 'The event has been classified as type D. Significant Loss of Structural Integrity...',   //new 1.3
        //'s4_2_d_si' => '(pending)',
        //'s4_2_d_sb' => '(pending)',
        //'s4_2_d_sk' => '(pending)',
        's4_2_e_total' => 'The event has been classified as type E. Vessels on Collission Course and Actual Vessels Collissions...',    //new 1.3
        's4_2_f_total' => 'The event has been classified as type F. Helicopter accidents, On or Near Offshore Installations',
        's4_2_g_total' => 'Section G has been reported',
        's4_2_h_total' => 'Section H has been reported',
        's4_2_i_total' => 'The event has been classified as type I. Any Evacuation of Personnel',
        's4_2_j_total' => 'The event has been classified as type J. A Major Environmental Incident',
        //'s4_3_total' => '(pending)',
        //'s4_3_fatalities',
        //'s4_3_inj',
        //'s4_3_sinj',
        's4_4_nsis' => 'At least one structural integrity system has been declared as failed (see Section C.2.1. Tabel Safety and Environmental Critical Elements Concerned - point (a)).',
        's4_4_npcs' => 'At least one process containment system has been declared as failed (see Section C.2.1. Tabel Safety and Environmental Critical Elements Concerned - point (b)).',
        's4_4_nics' => 'At least one ignition control system has been declared as failed (see Section C.2.1. Tabel Safety and Environmental Critical Elements Concerned - point (c)).',
        's4_4_nds' => 'At least one detection system has been declared as failed (see Section C.2.1. Tabel Safety and Environmental Critical Elements Concerned - point (d)).',
        's4_4_npcrs' => 'At least one process containment relief system has been declared as failed (see Section C.2.1. Tabel Safety and Environmental Critical Elements Concerned - point (e)).',
        's4_4_nps' => 'At least one protection system has been declared as failed (see Section C.2.1. Tabel Safety and Environmental Critical Elements Concerned - point (f)).',
        's4_4_nsds' => 'At least one shutdown system has been declared as failed (see Section C.2.1. Tabel Safety and Environmental Critical Elements Concerned - point (g)).',
        's4_4_nnavaids' => 'At least one of the navigational aids has been declared as failed (see Section C.2.1. Tabel Safety and Environmental Critical Elements Concerned - point (h)).',
        's4_4_roteq' => 'Rotating equipment - power supply system failure has been declared. (see Section C.2.1. Tabel Safety and Environmental Critical Elements Concerned - point (i)).',
        's4_4_eere' => 'At least one escape, evacuation and rescue system has been declared as failed (see Section C.2.1. Tabel Safety and Environmental Critical Elements Concerned - point (j)).',
        's4_4_ncoms' => 'Communication system has been declared as failed (see Section C.2.1. Tabel Safety and Environmental Critical Elements Concerned - point (k)).',
        's4_4_nother' => 'Other SECE has been declared as failed (see Section C.2.1. Tabel Safety and Environmental Critical Elements Concerned - point (l)).',
        
        //'s4_5_a_total'      => \Yii::t('app/cpf', 'Sum of all the release-type incidents caused by Design fault (see Section A.1 Cause of leak checklist'),
        's4_5_a_df'         => \Yii::t('app/cpf', 'Design failure has been reported as one of the causes of the release (see Section A.1 Cause of leak checklist (a))'),
        's4_5_a_co__int'    => \Yii::t('app/cpf', 'Internal corrosion has been reported as one of the causes of the release (see Section A.1 Cause of leak checklist (b))'),
        's4_5_a_co__ext'    => \Yii::t('app/cpf', 'External corrosion has been reported as one of the causes of the release (see Section A.1 Cause of leak checklist (b))'),
        's4_5_a_mf__f'      => \Yii::t('app/cpf', 'Mechanical failure due to fatigue has been reported as one of the causes of the release (see Section A.1 Cause of leak checklist (b))'),
        's4_5_a_mf__wo'     => \Yii::t('app/cpf', 'Mechanical fatigue due to wear out has been reported as one of the causes of the release (see Section A.1 Cause of leak checklist (b))'),
        's4_5_a_mf__dm'     => \Yii::t('app/cpf', 'Material defect has been reported as one of the causes of the release (see Section A.1 Cause of leak checklist (b))'),
        //'s4_5_a_mf__vh'     => \Yii::t('app/cpf', '(pending)'),   //cannot tell if vessel/helicopter mechanical failures
        //'s4_5_a_if'         => \Yii::t('app/cpf', '(pending)'),
        //'s4_5_a_csf'        => \Yii::t('app/cpf', '(pending)'),
        //'s4_5_a_other'      => \Yii::t('app/cpf', 'Other has been reported as one of the causes of the release (see Section A.1 Cause of leak checklist (a),(b))'),
        //'s4_5_b_total'    => '(pending)',)
        's4_5_b_err__op'    => \Yii::t('app/cpf', 'At least one of: Improper operation, Left open, Opened when containing HC, Dropped object, has been reported as one of the causes of the release (see Section A.1 Cause of leak checklist (c))'),
        's4_5_b_err__mnt'   => \Yii::t('app/cpf', 'Improper maintenance has been reported as one of the causes of the release (see Section A.1 Cause of leak checklist (c))'),
        's4_5_b_err__tst'   => \Yii::t('app/cpf', 'Improper testing has been reported as one of the causes of the release (see Section A.1 Cause of leak checklist (c))'),
        's4_5_b_err__insp'  => \Yii::t('app/cpf', 'Improper inspection has been reported as one of the causes of the release (see Section A.1 Cause of leak checklist (c))'),
        //'s4_5_b_err__dsgn',
        's4_5_b_other' => \Yii::t('app/cpf', 'Other or Erosion has been reported as one of the (equipment) causes of the release (see Section A.1 Cause of leak checklist (b))'),
        //'s4_5_c_total',
        //'s4_5_c_inq__rap',
        's4_5_c_inq__instp' => \Yii::t('app/cpf', 'Deficient procedure has been reported as one of the causes of the release (see Section A.1 Cause of leak checklist (d))'),
        's4_5_c_nc__prc'    => \Yii::t('app/cpf', 'Non-compliance with procedure has been reported as one of the causes of the release (see Section A.1 Cause of leak checklist (d))'),
        's4_5_c_nc__ptw'    => \Yii::t('app/cpf', 'Non-compliance with permit-to-work has been reported as one of the causes of the release (see Section A.1 Cause of leak checklist (d))'),
        //'s4_5_c_inq__com',
        //'s4_5_c_inq__pc',
        //'s4_5_c_inq__sup',
        //'s4_5_c_inq__safelead',
        's4_5_c_other'      => \Yii::t('app/cpf', 'Other has been reported as one of the (procedural) causes of the release (see Section A.1 Cause of leak checklist (d))'),
            
        //'s4_5_d_total',
        //'s4_5_d_exc__design__wind',
        //'s4_5_d_exc__design__wave',
        //'s4_5_d_exc__design__lowvis',
        //'s4_5_d_ice__icebergs',
        //'s4_5_d_other',

        //'4_6',            
        ];
    }        
    
    static function SectionNames() {
        return [
            '4.2' => ucwords(\Yii::t('app/cpf', 'Annex IX categories')),
            '4.3' => \Yii::t('app/cpf', 'Total number of fatalities and injuries'),
            '4.4' => \Yii::t('app/cpf', 'Failures of Safety and Environmental Critical Elements'),
            '4.5' => \Yii::t('app/cpf', 'Direct and Underlying causes of major incidents'),
            '4.6' => \Yii::t('app/cpf', 'Which are the most important lessons learned from the incidents that deserve to be shared?'),
        ];
    }
}
