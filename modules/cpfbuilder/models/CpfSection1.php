<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\modules\cpfbuilder\models;

use Yii;

/**
 * This is the model class for table "cpf_section1".
 *
 * @property integer $year
 * @property integer $user_id
 * @property string $ms
 * @property string $ca
 * @property string $dra
 * @property string $phone
 * @property string $email
 *
 * @property CpfSessions $cpfSession
 */
class CpfSection1 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cpf_section1';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['year', 'user_id', 'ms', 'ca', 'dra', 'phone', 'email'], 'required'],
            [['year', 'user_id'], 'integer'],
            [['ms'], 'string', 'max' => 2],
            [['ca', 'dra', 'email'], 'string', 'max' => 128],
            [['phone'], 'string', 'max' => 16]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'year' => Yii::t('app/cpf', 'Year'),
            'user_id' => 'User ID',
            'ms' => Yii::t('app/cpf', 'Ms'),
            'ca' => Yii::t('app/cpf', 'Ca'),
            'dra' => Yii::t('app/cpf', 'Dra'),
            'phone' => Yii::t('app/cpf', 'Phone'),
            'email' => Yii::t('app/cpf', 'Email'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCpfSession()
    {
        return $this->hasOne(CpfSessions::className(), ['year' => 'year', 'user_id' => 'user_id']);
    }
}
