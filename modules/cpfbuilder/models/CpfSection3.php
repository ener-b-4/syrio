<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\modules\cpfbuilder\models;

use Yii;
use app\modules\cpfbuilder\models\CpfReferences;

/**
 * This is the model class for table "cpf_section3".
 *
 * @property integer $year
 * @property integer $user_id
 * @property integer $n_offshore_inspections
 * @property double $man_days
 * @property integer $n_inpeceted
 * @property integer $n_major_accidents
 * @property integer $n_env_concerns
 * @property string $s3_3_text
 * @property string $description
 * @property string $rationale
 * @property string $expected_outcome
 *
 * @property CpfSessions $cpfSession
 * @property CpfReferences $references
 */
class CpfSection3 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cpf_section3';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['year', 'user_id'], 'required'],
            [['year', 'user_id', 'n_offshore_inspections', 'n_inpeceted', 'n_major_accidents', 'n_env_concerns'], 'integer'],
            [['man_days'], 'number'],
            [['s3_3_text', 'description', 'rationale', 'expected_outcome'], 'string'],
            //[['n_offshore_inspections', 'n_inpeceted', 'n_major_accidents', 'n_env_concerns'], 'required', 'on'=>'xml'],
            [['man_days'], 'required', 'on'=>'xml', 'message'=>\Yii::t('app/cpf', 'You must provide a value for {evt} ({sec})',
                    ['evt'=>\Yii::t('app/cpf', 'Man-days spent on installation'), 'sec'=>\Yii::t('app/crf', 'Section {evt}', ['evt'=>' 3.1'])])],
            [['n_offshore_inspections'], 'required', 'on'=>'xml', 'message'=>\Yii::t('app/cpf', 'You must provide a value for {evt} ({sec})',
                    ['evt'=>\Yii::t('app/cpf', 'Number of offshore inspections'), 'sec'=>\Yii::t('app/crf', 'Section {evt}', ['evt'=>' 3.1'])])],
            [['n_inpeceted'], 'required', 'on'=>'xml', 'message'=>\Yii::t('app/cpf', 'You must provide a value for {evt} ({sec})',
                    ['evt'=>\Yii::t('app/cpf', 'Number of inspected installations'), 'sec'=>\Yii::t('app/crf', 'Section {evt}', ['evt'=>' 3.1'])])],
            [['n_major_accidents'], 'required', 'on'=>'xml', 'message'=>\Yii::t('app/cpf', 'You must provide a value for {evt} ({sec})',
                    ['evt'=>\Yii::t('app/cpf', '(number of) Major accidents'), 'sec'=>\Yii::t('app/crf', 'Section {evt}', ['evt'=>' 3.2(a)'])])],
            [['n_env_concerns'], 'required', 'on'=>'xml', 'message'=>\Yii::t('app/cpf', 'You must provide a value for {evt} ({sec})',
                    ['evt'=>\Yii::t('app/cpf', '(number of) Environmental concerns'), 'sec'=>\Yii::t('app/crf', 'Section {evt}', ['evt'=>' 3.2(b)'])])],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'year' => Yii::t('app/cpf', 'Year'),
            'user_id' => 'User ID',
            'n_offshore_inspections' => Yii::t('app/cpf', 'Number of offshore inspections'),
            'man_days' => Yii::t('app/cpf', 'Man-days spent on installation (travel time not included)'),
            'n_inpeceted' => Yii::t('app/cpf', 'Number of inspected installations'),
            'n_major_accidents' => Yii::t('app/cpf', 'Major accidents'),
            'n_env_concerns' => Yii::t('app/cpf', 'Environmental concerns'),
            's3_3_text' => Yii::t('app/cpf', 'Main enforcement actions'),
            'description' => Yii::t('app', 'Description'),
            'rationale' => Yii::t('app/cpf', 'Rationale'),
            'expected_outcome' => Yii::t('app/cpf', 'Expected outcome'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCpfSession()
    {
        return $this->hasOne(CpfSessions::className(), ['year' => 'year', 'user_id' => 'user_id']);
    }
    
    /**
     * 
     */
    public function getReferences()
    {
        return $this->hasMany(CpfReferences::className(), ['id'=>'ref_id'])
                ->viaTable('cpf_report_refs', ['year'=>'year', 'user_id'=>'user_id']);
        
        
        //$refs = CpfReferences::find()
        //        ->where(['year' => $this->year])
        //        ->andWhere(['user_id'=>$this->user_id])->all();
        
        //return $refs;        
        
        //return $this->cpfSession->cpfReferences;
        //return $this->hasMany(CpfReferences::className(), ['year' => 'year', 'user_id' => 'user_id']);
    }
    
}
