<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\modules\cpfbuilder\models;

use Yii;

/**
 * This is the model class for table "cpf_references".
 *
 * @property integer $id
 * @property integer $year
 * @property integer $user_id
 * @property string $name
 * @property string $url
 *
 * @property CpfSessions $year0
 */
class CpfReferences extends \yii\db\ActiveRecord
{
    
    public $_ref;
    
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cpf_references';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['url'], 'string', 'max' => 512],
            [['url'], 'url', 'defaultScheme' => 'http'],
            [['_ref'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'year' => Yii::t('app/cpf', 'Year'),
            'user_id' => 'User ID',
            'name' => ucfirst(Yii::t('app', 'title')),
            'url' => Yii::t('app/cpf', 'Url'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCpfs()
    {
        return $this->hasMany(CpfSessions::className(), ['year'=>'year', 'user_id'=>'user_id'])
                ->viaTable('cpf_report_refs', ['ref_id'=>'id']);
        
        //return $this->hasMany(CpfSessions::className(), ['year' => 'year', 'user_id' => 'user_id']);
    }
    
    
    
}
