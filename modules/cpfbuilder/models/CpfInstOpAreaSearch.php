<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\modules\cpfbuilder\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\cpfbuilder\models\CpfInstOpArea;

/**
 * CpfInstOpAreaSearch represents the model behind the search form about `app\modules\cpfbuilder\models\CpfInstOpArea`.
 */
class CpfInstOpAreaSearch extends CpfInstOpArea
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'installation_id', 'year', 'user_id'], 'integer'],
            [['name'], 'safe'],
            [['duration'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CpfInstOpArea::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'installation_id' => $this->installation_id,
            'year' => $this->year,
            'user_id' => $this->user_id,
            'duration' => $this->duration,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
