<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\modules\cpfbuilder\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\cpfbuilder\models\CpfReferences;

/**
 * CpfReferencesSearch represents the model behind the search form about `app\modules\cpfbuilder\models\CpfReferences`.
 */
class CpfReferencesSearchEx extends CpfReferences
{
    public $year;
    public $user_id;
    
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'year', 'user_id'], 'integer'],
            [['name', 'url'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $subquery = new \yii\db\Query();
        $subquery->select('{{cpf_report_refs}}.[[ref_id]]')
                ->from('{{cpf_report_refs}}')
                ->where(['{{cpf_report_refs}}.[[year]]'=>$this->year])                
                ->andWhere('{{cpf_report_refs}}.[[ref_id]]={{cpf_references}}.[[id]]');
        
        $query = CpfReferences::find()
                ->join('left join', '{{cpf_report_refs}}', '{{cpf_report_refs}}.[[ref_id]] = {{cpf_references}}.[[id]]')
                ->where(['not exists', $subquery]);

        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'url', $this->url]);

        return $dataProvider;
    }
}
