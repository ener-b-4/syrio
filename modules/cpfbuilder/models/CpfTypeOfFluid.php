<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\modules\cpfbuilder\models;

use Yii;

/**
 * This is the model class for table "cpf_tof".
 *
 * @property string $type
 * @property string $name
 *
 * @property CpfInstallations[] $cpfInstallations
 */
class CpfTypeOfFluid extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cpf_tof';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'name'], 'required'],
            [['type'], 'string', 'max' => 16],
            [['name'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'type' => ucfirst(Yii::t('app', 'type')),
            'name' => Yii::t('app/cpf', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCpfInstallations()
    {
        return $this->hasMany(CpfInstallations::className(), ['tof' => 'type']);
    }
}
