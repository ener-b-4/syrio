<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\modules\cpfbuilder\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\cpfbuilder\models\CpfCrfAssessments;

/**
 * CpfCrfAssessmentsSearch represents the model behind the search form about `app\modules\cpfbuilder\models\CpfCrfAssessments`.
 */
class CpfCrfAssessmentsSearch extends CpfCrfAssessments
{
    
    public $report_id;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['year', 'user_id', 's_s4_2_a_total', 's_s4_2_a_if', 's_s4_2_a_ix', 's_s4_2_a_nig', 's_s4_2_a_nio', 's_s4_2_a_haz', 's_s4_2_b_total', 's_s4_2_b_bo', 's_s4_2_b_bda', 's_s4_2_b_wbf', 's_s4_2_c_total', 's_s4_2_d_total', 's_s4_2_d_si', 's_s4_2_d_sb', 's_s4_2_d_sk', 's_s4_2_e_total', 's_s4_2_f_total', 's_s4_2_g_total', 's_s4_2_h_total', 's_s4_2_i_total', 's_s4_2_j_total', 's_s4_3_total', 's_s4_3_fatalities', 's_s4_3_inj', 's_s4_3_sinj', 's_s4_4_nsis', 's_s4_4_npcs', 's_s4_4_nics', 's_s4_4_nds', 's_s4_4_npcrs', 's_s4_4_nps', 's_s4_4_nsds', 's_s4_4_nnavaids', 's_s4_4_roteq', 's_s4_4_eere', 's_s4_4_ncoms', 's_s4_4_nother', 's_s4_5_a_total', 's_s4_5_a_df', 's_s4_5_a_co__int', 's_s4_5_a_co__ext', 's_s4_5_a_mf__f', 's_s4_5_a_mf__wo', 's_s4_5_a_mf__dm', 's_s4_5_a_mf__vh', 's_s4_5_a_if', 's_s4_5_a_csf', 's_s4_5_a_other', 's_s4_5_b_total', 's_s4_5_b_err__op', 's_s4_5_b_err__mnt', 's_s4_5_b_err__tst', 's_s4_5_b_err__insp', 's_s4_5_b_err__dsgn', 's_s4_5_b_other', 's_s4_5_c_total', 's_s4_5_c_inq__rap', 's_s4_5_c_inq__instp', 's_s4_5_c_nc__prc', 's_s4_5_c_nc__ptw', 's_s4_5_c_inq__com', 's_s4_5_c_inq__pc', 's_s4_5_c_inq__sup', 's_s4_5_c_inq__safelead', 's_s4_5_c_other', 'u_s4_2_a_total', 'u_s4_2_a_if', 'u_s4_2_a_ix', 'u_s4_2_a_nig', 'u_s4_2_a_nio', 'u_s4_2_a_haz', 'u_s4_2_b_total', 'u_s4_2_b_bo', 'u_s4_2_b_bda', 'u_s4_2_b_wbf', 'u_s4_2_c_total', 'u_s4_2_d_total', 'u_s4_2_d_si', 'u_s4_2_d_sb', 'u_s4_2_d_sk', 'u_s4_2_e_total', 'u_s4_2_f_total', 'u_s4_2_g_total', 'u_s4_2_h_total', 'u_s4_2_i_total', 'u_s4_2_j_total', 'u_s4_3_total', 'u_s4_3_fatalities', 'u_s4_3_inj', 'u_s4_3_sinj', 'u_s4_4_nsis', 'u_s4_4_npcs', 'u_s4_4_nics', 'u_s4_4_nds', 'u_s4_4_npcrs', 'u_s4_4_nps', 'u_s4_4_nsds', 'u_s4_4_nnavaids', 'u_s4_4_roteq', 'u_s4_4_eere', 'u_s4_4_ncoms', 'u_s4_4_nother', 'u_s4_5_a_total', 'u_s4_5_a_df', 'u_s4_5_a_co__int', 'u_s4_5_a_co__ext', 'u_s4_5_a_mf__f', 'u_s4_5_a_mf__wo', 'u_s4_5_a_mf__dm', 'u_s4_5_a_mf__vh', 'u_s4_5_a_if', 'u_s4_5_a_csf', 'u_s4_5_a_other', 'u_s4_5_b_total', 'u_s4_5_b_err__op', 'u_s4_5_b_err__mnt', 'u_s4_5_b_err__tst', 'u_s4_5_b_err__insp', 'u_s4_5_b_err__dsgn', 'u_s4_5_b_other', 'u_s4_5_c_total', 'u_s4_5_c_inq__rap', 'u_s4_5_c_inq__instp', 'u_s4_5_c_nc__prc', 'u_s4_5_c_nc__ptw', 'u_s4_5_c_inq__com', 'u_s4_5_c_inq__pc', 'u_s4_5_c_inq__sup', 'u_s4_5_c_inq__safelead', 'u_s4_5_c_other', 'status', 'assessment_status'], 'integer'],
            [['report_id'], 'string', 'max'=>64],
            [['s_s4_6', 'u_s4_6'], 'safe'],
            [['report_id'], 'string'],
            [['created_at'], 'string'],
            [['created_at'], 'match', 'pattern' => '#(\d{4}|\*{1})\-{1}(\d{2}|\*){1}\-{1}(\d{2}|\*{1})#'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        //clean report_id
        $joined = [];
        
        
        //{{event_classifications}}
        $query = CpfCrfAssessments::find();
        $query->join('inner join', '{{ca_incident_cat}}', '{{ca_incident_cat}}.[[draft_id]] = {{cpf_crf_assessments}}.[[report_id]]');

        $joined[] = 'ca_incident_cat';
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        //return $dataProvider;

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        //return $dataProvider;
        
        
        $query->andFilterWhere([
            'year' => $this->year,
//            'user_id' => $this->user_id,
//            's_s4_2_a_total' => $this->s_s4_2_a_total,
//            's_s4_2_a_if' => $this->s_s4_2_a_if,
//            's_s4_2_a_ix' => $this->s_s4_2_a_ix,
//            's_s4_2_a_nig' => $this->s_s4_2_a_nig,
//            's_s4_2_a_nio' => $this->s_s4_2_a_nio,
//            's_s4_2_a_haz' => $this->s_s4_2_a_haz,
//            's_s4_2_b_total' => $this->s_s4_2_b_total,
//            's_s4_2_b_bo' => $this->s_s4_2_b_bo,
//            's_s4_2_b_bda' => $this->s_s4_2_b_bda,
//            's_s4_2_b_wbf' => $this->s_s4_2_b_wbf,
//            's_s4_2_c_total' => $this->s_s4_2_c_total,
//            's_s4_2_d_total' => $this->s_s4_2_d_total,
//            's_s4_2_d_si' => $this->s_s4_2_d_si,
//            's_s4_2_d_sb' => $this->s_s4_2_d_sb,
//            's_s4_2_d_sk' => $this->s_s4_2_d_sk,
//            's_s4_2_e_total' => $this->s_s4_2_e_total,
//            's_s4_2_f_total' => $this->s_s4_2_f_total,
//            's_s4_2_g_total' => $this->s_s4_2_g_total,
//            's_s4_2_h_total' => $this->s_s4_2_h_total,
//            's_s4_2_i_total' => $this->s_s4_2_i_total,
//            's_s4_2_j_total' => $this->s_s4_2_j_total,
//            's_s4_3_total' => $this->s_s4_3_total,
//            's_s4_3_fatalities' => $this->s_s4_3_fatalities,
//            's_s4_3_inj' => $this->s_s4_3_inj,
//            's_s4_3_sinj' => $this->s_s4_3_sinj,
//            's_s4_4_nsis' => $this->s_s4_4_nsis,
//            's_s4_4_npcs' => $this->s_s4_4_npcs,
//            's_s4_4_nics' => $this->s_s4_4_nics,
//            's_s4_4_nds' => $this->s_s4_4_nds,
//            's_s4_4_npcrs' => $this->s_s4_4_npcrs,
//            's_s4_4_nps' => $this->s_s4_4_nps,
//            's_s4_4_nsds' => $this->s_s4_4_nsds,
//            's_s4_4_nnavaids' => $this->s_s4_4_nnavaids,
//            's_s4_4_roteq' => $this->s_s4_4_roteq,
//            's_s4_4_eere' => $this->s_s4_4_eere,
//            's_s4_4_ncoms' => $this->s_s4_4_ncoms,
//            's_s4_4_nother' => $this->s_s4_4_nother,
//            's_s4_5_a_total' => $this->s_s4_5_a_total,
//            's_s4_5_a_df' => $this->s_s4_5_a_df,
//            's_s4_5_a_co__int' => $this->s_s4_5_a_co__int,
//            's_s4_5_a_co__ext' => $this->s_s4_5_a_co__ext,
//            's_s4_5_a_mf__f' => $this->s_s4_5_a_mf__f,
//            's_s4_5_a_mf__wo' => $this->s_s4_5_a_mf__wo,
//            's_s4_5_a_mf__dm' => $this->s_s4_5_a_mf__dm,
//            's_s4_5_a_mf__vh' => $this->s_s4_5_a_mf__vh,
//            's_s4_5_a_if' => $this->s_s4_5_a_if,
//            's_s4_5_a_csf' => $this->s_s4_5_a_csf,
//            's_s4_5_a_other' => $this->s_s4_5_a_other,
//            's_s4_5_b_total' => $this->s_s4_5_b_total,
//            's_s4_5_b_err__op' => $this->s_s4_5_b_err__op,
//            's_s4_5_b_err__mnt' => $this->s_s4_5_b_err__mnt,
//            's_s4_5_b_err__tst' => $this->s_s4_5_b_err__tst,
//            's_s4_5_b_err__insp' => $this->s_s4_5_b_err__insp,
//            's_s4_5_b_err__dsgn' => $this->s_s4_5_b_err__dsgn,
//            's_s4_5_b_other' => $this->s_s4_5_b_other,
//            's_s4_5_c_total' => $this->s_s4_5_c_total,
//            's_s4_5_c_inq__rap' => $this->s_s4_5_c_inq__rap,
//            's_s4_5_c_inq__instp' => $this->s_s4_5_c_inq__instp,
//            's_s4_5_c_nc__prc' => $this->s_s4_5_c_nc__prc,
//            's_s4_5_c_nc__ptw' => $this->s_s4_5_c_nc__ptw,
//            's_s4_5_c_inq__com' => $this->s_s4_5_c_inq__com,
//            's_s4_5_c_inq__pc' => $this->s_s4_5_c_inq__pc,
//            's_s4_5_c_inq__sup' => $this->s_s4_5_c_inq__sup,
//            's_s4_5_c_inq__safelead' => $this->s_s4_5_c_inq__safelead,
//            's_s4_5_c_other' => $this->s_s4_5_c_other,
//            'u_s4_2_a_total' => $this->u_s4_2_a_total,
//            'u_s4_2_a_if' => $this->u_s4_2_a_if,
//            'u_s4_2_a_ix' => $this->u_s4_2_a_ix,
//            'u_s4_2_a_nig' => $this->u_s4_2_a_nig,
//            'u_s4_2_a_nio' => $this->u_s4_2_a_nio,
//            'u_s4_2_a_haz' => $this->u_s4_2_a_haz,
//            'u_s4_2_b_total' => $this->u_s4_2_b_total,
//            'u_s4_2_b_bo' => $this->u_s4_2_b_bo,
//            'u_s4_2_b_bda' => $this->u_s4_2_b_bda,
//            'u_s4_2_b_wbf' => $this->u_s4_2_b_wbf,
//            'u_s4_2_c_total' => $this->u_s4_2_c_total,
//            'u_s4_2_d_total' => $this->u_s4_2_d_total,
//            'u_s4_2_d_si' => $this->u_s4_2_d_si,
//            'u_s4_2_d_sb' => $this->u_s4_2_d_sb,
//            'u_s4_2_d_sk' => $this->u_s4_2_d_sk,
//            'u_s4_2_e_total' => $this->u_s4_2_e_total,
//            'u_s4_2_f_total' => $this->u_s4_2_f_total,
//            'u_s4_2_g_total' => $this->u_s4_2_g_total,
//            'u_s4_2_h_total' => $this->u_s4_2_h_total,
//            'u_s4_2_i_total' => $this->u_s4_2_i_total,
//            'u_s4_2_j_total' => $this->u_s4_2_j_total,
//            'u_s4_3_total' => $this->u_s4_3_total,
//            'u_s4_3_fatalities' => $this->u_s4_3_fatalities,
//            'u_s4_3_inj' => $this->u_s4_3_inj,
//            'u_s4_3_sinj' => $this->u_s4_3_sinj,
//            'u_s4_4_nsis' => $this->u_s4_4_nsis,
//            'u_s4_4_npcs' => $this->u_s4_4_npcs,
//            'u_s4_4_nics' => $this->u_s4_4_nics,
//            'u_s4_4_nds' => $this->u_s4_4_nds,
//            'u_s4_4_npcrs' => $this->u_s4_4_npcrs,
//            'u_s4_4_nps' => $this->u_s4_4_nps,
//            'u_s4_4_nsds' => $this->u_s4_4_nsds,
//            'u_s4_4_nnavaids' => $this->u_s4_4_nnavaids,
//            'u_s4_4_roteq' => $this->u_s4_4_roteq,
//            'u_s4_4_eere' => $this->u_s4_4_eere,
//            'u_s4_4_ncoms' => $this->u_s4_4_ncoms,
//            'u_s4_4_nother' => $this->u_s4_4_nother,
//            'u_s4_5_a_total' => $this->u_s4_5_a_total,
//            'u_s4_5_a_df' => $this->u_s4_5_a_df,
//            'u_s4_5_a_co__int' => $this->u_s4_5_a_co__int,
//            'u_s4_5_a_co__ext' => $this->u_s4_5_a_co__ext,
//            'u_s4_5_a_mf__f' => $this->u_s4_5_a_mf__f,
//            'u_s4_5_a_mf__wo' => $this->u_s4_5_a_mf__wo,
//            'u_s4_5_a_mf__dm' => $this->u_s4_5_a_mf__dm,
//            'u_s4_5_a_mf__vh' => $this->u_s4_5_a_mf__vh,
//            'u_s4_5_a_if' => $this->u_s4_5_a_if,
//            'u_s4_5_a_csf' => $this->u_s4_5_a_csf,
//            'u_s4_5_a_other' => $this->u_s4_5_a_other,
//            'u_s4_5_b_total' => $this->u_s4_5_b_total,
//            'u_s4_5_b_err__op' => $this->u_s4_5_b_err__op,
//            'u_s4_5_b_err__mnt' => $this->u_s4_5_b_err__mnt,
//            'u_s4_5_b_err__tst' => $this->u_s4_5_b_err__tst,
//            'u_s4_5_b_err__insp' => $this->u_s4_5_b_err__insp,
//            'u_s4_5_b_err__dsgn' => $this->u_s4_5_b_err__dsgn,
//            'u_s4_5_b_other' => $this->u_s4_5_b_other,
//            'u_s4_5_c_total' => $this->u_s4_5_c_total,
//            'u_s4_5_c_inq__rap' => $this->u_s4_5_c_inq__rap,
//            'u_s4_5_c_inq__instp' => $this->u_s4_5_c_inq__instp,
//            'u_s4_5_c_nc__prc' => $this->u_s4_5_c_nc__prc,
//            'u_s4_5_c_nc__ptw' => $this->u_s4_5_c_nc__ptw,
//            'u_s4_5_c_inq__com' => $this->u_s4_5_c_inq__com,
//            'u_s4_5_c_inq__pc' => $this->u_s4_5_c_inq__pc,
//            'u_s4_5_c_inq__sup' => $this->u_s4_5_c_inq__sup,
//            'u_s4_5_c_inq__safelead' => $this->u_s4_5_c_inq__safelead,
//            'u_s4_5_c_other' => $this->u_s4_5_c_other,
//            'status' => $this->status,
            'assessment_status' => $this->assessment_status,
//            'created_at' => $this->created_at,
//            'created_by' => $this->created_by,
//            'modified_at' => $this->modified_at,
//            'modified_by' => $this->modified_by,
        ]);

        
        if (isset($this->report_id) && trim($this->report_id)!=='') {
            //$query->join('inner join', '{{ca_incident_cat}}', '{{ca_incident_cat}}.[[id]] = {{cpf_crf_assessments}}.[[report_id]]');
            $query->join('inner join', '{{event_classifications}}', '{{event_classifications}}.[[id]] = {{cpf_crf_assessments}}.[[report_id]]');
            $query->join('inner join', '{{event_declaration}}', '{{event_declaration}}.[[id]] = {{event_classifications}}.[[event_id]]');
            $query->join('inner join', '{{installations}}', 
                    '{{event_declaration}}.[[installation_id]] = {{installations}}.[[id]]');

            $query->andFilterWhere([
                    'or',
                    ['like', '{{event_declaration}}.[[operator]]', $this->report_id],
                    ['like', '{{event_declaration}}.[[field_name]]', $this->report_id],
                    ['like', '{{installations}}.[[name]]', $this->report_id],
                    //['like', 'YEAR({{event_declaration}}.[[event_date_time]])', $this->report_id]
            ]);
            
            //check if its integer so the dates are searched for
//            if (is_numeric($this->report_id)) {
//                $num = intval($this->report_id);
//                $query->orFilterWhere([
//                    'or',
//                    ['=', 'MONTH({{event_declaration}}.[[event_date_time]])', $num],
//                    ['=', 'DAY({{event_declaration}}.[[event_date_time]])', $num],
//                ]);
//            }
            
        }
    
        $this->search_time($query);
        
        return $dataProvider;
    }
    
    private function search_time($query) {
        if (isset($this->created_at))
        {
            /*
            $query->andFilterWhere(['and',
                'event_date_time BETWEEN "' . $this->event_date_time . '" AND "' . $this->event_date_time . ' 23:59:59"'
                ]);
             * 
             */
            
            /* '#(\d{4}|\*{1})\-{1}(\d{2}|\*){1}\-{1}(\d{2}|\*{1})#' */
            
            /* case Y-m-d */
            if (preg_match_all('#(\d{4})\-{1}(\d{2}){1}\-{1}(\d{2})#', $this->created_at))
            {
                /* 
                 * returns all the events in a given date 
                 * year month day set
                 */
                $query->andFilterWhere(['and',
                    'FROM_UNIXTIME(created_at) BETWEEN "' . $this->created_at . '" AND "' . $this->created_at . ' 23:59:59"'
                    ]);
            }
            else if (preg_match_all('#(\d{4})\-{1}(\d{2}){1}\-{1}(\*{1})#', $this->created_at))
            {
                /* 
                 * returns all the events in a given year and month (day irrespective) 
                 * year month set
                 */
                
                $parts = explode('-', $this->created_at);
                
                $query->andFilterWhere(['and',
                    'year(FROM_UNIXTIME(created_at)) = ' . $parts[0],    
                    'month(FROM_UNIXTIME(created_at)) = ' . $parts[1]
                    ]);
            }
            elseif ((preg_match_all('#(\d{4})\-{1}(\*{1})\-{1}(\*{1})#', $this->created_at)))
            {
                /* 
                 * returns all the events in a given year (month and day irrespective) 
                 * year set
                 */
                
                $parts = explode('-', $this->created_at);
                
                $query->andFilterWhere(['and',
                    'year(FROM_UNIXTIME(created_at)) = ' . $parts[0],    
                    ]);
            }
            elseif ((preg_match_all('#(\*{1})\-{1}(\d{2}){1}\-{1}(\*{1})#', $this->created_at)))
            {
                /* 
                 * returns all the events in a given month (year and day irrespective) 
                 * year set
                 */
                
                $parts = explode('-', $this->created_at);
                
                $query->andFilterWhere(['and',
                    'month(FROM_UNIXTIME(created_at)) = ' . $parts[1],    
                    ]);
            }
            elseif ((preg_match_all('#(\*{1})\-{1}(\*){1}\-{1}(\d{2})#', $this->created_at)))
            {
                /* 
                 * returns all the events in a given day (year and month irrespective) 
                 * year set
                 */
                
                $parts = explode('-', $this->created_at);
                
                $query->andFilterWhere(['and',
                    'day(FROM_UNIXTIME(created_at)) = ' . $parts[2],    
                    ]);
            }
            elseif ((preg_match_all('#(\*{1})\-{1}(\d{2}){1}\-{1}(\d{2})#', $this->created_at)))
            {
                /* 
                 * returns all the events in a month on a particular day (year irrespective) 
                 * month day set
                 */
                $parts = explode('-', $this->created_at);
                
                $query->andFilterWhere(['and',
                    'month(FROM_UNIXTIME(created_at)) = ' . $parts[1],
                    'day(FROM_UNIXTIME(created_at)) = ' . $parts[2]    
                    ]);
            }
            elseif ((preg_match('#(\*{1})\-{1}(\*{1})\-{1}(\*{1})#', $this->created_at)))
            {
                //echo 'select all';
                //die();
            }
            else if (preg_match_all('#(\d{4})\-{1}(\*{1}){1}\-{1}(\d{2})#', $this->created_at))
            {
                /* 
                 * returns all the events in a given year and day (month irrespective) 
                 * year day set
                 */
                
                $parts = explode('-', $this->created_at);
                
                $query->andFilterWhere(['and',
                    'year(FROM_UNIXTIME(created_at)) = ' . $parts[0],    
                    'day(FROM_UNIXTIME(created_at)) = ' . $parts[2]
                    ]);
            }
            
            
        }
        
    }
    
}
