<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\modules\cpfbuilder\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\cpfbuilder\models\CpfSection2;

/**
 * CpfSection2Search represents the model behind the search form about `app\modules\cpfbuilder\models\CpfSection2`.
 */
class CpfSection2Search extends CpfSection2
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['year', 'user_id', 'norm_oil_u', 'norm_oil_n', 'norm_gas_u', 'norm_gas_n'], 'integer'],
            [['norm_ktoe', 'norm_oil_q', 'norm_gas_q'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CpfSection2::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'year' => $this->year,
            'user_id' => $this->user_id,
            'norm_ktoe' => $this->norm_ktoe,
            'norm_oil_q' => $this->norm_oil_q,
            'norm_oil_u' => $this->norm_oil_u,
            'norm_oil_n' => $this->norm_oil_n,
            'norm_gas_q' => $this->norm_gas_q,
            'norm_gas_u' => $this->norm_gas_u,
            'norm_gas_n' => $this->norm_gas_n,
        ]);

        return $dataProvider;
    }
}
