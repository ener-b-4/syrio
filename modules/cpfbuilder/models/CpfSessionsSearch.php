<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\modules\cpfbuilder\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\cpfbuilder\models\CpfSessions;

/**
 * CpfSessionsSearch represents the model behind the search form about `app\modules\cpfbuilder\models\CpfSessions`.
 */
class CpfSessionsSearch extends CpfSessions
{
    
    public $synced;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['year', 'user_id', 'synced'], 'integer'],
            [['synced'], 'in', 'range'=>[0,1]]
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CpfSessions::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'year' => $this->year,
            'user_id' => $this->user_id,
        ]);

        if (isset($this->synced) && trim($this->synced)!=='') {
            if ($this->synced==1) {
                $query->andWhere([
                    'or',
                    ['refresh_installations'=>0],
                    ['refresh_data'=>0]
                ]);
            } else {
                $query->andWhere([
                    'or',
                    ['refresh_installations'=>1],
                    ['refresh_data'=>1]
                ]);
            }
        }
        
        return $dataProvider;
    }
}
