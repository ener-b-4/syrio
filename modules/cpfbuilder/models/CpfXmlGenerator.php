<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\modules\cpfbuilder\models;

use app\modules\cpfbuilder\models\CpfSection1;

/**
 * Description of CpfXmlGenerator
 *
 * @author vamanbo
 */
class CpfXmlGenerator {
    
    
    public static function ConvertToXml($cpf)
    {
        /* @var $cpf CpfSessions */
        
        $xml = new \DOMDocument('1.0', 'utf-8');
        $xml->formatOutput = true;

        $section1 = $cpf->cpfSection1;
        $section2 = $cpf->cpfSection2;
        $section3 = $cpf->cpfSection3;
        $section4 = $cpf->cpfSection4;
        
        $cpf = $xml->createElementNS('http://ospis.ec.europa.eu/schemas/1.0', 'common_publication_format');
        $xml->appendChild($cpf);
        
        self::ConvertSection1($xml, $section1, $cpf);
        self::ConvertSection2($xml, $section2, $cpf);
        self::ConvertSection3($xml, $section3, $cpf);
        self::ConvertSection4($xml, $section4, $cpf);
        
        return $xml->saveXML();
    }
    
    
    /**
     * This returns the xml representation of a CpfSection1
     * 
     * 
     * @param app\modules\cpfbuilder\models\CpfSection1 $section
     * 
     * @return the xml DOMDocument element (node)
     * 
     * @author vamanbo
     * @version 1.0.20150910
     */
    public static function ConvertSection1(&$xml, $section, &$cpf = null)
    {
        /* @var $section \app\modules\cpfbuilder\models\CpfSection1 */
        
        //$xml = new \DOMDocument('1.0', 'utf-8');
        //$xml->formatOutput = true;
        
        
        $section1 = $xml->createElement('section1');
        $ms = $xml->createElement('ms', $section->ms);
        $period = $xml->createElement('period', $section->year);
        $competent_authority = $xml->createElement('ca', $section->ca);
        $designated = $xml->createElement('dra', $section->dra);
        
        $contact = $xml->createElement('contact');
        $contact_tel = $xml->createElement('tel', $section->phone);
        $contact_email = $xml->createElement('email', $section->email);
        
        $contact->appendChild($contact_tel);
        $contact->appendChild($contact_email);
        
        $section1->appendChild($ms);
        $section1->appendChild($period);
        $section1->appendChild($competent_authority);
        $section1->appendChild($designated);
        $section1->appendChild($contact);
        
        isset($cpf) ? $cpf->appendChild($section1) : $xml->appendChild($section1);
        
        return $xml;
    }
    
    
    /**
     * 
     * @param \DOMDocument $xml
     * @param \app\modules\cpfbuilder\models\CpfSection2 $section
     */
    public static function ConvertSection2(&$xml, $section, &$cpf = null)
    {
        /* @var $section \app\modules\cpfbuilder\models\CpfSection2 */

        //$xml = new \DOMDocument('1.0', 'utf-8');
        //$xml->formatOutput = true;
                
        //$fixed_operational = $section->s_2_1_installations;
        //$fixed_2015 = $section->s_2_2_a_installations;
        //$fixed_decom = $section->s_2_2_b_installations;
        
        //$modus = $section->s_2_3_installations;
        
        //echo '<pre>';
        //echo var_dump($fixed_operational);
        //echo '<br>/<h3>fixed new</h3>';
        //echo var_dump($fixed_2015);
        //echo '<br>/<h3>decom 2015</h3>';
        //echo var_dump($fixed_decom);
        //echo '<br>/<h3>modu</h3>';
        //echo var_dump($modus);
        
        //echo '</pre>';
        //die();

        
        $section2 = $xml->createElement('section2');
        
        $fixed_installations = $xml->createElement('fixed_installations');
        $operational = $xml->createElement('operational');
        foreach($section->fixedInstallations as $inst)
        {
            /* @var $inst \app\modules\cpfbuilder\models\CpfInstallations */
            $fixed_inst = $xml->createElement('fixed_inst');
            $fixed_inst->appendChild($xml->createElement('ident', $inst->ident));
            $fixed_inst->appendChild($xml->createElement('type', $inst->type));
            
            $fixed_inst->appendChild($xml->createElement('y_inst', $inst->installation_year));
            
            $tof = CpfTypeOfFluid::find($inst->tof)->one();
            $fixed_inst->appendChild($xml->createElement('tof', $tof->name));
            $fixed_inst->appendChild($xml->createElement('n_beds', $inst->n_beds));
            
            $coords = $xml->createElement('coordinates');
            $coords->setAttribute('lat', $inst->lat);
            $coords->setAttribute('lon', $inst->lon);
            $fixed_inst->appendChild($coords);
            
            $operational->appendChild($fixed_inst);
        }

        $decomNode = $xml->createElement('decommisioned');
        foreach($section->s_2_2_b_installations as $inst)
        {
            /* @var $inst \app\modules\cpfbuilder\models\CpfInstallations */
            $decom_item = $xml->createElement('decom_inst');
            $decom_item->setAttribute('ident', $inst->ident);
            $decom_item->setAttribute('decom_type', $inst->decom_type == 1 ? 'Temporary' : 'Permanent');

            $decomNode->appendChild($decom_item);
        }
        
        /* complete fixed installations */
        $fixed_installations->appendChild($operational);
        $fixed_installations->appendChild($decomNode);
        
        /*mobile installations node */
        $mobile_installations = $xml->createElement('mobile_installations');
        foreach($section->s_2_3_installations as $inst)
        {
            /* @var $inst \app\modules\cpfbuilder\models\CpfInstallations */
            $mobile_inst = $xml->createElement('mo_inst');
            $mobile_inst->appendChild($xml->createElement('ident', $inst->ident));
            $mobile_inst->appendChild($xml->createElement('type', $inst->type));
            $mobile_inst->appendChild($xml->createElement('y_const', $inst->installation_year));
            $mobile_inst->appendChild($xml->createElement('n_beds', $inst->n_beds));
            
            $op_areas_node = $xml->createElement('op_areas');
            foreach($inst->cpfInstOpAreas as $op_area)
            {
                $area_node = $xml->createElement('op_area');
                $area_node->setAttribute('name', $op_area->name);
                $area_node->setAttribute('duration', $op_area->duration);
                
                $op_areas_node->appendChild($area_node);
            }
            
            $mobile_inst->appendChild($op_areas_node);
            /* augment mobile installations */
            $mobile_installations->appendChild($mobile_inst);        
        }
        
        $gas_unit = \app\models\units\Units::findOne([
            'scope'=> \app\models\units\Units::UNIT_OF_ENERGY_GO,
            'id' => $section->norm_gas_u])->unit;

        $oil_unit = \app\models\units\Units::findOne([
            'scope'=> \app\models\units\Units::UNIT_OF_ENERGY_GO,
            'id' => $section->norm_oil_u])->unit;
        
        $data_norm = $xml->createElement('data_norm');
        $data_norm->appendChild($xml->createElement('working_hours', $section->working_hours));
        $prod_node = $xml->createElement('production');
        $prod_node->appendChild($xml->createElement('ktoe_total', $section->norm_ktoe));

        
        //norm
        $norm=  \app\models\units\Units::find()
                ->where(['scope'=>  \app\models\units\Units::UNIT_OF_ENERGY_GO])
                ->andWhere(['c_factor'=>1])->one();
        
        $oil_node=$xml->createElement('oil');        
        $oil_node->setAttribute('q', \app\models\units\Units::Normalize($section->norm_oil_q, \app\models\units\Units::UNIT_OF_ENERGY_GO));
        $oil_node->setAttribute('u', $norm->unit);
        
        $gas_node=$xml->createElement('gas');
        $gas_node->setAttribute('q', \app\models\units\Units::Normalize($section->norm_gas_q, \app\models\units\Units::UNIT_OF_ENERGY_GO));
        $gas_node->setAttribute('u', $norm->unit);
        
        $prod_node->appendChild($oil_node);
        $prod_node->appendChild($gas_node);
        $data_norm->appendChild($prod_node);
        
        $section2->appendChild($fixed_installations);
        $section2->appendChild($mobile_installations);
        $section2->appendChild($data_norm);
        
        //$xml->appendChild($section2);
        
        isset($cpf) ? $cpf->appendChild($section2) : $xml->appendChild($section2);
    }

    
    /**
     * This returns the xml representation of a CpfSection1
     * 
     * 
     * @param app\modules\cpfbuilder\models\CpfSection1 $section
     * @param \DOMDocument $xml by ref
     * 
     * @return the xml DOMDocument element (node)
     * 
     * @author vamanbo
     * @version 1.0.20150910
     */
    public static function ConvertSection3(&$xml, $section, &$cpf = null)
    {
        /* @var $section \app\modules\cpfbuilder\models\CpfSection3 */
        /* @var $s3_3 \DomElement */
        /* @var $s3_4 \DomElement */
        
        //$xml = new \DOMDocument('1.0', 'utf-8');
        //$xml->formatOutput = true;
        
        
        $section3 = $xml->createElement('section3');
        
        $s3_1 = $xml->createElement('s3_1');
        $s3_1->setAttribute('n_inspected', $section->n_inpeceted);
        $s3_1->setAttribute('man_days', $section->man_days);
        $s3_1->setAttribute('n_offshore_inspections', $section->n_offshore_inspections);
        
        $s3_2 = $xml->createElement('s3_2');
        $s3_2->setAttribute('n_env_concerns', $section->n_env_concerns);
        $s3_2->setAttribute('n_major_accidents', $section->n_major_accidents);
        
        $s3_3 = $xml->createElement('s3_3');
        $s3_cdata = $xml->createCDATASection($section->s3_3_text);
        $s3_3->appendChild($s3_cdata);
        
        $s3_4 = $xml->createElement('s3_4');
        $s3_4_desc = $xml->createElement('description');
        $s3_4_rat = $xml->createElement('rationale');
        $s3_4_exp_out = $xml->createElement('expected_outcome');
        $s3_4_desc_cdata = $xml->createCDATASection($section->description);
        $s3_4_rat_cdata = $xml->createCDATASection($section->rationale);
        $s3_4_exp_out_cdata = $xml->createCDATASection($section->expected_outcome);
        
        $s3_4_desc->appendChild($s3_4_desc_cdata);
        $s3_4_rat->appendChild($s3_4_rat_cdata);
        $s3_4_exp_out->appendChild($s3_4_exp_out_cdata);
        
        $s3_4->appendChild($s3_4_desc);
        $s3_4->appendChild($s3_4_rat);
        $s3_4->appendChild($s3_4_exp_out);
        
        //$s3_4->appendChild($xml->createElement('description', $section->description));
        //$s3_4->appendChild($xml->createElement('rationale', $section->rationale));
        //$s3_4->appendChild($xml->createElement('expected_outcome', $section->expected_outcome));
        
        $s3_4_ref = $xml->createElement('references');
        
        foreach($section->references as $reference)
        {
            $ref = $xml->createElement('ref');
            $ref->setAttribute('name', $reference->name);
            $ref->setAttribute('url', $reference->url);
            $s3_4_ref->appendChild($ref);
        }
        $s3_4->appendChild($s3_4_ref);
        
        
        $section3->appendChild($s3_1);
        $section3->appendChild($s3_2);
        $section3->appendChild($s3_3);
        $section3->appendChild($s3_4);
        
        //$xml->appendChild($section3);

        isset($cpf) ? $cpf->appendChild($section3) : $xml->appendChild($section3);
        
        return $xml;
    }    
    
    
    /**
     * This returns the xml representation of a CpfSection1
     * 
     * 
     * @param app\modules\cpfbuilder\models\CpfSection4 $section
     * @param \DOMDocument $xml by ref
     * 
     * @return the xml DOMDocument element (node)
     * 
     * @author vamanbo
     * @version 1.0.20150910
     */
    public static function ConvertSection4(&$xml, $section, &$cpf = null)
    {
        /* @var $section \app\modules\cpfbuilder\models\CpfSection4 */
        /* @var $s4_3 \DomElement */
        /* @var $s4_4 \DomElement */
        
        //$xml = new \DOMDocument('1.0', 'utf-8');
        //$xml->formatOutput = true;
        
        
        $section4 = $xml->createElement('section4');

        $s4_1 = $xml->createElement('s4_1');
        $s4_1->setAttribute('n_major_accidents', $section->s4_1_n_major_accidents);
        $s4_1->setAttribute('n_events_total', $section->s4_1_n_events_total);
        $section4->appendChild($s4_1);
        
        $s4_2 = $xml->createElement('s4_2');
        foreach(range('a','j') as $subsection)
        {
            $ssnode = $xml->createElement('s4_2_' . $subsection);
            $attrs = CpfCore::cpf_core_attr_s4_x('2_' . $subsection);
            foreach($attrs as $attr)
            {
                $xmlattr = str_replace('s4_1_', '', $attr);
                $ssnode->setAttribute($xmlattr, $section->$attr);
            }
            $s4_2->appendChild($ssnode);
        }
        $section4->appendChild($s4_2);
        
        
        foreach(range(3,4) as $subsection)
        {
            $exclude = [
                's4_3_total'    => '',
                's4_4_roteq'    => 's4_4_nroteq',
                's4_4_eere'     => 's4_4_neere',
            ];
            $ssnode = $xml->createElement('s4_' . $subsection);
            $attrs = CpfCore::cpf_core_attr_s4_x($subsection);
            foreach($attrs as $attr)
            {
                $xmlattr = str_replace('__', '-', $attr);
                if (key_exists($attr, $exclude))
                {
                    if ($exclude[$attr] !== '')
                    {
                        $ssnode->setAttribute($exclude[$attr], $section->$attr);
                    }
                }
                else
                {
                    $ssnode->setAttribute($xmlattr, $section->$attr);
                }
            }
            $section4->appendChild($ssnode);
        }


        
        $s4_5 = $xml->createElement('s4_5');
        foreach(range('a','d') as $subsection)
        {
            $ssnode = $xml->createElement('s4_5_' . $subsection);
            $attrs = CpfCore::cpf_core_attr_s4_x('5_' . $subsection);
            foreach($attrs as $attr)
            {
                $xmlattr = str_replace('__', '-', $attr);
                $ssnode->setAttribute($xmlattr, $section->$attr);
            }
            $s4_5->appendChild($ssnode);
        }
        $section4->appendChild($s4_5);
        
        $s4_6=$xml->createElement('s4_6');
        $s4_6_cdata = $xml->createCDATASection($section->s4_6);
        $s4_6->appendChild($s4_6_cdata);

        $section4->appendChild($s4_6);
        
        //$section4->appendChild($s4_1);
        //$xml->appendChild($section4);
        isset($cpf) ? $cpf->appendChild($section4) : $xml->appendChild($section4);
        
        return $xml;
    }    
        
    
    
}
