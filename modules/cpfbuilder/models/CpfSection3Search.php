<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\modules\cpfbuilder\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\cpfbuilder\models\CpfSection3;

/**
 * CpfSection3Search represents the model behind the search form about `app\modules\cpfbuilder\models\CpfSection3`.
 */
class CpfSection3Search extends CpfSection3
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['year', 'user_id', 'n_offshore_inspections', 'n_inpeceted', 'n_major_accidents', 'n_env_concerns'], 'integer'],
            [['man_days'], 'number'],
            [['s3_3_text', 'description', 'rationale', 'expected_outcome'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CpfSection3::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'year' => $this->year,
            'user_id' => $this->user_id,
            'n_offshore_inspections' => $this->n_offshore_inspections,
            'man_days' => $this->man_days,
            'n_inpeceted' => $this->n_inpeceted,
            'n_major_accidents' => $this->n_major_accidents,
            'n_env_concerns' => $this->n_env_concerns,
        ]);

        $query->andFilterWhere(['like', 's3_3_text', $this->s3_3_text])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'rationale', $this->rationale])
            ->andFilterWhere(['like', 'expected_outcome', $this->expected_outcome]);

        return $dataProvider;
    }
}
