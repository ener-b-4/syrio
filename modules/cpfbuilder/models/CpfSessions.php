<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\modules\cpfbuilder\models;

use Yii;

/**
 * This is the model class for table "cpf_sessions".
 *
 * @property integer $year
 * @property integer $user_id
 * @property integer $refresh_installations
 * @property integer $refresh_data
 *
 * @property CpfReferences[] $cpfReferences
 * @property CpfSection1 $cpfSection1
 * @property CpfSection2 $cpfSection2
 * @property CpfSection3 $cpfSection3
 * @property CpfSection4 $cpfSection4
 */
class CpfSessions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cpf_sessions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['year', 'user_id'], 'required'],
            [['year', 'user_id', 'refresh_installations', 'refresh_data'], 'integer'],
            [['refresh_installations', 'refresh_data'], 'in', 'range'=>[0,1]],
            [['refresh_installations', 'refresh_data'], 'safe'],
            ['year', 'unique', 'targetAttribute' => ['year'], 'message'=>\Yii::t('app/cpf', 'You already have a report registered for this year.')]            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'year' => Yii::t('app/cpf', 'Year'),
            'user_id' => 'User ID',
            'refresh_installations' => Yii::t('app/cpf', 'Refresh installations'),
            'refresh_data' => Yii::t('app/cpf', 'Refresh Section 4 data'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCpfReferences()
    {
        $query = CpfReferences::find()
                ->where(['year' => $this->year, 'user_id'=>$this->user_id])->all();
        
        echo print_r($query);
        
        return $query;
        //return $this->hasMany(CpfReferences::className(), ['year' => 'year', 'user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCpfSection1()
    {
        return $this->hasOne(CpfSection1::className(), ['year' => 'year', 'user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCpfSection2()
    {
        return $this->hasOne(CpfSection2::className(), ['year' => 'year', 'user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCpfSection3()
    {
        return $this->hasOne(CpfSection3::className(), ['year' => 'year', 'user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCpfSection4()
    {
        return $this->hasOne(CpfSection4::className(), ['year' => 'year', 'user_id' => 'user_id']);
    }
    
    /**
     * returns the Cpf Crf Reports (year, user) that are finalized
     * 
     * @return CpfCrfAssessments[]
     */
    public function getAssessedReports()
    {
        $query = CpfCrfAssessments::find()
                ->join('join', '{{ca_incident_cat}}', '{{ca_incident_cat}}.[[draft_id]] = {{cpf_crf_assessments}}.[[report_id]]')
                ->where(['year'=>$this->year])
                ->andWhere(['status'=>10])
                ->andWhere(['assessment_status'=>  CpfCrfAssessments::ASSESS_STATUS_FINALIZED])->all();
        return $query;
    }
    
    public function getMajorAccidentsReports()
    {
        $query = CpfCrfAssessments::find()
                ->where(['year'=>$this->year])
                ->andWhere(['status'=>10])
                ->andWhere(['assessment_status'=>  CpfCrfAssessments::ASSESS_STATUS_FINALIZED])
                
                ->join('inner join', '{{ca_incident_cat}}', '[[report_id]] = [[draft_id]]')
                
                ->andWhere(['or',
                    ['{{ca_incident_cat}}.[[is_major_a]]' => 1],
                    ['{{ca_incident_cat}}.[[is_major_b]]' => 1],
                    ['{{ca_incident_cat}}.[[is_major_c]]' => 1],
                    ['{{ca_incident_cat}}.[[is_major_d]]' => 1],
                    ['{{ca_incident_cat}}.[[is_major_e]]' => 1],
                    ['{{ca_incident_cat}}.[[is_major_f]]' => 1],
                    ['{{ca_incident_cat}}.[[is_major_g]]' => 1],
                    ['{{ca_incident_cat}}.[[is_major_h]]' => 1],
                    ['{{ca_incident_cat}}.[[is_major_i]]' => 1],
                    ['{{ca_incident_cat}}.[[is_major_j]]' => 1],
                    
                    ])
                ->all();
        return $query;
    }    
}
