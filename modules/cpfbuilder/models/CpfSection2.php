<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\modules\cpfbuilder\models;

use Yii;
use \app\models\units\Units;

/**
 * This is the model class for table "cpf_section2".
 *
 * @property integer $year
 * @property integer $user_id
 * @property double $norm_ktoe
 * @property double $norm_oil_q
 * @property integer $norm_oil_u
 * @property integer $norm_oil_n
 * @property double $norm_gas_q
 * @property integer $norm_gas_u
 * @property integer $norm_gas_n
 * @property integer $working_hours
 * 
 * @property \yii\db\ActiveQuery $fixedInstallations installations INCLUDING new and decommissioned in current year.
 * @property \yii\db\ActiveQuery $s_2_1_installations installations in section 2.1
 * @property \yii\db\ActiveQuery $s_2_2_a_installations installations in section 2.2.a (new)
 * @property \yii\db\ActiveQuery $s_2_2_b_installations installations in section 2.2.b (decommissioned)
 * @property \yii\db\ActiveQuery $s_2_3_installations installations in section 2.3 (mobile)
 *
 * @property CpfInstallations[] $cpfInstallations
 */
class CpfSection2 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cpf_section2';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['year', 'user_id'], 'required'],
            [['year', 'user_id', 'norm_oil_u', 'norm_gas_u'], 'integer', 'min'=>0],
            [['norm_oil_n', 'norm_gas_n'], 'number'],
            
            /* for current version of db */
            //[['norm_ktoe', 'working_hours'], 'number', 'min'=>0],
            //[[ 'norm_oil_q', 'norm_gas_q',], 'integer', 'min'=>0],
            /* comment the two lines above and uncomment the next one as soon as the db is aligned */
            [['norm_ktoe', 'norm_oil_q', 'norm_gas_q', 'working_hours'], 'number', 'min'=>0],

            
            [['working_hours'], 'number', 'min'=>0],
            
            [['working_hours'], 'required', 'on'=>'xml', 'message'=>\Yii::t('app/cpf', 'You must provide a value for {evt} ({sec})',
                    ['evt'=>\Yii::t('app/cpf', 'Total number of actual offshore working hours for all installations'), 'sec'=>\Yii::t('app/crf', 'Section {evt}', ['evt'=>' 2.4(a)'])])],
            [['norm_ktoe'], 'required', 'on'=>'xml', 'message'=>\Yii::t('app/cpf', 'You must provide a value for {evt} ({sec})',
                    ['evt'=>\Yii::t('app/cpf', 'Total production, in kTOE'), 'sec'=>\Yii::t('app/crf', 'Section {evt}', ['evt'=>' 2.4(b)'])])],
            [['norm_oil_q'], 'required', 'on'=>'xml', 'message'=>\Yii::t('app/cpf', 'You must provide a value for {evt} ({sec})',
                    ['evt'=>\Yii::t('app/cpf', 'Oil production'), 'sec'=>\Yii::t('app/crf', 'Section {evt}', ['evt'=>' 2.4(b)'])])],
            [['norm_oil_u'], 'required', 'on'=>'xml', 'message'=>\Yii::t('app/cpf', 'You must provide a value for {evt} ({sec})',
                    ['evt'=>\Yii::t('app/cpf', 'Oil production unit'), 'sec'=>\Yii::t('app/crf', 'Section {evt}', ['evt'=>' 2.4(b)'])])],
            [['norm_gas_q'], 'required', 'on'=>'xml', 'message'=>\Yii::t('app/cpf', 'You must provide a value for {evt} ({sec})',
                    ['evt'=>\Yii::t('app/cpf', 'Gas production unit'), 'sec'=>\Yii::t('app/crf', 'Section {evt}', ['evt'=>' 2.4(b)'])])],
            [['norm_gas_u'], 'required', 'on'=>'xml', 'message'=>\Yii::t('app/cpf', 'You must provide a value for {evt} ({sec})',
                    ['evt'=>\Yii::t('app/cpf', 'Gas production unit'), 'sec'=>\Yii::t('app/crf', 'Section {evt}', ['evt'=>' 2.4(b)'])])],
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'year' => Yii::t('app/cpf', 'Year'),
            'user_id' => 'User ID',
            'norm_ktoe' => Yii::t('app/cpf', 'Total production, in kTOE'),
            'norm_oil_q' => Yii::t('app/cpf', 'Oil production'),
            'norm_oil_u' => 'Norm Oil U',
            'norm_oil_n' => Yii::t('app/cpf', 'Oil production (norm)'),
            'norm_gas_q' => Yii::t('app/cpf', 'Gas production'),
            'norm_gas_u' => 'Norm Gas U',
            'norm_gas_n' => Yii::t('app/cpf', 'Gas production (norm)'),
        ];
    }

    public function beforeSave($insert) {
        
        if (parent::beforeSave($insert))
        {
            /* 20150920_01 */
            $this->norm_gas_n = isset($this->norm_gas_q) && isset($this->norm_gas_u) ? 
                    Units::Normalize($this->norm_gas_q, $this->norm_gas_u) : 0;
            
            $this->norm_oil_n = isset($this->norm_oil_q) && isset($this->norm_oil_u) ? 
                    Units::Normalize($this->norm_oil_q, $this->norm_oil_u) : 0;
            /* end - 20150920_01 */
            
            
            $this->norm_ktoe = ($this->norm_gas_n + $this->norm_oil_n)/1000;
            
            //echo 'oil: ' . $this->norm_oil_n . '<br/>';
            //echo 'gas: ' . $this->norm_gas_n . '<br/>';
            //echo 'sum: ' . $this->norm_ktoe;
            //die();
            return true;
        }
        else
        {
            return false;
        }
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCpfInstallations()
    {
        return $this->hasMany(CpfInstallations::className(), ['year' => 'year', 'user_id' => 'user_id']);
    }
    
    /**
     * Fixed installations operational at January 1st INCLUDING NEW 
     * 
     * @return \yii\db\ActiveQuery
     */
    public function getFixedInstallations()
    {
        /* @var $query \yii\db\ActiveQuery */
        
        /*
         * imi trebuie cele care:
         * - al carui tip este fix              => installation_type.mobile = 0
         * - anul de instalare NU este 2015     => installation_year != 2015
         */
        $query = \app\modules\cpfbuilder\models\CpfInstallations::find()
                 ->where(['year' => $this->year, 'user_id' => $this->user_id]);
        
        $query->join('inner join', '{{installation_types}}', '{{cpf_installations}}.[[type]] = {{installation_types}}.[[type]]');

        $query->andWhere(['{{installation_types}}.[[mobile]]' => 0]);
        
        return $query->all();
    }    
    
    
    /**
     * Fixed installations operational at January 1st AND NOT 
     * 
     * @return \yii\db\ActiveQuery
     */
    public function getS_2_1_installations()
    {
        /* @var $query \yii\db\ActiveQuery */
        
        /*
         * imi trebuie cele care:
         * - al carui tip este fix              => installation_type.mobile = 0
         * - anul de instalare NU este 2015     => installation_year != 2015
         * - NU sunt decomisionate              => decom_type == null
         */
        $query = \app\modules\cpfbuilder\models\CpfInstallations::find()
                 ->where(['year' => $this->year, 'user_id' => $this->user_id]);
        
        $query->join('inner join', '{{installation_types}}', '{{cpf_installations}}.[[type]] = {{installation_types}}.[[type]]');

        $query->andWhere(['{{installation_types}}.[[mobile]]' => 0])
                ->andWhere(['not', ['[[installation_year]]'=>$this->year]])
                ->andWhere(['decom_type' => null]);
        
        return $query->all();
    }

    /**
     * Fixed NEW installations
     * 
     * @return \yii\db\ActiveQuery
     */
    public function getS_2_2_a_installations()
    {
        /* @var $query \yii\db\ActiveQuery */
        
        /*
         * imi trebuie cele care:
         * - al carui tip este fix              => installation_type.mobile = 0
         * - anul de instalare ESTE este 2015     => installation_year != 2015
         * - NU sunt decomisionate              => decom_type == null
         */
        $query = \app\modules\cpfbuilder\models\CpfInstallations::find()
                 ->where(['year' => $this->year, 'user_id' => $this->user_id]);
        
        $query->join('inner join', '{{installation_types}}', '{{cpf_installations}}.[[type]] = {{installation_types}}.[[type]]');

        $query->andWhere(['{{installation_types}}.[[mobile]]' => 0])
                ->andWhere(['[[installation_year]]'=>$this->year])
                ->andWhere(['decom_type' => null]);
        
        return $query->all();
    }
    
    /**
     * Decommissioned installations
     * 
     * @return \yii\db\ActiveQuery
     */
    public function getS_2_2_b_installations()
    {
        /* @var $query \yii\db\ActiveQuery */
        
        /*
         * imi trebuie cele care:
         * - al carui tip este fix              => installation_type.mobile = 0
         * - Sunt decomisionate              => decom_type == null
         */
        $query = \app\modules\cpfbuilder\models\CpfInstallations::find()
                 ->where(['year' => $this->year, 'user_id' => $this->user_id]);
        
        $query->join('inner join', '{{installation_types}}', '{{cpf_installations}}.[[type]] = {{installation_types}}.[[type]]');

        $query->andWhere(['{{installation_types}}.[[mobile]]' => 0])
                ->andWhere(['not', ['decom_type' => null]]);
        
        return $query->all();
    }    
    
    /**
     * MOBILE installations
     * 
     * @return \yii\db\ActiveQuery
     */
    public function getS_2_3_installations()
    {
        /* @var $query \yii\db\ActiveQuery */
        
        /*
         * imi trebuie cele care:
         * - al carui tip este mobil         => installation_type.mobile = 1
         */
        $query = \app\modules\cpfbuilder\models\CpfInstallations::find()
                 ->where(['year' => $this->year, 'user_id' => $this->user_id]);
        
        $query->join('inner join', '{{installation_types}}', '{{cpf_installations}}.[[type]] = {{installation_types}}.[[type]]');

        $query->andWhere(['{{installation_types}}.[[mobile]]' => 1]);        
        return $query->all();
    }        
    
    
    public function getProductionUnits()
    {
        return \app\models\units\Units::find(['scope' => \app\models\units\Units::UNIT_OF_ENERGY_GO])->all();
    }
    
    
    public function RefreshInferred()
    {
        
        $query = \app\models\Installations::find();

        $query->join('inner join', 
            '{{event_declaration}}', 
            '{{event_declaration}}.[[installation_id]] = {{installations}}.[[id]]');
  
        $query->join('inner join', 
                '{{event_classifications}}', 
                '{{event_classifications}}.[[event_id]] = {{event_declaration}}.[[id]]');


        $query->join('inner join', 
                '{{ca_incident_cat}}', 
                '{{event_classifications}}.[[id]] = {{ca_incident_cat}}.[[draft_id]]');
        
        //vamanbo 2015/30/04
        $query->join('inner join', 
                '{{cpf_crf_assessments}}',
                '{{cpf_crf_assessments}}.[[report_id]] = {{ca_incident_cat}}.[[draft_id]]');
        
        $query->where(['{{ca_incident_cat}}.[[is_done]]' => 1]);
        $query->andWhere(['like', '{{event_declaration}}.[[event_date_time]]', $this->year]);
        //vamanbo 2015/30/04
        $query->andWhere(['{{cpf_crf_assessments}}.[[assessment_status]]' => CpfCrfAssessments::ASSESS_STATUS_FINALIZED]);
        
        $query->select([
            '{{installations}}.[[id]]',
            '{{installations}}.[[name]]',
            '{{installations}}.[[type]]',
            //'{{installations_types}}.[[type]]',
            '{{installations}}.[[year_of_construction]]',
            '{{installations}}.[[number_of_beds]]',
        ]);
        $query->distinct();
        
        $installations = $query->all();
        $already_imported = CpfInstallations::find()->asArray()
                ->where(['year'=>$this->year, 'user_id'=>$this->user_id])
                ->andWhere(['not', ['syrio_id' => null]])
                ->select(['syrio_id'])
                ->all();
        
        $existing = [];
        foreach($already_imported as $item)
        {
            $existing[] = $item['syrio_id'];
        }
        
        echo print_r($already_imported);
        echo '<br/>';
        echo print_r($existing);
        
        $transaction = \Yii::$app->db->beginTransaction();
        $news=[];
        $skipped=[];
        
        //die();
        foreach($installations as $installation) {
            if (!CpfInstallations::findOne(['syrio_id'=>$installation->id, 'year'=>$this->year, 'user_id'=>$this->user_id]))
            {
                /* @var $installation \app\models\Installations */
                
                //create a new CpfInstallation
                $new = new CpfInstallations();
                $new->year=$this->year;
                $new->user_id = $this->user_id;
                
                $new->ident = $installation->name;
                $new->type = $installation->type;
                $new->installation_year =$installation->year_of_construction;
                //$new->tof
                $new->n_beds = $installation->number_of_beds;
                //$new->lat;
                //$new->lon;
                $new->tof = 'oil';
                $new->syrio_id = $installation->id;
                
                if ($new->validate())
                {
                    if (!$new->save()){
                        $transaction->rollBack();
                        return null;
                    }
                }
                else
                {
                    $transaction->rollBack();
                    return null;
                    echo '<pre>';
                    echo var_dump($new->errors);
                    echo '<pre/>';
                    die();
                }
                $news[] = $new->ident;
            }
            else {
                $skipped[] = $installation->id;
            }
        }
        
        $newlist = \yii\helpers\ArrayHelper::merge($news, $skipped);
        $removed = [];
        /* remove the ones that were previously imported but are no longer in the events list */
        foreach($existing as $toremove)
        {
            if (!in_array($toremove, $newlist))
            {
                CpfInstallations::deleteAll(['year'=>$this->year, 'user_id'=>$this->user_id, 'syrio_id'=>$toremove]);
                $removed[] = $toremove;
            }
        }
        
        $transaction->commit();
        
        $return = [
            'new' => $news,
            'skipped' => $skipped,
            'removed' => $removed
        ];

        
        return $return;
        
        echo '<pre>';
        //echo var_dump($newlist);
        echo var_dump($return);
        echo '<pre/>';
        die();
        
    }    
}
