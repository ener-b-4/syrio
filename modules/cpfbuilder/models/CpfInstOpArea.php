<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\modules\cpfbuilder\models;

use Yii;

/**
 * This is the model class for table "cpf_inst_op_area".
 *
 * @property integer $id
 * @property integer $installation_id
 * @property integer $year
 * @property integer $user_id
 * @property string $name
 * @property double $duration
 *
 * @property CpfInstallations $installation
 */
class CpfInstOpArea extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cpf_inst_op_area';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['installation_id', 'year', 'user_id', 'name', 'duration'], 'required'],
            [['installation_id', 'year', 'user_id'], 'integer'],
            [['duration'], 'number', 'min'=>0, 'max'=>'12'],
            [['duration'], 'required'],
            [['name'], 'required']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'installation_id' => 'Installation ID',
            'year' => Yii::t('app/cpf', 'Year'),
            'user_id' => 'User ID',
            'name' => Yii::t('app', 'Name'),
            'duration' => Yii::t('app/cpf', 'Duration'),
            
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInstallation()
    {
        return $this->hasOne(CpfInstallations::className(), ['id' => 'installation_id']);
    }
}
