<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\modules\cpfbuilder\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\cpfbuilder\models\CpfSection1;

/**
 * CpfSection1Search represents the model behind the search form about `app\modules\cpfbuilder\models\CpfSection1`.
 */
class CpfSection1Search extends CpfSection1
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['year', 'user_id'], 'integer'],
            [['ms', 'ca', 'dra', 'phone', 'email'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CpfSection1::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'year' => $this->year,
            'user_id' => $this->user_id,
        ]);

        $query->andFilterWhere(['like', 'ms', $this->ms])
            ->andFilterWhere(['like', 'ca', $this->ca])
            ->andFilterWhere(['like', 'dra', $this->dra])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'email', $this->email]);

        return $dataProvider;
    }
}
