<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\modules\cpfbuilder\models;

use Yii;

/**
 * This is the model class for table "cpf_installations".
 *
 * @property integer $id
 * @property integer $year
 * @property integer $user_id
 * @property string $type
 * @property string $ident
 * @property integer $installation_year
 * @property string $tof
 * @property integer $n_beds
 * @property double $lat
 * @property double $lon
 * @property integer $decom_type
 * @property string $syrio_id
 *
 * @property CpfInstOpArea[] $cpfInstOpAreas
 * @property CpfSection2 $year0
 * @property InstallationTypes $type0
 * @property CpfTypeOfFluid $tof0
 */
class CpfInstallations extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cpf_installations';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['year', 'user_id', 'type', 'ident', 'installation_year', 'tof'], 'required'],
            [['year', 'user_id', 'installation_year', 'n_beds', 'decom_type'], 'integer'],
            [['lat'], 'number', 'min'=>-179.999, 'max'=>180.0],
            [['lon'], 'number', 'min'=>-89.5, 'max'=>89.5],
            [['n_beds'], 'integer', 'min'=>0, 'max'=>150],
            [['type'], 'string', 'max' => 8],
            [['ident'], 'string', 'max' => 128],
            [['tof'], 'string', 'max' => 16],
            [['syrio_id'], 'string', 'max' => 12]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'year' => Yii::t('app/cpf', 'Year'),
            'user_id' => 'User ID',
            'type' => ucfirst(Yii::t('app', 'type')),
            'ident' => Yii::t('app/cpf', 'Name or ID'),
            'installation_year' => Yii::t('app/cpf', 'Installation Year'),
            'tof' => Yii::t('app/cpf', 'Type of fluid'),
            'n_beds' => Yii::t('app/cpf', 'Number of beds'),
            'lat' => Yii::t('app/cpf', 'latitude'),
            'lon' => Yii::t('app/cpf', 'longitude'),
            'decom_type' => Yii::t('app/cpf', 'Decommissioning type'),
            'syrio_id' => 'Syrio ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCpfInstOpAreas()
    {
        return $this->hasMany(CpfInstOpArea::className(), ['installation_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getYear0()
    {
        return $this->hasOne(CpfSection2::className(), ['year' => 'year', 'user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType0()
    {
        return $this->hasOne(\app\models\InstallationTypes::className(), ['type' => 'type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTof0()
    {
        return $this->hasOne(CpfTypeOfFluid::className(), ['type' => 'tof']);
    }
}
