<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\modules\cpfbuilder\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\cpfbuilder\models\CpfInstallations;

/**
 * CpfInstallationsSearch represents the model behind the search form about `app\modules\cpfbuilder\models\CpfInstallations`.
 */
class CpfInstallationsSearch extends CpfInstallations
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'year', 'user_id', 'tof', 'n_beds', 'decom_type'], 'integer'],
            [['type', 'ident', 'syrio_id'], 'safe'],
            [['lat', 'lon'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CpfInstallations::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'year' => $this->year,
            'user_id' => $this->user_id,
            'tof' => $this->tof,
            'n_beds' => $this->n_beds,
            'lat' => $this->lat,
            'lon' => $this->lon,
            'decom_type' => $this->decom_type,
        ]);

        $query->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'ident', $this->ident])
            ->andFilterWhere(['like', 'syrio_id', $this->syrio_id]);

        return $dataProvider;
    }
}
