<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\modules\cpfbuilder\assets;

use yii\web\AssetBundle;

class cpf_builderAsset extends AssetBundle 
{
    public $sourcePath = '@app/modules/cpfbuilder/assets'; 
    public $js = [
        //'js/ui_interaction.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
    public $css = [ 
        //'css/main.css', 
    ]; 
    
    /*
    public function init()
    {
        parent::init();
        $this->publishOptions['beforeCopy'] = function ($from, $to) {
            $dirname = basename(dirname($from));
            return $dirname === 'fonts' || $dirname === 'css';
        };
    }
     * 
     */
}