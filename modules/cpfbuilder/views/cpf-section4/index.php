<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\cpfbuilder\models\CpfSection4Search */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app/cpf', 'Cpf Section4s');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cpf-section4-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app/cpf', 'Create Cpf Section4'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'year',
            'user_id',
            's4_1_n_events_total',
            's4_1_n_major_accidents',
            's4_2_a_total',
            // 's4_2_a_if',
            // 's4_2_a_ix',
            // 's4_2_a_nig',
            // 's4_2_a_nio',
            // 's4_2_a_haz',
            // 's4_2_b_total',
            // 's4_2_b_bo',
            // 's4_2_b_bda',
            // 's4_2_b_wbf',
            // 's4_2_c_total',
            // 's4_2_d_total',
            // 's4_2_d_si',
            // 's4_2_d_sb',
            // 's4_2_d_sk',
            // 's4_2_e_total',
            // 's4_2_f_total',
            // 's4_2_g_total',
            // 's4_2_h_total',
            // 's4_2_i_total',
            // 's4_2_j_total',
            // 's4_3_total',
            // 's4_3_fatalities',
            // 's4_3_inj',
            // 's4_3_sinj',
            // 's4_4_nsis',
            // 's4_4_npcs',
            // 's4_4_nics',
            // 's4_4_nds',
            // 's4_4_npcrs',
            // 's4_4_nps',
            // 's4_4_nsds',
            // 's4_4_nnavaids',
            // 's4_4_roteq',
            // 's4_4_eere',
            // 's4_4_ncoms',
            // 's4_4_nother',
            // 's4_5_a_total',
            // 's4_5_a_df',
            // 's4_5_a_co__int',
            // 's4_5_a_co__ext',
            // 's4_5_a_mf__f',
            // 's4_5_a_mf__wo',
            // 's4_5_a_mf__dm',
            // 's4_5_a_mf__vh',
            // 's4_5_a_if',
            // 's4_5_a_csf',
            // 's4_5_a_other',
            // 's4_5_b_total',
            // 's4_5_b_err__op',
            // 's4_5_b_err__mnt',
            // 's4_5_b_err__tst',
            // 's4_5_b_err__insp',
            // 's4_5_b_err__dsgn',
            // 's4_5_b_other',
            // 's4_5_c_total',
            // 's4_5_c_inq__rap',
            // 's4_5_c_inq__instp',
            // 's4_5_c_nc__prc',
            // 's4_5_c_nc__ptw',
            // 's4_5_c_inq__com',
            // 's4_5_c_inq__pc',
            // 's4_5_c_inq__sup',
            // 's4_5_c_inq__safelead',
            // 's4_5_c_other',
            // 's4_5_d_',
            // 's4_6:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
