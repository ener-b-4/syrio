<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\cpfbuilder\models\CpfSection4 */
/* @var $readonly boolean */

$this->title = $model->year;
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app/cpf', 'Cpf Section4s'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cpf-section4-view">

    <div class="row sy_margin_bottom_26" id="section4">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-xs-10 sy_pad_bottom_18">
                    <span class="cpf-section-title"><?= ucfirst(\Yii::t('app/crf', 'Section {evt}', ['evt'=>'4'])) ?></span>
                    <span class="cpf-section-subtitle"><?= \Yii::t('app/cpf', 'INCIDENT DATA AND PERFORMANCE OF OFFSHORE OPERATIONS') ?></span>
                </div>
                <div class="col-xs-2">
                </div>
            </div>
        </div>        
        
        
        <div class="col-lg-12">
            <?= $this->render('partial_views/s4_1', ['model'=>$model]) ?>
            <?= $this->render('partial_views/s4_2', ['model'=>$model]) ?>
            <?= $this->render('partial_views/s4_3', ['model'=>$model]) ?>
            <?= $this->render('partial_views/s4_4', ['model'=>$model]) ?>
            <?= $this->render('partial_views/s4_5', ['model'=>$model]) ?>
            <?= $this->render('partial_views/s4_6', ['model'=>$model]) ?>
        </div>
    </div>
    
    
</div>

<?php
$js = <<< 'SCRIPT'
/* To initialize BS3 tooltips set this below */
    $('body').tooltip({selector: '[data-toggle="tooltip"]'});
/* To initialize BS3 popovers set this below */
    $('body').popover({selector: '[data-toggle="popover"]'});
    
SCRIPT;
// Register tooltip/popover initialization javascript
$this->registerJs($js);
?>
