<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use app\models\shared\NullValueFormatter;

/* @var $this yii\web\View */
/* @var $model app\modules\cpfbuilder\models\CpfSection4 */

$section_no = '4.4 ';
$section_title = \Yii::t('app/cpf', 'Failures of Safety and Environmental Critical Elements') . ' (SECE)';

?>

<div class="row sy_margin_bottom_26" id="section4_4">
    

    <div class='col-lg-12'>
        <p style="margin-bottom: 0px"><span class="text-info"><?= $section_no ?></span><strong class="text-info" style="margin-bottom: 12px;"><?= $section_title ?></strong></p>           
        <p/>
            
            <?php
            $s4 = new \app\modules\cpfbuilder\models\CpfSection4();
            $attributes = app\modules\cpfbuilder\models\CpfCore::cpf_core_attr_s4_x(4);
            ?>

                <table class="table table-condensed">
                    <thead>
                        <tr>
                            <th>SECE</th>
                            <th class="text-center" colspan="3">
                                <?= \Yii::t('app/cpf', 'Number related to major accidents') ?>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $letters =  range('a', 'l');
                        $index = 0;
                    foreach ($attributes as $attr)
                    {
                        ?>
                        <?php
                        $user_attr = $attr;
                        $total_css = ''; //strpos($attr, 'total') !== FALSE ? 'sy_pad_top_18' : 'sy_pad_left_6';
                        ?>
                        <tr>
                            <td class="<?= $total_css ?>" text-center>
                                <?= '(' . $letters[$index] . ')<span style = "padding-left:12px;">' 
                                . $s4->attributeLabels()[$attr]
                                . '</span>' ?>
                            </td>
                            <td class="text-center">
                                <?= NullValueFormatter::Format($model->$user_attr) ?>
                            </td>
                        </tr>
                        <?php
                        $index++;
                    }

                    ?>
                    </tbody>
                </table>        
        
    </div>    <!-- end table content div -->
  
    
</div>