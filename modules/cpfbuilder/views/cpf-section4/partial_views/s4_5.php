<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use app\models\shared\NullValueFormatter;

/* @var $this yii\web\View */
/* @var $model app\modules\cpfbuilder\models\CpfSection4 */

$section_no = '4.5 ';
$section_title = \Yii::t('app/cpf', 'Direct and Underlying causes of major incidents');

?>

<div class="row sy_margin_bottom_26" id="section4_4">
    

    <div class='col-lg-12'>
        <p style="margin-bottom: 0px"><span class="text-info"><?= $section_no ?></span><strong class="text-info" style="margin-bottom: 12px;"><?= $section_title ?></strong></p>           
        <p/>
            
            <?php
            $s4 = new \app\modules\cpfbuilder\models\CpfSection4();
            
            $headers = [
                'a' => \Yii::t('app/cpf', 'Equipment-related causes'),
                'b' => \Yii::t('app/cpf', 'Human error - operational causes'),
                'c' => \Yii::t('app/cpf', 'Procedural / Organizational error'),
                'd' => \Yii::t('app/cpf', 'Weather-related causes')
            ];
            
            foreach (range('a', 'd') as $letter) {
            
            $attributes = app\modules\cpfbuilder\models\CpfCore::cpf_core_attr_s4_x('5_'. $letter);
            ?>

                <table class="table table-condensed">
                    <thead>
                        <tr>
                            <th>(<?= $letter ?>) <?= $headers[$letter] ?></th>
                            <th>
                                <?= \Yii::t('app/cpf', 'Number related to major accidents') ?>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                    foreach ($attributes as $attr)
                    {
                        ?>
                        <?php
                        $user_attr = $attr;
                        $total_css = ''; //strpos($attr, 'total') !== FALSE ? 'sy_pad_top_18' : 'sy_pad_left_6';
                        ?>
                        <tr>
                            <td class="<?= $total_css ?>">
                                <?= $s4->attributeLabels()[$attr] ?>
                            </td>
                            <td class="text-center">
                                <?= $model->$user_attr ?>
                            </td>
                        </tr>
                        <?php
                    }

                    ?>
                    </tbody>
                </table>
            <?php
            
            }   //close letters range
            ?>
                
    </div>    <!-- end table content div -->
  
    
</div>