<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use app\models\shared\NullValueFormatter;

/* @var $this yii\web\View */
/* @var $model app\modules\cpfbuilder\models\CpfSection4 */

$section_no = '4.1 ';
$section_title = \Yii::t('app/cpf', 'Incident data');
?>

<div class="row sy_margin_bottom_26" id="section4_1">
    <div class="col-lg-12">
        <p style="margin-bottom: 0px"><span class="text-info"><?= $section_no ?></span>
            <strong class="text-info" style="margin-bottom: 12px;"><?= $section_title ?></strong></p>           
        <p/>
        <p>
            <?= $model->attributeLabels()['s4_1_n_events_total'] ?>: 
            <?= NullValueFormatter::Format($model->s4_1_n_events_total) ?>
        </p>
        <p>
            <?= \Yii::t('app/cpf', 'of which identified as being major accidents') ?>: 
            <?= NullValueFormatter::Format($model->s4_1_n_major_accidents) ?>
        </p>
    </div>
</div>