<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use app\models\shared\NullValueFormatter;

/* @var $this yii\web\View */
/* @var $model app\modules\cpfbuilder\models\CpfSection4 */

$section_no = '4.3 ';
$section_title = \Yii::t('app/cpf', 'Total number of fatalities and injuries');

//$norm_value_ktoe = count($model->cpfSession->assessedReports);

if (!isset($model->cpfSession->cpfSection2->norm_ktoe) || $model->cpfSession->cpfSection2->norm_ktoe == 0) {
    $norm_value_ktoe = 0;
} else {
    $norm_value_ktoe = $model->cpfSession->cpfSection2->norm_ktoe;
}

if (!isset($model->cpfSession->cpfSection2->working_hours) || $model->cpfSession->cpfSection2->working_hours == 0) {
    $norm_value_hours = 0;
} else {
    $norm_value_hours = $model->cpfSession->cpfSection2->working_hours;
}

//echo '<pre>';
//echo var_dump($s4_2_node_equiv);
//echo '</pre>';
//die();
?>

<div class="row sy_margin_bottom_26" id="section4_3">
    

    <div class='col-lg-12'>
        <p style="margin-bottom: 0px"><span class="text-info"><?= $section_no ?></span><strong class="text-info" style="margin-bottom: 12px;"><?= $section_title ?></strong></p>           
        <p/>
            
            <?php
            $s4 = new \app\modules\cpfbuilder\models\CpfSection4();
            $attributes = app\modules\cpfbuilder\models\CpfCore::cpf_core_attr_s4_x(3);
            ?>
                <table class="table table-condensed cpf_table">
                    <thead>
                        <tr>
                            <th>Total number of fatalities and injuries</th>
                            <th>
                                Number
                            </th>
                            <th colspan="2">Normalized value</th>
                            <th>
                            </th>
                        </tr>
                        <tr>
                            <th></th>
                            <th><?= \Yii::t('app/cpf' ,'(reported number)') ?></th>
                            <th><?= \Yii::t('app/cpf' ,'(number/hours worked)') ?></th>
                            <th><?= \Yii::t('app/cpf' ,'(number/kTOE)') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                    foreach ($attributes as $attr)
                    {
                        ?>
                        <tr>
                            <td class="">
                                <?= $s4->attributeLabels()[$attr] ?>
                            </td>
                            <td>
                                <?= NullValueFormatter::Format($model->$attr) ?>
                            </td>
                            <td>
                                <?= $norm_value_hours !== 0 ? sprintf('%.3e', $model->$attr/$norm_value_hours) : 'x' ?> 
                            </td>
                            <td>
                                <?= $norm_value_ktoe !== 0 ? sprintf('%.3e', $model->$attr/$norm_value_ktoe) : 'x' ?> 
                            </td>
                        </tr>
                        <?php
                    }

                    ?>
                    </tbody>
                    <tfoot>
                      <tr>
                          <td rowspan="2" colspan="2"><?= \Yii::t('app/cpf', 'Values used for normalization') ?>:<br/>
                              <small>
                                  <?= \Yii::t('app/cpf', 'Provided in {evt} of this document', [
                                      'evt'=>'<a href="#section2_4">' . \Yii::t('app/crf', 'Section {evt}', [
                                          'evt'=>'2.4'
                                          ]) .'</a>'
                                  ]) ?>
                              </small>
                          </td>
                          <td colspan="3"><?= \Yii::t('app/cpf', 'Total number of actual offshore working hours for all installations') . ': '.NullValueFormatter::Format($model->cpfSession->cpfSection2->working_hours) ?></td>
                      </tr>
                      <tr>
                          <td colspan="3"><?= \Yii::t('app/cpf', 'Total production, in kTOE') . ': '.NullValueFormatter::Format($model->cpfSession->cpfSection2->norm_ktoe) ?></td>
                      </tr>
                    </tfoot>    
                </table>
        
    </div>    <!-- end table content div -->
  
    
</div>