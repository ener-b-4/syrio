<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use app\models\shared\NullValueFormatter;

/* @var $this yii\web\View */
/* @var $model app\modules\cpfbuilder\models\CpfSection4 */

$section_no = '4.2 ';
$section_title = \Yii::t('app/cpf', 'Annex IX Incident Categories');

$letters = ['a','b','c','d','e','f','g','h','i','j'];

$s4_2_node_equiv = [];
$s4_2_node_equiv['s4_2_a_total']    = \Yii::t('app/cpf', 'Unintended releases');
$s4_2_node_equiv['s4_2_a_if']       = \Yii::t('app/cpf', 'Ignited oil/gas releases - Fires');
$s4_2_node_equiv['s4_2_a_ix'   ]    = \Yii::t('app/cpf', 'Ignited oil/gas releases - Explosions');
$s4_2_node_equiv['s4_2_a_nig'  ]    = \Yii::t('app/cpf', 'Not ignited gas releases');
$s4_2_node_equiv['s4_2_a_nio'  ]    = \Yii::t('app/cpf', 'Not ignited oil releases');
$s4_2_node_equiv['s4_2_a_haz'  ]    = \Yii::t('app/cpf', 'Hazardous substances released');
$s4_2_node_equiv['s4_2_b_total']    = \Yii::t('app/cpf', 'Loss of well control');
$s4_2_node_equiv['s4_2_b_bo'   ]    = \Yii::t('app/cpf', 'Blowouts');
$s4_2_node_equiv['s4_2_b_bda'  ]    = \Yii::t('app/cpf', 'Activation of BOP / diverter system');
$s4_2_node_equiv['s4_2_b_wbf'  ]    = \Yii::t('app/cpf', 'Failure of a well barrier');
$s4_2_node_equiv['s4_2_c_total']    = \Yii::t('app/cpf', 'Failures of SECE');
$s4_2_node_equiv['s4_2_d_total']    = \Yii::t('app/cpf', 'Loss of structural integrity');
$s4_2_node_equiv['s4_2_d_si'   ]    = \Yii::t('app/cpf', 'Loss of structural integrity');
$s4_2_node_equiv['s4_2_d_sb'   ]    = \Yii::t('app/cpf', 'Loss of stability/buoyancy');
$s4_2_node_equiv['s4_2_d_sk'   ]    = \Yii::t('app/cpf', 'Loss of station keeping');
$s4_2_node_equiv['s4_2_e_total']    = \Yii::t('app/cpf', 'Vessel collisions');
$s4_2_node_equiv['s4_2_f_total']    = \Yii::t('app/cpf', 'Helicopter accidents');
$s4_2_node_equiv['s4_2_g_total']    = \Yii::t('app/cpf', 'Fatal accidents');
$s4_2_node_equiv['s4_2_h_total']    = \Yii::t('app/cpf', 'Serious injuries of 5 or more persons in the same accident');
$s4_2_node_equiv['s4_2_i_total']    = \Yii::t('app/cpf', 'Evacuation of personnel');
$s4_2_node_equiv['s4_2_j_total']    = \Yii::t('app/cpf', 'Environmental accidents');

//echo '<pre>';
//echo var_dump($s4_2_node_equiv);
//echo '</pre>';
//die();

       /* check if norm data is available */
        $norm_qtoe = isset($model->cpfSession->cpfSection2->norm_ktoe) ? $model->cpfSession->cpfSection2->norm_ktoe : 0;
        $norm_w_hours = isset($model->cpfSession->cpfSection2->working_hours) ? $model->cpfSession->cpfSection2->working_hours : 0;
     //echo $norm_qtoe;
     //die();
?>

<div class="row sy_margin_bottom_26" id="section4_2">
    

    <div class='col-lg-12'>
        <p style="margin-bottom: 0px"><span class="text-info"><?= $section_no ?></span><strong class="text-info" style="margin-bottom: 12px;"><?= $section_title ?></strong></p>           
        <p/>
            
        <table class="table table-condensed cpf_table">
       <thead>
         <tr>
             <th colspan="2"><?= ucfirst(\Yii::t('app/translations' ,'category')) ?></th>
             <th> <?= \Yii::t('app/cpf' ,'Number of events') ?></th>
             <th colspan="2"><?= \Yii::t('app/cpf' ,'Normalized number of events') ?></th>
         </tr>
         <tr>
             <th/>
             <th/>
             <th><?= \Yii::t('app/cpf' ,'(reported number)') ?></th>
             <th><?= \Yii::t('app/cpf' ,'(events/hours worked)') ?></th>
             <th><?= \Yii::t('app/cpf' ,'(events/kTOE)') ?></th>
         </tr>
       </thead>
       <tbody>
           <?php
           $returned_html = '';
           foreach ($letters as $letter)
           {
               $index = 0;
               foreach($s4_2_node_equiv as $key=>$value)
               {
                    if ( strpos($key, '_' . $letter . '_') !== FALSE)
                    {
                        ?>
                        <tr>
                        <?php
                        if ($index==0)
                        {
                            ?>
                            <td>(<?= $letter ?>)</td>
                            <td><?= $value ?></td>
                            <td><?= NullValueFormatter::Format($model->$key) ?></td>
                            <!-- $returned_html .= ' <td class="inferred ' + key[0]+'_wh">' + $cpf['inferred']['s4_2_'+$letter+'_norm'][key[0]+'_wh'] +'</td>';
                            //$returned_html += ' <td class="inferred ' + key[0]+'_ktoe">' + $cpf['inferred']['s4_2_'+$letter+'_norm'][key[0]+'_ktoe'] +'</td>'; -->
                            <td>
                                <strong>
                                <?= isset($norm_w_hours) && $norm_w_hours != 0 ? 
                                    isset($value) ? sprintf('%.3e', ($model->$key/$norm_w_hours)) : 'x' : 'x' ?>                            
                                </strong>
                            </td>
                            <td>
                                <strong>
                                <?= isset($norm_qtoe) && $norm_qtoe != 0 ? 
                                    isset($value) ? sprintf('%.3e', $model->$key/$norm_qtoe) : 'x' : 'x' ?>                                    
                                </strong>                                
                            </td>
                            <?php
                            //create the first row
                        }
                        else
                        {
                            ?>
                            <td/>
                            <td><?= $value ?></td>
                            <td><?= NullValueFormatter::Format($model->$key) ?></td>
                            <!--$returned_html += ' <td class="inferred ' + key[0]+'_wh">' + $cpf['inferred']['s4_2_'+$letter+'_norm'][key[0]+'_wh'] +'</td>';
                            //$returned_html += ' <td class="inferred ' + key[0]+'_ktoe">' + $cpf['inferred']['s4_2_'+$letter+'_norm'][key[0]+'_ktoe'] +'</td>';-->
                            <td>
                                <?= isset($norm_w_hours) && $norm_w_hours != 0 ? 
                                    isset($value) ? sprintf('%.3e', $model->$key/$norm_w_hours) : 'x' : 'x' ?>
                            </td>
                            <td>
                                <?= isset($norm_qtoe) && $norm_qtoe != 0 ? 
                                    isset($value) ? sprintf('%.3e', $model->$key/$norm_qtoe) : 'x' : 'x' ?>
                            </td>
                            <?php
                        }
                        $index++;
                    }
               }
           }
           ?>
       </tbody>
        <tfoot>
          <tr>
              <td rowspan="2" colspan="2"><?= \Yii::t('app/cpf', 'Values used for normalization') ?>:<br/>
                  <small>
                      <?= \Yii::t('app/cpf', 'Provided in {evt} of this document', [
                          'evt'=>'<a href="#section2_4">' . \Yii::t('app/crf', 'Section {evt}', [
                              'evt'=>'2.4'
                              ]) .'</a>'
                      ]) ?>
                  </small>
              </td>
              <td colspan="3"><?= \Yii::t('app/cpf', 'Total number of actual offshore working hours for all installations') . ': '.NullValueFormatter::Format($model->cpfSession->cpfSection2->working_hours) ?></td>
          </tr>
          <tr>
              <td colspan="3"><?= \Yii::t('app/cpf', 'Total production, in kTOE') . ': '.NullValueFormatter::Format($model->cpfSession->cpfSection2->norm_ktoe) ?></td>
          </tr>
        </tfoot>    
    </table>
        
      </div>    <!-- end table content div -->
  
    
</div>
<!-- 20150920_01 -->