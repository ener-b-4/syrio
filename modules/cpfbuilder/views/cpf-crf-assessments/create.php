<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\cpfbuilder\models\CpfCrfAssessments */

$this->title = Yii::t('app/cpf', 'Create Cpf Crf Assessments');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/cpf', 'Cpf Crf Assessments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cpf-crf-assessments-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
