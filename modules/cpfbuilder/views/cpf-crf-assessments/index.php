<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\cpfbuilder\models\CpfCrfAssessmentsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = ucwords(Yii::t('app', 'annual report preparation'));
$this->params['breadcrumbs'][] = $this->title;


$assessment_status = [
    0 => strtoupper(\Yii::t('app', 'new')),
    5 => strtoupper(\Yii::t('app', 'suspended')),
    10 => strtoupper(\Yii::t('app', 'in progress')),
    20 => strtoupper(\Yii::t('app', 'finalized')),
];

$cpf_drafting_url = \Yii::$app->urlManager->createUrl(['/cpfbuilder/cpf-sessions/index']);

$_canEdit = \Yii::$app->user->can('ca_generate_annual_report');

?>
<div class="cpf-crf-assessments-index">

    <div class="row">
        <div class="col-lg-12">
        <?php
        /* use this on any view to include the actionmessage logic and widget */
        //include Yii::getAlias('@app') . '\views\shared_parts\message_snippet.php';
        include Yii::getAlias('@app') . '/views/shared_parts/message_snippet.php';    

        ?>
        </div>
    </div>
    
    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="row sy_pad_top_36">
        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6">

            <?= $this->render('../help/cpf_crf_index_help', ['cpf_drafting_url'=>$cpf_drafting_url, '_canEdit' => $_canEdit]) ?>
            
        </div>
        <div class="col-lg-9 col-md-9 col-sm-8 col-xs-6">
            
            
            <?php if ($_canEdit) {?>
            <div class="form-group">
                <?= Html::a('<span class="glyphicon glyphicon-refresh" style="padding-right:6px;">' 
                        . '</span>'
                        . Yii::t('app/cpf', 'Refresh')
                        , ['refresh-list'], ['class' => 'btn btn-danger']) ?>
            </div>
            <?php } //end if can edit ?>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'year',
                    //'user_id',
                    [
                        'class' => \yii\grid\DataColumn::className(),
                        'attribute' => 'report_id',
                        'content' => function($model){
                            return $model->report->name;                    
                        },  
                    ],
                    [
                        'class' => \yii\grid\DataColumn::className(),
                        'attribute' => 'assessment_status',
                        'content' => function($model){
                            switch ($model->assessment_status)
                            {
                                case app\modules\cpfbuilder\models\CpfCrfAssessments::ASSESS_STATUS_NEW:
                                    return strtoupper(\Yii::t('app', 'new'));
                                    break;
                                case app\modules\cpfbuilder\models\CpfCrfAssessments::ASSESS_STATUS_SUSPENDED:
                                    return strtoupper(\Yii::t('app', 'suspended'));
                                    break;
                                case app\modules\cpfbuilder\models\CpfCrfAssessments::ASSESS_STATUS_IN_PROGRESS:
                                    return strtoupper(\Yii::t('app', 'in progress'));
                                    break;
                                case app\modules\cpfbuilder\models\CpfCrfAssessments::ASSESS_STATUS_FINALIZED:
                                    return strtoupper(\Yii::t('app', 'finalized'));
                                    break;
                            }
                        },
                        'filter' => Html::activeDropDownList(
                                $searchModel, 
                                'assessment_status', 
                                $assessment_status,
                                [
                                    'class'=>'form-control',
                                    'prompt' => '-- status --'])
                    ],
                                
                    [
                        'label' => \Yii::t('app', 'Added'),
                        'attribute' => 'created_at',
                        'format' => ['date', 'php:Y-m-d H:i'],
                        'width' => '150px;',
                          'filterType' => \yii\widgets\MaskedInput::className(),
                          'filterWidgetOptions' => [
                              'mask'=>'(9999)|*-(99)|*-(99)|*',
                              //'mask'=>'(9999)|*-(99)|*-(99)|* (99)|*:(99)|*:(99)|*',
                              'definitions'=>[
                                  '*'=>[
                                      'validator'=>'\*',
                                      'cardinality'=>1,
                                  ]
                              ],
                          ],
                        'filterInputOptions' => ['placeholder'=>'Y-m-d', 'class'=>'form-control'],
                        
                    ],
                    // 'created_at',
                    // 'created_by',
                    // 'modified_at',
                    // 'modified_by',


                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{view} {reopen} {update} {delete}',
                        'buttons' => [
                            'update' => function($url, $model, $key) {
                                if(Yii::$app->user->can('ca_generate_annual_report')) {
                                    if (($model->assessment_status != \app\modules\cpfbuilder\models\CpfCrfAssessments::ASSESS_STATUS_FINALIZED)) {
                                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                            'title'=>Yii::t('app', 'Update')
                                        ]);
                                    }
                                }
                            },

                            'delete' => function($url, $model, $key) {
                                if(Yii::$app->user->can('ca_generate_annual_report')) {
                                    $msg = Yii::t('app', 'Are you sure you want to delete this item?');
                                    
                                    if (($model->assessment_status != \app\modules\cpfbuilder\models\CpfCrfAssessments::ASSESS_STATUS_FINALIZED)) {
                                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                            'title'=>ucfirst(Yii::t('app', 'Delete')),
                                            'data' => [
                                                'method'=>'post',
                                                'confirm'=>$msg
                                            ]
                                        ]);
                                    }
                                }
                            },
                                    
                            'reopen' => function($url, $model, $key) {
                                if(Yii::$app->user->can('ca_generate_annual_report')) {
                                    if (($model->assessment_status == \app\modules\cpfbuilder\models\CpfCrfAssessments::ASSESS_STATUS_FINALIZED)) {
                                        $msg = Yii::t('app/messages', 'Reopening the assessment may have cascade effects on the Annual Report.');
                                        $msg .= "\n";
                                        $msg .= "\n";
                                        $msg .= Yii::t('app/messages', 'Ok to proceed?');
                                        
                                        return Html::a('<span class="glyphicon glyphicon-hand-up"></span>', $url, [
                                            'title'=>ucfirst(Yii::t('app', 'reopen')),
                                            'data' => [
                                                'method'=>'post',
                                                'confirm'=>$msg
                                            ]
                                        ]);
                                    }
                                }
                            }
                                    
                        ]
                    ],
                ],
            ]); ?>
    </div>
    
    </div>
</div>
    
