<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use \app\models\shared\NullValueFormatter;

/* @var $this yii\web\View */
/* @var $model app\modules\cpfbuilder\models\CpfCrfAssessments */

$subsection = '2';

$header_id = 'section'.$subsection.'header';
$content_name = 'section4_'.$subsection.'_content';
$datatarget= '#'. $content_name;

$chevron_id = 'chevron4_'.$subsection;
$inferrable_attributes = app\modules\cpfbuilder\models\CpfCore::cpf_inferrable_attr();
$section_titles = app\modules\cpfbuilder\models\CpfCore::SectionNames();

?>

        <div class="col-lg-12">
            <div id="<?= $header_id ?>" 
                 data-toggle="collapse" 
                 data-target=<?= $datatarget ?>>
                <h3>
                    <span id="<?= $chevron_id ?>" class='glyphicon glyphicon-chevron-up'></span>
                    <?= \Yii::t('app/crf', 'Section {evt}', ['evt' => '4']) ?>.<?= $subsection ?>
                </h3>
                <h4><?= Html::encode($section_titles['4.'.$subsection]) ?></h4>
            </div>            
            <div id="<?= $content_name ?>" class="section_4_content sy_pad_left_6 collapse in" chevron="<?= $chevron_id ?>">
            <?php
            $s4 = new \app\modules\cpfbuilder\models\CpfSection4();
            $attributes = app\modules\cpfbuilder\models\CpfCore::cpf_core_attr_s4_x(2);
            ?>
                <table class="table table-condensed">
                    <thead>
                        <tr>
                            <th rowspan="2">
                                <?= \Yii::t('app/cpf', 'Annex IX categories') ?> 
                            </th>
                            <th colspan="3" class="text-center">
                                <?= \Yii::t('app/cpf' ,'Number of events') ?>
                            </th>
                        </tr>
                        <tr>
                            <th class="text-center">SyRIO</th>
                            <th class="text-center">?</th>
                            <th class="text-center"><?= ucfirst(\Yii::t('app' , 'you')) ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                    foreach ($attributes as $attr)
                    {
                        ?>
                        <?php
                        $syrio_attr = 's_'.$attr;
                        $user_attr = 'u_'.$attr;
                        if (strpos($attr, 'total') > -1) {
                            $_total = isset($model->$syrio_attr) && $model->$syrio_attr != 0 ? $model->$syrio_attr : 0;
                            $syrio_text = \Yii::t('app', 'computed') . ' ('.$model->$syrio_attr.')';
                            $your_text = '';
                            $total_css = "cpf_table_group"; // 'font-weight: bold';
                            $td_extra_class = "";
                        }
                        else
                        {
                            $syrio_text = null;
                            $your_text = null;
                            $total_css = '';
                            $td_extra_class = "cpf_table_group_child";
                        }
                        ?>
                        <tr>
                            <td class="<?= $total_css ?> <?= $td_extra_class ?>">
                                <?= $s4->attributeLabels()[$attr] ?>
                            </td>
                            <td class="text-center <?= $total_css ?>">
                                <?php
                                    if (isset($syrio_text) && !key_exists($attr, $inferrable_attributes)) {
                                        echo '<span class="text-info">' . $syrio_text . '</span>';
                                    }
                                    else if (!key_exists($attr, $inferrable_attributes))
                                    {
                                        echo NullValueFormatter::Format(null, \Yii::t('app', 'can\'t tell'));
                                    }
                                    else
                                    {
                                        echo Html::checkbox('chk'.$s4->attributeLabels()[$attr], $model->$syrio_attr, ['disabled'=>'disabled']);
//                                        echo !isset($model->$syrio_attr) ? Html::checkbox('chk'.$s4->attributeLabels()[$attr], $model->$syrio_attr, ['disabled'=>'disabled']) :
//                                            Html::checkbox('chk'.$s4->attributeLabels()[$attr], $model->$syrio_attr, ['disabled'=>'disabled']);
                                    }
                                ?>
                            </td>
                            <td class="text-center <?= $total_css ?>">
                                <?php
                                if (key_exists($attr, $inferrable_attributes))
                                {
                                    ?>
                                
                                <span class='badge' 
                                      data-toggle="tooltip" 
                                      title= '<?= $inferrable_attributes[$attr] ?>'
                                      content="content"
                                      style="cursor: default;">
                                      ?
                                </span>                                
                                
                                    <?php
                                }   //end key_exists
                                ?>
                            </td>
                            <td class="text-center <?= $total_css ?>">
                                <?= !isset($model->$user_attr) ? NullValueFormatter::Format($model->$user_attr) :
                                    Html::checkbox('chk'.$s4->attributeLabels()[$attr], $model->$user_attr, ['disabled'=>'disabled']) ?>
                            </td>
                        </tr>
                        <?php
                    }

                    ?>
                    </tbody>
                </table>
            </div>
        </div>
