<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use \app\models\shared\NullValueFormatter;

/* @var $this yii\web\View */
/* @var $model app\modules\cpfbuilder\models\CpfCrfAssessments */
/* @var $form \yii\widgets\ActiveForm */

$subsection = '6';

$header_id = 'section'.$subsection.'header';
$content_name = 'section6_'.$subsection.'_content';
$datatarget= '#'. $content_name;

$chevron_id = 'chevron4_'.$subsection;
//$inferrable_attributes = app\modules\cpfbuilder\models\CpfCore::cpf_inferrable_attr();
$section_titles = app\modules\cpfbuilder\models\CpfCore::SectionNames();

?>
        <div class="col-lg-12">
            <div id="<?= $header_id ?>" 
                 data-toggle="collapse"
                 data-target=<?= $datatarget ?>
                 >
                <h3>
                    <span id="<?= $chevron_id ?>" class='glyphicon glyphicon-chevron-up'></span>
                    <?= \Yii::t('app/crf', 'Section {evt}', ['evt' => '4']) ?>.<?= $subsection ?>
                </h3>
                <h4><?= Html::encode($section_titles['4.'.$subsection]) ?></h4>
            </div>
            <div id="<?= $content_name ?>" class="section_4_content sy_pad_left_6 collapse in" chevron="<?= $chevron_id ?>">
                <?php
                $s4 = new \app\modules\cpfbuilder\models\CpfSection4();

                $attr = 's4_6';
                //reset($attr);
                $user_attr = 'u_'.$attr;
                $total_css = ''; //strpos($attr, 'total') !== FALSE ? 'sy_pad_top_18' : 'sy_pad_left_6';
                ?>
               

                <p>
                    <?= $form->field($model, $user_attr, ['template' => '{input}'])->textarea([
                        'class' => 'form-control',
                        'style' => 'min-height: 200px; height: 200px;'
                    ]) ?>
                </p>

                </div>
        </div>