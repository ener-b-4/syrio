<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use \app\models\shared\NullValueFormatter;

/* @var $this yii\web\View */
/* @var $model app\modules\cpfbuilder\models\CpfCrfAssessments */
/* @var $form \yii\widgets\ActiveForm */

$subsection = '3';

$header_id = 'section'.$subsection.'header';
$content_name = 'section4_'.$subsection.'_content';
$datatarget= '#'. $content_name;

$chevron_id = 'chevron4_'.$subsection;
$inferrable_attributes = app\modules\cpfbuilder\models\CpfCore::cpf_inferrable_attr();
$section_titles = app\modules\cpfbuilder\models\CpfCore::SectionNames();


?>
        <div class="col-lg-12">
            <div id="<?= $header_id ?>" 
                 data-toggle="collapse" 
                 data-target=<?= $datatarget ?>>
                <h3>
                    <span id="<?= $chevron_id ?>" class='glyphicon glyphicon-chevron-up'></span>
                    <?= \Yii::t('app/crf', 'Section {evt}', ['evt' => '4']) ?>.<?= $subsection ?>
                </h3>
                <h4><?= Html::encode($section_titles['4.'.$subsection]) ?></h4>
            </div>
            <div id="<?= $content_name ?>" class="section_4_content collapse in sy_pad_left_6" chevron="<?= $chevron_id ?>">
            <?php
            $s4 = new \app\modules\cpfbuilder\models\CpfSection4();
            $attributes = app\modules\cpfbuilder\models\CpfCore::cpf_core_attr_s4_x(3);
            ?>
                <table class="table table-condensed">
                    <thead>
                        <tr>
                            <th></th>
                            <th colspan="3" class="text-center">
                                Number
                            </th>
                        </tr>
                        <tr>
                            <th/>
                            <th class="text-center">SyRIO</th>
                            <th class="text-center">?</th>
                            <th class="text-center">You</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                    foreach ($attributes as $attr)
                    {
                        ?>
                        <?php
                        $syrio_attr = 's_'.$attr;
                        $user_attr = 'u_'.$attr;
                        $total_css = strpos($attr, 'total') !== FALSE ? 'sy_pad_top_18' : 'sy_pad_left_6';
                        ?>
                        <tr>
                            <td class="<?= $total_css ?>">
                                <?= $s4->attributeLabels()[$attr] ?>
                            </td>
                            <td class="text-center">
                                <?= NullValueFormatter::Format(null, \Yii::t('app', 'can\'t tell')) ?>
                            </td>
                            <td class="text-center">
                                <?php
                                if (key_exists($attr, $inferrable_attributes))
                                {
                                    ?>
                                
                                <span class='badge' 
                                      data-toggle="tooltip" 
                                      title= '<?= $inferrable_attributes[$attr] ?>'
                                      content="content"
                                      style="cursor: default;">
                                      ?
                                </span>                                
                                
                                    <?php
                                }   //end key_exists
                                ?>
                            </td>
                            <td class="text-center">
                                <?php 
                                        $attr_name = 'u_' . $attr; 
                                ?>
                                
                                <?= $form->field($model, $attr_name, ['template' => '{input}'])->textInput([
                                        'class'=>'form-control input-sm text-right', 'style'=>'max-width:65px; width:65px;'], false)
                                        ?>
                                
                                <?php //Html::textInput('chk'.$s4->attributeLabels()[$attr], $model->$user_attr, [
                                        //'class'=>'form-control input-sm text-right', 'style'=>'max-width:65px; width:65px;']) ?>
                            </td>
                        </tr>
                        <?php
                    }

                    ?>
                    </tbody>
                </table>
            </div>
        </div>