<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\cpfbuilder\models\CpfCrfAssessments */

$this->title = Yii::t('app', 'Update', []);
$this->params['breadcrumbs'][] = ['label' => ucwords(Yii::t('app', 'annual report preparation')), 'url' => [
    'index', 
    'year'=>$model->year,
    'user_id'=>\Yii::$app->user->id,
    'report_id'=>$model->report_id
]];
$this->params['breadcrumbs'][] = ['label' => $model->report->name, 'url'=>[
    'view',
    'year'=>$model->year,
    'user_id'=>\Yii::$app->user->id,
    'report_id'=>$model->report_id
]];
$this->params['breadcrumbs'][] = $this->title;

//$this->params['breadcrumbs'][] = ['label' => Yii::t('app/cpf', 'Cpf Crf Assessments'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->year, 'url' => ['view', 'year' => $model->year, 'user_id' => $model->user_id, 'report_id' => $model->report_id]];
//$this->params['breadcrumbs'][] = Yii::t('app/cpf', 'Update');
?>
<div class="cpf-crf-assessments-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <h3>
        <div class="sy_pad_top_18">
            <ul class="list-inline">
                <li><?= ucfirst(\Yii::t('app', 'event type')) ?></li>
                <?php
                    foreach (range('a', 'j') as $letter) {
                        $is = 'is_'.$letter;
                        if ($model->report->draft->$is) {
                            echo '<li>' . strtoupper($letter) . '</li>';
                        }
                    }


                ?>
            </ul>
        </div>
    </h3>
    
    
    <div class="cpf-crf-assessments-form">

        <?php $form = kartik\form\ActiveForm::begin([
            //'formConfig' => ['showLabels'=>'false'],
            //'fieldConfig' => [
            //    'template' => '<div class=\"\">{input}</div><div class=\"\">{error}</div>'
            //]
        ]); ?>
    
        <div class="row">
            <?= $this->render('partial_forms/_form_s4_2', ['model'=>$model, 'form'=>$form]); ?>
        </div>
        <div class="row">
            <?= $this->render('partial_forms/_form_s4_3', ['model'=>$model, 'form'=>$form]); ?>
        </div>
        <div class="row">
            <?= $this->render('partial_forms/_form_s4_4', ['model'=>$model, 'form'=>$form]); ?>
        </div>
        <div class="row">
            <?= $this->render('partial_forms/_form_s4_5', ['model'=>$model, 'form'=>$form]); ?>
        </div>
        <div class="row">
            <?= $this->render('partial_forms/_form_s4_6', ['model'=>$model, 'form'=>$form]); ?>
        </div>
        
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app/commands', 'Cancel'), 
                    \Yii::$app->urlManager->createUrl(['/cpfbuilder/cpf-crf-assessments/cancel', 'year'=>$model->year, 'user_id'=>\Yii::$app->user->id, 'report_id'=>$model->report_id]), 
            [
                'class'=>'btn btn-danger'
            ]) ?>
        </div>
        <?php kartik\form\ActiveForm::end(); ?>
    
    </div>
</div>

<?php

$js_path = \Yii::getAlias('@web');

$this->registerJsFile($js_path . '/js/cpf/cpf-prep.js', ['depends' => [\yii\web\JqueryAsset::className(), yii\bootstrap\BootstrapAsset::className()] ]);

?>    
