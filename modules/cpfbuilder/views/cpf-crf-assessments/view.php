<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\widgets\DetailView;
use \app\models\shared\NullValueFormatter;

/* @var $this yii\web\View */
/* @var $model app\modules\cpfbuilder\models\CpfCrfAssessments */

$this->title = $model->report->name;
$this->params['breadcrumbs'][] = ['label' => ucwords(Yii::t('app', 'annual report preparation')), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cpf-crf-assessments-view">

    <div class="row">   <!-- main row -->
        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12" id="leftPanel">
            <table class="table table-striped table-bordered table-condensed">
                <tr>
                    <td><strong><?= ucfirst(\Yii::t('app', 'created at')) ?></strong></td>
                    <td><?= date('Y-m-d H:i:s', $model->created_at) ?></td>
                </tr>
                <tr>
                    <td><strong><?= ucfirst(\Yii::t('app', 'created by')) ?></strong></td>
                    <td>
                        <?php
                        $user = app\models\User::findOne($model->created_by);
                        echo isset($user) ? $user->full_name : '-';
                        ?>
                    </td>
                </tr>
                <tr>
                    <td><strong><?= ucfirst(\Yii::t('app', 'modified at')) ?></strong></td>
                    <td><?= isset($model->modified_at) ? date('Y-m-d H:i:s', $model->modified_at) : '-' ?></td>
                </tr>
                    <td><strong><?= ucfirst(\Yii::t('app', 'modified by')) ?></strong></td>
                    <td>
                        <?php
                        $user = app\models\User::findOne($model->modified_by);
                        echo isset($user) ? $user->full_name : '-';
                        ?>
                    </td>
            </table>
            
            <?= $this->render('../help/cpf_crf_view_help.php') ?>
            
        </div>
        <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12" id="contentPanel">

    <p>
        <?php 
            if (\Yii::$app->user->can('ca_generate_annual_report'))
            {
                if ($model->assessment_status == app\modules\cpfbuilder\models\CpfCrfAssessments::ASSESS_STATUS_NEW || $model->assessment_status == app\modules\cpfbuilder\models\CpfCrfAssessments::ASSESS_STATUS_IN_PROGRESS) {
                    echo Html::a(Yii::t('app', 'Update'), ['update', 'year' => $model->year, 'report_id' => $model->report_id], ['class' => 'btn btn-primary']);
                    echo Html::a(Yii::t('app', 'Finalize'), ['finalize', 'year' => $model->year, 'report_id' => $model->report_id], [
                        'class' => 'btn btn-success pull-right',
                        'data' => [
                            'confirm' => Yii::t('app/cpf', 'Finalize assessment?'),
                            'method' => 'post',
                        ],
                    ]);
                } else {
                    echo Html::a(Yii::t('app', 'Re-open'), ['reopen', 'year'=>$model->year, 'report_id' => $model->report_id], [
                        'class' => 'btn btn-warning',
                        'data' => [
                            'confirm' => Yii::t('app/cpf', 'Reopen assessment?'),
                            'method' => 'post',
                        ],
                ]);}
            }
        ?>
    </p>

    <h1><?= \Yii::t('app/cpf', 'CPF Section 4 Incident Assessment') ?></h1>
    <h3>
        <?= ucfirst(\Yii::t('app', 'event report')) ?>: 
        <?php
            //r=ca%2Fincident-categorization%2Fview&id=17
            $url = \Yii::$app->urlManager->createUrl(['/ca/incident-categorization/view', 'id'=>$model->report->id]);
        ?>
        <?= Html::a(Html::encode($this->title), $url) ?>
        
        <div class="sy_pad_top_18" style="padding-left: 5px">
            <ul class="list-inline">
                <li><?= ucfirst(\Yii::t('app', 'event type')) ?>: </li>
                <?php
                    foreach (range('a', 'j') as $letter) {
                        $is = 'is_'.$letter;
                        if ($model->report->draft->$is) {
                            echo '<li>' . strtoupper($letter) . '</li>';
                        }
                    }


                ?>
            </ul>
        </div>
    </h3>
    
    
    <div class="row">
        <?= $this->render('partial_views/s4_2', ['model'=>$model]); ?>
    </div>
    <div class='row'>
        <?= $this->render('partial_views/s4_3', ['model'=>$model]); ?>
    </div>
    <div class='row'>
        <?= $this->render('partial_views/s4_4', ['model'=>$model]); ?>
    </div>
    <div class='row'>
        <?= $this->render('partial_views/s4_5', ['model'=>$model]); ?>
    </div>
    <div class='row'>
        <?= $this->render('partial_views/s4_6', ['model'=>$model]); ?>
    </div>
        </div>
    </div>
    
    
</div>

<?php
$js = <<< 'SCRIPT'
/* To initialize BS3 tooltips set this below */
    $('body').tooltip({selector: '[data-toggle="tooltip"]'});
/* To initialize BS3 popovers set this below */
    $('body').popover({selector: '[data-toggle="popover"]'});
    
SCRIPT;
// Register tooltip/popover initialization javascript
$this->registerJs($js);
?>

<?php
$js_path = \Yii::getAlias('@web');

$this->registerJsFile($js_path . '/js/cpf/cpf-prep.js', ['depends' => [\yii\web\JqueryAsset::className(), yii\bootstrap\BootstrapAsset::className()] ]);

?>    

