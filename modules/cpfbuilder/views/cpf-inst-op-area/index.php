<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\cpfbuilder\models\CpfInstOpAreaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app/cpf', 'Cpf Inst Op Areas');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cpf-inst-op-area-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app/cpf', 'Create Cpf Inst Op Area'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'installation_id',
            'year',
            'user_id',
            'name',
            // 'duration',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
