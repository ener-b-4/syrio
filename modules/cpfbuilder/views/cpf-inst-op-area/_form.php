<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\cpfbuilder\models\CpfInstOpArea */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cpf-inst-op-area-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'installation_id')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'year')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'user_id')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder'=>\Yii::t('app', 'area name')]) ?>

    <?= $form->field($model, 'duration')->textInput(['maxlength' => 5, 'placeholder'=>\Yii::t('app', 'duration in months')]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Add {evt}', ['evt'=>'']) : ucfirst(Yii::t('app', 'modify {evt}', ['evt'=>''])), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
