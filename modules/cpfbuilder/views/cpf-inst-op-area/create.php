<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\cpfbuilder\models\CpfInstOpArea */

$installation = \app\modules\cpfbuilder\models\CpfInstallations::find()
        ->where([
            'year' => $model->year,
            'user_id' => $model->user_id,
            'ident' => $model->installation_id
        ])->one();

$this->title = ucfirst(Yii::t('app/cpf', 'operation area')) . ' ' . $model->installation->ident;


//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Cpf Installations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'CPF sessions', 'url' => ['/cpfbuilder/cpf-sessions/index', 
    ]];
$this->params['breadcrumbs'][] = ['label' => $model->year, 'url' => ['/cpfbuilder/cpf-sessions/view', 
    'year' => $model->year, 'user_id'=>$model->user_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Add {evt}', ['evt'=>'']);
?>
<div class="cpf-inst-op-area-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
