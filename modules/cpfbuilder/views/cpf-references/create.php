<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\cpfbuilder\models\CpfReferences */

$this->title = \Yii::t('app', 'Register {evt}', [
    'evt'=>ucfirst(\Yii::t('app', 'document'))
]);
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cpf-references-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
