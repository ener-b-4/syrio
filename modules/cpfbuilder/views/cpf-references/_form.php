<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\cpfbuilder\models\CpfReferences */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cpf-references-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id')->hiddenInput()->label('', ['class'=>'hiddenBlock']) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder'=>\Yii::t('app', 'document title')]) ?>

    <?= $form->field($model, 'url')->textInput(['maxlength' => true, 'placeholder'=>\Yii::t('app', 'document url (if any)')]) ?>

    <?= $form->field($model, '_ref')->hiddenInput()->label('', ['class'=>'hiddenBlock']) ?>
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
