<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\cpfbuilder\models\CpfReferencesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = ucfirst(Yii::t('app', 'documents'));
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cpf-references-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Add {evt}', [
            'evt' => Yii::t('app', 'document')
        ]), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            'url:url',

            [
                'label' => ucfirst(\Yii::t('app', 'referenced in')),
                'class' => yii\grid\DataColumn::className(),
                'content' => function ($model, $key, $index, $column) {
                    return \Yii::t('app', '{evt, plural,=0{not referenced} =1{# Annual Report} other{# Annual Reports}}', ['evt'=>$model->getCpfs()->count()]);
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
                'buttons' => [
                    'delete' => function ($url, $model, $key) {
                                    if (!isset($model->cpfs) || $model->getCpfs()->count()==0) {
                                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                            'title'=>\Yii::t('app', 'Delete')
                                        ]);
                                    }
                                }
                ],
            ],
        ],
    ]); ?>

</div>
