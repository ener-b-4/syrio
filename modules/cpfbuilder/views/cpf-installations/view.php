<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\cpfbuilder\models\CpfInstallations */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Cpf Installations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cpf-installations-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'year',
            'user_id',
            'type',
            'ident',
            'tof',
            'n_beds',
            'lat',
            'lon',
            'decom_type',
            'syrio_id',
        ],
    ]) ?>

</div>
