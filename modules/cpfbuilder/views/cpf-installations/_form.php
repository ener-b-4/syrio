<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use app\modules\cpfbuilder\models\CpfTypeOfFluid;

/* @var $this yii\web\View */
/* @var $model app\modules\cpfbuilder\models\CpfInstallations */
/* @var $form yii\widgets\ActiveForm */

$decomTypes = [
    1=>\Yii::t('app','Temporary'),
    2 =>\Yii::t('app','Permanent')
    ];

$tofs =\yii\helpers\ArrayHelper::map(CpfTypeOfFluid::find()->all(), 'type', 'name');

foreach ($tofs as $key=>$value) {
    $tofs[$key] = \Yii::t('app',$value);
}

    if (isset($model->syrio_id)) {
        ?>
<div class="col-lg-12 alert alert-warning">
    <?= Yii::t('app', 'This installation has at least one incident recorded over the reporting period.') ?>
</div>
        <?php
    };
?>

<div class="cpf-installations-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'type')->textInput(['maxlength' => true, 'disabled' => 'true']) ?>

    <?= $form->field($model, 'ident')->textInput(['maxlength' => true, 'disabled' => 'true']) ?>

    <?= $form->field($model, 'tof')
            ->dropDownList($tofs, [
        'prompt' => Yii::t('app', '(not set)')
    ]) ?>

    <?= $form->field($model, 'n_beds')->textInput([]) ?>

    <?= $form->field($model, 'lat')->textInput([
    ]) ?>

    <?= $form->field($model, 'lon')->textInput() ?>

    <?= $form->field($model, 'decom_type')->
         dropDownList($decomTypes, ['prompt'=>Yii::t('app', '(none)')])
            ->hint(Yii::t('app','Leave empty in case of an operational installation'))?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
