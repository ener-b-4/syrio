<?php

/**
 * Description of cpf_sessions_view_help
 *
 * @author vamanbo
 */

/* @var cpf_reports_url string */
/* @var cpf_assess_url string */

?>

<div class="lead">
    <?php
    $s=\Yii::t('app', 'Description'); 
    echo $s;
    ?>
</div>
<p>
    <b>Editorul de rapoarte anuale</b> permite construirea raportului de sinteză ce trebuie înaintat Comisiei Europene conform Directivei 2013/30/UE.
</p>
<p>
    SyRIO oferă asistentă prin <b>extragerea informatiilor utile</b> din 
    <a href="<?=$cpf_reports_url?>" title="Afişează" data-toggle="tootltip" content="content">rapoartele de incident ale operatorilor</a> precum şi prin 
    <b>agregarea</b> informaţiilor continute în 
    <a href="<?=$cpf_assess_url?>" title="Afişează" data-toggle="tootltip" content="content">rapoartele individuale de evaluare în termenii Sectiunii 4 a CPF.</a></em> 
</p>
<p>
    Câmpurile raportului sunt pre-populate cu informaţiile extrase automat.
</p>
<p>
    În plus, <b>o serie de informaţii adiţionale trebuiesc completate de către dumneavoastră</b> (de ex. zona şi durata de operare în cazul unei instalaţii mobile).
</p>

<div id='info-wrapper' data-toggle="collapse" data-target='#info-list'>
    <div>
        <span class='text-info'><b>Informaţia extrasă automat</b><span id='chevron1' class='glyphicon glyphicon-chevron-down pull-right'></span></span>
        <div id='info-list' class='collapse sy_pad_top_6'>
            <dl>
                <dt>Secţiunea 1.</dt>
                <dd>Automat din datele utilizator.</dd>
            </dl>
            <dl>
                <dt>Secţiunea 2.</dt>
                <dd>Automat, din rapoartele de incident (CRF) din decursul anului de raportare.</dd>
                <dd>SyRIO populează (parţial!) <em>Tabelele 2.1, 2.2.a, 2.3.</em></dd>
            </dl>
            <dl>
                <dt>Secţiunea 4.</dt>
                <dd><b><em>4.1. Date referitoare la incidente</em></b></dd>
                <dd>Automat, din incidentele <span class=text-danger>raportate</span> şi <span class=text-danger>evaluate din punct de vedere al accidentului major</span> (CRF-uri) din decursul anului de raportare.</dd>
                <dd><b><em>4.2. Anexa IX Categorii de incidente</em></b></dd>
                <dd>Automat, din <span class=text-danger>incidentele evaluate în termenii Secţiunii 4 a CPF</span>.</dd>
                <dd><b><em>4.3. Numarul total de accidente mortale/vătămari</em></b></dd>
                <dd>Automat, din <span class=text-danger>incidentele evaluate în termenii Secţiunii 4 a CPF</span>.</dd>
                <dd><b><em>4.4. Defecţiuni ale elementelor critice de sigurantă şi de mediu (SECE)</em></b></dd>
                <dd>Automat, din <span class=text-danger>incidentele evaluate în termenii Secţiunii 4 a CPF</span>.</dd>
                <dd><b><em>4.5. Cauze directe şi de bază ale incidentelor majore</em></b></dd>
                <dd>Automat, din <span class=text-danger>incidentele evaluate în termenii Secţiunii 4 a CPF</span>.</dd>
            </dl>
        </div>
    </div>
</div>

<?php if (!$readonly) { ?>

<div id='info-wrapper' class="sy_pad_top_6" data-toggle="collapse" data-target='#how-to-list'>
    <div>
        <span class='text-info'><b>Instrucţiuni...</b><span id='chevron2' class='glyphicon glyphicon-chevron-down pull-right'></span></span>
        <div id='how-to-list' class='collapse sy_pad_top_6'>
            <dl>
                <dt>Actualizaţi</dt>
                <dd>Apasă butonul <mark>Actualizează {x}</mark> <br/><small class="text-muted">(unde {x} depinde de informaţia ce se doreşte a fi actualizată)</small><br/><br/></dd>
                <dd>Următoarele opţiuni sunt disponibile:</dd>
                <dd>
                    <ul class="list-unstyled">
                        <li><em>Actualizează instalaţiile</em></li>
                        <li><small>Actualizează informaţiile din <b>Secţiunea 2. Instalaţii</b></small><br/><br/></li>
                        <li><em>Actualizează datele</em></li>
                        <li><small>Actualizează informaţiile din <b>Secţiunea 4. Date referitoare la incidente şi executarea operaţiunilor offshore</b></small><br/></li>
                    </ul>
                </dd>
                <dt>Adaugă/Modifică informaţiile adiţionale</dt>
                <dd>Apăsaţi butoanele 
                    <span class="text-info">
                        <mark><span class="glyphicon glyphicon-plus"></span>
                        <mark><span class="glyphicon glyphicon-pencil"></span>
                        <mark><span class="glyphicon glyphicon-remove"></span></mark> 
                    </span>
                    acolo unde sunt disponibile.
                </dd>
            </dl>
        </div>
    </div>
</div>

<?php } ?>