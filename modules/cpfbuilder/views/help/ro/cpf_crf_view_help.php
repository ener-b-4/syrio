<?php

/**
 * Description of cpf_crf_view_help
 *
 * @author vamanbo
 */

?>

<div class="lead">
    <?php
    $s=\Yii::t('app', 'Description'); 
    echo $s;
    ?>
</div>
<p>
    În partea dreaptă este reprodusă <em>Secţiunea 4 a raportului anual (CPF)</em>.
</p>
<p>
    SyRIO oferă o <em>analiză preliminară</em> a raportului de eveniment curent (CRF); rezultatele sunt prezentate în coloana <kbd>SyRIO</kbd>.
</p>
<p>
    Pornind de la caracteristicile raportate (CRF) SyRIO realizează o clasificare a evenimentului pe baza <b>atributelor cuantificabile</b> ale acestuia şi ale unui set de <b>ipoteze</b>.
    Plasaţi cursorul deasupra <kbd>?</kbd> pentru a vedea ipoteza corespunzătoare fiecărui atribut cuantificabil.
</p>
<div class="lead sy_pad_top_18">
    'Sarcinile' dumneavoastră
</div>
<ul>
    <li>
        <span class="text-danger"><b> verificaţi rezultatele obţinute de SyRIO</b></span> şi 
        <em class="text-success">'aprobaţi'</em> sau <em class="text-danger">'rejectaţi'</em> (bifaţi căsuta din dreptul categoriei selectate - coloana <kbd>You</kbd>) şi 
    </li>
    <li>
        <span class="text-info">completaţi clasificarea evenimentului</span>, pe baza <em>informaţiilor necuantificabile</em>  ale raportului de incident (cum ar fi secţiunile descriptive).
    </li>
    <li>
        <kbd>Finalizaţi</kbd> evaluarea pentru a marca incidentul ca fiind <em>gata de a fi luat în considerare în raportul anual (CPF)</em> 
    </li>
</ul>

<div class="lead sy_pad_top_18 text-warning">
    'Notă' 
</div>
<p>
    <span class="text-warning">Doar atributele aprobate de dumneavoastră</span> 
    vor fi luate în considerare în faza de 
    <a href="<?=\Yii::$app->urlManager->createUrl(['/cpfbuilder/cpf-sessions'])?>">Editare a raportului anual</a>!
</p>
