<?php
/**
 * Description of cpf_sessions_index_help
 *
 * @author vamanbo
 */
?>

<div class="lead">
    <?php
    $s=\Yii::t('app', 'Description'); 
    echo $s;
    ?>
</div>
<p>
    În partea dreapta aveti lista Rapoartelor Anuale.
    <br/>
    Un singur raport anual este disponibil pentru fiecare an.
</p>

    <div id='info-wrapper' data-toggle="collapse" data-target='#how-to-list'>
        <div>
            <span class='text-info'><b><?= \Yii::t('app', 'How to') ?></b><span id='chevron2' class='glyphicon glyphicon-chevron-down pull-right'></span></span>
            <div id='how-to-list' class='collapse sy_pad_top_6'>
                <dl>
                    <dt>Crează raport anual<dt>
                    <dd>Apasă <mark>Crează raport anual</mark></dd>
                    <dt>Vizualizează / continuă editarea<dt>
                    <dd>Apasă <mark><span class="glyphicon glyphicon-eye-open"></span></mark> în dreptul anului.</dd>
                    <dt>Şterge raport<dt>
                    <dd>Apasă <mark><span class="glyphicon glyphicon-trash"></span></mark> în dreptul anului.</dd>
                </dl>
            </div>
        </div>
    </div>




<div class="lead sy_pad_top_18">
    'Note' 
</div>
<ol>
    <li>Editarea Raportului Anual este un <b>proces incremental</b>. Cu alte cuvinte, editarea raportului anual poate fi facuta <em>in timpul anului raportat</em>.</li>
    <li>Ca o consecinta, informatia continuta in raportul anual se poate modifica pe durata editarii raportului (de exemplu, in momentul in care un nou incident este raportat de catre operatori).</li>
    <li>O parte din informatia continuta in raportul anual este calculata automat pe baza evaluarii individuale a rapoartelor de incident.</li>
    <li>Asadar, editarea raportului anual necesita <em>analiza preliminara individuala a incidentelor raportate</em> (in termenii Sectiunii 4 a CPF).</li>
    <li>Utilizati <mark>Actualizeaza</mark> in sectiunea de editare a Raportului Anual pentru a-l mentine sincronizat cu eventualele modificari ale listei incidentelor raportate / evaluate dpdv al Sectiunii 4.</li>
</ol>
