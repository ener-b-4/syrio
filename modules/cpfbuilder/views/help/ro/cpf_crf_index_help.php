<?php
/* @var $cpf_drafting_url string */

?>

<div class="lead">
    <?php
    echo 'Descriere';
    ?>
</div>
<p>
    <?php
    $s = 'Pentru a fi luat în considerare în faza de <a href = "' . $cpf_drafting_url . '">editare a raportului anual (CPF)</a> fiecare incident (clasificat în prealabil din punct de vedere al accidentului major) trebuie analizat şi clasificat în termenii definiţi de Secţiunea 4 a CPF.'
            . '<br/><br/>'
            . 'În SYRIO, acest proces poartă denumirea de <b>Evaluarea incidentului în termenii Secţiunii 4 a CPF</b>.'
            . '<br/><br/>'
            . 'Odată încheiat acest proces, incidentul trebuie marcat ca <kbd>Finalizat</kbd>.'
            . '<br/><br/>'
            . '<span class="text-warning">' 
            . 'Doar incidentele marcate - în această fază - ca <kbd>Finalizat</kbd> sunt luate în considerare în faza de editare a CPF!' 
            . '</span><br/><br/>'
            . 'În partea dreaptă este afişată lista incidentelor, împreuna cu starea acestora din punct de vedere al evaluarii în termenii Secţiunii 4 a CPF.'
            . '<br/><br/>';
    
    if ($_canEdit) {
            $s .= '<span class="text-info">Ca <em>bună practică</em>, <span class="text-danger"><span class="glyphicon glyphicon-refresh"></span>Actualizaţi</span> lista periodic, pentru a o menţine sincronizată cu eventualele modificări in lista incidentelor inregistrate de către operatori.' . '</span>'
            . '<br/><br/>';
    };
    echo $s;

    ?>
</p>

<?php if ($_canEdit) { ?>

<div id='info-wrapper' data-toggle="collapse" data-target='#how-to-list'>
    <div>
        <span class='text-info'><b>Indicaţii</b><span id='chevron2' class='glyphicon glyphicon-chevron-down pull-right'></span></span>
        <div id='how-to-list' class='collapse sy_pad_top_6'>
            <dl>
                <dt>Actualizare listă incidente<dt>
                <dd>Apasă <kbd><span class="glyphicon glyphicon-refresh"></span> Actualizaţi</kbd></dd>
                <dt>Vizualizează evaluare<dt>
                <dd>Apasă <kbd><span class="glyphicon glyphicon-eye-open"></span></kbd> în dreptul incidentului.</dd>
                <dt>Modifică evaluare<dt>
                <dd>Apasă <kbd><span class="glyphicon glyphicon-pencil"></span></kbd> în dreptul incidentului.</dd>
                <dt>Şterge evaluare<dt>
                <dd>Apasă <kbd><span class="glyphicon glyphicon-trash"></span></kbd> în dreptul incidentului.</dd>
            </dl>
        </div>
    </div>
</div>

<?php } ?>
