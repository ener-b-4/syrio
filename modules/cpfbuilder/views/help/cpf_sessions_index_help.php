<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

/**
 * Description of cpf_sessions_index_help
 *
 * @author vamanbo
 */
?>

<div class="lead">
    <?php
    $s=\Yii::t('app', 'Description'); 
    echo $s;
    ?>
</div>
<p>
    <?php
    $s = 'The following is the list of your Annual Reports.<br/>'
            . 'Only one Annual Report per year is available.';
    echo $s;
    ?>
</p>

    <div id='info-wrapper' data-toggle="collapse" data-target='#how-to-list'>
        <div>
            <span class='text-info'><b><?= \Yii::t('app', 'How to') ?></b><span id='chevron2' class='glyphicon glyphicon-chevron-down pull-right'></span></span>
            <div id='how-to-list' class='collapse sy_pad_top_6'>
                <dl>
                    <dt>Create a new Annual Report<dt>
                    <dd>Click <mark>Create Annual Report</mark></dd>
                    <dt>View / continue drafting<dt>
                    <dd>Click <mark><span class="glyphicon glyphicon-eye-open"></span></mark> on the row corresponding to the desired year.</dd>
                    <dt>Delete report<dt>
                    <dd>Click <mark><span class="glyphicon glyphicon-trash"></span></mark> on the row corresponding to the desired year.</dd>
                </dl>
            </div>
        </div>
    </div>




<div class="lead sy_pad_top_18">
    <?php
    $s=\Yii::t('app', 'Notes'); 
    echo $s;
    ?>
</div>
<ol>
    <li>SyRIO Annual Report drafting is an <b>incremental process</b>. In other words, drafting the Annual Report may be done <em>during the reporting year</em>.</li>
    <li>As a consequence, the information in the Annual Report may change during the drafting period (ex. new incident is reported by the operators).</li>
    <li>Part of the information of the Annual Report is inferred automatically from the individual incident reports.</li>
    <li>Hence, Annual Report drafting requires an <em>individual pre-assessment of the reported incidents</em> (in terms of Section 4 of the CPF)</li>
    <li>Use <mark>Refresh</mark> in the Annual Report editor to keep it up to date with the eventual changes in the operators' incident reports.</li>                    
</ol>
