<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

/* @var $cpf_drafting_url string */
/* @var $_canEdit bool */

?>

<div class="lead">
    <?php
    echo 'Description';
    ?>
</div>
<p>
    <?php
    $s = 'For being accounted in the <a href = "' . $cpf_drafting_url . '">drafting of the CPF annual report</a>, each incident (previously assessed in terms of Major Incident) must be analysed in terms of qualifying in one or more failure categories that are part of the CPF\'s Section 4.';
    echo $s . '<br/>' . '<br/>';

    $s = 'In SYRIO, this process is referred to as the <b>Assessment in terms of CPF Section 4</b> of an incident.';
    echo $s . '<br/>' . '<br/>';

    $s = 'Once done, the incident must be marked as ready, i.e. <kbd>Finalized</kbd>.';
    echo $s . '<br/>' . '<br/>';

    $s = 'Only the incidents who\'s CPF preparation phase is <kbd>Finalized</kbd> are taken into account in the CPF drafting phase!';
    echo '<span class="text-warning">' . $s . '</span>' . '<br/>' . '<br/>';

    $s = 'On the right-hand side you may find the list of incidents and their status in respect to the CPF Section 4 assessment.';
    echo $s . '<br/>' . '<br/>';

    if ($_canEdit) {
    $s = '<span class="text-info">As <em>best practice</em>, <span class="text-danger"><span class="glyphicon glyphicon-refresh"></span>Refresh</span> the list periodically to keep up to date with any changes in the list of the incidents.' . '</span>';
    echo $s . '<br/><br/>';
    }
    
    ?>
</p>

<?php if ($_canEdit) { ?>
<div id='info-wrapper' data-toggle="collapse" data-target='#how-to-list'>
    <div>
        <span class='text-info'><b><?= \Yii::t('app', 'How to') ?></b><span id='chevron2' class='glyphicon glyphicon-chevron-down pull-right'></span></span>
        <div id='how-to-list' class='collapse sy_pad_top_6'>
            <dl>
                <dt>Refresh the list of incidents<dt>
                <dd>Click <kbd><span class="glyphicon glyphicon-refresh"></span> Refresh</kbd></dd>
                <dt>View assessment<dt>
                <dd>Click <kbd><span class="glyphicon glyphicon-eye-open"></span></kbd> on the row corresponding to the incident.</dd>
                <dt>Modify assessment<dt>
                <dd>Click <kbd><span class="glyphicon glyphicon-pencil"></span></kbd> on the row corresponding to the incident.</dd>
                <dt>Delete assessment<dt>
                <dd>Click <kbd><span class="glyphicon glyphicon-trash"></span></kbd> on the row corresponding to the incident.</dd>
            </dl>
        </div>
    </div>
</div>

<?php } ?>


