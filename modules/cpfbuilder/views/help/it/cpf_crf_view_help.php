<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

/**
 * Description of cpf_crf_view_help
 *
 * @author vamanbo
 */

?>

<div class="lead">
    <?php
    $s=\Yii::t('app', 'Description'); 
    echo $s;
    ?>
</div>
<p>
    La sezione destra riproduce il contenuto della <em>Relazione annuale del CPF Sezione 4</em>.
</p>
<p>
    SyRIO fornisce una <em>pre-valutazione</em> automatica dell'incidente e presenta i risultati (la colonna <kbd>SyRIO</kbd>).
</p>
<p>
    Sulla base delle caratteristiche degli incidenti, SyRIO applica <b>una serie di ipotesi</b> e fa una categorizzazione della manifestazione in base agli <b>attributi desumibili</b>.
    Passa il <kbd>?</kbd> per ottenere le ipotesi SyRIO per qualsiasi attributo dedotto automaticamente.            
</p>
<div class="lead sy_pad_top_18">
    'I tuoi compiti'
</div>
<ul>
    <li>
        <span class="text-danger"><b> doppia verifica SyRIO risultati</b></span> e 
        <em class="text-success">'concordare'</em> o <em class="text-danger">'non concordare'</em> (barrare le categorie sulla <kbd>Tua</kbd> colonna) e 
    </li>
    <li>
        <span class="text-info">aumentare la categorizzazione del caso</span>, basato su <em>attributi non desumibili nella Relazione dell'Incidente</em> (per esempio. informazioni descrittive).
    </li>
    <li>
        <kbd>Finalizza</kbd> la valutazione al fine di marcare l'incidente come <em>'pronto per essere contabilizzati nella Relazione Annuale del CPF'</em>                    
    </li>
</ul>

<div class="lead sy_pad_top_18 text-warning">
    'Notare che' 
</div>
<p>
    <span class="text-warning">Solo i vostro input</span> 
   saranno presi in considerazione quando si considera questo caso durante la
    <a href="<?=\Yii::$app->urlManager->createUrl(['/cpfbuilder/cpf-sessions'])?>">Rapporto Relazione Annuale del CPF</a>!
</p>
