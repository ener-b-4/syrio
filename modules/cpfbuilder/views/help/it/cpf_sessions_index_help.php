<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

/**
 * Description of cpf_sessions_index_help
 *
 * @author vamanbo
 */
?>

<div class="lead">
    <?php
    $s=\Yii::t('app', 'Description'); 
    echo $s;
    ?>
</div>
<p>
    <?php
    $s = 'Di seguito l\'elenco dei vostro Relazioni Annuali.<br/>'
            . 'Solo una relazione annuale per anno è disponibile .';
    echo $s;
    ?>
</p>

    <div id='info-wrapper' data-toggle="collapse" data-target='#how-to-list'>
        <div>
            <span class='text-info'><b><?= \Yii::t('app', 'How to') ?></b><span id='chevron2' class='glyphicon glyphicon-chevron-down pull-right'></span></span>
            <div id='how-to-list' class='collapse sy_pad_top_6'>
                <dl>
                    <dt>Creare una nuova Relazione Annuale<dt>
                    <dd>Click <mark>Creare Relazione Annuale</mark></dd>
                    <dt>Vedere / continuare la Redazione<dt>
                    <dd>Click <mark><span class="glyphicon glyphicon-eye-open"></span></mark> nella riga corrispondente all'anno desiderato.</dd>
                    <dt>Modificare anno di riferimento<dt>
                    <dd>Click <mark><span class="glyphicon glyphicon-pencil"></span></mark> nella riga corrispondente all'anno desiderato.</dd>
                    <dt>Cancella Relazione<dt>
                    <dd>Click <mark><span class="glyphicon glyphicon-trash"></span></mark> nella riga corrispondente all'anno desiderato.</dd>
                </dl>
            </div>
        </div>
    </div>




<div class="lead sy_pad_top_18">
    <?php
    $s=\Yii::t('app', 'Notes'); 
    echo $s;
    ?>
</div>
<ol>
    <li>SyRIO relazione annuale redazione è un <b>processo incrementale</b>. In altre parole, la stesura della relazione annuale può essere fatto <em>durante l'anno di riferimento</em>.</li>
	<li>Di conseguenza , le informazioni contenute nella Relazione Annuale può cambiare durante il periodo di redazione (as esempio: Nuovo incidente è segnalato dagli operatori) .</li>
	<li>Parte delle informazioni della relazione annuale viene dedotta automaticamente dai singoli rapporti sugli incidenti.</li>
	<li>Quindi , Annual Report redazione richiede un <em>individuo pre- valutazione degli incidenti segnalati</em> (in termini di sezione 4 della CPF)</li>
	<li>Utilizzare <mark>Aggiorna</mark> nell'editor relazione annuale per mantenerlo aggiornato con le eventuali variazioni dei rapporti sugli incidenti degli operatori.</li>   
</ol>
