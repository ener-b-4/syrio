<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

/**
 * Description of cpf_sessions_view_help
 *
 * @author vamanbo
 */

/* @var cpf_reports_url string */
/* @var cpf_assess_url string */

?>

<div class="lead">
    <?php
    $s=\Yii::t('app', 'Description'); 
    echo $s;
    ?>
</div>
<p>
    Il <b>Rapporto Annuale Editor</b> permette di costruire la relazione da presentare alla Commissione, come previsto dalla Direttiva 2013/30/EU.
</p>
<p>
    SyRIO fornisce assistenza nella costruzione del rapporto <b> desumere le informazioni</b> disponibile in
	
    <a href="<?=$cpf_reports_url?>" title="Click to open" data-toggle="tootltip" content="content">Rapporti sugli incidenti degli Operatori</a> ed anche
    <b>aggregando</b> le informazioni nel <em>personale <a href="<?=$cpf_assess_url?>" title="Click to open" data-toggle="tootltip" content="content">valutazione incidente in termini di sezione 4 del CPF.</a></em> 
</p>
<p>
    I campi corrispondenti della relazione sono pre-popolati dalle informazioni estratte dal codice.
</p>
<p>
    Inoltre, è <b>necessario fornire le ulteriori informazioni</b> che non possono essere estratti dei CRF incidenti (ad esempio area operativa e durata per ogni MODU).
</p>

<div id='info-wrapper' data-toggle="collapse" data-target='#info-list'>
    <div>
        <span class='text-info'><b>Informazioni desumibili</b><span id='chevron1' class='glyphicon glyphicon-chevron-down pull-right'></span></span>
        <div id='info-list' class='collapse sy_pad_top_6'>
            <dl>
                <dt>Sezione 1.</dt>
                <dd>Automaticamente dal tuo credenziali dell'utente.</dd>
            </dl>
            <dl>
                <dt>Sezione 2.</dt>
                <dd>Automaticamente dai tutti gli incidenti segnalati (CRF) nel corso dell'anno di riferimento.</dd>
                <dd>SyRIO popola automaticamente (in parte!) <em>Tabelle 2.1, 2.2.a, 2.3.</em></dd>
            </dl>
            <dl>
                <dt>Sezione 4.</dt>
                <dd><b><em>4.1. Dati dell'incidente</em></b></dd>
                <dd>Automaticamente dal tutto il <span class=text-danger>riportate</span> e <span class=text-danger>valutate in termini di Incidenti Rilevante</span> (CRFs) nel corso dell'anno di riferimento.</dd>
                <dd><b><em>4.2. Allegato IX Categorie degli incidenti</em></b></dd>
                <dd>Automaticamente dalla <span class=text-danger>incidenti che è stato valutato in termini della Sezione 4</span>.</dd>
                <dd><b><em>4.3. Numero totale di morti e feriti</em></b></dd>
                <dd>Automaticamente dalla <span class=text-danger>incidenti che è stato valutato in termini della Sezione 4</span>.</dd>
                <dd><b><em>4.4. I guasti di SECE</em></b></dd>
                <dd>Automaticamente dalla <span class=text-danger>incidenti che è stato valutato in termini della Sezione 4</span>.</dd>
                <dd><b><em>4.5. Cause dirette e sottostanti ai gravi incidenti</em></b></dd>
                <dd>Automaticamente dalla <span class=text-danger>incidenti che è stato valutato in termini della Sezione 4</span>.</dd>
            </dl>
        </div>
    </div>
</div>


<div id='info-wrapper' class="sy_pad_top_6" data-toggle="collapse" data-target='#how-to-list'>
    <div>
        <span class='text-info'><b>Come...</b><span id='chevron2' class='glyphicon glyphicon-chevron-down pull-right'></span></span>
        <div id='how-to-list' class='collapse sy_pad_top_6'>
            <dl>
                <dt>Aggiorna informazioni</dt>
                <dd>Click sul pulsanti <mark>Aggiorna {x}</mark> <br/><small class="text-muted"> (Dove {x} dipende dai dati di essere aggiornati)</small><br/><br/></dd>
                <dd>Di seguito sono disponibili:</dd>
                <dd>
                    <ul class="list-unstyled">
                        <li><em>Aggiorna impianti</em></li>
                        <li><small>Aggiorna le informazioni in <b>Sezione 2. Impianti</b></small><br/><br/></li>
                        <li><em>Aggiorna dati</em></li>
                        <li><small>Aggiorna le informazioni in <b>Sezione 4. Incidente dei dati e prestazioni di operazioni offshore</b></small><br/></li>
                    </ul>
                </dd>
                <dt>Aggiungi/Modifica informazioni aggiuntive</dt>
                <dd>Click su 
                    <span class="text-info">
                        <mark><span class="glyphicon glyphicon-plus"></span>
                        <mark><span class="glyphicon glyphicon-pencil"></span>
                        <mark><span class="glyphicon glyphicon-remove"></span></mark> 
                    </span>
                    pulsanti , se disponibili.
                </dd>
            </dl>
        </div>
    </div>
</div>

