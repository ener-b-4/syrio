<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */


/* @var $cpf_drafting_url string */

?>

<div class="lead">
    <?php
    echo 'Descrizione';
    ?>
</div>
<p>
    <?php
    $s = 'For being accounted in the <a href = "' . $cpf_drafting_url . '">drafting of the CPF annual report</a>, each incident (previously assessed in terms of Major Incident) must be analysed in terms of qualifying in one or more failure categories that are part of the CPF\'s Section 4.';
	
	
	$s = 'Per essere contabilizzato nella <a href = "' . $cpf_drafting_url . '"> stesura della Relazione Annuale del CPF</a>,, ogni incidente (precedentemente valutato in termini di incidenti di rilievo ) deve essere analizzato in termini di qualificazione in una o più categorie di guasto che fanno parte della Sezione 4. del CPF';
	
    echo $s . '<br/>' . '<br/>';

    $s = 'In SYRIO, questo processo viene indicato come la <b> Valutazione in termini della Sezione 4 del CPF </b> di un incidente.';
    echo $s . '<br/>' . '<br/>';

    $s = 'Una volta fatto, l\'incidente deve essere contrassegnato come pronto, vale a dire <kbd>Finalizzato</kbd>.';
    echo $s . '<br/>' . '<br/>';

    $s = 'Solo gli incidenti che sono <kbd>Finalizzati</kbd> saranno presi in considerazione nella fase di stesura del CPF!';
	
    echo '<span class="text-warning">' . $s . '</span>' . '<br/>' . '<br/>';

    $s = 'Sul lato destro è possibile trovare l\'elenco degli incidenti e il loro stato in relazione alla valutazione Sezione 4 del CPF.';
    echo $s . '<br/>' . '<br/>';

    $s = '<span class="text-info">Come <em>best practice</em>, <span class="text-danger"><span class="glyphicon glyphicon-refresh"></span>Aggiornare l\'elenco periodicamente per tenersi aggiornati con le eventuali modifiche nella lista degli incidenti.' . '</span>';
	
    echo $s . '<br/><br/>';

    ?>
</p>

<div id='info-wrapper' data-toggle="collapse" data-target='#how-to-list'>
    <div>
        <span class='text-info'><b><?= \Yii::t('app', 'How to') ?></b><span id='chevron2' class='glyphicon glyphicon-chevron-down pull-right'></span></span>
        <div id='how-to-list' class='collapse sy_pad_top_6'>
            <dl>
                <dt>Aggiornare l'elenco degli incidenti<dt>
                <dd>Click <kbd><span class="glyphicon glyphicon-refresh"></span> Aggiorna</kbd></dd>
                <dt>Vedere la valutazione<dt>
                <dd>Click <kbd><span class="glyphicon glyphicon-eye-open"></span></kbd> sulla riga corrispondente alla incidente.</dd>
                <dt>Modifica della valutazione<dt>
                <dd>Click <kbd><span class="glyphicon glyphicon-pencil"></span></kbd> sulla riga corrispondente alla incidente.</dd>
                <dt>Cancella la valutazione<dt>
                <dd>Click <kbd><span class="glyphicon glyphicon-trash"></span></kbd> sulla riga corrispondente alla incidente.</dd>
            </dl>
        </div>
    </div>
</div>


