<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

/**
 * Description of cpf_sessions_view_help
 *
 * @author vamanbo
 */

/* @var cpf_reports_url string */
/* @var cpf_assess_url string */

?>

<div class="lead">
    <?php
    $s=\Yii::t('app', 'Description'); 
    echo $s;
    ?>
</div>
<p>
    The <b>Annual Report editor</b> allows you to build the report to be submitted to the Commission, as required by Directive 2013/30/EU.
</p>
<p>
    SyRIO provides assistance in building the report by <b>inferring the information</b> available in the 
    <a href="<?=$cpf_reports_url?>" title="Click to open" data-toggle="tootltip" content="content">Operators' incident reports</a> and also by 
    <b>aggregating</b> the information in the <em>individual <a href="<?=$cpf_assess_url?>" title="Click to open" data-toggle="tootltip" content="content">incident assessment in terms of Section 4 of CPF.</a></em> 
</p>
<p>
    The correspondent fields of the report are pre-populated by the information extracted by the code.                     
</p>
<p>
    In addition, you <b>must provide the additional information</b> that cannot be extracted from the incident CRFs (e.g. operational area and duration for each MODU).
</p>

<div id='info-wrapper' data-toggle="collapse" data-target='#info-list'>
    <div>
        <span class='text-info'><b>Inferred information</b><span id='chevron1' class='glyphicon glyphicon-chevron-down pull-right'></span></span>
        <div id='info-list' class='collapse sy_pad_top_6'>
            <dl>
                <dt>Section 1.</dt>
                <dd>Automatically from your user credentials.</dd>
            </dl>
            <dl>
                <dt>Section 2.</dt>
                <dd>Automatically from the all the reported incidents (CRFs) over the reporting year.</dd>
                <dd>SyRIO automatically populates (partially!) <em>Tables 2.1, 2.2.a, 2.3.</em></dd>
            </dl>
            <dl>
                <dt>Section 4.</dt>
                <dd><b><em>4.1. Incident data</em></b></dd>
                <dd>Automatically from the all the <span class=text-danger>reported</span> and <span class=text-danger>assessed in terms of Major Accident</span> incidents (CRFs) over the reporting year.</dd>
                <dd><b><em>4.2. Annex IX Incident Categories</em></b></dd>
                <dd>Automatically from the <span class=text-danger>incidents that has been assessed in terms of Section 4</span>.</dd>
                <dd><b><em>4.3. Total number of fatalities and injuries</em></b></dd>
                <dd>Automatically from the <span class=text-danger>incidents that has been assessed in terms of Section 4</span>.</dd>
                <dd><b><em>4.4. Failures of SECE</em></b></dd>
                <dd>Automatically from the <span class=text-danger>incidents that has been assessed in terms of Section 4</span>.</dd>
                <dd><b><em>4.5. Direct and Underlying causes of major incidents</em></b></dd>
                <dd>Automatically from the <span class=text-danger>incidents that has been assessed in terms of Section 4</span>.</dd>
            </dl>
        </div>
    </div>
</div>

<?php if (!$readonly) { ?>
    
<div id='info-wrapper' class="sy_pad_top_6" data-toggle="collapse" data-target='#how-to-list'>
    <div>
        <span class='text-info'><b>How to...</b><span id='chevron2' class='glyphicon glyphicon-chevron-down pull-right'></span></span>
        <div id='how-to-list' class='collapse sy_pad_top_6'>
            <dl>
                <dt>Refresh information</dt>
                <dd>Click on <mark>Refresh {x}</mark> button <br/><small class="text-muted">(where {x} depends on the data to be refreshed)</small><br/><br/></dd>
                <dd>The following are available:</dd>
                <dd>
                    <ul class="list-unstyled">
                        <li><em>Refresh installations</em></li>
                        <li><small>Updates the information in <b>Section 2. Installations</b></small><br/><br/></li>
                        <li><em>Refresh data</em></li>
                        <li><small>Updates the information in <b>Section 4. Incident Data and Performance of Offshore Operations</b></small><br/></li>
                    </ul>
                </dd>
                <dt>Add/Edit additional information</dt>
                <dd>Click on 
                    <span class="text-info">
                        <mark><span class="glyphicon glyphicon-plus"></span>
                        <mark><span class="glyphicon glyphicon-pencil"></span>
                        <mark><span class="glyphicon glyphicon-remove"></span></mark> 
                    </span>
                    buttons where available.
                </dd>
            </dl>
        </div>
    </div>
</div>

<?php } ?>
