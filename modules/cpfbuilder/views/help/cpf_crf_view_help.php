<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

/**
 * Description of cpf_crf_view_help
 *
 * @author vamanbo
 */

?>

<div class="lead">
    <?php
    $s=\Yii::t('app', 'Description'); 
    echo $s;
    ?>
</div>
<p>
    The right-hand side section reproduces the content of the <em>CPF Annual Report's Section 4</em>.
</p>
<p>
    SyRIO provides an automatic <em>pre-assessment</em> of the incident and presents the findings (the <kbd>SyRIO</kbd> column).
</p>
<p>
    Based on the incident characteristics, SyRIO applies <b>a set of assumptions</b> and makes a categorization of the event based on the <b>inferable attributes</b>.
    Hover the <kbd>?</kbd> to get SyRIO assumptions for any automatically inferred attribute.                
</p>
<div class="lead sy_pad_top_18">
    'Your tasks'
</div>
<ul>
    <li>
        <span class="text-danger"><b> double-check SyRIO findings</b></span> and 
        <em class="text-success">'agree'</em> or <em class="text-danger">'disagree'</em> (tick the categories on the <kbd>You</kbd> column) and 
    </li>
    <li>
        <span class="text-info">augment the categorization of the case</span>, based on the <em>non-inferable attributes in the Incident Report</em> (e.g. descriptive information).
    </li>
    <li>
        <kbd>Finalize</kbd> the assessment in order to mark the incident as <em>'ready to be accounted in the Annual CPF report'</em>                    
    </li>
</ul>

<div class="lead sy_pad_top_18 text-warning">
    'Please note' 
</div>
<p>
    <span class="text-warning">Only your input</span> 
    will be taken into account when considering this case during the 
    <a href="<?=\Yii::$app->urlManager->createUrl(['/cpfbuilder/cpf-sessions'])?>">Annual CPF report drafting</a>!
</p>
