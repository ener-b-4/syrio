<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\cpfbuilder\models\CpfSection3 */
?>


<div class='row'>
    <div class='col-lg-12' id='remove_result'>
        
    </div>
</div>

<table class='table table-condensed'>
    <thead>
    <th>Title</th>
    <th>Url</th>
    </thead>
    
    <tbody>
<?php
    foreach($model->references as $ref) {
        ?>
        <tr>
            <td><?= $ref->name ?></td>
            <td><?= \app\models\shared\NullValueFormatter::Format($ref->url, \Yii::t('app', '(none)')) ?></td>
            <td>
                <div>
                    <input type='hidden' name=<?= Html::encode('rem_'.$ref->id) ?> /> 
                    <?php
                    $url = \Yii::$app->urlManager->createUrl('/cpfbuilder/cpf-section3/remove-reference');
                    ?>
                    <?= Html::button(\Yii::t('app', 'Remove'), ['class'=>'rem-btn btn btn-xs btn-danger', 'tag'=>$ref->id]) ?>
                </div>
            </td>
        </tr>
        <?php
    }

?>
    </tbody>
</table>


<?php

$url = \Yii::$app->urlManager->createUrl('/cpfbuilder/cpf-section3/remove-reference');

$js = <<<EOF
        var f = function(){
            \$('.rem-btn').each(function(index, value){
                \$(this).click(function(e){
                    e.stopPropagation();
                    e.preventDefault();
                    $.post(
                        '$url',
                        { 'id': $(this).attr('tag') },
                        function(data, status) {
                            console.log(status);
                            location.reload();
                        });
                        return false;
                    });
            });
        }();
           
EOF;

$this->registerJs($js, yii\web\View::POS_READY);
?>