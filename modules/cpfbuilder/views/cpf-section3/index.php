<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\cpfbuilder\models\CpfSection3Search */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app/cpf', 'Cpf Section3s');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cpf-section3-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app/cpf', 'Create Cpf Section3'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'year',
            'user_id',
            'n_offshore_inspections',
            'man_days',
            'n_inpeceted',
            // 'n_major_accidents',
            // 'n_env_concerns',
            // 's3_3_text:ntext',
            // 'description:ntext',
            // 'rationale:ntext',
            // 'expected_outcome:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
