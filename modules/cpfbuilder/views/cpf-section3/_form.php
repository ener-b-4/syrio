<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use kartik\form\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\cpfbuilder\models\CpfSection3 */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cpf-section3-form">

    <?php $form = ActiveForm::begin([
        'type'=>  ActiveForm::TYPE_INLINE
    ]); ?>


    <?= $form->field($model, 'year')->hiddenInput()->label(FALSE) ?>

    <?= $form->field($model, 'user_id')->hiddenInput()->label(FALSE) ?>

    <div class="col-lg-12">
        <strong class="text-info">3.1. <?= \Yii::t('app/cpf' ,'Inspections') ?></strong>
    </div>

    <div class="row sy_pad_bottom_18">
        <div class="col-md-3 text-right"><?= $model->attributeLabels()['n_offshore_inspections'] ?></div> 
        <div class="col-md-6">
            <?= $form->field($model, 'n_offshore_inspections')->textInput() ?>
        </div>
    </div>
    
    <div class="row sy_pad_bottom_18">
        <div class="col-md-3 text-right"><?= $model->attributeLabels()['man_days'] ?></em></small></div> 
        <div class="col-md-6">
            <?= $form->field($model, 'man_days')->textInput() ?>
        </div>
    </div>

    <div class="row sy_pad_bottom_18">
        <div class="col-md-3 text-right"><?= $model->attributeLabels()['n_inpeceted'] ?></div> 
        <div class="col-md-6">
            <?= $form->field($model, 'n_inpeceted')->textInput() ?>
        </div>
    </div>
    
    <div class="col-lg-12">
        <strong class="text-info">3.2. <?= \Yii::t('app/cpf', 'Investigations') ?></strong>
        <br/>
        <br/>
        <div class='sy_pad_bottom_18'>
            <p>
            <em class="text-muted sy_pad_bottom_18"><?= \Yii::t('app/cpf', 'Number and type of investigations performed during the reporting year') ?></em>
            </p>
        </div>
    </div>

    <div class="row sy_pad_bottom_18">
        <div class="col-md-3 text-right"><?= $model->attributeLabels()['n_major_accidents'] ?></div> 
        <div class="col-md-6">
            <?= $form->field($model, 'n_major_accidents')->textInput() ?>
        </div>
    </div>
    <div class="row sy_pad_bottom_18">
        <div class="col-md-3 text-right"><?= $model->attributeLabels()['n_env_concerns'] ?></div> 
        <div class="col-md-6">
            <?= $form->field($model, 'n_env_concerns')->textInput() ?>
        </div>
    </div>


    <div class="col-lg-12">
        <div class="row sy_pad_bottom_18">
            <div class="col-md-3">
                <strong class="text-info">3.3. <?= \Yii::t('app/cpf', 'Enforcement actions') ?></strong>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 's3_3_text')->textarea(['rows' => 6,
                    'style'=>'min-height: 150px; height:150px',
                    'placeholder'=>'add text'])->label(\Yii::t('app/cpf', 'Narrative')) ?>
            </div>
        </div>
    </div>

    <div class="col-lg-12">
        <strong class="text-info">3.4. <?= \Yii::t('app/cpf', 'Major changes in the offshore regulatory framework') ?></strong>
        <p>
        <em class="text-muted"><?= \Yii::t('app/cpf', 'Please describe any major changes in the offshore regulatory framework during the reporting period') ?></em>
        </p>
    </div>
    <div class="row sy_pad_bottom_18">
        <div class="col-lg-12" style="margin-left:12px; margin-top:12px;">
            <?= $form->field($model, 'rationale')->textarea(['rows' => 6])->label('description') ?>

            <?= $form->field($model, 'description')->textarea(['rows' => 6])->label('rationale') ?>

            <?= $form->field($model, 'expected_outcome')->textarea(['rows' => 6])->label('expected outcome') ?>
        </div>
    </div>

    <div class='col-lg-12 sy_pad_bottom_18'>
        <p><strong><?= \Yii::t('app/cpf', 'References') ?></strong></p>
        <p>
            <?php
                    $url =\Yii::$app->urlManager->createUrl([ '/cpfbuilder/cpf-section3/references', 'year'=>$model->year, 'user_id'=>$model->user_id]);
                    echo Html::a('<span class="glyphicon glyphicon-plus"></span>', $url, [
                        'class' => 'btn btn-xs btn-primary',
                        'title' => \Yii::t('app', 'Add {evt}', ['evt'=>strtolower(\Yii::t('app/cpf', 'References'))])
                    ])
            ?>
            <em class="text-muted"><?= \Yii::t('app/cpf', 'Please add any reference related to the information in Section 3.4. above (if any)') ?></em>
            
            <?php
            ?>
        </p>
    
        <?= $this->render('_refs_table', ['model'=>$model]) ?>
        
        
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app/cpf', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    
    
    
    <?php ActiveForm::end(); ?>

</div>
