<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\cpfbuilder\models\CpfSection3 */

$this->title = $model->year;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/cpf', 'Cpf Section3s'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cpf-section3-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'year' => $model->year, 'user_id' => $model->user_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'year' => $model->year, 'user_id' => $model->user_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'year',
            'user_id',
            'n_offshore_inspections',
            'man_days',
            'n_inpeceted',
            'n_major_accidents',
            'n_env_concerns',
            's3_3_text:ntext',
            'description:ntext',
            'rationale:ntext',
            'expected_outcome:ntext',
        ],
    ]) ?>

</div>
