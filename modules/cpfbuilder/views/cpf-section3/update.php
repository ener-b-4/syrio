<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\cpfbuilder\models\CpfSection3 */

$this->title=Yii::t('app', 'Annual report') . ' ' . \Yii::t('app/crf', 'Section {evt}', ['evt'=>3]) . ': ' . \Yii::t('app', 'Update');

//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Cpf Installations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'CPF sessions', 'url' => ['/cpfbuilder/cpf-sessions/index', 
    ]];
$this->params['breadcrumbs'][] = ['label' => $model->year, 'url' => ['/cpfbuilder/cpf-sessions/view', 
    'year' => $model->year, 'user_id'=>$model->user_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');

?>
<div class="cpf-section3-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
