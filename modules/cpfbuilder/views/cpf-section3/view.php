<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\cpfbuilder\models\CpfSection3 */
/* @var $readonly boolean */

$this->title = $model->year;
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app/cpf', 'Cpf Section3s'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cpf-section3-view">
    <div class="row sy_margin_bottom_26" id="section3">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-xs-10">
                    <p>
                        <span class="cpf-section-title"><?= ucfirst(\Yii::t('app/crf', 'Section {evt}', ['evt'=>'3'])) ?></span>
                        <span class="cpf-section-subtitle"><?= \Yii::t('app/cpf', 'REGULATORY FUNCTIONS AND FRAMEWORK') ?></span>
                    </p>
                </div>
                <div class="col-xs-2 btn-group" style="text-align: bottom">
                    <div class="pull-right">
                        <?php
                        if (!$readonly) {
                            $url = \Yii::$app->urlManager->createUrl([
                                '/cpfbuilder/cpf-section3/update', 'year'=>$model->year, 'user_id'=>$model->user_id
                            ]);
                            
                            echo Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                'class' => 'btn btn-success btn-xs']);
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>        
        
        <div class="col-lg-12">
            <?= $this->render('partial_views/s3_1', ['model'=>$model]) ?>
            <?= $this->render('partial_views/s3_2', ['model'=>$model]) ?>
            <?= $this->render('partial_views/s3_3', ['model'=>$model]) ?>
            <?= $this->render('partial_views/s3_4', ['model'=>$model]) ?>
        </div>
    </div>
    
    
</div>
