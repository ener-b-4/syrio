<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;

/**
 * Description of addReferences
 *
 * @author vamanbo
 */

/* @var $this yii\web\View */
/* @var $model app\modules\cpfbuilder\models\CpfSection3 */
/* @var $references \app\modules\cpfbuilder\models\CpfReferences[] */
/* @var $searchModel app\modules\cpfbuilder\models\CpfReferencesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = \Yii::t('app', 'Annual report') . ' ' . \Yii::t('app/crf', 'Section {evt}', ['evt'=>'3.4']) . ': ' . \Yii::t('app', 'Add {evt}', ['evt'=> strtolower(\Yii::t('app/cpf', 'References'))]);

$regRefUrl = \Yii::$app->urlManager->createUrl('//cpfbuilder/cpf-references/create');
$regRefLnk = Html::a('<span class="glyphicon glyphicon-plus"></span>', $regRefUrl, [
    'class'=>'btn btn-xs btn-success'
]);


$mngRefUrl = \Yii::$app->urlManager->createUrl('//cpfbuilder/cpf-references/index');
$mngRefLnk = Html::a(\Yii::t('app', 'here'), $mngRefUrl, [
    'class'=>'btn btn-xs btn-primary'
]);
        
        
?>

<h1><?= Html::encode($this->title) ?></h1>

<div class='row'>
    
    <div class="col-lg-12">
        <p>
            <?= \Yii::t('app', 'From the list of titles currently available(*) tick the ones you want to use as reference in Section 3.4. of the current CRF.') ?>
        </p>
        <p>
            <?= \Yii::t('app', 'If you cannot find the title within the provided list, use {evt} to register a new document.', [
                'evt'=>$regRefLnk
            ]) ?>
        </p>
        <p>
            <?= \Yii::t('app', 'Click {evt} to manage the registered documents.', [
                'evt'=>$mngRefLnk
            ]) ?>
        </p>
    </div>
    
    <div class='col-lg-12'>
        <h2>
            <?= \Yii::t('app', 'Available titles') ?>
        </h2>
        <?= kartik\grid\GridView::widget([
                'id'=>'grid',
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    [
                        'class' => \yii\grid\CheckboxColumn::className(), // \kartik\grid\CheckboxColumn::className(),
                        //'width' => 20
                    ],
                    'name',
                    'url'
                ],
            ]); ?>
    </div>
    
    <div class='col-lg-12'>
        <?= Html::a(\Yii::t('app/commands', 'Submit'), '#', [
            'id'=>'btnSubmit',
            'data-method'=>'post',
            'class' => 'btn btn-success'
        ]) ?>
    </div>
</div>


<?php
$url = \Yii::$app->urlManager->createUrl(['/cpfbuilder/cpf-section3/references', 'year'=>$model->year, 'user_id'=>$model->user_id]);

$js = <<<EOF
        
        //var keys = $('#grid').yiiGridView('getSelectedRows');
        
        $('#btnSubmit').click(function(event){
            event.preventDefault();
            event.stopPropagation();

            var keys = $('#grid').yiiGridView('getSelectedRows');
            console.log(keys);
        
                $.post(
                    '$url',
                    { 'ids': keys },
                    function(data, status) {
                        console.log(status);
                    });
        
            return false;
            });
        
EOF;

$this->registerJs($js, yii\web\View::POS_READY);
?>