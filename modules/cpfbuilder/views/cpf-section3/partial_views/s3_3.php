<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\cpfbuilder\models\CpfSection3 */

?>

<div class="row sy_margin_bottom_26" id="section3_3">
    <div class="col-lg-12">
        <p style="margin-bottom: 0px"><span class="text-info">3.3 </span>
            <strong class="text-info" style="margin-bottom: 12px;"><?= \Yii::t('app/cpf', 'Enforcement actions') ?></strong></p>           
        <p class="text-muted"><?= \Yii::t('app/cpf', 'Main enforcement actions or convictions performed in the reporting period pursuant to Article 18 of Directive 2013/30/EU') ?>.</p>
        
        <!-- content 3.3 section -->
        <p><em class="text-info" style="margin-bottom: 12px;"><?= \Yii::t('app/cpf', 'Narrative') ?></em></p>

        <?= nl2br(\app\models\shared\NullValueFormatter::Format($model->s3_3_text)) ?>
        
        <!-- (end) content 3.3 section -->
    </div>
</div>