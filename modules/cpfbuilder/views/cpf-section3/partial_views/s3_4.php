<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\cpfbuilder\models\CpfSection3 */

?>

<div class="row sy_margin_bottom_26" id="section3_4">
    <div class="col-lg-12">
        <p style="margin-bottom: 0px"><span class="text-info">3.4 </span>
            <strong class="text-info" style="margin-bottom: 12px;"><?= \Yii::t('app/cpf', 'Major changes in the offshore regulatory framework') ?></strong></p>           
        <p class="text-muted sy_pad_bottom_6"><?= \Yii::t('app/cpf', 'Please describe any major changes in the offshore regulatory framework during the reporting period') ?>.
            <br/><em>(<?= \Yii::t('app/cpf', 'include e.g. rationale, description, expected outcome, references') ?>)</em>
        </p>
        


        <!-- content 3.4 section -->
        <dl class="dl-horizontal">
            <dl>
                <dt style="text-align: left"><?= $model->attributeLabels()['rationale'] ?></dt>
                <dd>
                    <?= nl2br(\app\models\shared\NullValueFormatter::Format($model->rationale)) ?>
                </dd>
            </dl>
            <dl>
                <dt style="text-align: left"><?= $model->attributeLabels()['description'] ?></dt>
                <dd>
                    <?= nl2br(\app\models\shared\NullValueFormatter::Format($model->description)) ?>
                </dd>
            </dl>
            <dl>
                <dt style="text-align: left"><?= $model->attributeLabels()['expected_outcome'] ?></dt>
                <dd>
                    <?= nl2br(\app\models\shared\NullValueFormatter::Format($model->expected_outcome)) ?>
                </dd>
            </dl>
            <dl>
                <dt style="text-align: left">
                    <?= \Yii::t('app/cpf', 'References') ?>
                </dt>
                <dd>
                    <div>
                        <?php
                        $refs = $model->references;
                        //echo '<pre>';
                        //echo print_r($refs);
                        //echo '</pre>';
                        //die();

                        if (isset($refs) && count($refs)>0)
                        {
                        ?>
                        <ol class="list" style="padding-left:20px;">
                        <?php
                            foreach($refs as $ref)
                            {
                                $s = '<li>' . $ref->name;
                                $s.= isset($ref->url) ? '<br/>' . Html::a($ref->url, $ref->url) . '</li>' : '</li>';
                                echo $s;
                            }
                        ?>
                        </ol>
                        <?php
                        }
                        else
                        {
                            //no references
                            echo '<em class="sy_alert_color">' . \Yii::t('app', '(none)') . '</em>';                         
                        }
                        ?>
                    </div>
                </dd>
            </dl>
            
        </dl>
        <?php
        
        ?>
        <!-- (end) content 3.4 section -->
    </div>
</div>