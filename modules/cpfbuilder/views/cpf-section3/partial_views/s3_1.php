<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\cpfbuilder\models\CpfSection3 */

?>

<div class="row sy_margin_bottom_26" id="section3_1">
    <div class="col-lg-12">
        <p style="margin-bottom: 0px"><span class="text-info">3.1 
            </span><strong class="text-info" style="margin-bottom: 12px;"><?= \Yii::t('app/cpf', 'Inspections') ?></strong></p>           
        <p class="text-muted"><?= \Yii::t('app/cpf', 'Number and offshore inspections performed during the reporting year') ?>.</p>

        <!-- table 3.1 section -->
        <table class="table table-condensed">
            <thead>
                <th>
                    <em><?= $model->attributeLabels()['n_offshore_inspections'] ?></em>
                </th>
                <th>
                    <em><?= $model->attributeLabels()['man_days'] ?></em>
                </th>
                <th>
                    <em><?= $model->attributeLabels()['n_inpeceted'] ?></em>
                </th>
                <th>
                </th>
            </thead>
            <tbody>
                <tr>
                    <td><?= isset($model->n_offshore_inspections) ? 
                        $model->n_offshore_inspections : 
                        '<em class="sy_alert_color">' . \Yii::t('app', '(not set)') . '</em>' ?>
                    </td>
                    <td><?= isset($model->man_days) ? 
                        $model->man_days : 
                        '<em class="sy_alert_color">' . \Yii::t('app', '(not set)') . '</em>' ?>
                    </td>
                    <td><?= isset($model->n_inpeceted) ? 
                        $model->n_inpeceted : 
                        '<em class="sy_alert_color">' . \Yii::t('app', '(not set)') . '</em>' ?>
                    </td>
                    <td/>
                </tr>
            </tbody>
        </table>
        <!-- (end) table 4.1 section -->
    </div>
</div>