<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\cpfbuilder\models\CpfSection3 */

?>

<div class="row sy_margin_bottom_26" id="section3_2">
    <div class="col-lg-12">
        <p style="margin-bottom: 0px"><span class="text-info">3.2 </span>
            <strong class="text-info" style="margin-bottom: 12px;"><?= \Yii::t('app/cpf', 'Investigations') ?></strong></p>           
        <p class="text-muted"><?= \Yii::t('app/cpf', 'Number and type of investigations performed during the reporting year') ?>.</p>

        <!-- table 3.2 section -->
    <table class="table table-condensed" style="border-top-width: 0px;">
       <tbody>
         <tr>
             <td style="vertical-align: top; width:32px; max-width: 32px;">(a)</td>
             <td>
                 <?= $model->attributeLabels()['n_major_accidents'] ?>:
                <?= isset($model->n_major_accidents) ? 
                 $model->n_major_accidents : 
                 '<em class="sy_alert_color">' . \Yii::t('app', '(not set)') . '</em>' ?>
                 <br/><small class="text-muted"><?= \Yii::t('app/cpf', 'pursuant to Article 26 of Directive 2013/30/EU') ?></small>
             </td>
         </tr>
         <tr>
             <td style="vertical-align: top;">(b)</td>
             <td>
                 <?= $model->attributeLabels()['n_env_concerns'] ?>:
                <?= isset($model->n_env_concerns) ? 
                 $model->n_env_concerns : 
                 '<em class="sy_alert_color">' . \Yii::t('app', '(not set)') . '</em>' ?>
                 <br/><small class="text-muted"><?= \Yii::t('app/cpf', 'pursuant to Article 22 of Directive 2013/30/EU') ?></small>
             </td>
         </tr>
       </tbody>
    </table>
        
        <!-- (end) table 3.2 section -->
    </div>
</div>