<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\cpfbuilder\models\CpfSessions */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cpf-sessions-form">

    <?php $form = ActiveForm::begin([
        'enableAjaxValidation'=>true
    ]); ?>

    <?= $form->field($model, 'year', ['template' => '<div class="row">'
        . '<div class="col-lg-12" style="width:100px">'
        . '{label}{input}'
        . '</div>'
        . '</div>'
        . '<div class="row">'
        . '<div class="col-lg-12">'
        . '{hint} {error}'
        . '</div>'
        . '</div>'])->textInput([
            'maxlength' => 4
        ])
            ->hint(Yii::t('app/cpf', 'Please provide the year for the report.')) ?>

    <?= $form->field($model, 'user_id', ['template' => '{input}'])->hiddenInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
