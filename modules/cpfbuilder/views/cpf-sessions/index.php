<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use kartik\grid\GridView;

use app\assets\FontAwesomeAsset;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\cpfbuilder\models\CpfSessionsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

FontAwesomeAsset::register($this);

$this->title = Yii::t('app', 'Annual reports');
$this->params['breadcrumbs'][] = $this->title;

$readonly = !(Yii::$app->user->can('ca_generate_annual_report'));

if (!$readonly) {
    $template = '{view}  {reset} {delete}';
} else {
    $template = '{view}';
}

?>
<div class="cpf-sessions-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="row sy_pad_top_36">
        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6">
            
            <?= $this->render('../help/cpf_sessions_index_help', ['readonly'=>$readonly]) ?>

        </div>
        <div class="col-lg-9 col-md-9 col-sm-8 col-xs-6">
            <div class="row">
                <div class="col-lg-12">
                    <?php include Yii::getAlias('@app') . '/views/shared_parts/message_snippet.php'; ?>
                </div>
            </div>
            
            
            <?php if (!$readonly) { ?>
            
            <p>
                <?= Html::a(Yii::t('app', 'Create Annual Report'), ['create'], ['class' => 'btn btn-success']) ?>
            </p>

            <?php } ?>
            
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => kartik\grid\SerialColumn::className()],

                    [
                        'class' => kartik\grid\DataColumn::className(),
                        'attribute' => 'synced',
                        'hAlign' => 'center',
                        'filterType' => GridView::FILTER_SELECT2,   // '\kartik\widgets\Select2',
                        'filter' => [
                                1=>\Yii::t('app', 'Yes'),
                                0=>\Yii::t('app', 'No')
                            ],
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear'=>true],
                        ],
                        'filterInputOptions' => ['placeholder'=>\Yii::t('app','all')],
                        
                        
                        'content' => function ($model, $key, $index, $column) {
                            if ($model->refresh_installations || $model->refresh_data) {
                                return '<span class="text-warning fa fa-exclamation-triangle"'
                                . ' data-toggle="tooltip"' 
                                . ' title= "' . \Yii::t('app/crf', 'Annual report IS NOT synced with the reported incidents.') . '"'
                                . ' content="content"'
                                . ' style="cursor: default;"'
                                . '></span>';
                            } else {
                                return '<span class="text-success fa fa-check-circle-o"'
                                . ' data-toggle="tooltip"' 
                                . ' title= "' . \Yii::t('app/crf', 'Annual report is synced with the reported incidents.') . '"'
                                . ' content="content"'
                                . ' style="cursor: default;"'
                                . '></span>';

                            }
                        }
                    ],
                    'year',
                    [
                        'class' => \kartik\grid\ActionColumn::className(),
                        'template' => $template,
                        'buttons' => [
                            'reset' => function ($url, $model, $key) {
                                return Html::a('<span class="glyphicon glyphicon-erase"></span>', $url, [
                                    'title' => ucfirst(Yii::t('app', 'reset')),
                                    'data' => [
                                        'method'=>'POST',
                                        'confirm'=>  strtoupper(Yii::t('app/commands', 'Reset {evt}', ['evt'=>Yii::t('app', 'Annual report')]))  
                                        .PHP_EOL
                                        .PHP_EOL
                                        .Yii::t('app', 'This will clear all the information in the current report.')
                                        .PHP_EOL
                                        .Yii::t('app', 'The individual CRF Assessments in Terms of Section 4 WILL BE PRESERVED.')
                                        .PHP_EOL
                                        .PHP_EOL
                                        .Yii::t('app', 'Ok to proceed?')
                                    ]
                                ]);
                            },
                                    
                            'delete' => function ($url, $model, $key) {
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                    'title' => ucfirst(Yii::t('app', 'Delete')),
                                    'data' => [
                                        'method'=>'POST',
                                        'confirm'=>  strtoupper(Yii::t('app', 'Delete {evt}', ['evt'=>Yii::t('app', 'Annual report')]))  
                                        .PHP_EOL
                                        .PHP_EOL
                                        .Yii::t('app', 'This will COMPLETELY delete the current report.')
                                        .PHP_EOL
                                        .PHP_EOL
                                        .strtoupper(Yii::t('app', 'warning')).'!!!'
                                        .PHP_EOL
                                        .Yii::t('app', 'The individual CRF Assessments in Terms of Section 4 WILL ALSO BE DELETED!')
                                        .PHP_EOL
                                        .PHP_EOL
                                        .Yii::t('app', 'Ok to proceed?')
                                    ]
                                ]);
                            }
                                    
                        ]
                    ],
                ],
            ]); ?>
        </div>
    </div>
    
</div>

<?php

$this->registerJsFile('/js/cpf/cpf-sessions-view.js', ['depends' => [\yii\web\JqueryAsset::className(), yii\bootstrap\BootstrapAsset::className()] ]);

?>

<?php
$js = <<< 'SCRIPT'
/* To initialize BS3 tooltips set this below */
    $('body').tooltip({selector: '[data-toggle="tooltip"]'});
/* To initialize BS3 popovers set this below */
    $('body').popover({selector: '[data-toggle="popover"]'});
    
SCRIPT;
// Register tooltip/popover initialization javascript
$this->registerJs($js);
?>