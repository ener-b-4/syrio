<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\cpfbuilder\models\CpfSessions */

$this->title = Yii::t('app', 'Create {evt}', [
    'evt' => \Yii::t('app', 'Annual Report'),
]);

$this->title = Yii::t('app', 'Create Annual Report');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Annual reports'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cpf-sessions-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
