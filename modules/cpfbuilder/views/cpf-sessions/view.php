<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\assets\FontAwesomeAsset;
use app\widgets\ValidationReportWidget;

FontAwesomeAsset::register($this);

/* @var $this yii\web\View */
/* @var $model app\modules\cpfbuilder\models\CpfSessions */
/* @var $readonly bool */


$this->title = $model->year;

$this->params['breadcrumbs'][] = ['label' => \Yii::t('app', 'Annual reports'), 'url' => ['/cpfbuilder/cpf-sessions/index', 
    ]];
$this->params['breadcrumbs'][] = ['label' => \Yii::t('app', 'Editor {evt}', ['evt'=>$model->year]), 
    //'url' => ['/cpfbuilder/cpf-sessions/view', 'year' => $model->year, 'user_id'=>$model->user_id]
    ];
//$this->params['breadcrumbs'][] = Yii::t('app', 'Update');

//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Cpf Sessions'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;

$cpf_reports_url = \Yii::$app->urlManager->createUrl(['/ca/incident-categorization/index']);
$cpf_assess_url = \Yii::$app->urlManager->createUrl(['/cpfbuilder/cpf-crf-assessments/index']);

//check if user can create the report and pass the parameter to the view
$readonly = !(Yii::$app->user->can('ca_generate_annual_report'));


if (!($readonly)) {
if ($model->refresh_installations || $model->refresh_data) {
    $refresh_content = \Yii::t('app/crf', 'Changes have been detected in the reports list for the current year.');
    $refresh_content .= ' ' . \Yii::t('app/crf', 'Please:');
    
    $refresh_content .= '<ul>';
    
    if ($model->refresh_installations) {
        $refresh_content .= '<li>'
                . \Yii::t('app/cpf', 'Refresh installations')
                . '</li>';
    }

    if ($model->refresh_data) {
        $refresh_content .= '<li>'
                . \Yii::t('app/cpf', 'Refresh data') . ' (' . \Yii::t('app/crf', 'Section {evt}', ['evt'=>4]) . ')'
                . '</li>';
    }
    
    $refresh_content .= '</ul>';
    
    $refresh_content .= \Yii::t('app/crf', 'to keep the Annual Report up to date.');
    
    
}
}


?>
<div class="cpf-sessions-view container">

            <?php
            if (isset($refresh_content)) {
                ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="alert alert-warning" role="alert">
                        <?= $refresh_content ?>
                    </div>
                </div>
            </div>
            <?php 
            }
            ?>
    
        <div class="row">
            <!-- (start) left panel -->
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12" id="leftPanel">

                <?= $this->render('../help/cpf_sessions_view_help', [
                    'cpf_reports_url' => $cpf_reports_url,
                    'cpf_assess_url' => $cpf_assess_url,
                    'readonly' => $readonly
                ]) ?>
                
            </div>
            <!-- (end) left panel -->
            <!-- (start) content panel -->
            <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12" id="contentPanel">
                
                <div class="row">
                    <div class="col-lg-12" style="margin-left: -14px;">
                    <?php
                    /* use this on any view to include the actionmessage logic and widget */
                    //include Yii::getAlias('@app') . '\views\shared_parts\message_snippet.php';
                    include Yii::getAlias('@app') . '/views/shared_parts/message_snippet.php';    

                    ?>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-lg-12">
                        <?php 
                        if ($model->hasErrors())
                        {
                            echo ValidationReportWidget::widget([
                                'title' => \Yii::t('app','Build SPIROS XML', []),
                                'errors_message' => \Yii::t('app','Unable to build the xml! Please review the information below and try again.'),
                                'errors'=>$model->errors,
                                'no_errors_message' => '',
                            ]);
                        }
                        ?>
                    </div>
                </div>
                
                <!-- toolbar section -->
                <div class="row">
                    <div class="col-lg-12 sy_toolbar_inverse">
                        <div class="btn-toolbar" role="toolbar">
                            <div class="">
                                
                                <!-- ver 1.3. -->
                                <?php
                                
                                if (!$readonly) {
                                    ?>
                                <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="glyphicon glyphicon-refresh"></span> <?= Yii::t('app/cpf', 'Refresh') ?> <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                    <li><a href="<?= \yii\helpers\Url::to([
                                        '/cpfbuilder/cpf-sessions/refresh-installations',
                                        'year'=>$model->year, 'user_id'=>$model->user_id]) ?>"
                                        ><?= \Yii::t('app/cpf', 'Refresh installations') ?></a></li>
                                    <li>
                                        <?php 
                                        $msg = 'Be aware that this process may take quite a while, depending on the number of '
                                                . 'events registered and prepared for being reported over the reporting period. '
                                                . 'Proceed?';
                                        
                                        echo Html::a(Yii::t('app/cpf', 'Refresh data'), 
                                                \yii\helpers\Url::to([
                                                '/cpfbuilder/cpf-sessions/refresh-inferred',
                                                'year'=>$model->year, 'user_id'=>$model->user_id]), [
                                                'data' => [
                                                    'confirm' => Yii::t('app/cpf', $msg),
                                                    'method' => 'post',
                                                    ],                                
                                        ]); 
                                        
                                        ?>
                                    </li>
                                </ul>                                
                                
                                
                                <?= Html::a('<span class="glyphicon glyphicon-erase"></span>'. ' '.'<span>'. ucfirst(Yii::t('app', 'reset')) .'</span>',
                                        ['reset', 'year'=>$model->year],
                                        [
                                            'class'=>"btn btn-warning btn-sm",
                                            'data' => [
                                                'method'=>'POST',
                                                'confirm'=>  strtoupper(Yii::t('app/commands', 'Reset {evt}', ['evt'=>Yii::t('app', 'Annual report')]))  
                                                .PHP_EOL
                                                .PHP_EOL
                                                .Yii::t('app', 'This will clear all the information in the current report.')
                                                .PHP_EOL
                                                .Yii::t('app', 'The individual CRF Assessments in Terms of Section 4 WILL BE PRESERVED.')
                                                .PHP_EOL
                                                .PHP_EOL
                                                .Yii::t('app', 'Ok to proceed?')
                                            ],   
                                        ]
                                        ) ?> 
                                
                                <?php    
                                }
                                ?>
                                
                            </div>
                            <div class="btn-group pull-right">
                                <?= Html::a('<i class="fa fa-file-code-o"></i>'.' '.'<span><strong>SPIROS XML</strong></span>',
                                        ['generate_xml', 'year'=>$model->year, 'user_id'=>$model->user_id],
                                        [
                                            'class'=>"btn btn-danger btn-sm",
                                            'data' => [
                                                'confirm' => Yii::t('app/cpf', 
                                                        'Generate SPIROS Xml?'),
                                                'method' => 'post',
                                            ],   
                                        ]
                                        ) ?>                                
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end toolbar section -->
                
                <!-- render Section 1 -->
                <?= $this->render('../cpf-section1/view', ['model'=>$model->cpfSection1, 'readonly' => $readonly]) ?>
                <!-- render Section 2 -->
                <?= $this->render('../cpf-section2/view', ['model'=>$model->cpfSection2, 'readonly' => $readonly]) ?>
                <!-- render Section 3 -->
                <?= $this->render('../cpf-section3/view', ['model'=>$model->cpfSection3, 'readonly' => $readonly]) ?>
                <!-- render Section 4 -->
                <?= $this->render('../cpf-section4/view', ['model'=>$model->cpfSection4, 'readonly' => $readonly]) ?>
            </div>
            <!-- (end) content panel -->
        </div>
</div>


<?php
$js = <<< 'SCRIPT'
/* To initialize BS3 tooltips set this below */
    $('body').tooltip({selector: '[data-toggle="tooltip"]'});
/* To initialize BS3 popovers set this below */
    $('body').popover({selector: '[data-toggle="popover"]'});
    
SCRIPT;
// Register tooltip/popover initialization javascript
$this->registerJs($js);
?>

<?php

//'js/bootstrap.min.js'

$this->registerJsFile('/js/cpf/cpf-sessions-view.js', ['depends' => [\yii\web\JqueryAsset::className(), yii\bootstrap\BootstrapAsset::className()] ]);

?>