<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\cpfbuilder\models\CpfSessions */

$this->title = Yii::t('app/cpf', 'Modify {modelClass}: ', [
    'modelClass' => 'Reporting Year',
]) . ' ' . $model->year;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Annual Reports'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->year, 'url' => ['view', 'year' => $model->year, 'user_id' => $model->user_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Modify reporting year');
?>
<div class="cpf-sessions-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
