<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\cpfbuilder\models\CpfSection2 */
/* @var $readonly boolean */

$title = \Yii::t('app/cpf', 'installations');
$this->title = \Yii::t('app/crf', 'Section {evt}', ['evt'=>'2']);

?>
<div class="cpf-section2-view">
    
    <?php
    //dummy data

    ?>
    
    <div class="row sy_margin_bottom_26" id="section2">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-xs-12">
                    <p>
                        <span class="cpf-section-title"><?= ucfirst(\Yii::t('app/crf', 'Section {evt}', ['evt'=>'2'])) ?></span>
                        <span class="cpf-section-subtitle"><?= \Yii::t('app', 'INSTALLATIONS') ?></span>
                    </p>
                </div>
            </div>
        </div>

        <div class="col-lg-12 text-danger" >
            <blockquote style="background-color: #fcf0ba">
                <h3><?= \Yii::t('app', 'Please note') ?></h3>
                <p>
                    <?= \Yii::t('app', 'Only the installations that have reported {evt} over the reporting period are taking into account.', [
                        'evt' => '<b>' . \Yii::t('app', 'at least one incident') . '</b>'
                    ]) ?>
                </p>
                <p>
                    <?= \Yii::t('app', 'Adding the rest of the installations is possible after uploading this report (XML file) on the SPIROS platform.') ?>
                </p>
            </blockquote>
        </div>
        
        <div class="col-lg-12">
            <?= $this->render('partial_views/s2_1', ['model'=>$model, 'readonly' => $readonly]) ?>
            <?= $this->render('partial_views/s2_2', ['model'=>$model, 'readonly' => $readonly]) ?>
            <?= $this->render('partial_views/s2_3', ['model'=>$model, 'readonly' => $readonly]) ?>
            <?= $this->render('partial_views/s2_4', ['model'=>$model, 'readonly' => $readonly]) ?>
        </div>
    </div>
    
    
    <!--
        <div class="col-lg-12">
            <? DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'year',
                    'user_id',
                    'norm_ktoe',
                    'norm_oil_q',
                    'norm_oil_u',
                    'norm_oil_n',
                    'norm_gas_q',
                    'norm_gas_u',
                    'norm_gas_n',
                ],
            ]) ?>
        </div>
    </div>
    -->

</div>
