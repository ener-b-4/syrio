<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use kartik\form\ActiveForm;
use app\models\units\Units;

/* @var $this yii\web\View */
/* @var $model app\modules\cpfbuilder\models\CpfSection2 */
/* @var $form yii\widgets\ActiveForm */

$units = Units::find()
        ->where(['scope' => Units::UNIT_OF_ENERGY_GO])->all();
$unisArray = yii\helpers\ArrayHelper::map($units, 'id', 'unit');

?>

<div class="cpf-section2-form">

    <?php $form = ActiveForm::begin([
        'type'=>  ActiveForm::TYPE_INLINE
    ]); ?>

    <?= $form->field($model, 'year')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'user_id')->hiddenInput()->label(false) ?>

    <div class="row sy_pad_bottom_18">
        <div class="col-md-3 text-right"><?= \Yii::t('app/cpf', 'Total number of actual offshore working hours for all installations') ?></div> 
        <div class="col-md-6">
            <?= $form->field($model, 'working_hours')->textInput(['placeholder'=>\Yii::t('app', 'hours')]) ?>
            <strong style="padding-left:12px; vertical-align: central">(<?=\Yii::t('app', 'hours')?>)</strong>
        </div>
    </div>
    
    <div class="col-lg-12 sy_pad_bottom_18">
        <strong class="text-info"><?= \Yii::t('app/cpf', 'Production data') ?></strong>
    </div>
    
    <div class="row">
        <div class="col-md-3 text-right"><?= \Yii::t('app/cpf', '{evt} production', ['evt'=>' (' . \Yii::t('app', 'total') . ')']) ?></div> 
        <div class="col-md-6">
            <?= $form->field($model, 'norm_ktoe')->textInput(['disabled'=>'true']) ?>
            <strong style="padding-left:12px; vertical-align: central">(Ktoe)</strong>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3 text-right"><?= \Yii::t('app/cpf', '{evt} production', ['evt'=>  strtolower(\Yii::t('app/crf', 'Oil'))]) ?></div> 
        <div class="col-md-2">
            <?= $form->field($model, 'norm_oil_q')->textInput() ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'norm_oil_u')->dropDownList($unisArray, [
                'prompt' => '-- units --'
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3 text-right"><?= \Yii::t('app/cpf', '{evt} production', ['evt'=>  strtolower(\Yii::t('app/crf', 'Gas'))]) ?></div> 
        <div class="col-md-2">
            <?= $form->field($model, 'norm_gas_q')->textInput() ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'norm_gas_u')->dropDownList($unisArray, [
                'prompt' => '-- units --'
            ]) ?>
        </div>
    </div>

    <?= $form->field($model, 'norm_oil_n')->hiddenInput()->label(false) ?>



    <?= $form->field($model, 'norm_gas_n')->hiddenInput()->label(false) ?>

    <div class="form-group" style="margin-top:18px">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
