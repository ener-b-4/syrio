<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\cpfbuilder\models\CpfSection2 */
/* @var $readonly boolean */

$installations = $model->s_2_3_installations;

?>

<div class="row sy_margin_bottom_26" id="section2_3">
    <div class="col-lg-12">
        <p style="margin-bottom: 0px"><span class="text-info">2.3 </span><strong class="text-info" style="margin-bottom: 12px;"><?= \Yii::t('app/cpf', 'Mobile installations') ?></strong></p>
        <p class="text-muted"><?= \Yii::t('app/cpf', 'Please report the mobile installations carrying out operations during the reporting period (MODUs and other non-production installations)') ?>.</p>
    </div>
    <div class="col-lg-12">
        <table class="table table-condensed">
           <thead>
             <tr>
                 <th colspan="6" style="vertical-align:top; text-align: center;">
                     <em><?= \Yii::t('app/cpf', 'Table {evt}', ['evt'=>'2.3']) ?></em>
                     <strong class="padding-bottom: 12px;"><?= \Yii::t('app/cpf', 'Mobile installations') ?></strong>
                 </th>
             </tr>
             <tr>
                 <th rowspan="2" style="vertical-align: top;"><?= \Yii::t('app/cpf', 'Name or ID') ?></th>
                 <th rowspan="2" style="vertical-align: top;"><?= \Yii::t('app/cpf', 'Type of installation') ?></th>
                 <th rowspan="2" style="vertical-align: top;"><?= \Yii::t('app/cpf', 'Year of construction') ?></th>
                 <th rowspan="2" style="vertical-align: top;"><?= \Yii::t('app/cpf', 'Number of beds') ?></th>
                 <th><?= \Yii::t('app/cpf', 'Geographical area of operations; and Duration') ?></th>
                 <th/>
             </tr>
             <tr>
                 <th>
                   <ul class='list list-unstyled'>
                    <li>
                        <div class="row">
                         <div class="col-xs-8">
                            <?= \Yii::t('app/cpf', 'Area') ?>
                         </div>                
                         <div class="col-xs-4 text-right">
                            <?= \Yii::t('app/cpf', 'Duration') ?> <em>(<?= \Yii::t('app/cpf', 'months') ?>)</em>
                         </div>
                     </div>
                    </li>
                   </ul>
                 </th>
             </tr>
           </thead>
           <tbody>
                <?php
                if (isset($installations) && count($installations)>0)
                {
                    foreach($installations as $installation)
                    {
                        /* @var $installation \app\modules\cpfbuilder\models\CpfInstallation */
                    ?>
                <tr>
                    <td><?= $installation->ident ?></td>
                    <td><?= $installation->type ?></td>
                    <td><?= $installation->installation_year ?></td>
                    <td>                        
                        <?= !(isset($installation->n_beds) && $installation->n_beds != '') ? '<em class="sy_alert_color">' . \Yii::t('app', '(not set)') . '</em>'  : $installation->n_beds ?>
                    </td>
                    <td>
                        <?php
                        //get the areas of operation of each installation
                        $area_ops = $installation->cpfInstOpAreas;
                        if (!isset($area_ops) || count($area_ops) == 0)
                        {
                            //there's a problem here
                        }
                        else
                        {
                        ?>
                        <ul class='list list-unstyled'>
                        <?php
                        
                            foreach ($area_ops as $area)
                            {
                       
                        //cpfbuilder%2Fcpf-installations%2Fupdate&id=19
                            $url = \Yii::$app->urlManager->createUrl([
                                '/cpfbuilder/cpf-inst-op-area/delete', 'id'=>$area->id
                            ]);
                            
                            if (!$readonly) {
                                $hh = Html::a('<span class="glyphicon glyphicon-remove"></span>', $url, [
                                    'class' => 'btn btn-xs btn-danger',

                                    'data' => [
                                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                        'method' => 'post',
                                    ],                                

                                    ]);
                            } else {
                                $hh = '';
                            }
                                                        
                                
                                $s = '<li>'
                                        . ' <div class="row">'                  //row
                                        . '     <div class="col-xs-7">'        //col1
                                        . '         <em>'
                                        .           $area->name
                                        . '         </em>'                      
                                        . '     </div>'                         //end_col_1
                                        . '     <div class="col-xs-3 text-right">'          //col2
                                        .           $area->duration
                                        . '     </div>'
                                        . '     <div class="col-xs-2 text-right">'
                                        .       $hh
                                        . '     </div>'
                                        . ' </div>'                                        
                                   . '</li>';
                                echo $s;
                            }
                        }
                        ?>
                        </ul> <!-- end list area operations -->
                    </td>
                    
                    <td>
                        <div class="button-group">
                        <?php
                        if (!$readonly) {
                        //cpfbuilder%2Fcpf-installations%2Fupdate&id=19
                            $url = \Yii::$app->urlManager->createUrl([
                                '/cpfbuilder/cpf-inst-op-area/create', 'id'=>$installation->id,
                                'year'=> $installation->year, 'user_id'=>$installation->user_id,
                                'installation_id'=>$installation->id
                            ]);
                            echo Html::a('<span class="glyphicon glyphicon-plus"></span>', $url, [
                                'class' => 'btn btn-xs btn-primary',
                                'title' => \Yii::t('app', 'Add {evt}', ['evt'=>\Yii::t('app/cpf', 'operation area')])
                            ]);

                            
                            //cpfbuilder%2Fcpf-installations%2Fupdate&id=19
                            $url = \Yii::$app->urlManager->createUrl([
                                '/cpfbuilder/cpf-installations/update', 'id'=>$installation->id
                            ]);
                            echo Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                'class' => 'btn btn-xs btn-success',
                                'title' => ucfirst(\Yii::t('app', 'modify {evt}', ['evt'=>lcfirst(\Yii::t('app', 'Information about installation'))]))
                            ]);

                            //cpfbuilder%2Fcpf-installations%2Fupdate&id=19
                            $url = \Yii::$app->urlManager->createUrl([
                                '/cpfbuilder/cpf-installations/delete', 'id'=>$installation->id
                            ]);
                            echo Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                'class' => 'btn btn-xs btn-danger',
                                'title' => ucfirst(\Yii::t('app', 'Remove')) . ' ' . \Yii::t('app', 'installation'),
                                
                                'data' => [
                                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                    'method' => 'post',
                                ],                                
                                
                                ]);
                        }   //end if readonly
                        ?>
                        </div>
                        
                    </td>                                    
                    
                </tr>
                    <?php
                    }
                }
                else
                {                    
                    //create one row with one column spanned 6
                    //no data
                    $message = isset($installations) ? \Yii::t('app', 'no installation to report') : \Yii::t('app', '(not set)'); 
                    
                    ?>
                <tr>
                    <td colspan="6">
                        <em class="sy_alert_color"><?= $message ?></em>
                    </td>
                </tr>
                    <?php
                }
                ?>
           </tbody>
        </table>
    </div>
</div>
