<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\cpfbuilder\models\CpfSection2 */
/* @var $readonly boolean */

?>

<div class="row sy_margin_bottom_26" id="section2_4">
    <div class="col-lg-12">
        <p style="margin-bottom: 0px">
            <span class="text-info">2.4 </span>
            <strong class="text-info" style="margin-bottom: 12px;">
                <?= \Yii::t('app/cpf' ,'Information for data normalization purposes') ?>
            </strong>

            <div class="btn-group pull-right" style="text-align: bottom">
                <?php
                
                if (!$readonly) {
                
                $url = \Yii::$app->urlManager->createUrl([
                    '/cpfbuilder/cpf-section2/update', 'year'=>$model->year, 'user_id'=>$model->user_id
                ]);

                echo Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                    'class' => 'btn btn-success btn-xs']);
                }   //end in not readonly
                ?>
            </div>
            
        </p>
        <p class="text-muted"><?= \Yii::t('app/cpf' ,'Please provide the total number of actual offshore working hours and the total production in the reporting period') ?>.</p>
    </div>
    <div class="col-lg-12">
        <table class="table table-condensed" style="border-top-width: 0px;">
           <tbody>
             <tr>
                 <td style="vertical-align: top;">(a)</td>
                 <td >
                     <?= \Yii::t('app/cpf' ,'Total number of actual offshore working hours for all installations') ?>: <!-- ' + $cpf.section2.data_norm.working_hours -->
                 </td>
                 <td>
                     <?= $model->working_hours ?>
                 </td>
             </tr>
             <tr>
                 <td style="vertical-align: top;">(b)</td>
                 <td >
                     <?= $model->attributeLabels()['norm_ktoe'] ?> :                      <!-- ' + $cpf.section2.data_norm.production.ktoe_total; -->
                 </td>
                 <td>
                     <?= $model->norm_ktoe ?>
                 </td>
             </tr>
             <tr>
                 <td/>
                 <td >
                     <?= $model->attributeLabels()['norm_oil_q'] ?> :                      <!-- ' + $cpf.section2.data_norm.production.ktoe_total; -->
                 </td>
                 <td>
                     <?php
                        if (isset($model->norm_oil_u))
                        {
                            if (\app\models\units\Units::EnergyUnitGo($model->norm_oil_u) !== NULL)
                            {
                                
                                $unit_text = \app\models\units\Units::EnergyUnitGo($model->norm_oil_u)->unit;
                            }
                            else
                            {
                                $unit_text  = '<em class="sy_alert_color">UNRECOGNIZED ENERGY UNIT!</em>';
                            }
                        }
                        else
                        {
                            $unit_text = '<em class="sy_alert_color"><?= not set ?></em>';
                        }
                        
                     ?>
                     
                     <?= $model->norm_oil_q . ' (' . $unit_text . ')' ?>
                 </td>
             </tr>
             <tr>
                 <td/>
                 <td >
                     <?= $model->attributeLabels()['norm_gas_q'] ?> :                     
                 </td>
                 <td>
                     <?php
                        if (isset($model->norm_gas_u))
                        {
                            if (\app\models\units\Units::EnergyUnitGo($model->norm_gas_u) !== NULL)
                            {
                                $unit_text = \app\models\units\Units::EnergyUnitGo($model->norm_gas_u)->unit;
                            }
                            else
                            {
                                $unit_text = '<em class="sy_alert_color">UNRECOGNIZED ENERGY UNIT!</em>';
                            }
                        }
                        else
                        {
                            $unit_text = '<em class="sy_alert_color"><?= not set ?></em>';
                        }
                     ?>
                     <?= $model->norm_gas_q . ' (' . $unit_text . ')' ?>
                 </td>
             </tr>
           </tbody>
        </table>
    </div>
</div>
