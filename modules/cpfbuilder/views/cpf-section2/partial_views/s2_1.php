<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\cpfbuilder\models\CpfSection2 */
/* @var $readonly boolean */

/* get the installations in jurisdiction on 1 Jan */
$installations = $model->s_2_1_installations;

?>

<div class="row sy_margin_bottom_26" id="section2_1">
    <div class="col-lg-12">
        <p style="margin-bottom: 0px"><span class="text-info">2.1 </span><strong class="text-info" style="margin-bottom: 12px;"><?= \Yii::t('app/cpf', 'Fixed installations') ?></strong></p>           
        <p class="text-muted"><?= \Yii::t('app/cpf', 'Please provide detailed list of installations for offshore oil and gas operation in your country (on first of January of the reported year).') ?></p>

        <!-- table 2.1 section -->
        <table class="table table-condensed">
            <thead>
                <tr>
                    <th colspan="7" style="vertical-align:top; text-align: center;">
                        <em><?= \Yii::t('app/cpf', 'Table {evt}', ['evt'=>'2.1']) ?></em><br/><strong class="padding-bottom: 12px;">
                            <?= \Yii::t('app/cpf', 'Installations within jurisdiction on 1 January of the reporting year') ?>
                        </strong>
                    </th>
                </tr>
                <tr>
                    <th style="vertical-align:top"> <?= \Yii::t('app/cpf', 'Name or ID') ?> </th>
                    <th style="vertical-align:top"> <?= \Yii::t('app/cpf', 'Type of installation') ?>  </th>
                    <th style="vertical-align:top"> <?= \Yii::t('app/cpf', 'Year of installation') ?>  </th>
                    <th style="vertical-align:top"> <?= \Yii::t('app/cpf', 'Type of fluid') ?> </th>
                    <th style="vertical-align:top"> <?= \Yii::t('app/cpf', 'Number of beds') ?> </th>
                    <th><?= \Yii::t('app/cpf', 'Coordinates') ?><br/><small class="text-muted"><?= \Yii::t('app/cpf', '(lon - lat)') ?></small></th>
                    <th/>
                </tr>
            </thead>
            <tbody>
                <?php
                if (isset($installations) && count($installations)>0)
                {
                    foreach($installations as $installation)
                    {
                        /* @var $installation \app\modules\cpfbuilder\models\CpfInstallation */
                    ?>
                <tr>
                    <td><?= $installation->ident ?></td>
                    <td><?= $installation->type ?></td>
                    <td><?= $installation->installation_year ?></td>
                    <td><?= $installation->tof0->name ?></td>
                    <td><?= $installation->n_beds ?></td>
                    <td><?= '(' . $installation->lat . ', ' . $installation->lon .')' ?></td>
                    <td>
                        
                        <div class="button-group">
                        <?php
                        
                            if (!$readonly) {
                        
                            //cpfbuilder%2Fcpf-installations%2Fupdate&id=19
                                $url = \Yii::$app->urlManager->createUrl([
                                    '/cpfbuilder/cpf-installations/update', 'id'=>$installation->id
                                ]);
                                echo Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                    'class' => 'btn btn-xs btn-success',
                                    'title' => ucfirst(\Yii::t('app', 'modify {evt}', ['evt'=>lcfirst(\Yii::t('app', 'Information about installation'))]))
                                ]);
                            //cpfbuilder%2Fcpf-installations%2Fupdate&id=19
                                $url = \Yii::$app->urlManager->createUrl([
                                    '/cpfbuilder/cpf-installations/delete', 'id'=>$installation->id
                                ]);
                                echo Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                    'class' => 'btn btn-xs btn-danger',
                                    'title' => ucfirst(\Yii::t('app', 'Remove')) . ' ' . \Yii::t('app', 'installation'),

                                    'data' => [
                                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                        'method' => 'post',
                                    ],                                
                                ]);
                            }
                        ?>
                        </div>
                    </td>
                </tr>
                    <?php
                    }
                }
                else
                {                    
                    //create one row with one column spanned 6
                    //no data
                    $message = isset($installations) ? \Yii::t('app', 'no installation to report') : \Yii::t('app', '(not set)'); 
                    
                    ?>
                <tr>
                    <td colspan="6">
                        <em class="sy_alert_color"><?= $message ?></em>
                    </td>
                </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
        <!-- (end) table 2.1 section -->
    </div>
</div>