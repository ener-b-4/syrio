<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\cpfbuilder\models\CpfSection2 */
/* @var $readonly boolean */

$installations = $model->s_2_2_b_installations;

?>

<div class="row sy_margin_bottom_26" id="section2_2_b">
    <div class="col-lg-12">
        <p style="padding-top: 24px;">(b) <strong><?= \Yii::t('app/cpf', 'Fixed installations out of operation') ?></strong> 
            <span class="text-muted"><?= \Yii::t('app/cpf', 'Please report the installations that went out of offshore oil and gas operations during the reporting period') ?>.</span></p>

        <!-- (start) table 2.3 section -->
        <table class="table table-condensed">
           <thead>
             <tr>
                 <th colspan="6" style="vertical-align:top; text-align: center;">
                     <em><?= \Yii::t('app/cpf', 'Table {evt}', ['evt'=>'2.2.b']) ?></em>
                     <strong class="padding-bottom: 12px;"><?= \Yii::t('app/cpf', 'Installations that were decommissioned during the reporting period') ?></strong>
                 </th>
             </tr>
             <tr>
                <th style="vertical-align:top"> <?= \Yii::t('app/cpf', 'Name or ID') ?> </th>
                <th style="vertical-align:top"> <?= \Yii::t('app/cpf', 'Type of installation') ?>  </th>
                <th style="vertical-align:top"> <?= \Yii::t('app/cpf', 'Year of installation') ?>  </th>
                <th style="vertical-align:top"><?= \Yii::t('app/cpf', 'Coordinates') ?><br/><small class="text-muted"><?= \Yii::t('app/cpf', '(lon - lat)') ?></small></th>
                <th style="vertical-align:top"> <?= \Yii::t('app', 'Temporary') .  '/' . \Yii::t('app', 'Permanent') ?>  </th>
             </tr>
           </thead>
           <tbody>
                <?php
                if (isset($installations) && count($installations)>0)
                {
                    foreach($installations as $installation)
                    {
                        /* @var $installation \app\modules\cpfbuilder\models\CpfInstallation */
                    ?>
                <tr>
                    <td><?= $installation->ident ?></td>
                    <td><?= $installation->type ?></td>
                    <td><?= $installation->installation_year ?></td>
                    <td><?= '(' . $installation->lat . ', ' . $installation->lon .')' ?></td>
                    <td><?= $installation->decom_type == 1 ? \Yii::t('app', 'Temporary') : \Yii::t('app', 'Permanent') ?></td>
                
                    <td>
                        <div class="button-group">
                        <?php
                        
                        if (!$readonly) {
                        
                        //cpfbuilder%2Fcpf-installations%2Fupdate&id=19
                            $url = \Yii::$app->urlManager->createUrl([
                                '/cpfbuilder/cpf-installations/update', 'id'=>$installation->id
                            ]);
                            echo Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                'class' => 'btn btn-xs btn-success',
                                'title' => ucfirst(\Yii::t('app', 'modify {evt}', ['evt'=>lcfirst(\Yii::t('app', 'Information about installation'))]))
                            ]);
                            
                        //cpfbuilder%2Fcpf-installations%2Fupdate&id=19
                            $url = \Yii::$app->urlManager->createUrl([
                                '/cpfbuilder/cpf-installations/delete', 'id'=>$installation->id
                            ]);
                            echo Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                'class' => 'btn btn-xs btn-danger',
                                'title' => ucfirst(\Yii::t('app', 'Remove')) . ' ' . \Yii::t('app', 'installation'),

                                'data' => [
                                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                    'method' => 'post',
                                ],                                
                                
                                ]);
                        } //end readonly
                        ?>
                        </div>
                    </td>                
                
                
                </tr>
                    <?php
                    }
                }
                else
                {                    
                    //create one row with one column spanned 6
                    //no data
                    $message = isset($installations) ? \Yii::t('app', 'no installation to report') : \Yii::t('app', '(not set)'); 
                    
                    ?>
                <tr>
                    <td colspan="6">
                        <em class="sy_alert_color"><?= $message ?></em>
                    </td>
                </tr>
                    <?php
                }
                ?>
           </tbody>
        </table>
        <!-- (end) table 2.3 section -->
    <!-- (end) table 2.1 section -->
    </div>
</div>