<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\cpfbuilder\models\CpfSection2 */
/* @var $readonly boolean */

?>

<div class="row sy_margin_bottom_26" id="section2_2">
    <div class="col-lg-12">
        <p style="margin-bottom: 0px"><span class="text-info">2.2 </span><strong class="text-info" style="margin-bottom: 12px;"><?= \Yii::t('app/cpf', 'Changes since the previous reporting year') ?></strong></p>           
    </div>
    <div class="col-lg-12">
        <?= $this->render('s2_2_a', ['model'=>$model, 'readonly' => $readonly]) ?>
        <?= $this->render('s2_2_b', ['model'=>$model, 'readonly' => $readonly]) ?>
    </div>
</div>
