<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\cpfbuilder\models\CpfSection2 */

$title = \Yii::t('app/cpf', 'installations');
$this->title = \Yii::t('app/crf', 'Section {evt}', ['evt'=>'2']);

?>
<div class="cpf-section2-view">

    <div class="row">
        <div class="col-xs-9 text-center">
            <div class="cpf-section-number"><?= strtoupper(Html::encode($this->title)) ?></div>
            <div class="cpf-section-title"><?= strtoupper(Html::encode($title)) ?></div>
        </div>
        <div class="col-xs-3">
            <div class="btn-group pull-right">
                <?= Html::a(Yii::t('app', 'Update'), ['update', 'year' => $model->year, 'user_id' => $model->user_id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'year' => $model->year, 'user_id' => $model->user_id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                        'method' => 'post',
                    ],
                ]) ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 cpf-para p1" id="cpf-section-2-1-container">
            <ul class="list list-unstyled">
                <li class="p2">
                    Fixed installations
                </li>
                <li class="p2">
                    Changes since the previous reporting year
                    <ul class="list list-unstyled">
                        <li class="p3">
                            Changes since the previous year
                            <ul class="list list-unstyled">
                                <li class="p4">
                                    New fixed installations fdsfsd fsdf sdf sdf sd fsd f sdf sdf sd f sdf f sdf sd ff sdf dsf sd fs ff sdf
                                </li>
                                <li class="p4">
                                    Fixed installations out of operation
                                </li>
                            </ul>
                        </li>
                        <li>
                            <div class="p3">
                                Mobile installations
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        
        <!--
        <div class="p1">
            first paragraphed
            <div class="p2">
                para 1.1
            </div>
            <div class="p2">
                para 1.2
            </div>
        </div>
        <div class="p1">
            first paragraphed
            <div class="p2">
                para 1.1
            </div>
            <div class="p2">
                para 1.2
            </div>
        </div>
        -->
        
        <div class="col-lg-12">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'year',
                    'user_id',
                    'norm_ktoe',
                    'norm_oil_q',
                    'norm_oil_u',
                    'norm_oil_n',
                    'norm_gas_q',
                    'norm_gas_u',
                    'norm_gas_n',
                ],
            ]) ?>
        </div>
    </div>


</div>
