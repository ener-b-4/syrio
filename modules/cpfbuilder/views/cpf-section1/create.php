<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\cpfbuilder\models\CpfSection1 */

$this->title = Yii::t('app/cpf', 'Create Cpf Section1');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/cpf', 'Cpf Section1s'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cpf-section1-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
