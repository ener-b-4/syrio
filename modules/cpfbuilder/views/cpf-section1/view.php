<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\cpfbuilder\models\CpfSection1 */

$title = \Yii::t('app/cpf', 'profile');
$this->title = \Yii::t('app/crf', 'Section {evt}', ['evt'=>'1']);

$ms = app\modules\management\models\Country::findOne(['iso2'=>$model->ms]);
if (isset($ms)) {
    $ms_full = $ms->short_name . ' ('.$model->ms.')';
} else {
    $ms_full = 'UKNOWN COUNTRY!' . ' ('.$model->ms.')';
}
?>
<div class="cpf-section1-view">
    

    <div class="row">
        <div class="col-sm-12 s41content sy_pad_top_36" id='s41_div' style="margin-bottom: 26px;">
            <div class="row">
                <div class="col-xs-9">
                    <p>
                        <span class="cpf-section-title"><?= ucfirst(\Yii::t('app/crf', 'Section {evt}', ['evt'=>'1'])) ?></span>
                        <span class="cpf-section-subtitle"><?= \Yii::t('app/cpf', 'PROFILE') ?></span>
                    </p>
                   <p><strong class="text-info" style="margin-bottom: 12px;"><?= \Yii::t('app/cpf', 'Information on Member State and Reporting Authority') ?></strong></p>
                </div>
                <div class="col-xs-3">
                </div>
            </div>
            
            <div><span style="padding-right: 6px;">(a)</span> <?= \Yii::t('app/cpf', 'Member State') ?>: <?= $ms_full ?> </div>
            <div><span style="padding-right: 6px;">(b)</span> <?= \Yii::t('app/cpf', 'Reporting period')?> (<?= \Yii::t('app/cpf', 'Calendar Year') ?>): <?= $model->year ?></div>
            <div><span style="padding-right: 6px;">(c)</span> <?= \Yii::t('app', 'Competent Authority') ?>: <?= $model->ca ?></div>
            <div><span style="padding-right: 6px;">(d)</span> <?= \Yii::t('app/cpf', 'Designated Reporting Authority') ?>: <?= $model->dra ?></div>
            <div>
                <span style="padding-right: 6px;">(e)</span> <?= \Yii::t('app/crf', 'Contact details') ?>
                <br/>
                <span style="margin-left: 29px; margin-top: 12px;"><?= \Yii::t('app/crf', 'Telephone number') ?>: <?= $model->phone ?></span>
                <br/>
                <span style="margin-left: 29px;"><?= \Yii::t('app/crf', 'E-mail address') ?>: <?= Html::a($model->email, "mailto:'" . $model->email)?></span>
                <br/>
            </div>
        </div>
    </div>
</div>

