<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\modules\cpfbuilder;

use yii\base\Module;

/**
 * Description of CpfBuilder
 *
 * @author vamanbo
 */
class cpf_builder extends Module
{
    public $controllerNamespace = 'app\modules\cpfbuilder\controllers';

    public function init()
    {
        parent::init();
    }
    
    
}
