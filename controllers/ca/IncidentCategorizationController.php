<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\controllers\ca;

use Yii;
use app\models\ca\IncidentCategorization;
use app\models\ca\IncidentCategorizationSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\common\ActionMessage;
use app\models\events\drafts\EventDraft;
use app\models\events\EventDeclaration;

use app\models\cpf\CpfSection4;
use yii\helpers\Url;

use app\components\Errors;
use app\models\email\EmailDispacher;

/**
 * IncidentCategorizationController implements the CRUD actions for IncidentCategorization model.
 */
class IncidentCategorizationController extends Controller
{
    
    public function beforeAction($action) {
        if (\Yii::$app->user->isGuest || !(\Yii::$app->user->identity->isCaUser || !\Yii::$app->user->can('sys_admin'))) {
            throw new NotFoundHttpException(\Yii::t('app', 'The requested page does not exist.'));
        }
        return parent::beforeAction($action);
    }
    
    
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                //only authenticated users
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'clearHistory' => ['post']
                ],
            ],
        ];
    }

    /**
     * Lists all IncidentCategorization models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new IncidentCategorizationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single IncidentCategorization model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }


    /**
     * Updates an existing IncidentCategorization model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if (!(\Yii::$app->user->can('ca_assessor')) )
        {
            return \Yii::$app->error->ThrowError(\app\components\Errors::RESTRICTED_OPERATION_ERROR);
        }        
        
        $actionMessage = new ActionMessage();
        
        $model = $this->findModel($id);

        //check if there's already a cpf_crf assessment of the model
        $cpf_crf_assessment = $model->cpfAssessment;
        
        
        
        if ($model->load(Yii::$app->request->post())) {
//            echo '<pre>';
//            echo print_r($_POST);
//            echo '</pre>';
            
            $transaction = \Yii::$app->db->beginTransaction();
            
            //$model->setScenario('submit');
            if (intval($_POST['hi_draft']) !== 1) {
            //return $this->redirect(['view', 'id' => $model->id]);
                $model->is_major = ($model->is_major_a 
                        || $model->is_major_b 
                        || $model->is_major_c
                        || $model->is_major_d
                        || $model->is_major_e
                        || $model->is_major_f
                        || $model->is_major_g
                        || $model->is_major_h
                        || $model->is_major_i
                        || $model->is_major_j);
                     
                $model->save_as_draft = 0;
                
                $draft = $model->draft;
                $draft->ca_status = EventDeclaration::INCIDENT_REPORT_ACCEPTED;
                $draft->ca_rejection_desc = '';
                if (!$draft->save()) {
                    $ok = 0;
                }
                
                $message = '';
                $message_title = '';
                                
                $model->setScenario('submit');
                $model->is_done=1;

                //here I should check the dirtyAttributes
                $model->validate();
                

                $message_title = Yii::t('app', 'Event assessed successfully!');
                
                //assessed for CA
                $description = [
                    'as' => $model->is_major ? \Yii::t('app', 'Major Accident') : \Yii::t('app', 'Minor Incident'),
                    'by' => \Yii::$app->user->identity->full_name,
                    'at' => date('Y-m-d H:i:s', \time()),
                ];

                //log the event history
                if (!\app\models\ReportingHistory::addHistoryItem(
                        $model->report->event_id, $model->draft_id, 
                        \app\models\ReportingHistory::EVENT_ASSESSED, 
                        json_encode($description), 1))
                {
                        $ok=0;
                }
                
                //inform the oo that the report has been accepted (if not previously done)
                if (\app\models\ReportingHistory::find()
                        ->where(['evt_id' => $model->draft->event_id])
                        ->andWhere(['draft_id' => $model->draft_id])
                        ->andWhere(['event' => \app\models\ReportingHistory::EVENT_ACCEPTED])->count() < 1) {

                    $description = [
                        //'by' => \Yii::$app->user->identity->full_name,
                        'at' => date('Y-m-d H:i:s', \time()),
                    ];

                    //log the event history
                    if (!\app\models\ReportingHistory::addHistoryItem(
                            $model->report->event_id, $model->draft_id, 
                            \app\models\ReportingHistory::EVENT_ACCEPTED, 
                            json_encode($description)))
                    {
                            $ok=0;
                    }

                    //vamanbo 16/11/05 - email notification
                    EmailDispacher::sendCaAcceptsReport($model);
                }
                
            }
            else
            {
                if ($model->is_done === IncidentCategorization::INCIDENT_REPORT_ASSESSED) {
                    //assessed for CA
                    $description = [
                        'by' => \Yii::$app->user->identity->full_name,
                        'at' => date('Y-m-d H:i:s', \time()),
                    ];

                    //log the event history
                    // FIX 2024-05-16.002 vamanbo
                    \app\models\ReportingHistory::addHistoryItem(
                            $model->report->event_id, $model->draft_id, 
                            \app\models\ReportingHistory::EVENT_MA_REOPENED, 
                            json_encode($description), 1);
                }
                
                
                $message_title = Yii::t('app', 'Draft saved for {evt}!', ['evt' => $model->name]);
                
                $model->save_as_draft = 1;
                $model->is_done=0;
            }
            if ($model->validate()) 
            {
                if ($model->save()) {
                    
                    //get the message from Session
                    $session= \Yii::$app->session;
                    
                    //get the message from session
                    $message = $session->getFlash('update_msg');
                    
                    $actionMessage->SetSuccess(
                            $message_title, $message);
                    Yii::$app->session->set('finished_action_result', $actionMessage);                    

                    Yii::$app->session->set('finished_action_result', $actionMessage);
                    
                    //write the history
                    //accepted for both
                    //IF not already accepted
                    
                    $ok = 1;
                    
                }
                
                //update the cpf information
                //UpdateCpfData($model);
                if ($ok == 1) {
                    $transaction->commit();
                } else {
                    $transaction->rollBack();
                }
                
                return $this->render('view', ['model'=>$model]);
            }
            else
            {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        } else {
            //save as draft 
            
            /*  check if the report had been accounted in the list for CPF CRF
                and pass the string to be prompted if saved as draft*/
            if (isset($cpf_crf_assessment)) {
                $promptMessage = \Yii::t('app', 'Saving this as draft will lead to CHANGES of the report statu in the CPF preparation list. Proceed?');
                return $this->render('update', [
                    'model' => $model,
                    'promptMessage' => $promptMessage
                ]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
            
        }
    }
    
    public function actionSaveDraft($id)
    {
        if (!(\Yii::$app->user->can('ca_assessor')) )
        {
            return \Yii::$app->error->ThrowError(\app\components\Errors::RESTRICTED_OPERATION_ERROR);
        }        
        
        $model = $this->findModel($id);
        
        $transaction = Yii::$app->db->beginTransaction();
        
        if ($model->load(Yii::$app->request->post())) {
            //return $this->redirect(['view', 'id' => $model->id]);
            $model->is_major = ($model->is_major_a 
                    || $model->is_major_b 
                    || $model->is_major_c
                    || $model->is_major_d
                    || $model->is_major_e
                    || $model->is_major_f
                    || $model->is_major_g
                    || $model->is_major_h
                    || $model->is_major_i
                    || $model->is_major_j);
            
            //$model->setScenario('submit');
            $model->setScenario('default');
            if ($model->validate()) 
            {
                if (count($model->dirtyAttributes)>0) {
                    echo '<pre>';
                    echo var_dump($model->dirtyAttributes);
                    echo '</pre>';
                } else {
                    echo '<pre>';
                    echo 'no dirty attributes';
                    echo '</pre>';
                }
                
                die();
                
                
                $model->is_done=0;
                $model->save();
                
                /*
                 * disabled since 1.3
                 */
                //$draft = $model->draft;
                //$draft->ca_status = EventDeclaration::INCIDENT_REPORT_ACCEPTED;
                //$draft->ca_rejection_desc = '';
                //$draft->save();
                
                $actionMessage->SetSuccess(
                        Yii::t('app', 'Draft saved for {evt}!', ['evt' => $model->name]), '');
                Yii::$app->session->set('finished_action_result', $actionMessage);
                
                //update the cpf information
                //UpdateCpfData($model);

                $transaction->commit();
                
                return $this->render('view', ['model'=>$model]);
            }
        }
        
        else {
            echo 'not loaded';
        }
    }
    
    
    /**
     * Deletes an existing IncidentCategorization model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if (\Yii::$app->user->isGuest || !(\Yii::$app->user->identity->isCaUser || !\Yii::$app->user->can('sys_admin'))) {
            throw new NotFoundHttpException(\Yii::t('app', 'The requested page does not exist.'));
        }
        
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    
    public function actionReject($id)
    {
        if (!(\Yii::$app->user->can('ca_assessor')) )
        {
            return \Yii::$app->error->ThrowError(\app\components\Errors::RESTRICTED_OPERATION_ERROR);
        }        
        
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()))
        {
            $model->setScenario('reject');

            if (!$model->validate())
            {
                return $this->render('reject-form', ['model'=>$model]);
            }
            else
            {
                $actionMessage = new ActionMessage();
                $transaction = \Yii::$app->db->beginTransaction();
                
                try
                {
                    //set the other properties
                    $model->is_done = IncidentCategorization::INCIDENT_REPORT_REJECTED;
                    $model->is_major = NULL;

                    if (!$model->save())
                    {
                        $transaction->rollBack();
                        
                        $actionMessage->SetError('Unable to perform request!', 'Something went wrong when processing the request. Please retry later.');
                        Yii::$app->session->set('finished_action_result', $actionMessage);
                        return $this->render('view', ['model'=>$model]);
                    }
                    
                    //set the event draft
                    $eventDraft = $model->draft;
                    $eventDraft->ca_status = \app\models\events\EventDeclaration::INCIDENT_REPORT_REJECTED;
                    $eventDraft->ca_rejection_desc = $model->rejection_desc;
                    $eventDraft->status = \app\models\events\EventDeclaration::INCIDENT_REPORT_DRAFT;
  
                    //vamanbo 20160225
                    //put to draft any section of the event
                    foreach(range('a', 'j') as $letter) {
                        if ($letter !== 'g' && $letter !== 'h') {
                            $attr = 'is_'.$letter;
                            $section = 'section'.strtoupper($letter);
                            if ($eventDraft->$attr) {
                                $eventDraft->$section->status = EventDeclaration::INCIDENT_REPORT_DRAFT;
                                if (!$eventDraft->$section->save()) {
                                    die('error on saving');
                                }
                            }
                        }
                    }
                    
                    $event = $eventDraft->event;
                    //(start vamanbo 1909)
                    $event->status = \app\models\events\EventDeclaration::INCIDENT_REPORT_REJECTED;
                    
                    //write the history
                    $ok = 1;
                    
                    $description = [
                        'by' => \Yii::$app->user->identity->full_name,
                        'at' => date('Y-m-d H:i:s', \time()),
                        'Justification' => $model->rejection_desc,
                    ];

                    $json_fix = strlen(json_encode($description))-255;
                    if ($json_fix>0) {
                        $description['Justification'] = substr($description['Justification'], 0, strlen($description['Justification'])-$json_fix-10) . '[...]';
                    } 

                    //log the event history
                    if (!\app\models\ReportingHistory::addHistoryItem(
                            $model->report->event_id, $model->draft_id, 
                            \app\models\ReportingHistory::EVENT_REJECTED, 
                            json_encode($description)))
                    {
                            $ok=0;
                    }
                    
                    
                    
                    if (!$eventDraft->save() || !$event->save() || $ok === 0)
                    {
                        //(end vamanbo 1909)
                        $transaction->rollBack();                        
                        
                        $actionMessage->SetError('Unable to perform request!', 'Something went wrong when processing the request. Please retry later.');
                        Yii::$app->session->set('finished_action_result', $actionMessage);
                        return $this->render('view', ['model'=>$model]);
                    }
                    
                    $transaction->commit();
                    $actionMessage->SetSuccess(\Yii::t('app', 'Incident report rejected!'), \Yii::t('app', 'You have rejected the incident report.'));
                    Yii::$app->session->set('finished_action_result', $actionMessage);

                    //vamanbo 16/11/05 - email notification
                    EmailDispacher::sendCaRejectsReport($model);
                    
                    return $this->render('view', ['model'=>$model]);
                    
                } catch (Exception $ex) {
                    $transaction->rollBack();
                }
            }
        }
        else
        {
            $model->setScenario('reject');
            return $this->render('reject-form', ['model'=>$model]);
        }
    }
    
    
    
    /**
     * Finds the IncidentCategorization model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return IncidentCategorization the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = IncidentCategorization::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    
    public function actionPdf($id)
    {
        $model = $this->findModel($id);
        
        $mpdf = new \mPDF('utf-8', 'A4');
        
        $mpdf->WriteHTML($this->render('view_pdf', [
          'model' => $model,
        ]));      

        $mpdf->Output();  // Output inside the page
        exit;
    }

    public function actionSection($id, $section)
    {
        //echo 'sono qui';
        //die();
        //$id = 53;
        $model = $this->findModel($id);
        //return $this->render(['@app/views/ca/incident-categorization/sections/section-a',
        return $this->render('viewSections/section' . strtoupper($section), [
            'model' => $model,
            'draft' => $model->draft,
        ]);
        
        //echo $this->render('@app/modules/pdfgenerator/views/sections/a/view-pdf-a1', ['model'=>$model->sectionA->a1, 'isCampactPrint'=>$isCampactPrint]); 
    }

    
    public function actionHistory($id) {
        
        //no need for testing user since this is done in the beforeAction and ALL the ca_users may view the reporting history
        
        //if (!\Yii::$app->user->can('ca_user') || !Yii::$app->user->can('sys_admin')) {
            //return \Yii::$app->error->ThrowError(Errors::RESTRICTED_AREA_ERROR);
        //} else {
        $model = $this->findModel($id);                             //incident_categorization
        //get the evt_id corresponding to this incident_categorization
        $evt_id = $model->event->id;
        
        $params = Yii::$app->request->queryParams;
        $searchModel = new \app\models\ReportingHistorySearch();
        $searchModel->evt_id = $evt_id;
        
        $dataProvider = $searchModel->search($params);

        return $this->render('history', [
            'searchModel' => $searchModel, 
            'dataProvider' => $dataProvider, 
            'evt_id'=>$evt_id,
            'evt_name' => $model->name,
            'incident_cat_id' => $model->id]);
        //}
    }
    
    public function actionClearHistory($id) {
        //this can only be done by ca_admin or sys_admin
        if (!(\Yii::$app->user->can('ca_admin') || \Yii::$app->user->can('sys_admin'))) {
            return \Yii::$app->error->ThrowError(Errors::RESTRICTED_AREA_ERROR);
        }
        
        $actionMessage = new ActionMessage();
        
        //delete all the history records that are for the CA alone
        
        $theId = intval($id);
        
        $event_id = $this->findModel($id)->event->id;
        
        $transaction = \Yii::$app->db->beginTransaction();
        
        if (\app\models\ReportingHistory::deleteAll('evt_id = :evt_id AND visibility = :visibility', [
            'evt_id' => $event_id,
            'visibility' => \app\models\ReportingHistory::VISIBLE_CA
        ])) {
        
            $transaction->commit();
            $actionMessage->SetSuccess(\Yii::t('app', 'Clear history'), \Yii::t('app', 'The CA related history has been cleared!'));
            Yii::$app->session->set('finished_action_result', $actionMessage);

        } else {
            
            $transaction->rollBack();
            
            $msg = \app\components\helpers\TGenericMessage::Error(true);
            
            $actionMessage->SetError(\Yii::t('app', 'Clear history'), $msg);
            Yii::$app->session->set('finished_action_result', $actionMessage);
            
        }
        
        return $this->redirect(['history', 'id'=>$id]);
    }
    
}

/*20150930*/