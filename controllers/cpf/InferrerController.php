<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\controllers\cpf;

use app\models\ca\IncidentCategorization;
use app\models\events\EventDeclaration;
use app\models\events\drafts\EventDraft;
use app\models\cpf\CpfSection4;

use yii\data\ActiveDataProvider;

class InferrerController extends \yii\web\Controller
{
    /**
     * Infers the CPF data for $year
     * 
     * @param integer $year
     */
    public function actionInferCpf($year)
    {
        /*
         * try to get the CpfSection4 corresponding to $year;
         * create it otherwise
         */
        
        CpfSection4::deleteAll(['year'=>$year]);
        //if (!isset($cpf_section4)) {
            $cpf_section4 = new CpfSection4();
            //set the id as the year
            $cpf_section4->year = $year;
            $cpf_section4->save();
        //}
        
        /*
         * initialize local variables
         */
        
        /*
         * ====> Get ALL the events:
         * - reported to the CA in $year;
         * - finalized (assessed) by the CA;
         * 
         */
        $query = IncidentCategorization::find()
                ->join('join','event_classifications','ca_incident_cat.draft_id = event_classifications.id')
                ->join('join','event_declaration','event_declaration.id = event_classifications.event_id')
                ->where('ca_incident_cat.is_done = :p1', [':p1'=>1])
                ->andWhere(['like', 'event_declaration.event_date_time', $year]);
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $ca_events = $dataProvider->getModels();
        /*
         * Get ALL the events <=====
         */
        

        /*
         * ====> Create the arrays of models:
         * - $accidents[] the array with events NON Major Accidents
         * - $major_accidents[] the array with events NON Major Accidents
         */
        $accidents = [];
        $major_accidents = [];
        foreach ($ca_events as $ca_event)
        {
            $ca_event->is_major == 1 ? $major_accidents[] = $ca_event : $accidents[] = $ca_event;
        }
        /*
         * Create the arrays of models <====
         */
        
        
        /*
         * start augmenting CpfSection4
         */
        
        $cpf_section4->s4_1_n_events_total = count($ca_events);
        $cpf_section4->s4_1_n_major_accidents = count($major_accidents);
        
        $letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j'];
        /*
         * Compute 4.2.{a-j}.totals
         */
        foreach($accidents as $accident)
        {
            $draft = $accident->draft;
            $event = $draft->event;
            foreach ($letters as $letter)
            {
                $is_attr = 'is_'.$letter;                   //event.is_a
                $total_attr = 's4_2_'.$letter.'_total';     //s4_2_a_total
                
                if ($draft->$is_attr == 1) {
                    $cpf_section4->$total_attr += 1;
                    //augment the specific 4.2.{letter}
                    $this->classify_4_2_x($letter, $draft, $cpf_section4);
                }
            }
        }
        
        foreach($major_accidents as $accident)
        {
            $draft = $accident->draft;
            $event = $draft->event;
            foreach ($letters as $letter)
            {
                $is_attr = 'is_'.$letter;                   //event.is_a
                $total_attr = 's4_2_'.$letter.'_total';     //s4_2_a_total
                
                if ($draft->$is_attr == 1) {
                    $cpf_section4->$total_attr += 1;
                    //augment the specific 4.2.{letter}
                    $this->classify_4_2_x($letter, $draft, $cpf_section4);
                    if ($letter == 'c')
                    {
                        $this->classify_4_4_x($letter, $draft, $cpf_section4);
                    }
                    if ($letter == 'a')
                    {
                        $this->infer_cpf_4_5($draft, $cpf_section4);
                    }
                }
            }
        }
        
        
        
        //echo '<pre>';
        //echo var_dump($cpf_section4);
        //echo '</pre>';
        
        $cpf_section4->save();

        $url = \Yii::$app->urlManager->createAbsoluteUrl(['cpf/cpf-section-4/view', 'id'=>$cpf_section4->year]);
        return $this->redirect($url);
        
        echo 'total number of events for the CA in ' . $year . ': ' . count($ca_events) . '<br/>';
        echo 'of which: ' . count($accidents) . ' minor accidents and ' . count($major_accidents) . ' Major Accidents' . '<br/>';
        die();
        
        
        //return $this->render('infer', ['$model'=>$cpf_section4]);
    }
    
    public function actionIndex()
    {
        echo 'here';
        die();
    }
    
    
    /**
     * 
     * @param string $letter {a-j}
     * @param \app\models\events\drafts\EventDraft $draft
     * @param byref \app\models\cpf\CpfSection4 $cpf_section4
     */
    private function classify_4_2_x($letter, $draft, &$cpf_section4)
    {
        //only a, b, d sections must be evaluated here!
        switch ($letter)
        {
            case 'a':
                /* @var $sectionA \app\models\events\drafts\sections\SectionA */        
                //get SectionA
                $sectionA = $draft->sectionA;
                
                //echo '<pre>';
                //echo var_dump($sectionA);
                //echo '</pre>';
                //die();
                
                if ($sectionA->a1->axii_flash_index ||
                        $sectionA->a1->axii_jet_index ||
                        $sectionA->a1->axii_pool_index)
                {
                    $cpf_section4->s4_2_a_if += 1;
                }
                if ($sectionA->a1->axii_exp_index)
                {
                    $cpf_section4->s4_2_a_ix += 1;
                }

                //check for $cpf_section4->s4_2_a_nig
                //the release MUST NOT be IGNITED!
                if ($sectionA->a1->a1i_g == 1 &&
                        $sectionA->a1->axii_exp_index == 0 &&
                        $sectionA->a1->axii_flash_index == 0 &&
                        $sectionA->a1->axii_jet_index == 0 &&
                        $sectionA->a1->axii_pool_index == 0
                        )
                {
                        $cpf_section4->s4_2_a_nig += 1;
                }

                //check for $cpf_section4->s4_2_a_nio
                //the release MUST NOT be IGNITED!
                if ($sectionA->a1->a1i_o == 1 &&
                        $sectionA->a1->axii_exp_index == 0 &&
                        $sectionA->a1->axii_flash_index == 0 &&
                        $sectionA->a1->axii_jet_index == 0 &&
                        $sectionA->a1->axii_pool_index == 0
                        )
                {
                        $cpf_section4->s4_2_a_nio += 1;
                }
                
                //check for $cpf_section4->s4_2_a_haz
                //H2S is considered hazmat (=> gas)
                // + numarul de evenimente in care este declarat release od non-hc hazmat (A.2.1_1 = 1)
                
                if ($sectionA->a1->a1i_g == 1 ||
                        $sectionA->a1->a1i_2p == 1 ||
                        $sectionA->a2->a2_1 == 1
                        )
                {
                        $cpf_section4->s4_2_a_haz += 1;
                }
                
                break;
            case 'b':
                /*
                 * to compute:
                 * - $cpf_section4->s4_2_b_total;    //previously computed
                 * - $cpf_section4->s4_2_b_bo;       //CANNOT be computed; the only information on blow-outs is given in CRF B.2.1_1
                 *                                   //However, CRF B.2.1_1 is also required by CPF s4_2_b_bda
                 * - $cpf_section4->s4_2_b_bda;      //activation of BOP/Diverter systems
                 *                                   // = number of SectionB>b2_1 == 1 OR SectionB>b2_2 == 1
                 * - $cpf_section4->s4_2_b_wbf;      //well barrier failures
                 *                                   // = number of SectionB>b2_4 == 1
                 */
                
                /* @var $sectionB \app\models\events\drafts\sections\SectionA */        
                $sectionB = $draft->sectionB;
                
                if ($sectionB->b2->b2_1 == 1 || $sectionB->b2->b2_2 == 1)
                {
                    $cpf_section4->s4_2_b_bda += 1;
                }
                
                if ($sectionB->b2->b2_4_a == 1 
                        || $sectionB->b2->b2_4_b == 1
                        || $sectionB->b2->b2_4_c == 1)
                {
                    $cpf_section4->s4_2_b_wbf += 1;
                }
                break;
            case 'd':
                /*
                 * to compute:
                 * - $cpf_section4->s4_2_d_si;   //structural integrity                                            
                 * - $cpf_section4->s4_2_d_sb;   //stability/buoyancy
                 * - $cpf_section4->s4_2_d_sk;   //station keeping
                 * 
                 * NONE of these can be computed from CRF
                 */
                break;
        }
    }
    
    
    /**
     * 
     * @param string $letter {a-j}
     * @param \app\models\events\drafts\EventDraft $draft
     * @param \app\models\cpf\CpfSection4 $cpf_section4
     */
    private function classify_4_4_x($letter, $draft, &$cpf_section4)
    {
        //var_dump($draft->sectionC->c2->sC2_1->seces);
        $add_a = 0;
        $add_b = 0;
        $add_c = 0;
        $add_d = 0;
        $add_e = 0;
        $add_f = 0;
        $add_g = 0;
        $add_h = 0;
        $add_i = 0;
        $add_j = 0;
        $add_k = 0;
        $add_l = 0;
                
        foreach($draft->sectionC->c2->sC2_1->seces as $sece) 
        {
            if ($sece->id>100 && $sece->id<200) { $add_a = 1; }
            else if ($sece->id>200 && $sece->id<300) { $add_b = 1; }
            else if ($sece->id>300 && $sece->id<400) { $add_c = 1; }
            else if ($sece->id>400 && $sece->id<500) { $add_d = 1; }
            else if ($sece->id>500 && $sece->id<600) { $add_e = 1; }
            else if ($sece->id>600 && $sece->id<700) { $add_f = 1; }
            else if ($sece->id>700 && $sece->id<800) { $add_g = 1; }
            else if ($sece->id>800 && $sece->id<900) { $add_h = 1; }
            else if ($sece->id>900 && $sece->id<1000) { $add_i = 1; }
            else if ($sece->id>1000 && $sece->id<1100) { $add_j = 1; }
            else if ($sece->id>1100 && $sece->id<1200) { $add_k = 1; }
            else if ($sece->id>1200 && $sece->id<1300) { $add_l = 1; }
            
            //echo $sece->id . '<br/>';
        }
        
        /* only for testing
        $letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l'];
        foreach ($letters as $lett)
        {
            $attr = 'add_'.$lett;
            echo $attr.': ' . ${$attr} . '<br/>';
            
        }
        */
        
        $cpf_section4->s4_4_nsis += $add_a;
        $cpf_section4->s4_4_npcs += $add_b;
        $cpf_section4->s4_4_nics += $add_c;
        $cpf_section4->s4_4_nds += $add_d;
        $cpf_section4->s4_4_npcrs += $add_e;
        $cpf_section4->s4_4_nps += $add_f;
        $cpf_section4->s4_4_nsds += $add_g;
        $cpf_section4->s4_4_nnavaids += $add_h;
        $cpf_section4->s4_4_roteq += $add_i;
        $cpf_section4->s4_4_eere += $add_j;
        $cpf_section4->s4_4_ncoms += $add_k;
        $cpf_section4->s4_4_nother += $add_l;
        
        //var_dump($draft->sectionC->c2->sC2_1->seces);
        //die();
    }
    
    /**
     * 
     * @param string $letter {a-j}
     * @param \app\models\events\drafts\EventDraft $draft
     * @param \app\models\cpf\CpfSection4 $cpf_section4
     */
    private function infer_cpf_4_5($draft, &$cpf_section4)
    {
        /* @var $sectionA \app\models\events\drafts\sections\SectionA */        
        
        $sectionA = $draft->sectionA;

        
        //get the totals
        if ($sectionA->a1->a1_fail_design_code == 2 ||
                $sectionA->a1->a1_fail_design_code == 1000 ||
                $sectionA->a1->a1_fail_equipment_code == 2 ||
                ($sectionA->a1->a1_fail_equipment_code >= 1100 && $sectionA->a1->a1_fail_equipment_code<1200))
        {
            $cpf_section4->s4_5_a_total += 1;
        }
        
        if ($sectionA->a1->a1_fail_operation_code == 2 ||
                ($sectionA->a1->a1_fail_operation_code >= 1200 && $sectionA->a1_fail_operation_code<1300))
        {
            $cpf_section4->s4_5_b_total += 1;
        }
        
        if ($sectionA->a1->a1_fail_procedural_code == 2 ||
                ($sectionA->a1->a1_fail_procedural_code >= 1200 && $sectionA->a1->a1_fail_procedural_code<1300))
        {
            $cpf_section4->s4_5_c_total += 1;
        }
        
        
        $cpf_section4->{'s4_5_a_co__ext'} += $sectionA->a1->a1_fail_equipment_code == 1101 ? 1 : 0;
        $cpf_section4->{'s4_5_a_co__int'} += $sectionA->a1->a1_fail_equipment_code == 1100 ? 1 : 0;
        //$cpf_section4->{'s4_5_a_csf'} = 0;  //control system failure
        $cpf_section4->{'s4_5_a_df'} += $sectionA->a1->a1_fail_design_code == 1000 ? 1 : 0;
        //$cpf_section4->{'s4_5_a_if'} = 0;   //instrument failure
        $cpf_section4->{'s4_5_a_mf__dm'} += $sectionA->a1->a1_fail_design_code == 1105 ? 1 : 0;
        $cpf_section4->{'s4_5_a_mf__f'} += $sectionA->a1->a1_fail_design_code == 1102 ? 1 : 0;
        //$cpf_section4->{'s4_5_a_mf__vh'} = 0;
        $cpf_section4->{'s4_5_a_mf__wo'} += $sectionA->a1->a1_fail_design_code == 1103 ? 1 : 0;;
        $cpf_section4->{'s4_5_a_other'} += $sectionA->a1->a1_fail_design_code == 2 
                || $sectionA->a1->a1_fail_design_code == 1104 ? 1 : 0; 
        
        
        //$cpf_section4->{'s4_5_b_err__dsgn'} = 0;
        $cpf_section4->{'s4_5_b_err__insp'} += $sectionA->a1->a1_fail_equipment_code == 1202 ? 1 : 0;
        $cpf_section4->{'s4_5_b_err__mnt'} += $sectionA->a1->a1_fail_equipment_code == 1205 ? 1 : 0;
        $cpf_section4->{'s4_5_b_err__op'} += $sectionA->a1->a1_fail_equipment_code == 1200
                || $sectionA->a1->a1_fail_equipment_code == 1201
                || $sectionA->a1->a1_fail_equipment_code == 1204
                || $sectionA->a1->a1_fail_equipment_code == 1206
                || $sectionA->a1->a1_fail_equipment_code == 1208 ? 1 : 0;
        $cpf_section4->{'s4_5_b_err__tst'} += $sectionA->a1->a1_fail_equipment_code == 1203 ? 1 : 0;
        $cpf_section4->{'s4_5_b_other'} += $sectionA->a1->a1_fail_equipment_code == 2
                || $sectionA->a1->a1_fail_equipment_code == 1207 ? 1 : 0;
        
        
        //$cpf_section4->{'s4_5_c_inq__com'} = 0;
        $cpf_section4->{'s4_5_c_inq__instp'} += $sectionA->a1->a1_fail_procedural_code == 1302 ? 1 : 0;
        //$cpf_section4->{'s4_5_c_inq__pc'} = 0;
        //$cpf_section4->{'s4_5_c_inq__rap'} = 0;
        //$cpf_section4->{'s4_5_c_inq__safelead'} = 0;
        //$cpf_section4->{'s4_5_c_inq__sup'} = 0;
        $cpf_section4->{'s4_5_c_nc__prc'} += $sectionA->a1->a1_fail_procedural_code == 1300 ? 1 : 0;
        $cpf_section4->{'s4_5_c_nc__ptw'} += $sectionA->a1->a1_fail_procedural_code == 1301 ? 1 : 0;
        $cpf_section4->{'s4_5_c_other'} += $sectionA->a1->a1_fail_procedural_code == 2 ? 1 : 0;

    }    
}
