<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\controllers\admin\users;

use yii\web\Controller;
use app\models\User;    // app\models\admin\users\User;
use app\models\admin\users\CaUser;
use app\models\admin\users\CaUserSearch;
use yii\data\ActiveDataProvider;
use \app\models\admin\rbac\UserAssignment;

use app\models\admin\users\SignupCaUserForm;
use app\models\admin\rbac\Role;

use \app\models\admin\users\UsersCaOrganization;
use app\components\behaviors\EditableCaUserBehavior;

use app\models\common\ActionMessage;
use yii\filters\VerbFilter;
use Yii;

//use \app\modules\management\models\CompetentAuthorityOrganization;
use \app\modules\management\models\Organization;
use \app\models\Installations;
use yii\helpers\Url;
use app\models\UserSearch;
use yii\helpers\Html;
use app\models\email\EmailDispacher;

use yii\web\NotFoundHttpException;

use app\components\Errors;

/**
 * UserController
 *
 * version 16 July 2015
 * @author bogdanV
 */
class UserController extends Controller{
    
    /**
     * SyRIO
     * 
     * Date added: 09 July 2015
     * 
     * bv
     * @return type
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                //only authenticated users
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'create-ca-for' => ['post'],
                ],
            ],
        ];
    }
    
    
    public function actionAccount()
    {
        return $this->render('account', ['model' => Yii::$app->user->identity]);
    }
    
    public function actionChange_contact_details() {
        $model = new \app\models\forms\ChangeOwnDetails();
        $model->first_name = \Yii::$app->user->identity->first_names;
        $model->last_name = \Yii::$app->user->identity->last_name;
        $model->phone = \Yii::$app->user->identity->phone;
        $model->role = \Yii::$app->user->identity->role_in_organization;
        $model->email = \Yii::$app->user->identity->email;
        
        if (Yii::$app->user->can('sys_admin')) {
            $model->setScenario('default_admin');
        }
        if ($model->load(\Yii::$app->request->post()))
        {
            if ($model->validate())
            {
                $user = \Yii::$app->user->identity;
                $user->first_names = $model->first_name;
                $user->last_name = $model->last_name;
                $user->phone = $model->phone;
                $user->email = $model->email;
                $user->role_in_organization = $model->role;
                
                if (Yii::$app->user->can('sys_admin')) {
                    $user->setScenario('default_admin');
                }
                if ($user->save()) {
                    $actionMessage = new ActionMessage();
                    $actionMessage->SetSuccess(
                            Yii::t('app', 'Contact updated!', ['evt' => 'user']), 
                            Yii::t('app', 'Your contact details have been updated.'));
                    Yii::$app->session->set('finished_action_result', $actionMessage);                
                    return $this->render('account', ['model' => Yii::$app->user->identity]);
                }
                else
                {
                    $actionMessage = new ActionMessage();
                    $actionMessage->SetError(
                            Yii::t('app', 'Update failed!', ['evt' => 'user']), 
                            Yii::t('app', 'There\'s been a problem with updating your details.<br/>Please retry later.'));
                    Yii::$app->session->set('finished_action_result', $actionMessage);                
                    return $this->render('account', ['model' => Yii::$app->user->identity]);
                }
                
            }
        }
        
        return $this->render('contact_details', ['model'=>$model]);
    }
                
    
    public function actionChange_password()
    {
        if (Yii::$app->user->isGuest) 
        {
            throw new NotFoundHttpException();
        }
        
        /* @var $user \app\models\User */
        $model = new \app\models\forms\ChangePassword();
        
        if (Yii::$app->request->isAjax && $model->load($_POST))
        {
            Yii::$app->response->format = 'json';
            return \yii\widgets\ActiveForm::validate($model);
        }
        
        if ($model->load(Yii::$app->request->post()))
        {
            if ($model->validate())
            {
                $user=Yii::$app->user->identity;
                //decrypt new password
                $decrypted = $user->decryptPassword($model->new_password);

                $user->setPassword($decrypted);
                
                if (Yii::$app->user->can('sys_admin')) {
                    $user->setScenario('default_admin');
                }
                if (!($user->validate() && $user->save())) {
                    echo '<pre>';
                    echo var_dump($user->errors);
                    echo '</pre>';
                    die('unable to change password');
                }
                
                $actionMessage = new ActionMessage();
                $actionMessage->SetSuccess(
                        Yii::t('app', 'Password modified', []), 
                        Yii::t('app', 'You have successfully modified your password.') 
                        . '<br/>' . Yii::t('app', 'Use the new password next time you login to SyRIO.'));
                Yii::$app->session->set('finished_action_result', $actionMessage);
                
                
                return $this->goBack();
            }
            else
            {
                $model->old_password = '';
                $model->new_password = '';
                $model->password_repeat = '';
                return $this->render('change_password', ['model' => $model]);
            }
            
        }
        
        return $this->render('change_password', ['model' => $model]);
    }
    
    
    public function actionChange_username()
    {
        if (Yii::$app->user->isGuest) 
        {
            throw new NotFoundHttpException();
        }
        
        /* @var $user \app\models\User */
        $model = new \app\models\forms\ChangeUsername();
        
        if (Yii::$app->request->isAjax && $model->load($_POST))
        {
            Yii::$app->response->format = 'json';
            return \yii\widgets\ActiveForm::validate($model);
        }

        if ($model->load($_POST) && $model->validate())
        {
            $user = Yii::$app->user->identity;
            
            $user->username = $model->new_username;
            
            if (Yii::$app->user->can('sys_admin')) {
                $user->setScenario('default_admin');
            }
            
            $user->save();
            
            $actionMessage = new ActionMessage();
            $actionMessage->SetSuccess(
                    Yii::t('app', 'Username modified', []), 
                    Yii::t('app', 'You have successfully modified your user name.') 
                    . '<br/>' . Yii::t('app', 'Use the new user name next time you login to SyRIO.'));
            Yii::$app->session->set('finished_action_result', $actionMessage);


            return $this->goBack();
        }
        
        return $this->render('change_username', ['model'=>$model]);
    }
    
    /* @var $user \app\models\User */
    
    public function actionResetPassword()
    {
        if (Yii::$app->user->isGuest)
        {
            return new NotFoundHttpException();
        }
        
        $user = Yii::$app->user->identity;
        echo Yii::$app->security->generateRandomString() . '_' . time();
        echo $user->generatePasswordResetToken();
        die();
    }

    
    public function actionIndex()
    {
        if (Yii::$app->user->isGuest) {
            return Yii::$app->error->ThrowError(Errors::RESTRICTED_AREA_ERROR);
        }
        
        if (!Yii::$app->user->can('user-list'))
        {
            return Yii::$app->error->ThrowError(Errors::RESTRICTED_AREA_ERROR);
        }
        
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    

    public function actionCreateUser()
    {
        if (!Yii::$app->user->can('sys_admin'))
        {
            return Yii::$app->error->ThrowError(Errors::RESTRICTED_OPERATION_ERROR);
        }
        $model = new \app\models\admin\users\CreateUserModel();
        
        if ($model->load(Yii::$app->request->post()))
        {
            if (isset($model->user_type) && !isset($model->target))
            {
                if ($model->user_type == 'ca')
                {
                    return $this->redirect(['create-ca-user', 'ca_org_id'=>'']);
                }
                
                $targets = $model->getTargets($model->user_type);
                
                return $this->render('create-user', ['model' => $model, 'targets' => $targets]);
            }
            elseif (isset($model->user_type) && isset($model->target))
            {
                //$targets = $model->getTargets($model->user_type);
                
                switch($model->user_type)
                {
                    case 'ca':
                        //this one is never true since in this version (3) there is only ONE Competent Authority
                        //return $this->redirect(['create-ca-user', 'country'=>$model->target]);
                        break;
                    case 'oo':
                        return $this->redirect(['create-oo-user', 'org_id'=>$model->target]);
                        break;
                    case 'inst':
                        return $this->redirect(['create-installation-user', 'installation_id'=>$model->target]);
                        break;
                    default:
                        throw new NotFoundHttpException('The requested page does not exist.');
                }
                
                return $this->render('create-user', [
                    'model'=>$model,
                    'target' => $model->target,
                    'targets' => $targets]);
            }
            return $this->render('create-user', ['model' => $model]);
        }
        
        return $this->render('create-user', ['model' => $model]);
    }
    
    public function actionCreateCaUser($ca_org_id = null)
    {
        $access=0;
        if (Yii::$app->user->can('sys_admin'))
        {
            $access=1;
        }
        else if (Yii::$app->user->can('ca-user-create'))
        {
            $access=1;
        }
        if ($access==0)
        {
            return Yii::$app->error->ThrowError(Errors::RESTRICTED_OPERATION_ERROR);
        }
        
        $actionMessage = new ActionMessage();
        
        $model = new User();
        if ($model->load(Yii::$app->request->post()))
        {
            //create the username
            $username = $this->CreateLoginName($model);

            //create a password
            $tempPass = Yii::$app->security->generateRandomString(8);
            $model->setPassword($tempPass);
            $model->generateAuthKey();

            $model->username = Html::encode($username);
            //$model->password = $tempPass;
            $model->status = User::STATUS_NEW;
            
            if (!$model->validate())
            {
                //get the list of competent authorities registered to the current user country
                $orgs = Organization::find()
                        //->where(['country' => Yii::$app->user->identity->organization->country])
                        ->where(['like', 'id', 'CA'])
                        ->asArray()->all();
                $organizations = \yii\helpers\ArrayHelper::map($orgs, 'id', 'organization_acronym' . ' - ' . 'organization_name');
                $roleObj = new Role();
                if (count($organizations) == 0)
                {
                    throw new \yii\web\HttpException(1001, 'There must be at least one Competent Authority Organization registered in order to proceed.');
                }
                return $this->render('create-ca-user', ['model'=>$model, 'ca_roles'=>$ca_roles, 'organizations'=>$organizations]);
            }

            $transaction = Yii::$app->db->beginTransaction();
            $flag = 0;
            if (!$model->save())
            {
                $transaction->rollBack();
                $flag = 1;   // rollback
            }
            else
            {
                //assign the user to roles
                $auth = Yii::$app->getAuthManager();
                $roles = $_POST['User']['currentRoles'];
                foreach ($roles as $role) 
                {
                    $_role = $auth->getRole(Html::encode($role));
                    if (!isset($_role))
                    {
                        $this->addError('role', 'Role' . $this->roles . 'doesn\'t exist');
                        $transaction->rollBack();
                        $flag=1;
                    }
                    else
                    {
                        $auth->assign($_role, $model->id);
                    }
                }
            }

            if ($flag == 0)
            {
                $transaction->commit();

                /* TODO: send emails */
                //send the confirmation email to the newly created user
                EmailDispacher::sendCaUserRegistrationConfirmation($model, $tempPass);

                $msg = Yii::t('app', 'User {user} has been created and registered with {org}.', [
                    'user' => $model->username,
                    'org' => $model->organization->organization_acronym,
                ]) . '</br>';
                $msg .= Yii::t('app', 'The user has been notified through email.', []);

                $actionMessage->SetSuccess(Yii::t('app', 'Register user'), $msg);
                Yii::$app->session->set('finished_action_result', $actionMessage);
            }
            else
            {
                //there's been an error
                $actionMessage->SetError(Yii::t('app', 'Register user'), Yii::t('app', 'Unable to create user {evt}.', ['evt'=>$model->username]));
                Yii::$app->session->set('finished_action_result', $actionMessage);
            }

            //save and stuff
            //return $this->render(Url::to(['/admin/users/user/list-users']));
            $searchModel = new UserSearch();
            $dataProvider =  $searchModel->search(Yii::$app->request->queryParams);

            //return $this->redirect('index', ['dataProvider'=>$dataProvider, 'searchModel' => $searchModel]);
            return $this->redirect(['index', ['dataProvider'=>$dataProvider, 'searchModel' => $searchModel]]);
        }

        
        //die('here');
        //get the list of competent authorities registered to the current user country
        $orgs = Organization::find()
                //->where(['country' => Yii::$app->user->identity->organization->country])
                ->where(['like', 'id', 'CA'])
                ->asArray()->all();
        $organizations = \yii\helpers\ArrayHelper::map($orgs, 'id', 'organization_name');
        if (count($organizations) == 0)
        {
            throw new NotFoundHttpException(Yii::t('app', 'You must first register a new Competent Authority Organization.'));
        }
        //get the possible ca roles
        $roleObj = new Role();
        $model->org_id = $ca_org_id;
        return $this->render('create-ca-user', ['model'=>$model, 'organizations'=>$organizations, 'ca_roles'=>$roleObj->getCaRoles()]);
    }
    
    public function actionCreateOoUser($org_id)
    {

        $access=0;
        if (Yii::$app->user->can('sys_admin'))
        {
            $access=1;
        }
        else if (Yii::$app->user->can('oo-user-create') && $org_id==Yii::$app->user->identity->organization->id)
        {
            $access=1;
        }
        if ($access==0)
        {
            return Yii::$app->error->ThrowError(Errors::RESTRICTED_OPERATION_ERROR);
        }
        
        $actionMessage = new ActionMessage();
        
        $model = new User();
        if ($model->load(Yii::$app->request->post()))
        {
            
            //create the username
            $username = $this->CreateLoginName($model);

            //create a password
            $tempPass = Yii::$app->security->generateRandomString(8);
            $model->setPassword($tempPass);
            $model->generateAuthKey();

            $model->username = Html::encode($username);
            //$model->password = $tempPass;
            $model->status = User::STATUS_NEW;
            
            if (!$model->validate())
            {
                //get the organization by id
                $organization = Organization::find()
                        ->where(['id'=>$org_id])
                        ->one();
                if (!isset($organization))
                {
                    throw new \yii\web\HttpException(1002, 'There is no Operators/Owners organization corresponding to the information provided. Please register the Organization first.');
                }
                $model->org_id = $organization->id;
                //get the possible op roles
                $roleObj = new Role();
                return $this->render('create-oo-user', ['model'=>$model, 'organization'=>$organization, 'op_roles'=>$roleObj->getOoRoles()]);
            }

            $transaction = Yii::$app->db->beginTransaction();
            $flag = 0;
            if (!$model->save())
            {
                $transaction->rollBack();
                $flag = 1;   // rollback
            }
            else
            {
                //assign the user to roles
                $auth = Yii::$app->getAuthManager();
                $roles = $_POST['User']['currentRoles'];
                
                foreach ($roles as $role) 
                {
                    $_role = $auth->getRole(Html::encode($role));
                    if (!isset($_role))
                    {
                        $this->addError('role', 'Role' . $this->roles . 'doesn\'t exist');
                        $transaction->rollBack();
                        $flag=1;
                    }
                    else
                    {
                        $auth->assign($_role, $model->id);
                    }
                }
            }

            if ($flag == 0)
            {
                $transaction->commit();

                /* TODO: send emails */
                //send the confirmation email to the newly created user
                EmailDispacher::sendOoUserRegistrationConfirmation($model, $tempPass);

                $msg = Yii::t('app', 'User {user} has been created and registered with {org}.', [
                    'user' => $model->username,
                    'org' => $model->organization->organization_acronym,
                ]) . '</br>';
                $msg .= Yii::t('app', 'The user has been notified through email.', []);

                $actionMessage->SetSuccess(Yii::t('app', 'Register user'), $msg);
                Yii::$app->session->set('finished_action_result', $actionMessage);
            }
            else
            {
                //there's been an error
                $actionMessage->SetError(Yii::t('app', 'Register user'), Yii::t('app', 'Unable to create user {evt}.', ['evt'=>$model->username]));
                Yii::$app->session->set('finished_action_result', $actionMessage);
            }

            //save and stuff
            //return $this->render(Url::to(['/admin/users/user/list-users']));
            $searchModel = new UserSearch();
            $dataProvider =  $searchModel->search(Yii::$app->request->queryParams);

            //return $this->redirect('index', ['dataProvider'=>$dataProvider, 'searchModel' => $searchModel]);
            return $this->redirect(['index', ['dataProvider'=>$dataProvider, 'searchModel' => $searchModel]]);
        }

        //get the organization by id
        $organization = Organization::find()
                ->where(['id'=>$org_id])
                ->one();
        if (!isset($organization))
        {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
        //get the possible op roles
        $model->org_id = $organization->id;
        $roleObj = new Role();
        return $this->render('create-oo-user', ['model'=>$model, 'organization'=>$organization, 'op_roles'=>$roleObj->getOoRoles()]);
        
    }
    
    public function actionCreateInstallationUser($installation_id)
    {
        if (!Yii::$app->user->can('user-create'))
        {
            return Yii::$app->error->ThrowError(Errors::RESTRICTED_OPERATION_ERROR);
        }
        
        $actionMessage = new ActionMessage();
        
        $model = new User();
        if ($model->load(Yii::$app->request->post()))
        {
            //create the username
            $username = $this->CreateLoginName($model);

            //create a password
            $tempPass = Yii::$app->security->generateRandomString(8);
            $model->setPassword($tempPass);
            $model->generateAuthKey();

            $model->username = Html::encode($username);
            //$model->password = $tempPass;
            $model->status = User::STATUS_NEW;
            
            $model->inst_id = $installation_id;
            //get the installation by id
            $installation = Installations::find()
                    ->where(['id'=>$installation_id])
                    ->one();
            if (!isset($installation))
            {
                throw new \yii\web\HttpException(1002, 'There is no Installation with such name registered, or the Installation does not belong / is not operated by the Organization provided.');
            }
            $model->org_id = $installation->operator_id;
            
            if (!$model->validate())
            {
                $model->inst_id = $installation_id;
                $model->org_id = $installation->operator->id;
                //get the possible op roles
                $roleObj = new Role();
                return $this->render('create-inst-user', ['model'=>$model, 'organization'=>$organization, 'inst_roles'=>$roleObj->getInstallationRoles()]);
            }

            $transaction = Yii::$app->db->beginTransaction();
            $flag = 0;
            if (!$model->save())
            {
                $transaction->rollBack();
                $flag = 1;   // rollback
            }
            else
            {
                //assign the user to roles
                $auth = Yii::$app->getAuthManager();
                $roles = $_POST['User']['currentRoles'];
                foreach ($roles as $role) 
                {
                    $_role = $auth->getRole(Html::encode($role));
                    if (!isset($_role))
                    {
                        $this->addError('role', 'Role' . $this->roles . 'doesn\'t exist');
                        $transaction->rollBack();
                        $flag=1;
                    }
                    else
                    {
                        $auth->assign($_role, $model->id);
                    }
                }
            }

            if ($flag == 0)
            {
                $transaction->commit();

                /* TODO: send emails */
                //send the confirmation email to the newly created user
                EmailDispacher::sendInstallationUserRegistrationConfirmation($model, $tempPass);

                $msg = Yii::t('app', 'User {user} has been created and registered with {org}.', [
                    'user' => $model->username,
                    'org' => $model->organization->organization_acronym,
                ]) . '</br>';
                $msg .= Yii::t('app', 'The user has been notified through email.', []);

                $actionMessage->SetSuccess(Yii::t('app', 'Register user'), $msg);
                Yii::$app->session->set('finished_action_result', $actionMessage);
            }
            else
            {
                //there's been an error
                $actionMessage->SetError(Yii::t('app', 'Register user'), Yii::t('app', 'Unable to create user {evt}.', ['evt'=>$model->username]));
                Yii::$app->session->set('finished_action_result', $actionMessage);
            }

            //save and stuff
            //return $this->render(Url::to(['/admin/users/user/list-users']));
            $searchModel = new UserSearch();
            $dataProvider =  $searchModel->search(Yii::$app->request->queryParams);

            //return $this->redirect('index', ['dataProvider'=>$dataProvider, 'searchModel' => $searchModel]);
            return $this->redirect(['index', ['dataProvider'=>$dataProvider, 'searchModel' => $searchModel]]);
        }        

        //get the installation by id
        $installation = Installations::find()
                ->where(['id'=>$installation_id])
                ->one();
        if (!isset($installation))
        {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
        //get the possible op roles
        $model->org_id = $installation->id;
        $roleObj = new Role();
        
        return $this->render('create-inst-user', ['model'=>$model, 'installation'=>$installation, 'inst_roles'=>$roleObj->getInstallationRoles()]);
        
    }
    

    
    public function actionEditUser($id)
    {
        $actionName = 'Edit User';
        return $actionName;
    }
    
    public function actionEditCaUser($id)
    {
        if (!Yii::$app->user->can('user-edit') && !Yii::$app->user->can('ca-user-edit', ['id'=>$id]))
        {
            return Yii::$app->error->ThrowError(Errors::RESTRICTED_OPERATION_ERROR);
        }
        /*
         * Get the user with the provided id
         */
        $model=$this->findModel($id);

        /* throw page not found if the user to be edited is not CA user */
        if (!$model->isCaUser) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        
        $actionMessage = new ActionMessage();
        
        if ($model->load(Yii::$app->request->post()))
        {
            if (!$model->validate())
            {
                //get the list of competent authorities registered to the current user country
                $orgs = Organization::find()
                        //->where(['country' => Yii::$app->user->identity->organization->country])
                        ->where(['like', 'id', 'CA'])
                        ->asArray()->all();
                $organizations = \yii\helpers\ArrayHelper::map($orgs, 'id', 'organization_acronym' . ' - ' . 'organization_name');
                return $this->render('edit-ca-user', ['model'=>$model, 'ca_roles'=>$ca_roles, 'organizations'=>$organizations]);
            }

            $transaction = Yii::$app->db->beginTransaction();
            $flag = 0;

            //save the model
            try
            {
                $model->save();
                //reset the user credentials and update with the new ones
                //get the Current Roles
                $selectedRoles = $_POST['User']['currentRoles'];

                //delete all previous user (CA) roles
                $role = new Role();
                foreach($role->getCaRoles() as $ca_role) {
                    UserAssignment::deleteAll('user_id = ' . $model->id . ' AND ' . 'item_name = "' . $ca_role->name . '"');
                }
                //add the edited created roles
                $dt = new \DateTime();
                $timestamp = $dt->getTimestamp();

                foreach($selectedRoles as $newRole) {
                    $newAssignment = new UserAssignment();
                    $newAssignment->item_name = $newRole;
                    $newAssignment->user_id = $model->id;
                    $newAssignment->created_at = $timestamp;
                    $newAssignment->save();
                }
            } catch (Exception $ex) {
                $flag=1;
            }

            if ($flag==1)
            {
                //error
                $transaction->rollBack();
                //there's been an error
                $actionMessage->SetError(Yii::t('app', 'Edit user'), Yii::t('app', 'Unable to update user {evt}.', ['evt'=>$model->username]));
                Yii::$app->session->set('finished_action_result', $actionMessage);
            }
            else
            {
                $transaction->commit();
                $msg = Yii::t('app', 'User {user} has been successfully edited.', [
                    'user' => $model->username,
                ]) . '</br>';

                $actionMessage->SetSuccess(Yii::t('app', 'Edit user'), $msg);
                Yii::$app->session->set('finished_action_result', $actionMessage);
            }

            //navigate to users list
            $searchModel = new UserSearch();
            $dataProvider =  $searchModel->search(Yii::$app->request->queryParams);

            //return $this->redirect('index', ['dataProvider'=>$dataProvider, 'searchModel' => $searchModel]);
            return $this->redirect(['index', ['dataProvider'=>$dataProvider, 'searchModel' => $searchModel]]);
        }        
        
        
        //get the list of competent authorities registered to the current user country
        $orgs = Organization::find()
                //->where(['country' => Yii::$app->user->identity->organization->country])
                ->where(['like', 'id', 'CA'])
                ->asArray()->all();
        $organizations = \yii\helpers\ArrayHelper::map($orgs, 'id', 'organization_name');
        return $this->render('edit-ca-user', ['model'=>$model, 'organizations'=>$organizations, 'ca_roles'=>Yii::$app->user->identity->possibleRoles]);
    }
    
    public function actionEditOoUser($id)
    {
        if (!Yii::$app->user->can('user-edit') && !Yii::$app->user->can('oo-user-edit', ['id'=>$id]))
        {
            return Yii::$app->error->ThrowError(Errors::RESTRICTED_OPERATION_ERROR);
        }
        /*
         * Get the user with the provided id
         */
        $model=$this->findModel($id);

        /* throw page not found if the user to be edited is not CA user */
        if (!($model->isOperatorUser)) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        
        $actionMessage = new ActionMessage();
        
        if ($model->load(Yii::$app->request->post()))
        {
            if (!$model->validate())
            {
                //get the list of competent authorities registered to the current user country
                $roleObj = new Role();
                $org = Organization::find()
                        ->where(['id' => $model->organization->id])
                        ->one();
                return $this->render('edit-oo-user', ['model'=>$model, 'oo_roles'=>$roleObj->getOoRoles(), 'organization'=>$org]);
            }

            $transaction = Yii::$app->db->beginTransaction();
            $flag = 0;

            //save the model
            try
            {
                $model->save();
                //reset the user credentials and update with the new ones
                //get the Current Roles
                $selectedRoles = $_POST['User']['currentRoles'];

                //delete all previous user (CA) roles
                $role = new Role();
                foreach($role->getOoRoles() as $ca_role) {
                    UserAssignment::deleteAll('user_id = ' . $model->id . ' AND ' . 'item_name = "' . $ca_role->name . '"');
                }
                //add the edited created roles
                $dt = new \DateTime();
                $timestamp = $dt->getTimestamp();

                foreach($selectedRoles as $newRole) {
                    $newAssignment = new UserAssignment();
                    $newAssignment->item_name = $newRole;
                    $newAssignment->user_id = $model->id;
                    $newAssignment->created_at = $timestamp;
                    $newAssignment->save();
                }
            } catch (Exception $ex) {
                $flag=1;
            }

            if ($flag==1)
            {
                //error
                $transaction->rollBack();
                //there's been an error
                $actionMessage->SetError(Yii::t('app', 'Edit user'), Yii::t('app', 'Unable to update user {evt}.', ['evt'=>$model->username]));
                Yii::$app->session->set('finished_action_result', $actionMessage);
            }
            else
            {
                $transaction->commit();
                $msg = Yii::t('app', 'User {user} has been successfully edited.', [
                    'user' => $model->username,
                ]) . '</br>';

                $actionMessage->SetSuccess(Yii::t('app', 'Edit user'), $msg);
                Yii::$app->session->set('finished_action_result', $actionMessage);
            }

            //navigate to users list
            $searchModel = new UserSearch();
            $dataProvider =  $searchModel->search(Yii::$app->request->queryParams);

            //return $this->redirect(['index', ['dataProvider'=>$dataProvider, 'searchModel' => $searchModel]]);
            return $this->redirect(['index', ['dataProvider'=>$dataProvider, 'searchModel' => $searchModel]]);
        }        
        
        
        //get the list of competent authorities registered to the current user country
                //get the list of competent authorities registered to the current user country
        $roleObj = new Role();
        $org = Organization::find()
                ->where(['id' => $model->organization->id])
                ->one();
        return $this->render('edit-oo-user', ['model'=>$model, 'oo_roles'=>$roleObj->getOoRoles(), 'organization'=>$org]);
   }
    
    public function actionEditInstallationUser($id)
    {
        if (!Yii::$app->user->can('user-edit') && !Yii::$app->user->can('oo-user-edit', ['id'=>$id]))
        {
            return Yii::$app->error->ThrowError(Errors::RESTRICTED_OPERATION_ERROR);
        }
        /*
         * Get the user with the provided id
         */
        $model=$this->findModel($id);

        /* throw page not found if the user to be edited is not CA user */
        if (!($model->isInstallationUser)) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        
        $actionMessage = new ActionMessage();
        
        if ($model->load(Yii::$app->request->post()))
        {
            if (!$model->validate())
            {
                //get the list of competent authorities registered to the current user country
                $roleObj = new Role();
                $org = Organization::find()
                        ->where(['id' => $model->organization->id])
                        ->one();
                return $this->render('edit-oo-user', ['model'=>$model, 'oo_roles'=>$roleObj->getInstallationRoles(), 'organization'=>$org]);
            }

            $transaction = Yii::$app->db->beginTransaction();
            $flag = 0;

            //save the model
            try
            {
                $model->save();
                //reset the user credentials and update with the new ones
                //get the Current Roles
                $selectedRoles = $_POST['User']['currentRoles'];

                //delete all previous user (CA) roles
                $role = new Role();
                foreach($role->getInstallationRoles() as $ca_role) {
                    UserAssignment::deleteAll('user_id = ' . $model->id . ' AND ' . 'item_name = "' . $ca_role->name . '"');
                }
                //add the edited created roles
                $dt = new \DateTime();
                $timestamp = $dt->getTimestamp();

                foreach($selectedRoles as $newRole) {
                    $newAssignment = new UserAssignment();
                    $newAssignment->item_name = $newRole;
                    $newAssignment->user_id = $model->id;
                    $newAssignment->created_at = $timestamp;
                    $newAssignment->save();
                }
            } catch (Exception $ex) {
                $flag=1;
            }

            if ($flag==1)
            {
                //error
                $transaction->rollBack();
                //there's been an error
                $actionMessage->SetError(Yii::t('app', 'Edit user'), Yii::t('app', 'Unable to update user {evt}.', ['evt'=>$model->username]));
                Yii::$app->session->set('finished_action_result', $actionMessage);
            }
            else
            {
                $transaction->commit();
                $msg = Yii::t('app', 'User {user} has been successfully edited.', [
                    'user' => $model->username,
                ]) . '</br>';

                $actionMessage->SetSuccess(Yii::t('app', 'Edit user'), $msg);
                Yii::$app->session->set('finished_action_result', $actionMessage);
            }

            //navigate to users list
            $searchModel = new UserSearch();
            $dataProvider =  $searchModel->search(Yii::$app->request->queryParams);

            //return $this->redirect(['index', ['dataProvider'=>$dataProvider, 'searchModel' => $searchModel]]);
            return $this->redirect(['index', ['dataProvider'=>$dataProvider, 'searchModel' => $searchModel]]);
        }        
        
        
        //get the list of competent authorities registered to the current user country
                //get the list of competent authorities registered to the current user country
        $roleObj = new Role();
        $org = Organization::find()
                ->where(['id' => $model->organization->id])
                ->one();
        return $this->render('edit-oo-user', ['model'=>$model, 'oo_roles'=>$roleObj->getInstallationRoles(), 'organization'=>$org]);
    }
    

    /**
     * Deletes (can be undone) a user
     * 
     * @param integer $id the id of the user to be deleted
     * @return the index view if successful, not allowed if not enough credentials
     * @throws NotFoundHttpException
     */
    public function actionDelete($id)
    {
        /*
         * TESTS PERFORMED:
         * 
         * sys_admin CAN delete any user                        ------ PASSED
         * ca_admin cannot delete a (ca_user) from another CA   ------ PASSED
         * 
         */
        
        if (!Yii::$app->user->can('user-delete', ['id'=>$id]))
        {
            Yii::$app->error->ThrowError(Errors::RESTRICTED_OPERATION_ERROR);
        }
        
        $actionMessage = new ActionMessage();
        
        $model = User::findOne($id);
        if (!isset($model)) {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
        $model->status = User::STATUS_DELETED;
        
        $transaction = Yii::$app->db->beginTransaction();
        
        try
        {
            $model->setScenario('delete');  //this is to enable deletion of older users
            $model->validate();
            
            $model->save();

            $transaction->commit();
            
            $msg = Yii::t('app', 'User {user} has been successfully deleted.', [
                'user' => $model->username,
            ]) 
                    . '</br>'
                    . Yii::t('app', 'You may reactivate user later or purge it for permanent deletion.');

            $actionMessage->SetSuccess(Yii::t('app', 'Delete user'), $msg);
            Yii::$app->session->set('finished_action_result', $actionMessage);
            
            //navigate to users list
            $searchModel = new UserSearch();
            $dataProvider =  $searchModel->search(Yii::$app->request->queryParams);

            //return $this->redirect('index', ['dataProvider'=>$dataProvider, 'searchModel' => $searchModel]);
            return $this->redirect(['index', ['dataProvider'=>$dataProvider, 'searchModel' => $searchModel]]);
        } catch (Exception $ex) {
            
            //there's been an error
            $transaction->rollBack();
            
            $actionMessage->SetError(Yii::t('app', 'Delete user'), Yii::t('app', 'There\'s been an error when performing the requested operation ({evt}). Please retry later.', ['evt'=>Yii::t('app', 'Delete user')]));
            Yii::$app->session->set('finished_action_result', $actionMessage);

            //navigate to users list
            $searchModel = new UserSearch();
            $dataProvider =  $searchModel->search(Yii::$app->request->queryParams);

            //return $this->redirect('index', ['dataProvider'=>$dataProvider, 'searchModel' => $searchModel]);
            return $this->redirect(['index', ['dataProvider'=>$dataProvider, 'searchModel' => $searchModel]]);
        }
    }
    
    
    public function actionReactivate($id)
    {
        //only the ones with delete credentials can reactivate a user
        /*
         * TESTS PERFORMED:
         * 
         * sys_admin CAN reactivate any user                        ------ PASSED
         * ca_admin cannot reactivate a (ca_user) from another CA   ------ PASSED
         * 
         */
        
        if (!Yii::$app->user->can('user-delete', ['id'=>$id]))
        {
            Yii::$app->error->ThrowError(Errors::RESTRICTED_OPERATION_ERROR);
        }
        
        $actionMessage = new ActionMessage();
        
        $model = User::findOne($id);
        if (!isset($model)) {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
        $model->status = User::STATUS_ACTIVE;
        
        $transaction = Yii::$app->db->beginTransaction();
        
        try
        {
            $model->setScenario('delete');  //this is to enable deletion of older users - the same scenario because I don't care about the actual Name of the scenario
            $model->validate();
            
            $model->save();

            $transaction->commit();
            
            $msg = Yii::t('app', 'User {user} has been successfully reactivated.', [
                'user' => $model->username,
            ]); 

            $actionMessage->SetSuccess(Yii::t('app', 'Reactivate user'), $msg);
            Yii::$app->session->set('finished_action_result', $actionMessage);
            
            //navigate to users list
            $searchModel = new UserSearch();
            $dataProvider =  $searchModel->search(Yii::$app->request->queryParams);

            //return $this->redirect('index', ['dataProvider'=>$dataProvider, 'searchModel' => $searchModel]);
            return $this->redirect(['index', ['dataProvider'=>$dataProvider, 'searchModel' => $searchModel]]);
        } catch (Exception $ex) {
            
            //there's been an error
            $transaction->rollBack();
            
            $actionMessage->SetError(Yii::t('app', 'Reactivate user'), Yii::t('app', 'There\'s been an error when performing the requested operation ({evt}). Please retry later.', ['evt'=>Yii::t('app', 'Reactivate user')]));
            Yii::$app->session->set('finished_action_result', $actionMessage);

            //navigate to users list
            $searchModel = new UserSearch();
            $dataProvider =  $searchModel->search(Yii::$app->request->queryParams);

            //return $this->redirect('index', ['dataProvider'=>$dataProvider, 'searchModel' => $searchModel]);
            return $this->redirect(['index', ['dataProvider'=>$dataProvider, 'searchModel' => $searchModel]]);
            
        }
    }
    
    /**
     * Permanently deletes an user
     * 
     * NOTE: the user must be in User::STATUS_DELETED
     * 
     * @param type $id the id of the user to be purged
     */
    public function actionPurge($id)
    {
        $actionMessage = new ActionMessage();

        if (!Yii::$app->user->can('user-purge', ['id'=>$id]))
        {
            Yii::$app->error->ThrowError(Errors::RESTRICTED_OPERATION_ERROR);
        }
        
        $model=User::findOne($id);
        if (!isset($model)) {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }

        if ($model->status !== User::STATUS_DELETED) {
            $actionMessage->SetWarning(Yii::t('app', 'Purge user'), Yii::t('app', '{evt} cannot be purged. Please delete it first.', ['evt'=>Yii::t('app', 'Purge user')]));
            Yii::$app->session->set('finished_action_result', $actionMessage);

            //navigate to users list
            $searchModel = new UserSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            //return $this->redirect('index', ['dataProvider'=>$dataProvider, 'searchModel' => $searchModel]);                
            return $this->redirect(['index', ['dataProvider'=>$dataProvider, 'searchModel' => $searchModel]]);
        }
        else
        {
            $transaction = Yii::$app->db->beginTransaction();
            try
            {
                User::deleteAll(['id'=>$id]);
                
                $transaction->commit();
                
                $msg = Yii::t('app', 'User {user} has been purged (permanently deleted).', [
                    'user' => $model->username,
                ]); 

                $actionMessage->SetSuccess(Yii::t('app', 'Purge user'), $msg);
                Yii::$app->session->set('finished_action_result', $actionMessage);

                //navigate to users list
                $searchModel = new UserSearch();
                $dataProvider =  $searchModel->search(Yii::$app->request->queryParams);

                //return $this->redirect('index', ['dataProvider'=>$dataProvider, 'searchModel' => $searchModel]);
                return $this->redirect(['index', ['dataProvider'=>$dataProvider, 'searchModel' => $searchModel]]);
                
            } catch (Exception $ex) {

                $transaction->rollBack();
                
                $actionMessage->SetError(Yii::t('app', 'Purge user'), Yii::t('app', 'There\'s been an error when performing the requested operation ({evt}). Please retry later.', ['evt'=>Yii::t('app', 'Purge user')]));
                Yii::$app->session->set('finished_action_result', $actionMessage);

                //navigate to users list
                $searchModel = new UserSearch();
                $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

                //return $this->redirect('index', ['dataProvider'=>$dataProvider, 'searchModel' => $searchModel]);                
                return $this->redirect(['index', ['dataProvider'=>$dataProvider, 'searchModel' => $searchModel]]);
            }
            
        }
        
        return 'user purged';
    }
    
    
    protected function findModel($id) {
        if (($model = User::findOne(['id' => $id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    
    
    /*
     * FUNCTIONS SECTION
     */
    
    /**
     * 
     * @param app/models/User $model the user to create a username for
     */
    private function CreateLoginName($model)
    {
        /* first create the user login */
        $dotFirstNames = strtolower(str_ireplace(' ', '.', $model->first_names));
        $dotLastNames = strtolower(str_ireplace(' ', '.', $model->last_name));
        $fullName = $dotFirstNames . '.' . $dotLastNames;

        //check if username is taken and create a random suffix otherwise
        $suffix='';
        $iterator=0;        //iterator to prevent infinite loop
        $infLoop = false;   //flag to test if infinite loop
        do
        {
            $suffix!='' ? $fullNameNew = $fullName . '.' .$suffix : $fullNameNew = $fullName;
            $existingUser = User::findOne(['username' => $fullNameNew]);

            isset($existingUser) ? $proceed = true : $proceed = false;
            $suffix = rand(1,1000);
            $iterator++;

            if ($iterator>1000)
            {
                $infLoop=true;
                break;
            }
        }
        while ($proceed);
        
        return $fullNameNew;
    }

    
    
    public function actionView($id) {
        
        //echo var_dump(!Yii::$app->user->can('user-view', ['id'=>$id])).'<br/>';
        
        
        if (!Yii::$app->user->can('user-view', ['id'=>$id]))
        {
            Yii::$app->error->ThrowError(Errors::RESTRICTED_OPERATION_ERROR);
            return;
        }
        
        $actionMessage = new ActionMessage();
        
        $model = User::findOne($id);
        if (!isset($model)) {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
        
        return $this->render('view', [
            'model' => $model
        ]);
        
    }
    
}

/*20150920*/

