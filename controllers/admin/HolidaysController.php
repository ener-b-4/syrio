<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\controllers\admin;

use Yii;
use app\models\Holidays;
use app\models\HolidaysSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use app\components\Errors;

/**
 * HolidaysController implements the CRUD actions for Holidays model.
 */
class HolidaysController extends Controller
{
    public function beforeAction($action) {
        if (\Yii::$app->user->isGuest ) {
            throw new NotFoundHttpException(\Yii::t('app', 'The requested page does not exist.'));
        }
        return parent::beforeAction($action);
    }
    
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Holidays models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new HolidaysSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Holidays model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        
        if (!(\Yii::$app->user->can('holiday_crud')) )
        {
            return \Yii::$app->error->ThrowError(Errors::RESTRICTED_OPERATION_ERROR);
        }
        
        $model = new Holidays();

        if ($model->load(Yii::$app->request->post())) {
            //get the date from date
            $date = new \DateTime($model->date);
            $model->year= date_format($date, 'Y');
            $model->month= date_format($date, 'm');
            $model->day= date_format($date, 'd');

            if (!$model->validate()) {
                die (print_r($model->errors));
            }
            else {
                $model->save();
            }
            
            return $this->redirect($model->request);
        } else {
            $model->request = \Yii::$app->request->headers->get('Referer');
            
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Holidays model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if (!(\Yii::$app->user->can('holiday_crud')) )
        {
            return \Yii::$app->error->ThrowError(Errors::RESTRICTED_OPERATION_ERROR);
        }
        
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            //get the date from date
            $date = new \DateTime($model->dateAll);

            if (isset($model->repeat) && $model->repeat === 1) {
                $model->year= null;
            } else {
                $model->year= date_format($date, 'Y');
            }
            $model->month= date_format($date, 'm');
            $model->day= date_format($date, 'd');
            
            if (!$model->validate()) {
                die (print_r($model->errors));
            }
            else {
                $model->save();
            }
            
            if (isset($model->request) && trim($model->request) !== '') {
                return $this->redirect($model->request);
            } else {
                return $this->redirect(['index']);
            }
        } else {
            if (\Yii::$app->request->absoluteUrl !== \Yii::$app->request->headers->get('Referer')) {
                $model->request = \Yii::$app->request->headers->get('Referer');
            }
            
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Holidays model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if (!(\Yii::$app->user->can('holiday_crud')) )
        {
            return \Yii::$app->error->ThrowError(Errors::RESTRICTED_OPERATION_ERROR);
        }
        
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Holidays model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Holidays the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Holidays::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(\Yii::t('app', 'The requested page does not exist.'));
        }
    }
}
