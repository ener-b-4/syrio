<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\controllers;

use Yii;
use app\models\units\Units;
use app\models\units\UnitsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\components\Errors;

/**
 * UnitsController implements the CRUD actions for Units model.
 */
class UnitsController extends Controller
{
    /**
     * Add this to ensure that only the CAusers (actually CA rapporteurs) have access to this
     * @param type $action
     * @return boolean
     * 
     * @version 1.0.20151009
     * @author vamanbo <john.doe@example.com>
     */
    public function beforeAction($action) {
        
        if (!(\Yii::$app->user->can('sys_admin') || \Yii::$app->user->identity->isCaUser || \Yii::$app->user->can('units-list')) )
        {
            return \Yii::$app->error->ThrowError(Errors::RESTRICTED_AREA_ERROR);
        }
        return parent::beforeAction($action);        
    }
    
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Units models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UnitsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Units model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Units model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (!(\Yii::$app->user->can('units-create')))
        {
            return \Yii::$app->error->ThrowError(Errors::RESTRICTED_AREA_ERROR);
        }
        
        $model = new Units();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Units model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if (!(\Yii::$app->user->can('units-edit')))
        {
            return \Yii::$app->error->ThrowError(Errors::RESTRICTED_AREA_ERROR);
        }
        
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Units model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {

        $actionMessage = new \app\models\common\ActionMessage();
        $actionMessage->SetWarning(
                Yii::t('app', 'Unable to delete {evt}!', ['evt' => \Yii::t('app', 'unit')]), 
                Yii::t('app/messages', 'For system stability reasons this option is disabled in the current version of SyRIO.'));
        Yii::$app->session->set('finished_action_result', $actionMessage);
        
        //$this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Units model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Units the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Units::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
