<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\controllers;

use Yii;
use app\models\Installations;
use app\models\InstallationsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * InstallationsController implements the CRUD actions for Installations model.
 */
class InstallationsController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                //only authenticated users
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Installations models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new InstallationsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Installations model.
     * @param string $id
     * @param string $operator_id
     * @return mixed
     */
    public function actionView($id, $operator_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id, $operator_id),
        ]);
    }

    /**
     * Creates a new Installations model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        
        
        $model = new Installations();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id, 'operator_id' => $model->operator_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Installations model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @param string $operator_id
     * @return mixed
     */
    public function actionUpdate($id, $operator_id)
    {
        $model = $this->findModel($id, $operator_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id, 'operator_id' => $model->operator_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Installations model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @param string $operator_id
     * @return mixed
     */
    public function actionDelete($id, $operator_id)
    {
        $this->findModel($id, $operator_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Installations model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @param string $operator_id
     * @return Installations the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id, $operator_id)
    {
        if (($model = Installations::findOne(['id' => $id, 'operator_id' => $operator_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
