<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\controllers\registration;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \app\models\User;
use yii\helpers\Html;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserRegistration
 *
 * @author vamanbo
 */
class UserRegistrationController extends Controller {
    
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                //only authenticated users
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['index', 'operators', 'installation', 'competent-authority' , 'captcha'],
                        'allow' => true,
                        'roles' => [ '?' ]
                    ]
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
    
    
    public function actionIndex()
    {
        return $this->render('@app/views/shared_parts/not-implemented.php', ['action' => $this->id . '/' . $this->action->id]);
    }
    
    public function actionOperators()
    {
        /* @var $newUser \app\models\User */
        
        //return $this->render('@app/views/shared_parts/not-implemented.php', ['action' => $this->id . '/' . $this->action->id]);
        
        $model = new \app\models\forms\UserRegistrationOperators();
        $roles = [
            'op_admin' => \Yii::t('app', 'Administrator'),
            'op_raporteur' => \Yii::t('app', 'Rapporteur'),
            'op_user' => \Yii::t('app', 'Member'),
        ];
        
        if ($model->load(\Yii::$app->request->post()))
        {
            $actionMessage = new \app\models\common\ActionMessage();
            
            if (!$model->validate())
            {
                return $this->render('register-operator-form', ['model'=>$model, 'roles'=>$roles]);
            }
            
            /* how does it work:
             * 
             * C1. first check if there's a company having either the name or the acronym as the one provided
             * 
             * - if NOT C1 send an email to the requester
             * 
             * C2.  - create the user
             *      - set his status as REQUEST
             * 
             * C3.  - send a mail to the user;
             *      - send a mail to the sys_admin (for the beta version)
             */
            
            $org = \app\modules\management\models\Organization::find()
                    ->where(['status' => \app\modules\management\models\Organization::STATUS_ACTIVE])
                    ->andWhere([
                        'or',
                        ['organization_name' => $model->organization],
                        ['organization_acronym' => $model->organization]
                    ])->one();
            
            if (!$org) {
                echo 'no such organization found!';
                die();
            } elseif ($org->organizationType !== \app\modules\management\models\Organization::OPERATORS_OWNERS)
            {
                echo 'the organization is NOT a OO!';
                die();
            }
            
            $newUser = new User();
            
            //create the username
            $username = $this->CreateLoginName($model);

            //create a password
            $tempPass = \Yii::$app->security->generateRandomString(8);
            $newUser->setPassword($tempPass);
            $newUser->generateAuthKey();

            $newUser->username = Html::encode($username);
            $newUser->org_id = $org->id;
            $newUser->email = $model->email;
            $newUser->first_names = $model->first_name;
            $newUser->last_name = $model->second_name;
            $newUser->role_in_organization = $model->role_in_organization;
            
            //$model->password = $tempPass;
            $newUser->status = \app\models\User::STATUS_REQUEST_CREATE;
            
            $transaction = \Yii::$app->db->beginTransaction();
            $flag = 0;
            
            if (!$newUser->validate())
            {
                echo '<pre>';
                echo var_dump($newUser->errors);
                echo '</pre>';
                die();
            }
            
            if (!$newUser->save())
            {
                $transaction->rollBack();
                $flag = 1;   // rollback
            }
            else
            {
                //assign the user to roles
                $auth = \Yii::$app->getAuthManager();
                //$roles = $_POST['UserRegistrationOperators']['roles'];
                foreach ($model->roles as $role) 
                {
                    $_role = $auth->getRole(Html::encode($role));
                    if (!isset($_role))
                    {
                        $this->addError('role', 'Role' . $this->roles . 'doesn\'t exist');
                        $transaction->rollBack();
                        $flag=1;
                    }
                    else
                    {
                        $auth->assign($_role, $newUser->id);
                    }
                }
            }

            if ($flag == 0)
            {
                $transaction->commit();

                /* TODO: send emails */
                //send the confirmation email to the newly created user
                //EmailDispacher::sendOoUserRegistrationConfirmation($model, $tempPass);

                $msg = \Yii::t('app', 'User {user} has been created and registered with {org}.', [
                    'user' => $newUser->username,
                    'org' => \Yii::t('app', $newUser->organization->organization_acronym),
                ]) . '</br>';
                $msg .= \Yii::t('app', 'Emails have been sent to: {data}', [
                    'data' => $newUser->email,
                ]);

                $actionMessage->SetSuccess(\Yii::t('app', 'Register user'), $msg);
                \Yii::$app->session->set('finished_action_result', $actionMessage);
            }
            else
            {
                //there's been an error
                $actionMessage->SetError(\Yii::t('app', 'Register user'), \Yii::t('app', 'Unable to create user {evt}.', ['evt'=>$newUser->username]));
                \Yii::$app->session->set('finished_action_result', $actionMessage);
            }
            
            return $this->goHome();
            
        }   //end if post
        
        return $this->render('register-operator-form', ['model'=>$model, 'roles'=>$roles]);
    }
    
    public function actionInstallation()
    {
        return $this->render('@app/views/shared_parts/not-implemented.php', ['action' => $this->id . '/' . $this->action->id]);
    }
    
    public function actionCompetentAuthority()
    {
        return $this->render('@app/views/shared_parts/not-implemented.php', ['action' => $this->id . '/' . $this->action->id]);
    }
    
    
    
    /**
     * The same as in UserController
     * @param app/models/User $model the user to create a username for
     */
    private function CreateLoginName($model)
    {
        /* first create the user login */
        $dotFirstNames = strtolower(str_ireplace(' ', '.', $model->first_name));
        $dotLastNames = strtolower(str_ireplace(' ', '.', $model->second_name));
        $fullName = $dotFirstNames . '.' . $dotLastNames;

        //check if username is taken and create a random suffix otherwise
        $suffix='';
        $iterator=0;        //iterator to prevent infinite loop
        $infLoop = false;   //flag to test if infinite loop
        do
        {
            $suffix!='' ? $fullNameNew = $fullName . '.' .$suffix : $fullNameNew = $fullName;
            $existingUser = User::findOne(['username' => $fullNameNew]);

            isset($existingUser) ? $proceed = true : $proceed = false;
            $suffix = rand(1,1000);
            $iterator++;

            if ($iterator>1000)
            {
                $infLoop=true;
                break;
            }
        }
        while ($proceed);
        
        return $fullNameNew;
    }
    
}
