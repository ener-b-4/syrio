<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\controllers\events;

use Yii;
use app\models\events\EventDeclaration;
use app\models\events\EventDeclarationSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use app\components\Errors;

use app\models\common\ActionMessage;

/**
 * EventDeclarationController implements the CRUD actions for EventDeclaration model.
 */
class EventDeclarationController extends Controller
{
    public function __construct( $id, $module, $config = [] )
    {
        parent::__construct($id, $module, $config);
        
        if (!\Yii::$app->user->isGuest) {
            
            //i don't need to check if operators. ALWAYS operators
                
//            $layoutFile = 'operators\main.php';
//            $this->layout = $layoutFile;
        }
    }
    
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                //only authenticated users
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ]
            ],
            
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all EventDeclaration models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->user->isGuest)
        {
            return Yii::$app->error->ThrowError(\app\components\Errors::RESTRICTED_AREA_ERROR);
        }
        if (!Yii::$app->user->can('incident_list') && !Yii::$app->user->can('sys_admin'))
        {
            return Yii::$app->error->ThrowError(\app\components\Errors::RESTRICTED_AREA_ERROR);
        }

        /* 
         * I need to check if cUser can verify against the credentials of registering a new model 
         * The user must be either operator or installation rap
         */
        
        $canRegister = Yii::$app->user->can('inst_rapporteur') ? true : false;
        
        $params = Yii::$app->request->queryParams;
        $params['EventDeclarationSearch']['del_status']=  EventDeclaration::STATUS_ACTIVE;
        
        $searchModel = new EventDeclarationSearch();
        $dataProvider = $searchModel->search($params);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'canRegister' => $canRegister,
        ]);
    }

    
    /**
     * Lists all deleted EventDeclaration models.
     * @return mixed
     */
    public function actionTrash()
    {
        if (Yii::$app->user->isGuest)
        {
            return Yii::$app->error->ThrowError(\app\components\Errors::RESTRICTED_AREA_ERROR);
        }
        if (!Yii::$app->user->can('incident_list') )
        {
            return Yii::$app->error->ThrowError(\app\components\Errors::RESTRICTED_AREA_ERROR);
        }

        /* 
         * I need to check if cUser can verify against the credentials of registering a new model 
         * The user must be either operator or installation rap
         */
        
        $canRegister = false;
        
        $searchModel = new EventDeclarationSearch();
        
        $params = Yii::$app->request->queryParams;
        $params['EventDeclarationSearch']['del_status']=  EventDeclaration::STATUS_DELETED;
        
        //$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider = $searchModel->search($params);
        return $this->render('trash', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'canRegister' => $canRegister,
        ]);
    }
    
    
    
    /**
     * Displays a single EventDeclaration model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        if (!Yii::$app->user->can('incident_view', ['event'=>$model]))
        {
            return \Yii::$app->error->ThrowError(\app\components\Errors::RESTRICTED_AREA_ERROR);
        }
        return $this->render('view', [
            'model' => $model,
            'evtID' => $id,
        ]);
    }

    
    /**
     * Creates a new EventDeclaration model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @author Bogdan Vamanu <bogdan.vamanu@jrc.ec.europa.eu>
     * @version syrio_alpha_4 (28 July 2015)
     */
    public function actionCreate()
    {
        
        if (Yii::$app->user->isGuest)
        {
            return Yii::$app->error->ThrowError(\app\components\Errors::RESTRICTED_AREA_ERROR);
        }

        /*
         * the rest of authorization checks is done AFTER setting the default $model
         */
        $model = new EventDeclaration();
                
        //get user's details anf fill in the model
        $user = Yii::$app->user->getIdentity();
        $model->raporteur_name = $user->first_names . ' ' . $user->last_name;
        $model->raporteur_role = $user->role_in_organization;
        $model->contact_email = $user->email;
        $model->contact_tel = $user->phone;
        if (!$user->organization) {
            return Yii::$app->error->ThrowError(\app\components\Errors::RESTRICTED_AREA_ERROR);
        }
        $model->operator = $user->organization->organization_name;
        $model->raporteur_id = $user->id;
        $model->operator_id = $user->organization->id;
        
        /* (start) Author: vamanbo  Date:31/08/2015 On: allow registering new incident */
        if (Yii::$app->user->identity->isInstallationUser)
        {
            $model->installation_id = $user->installation->id;
        }

        if (!Yii::$app->user->can('incident_register', ['event' => $model]))
        {
            return Yii::$app->error->ThrowError(\app\components\Errors::RESTRICTED_AREA_ERROR);
        }
        /* (end) Author: vamanbo  Date:31/08/2015 On: allow registering new incident */
        
        
        /* escape input */
        $post_array = \yii\helpers\ArrayHelper::htmlEncode(Yii::$app->request->post());        
        
        if (Yii::$app->request->isAjax && $model->load($post_array))
        {
            Yii::$app->response->format = 'json';
            if (!$model->validate()) {
                return \yii\widgets\ActiveForm::validate($model);
                die();
            }
        }
        
        if ($model->load($post_array)) {
            /*
             * 
             */
            if (isset($model->event_date_time) && !isset($model->installation_id))
            {
                
                /*
                 * the selection of installations is already done in Installation::find()
                 */
                //$installations = \app\models\Installations::find()->where(['operator_id' => Yii::$app->user->identity->organization->id])->orderBy('name')->all();
                $installations = \app\models\Installations::find()->orderBy('name')->all();
                $instArray = \yii\helpers\ArrayHelper::map($installations, 'id', function($data) {
                    return $data->name . ' / ' . $data->type;
                });
                $model->scenario = 'step2';
                return $this->render('create', [
                    'model' => $model,
                    'step' => '2',
                    'installations' => $instArray,
                ]);
            }
            elseif(isset($model->event_date_time) && isset($model->installation_id) && (!isset($model->event_name) || $model->event_name == ''))
            {
                /*
                 * the selection of installations is already done in Installation::find()
                 */
                //$installations = \app\models\Installations::find()->where(['operator_id' => Yii::$app->user->identity->organization->id])->orderBy('name')->all();
                $installations = \app\models\Installations::find()->orderBy('name')->all();
                $instArray = \yii\helpers\ArrayHelper::map($installations, 'id', function($data) {
                    return $data->name . ' / ' . $data->type;
                });
                //$model->installation_name_type = $model->installation->name . ' / ' . $model->installation->type;
                $model->scenario = 'step3';
                return $this->render('create', [
                    'model' => $model,
                    'step' => '3',
                    'installations' => $instArray,
                ]);
            }
            elseif(isset($model->event_date_time) && isset($model->installation_id) && isset($model->event_name))
            {
                /*
                 * double check the curent user (if installation) can register the event
                 */
                
                if (!Yii::$app->user->can('incident_register', ['event' => $model]))
                {
                    return Yii::$app->error->ThrowError(\app\components\Errors::RESTRICTED_OPERATION_ERROR);
                }
                
                $msg = '';
                $model->del_status = EventDeclaration::STATUS_ACTIVE;
                $model->scenario = 'submit';
                
                $model->created = time();
                $model->created_by = \Yii::$app->user->id;
                
                $model->id = time();
                
                if ($model->validate())
                {
                    /* transaction session vamanbo 20150909 */
                    $transaction = Yii::$app->db->beginTransaction();
                    $ok=1;
                    
                    if ($model->save())   //$model->save()
                    {
                        //log the event history
                        $description = [
                            'by' => \Yii::$app->user->identity->full_name,
                            'at' => date('Y-m-d H:i:s', $model->created)
                        ];
                        
                        if (!\app\models\ReportingHistory::addHistoryItem($model->id, null, \app\models\ReportingHistory::EVENT_REGISTERED, \yii\helpers\Json::encode($description)))
                        {
                            $ok=0;
                        }
                    }
                    
                    if ($ok=1)
                    {
                        $link=Yii::$app->getUrlManager()->createUrl(['events/event-declaration/display-drafts', 'id'=>$model->id]);
                        $evt2 = \yii\helpers\Html::a(\Yii::t('app/messages', 'drafting the incident report'), $link, [
                            'class' => 'alert-link'
                        ]);
                        $msg = 'Incident \'{evt}\' has been successfully registered.\nPlease proceed with {evt2}.';
                        $msg = \Yii::t('app/messages', $msg, [
                            'evt' => $model->event_name,
                            'evt2' => $evt2
                        ]);
                        
                        $transaction->commit();
                    }
                    else
                    {
                        $transaction->rollBack();
                        $msg .= \app\components\helpers\TGenericMessage::Error(false);
                    }
                    
                    Yii::$app->session->setFlash('msg', $msg, true);

                    return $this->redirect(['index']);
                    
                    /* just keep it for now */
                    $searchModel = new EventDeclarationSearch();
                    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            
                    /* Author: vamanbo  31/08/2015 */
                    $canRegister = Yii::$app->user->can('inst_rapporteur') ? true : false;
                    
                    return $this->render('index', [
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,
                        'canRegister' => $canRegister
                    ]);
                }
                else
                {
                    $msg .= \app\components\helpers\TGenericMessage::Validation(false);
                    Yii::$app->session->setFlash('msg', $msg, true);
                    //$installations = \app\models\Installations::find()->where(['operator_id' => Yii::$app->user->identity->organization->id])->orderBy('name')->all();
                    $installations = \app\models\Installations::find()->orderBy('name')->all();
                    
                    $instArray = \yii\helpers\ArrayHelper::map($installations, 'id', function($data) {
                        return $data->name . ' / ' . $data->type;
                    });
                    //$model->installation_name_type = $model->installation->name . ' / ' . $model->installation->type;
                    $model->scenario = 'step3';
                    return $this->render('create', [
                        'model' => $model,
                        'step' => '3',
                        'installations' => $instArray,
                    ]);
                }
            }
            return 'done';
            die();
        } else {
            $model->scenario = 'step1';
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    

    
    /**
     * Updates an existing EventDeclaration model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if (Yii::$app->user->isGuest)
        {
            return Yii::$app->error->ThrowError(\app\components\Errors::RESTRICTED_AREA_ERROR);
        }

        /*
         * the rest of authorization checks is done AFTER setting the default $model
         */
        $model = $this->findModel($id);

        if (!Yii::$app->user->can('incident_update', ['event' => $model]))
        {
            return Yii::$app->error->ThrowError(\app\components\Errors::RESTRICTED_OPERATION_ERROR);
        }
        
        $model = $this->findModel($id);

        /* escape input */
        $post_array = \yii\helpers\ArrayHelper::htmlEncode(Yii::$app->request->post());        
        
        if (Yii::$app->request->isAjax && $model->load($post_array))
        {
            Yii::$app->response->format = 'json';
            if (!$model->validate()) {
                return \yii\widgets\ActiveForm::validate($model);
                die();
            }
        }
        
        if ($model->load($post_array)) {
            $model->modified_by_id = Yii::$app->user->id;

            if (!Yii::$app->user->can('incident_register', ['event' => $model]))
            {
                return Yii::$app->error->ThrowError(\app\components\Errors::RESTRICTED_OPERATION_ERROR);
            }

            $msg = 'Incident \'' . $model->event_name . '\' ';
            $model->scenario = 'submit';
            if ($model->validate())
            {
                $model->modified_by_id = \Yii::$app->user->id;
                $model->modified_at = time();
                if ($model->save())   //$model->save()
                {
                    $link=Yii::$app->getUrlManager()->createUrl(['events/event-declaration/display-drafts', 'id'=>$model->id]);
                    $msg .= 'has been successfully updated.\n';
                }
                else
                {
                    $msg .= \app\components\helpers\TGenericMessage::Error(false);
                }
                Yii::$app->session->setFlash('msg', $msg, true);

                
                return $this->redirect(['index']);
            }
            else
            {
                $msg .= \app\components\helpers\TGenericMessage::Validation(false);
                Yii::$app->session->setFlash('msg', $msg, true);
                //$installations = \app\models\Installations::find()->where(['operator_id' => Yii::$app->user->identity->organization->id])->orderBy('name')->all();
                $installations = \app\models\Installations::find()->orderBy('name')->all();

                $instArray = \yii\helpers\ArrayHelper::map($installations, 'id', function($data) {
                    return $data->name . ' / ' . $data->type;
                });
                //$model->installation_name_type = $model->installation->name . ' / ' . $model->installation->type;
                $model->scenario = 'step3';
                return $this->render('update', [
                    'model' => $model,
                    'step' => '3',
                    'installations' => $instArray,
                ]);
            }
            
            
            
            
        } else {
            $installations = \app\models\Installations::find()->orderBy('name')->all();

            $instArray = \yii\helpers\ArrayHelper::map($installations, 'id', function($data) {
                return $data->name . ' / ' . $data->type;
            });
            //$model->installation_name_type = $model->installation->name . ' / ' . $model->installation->type;
            $model->scenario = 'step3';
            return $this->render('update', [
                'model' => $model,
                'step' => '3',
                'installations' => $instArray,
            ]);
        }
    }

    /**
     * Purges (permanently deletes) an existing EventDeclaration model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionPurge($id)
    {
        if (Yii::$app->user->isGuest)
        {
            return Yii::$app->error->ThrowError(\app\components\Errors::RESTRICTED_AREA_ERROR);
        }

        /*
         * the rest of authorization checks is done AFTER setting the default $model
         */
        $model = $this->findModel($id);

        if (!Yii::$app->user->can('incident_purge', ['event' => $model]))
        {
            return Yii::$app->error->ThrowError(\app\components\Errors::RESTRICTED_OPERATION_ERROR);
        }
        
        if ($model->delete())
        {
            $evt = ucfirst(\Yii::t('app', 'the incident'));
            $msg = \app\components\helpers\TGenericMessage::Purged($evt);
            Yii::$app->session->setFlash('msg', $msg, true);
        }
        else
        {
            $msg = \app\components\helpers\TGenericMessage::Error(false);
            Yii::$app->session->setFlash('msg', $msg, true);
        }

        $searchModel = new EventDeclarationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        /* Author: vamanbo  31/08/2015 */
        $canRegister = Yii::$app->user->can('inst_rapporteur') ? true : false;

        return $this->redirect(['trash']);
    }

    /**
     * Deletes an existing EventDeclaration model. The operation can be undone.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if (Yii::$app->user->isGuest)
        {
            return Yii::$app->error->ThrowError(\app\components\Errors::RESTRICTED_AREA_ERROR);
        }

        /*
         * the rest of authorization checks is done AFTER setting the default $model
         */
        $model = $this->findModel($id);

        if (!Yii::$app->user->can('incident_delete', ['event' => $model]))
        {
            return Yii::$app->error->ThrowError(\app\components\Errors::RESTRICTED_OPERATION_ERROR);
        }
        
        $model = $this->findModel($id);
        
        $model->del_status = EventDeclaration::STATUS_DELETED;
        $model->modified_by_id = Yii::$app->user->id;
        $model->modified_at = time();

        $evt = ucfirst(\Yii::t('app', 'the incident'))  . ' ' .  '\'' . $model->event_name . '\'';
        if ($model->save())
        {
            $msg = \app\components\helpers\TGenericMessage::Removed($evt);
            Yii::$app->session->setFlash('msg', $msg, true);
        }
        else
        {
            $msg = \app\components\helpers\TGenericMessage::Error(false);
            Yii::$app->session->setFlash('msg', $msg, true);
        }

        $searchModel = new EventDeclarationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        /* Author: vamanbo  31/08/2015 */
        $canRegister = Yii::$app->user->can('inst_rapporteur') ? true : false;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'canRegister' => $canRegister
        ]);
        
    }
    
    public function actionRequestDeletion($id)
    {
        $model = $this->findModel($id);
        return 'i should display the delete request form for ' . $model->event_name;
    }
    
    
    /**
     * Deletes an existing EventDeclaration model. The operation can be undone.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionReactivate($id)
    {
        if (Yii::$app->user->isGuest)
        {
            return Yii::$app->error->ThrowError(\app\components\Errors::RESTRICTED_AREA_ERROR);
        }

        /*
         * the rest of authorization checks is done AFTER setting the default $model
         */
        $model = $this->findModel($id);

        if (!Yii::$app->user->can('incident_delete', ['event' => $model]))
        {
            return Yii::$app->error->ThrowError(\app\components\Errors::RESTRICTED_OPERATION_ERROR);
        }
        
        $model = $this->findModel($id);
        
        $model->del_status = EventDeclaration::STATUS_ACTIVE;
        $model->modified_by_id = Yii::$app->user->id;
        $model->modified_at = time();

        $evt = ucfirst(\Yii::t('app', 'the incident')) . ' ' . '\'' . $model->event_name . '\'';
        if ($model->save())
        {
            $msg=  \app\components\helpers\TGenericMessage::Reactivated($evt, false);
            Yii::$app->session->setFlash('msg', $msg, true);
        }
        else
        {
            Yii::$app->session->setFlash('msg', \app\components\helpers\TGenericMessage::Error(false), true);
        }

        
        return $this->redirect(['index']);
        
    }
    
    
    
    public function actionViewDrafts($id)
    {
        $model = $this->findModel($id);
        $url = Yii::$app->urlManager->createAbsoluteUrl(['events/drafts/event-draft', 'evtID'=>$model->id]);
        return $this->redirect($url);
    }
    
    public function actionDisplayDrafts($id)
    {
        $model = $this->findModel($id);
        if (!Yii::$app->user->can('incident_view', ['event'=>$model]))
        {
            return \Yii::$app->error->ThrowError(\app\components\Errors::RESTRICTED_AREA_ERROR);
        }
        
        $url = \Yii::$app->urlManager->createUrl(['/events/drafts/event-draft/index', 'evtID'=>$model->id]);
        
        return $this->redirect($url);
    }
    
    /**
     * Method for signing a report draft
     * 
     * @param int $id The [id] of the reporting draft to be signed
     */
    public function actionSignDraft($id)
    {
        if (!Yii::$app->user->can('incident_draft_sign'))
        {
            return \Yii::$app->error->ThrowError(\app\components\Errors::RESTRICTED_OPERATION_ERROR);
        }
        
        $model = $this->findModel($id);
        $model->status = EventDeclaration::INCIDENT_REPORT_SIGNED;
        $model->signed_by = Yii::$app->user->id();
        $dt = new \DateTime();
        $model->signed_at = date_format($dt, 'Y-m-d H:i:s');
        
        /* transaction session vamanbo 20150909 */
        $transaction = Yii::$app->db->beginTransaction();
        $ok=1;
        if ($model->save())
        {
            //log the event history
            $description = [
                'by' => \Yii::$app->user->identity->full_name,
                'at' => date('Y-m-d H:i:s', time())
            ];
            
            if (!\app\models\ReportingHistory::addHistoryItem($model->evt_id, $model->id , \app\models\ReportingHistory::EVENT_SIGNED, \yii\helpers\Json::encode($description)))
            {
                $ok = 0;
            };
        }
        else
        {
            $ok = 0;
        }
        
        if ($ok == 1)
        {
            $transaction->commit();
            $msg = 'Incident report for \'' . $model->event_name . '\.\nYou may now proceed with submitting the Report to the Competent Authority.';
        }
        else
        {
            $transaction->rollBack();
            $msg = 'UNABLE TO SIGN the Incident report for \'' . $model->event_name . '\.\nPlease try again later.' . '[err]';
        }
    }
    
    public function actionReview($id, $draftId)
    {
        return $this->render('review');
    }
    
    
    
    /**
     * Finds the EventDeclaration model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return EventDeclaration the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = EventDeclaration::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    
    public function actionHistory($evt_id) {
        $model = $this->findModel($evt_id);
        
        if (!\Yii::$app->user->can('incident_view_operators', ['event'=>$model])) {
            return \Yii::$app->error->ThrowError(Errors::RESTRICTED_AREA_ERROR);
        } else {
            $params = Yii::$app->request->queryParams;
            $searchModel = new \app\models\ReportingHistorySearch();
            $searchModel->evt_id = $evt_id;
            $dataProvider = $searchModel->search($params);
            
            return $this->render('history', [
                'searchModel' => $searchModel, 
                'dataProvider' => $dataProvider, 
                'evt_id'=>$evt_id,
                'evt_name' => $model->event_name]);
        }
    }
    
    
    public function actionClearHistory($id) {

        $theId = intval($id);
        $model = $this->findModel($theId);
        //this can only be done by the oo_rapp
        if (!(\Yii::$app->user->can('clear_history', ['event'=>$model]) || \Yii::$app->user->can('sys_admin'))) {
            return \Yii::$app->error->ThrowError(Errors::RESTRICTED_AREA_ERROR);
        }
        
        $actionMessage = new ActionMessage();
        
        //delete all the history records that are for the CA alone
        
        $transaction = \Yii::$app->db->beginTransaction();
        
        if (\app\models\ReportingHistory::deleteAll('evt_id = :evt_id AND visibility = :visibility', [
            'evt_id' => $theId,
            'visibility' => \app\models\ReportingHistory::VISIBLE_OO
        ])) {
        
            $transaction->commit();
            $actionMessage->SetSuccess(\Yii::t('app', 'Clear history'), \Yii::t('app', 'The OO related history has been cleared!'));
            Yii::$app->session->set('finished_action_result', $actionMessage);

        } else {
            
            $transaction->rollBack();
            
            $msg = \app\components\helpers\TGenericMessage::Error(true);
            
            $actionMessage->SetError(\Yii::t('app', 'Clear history'), $msg);
            Yii::$app->session->set('finished_action_result', $actionMessage);
            
        }
        
        return $this->redirect(['history', 'evt_id'=>$id]);
    }    
}
