<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\controllers\events\drafts\sections\subsections;

ignore_user_abort('true');
set_time_limit(0);

use Yii;
use app\models\events\drafts\sections\subsections\E2;
use app\models\events\drafts\sections\subsections\E2Search;
use app\models\common\ActionMessage;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * E2Controller implements the CRUD actions for E2 model.
 */
class E2Controller extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                //only authenticated users
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Updates an existing E2 model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        //incident_draft_create_operators
        if (!Yii::$app->user->can('incident_draft_section_edit', ['section' => $model->sE]))
        {
                return Yii::$app->error->ThrowError(\app\components\Errors::RESTRICTED_OPERATION_ERROR);
        }
        
        if ($model->load(Yii::$app->request->post())) {
            
            
            $actMessage = new ActionMessage();
            
            $model->validate();
            if ($model->hasErrors()) 
            {
                $errMsg = '';
                foreach ($model->getErrors() as $error) {
                    if (is_array($error)) {
                        foreach ($error as $key=>$value) {
                            $errMsg .= $value . '<br/>';
                        }
                    }
                    else
                    {
                        $errMsg .= $error . '<br/>';
                    }
                }
                $actMessage->SetWarning(Yii::t('app', 'Unable to save changes'), $errMsg);
                Yii::$app->session->set('finished_action_result', $actMessage);
                
                return $this->render('update', [
                    'model' => $model,
                ]);
                
            }
            
            if (!$model->save())
            {
                $errMsg = Yii::t('app', 'Please retry later.');
                $actMessage->SetWarning(Yii::t('app', 'Unable to save changes'), $errMsg);
                Yii::$app->session->set('finished_action_result', $actMessage);
                
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
            
            //unlock 
            $SE = $model->sE;
            $SE->status = \app\models\events\EventDeclaration::INCIDENT_REPORT_DRAFT;
            $SE->locked_by = NULL;
            $SE->locked_at = 0;
            $SE->save();
            
            $errMsg = Yii::t('app', 'Section has been successfully modified.');
            $actMessage->SetSuccess(Yii::t('app', 'Section modified'), $errMsg);
            Yii::$app->session->set('finished_action_result', $actMessage);
            
            $url = '/events/drafts/event-draft/view-section-e';
            return $this->redirect([$url, 'id' => $model->sE->eventDraft->id, 'v' => 2]);        
            
        } 
        else 
        {
           //lock
            $SE = $model->sE;
            $SE->locked_by = \Yii::$app->user->id;
            $SE->locked_at = time();
            $SE->status = \app\models\events\EventDeclaration::INCIDENT_REPORT_LOCKED;
            $SE->save();
            
            $model->ref = Yii::$app->request->referrer;
            
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }


    public function actionCancel($id, $ref)
    {
        $model = $this->findModel($id);
        //unlock SE
        $SE = $model->sE;
        $SE->locked_at = 0;
        $SE->status = \app\models\events\EventDeclaration::INCIDENT_REPORT_DRAFT;
        $SE->locked_by = NULL;
        $SE->save();    
        
        //$this->goBack();
        
        if($ref){
            echo '<script>alert($ref);</script>';
            return $this->redirect($ref);
        }else{
            return $this->goHome();
        }        
        
    }        
    
    /**
     * Cancels an Update action on a section and unlock it.
     * Use this in conjunction with the navigation-away javascript.
     * 
     * @param int $id the id of the section to be unlocked
     */
    public function actionCancelA($id)
    {
        $model = $this->findModel($id);
        //unlock SB
        $SE = $model->sE;
        $SE->status = \app\models\events\EventDeclaration::INCIDENT_REPORT_DRAFT;
        $SE->locked_by = NULL;
        $SE->locked_at = 0;
        $SE->save();    
        
        die();
    }    
    
    /**
     * Finds the E2 model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return E2 the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = E2::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
