<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\controllers\events\drafts\sections\subsections;

ignore_user_abort('true');
set_time_limit(0);

use Yii;
use app\models\events\drafts\sections\subsections\A1;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use app\models\common\ActionMessage;

use app\models\crf\LeakCause;
use app\components\helpers\TArrayHelper;

use app\models\crf\EventReleaseCause;

/**
 * A1Controller implements the CRUD actions for A1 model.
 */
class A1Controller extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                //only authenticated users
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }


    /**
     * Updates an existing A1 model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        //incident_draft_create_operators
        if (!Yii::$app->user->can('incident_draft_section_edit', ['section' => $model->sA]))
        {
                return Yii::$app->error->ThrowError(\app\components\Errors::RESTRICTED_OPERATION_ERROR);
        }
        
        if ($model->load(Yii::$app->request->post())) {

            $actMessage = new ActionMessage();

            //I need it here to have all the values casted into appropriate data types!
            //otherwise, the dirtyAttributes WON'T WORK!!!!!!!!
            $model->validate();
            
            /* get the POST values for rel_causes */
            if (isset($_POST['rel_causes'])) {
                $release_causes = $_POST['rel_causes'];
            } else {
                $release_causes = [];
            }
            $possible_values = TArrayHelper::getColumn(LeakCause::find()->where(['>','category_id', 0])
                    ->asArray()->all(), 'num_code');
            
            /*
             * Validation of the values (server side)
             * 
             * each value must be an integer
             * each value should be in the array of possible values
             * 
             */
            $array_dif = array_diff($release_causes, $possible_values);
            if (count($array_dif)>0) {
                $model->addError(Yii::t('app/crf', 'Cause of release'), Yii::t('app/crf', 'Cause of release') . ': ' . 'bad parameter received!');
            }

            //update the causes of release tables
            $existing_causes = TArrayHelper::getColumn(EventReleaseCause::find()->where(['a1_id'=>$model->id])->all(), 'leak_cause_id');
            $to_add = array_diff($release_causes, $existing_causes);
            $to_remove = array_diff($existing_causes, $release_causes);

            if ($model->hasErrors()) 
            {
                $errMsg = '';
                foreach ($model->getErrors() as $error) {
                    if (is_array($error)) {
                        foreach ($error as $key=>$value) {
                            $errMsg .= $value . '<br/>';
                        }
                    }
                    else
                    {
                        $errMsg .= $error . '<br/>';
                    }
                }
                $actMessage->SetWarning(Yii::t('app', 'Unable to save changes'), $errMsg);
                Yii::$app->session->set('finished_action_result', $actMessage);
                
                return $this->render('update', [
                    'model' => $model,
                    //'s' => $s
                ]);
            }
            
            
            $transaction = Yii::$app->db->beginTransaction();
            
            if (!$model->save())
            {
                $transaction->rollBack();
                
                $errMsg = Yii::t('app', 'Please retry later.');
                $actMessage->SetWarning(Yii::t('app', 'Unable to save changes'), $errMsg);
                Yii::$app->session->set('finished_action_result', $actMessage);
                
                return $this->render('update', [
                    'model' => $model,
                    //'s' => $s
                ]);
            }

            try {
                /* update the EventReleaseCauses (table a1_release_causes) */
                //remove all in $toRemove
                if (count($to_remove)>0) {
                    EventReleaseCause::deleteAll(['a1_id'=>$model->id, 'leak_cause_id'=>$to_remove]);
                }

                if (count($to_add)>0) {
                    //add all in $to_add through batchInsert
                    $values= [];
                    foreach($to_add as $code) {
                        $values[] = [$model->id,$code];
                    }
                    Yii::$app->db->createCommand()->batchInsert('{{a1_release_causes}}',
                            ['[[a1_id]], [[leak_cause_id]]'], 
                            $values)->execute();
                }
            } catch (Exception $ex) {
                $transaction->rollBack();

                $errMsg = Yii::t('app', 'Please retry later.');
                $actMessage->SetWarning(Yii::t('app', 'Unable to save changes'), $errMsg);
                Yii::$app->session->set('finished_action_result', $actMessage);


                $url = '/events/drafts/event-draft/view-section-a';

                return $this->render('update', [
                    'model' => $model,
                    //'s' => $s
                ]);
            }
            
            //echo var_dump($model->a1_op);
            //die();
            
            //unlock 
            $SA = $model->sA;
            $SA->status = \app\models\events\EventDeclaration::INCIDENT_REPORT_DRAFT;
            $SA->locked_at = 0;
            $SA->locked_by = NULL;
            $SA->update();// save();

            
            $transaction->commit();
            
            
            $errMsg = Yii::t('app', 'Section has been successfully modified.');
            $actMessage->SetSuccess(Yii::t('app', 'Section modified'), $errMsg);
            Yii::$app->session->set('finished_action_result', $actMessage);

            
            $url = '/events/drafts/event-draft/view-section-a';
            
            return $this->redirect([$url, 'id' => $model->sA->eventDraft->id, 'v' => 1]);
        } else {
            //lock
            $SA = $model->sA;
            $SA->locked_by = \Yii::$app->user->id;
            $SA->locked_at = time();
            $SA->status = \app\models\events\EventDeclaration::INCIDENT_REPORT_LOCKED;
            $SA->save();
            
            $model->ref = Yii::$app->request->referrer;
            
            return $this->render('update', ['model' => $model]);
        }
    }

    
    public function actionViewAll($id)
    {
        if (\Yii::$app->user->can('op_user'))
        {
            return $this->render('view_all', [
                'model' => $this->findModel($id),
            ]);
        }
    }
    
    
    public function actionCancel($id, $ref)
    {
        $model = $this->findModel($id);
        //unlock SA
        $SA = $model->sA;
        $SA->status = \app\models\events\EventDeclaration::INCIDENT_REPORT_DRAFT;
        $SA->locked_by = NULL;
        $SA->save();    
        
        //$this->goBack();
        
        if($ref){
            echo '<script>alert($ref);</script>';
            return $this->redirect($ref);
        }else{
            return $this->goHome();
        }        
        
    }        
    
    /**
     * Cancels an Update action on a section and unlock it.
     * Use this in conjunction with the navigation-away javascript.
     * 
     * @param int $id the id of the section to be unlocked
     */
    public function actionCancelA($id)
    {
        $model = $this->findModel($id);
        //unlock SA
        $SA = $model->sA;
        $SA->status = \app\models\events\EventDeclaration::INCIDENT_REPORT_DRAFT;
        $SA->locked_by = NULL;
        $SA->locked_at = 0;
        $SA->save();    
        
        die();
    }    

     /**
     * Finds the A1 model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return A1 the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = A1::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
