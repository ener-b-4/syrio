<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\controllers\events\drafts\sections\subsections;

ignore_user_abort('true');
set_time_limit(0);

use Yii;
use app\models\events\drafts\sections\subsections\C1;
use app\models\events\drafts\sections\subsections\C1Search;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use app\models\common\ActionMessage;

/**
 * C1Controller implements the CRUD actions for C1 model.
 */
class C1Controller extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                //only authenticated users
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }


    /**
     * Updates an existing C1 model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        //incident_draft_create_operators
        if (!Yii::$app->user->can('incident_draft_section_edit', ['section' => $model->sC]))
        {
                return Yii::$app->error->ThrowError(\app\components\Errors::RESTRICTED_OPERATION_ERROR);
        }
        
        if ($model->load(Yii::$app->request->post())) {
            $actMessage = new ActionMessage();
            
            $model->validate();
            if ($model->hasErrors()) 
            {
                $errMsg = '';
                foreach ($model->getErrors() as $error) {
                    if (is_array($error)) {
                        foreach ($error as $key=>$value) {
                            $errMsg .= $value . '<br/>';
                        }
                    }
                    else
                    {
                        $errMsg .= $error . '<br/>';
                    }
                }
                $actMessage->SetWarning(Yii::t('app', 'Unable to save changes'), $errMsg);
                Yii::$app->session->set('finished_action_result', $actMessage);
                
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
            if (!$model->save())
            {
                $errMsg = Yii::t('app', 'Please retry later.');
                $actMessage->SetWarning(Yii::t('app', 'Unable to save changes'), $errMsg);
                Yii::$app->session->set('finished_action_result', $actMessage);
                
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
            
            
            //unlock
            $SC = $model->sC;
            $SC->status = \app\models\events\EventDeclaration::INCIDENT_REPORT_DRAFT;
            $SC->locked_by = NULL;
            $SC->locked_at = 0;
            $SC->save();
            
            $errMsg = Yii::t('app', 'Section has been successfully modified.');
            $actMessage->SetSuccess(Yii::t('app', 'Section modified'), $errMsg);
            Yii::$app->session->set('finished_action_result', $actMessage);
            
            $url = '/events/drafts/event-draft/view-section-c';      
            return $this->redirect([$url, 'id' => $model->sC->eventDraft->id, 'v' => 1]);
        } else {
            //lock
            $SC = $model->sC;
            $SC->status = \app\models\events\EventDeclaration::INCIDENT_REPORT_LOCKED;
            $SC->locked_by = \Yii::$app->user->id;
            $date = time();
            $SC->locked_at = time();            
            $SC->save();

            $model->ref = Yii::$app->request->referrer;
            
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }


    public function actionCancel($id, $ref)
    {
        $model = $this->findModel($id);
        //unlock SB
        $SC = $model->sC;
        $SC->status = \app\models\events\EventDeclaration::INCIDENT_REPORT_DRAFT;
        $SC->locked_by = NULL;
        $SC->locked_at = 0;
        $SC->save();    
        
        //$this->goBack();
        
        if(isset($ref)){
            echo '<script>alert($ref);</script>';
            return $this->redirect($ref);
        }else{
            die();
            //return $this->goHome();
        }        
        
    }    
    

    public function actionCancelA($id)
    {
        $model = $this->findModel($id);
        //unlock SB
        $SC = $model->sC;
        $SC->status = \app\models\events\EventDeclaration::INCIDENT_REPORT_DRAFT;
        $SC->locked_by = NULL;
        $SC->locked_at = 0;
        $SC->save();    
        
        die();
    }    
    
    
    
    /**
     * Finds the C1 model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return C1 the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = C1::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
