<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\controllers\events\drafts\sections\subsections;

ignore_user_abort('true');
set_time_limit(0);

use Yii;
use app\models\events\drafts\sections\subsections\B1;
use app\models\events\drafts\sections\subsections\B1Search;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use app\models\common\ActionMessage;

/**
 * B1Controller implements the CRUD actions for B1 model.
 */
class B1Controller extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                //only authenticated users
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Updates an existing B1 model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        //incident_draft_create_operators
        if (!Yii::$app->user->can('incident_draft_section_edit', ['section' => $model->sB]))
        {
                return Yii::$app->error->ThrowError(\app\components\Errors::RESTRICTED_OPERATION_ERROR);
        }
        
        if ($model->load(Yii::$app->request->post())) {
            $actMessage = new ActionMessage();
            
            $model->validate();
            /*inline validation
             *  it is not implemented as a validation rule in the model to minimize the queries to the db
             *  the event_date_time is already sent as form variable 
            */
            if (isset($model->b1_4_s) && $model->b1_4_s != "") {
                $eventDateTime = \date($_POST['event_date_time']);
                $startDate = \date($model->b1_4_s);
                if ($startDate < $eventDateTime) {
                    $model->addError('b1_4_s', Yii::t('app', 'This event date and time (d) cannot be prior to the date and time declared in Event Declaration.'));
                }
            }
            
            if ($model->hasErrors()) 
            {
                $errMsg = '';
                foreach ($model->getErrors() as $error) {
                    if (is_array($error)) {
                        foreach ($error as $key=>$value) {
                            $errMsg .= $value . '<br/>';
                        }
                    }
                    else
                    {
                        $errMsg .= $error . '<br/>';
                    }
                }
                $actMessage->SetWarning(Yii::t('app', 'Unable to save changes'), $errMsg);
                Yii::$app->session->set('finished_action_result', $actMessage);
                
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
            if (!$model->save())
            {
                $errMsg = Yii::t('app', 'Please retry later.');
                $actMessage->SetWarning(Yii::t('app', 'Unable to save changes'), $errMsg);
                Yii::$app->session->set('finished_action_result', $actMessage);
                
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
            
            
            //unlock 
            $SB = $model->sB;
            $SB->status = \app\models\events\EventDeclaration::INCIDENT_REPORT_DRAFT;
            $SB->locked_by = NULL;
            $SB->locked_at = 0;
            $SB->save();
            
            $errMsg = Yii::t('app', 'Section has been successfully modified.');
            $actMessage->SetSuccess(Yii::t('app', 'Section modified'), $errMsg);
            Yii::$app->session->set('finished_action_result', $actMessage);
            
            $url = '/events/drafts/event-draft/view-section-b';
            return $this->redirect([$url, 'id' => $model->sB->eventDraft->id, 'v' => 1]);
        } else {
            //lock
            $SB = $model->sB;
            $SB->locked_by = \Yii::$app->user->id;
            $SB->locked_at = time();
            $SB->status = \app\models\events\EventDeclaration::INCIDENT_REPORT_LOCKED;
            $SB->save();
            
            $model->ref = Yii::$app->request->referrer;
            
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }


    public function actionCancel($id, $ref)
    {
        $model = $this->findModel($id);
        //unlock SB
        $SB = $model->sB;
        $SB->status = \app\models\events\EventDeclaration::INCIDENT_REPORT_DRAFT;
        $SB->locked_by = NULL;
        $SB->locked_at = 0;
        $SB->save();    
        
        //$this->goBack();
        
        if($ref){
            echo '<script>alert($ref);</script>';
            return $this->redirect($ref);
        }else{
            return $this->goHome();
        }        
        
    }    
    
    /**
     * Cancels an Update action on a section and unlock it.
     * Use this in conjunction with the navigation-away javascript.
     * 
     * @param int $id the id of the section to be unlocked
     */
    public function actionCancelA($id)
    {
        $model = $this->findModel($id);
        //unlock SB
        $SB = $model->sB;
        $SB->status = \app\models\events\EventDeclaration::INCIDENT_REPORT_DRAFT;
        $SB->locked_by = NULL;
        $SB->locked_at = 0;
        $SB->save();    
        
        die();
    }    
    
    /**
     * Finds the B1 model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return B1 the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = B1::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
