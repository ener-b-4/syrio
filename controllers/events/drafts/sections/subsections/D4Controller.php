<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\controllers\events\drafts\sections\subsections;

ignore_user_abort('true');
set_time_limit(0);

use Yii;
use app\models\events\drafts\sections\subsections\D4;
use app\models\events\drafts\sections\subsections\D4Search;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use app\models\common\ActionMessage;

/**
 * D4Controller implements the CRUD actions for D4 model.
 */
class D4Controller extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                //only authenticated users
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }


    /**
     * Updates an existing D4 model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id); //D4

        //incident_draft_create_operators
        if (!Yii::$app->user->can('incident_draft_section_edit', ['section' => $model->sD]))
        {
                return Yii::$app->error->ThrowError(\app\components\Errors::RESTRICTED_OPERATION_ERROR);
        }
        
        if ($model->load(Yii::$app->request->post())) {
            $actMessage = new ActionMessage();
            
            $model->validate();
            if ($model->hasErrors()) 
            {
                $errMsg = '';
                foreach ($model->getErrors() as $error) {
                    if (is_array($error)) {
                        foreach ($error as $key=>$value) {
                            $errMsg .= $value . '<br/>';
                        }
                    }
                    else
                    {
                        $errMsg .= $error . '<br/>';
                    }
                }
                $actMessage->SetWarning(Yii::t('app', 'Unable to save changes'), $errMsg);
                Yii::$app->session->set('finished_action_result', $actMessage);
                
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
            if (!$model->save())
            {
                $errMsg = Yii::t('app', 'Please retry later.');
                $actMessage->SetWarning(Yii::t('app', 'Unable to save changes'), $errMsg);
                Yii::$app->session->set('finished_action_result', $actMessage);
                
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
            
            
            //unlock 
            $SD = $model->sD;
            $SD->status = \app\models\events\EventDeclaration::INCIDENT_REPORT_DRAFT;
            $SD->locked_by = NULL;
            $SD->locked_at = 0;
            $SD->save();
            
            $errMsg = Yii::t('app', 'Section has been successfully modified.');
            $actMessage->SetSuccess(Yii::t('app', 'Section modified'), $errMsg);
            Yii::$app->session->set('finished_action_result', $actMessage);
            
            $url = '/events/drafts/event-draft/view-section-d';
            return $this->redirect([$url, 'id' => $model->sD->eventDraft->id, 'v' => 4]);        
            
        } 
        else 
        {
           //lock
            $SD = $model->sD;
            $SD->locked_by = \Yii::$app->user->id;
            $SD->locked_at = time();
            $SD->status = \app\models\events\EventDeclaration::INCIDENT_REPORT_LOCKED;
            $SD->save();
            
            $model->ref = Yii::$app->request->referrer;
            
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionCancel($id, $ref)
    {
        $model = $this->findModel($id);
        //unlock SD
        $SD = $model->sD;
        $SD->status = \app\models\events\EventDeclaration::INCIDENT_REPORT_DRAFT;
        $SD->locked_by = NULL;
        $SD->locked_at = 0;
        $SD->save();    
        
        //$this->goBack();
        
        if($ref){
            echo '<script>alert($ref);</script>';
            return $this->redirect($ref);
        }else{
            return $this->goHome();
        }        
        
    }        
    
    /**
     * Cancels an Update action on a section and unlock it.
     * Use this in conjunction with the navigation-away javascript.
     * 
     * @param int $id the id of the section to be unlocked
     */
    public function actionCancelA($id)
    {
        $model = $this->findModel($id);
        //unlock SD
        $SD = $model->sD;
        $SD->status = \app\models\events\EventDeclaration::INCIDENT_REPORT_DRAFT;
        $SD->locked_by = NULL;
        $SD->locked_at = 0;
        $SD->save();    
        
        die();
    }    
    
    /**
     * Finds the D4 model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return D4 the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = D4::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
