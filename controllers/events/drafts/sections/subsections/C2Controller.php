<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\controllers\events\drafts\sections\subsections;

ignore_user_abort('true');
set_time_limit(0);

use Yii;
use app\models\events\drafts\sections\subsections\C2;
use app\models\events\drafts\sections\subsections\C2Search;

use app\models\events\drafts\sections\subsections\C2_1;
use app\models\events\drafts\sections\subsections\C2_2;
use app\models\events\drafts\sections\subsections\SC21Sece;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;


use app\models\common\ActionMessage;

/**
 * C2Controller implements the CRUD actions for C2 model.
 */
class C2Controller extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                //only authenticated users
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ]
            ],
           'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Updates an existing C2 model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        
        //incident_draft_create_operators
        if (!Yii::$app->user->can('incident_draft_section_edit', ['section' => $model->sC]))
        {
                return Yii::$app->error->ThrowError(\app\components\Errors::RESTRICTED_OPERATION_ERROR);
        }
        
        $sc2_1 = $model->sC2_1;
        $sc2_2 = $model->sC2_2;
        
        if ($model->load(Yii::$app->request->post()) && 
                ($sc2_1->load(Yii::$app->request->post())) &&
                $sc2_2->load(Yii::$app->request->post())) {

            $actMessage = new ActionMessage();
        
            if (!$sc2_1->validate())
            {
                foreach ($sc2_1->getErrors() as $error) {
                    foreach ($error as $item=>$key)
                    {
                        $model->addError('sC2_1', 'C.2.1. ' . $key);
                    }
                }
            }
            if (!$sc2_2->validate())
            {
                foreach ($sc2_2->getErrors() as $error) {
                    foreach ($error as $item=>$key)
                    {
                        $model->addError('sC2_2', 'C.2.2. ' . $key);
                    }
                }
            }
            
            if ($model->hasErrors()) 
            {
                $errMsg = '';
                foreach ($model->getErrors() as $error) {
                    if (is_array($error)) {
                        foreach ($error as $key=>$value) {
                            $errMsg .= $value . '<br/>';
                        }
                    }
                    else
                    {
                        $errMsg .= $error . '<br/>';
                    }
                }
                $actMessage->SetWarning(Yii::t('app', 'Unable to save changes'), $errMsg);
                Yii::$app->session->set('finished_action_result', $actMessage);
                
                return $this->render('update', [
                    'model' => $model,
                    'sC2_1' => $sc2_1,
                    'sC2_2' => $sc2_2
                ]);
                
            }

            
            $transaction = Yii::$app->db->beginTransaction();
            $commit_transaction = 1;
            if (!$model->save()) { $commit_transaction = 0; }
            
            if (!$sc2_1->save())  { $commit_transaction = 0; }
            if (!$sc2_2->save()) { $commit_transaction = 0; }

            
            /*
             * UPDATE Section C2_1 SECES
             */
            
            
        if (key_exists('C2_1', $_POST)) {
            
            $c21_posts = $_POST['C2_1'];
            
            //delete all the SECEs that are related to this sC2_1
            SC21Sece::deleteAll(['s_c_2_1_id' => $model->sC2_1->id]);
            
            //go through the new seces and load them
            foreach (range('a', 'k') as $letter) {
                $sece_key = 'sece_'.$letter;
                
                //add all the seces in c21_posts[$sece_key]
                
                if (key_exists($sece_key, $c21_posts) && is_array($c21_posts[$sece_key])) {
                    foreach($c21_posts[$sece_key] as $sece_code) {
                        $new_link = new SC21Sece();
                        $new_link->s_c_2_1_id = $model->sC2_1->id;
                        $new_link->sece_id = $sece_code;

                        if (!$new_link->save()) { $commit_transaction = 0; }
                    }
                }   //end adding the seces
            }       //end going through a-j

            if ($commit_transaction === 1) {
                $errMsg = Yii::t('app', 'Section has been successfully modified.');
                $actMessage->SetSuccess(Yii::t('app', 'Section modified'), $errMsg);
                Yii::$app->session->set('finished_action_result', $actMessage);
                $transaction->commit();
            } else {
                $errMsg = Yii::t('app', 'There\'s been an error when saving your changes.');
                $actMessage->SetError('Section has not been modified.', $errMsg);
                Yii::$app->session->set('finished_action_result', $actMessage);
                $transaction->rollBack();
            }
        }
        

            
            //unlock
            $SC = $model->sC;
            $SC->status = \app\models\events\EventDeclaration::INCIDENT_REPORT_DRAFT;
            $SC->locked_by = NULL;
            $SC->locked_at = 0;
            $SC->save();
            
            $url = '/events/drafts/event-draft/view-section-c';      
            return $this->redirect([$url, 'id' => $model->sC->eventDraft->id, 'v' => 2]);
            
        } else {
            //lock
            $SC = $model->sC;
            $SC->status = \app\models\events\EventDeclaration::INCIDENT_REPORT_LOCKED;
            $SC->locked_by = \Yii::$app->user->id;
            //$date = new \DateTime();
            $SC->locked_at = time();            
            $SC->save();

            $refferer = Yii::$app->request->referrer;
            $model->ref = $refferer;
            
            return $this->render('update', [
                'model' => $model,
                'sC2_1' => $model->sC2_1,
                'sC2_2' => $model->sC2_2
            ]);
        }
    }


    /**
     * Finds the C2 model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return C2 the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = C2::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    

    /**
     * 
     * @param int $id the id of the section to be unlocked
     * @param string $ref where to navigate afterwards
     * @return View
     */
    public function actionCancel($id, $ref)
    {
        $model = $this->findModel($id);
        //unlock SB
        $SC = $model->sC;
        $SC->status = \app\models\events\EventDeclaration::INCIDENT_REPORT_DRAFT;
        $SC->locked_by = NULL;
        $SC->locked_at = 0;
        $SC->save();    
        
        //$this->goBack();
        
        if(isset($ref)){
            echo '<script>alert($ref);</script>';
            return $this->redirect($ref);
        }else{
            die();
            //return $this->goHome();
        }        
        
    }    
    
    /**
     * Cancels an Update action on a section and unlock it.
     * Use this in conjunction with the navigation-away javascript.
     * 
     * @param int $id the id of the section to be unlocked
     */
    public function actionCancelA($id)
    {
        $model = $this->findModel($id);
        //unlock SB
        $SC = $model->sC;
        $SC->status = \app\models\events\EventDeclaration::INCIDENT_REPORT_DRAFT;
        $SC->locked_by = NULL;
        $SC->locked_at = 0;
        $SC->save();    
        
        die();
    }    
    
    
}
