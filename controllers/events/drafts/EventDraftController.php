<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\controllers\events\drafts;

use Yii;
use app\models\events\drafts\EventDraft;
use app\models\events\drafts\EventDraftSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\ca\IncidentCategorization;
use app\models\events\EventDeclaration;
use app\models\common\ActionMessage;

use mPDF;

/**
 * EventDraftController implements the CRUD actions for EventDraft model.
 */
class EventDraftController extends Controller
{
	public function behaviors()
	{
		return [
			'access' => [
				'class' => \yii\filters\AccessControl::className(),
				//only authenticated users
				'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				]
			],
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['post'],
				],
			],
		];
	}

	public function beforeAction($action)
	{
		if (parent::beforeAction($action)) {
			// your custom code here
			//$actionname = $action->id;
			//echo '<script>alert(\'' . $actionname . '\')</script>';
			
			return true;  // or false if needed
		} else {
			return false;
		}
	}	
	
	
	/**
	 * Lists all EventDraft models for a given Event.
	 * @return mixed
	 */
	public function actionIndex($evtID)
	{
		if (!Yii::$app->user->can('incident_draft_list', ['event' => \app\models\events\EventDeclaration::findOne(['id'=>$evtID])]))
		{
			//echo 'cannot list the drafts for this event';
			throw new NotFoundHttpException('The requested page does not exist.');
		}
		
		$searchModel = new EventDraftSearch();
		
                $params = Yii::$app->request->queryParams;
                
                $params['EventDraftSearch']['event_id']= $evtID;
                $params['EventDraftSearch']['del_status']=  EventDeclaration::STATUS_ACTIVE;
		
		$dataProvider = $searchModel->search($params);

		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
			'evtID' => $evtID,
		]);
	}

	
	/**
	 * Lists all deleted EventDraft models for a given event.
	 * @return mixed
	 */
	public function actionTrash($evtID)
	{
		if (Yii::$app->user->isGuest)
		{
			return Yii::$app->error->ThrowError(\app\components\Errors::RESTRICTED_AREA_ERROR);
		}
		if (!Yii::$app->user->can('incident_list') )
		{
			return Yii::$app->error->ThrowError(\app\components\Errors::RESTRICTED_AREA_ERROR);
		}

		$searchModel = new EventDraftSearch();
		
		$params = Yii::$app->request->queryParams;
		$params['EventDraftSearch']['event_id'] = $evtID;
		$params['EventDraftSearch']['del_status']=  EventDeclaration::STATUS_DELETED;
		
		$dataProvider = $searchModel->search($params);

		return $this->render('trash', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
			'evtID' => $evtID,
		]);
	}	
	
	/**
	 * Displays a single EventDraft model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($id)
	{
		//echo var_dump(Yii::$app->user->can('op_user')); //ok
		//echo '<br/>';
		
		if (Yii::$app->user->isGuest)
		{
			return Yii::$app->error->ThrowError(\app\components\Errors::RESTRICTED_AREA_ERROR);
		}
		
		$model = $this->findModel($id);
		
		if (!Yii::$app->user->can('incident_draft_view', ['draft' => $model]))
		{
			return Yii::$app->error->ThrowError(\app\components\Errors::RESTRICTED_AREA_ERROR);
		}
		
		return $this->render('view', [
			'model' => $model,
		]);
	}

	/**
	 * Creates a new EventDraft model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate($evtId)
	{
            $event = EventDeclaration::findOne(['id'=>$evtId]);
            
            if (!Yii::$app->user->can('incident_draft_create_operators', ['event' => $event]))
            {
                    return Yii::$app->error->ThrowError(\app\components\Errors::RESTRICTED_OPERATION_ERROR);
            }
            
		$model = new EventDraft();
		$model->event_id = $evtId;
		$uId = Yii::$app->user->getId();
		$model->is_a=0;
		$model->is_b=0;
		$model->is_c=0;
		$model->is_d=0;
		$model->is_e=0;
		$model->is_f=0;
		$model->is_g=0;
		$model->is_h=0;
		$model->is_i=0;
		$model->created_by = $uId;
		$model->status = 0;		
		
		if ($model->load(Yii::$app->request->post())) {
			$dt=new \DateTime();
			$model->created_at = time();
			$model->created_by = Yii::$app->user->id;
			$model->del_status = EventDeclaration::STATUS_ACTIVE;
			
			$model->modified_at = time();
			$model->modified_by = $uId;	//FIX 2024-05-16.005

			if ($model->save()) {
                            $description = [
                                'Draft' => $model->session_desc,
                                'by' => \Yii::$app->user->identity->full_name,
                                'at' => date('Y-m-d H:i:s', time())
                            ];
                            if (!\app\models\ReportingHistory::addHistoryItem($model->event_id, $model->id, \app\models\ReportingHistory::EVENT_DRAFT_CREATED, json_encode($description)))
                            {
                                    $ok=0;
                            }
                            return $this->redirect(['view', 'id' => $model->id]);
                        } else {
                            die ('unable to save draft');
                        }
		} else {
			return $this->render('create', [
				'model' => $model,
				'evtID' => $evtId,
			]);
		}
	}

	
	/**
	 * Updates an existing EventDraft model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($id)
	{
		$model = $this->findModel($id);

            if (!Yii::$app->user->can('incident_draft_view_operators', ['draft' => $model]))
            {
                    return Yii::$app->error->ThrowError(\app\components\Errors::RESTRICTED_OPERATION_ERROR);
            }
                
		if ($model->load(Yii::$app->request->post())) {
			$dt=new \DateTime();
			$model->modified_at = \time();
			$model->modified_by = Yii::$app->user->id;

			//unlock 
			$model->status = \app\models\events\EventDeclaration::INCIDENT_REPORT_DRAFT;
			
			$model->save();

			return $this->redirect(['view', 'id' => $model->id]);
		} else {
			//lock
			$model->status = \app\models\events\EventDeclaration::INCIDENT_REPORT_LOCKED;
			
			if (!$model->save())
			{
				throw new Exception();
			}
			
			return $this->render('update', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Deletes an existing EventDraft model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($id)
	{
		
		if (Yii::$app->user->isGuest)
		{
			return Yii::$app->error->ThrowError(\app\components\Errors::RESTRICTED_AREA_ERROR);
		}

		/*
		 * the rest of authorization checks is done AFTER setting the default $model
		 */
		$model = $this->findModel($id);

		if (!Yii::$app->user->can('incident_draft_delete', ['draft' => $model]))
		{
			return Yii::$app->error->ThrowError(\app\components\Errors::RESTRICTED_OPERATION_ERROR);
		}
		
		$model = $this->findModel($id);
		
		$model->del_status = EventDeclaration::STATUS_DELETED;
		$model->modified_by = Yii::$app->user->id;
		$model->modified_at = \time();
		
		
		if ($model->save())
		{
                        $description = [
                            'Draft' => $model->session_desc,
                            'by' => \Yii::$app->user->identity->full_name,
                            'at' => date('Y-m-d H:i:s', time())
                        ];
                        if (!\app\models\ReportingHistory::addHistoryItem($model->event_id, $model->id, \app\models\ReportingHistory::EVENT_DRAFT_DELETED, json_encode($description)))
                        {
                                $ok=0;
                        }
			Yii::$app->session->setFlash('msg', 'Draft \'' . $model->session_desc . '\' has been removed! The operation can be undone.', true);
		}
		else
		{
			Yii::$app->session->setFlash('msg', 'Draft \'' . $model->session_desc . '\' COULD NOT be removed![err]', true);
		}

		
		//return $this->redirect(['index','evtID'=>$evtId]);
		//events%2Fevent-declaration%2Fdisplay-drafts&id=5
		//return $this->redirect(['events/event-declaration/display-drafts','id'=>$evtId]);
		return $this->redirect(['index', 'evtID'=>$model->event_id]);
	}


	/**
	 * Purges (permanently deletes) an existing EventDraft model.
	 * If deletion is successful, the browser will be redirected to the 'trash' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionPurge($id)
	{
		if (Yii::$app->user->isGuest)
		{
			return Yii::$app->error->ThrowError(\app\components\Errors::RESTRICTED_AREA_ERROR);
		}

		/*
		 * the rest of authorization checks is done AFTER setting the default $model
		 */
		$model = $this->findModel($id);

		if (!Yii::$app->user->can('incident_draft_purge', ['draft' => $model]))
		{
			return Yii::$app->error->ThrowError(\app\components\Errors::RESTRICTED_OPERATION_ERROR);
		}
		
		if ($model->delete())
		{
			Yii::$app->session->setFlash('msg', 'Draft \'' . $model->session_desc . '\' has been purged (completely removed)!', true);
		}
		else
		{
			Yii::$app->session->setFlash('msg', 'Incident \'' . $model->session_desc . '\' COULD NOT be purged![err]', true);
		}

		return $this->redirect(['trash', 'evtID'=>$model->event_id]);
	}
	
   /**
	 * Reactivates an existing Draft model.
	 * The browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionReactivate($id)
	{
		if (Yii::$app->user->isGuest)
		{
			return Yii::$app->error->ThrowError(\app\components\Errors::RESTRICTED_AREA_ERROR);
		}

		/*
		 * the rest of authorization checks is done AFTER setting the default $model
		 */
		$model = $this->findModel($id);

		if (!Yii::$app->user->can('incident_draft_delete', ['draft' => $model]))
		{
			return Yii::$app->error->ThrowError(\app\components\Errors::RESTRICTED_OPERATION_ERROR);
		}
		
		$model = $this->findModel($id);
		
		$model->del_status = EventDeclaration::STATUS_ACTIVE;
		$model->modified_by = Yii::$app->user->id;
		$model->modified_at = \time();

		if ($model->save())
		{
                        $description = [
                            'Draft' => $model->session_desc,
                            'by' => \Yii::$app->user->identity->full_name,
                            'at' => date('Y-m-d H:i:s', time())
                        ];
                        if (!\app\models\ReportingHistory::addHistoryItem($model->event_id, $model->id, \app\models\ReportingHistory::EVENT_DRAFT_RESTORED, json_encode($description)))
                        {
                                $ok=0;
                        }
                    
			Yii::$app->session->setFlash('msg', 'Draft \'' . $model->session_desc . '\' has been reactivated!', true);
		}
		else
		{
			Yii::$app->session->setFlash('msg', 'Draft \'' . $model->session_desc . '\' COULD NOT be reactivated!', true);
		}

		
		return $this->redirect(['index', 'evtID'=>$model->event_id]);
		
	}	
	
	/**
	 * Displays the current draft Section A
	 * @param integer $id
	 * @param integer $v 
	 * @return mixed
	 */	
	public function actionViewSectionA($id, $v)
	{
		$model = $this->findModel($id);
		
            if (!Yii::$app->user->can('incident_draft_view_operators', ['draft' => $model]))
            {
                
                    return Yii::$app->error->ThrowError(\app\components\Errors::RESTRICTED_OPERATION_ERROR);
            }
            
		return $this->render('section-a-view', [
		   'model' => $model,
		   'v' => $v, 
		]);
	}
	
	/**
	 * Displays the current draft Section B
	 * @param integer $id the id of the CurrentDraft
	 * @param integer $v subsection index
	 * @return mixed
	 */	
	public function actionViewSectionB($id, $v)
	{
		$model = $this->findModel($id);
		
            if (!Yii::$app->user->can('incident_draft_view_operators', ['draft' => $model]))
            {
                    return Yii::$app->error->ThrowError(\app\components\Errors::RESTRICTED_OPERATION_ERROR);
            }
            
		return $this->render('section-b-view', [
		   'model' => $model,
		   'v' => $v, 
		]);
	}
	
	/**
	 * Displays the current draft Section C
	 * @param integer $id the id of the CurrentDraft
	 * @param integer $v subsection index
	 * @return mixed
	 */	
	public function actionViewSectionC($id, $v)
	{
		$model = $this->findModel($id);
		
            if (!Yii::$app->user->can('incident_draft_view_operators', ['draft' => $model]))
            {
                    return Yii::$app->error->ThrowError(\app\components\Errors::RESTRICTED_OPERATION_ERROR);
            }
            
		return $this->render('section-c-view', [
		   'model' => $model,
		   'v' => $v, 
		]);
	}

	/**
	 * Displays the current draft Section D
	 * @param integer $id the id of the CurrentDraft
	 * @param integer $v subsection index
	 * @return mixed
	 */	
	public function actionViewSectionD($id, $v)
	{
		$model = $this->findModel($id);
		
            if (!Yii::$app->user->can('incident_draft_view_operators', ['draft' => $model]))
            {
                    return Yii::$app->error->ThrowError(\app\components\Errors::RESTRICTED_OPERATION_ERROR);
            }
            
		return $this->render('section-d-view', [
		   'model' => $model,
		   'v' => $v, 
		]);
	}
	
	/**
	 * Displays the current draft Section E
	 * @param integer $id the id of the CurrentDraft
	 * @param integer $v subsection index
	 * @return mixed
	 */	
	public function actionViewSectionE($id, $v)
	{
		$model = $this->findModel($id);
		
            if (!Yii::$app->user->can('incident_draft_view_operators', ['draft' => $model]))
            {
                    return Yii::$app->error->ThrowError(\app\components\Errors::RESTRICTED_OPERATION_ERROR);
            }
                
		return $this->render('section-e-view', [
		   'model' => $model,
		   'v' => $v, 
		]);
	}

	/**
	 * Displays the current draft Section F
	 * @param integer $id the id of the CurrentDraft
	 * @param integer $v subsection index
	 * @return mixed
	 */	
	public function actionViewSectionF($id, $v)
	{
		$model = $this->findModel($id);
		
            if (!Yii::$app->user->can('incident_draft_view_operators', ['draft' => $model]))
            {
                    return Yii::$app->error->ThrowError(\app\components\Errors::RESTRICTED_OPERATION_ERROR);
            }
            
		return $this->render('section-f-view', [
		   'model' => $model,
		   'v' => $v, 
		]);
	}

	//Author : cavesje  Date : 20150722 - Error click on button GOTO SECTION G
	/**
	 * Displays the current draft Section G
	 * @param integer $id the id of the CurrentDraft
	 * @param integer $v subsection index
	 * @return mixed
	 */	
	public function actionViewSectionG($id, $v)
	{
		$model = $this->findModel($id);
		
            if (!Yii::$app->user->can('incident_draft_view_operators', ['draft' => $model]))
            {
                    return Yii::$app->error->ThrowError(\app\components\Errors::RESTRICTED_OPERATION_ERROR);
            }
                
		return $this->render('section-g-view', [
		   'model' => $model,
		   'v' => $v, 
		]);
	}
	
	//Author : cavesje  Date : 20150722 - Error click on button GOTO SECTION H
	/**
	 * Displays the current draft Section G
	 * @param integer $id the id of the CurrentDraft
	 * @param integer $v subsection index
	 * @return mixed
	 */	
	public function actionViewSectionH($id, $v)
	{
		$model = $this->findModel($id);
		
            if (!Yii::$app->user->can('incident_draft_view_operators', ['draft' => $model]))
            {
                    return Yii::$app->error->ThrowError(\app\components\Errors::RESTRICTED_OPERATION_ERROR);
            }
                
		return $this->render('section-h-view', [
		   'model' => $model,
		   'v' => $v, 
		]);
	}
	
	/**
	 * Displays the current draft Section I
	 * @param integer $id the id of the CurrentDraft
	 * @param integer $v subsection index
	 * @return mixed
	 */	
	public function actionViewSectionI($id, $v)
	{
		$model = $this->findModel($id);
		
            if (!Yii::$app->user->can('incident_draft_view_operators', ['draft' => $model]))
            {
                    return Yii::$app->error->ThrowError(\app\components\Errors::RESTRICTED_OPERATION_ERROR);
            }
                
		return $this->render('section-i-view', [
		   'model' => $model,
		   'v' => $v, 
		]);
	}

	/**
	 * Displays the current draft Section J
	 * @param integer $id the id of the CurrentDraft
	 * @param integer $v subsection index
	 * @return mixed
	 */	
	public function actionViewSectionJ($id, $v)
	{
		$model = $this->findModel($id);
		
            if (!Yii::$app->user->can('incident_draft_view_operators', ['draft' => $model]))
            {
                    return Yii::$app->error->ThrowError(\app\components\Errors::RESTRICTED_OPERATION_ERROR);
            }
                
		return $this->render('section-j-view', [
		   'model' => $model,
		   'v' => $v, 
		]);
	}
	
	/**
	 * Returns the id of the current registered user
	 */
	protected function getUserId()
	{
		$users = Yii::$app->user->getId();
	}
	
	
	public function actionFinalize($id)
	{
		$model=$this->findModel($id);

		if (\Yii::$app->user->can('incident_draft_view', ['draft' => $model]))
		{

			$section_letters = ['a', 'b', 'c', 'd', 'e', 'f', 'i', 'j'];
			$section_numbers = ['1', '2', '3', '4'];

			$model=$this->findModel($id);
			
			foreach($section_letters as $cSectionLetter)
			{
				$is_section_attr = 'is_' . $cSectionLetter;
				if ($model->$is_section_attr) {
					$section_name = 'section' . strtoupper($cSectionLetter);	//this gives e.g. sectionC
					$section = $model->$section_name; //->sectionC;

					$section_has_errors = 0; //finalize section if no errors in subsections
					for ($i=1; $i<5; $i++) {
						$subsection_property_name = $cSectionLetter . $i;   //this gives e.g c1
						$subsection = $section->$subsection_property_name;

						$subsection->setScenario('finalize');
						$subsection->validate();

						if ($subsection->hasErrors())
						{
                                                    $evt = strtoupper($cSectionLetter . '.' . $i . '.');
                                                    $err_text = \Yii::t('app/messages/errors', 'Section {evt} failed validation. Please revise.', ['evt'=>$evt]);
							$model->addError($subsection_property_name, $err_text);
							foreach ($subsection->getErrors() as $key=>$message) {
								$attr_name =  $subsection->attributeLabels()[$key];
								$model->addError($subsection_property_name, $message);
							}
							$section_has_errors = 1;
						}
					}
					
					/*
					 * mark section as finalized if no errors
					 */
					if ($section_has_errors == 0)
					{
						$section->status = \app\models\events\EventDeclaration::INCIDENT_REPORT_FINALIZED;
						$section->save();
					}					
				}				
			}   //end section by section validation

			if (!$model->hasErrors())
			{
                                $description = [
                                    'by' => \Yii::$app->user->identity->full_name,
                                    'at' => date('Y-m-d H:i:s', time())
                                ];
                                if (!\app\models\ReportingHistory::addHistoryItem($model->event_id, $model->id, \app\models\ReportingHistory::EVENT_FINALIZED, json_encode($description)))
                                {
                                        $ok=0;
                                }
                            
				$model->status = \app\models\events\EventDeclaration::INCIDENT_REPORT_FINALIZED;
				$model->save();
			}

			return $this->render('review', [
				'model' => $model,
				'display_status' => true,
				'task' => 'finalize'
			]);				 
			
			
			//$model->RefreshFinalizeStatus();
/*
				echo '<pre>';
				echo var_dump($model);
				echo '</pre>';
				die();
*/		  
			
			
			
			
			
			
			$model = $this->findModel($id);
			$model->status = \app\models\events\EventDeclaration::INCIDENT_REPORT_FINALIZED;
			$dt = new \DateTime();
			$model->modified_at = \time();

			if ($model->save())
			{
				$msg = 'Incident Report Draft \'' . $model->session_desc . '\ has been marked as finalized.\nYou may now proceed with signing the event.';
			}
			else
			{
				$msg = 'UNABLE TO FINALIZE the Incident Report Draft for \'' . $model->event_name . '\.\nPlease try again later.' . '[err]';
			}

			\Yii::$app->session->setFlash('msg', $msg, TRUE);

			//http://localhost:8080/SyRIO_DEV/web/index.php?r=events%2Fevent-declaration%2Fdisplay-drafts&id=12
			$retPath = Yii::$app->getUrlManager()->createUrl(['events/event-declaration/display-drafts', 'id'=>$model->event_id]);
			return $this->redirect($retPath);
		}
		else {
			echo 'not allowed';
		}
	}
	
	public function actionReopen($id)
	{
		/*
		 * As authorization logic:
		 * WHOEVER SIGNS THE DRAFT CAN RE-OPEN IT
		 * 
		 */
		$model = $this->findModel($id);
		
		if (!\Yii::$app->user->can('incident_draft_reopen', ['draft'=>$model]))
		{
			return Yii::$app->error->ThrowError(\app\components\Errors::RESTRICTED_OPERATION_ERROR);
		}
		
			/* transaction session vamanbo 20150909 */
			$transaction = \Yii::$app->db->beginTransaction();
			$ok=1;
			
		$letters = ['a', 'b', 'c', 'd', 'e', 'f', 'i', 'j'];

		foreach ($letters as $small_letter)
		{
			$is_var = 'is_' . $small_letter;						//is_a
			$section_var = 'section' . strtoupper($small_letter);   //sectionA
			if ($model->$is_var)
			{
				$model->$section_var->status=\app\models\events\EventDeclaration::INCIDENT_REPORT_DRAFT;
				if (!$model->$section_var->save()) {$ok = 0;}
			}
		}
			
			$model->status = \app\models\events\EventDeclaration::INCIDENT_REPORT_DRAFT;
			$dt = new \DateTime();
			$model->modified_at = \time();
			$model->modified_by = \Yii::$app->user->id;
			
			if (!$model->save()) {
				$ok = 0;
			}
			else
			{
                                $description = [
                                    'by' => \Yii::$app->user->identity->full_name,
                                    'at' => date('Y-m-d H:i:s', $model->modified_at)
                                ];
                            
				//log the event history
				if (!\app\models\ReportingHistory::addHistoryItem($model->event_id, $model->id, \app\models\ReportingHistory::EVENT_REOPENED, json_encode($description)))
				{
					$ok=0;
				}
			}

			if ($ok == 1)
			{
				$msg = Yii::t('app', 'Incident Report Draft \'{evt}\' has been RE-OPENED. You may now proceed with modifying it.', [
                                    'evt' => $model->session_desc
                                ]);
                            
				$transaction->commit();
			}
			else
			{
				$msg = \app\components\helpers\TGenericMessage::Error(false);
				$transaction->rollBack();
			}

			\Yii::$app->session->setFlash('msg', $msg, TRUE);

			//http://localhost:8080/SyRIO_DEV/web/index.php?r=events%2Fevent-declaration%2Fdisplay-drafts&id=12
			return $this->redirect(['index', 'evtID' => $model->event_id]);
	}
	

        
	public function actionReopenSection($id, $section)
	{
		/*
		 * As authorization logic:
		 * WHOEVER SIGNS THE DRAFT CAN RE-OPEN IT
		 * 
		 */
            
		$model = $this->findModel($id);
		$is_var = 'is_'.strtolower($section);
                $section_var = 'section' . strtoupper($section);   //sectionA
                
                if (!\Yii::$app->user->can('incident_draft_section_reopen_operators', ['section' => $model->$section_var]))
		{
			return Yii::$app->error->ThrowError(\app\components\Errors::RESTRICTED_OPERATION_ERROR);
		}
		
			/* transaction session vamanbo 20150909 */
			$transaction = \Yii::$app->db->beginTransaction();
			$ok=1;
			
                if ($model->$is_var)
                {
                        $model->$section_var->status=\app\models\events\EventDeclaration::INCIDENT_REPORT_DRAFT;
                        if (!$model->$section_var->save()) {$ok = 0;}

			$model->status = \app\models\events\EventDeclaration::INCIDENT_REPORT_DRAFT;
			$dt = new \DateTime();
			$model->modified_at = \time();
			$model->modified_by = \Yii::$app->user->id;
			
                        $description = [
                            'Draft' => $model->session_desc,
                            'Section' => strtoupper($section),
                            'by' => \Yii::$app->user->identity->full_name,
                            'at' => date('Y-m-d H:i:s', $model->modified_at)
                        ];
                        
			if (!$model->save()) {
				$ok = 0;
			}
			elseif (!\app\models\ReportingHistory::addHistoryItem($model->event_id, $model->id, \app\models\ReportingHistory::EVENT_SECTION_REOPENED, json_encode($description)))
			{
                            //log the event history
                            $ok=0;
                            $transaction->rollBack();
			}

			if ($ok == 1)
			{
				$msg = Yii::t('app', 'Section {evt1} of \'{evt2}\' has been RE-OPENED. You may now proceed with modifying it.', [
                                    'evt1' => strtoupper($section),
                                    'evt2' => $model->session_desc,
                                ]);
    				$transaction->commit();
			}
			else
			{
				$msg = \app\components\helpers\TGenericMessage::Error(false);
				$transaction->rollBack();
			}

			\Yii::$app->session->setFlash('msg', $msg, TRUE);

			//http://localhost:8080/SyRIO_DEV/web/index.php?r=events%2Fevent-declaration%2Fdisplay-drafts&id=12
			return $this->redirect(['index', 'evtID' => $model->event_id]);                        
                        
                } else {
                    throw new \yii\base\InvalidCallException(\Yii::t('app', 'Cannot re-open section'));
                }
                
	}
        
        
        
	public function actionValidate($id)
	{
		$model = $this->findModel($id);
		$model->sectionA->a2->setScenario('finalize');
		$model->sectionA->a3->setScenario('finalize');
		
		
		
		$toValidate = [$model->sectionA->a1, $model->sectionA->a2, $model->sectionA->a3];
		$myErrors = [];
		
		if ($model->validateMultiple($toValidate))
			{
				return 'valid';
			}
			else
			{
				echo 'not validated multiple';
				foreach($toValidate as $validatedModel)
				{
					if($validatedModel->hasErrors())
					{
						$myErrors[$validatedModel->className()] = $validatedModel->errors;
					}
				}
				echo '<pre>';

				var_dump($myErrors);

				echo '</pre>';				   
			}
		;
		
//		if ($model->sectionA->a2->validate())
//		{
//			return 'valid';
//		}
//		else
//		{
//			echo 'not validated';
//			echo '<pre>';
//			
//			var_dump($model->sectionA->a2->errors);
//			
//			echo '</pre>';
//		}
	}
	
	
	public function actionReview($id)
	{
		if (Yii::$app->user->isGuest)
		{
			return Yii::$app->error->ThrowError(\app\components\Errors::RESTRICTED_AREA_ERROR);
		}
		
		$model = $this->findModel($id);
		
		if (!Yii::$app->user->can('incident_draft_view', ['draft' => $model]))
		{
			return Yii::$app->error->ThrowError(\app\components\Errors::RESTRICTED_AREA_ERROR);
		}

		return $this->render('review', [
			'model' => $model,
		]);		
	}	
	
	
	public function actionFinalizeSection($id, $s)
	{
		$section_letters = ['a', 'b', 'c', 'd', 'e', 'f', 'i', 'j'];
		$section_numbers = ['1', '2', '3', '4'];
		
		$model=$this->findModel($id);
		switch ($s)
		{
			case 'A':
				$cSectionLetter = 'a';
				
				$section_name = 'section' . strtoupper($cSectionLetter);	//this gives e.g. sectionC
				$section = $model->$section_name; //->sectionC;

				for ($i=1; $i<5; $i++) {
					$subsection_property_name = $cSectionLetter . $i;   //this gives e.g c1
					$subsection = $section->$subsection_property_name;
					
					$subsection->setScenario('finalize');
					$subsection->validate();

					if ($subsection->hasErrors())
					{
                                            $evt = strtoupper($cSectionLetter . '.' . $i . '.');
                                            $err_text = \Yii::t('app/messages/errors', 'Section {evt} failed validation. Please revise.', ['evt'=>$evt]);
                                                $model->addError($subsection_property_name, $err_text);
                                            
						foreach ($subsection->getErrors() as $key=>$message) {
							$attr_name =  $subsection->attributeLabels()[$key];
							$model->addError($subsection_property_name, $message);
						}
					}
				}
				
				break;
			case 'B':
				$cSectionLetter = 'b';
				
				$section_name = 'section' . strtoupper($cSectionLetter);	//this gives e.g. sectionC
				$section = $model->$section_name; //->sectionC;

				for ($i=1; $i<5; $i++) {
					$subsection_property_name = $cSectionLetter . $i;   //this gives e.g c1
					$subsection = $section->$subsection_property_name;
					
					$subsection->setScenario('finalize');
					$subsection->validate();

					if ($subsection->hasErrors())
					{
                                                $evt = strtoupper($cSectionLetter . '.' . $i . '.');
                                                $err_text = \Yii::t('app/messages/errors', 'Section {evt} failed validation. Please revise.', ['evt'=>$evt]);
                                                    $model->addError($subsection_property_name, $err_text);
                                            
						foreach ($subsection->getErrors() as $key=>$message) {
							$attr_name =  $subsection->attributeLabels()[$key];
							$model->addError($subsection_property_name, $message);
						}
					}
				}
				
				break;
			case 'C':
				$cSectionLetter = 'c';
				
				$section_name = 'section' . strtoupper('c');	//this gives e.g. sectionC
				$section = $model->$section_name; //->sectionC;

				for ($i=1; $i<5; $i++) {
					$subsection_property_name = $cSectionLetter . $i;   //this gives e.g c1
					$subsection = $section->$subsection_property_name;
					
					$subsection->setScenario('finalize');
					$subsection->validate();

					if ($subsection->hasErrors())
					{
                                                $evt = strtoupper($cSectionLetter . '.' . $i . '.');
                                                $err_text = \Yii::t('app/messages/errors', 'Section {evt} failed validation. Please revise.', ['evt'=>$evt]);
                                                    $model->addError($subsection_property_name, $err_text);
                                                    
						foreach ($subsection->getErrors() as $key=>$message) {
							$attr_name =  $subsection->attributeLabels()[$key];
							$model->addError($subsection_property_name, $message);
						}						
					}
				}
				
				break;
			case 'D':
				$cSectionLetter = 'd';
				
				$section_name = 'section' . strtoupper('d');	//this gives e.g. sectionC
				$section = $model->$section_name; //->sectionC;

				for ($i=1; $i<5; $i++) {
					$subsection_property_name = $cSectionLetter . $i;   //this gives e.g c1
					$subsection = $section->$subsection_property_name;
					
					$subsection->setScenario('finalize');
					$subsection->validate();

					if ($subsection->hasErrors())
					{
                                                $evt = strtoupper($cSectionLetter . '.' . $i . '.');
                                                $err_text = \Yii::t('app/messages/errors', 'Section {evt} failed validation. Please revise.', ['evt'=>$evt]);
                                                    $model->addError($subsection_property_name, $err_text);
						foreach ($subsection->getErrors() as $key=>$message) {
							$attr_name =  $subsection->attributeLabels()[$key];
							$model->addError($subsection_property_name, $message);
						}						
					}
				}
				
				break;
			case 'E':
				$cSectionLetter = 'e';
				
				$section_name = 'section' . strtoupper('e');	//this gives e.g. sectionC
				$section = $model->$section_name; //->sectionC;

				for ($i=1; $i<5; $i++) {
					$subsection_property_name = $cSectionLetter . $i;   //this gives e.g c1
					$subsection = $section->$subsection_property_name;
					
					$subsection->setScenario('finalize');
					$subsection->validate();

					if ($subsection->hasErrors())
					{
                                                $evt = strtoupper($cSectionLetter . '.' . $i . '.');
                                                $err_text = \Yii::t('app/messages/errors', 'Section {evt} failed validation. Please revise.', ['evt'=>$evt]);
                                                    $model->addError($subsection_property_name, $err_text);
						foreach ($subsection->getErrors() as $key=>$message) {
							$attr_name =  $subsection->attributeLabels()[$key];
							$model->addError($subsection_property_name, $message);
						}						
					}
				}
				
				break;
			case 'F':
				$cSectionLetter = 'f';
				
				$section_name = 'section' . strtoupper('f');	//this gives e.g. sectionC
				$section = $model->$section_name; //->sectionC;

				for ($i=1; $i<5; $i++) {
					$subsection_property_name = $cSectionLetter . $i;   //this gives e.g c1
					$subsection = $section->$subsection_property_name;
					
					$subsection->setScenario('finalize');
					$subsection->validate();

					if ($subsection->hasErrors())
					{
                                                $evt = strtoupper($cSectionLetter . '.' . $i . '.');
                                                $err_text = \Yii::t('app/messages/errors', 'Section {evt} failed validation. Please revise.', ['evt'=>$evt]);
                                                    $model->addError($subsection_property_name, $err_text);
						foreach ($subsection->getErrors() as $key=>$message) {
							$attr_name =  $subsection->attributeLabels()[$key];
							$model->addError($subsection_property_name, $message);
						}						
					}
				}
				
				break;
			case 'I':
				$cSectionLetter = 'i';
				
				$section_name = 'section' . strtoupper('i');	//this gives e.g. sectionC
				$section = $model->$section_name; //->sectionC;

				for ($i=1; $i<5; $i++) {
					$subsection_property_name = $cSectionLetter . $i;   //this gives e.g c1
					$subsection = $section->$subsection_property_name;
					
					$subsection->setScenario('finalize');
					$subsection->validate();

					if ($subsection->hasErrors())
					{
                                                $evt = strtoupper($cSectionLetter . '.' . $i . '.');
                                                $err_text = \Yii::t('app/messages/errors', 'Section {evt} failed validation. Please revise.', ['evt'=>$evt]);
                                                    $model->addError($subsection_property_name, $err_text);
						foreach ($subsection->getErrors() as $key=>$message) {
							$attr_name =  $subsection->attributeLabels()[$key];
							$model->addError($subsection_property_name, $message);
						}						
					}
				}
				
				break;				
			case 'J':
				$cSectionLetter = 'j';
				
				$section_name = 'section' . strtoupper('J');	//this gives e.g. sectionC
				$section = $model->$section_name; //->sectionC;

				for ($i=1; $i<5; $i++) {
					$subsection_property_name = $cSectionLetter . $i;   //this gives e.g c1
					$subsection = $section->$subsection_property_name;
					
					$subsection->setScenario('finalize');
					$subsection->validate();

					if ($subsection->hasErrors())
					{
                                                $evt = strtoupper($cSectionLetter . '.' . $i . '.');
                                                $err_text = \Yii::t('app/messages/errors', 'Section {evt} failed validation. Please revise.', ['evt'=>$evt]);
                                                    $model->addError($subsection_property_name, $err_text);
						foreach ($subsection->getErrors() as $key=>$message) {
							$attr_name =  $subsection->attributeLabels()[$key];
							$model->addError($subsection_property_name, $message);
						}						
					}
				}
				
				break;				
				
		}   //end switch case
                
                if (!$model->hasErrors())
                {
                        $section->status = \app\models\events\EventDeclaration::INCIDENT_REPORT_FINALIZED;
                        $section->save();
                        $model->RefreshFinalizeStatus();

                        $description = [
                            'Draft' => $model->session_desc,
                            'Section' => strtoupper($s),
                            'by' => \Yii::$app->user->identity->full_name,
                            'at' => date('Y-m-d H:i:s', time())
                        ];

                        if (!\app\models\ReportingHistory::addHistoryItem($model->event_id, $model->id, \app\models\ReportingHistory::EVENT_SECTION_FINALIZED, json_encode($description)))
                        {
                                $ok=0;
                        }
                        
                        
                        $msg = \Yii::t('app/crf', 'Section {evt} has been successfully \'finalized\'.', ['evt'=>  strtoupper($cSectionLetter)]);

                        if ($model->status==\app\models\events\EventDeclaration::INCIDENT_REPORT_FINALIZED)
                        {
                            
                                $description = [
                                    'by' => \Yii::$app->user->identity->full_name,
                                    'at' => date('Y-m-d H:i:s', time())
                                ];
                                if (!\app\models\ReportingHistory::addHistoryItem(
                                        $model->event_id, 
                                        $model->id, 
                                        \app\models\ReportingHistory::EVENT_FINALIZED, 
                                        json_encode($description), 
                                        1));
                                {
                                        $ok=0;
                                }
                            
                            
                                if (\Yii::$app->user->can('op_raporteur'))
                                {
                                    $msg .= \Yii::t('app/crf', 'The Incident Report \'{evt}\' is ready to be signed.', ['evt'=>$model->event->event_name]);
                                }
                                else
                                {
                                    $msg .= \Yii::t('app/crf', 'The Incident Report \'{evt}\' is ready to be signed.', ['evt'=>$model->event->event_name]);
                                }
                        }
                        else
                        {
                                $msg .= \Yii::t('app/crf', 'You may proceed with drafting the remaining sections of the Incident Report');
                        }
                        \Yii::$app->session->setFlash('msg', $msg, false);
                }

                $view_name = 'section-'.strtolower($cSectionLetter).'-view';
                return $this->render($view_name, [
                   'model' => $model,
                   'v' => 0, 
                ]);
                
		
	}
	
	
	public function actionViewAll($id)
	{
		if (Yii::$app->user->isGuest)
		{
			return Yii::$app->error->ThrowError(\app\components\Errors::RESTRICTED_AREA_ERROR);
		}
		elseif (!Yii::$app->user->can('incident_draft_view'))
		{
			return Yii::$app->error->ThrowError(\app\components\Errors::RESTRICTED_AREA_ERROR);
		}
		return $this->render('view_all', [
			$this->findModel($id) => $model,
		]);
	}
	
	
	public function actionSign($id)
	{
		$model = $this->findModel($id);
		
		if (!\Yii::$app->user->can('incident_draft_sign', ['draft'=>$model]))
		{
			return Yii::$app->error->ThrowError(\app\components\Errors::RESTRICTED_OPERATION_ERROR);
		}

		$letters = ['a', 'b', 'c', 'd', 'e', 'f', 'i', 'j'];

		foreach ($letters as $small_letter)
		{
			$is_var = 'is_' . $small_letter;						//is_a
			$section_var = 'section' . strtoupper($small_letter);   //sectionA
			if ($model->$is_var && $model->$section_var->status < \app\models\events\EventDeclaration::INCIDENT_REPORT_FINALIZED)
			{
				$url = \Yii::$app->getUrlManager()->createUrl(['/events/drafts/event-draft/view-section-' . $small_letter, 'id'=>$model->id, 'v'=>1]);
				$model->addError('Section ' . strtoupper($small_letter), 'Section ' . strtoupper($small_letter) . ' is not finalized. Click <a href="' . $url . '" class="alert-link">here</a> to finalize!');
			}
		}
		
		if (!$model->hasErrors())
		{
			/* transaction session vamanbo 20150909 */
			$transaction = \Yii::$app->db->beginTransaction();
			$ok=1;
			
			foreach ($letters as $small_letter)
			{
				$is_var = 'is_' . $small_letter;						//is_a
				$section_var = 'section' . strtoupper($small_letter);   //sectionA
				
				if ($model->$is_var) {
					$model->$section_var->status = \app\models\events\EventDeclaration::INCIDENT_REPORT_SIGNED;
					if (!$model->$section_var->save()) { $ok = 0; }
				}
			}
			
                        $model->modified_at = \time();
                        $model->modified_by = \Yii::$app->user->id;
			$model->status =  \app\models\events\EventDeclaration::INCIDENT_REPORT_SIGNED;
			if (!$model->save()) {
                            $ok = 0; 
                        } 
			
			//log the event history
                        $description = [
                            'by' => \Yii::$app->user->identity->full_name,
                            'at' => date('Y-m-d H:i:s', $model->modified_at)
                        ];
                        
			if (!\app\models\ReportingHistory::addHistoryItem($model->event_id, $model->id, \app\models\ReportingHistory::EVENT_SIGNED, json_encode($description)))
			{
                            $ok=0;
                            die ('unable to save history');
			}
			
			$ok == 1 ? $transaction->commit() : $transaction->rollBack();
		}
		
		//\Yii::$app->session->setFlash('msg', 'signed', true);
                $actionMessage = new ActionMessage();
                $actionMessage->SetSuccess(
                        Yii::t('app', 'Report draft signed'), 
                        Yii::t('app', 'The incident report draft \'{evt}\' has been signed.', ['evt'=>$model->session_desc])
                        . '<br/>'  .
                        Yii::t('app', 'You may use this draft for reporting the event (\'{evt}\') to the CA (by choosing Submit, unless \'{evt}\' has not been previously reported).', [
                            'evt'=>$model->event->event_name
                        ]));
                Yii::$app->session->set('finished_action_result', $actionMessage);
		
                
                if (isset(Yii::$app->request->referrer)) {
                    return $this->redirect(Yii::$app->request->referrer);
                }
                
		return $this->render('review', [
			 'model' => $model,
		]); 
		
	}
	
	public function actionSubmitConfirmation($id)
	{
		$model=$this->findModel($id);

		if (!Yii::$app->user->can('incident_submit_operators', ['draft'=>$model]))
		{
			return \Yii::$app->error->ThrowError(\app\components\Errors::RESTRICTED_OPERATION_ERROR);
		}
		return $this->render('confirm', [
			   'model' => $model,
			]);
	}
	
	public function actionSubmitIncident($id)
	{
		$model=$this->findModel($id);
		if (!Yii::$app->user->can('incident_submit_operators', ['draft'=>$model]))
		{
			return \Yii::$app->error->ThrowError(\app\components\Errors::RESTRICTED_OPERATION_ERROR);
		}
		
		$actionMessage = new \app\models\common\ActionMessage();
		if ($model->status==\app\models\events\EventDeclaration::INCIDENT_REPORT_SIGNED)
		{

			$transaction = \Yii::$app->db->beginTransaction();
			
			try
			{
				$model->ca_status = \app\models\events\EventDeclaration::INCIDENT_REPORT_REPORTED;

                                //vamanbo 20160413
                                //update the details on the person who is reporting the event to match the current user
                                
                                $cUser = \Yii::$app->user->identity;
                                $model->event->raporteur_name = $cUser->full_name;
                                $model->event->raporteur_role = $cUser->role_in_organization;
                                $model->event->contact_tel = $cUser->phone;
                                $model->event->contact_email = $cUser->email;
                                $model->event->raporteur_id = \Yii::$app->user->id;
                                
                                //end vamanbo 20160413
                                
                                if (isset($model->event->status) && $model->event->status == EventDeclaration::INCIDENT_REPORT_REJECTED) {
                                    $resubmitted = 1;
                                }
                                $model->event->status = \app\models\events\EventDeclaration::INCIDENT_REPORT_REPORTED;
				
                                $model->modified_at = \time();
                                $model->modified_by = \Yii::$app->user->id;
				
				$flag1 = $model->save();
                                
                                if (!$flag1) {
                                    echo var_dump($model->errors);
                                }
                                
                                
				$flag2 = $model->event->save();
				$incident = IncidentCategorization::findOne(['draft_id' => $model->id]);
				if (!isset($incident)) {
					$incident = new IncidentCategorization();
					$incident->draft_id=$model->id;
				}

				$dt = new \DateTime();
                                $dt = date_timestamp_set($dt, $model->modified_at);
				
                                $incident->op_submitted_at = date_format($dt, 'Y-m-d H:i:s');
				$incident->is_done = 0;
				
				$flag3 = $incident->save();
				
				$ok = 1;
				
				//log the event history
                                $description = [
                                    'by' => \Yii::$app->user->identity->full_name,
                                    'at' => date('Y-m-d H:i:s', $model->modified_at)
                                ];
                                
				if (!\app\models\ReportingHistory::addHistoryItem($model->event_id, $model->id, \app\models\ReportingHistory::EVENT_SUBMITTED, json_encode($description)))
				{
					$ok=0;
				}
				
				$flag = $flag1 && $flag2 && $flag3 && $ok;
				
				//echo '<pre>';
                                //echo var_dump($model);
                                //echo '</pre>';
                                
                                //echo $model->modified_at . '<br/>';
                                //echo date('Y-m-d H:i:s', $model->modified_at) . '<br/>';
                                //echo $incident->op_submitted_at;
                                //die();
				if ($flag)
				{
					$transaction->commit();
                                        $s = \Yii::t('app', '{evt} HAS BEEN SUBMITTED TO THE COMPETENT AUTHORITY.', ['evt'=>'\'' . $model->event->event_name . '/' . $model->session_desc . '\'']);
					
                                        //vamanbo 20160225
                                        //send email to all ca_assessors
                                        //check if event is resubmitted and send the emails accordingly
                                        
                                        isset($resubmitted) ? \app\models\email\EmailDispacher::sendOoReSubmitsReport($incident) : \app\models\email\EmailDispacher::sendOoSubmitsReport($incident);
                                        //die('eventdraft');
                                        
					$actionMessage->SetSuccess(\Yii::t('app', 'Submit Incident Report'), $s);
					\Yii::$app->session->set('finished_action_result', $actionMessage);					
					return $this->redirect(['index', 'evtID' => $model->event->id]);
				}
				else
				{
					$transaction->rollBack();

					$s='\'' . $model->event->event_name . 'ERROR: /' . $model->session_desc . '\' COULD NOT BE SUBMITTED TO THE COMPETENT AUTHORITY.';
					
					$actionMessage->SetError(\Yii::t('app', 'Submit incident report'), Yii::t('app', $s));
					\Yii::$app->session->set('finished_action_result', $actionMessage);					
					return $this->redirect(['index', 'evtID' => $model->event->id]);
				}
				
			} catch (Exception $ex) {
				$transaction->rollBack();
				echo 'error when submitting';
				die();
			}		
		}
		else
		{
			$s = '/' . $model->session_desc . '\' must be signed before submission.';
			$actionMessage->SetWarning(\Yii::t('app', 'Submit incident report'), Yii::t('app', $s));
			\Yii::$app->session->set('finished_action_result', $actionMessage);					
					return $this->redirect(['index', 'evtID' => $model->event->id]);
		}
	}
	
	public function actionCancelSubmit($id)
	{
		$model=$this->findModel($id);
                
		Yii::$app->session->setFlash('msg', 'Submission of \'' . $model->event->event_name . '/' . $model->session_desc . '\' canceled.', true);
		
		//http://localhost:8080/SyRIO_DEV/web/index.php?r=events%2Fevent-declaration%2Fdisplay-drafts&id=17
			$retPath = Yii::$app->getUrlManager()->createUrl(['events/event-declaration/display-drafts', 'id'=>$model->event_id]);
			return $this->redirect($retPath);

	}	
	
	
	/**
	 * Forces the unlocking of all the draft sections
	 * 
	 * @param intgeger $id The id of the current draft
	 */
	public function actionUnlockAll($id)
	{
		if (!(Yii::$app->user->can('op_raporteur') || Yii::$app->user->can('inst_rapporteur')))
		{
			return 'you are not allowed to force unlocking sections.';
		}
		
		if (\Yii::$app->request->isPost)
		{
			/**
			 * @param app\models\events\drafts\EventDraft $model The current draft
			 */
			$model=$this->findModel($id);

			$model->ForceUnlockSections();
                        $model->status = EventDeclaration::INCIDENT_REPORT_DRAFT;
                        $model->save();
                        
			//refresh the view
			return $this->redirect(['index', 'evtID'=>$model->event_id]);
			//return $this->refresh();
		}
		
	}
	
	
	
	/**
	 * Returns the parent event of the current draft
	 */
//	protected function getEvent()
//	{
//		$events = new EventDeclaration();
//	}

	/**
	 * Finds the EventDraft model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return EventDraft the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		if (($model = EventDraft::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
	
	//(start) Author : cavesje  Date : 20150617 - Add new button for GeneratePDF
	public function actionPdf($id)
	{
	  $model = $this->findModel($id);
	  return $this->redirect(
		[
		  'pdfgenerator/default/index', 
		  'id'=>$model->id, 
		]);
	} 
	
	public function actionViewPdf($id)
	{
		return $this->render('view_pdf', [
			$this->findModel($id) => $model,
		]);
	}
	//(end) Author : cavesje  Date : 20150617 - Add new button for GeneratePDF
}

/*20130920*/