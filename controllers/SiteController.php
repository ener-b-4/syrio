<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;

use app\models\admin\users\UserFirstLoginBehavior;
use app\models\admin\users\FirstLoginForm;
use app\models\common\ActionMessage;

use app\models\User;

class SiteController extends Controller
{
    
    public function beforeAction($action) {
        if (User::find()->where(['=', 'status', 10])->count() == 0) {
            
            $session = \Yii::$app->db->beginTransaction();
            try {
                
            } catch (Exception $ex) {
                $session->rollBack();
                die('unable to create the default sys admin');
            }
            
            $newUser = new User();
            $newUser->username = 'admin';
            $newUser->first_names = 'admin';
            $newUser->last_name = 'SyRIO';
            $newUser->email = 'noone@nomail.com';
            $newUser->role_in_organization = \Yii::t('app', 'default administrator user');
            $newUser->setPassword('syrio_admin');
            $newUser->generateAuthKey();      // fix: General error: 1364 Field 'auth_key' doesn't have a default value (2022-12-07)
            $newUser->setScenario('default_admin');
            
            if ($newUser->validate()) {
                $newUser->save();
                
                //add the new user as sys_admin
                $role = \Yii::$app->getAuthManager()->getRole('sys_admin');
                \Yii::$app->getAuthManager()->assign($role, $newUser->id);
                
                $session->commit();
                
            } else {
                $session->rollBack();
                die ('unable to create default admin');
            }
        }
        
        return parent::beforeAction($action);
    }
    
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                //'only' => ['login', 'logout', 'assignments', 'firstLogin', 'permissions', ],
                'except' => [
                    'index', 'about', 'contact', 
                    'login', 'requests', 'verify-password-reset-token', 'set-new-password', 'contact', 'about',
                    'language'],
                'rules' => [
                    [
                        //'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['index', 'login', 'reset-password', 'captcha', 'verify-password-reset-token', 'set-new-password'],
                        'allow' => true,
                        'roles' => [ '?' ]
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                    'set-new-password' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionRequests(){
        $url=Yii::$app->urlManager->createUrl(['/requests/request/index']);
        return $this->redirect($url);
    }
    
    public function actionIndex()
    {
        if (!\Yii::$app->user->isGuest) {
            $user = User::findOne(['id' => Yii::$app->user->id]);
            $user->attachBehavior('firstLogin' , new UserFirstLoginBehavior());
            if ($user->firstLogin)
            {
                //$this->redirect('first-login');
                return $this->render('first-login', ['id'=>\Yii::$app->user->id]);
            }
            else
            {
                return $this->render('index');
            }
        }
        else
        {
            return $this->render('index');
        }
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            //get the loggedin user
            $user = User::findOne(Yii::$app->user->id);

//          Author: cavesje       Date: 2015.08.04
//          I commented this part of code because the image of CAPTCHA is not visible (403 Forbidden error)
//          I left in the index action becuase it's working!

//            //attach UserFirstBehavior to the user
//            $user->attachBehavior('firstLogin' , new UserFirstLoginBehavior());
//            if ($user->firstLogin)
//            {
//                //$this->redirect('first-login');
//                return $this->render('first-login', ['id'=>$user->id]);
//            }
//            else
//            {
                //set the login time as session variable
                \Yii::$app->session->set('userSessionTimeout', time() + Yii::$app->params['sessionTimeoutSeconds']);
                //return $this->goBack();
                return $this->goHome();
//            }
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        //unset the login time as session variable
        \Yii::$app->session->remove('userSessionTimeout');

        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionContact($devs = null)
    {
        $model = new \app\models\ContactForm();
        if (Yii::$app->user->can('sys_admin') && (isset($devs) && $devs==true)) {
            if ($model->load(Yii::$app->request->post())) {

                $model->subject = '(SyRIO ' . Yii::$app->params['competentAuthorityIso2'] . ') ' . $model->subject;
                $model->contact(Yii::$app->params['developersEmail']);
                
                Yii::$app->session->setFlash('contactFormSubmitted');

                return $this->refresh();
            } else {
                $model->name = Yii::$app->user->identity->full_name;
                $model->email = Yii::$app->user->identity->email;
                
                return $this->render('contact_devs', [
                    'model' => $model,
                ]);
            }
        }
        else {
            if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('contactFormSubmitted');

                return $this->refresh();
            } else {
                return $this->render('contact', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionAbout()
    {
        return $this->render('about');
    }
    
    /**
     * SyRIO
     * bv
     * 
     * Redirects the application to restricted area page
     * 
     * @param integer $code the code of the restriction type
     * @return mixed view
     */
    public function actionRestricted($code)
    {
        Yii::$app->response->statusCode = 403; //forbidden
        return $this->render('restricted-area', ['code'=>$code]);
    }
    

    /**
     * Changes the current language of the UI
     * 
     * @author Bogdan Vamanu <bogdan.vamanu@jrc.ec.europa.eu>
     */
    public function actionLanguage()
    {
        $requestedLanguage = Yii::$app->request->post('_lang');
        
        if ($requestedLanguage !== NULL && array_key_exists($requestedLanguage, Yii::$app->params['languages'])) 
        {
            Yii::$app->language = $requestedLanguage;
            $cookie = new \yii\web\Cookie([
                'name' => '_lang',
                'value' => $requestedLanguage,
            ]);
            //set the cookie in reponse
            Yii::$app->getResponse()->getCookies()->add($cookie);
        }
        Yii::$app->end();
    }
    
    
    /* ====================== SyRIO exclusive actions ===================
     * 
     */
    /**
     * The action triggered when this is the first time a new user logs in
     */
    public function actionFirstLogin()
    {
        $model = new FirstLoginForm();
        
        if ($model->load(Yii::$app->request->post()) && $model->validate())
        {
            //get the current user, modify its modified_at property, then save
            $user = User::findOne(Yii::$app->user->id);
            
            $user->setPassword($model->new_password);
            
            $user->modified_at = date('Y-m-d H:i:s');
            $user->status = User::STATUS_ACTIVE;
            
            if (!$user->save())
            {
                echo 'unable to save';
                die();
            }
            return $this->goHome();
        }
        else
        {
            echo '<pre>';
            echo var_dump($model);
            echo '</pre>';
        }
        return $this->render('first-login', ['model'=>$model]);    
    }
    
    public function actionResetPassword() {
        /* @var $user \app\models\User */
        
        $model=new \app\models\forms\PasswordReset();
        
        if ($model->load(Yii::$app->request->post()) && $model->validate())
        {
            //get the user
            $user = User::find()
                    ->where(['username'=>$model->username])
                    ->andWhere(['email'=>$model->email])->one();

            if (!$user)
            {
                $actionMessage = new ActionMessage();
                $actionMessage->SetError(
                        Yii::t('app', 'Either the username or password provided are wrong!', []), 
                        Yii::t('app', 'Please try again.'));
                Yii::$app->session->set('finished_action_result', $actionMessage);                
                return $this->refresh();
            }
            
            //if the user is found, create a passwordresttoken for it
            $user->generatePasswordResetToken();
            $user->save();
            
            if (!\app\models\email\EmailDispacher::sendPasswordResetInformation($model->email, $user->password_reset_token))
            {
                $actionMessage = new ActionMessage();
                $actionMessage->SetError(
                        Yii::t('app', 'There\'s been a problem with our system.' , []), 
                        Yii::t('app', 'Please try again later. We are sorry for any inconvenience.'));
                Yii::$app->session->set('finished_action_result', $actionMessage);                
            };
            
            return $this->render('password-reset-sent', ['model' => $model]);
        }
        
        return $this->render('password-reset', ['model'=>$model]);
    }
    
    /**
     * @author Bogdan Vamanu <bogdan.vamanu@jrc.ec.europa.eu>
     * @version '1.0.0.20150905'
     * 
     * @param type $token
     */
    public function actionVerifyPasswordResetToken($token)
    {
        $new_pass_model = new \app\models\forms\NewPassword();
                
        //get the user with this token
        //this guy also checks the validity of the token (including the time validity)
        //the validity duration is set in the app params config as 'user.passwordResetTokenExpire' =>  86400,   //24 hours in seconds   
        $user = User::findByPasswordResetToken($token);
        
        if (!$user)
        {
            throw new \yii\web\NotFoundHttpException();
        }
        
        $new_pass_model->user_id = $user->id;
        $new_pass_model->token = $token;
        return $this->render('new-password', ['model' => $new_pass_model]);
    }
    
    public function actionSetNewPassword()
    {

        $new_pass_model = new \app\models\forms\NewPassword();

        if ($new_pass_model->load(Yii::$app->request->post()) && $new_pass_model->validate())
        {
            //get the user by id
            $user = User::findByPasswordResetToken($new_pass_model->token);
            
            if (!$user)
            {
                $actionMessage = new ActionMessage();
                $actionMessage->SetError(
                        Yii::t('app', 'Unable to modify your password.' , []), 
                        Yii::t('app', 'Token expired. Please submit a new request.'));
                Yii::$app->session->set('finished_action_result', $actionMessage);
                return $this->render('site/login');
            }
            
            //uncrypt the password sent
            $password = $user->decryptPassword($new_pass_model->new_password);
            
            $user->setPassword($password);
            $user->removePasswordResetToken();
            $user->save();
            
            $actionMessage = new ActionMessage();
            $actionMessage->SetSuccess(
                    Yii::t('app', 'Password successfully changed.' , []), 
                    Yii::t('app', 'Please login using your username and the new password.'));
            Yii::$app->session->set('finished_action_result', $actionMessage);
            
            return $this->redirect(['site/login']);
        }
        
        throw new \ErrorException('wrong token');
    }
    
    /*
     * ====================== SECTION RBAC ==============================
     * 
     *      THE ACTIONS IN THIS SECTION ARE ONLY FOR DEVELOPMENT VERSION
     * 
     *      REMOVE ALL THE CONTENT WHEN PUTING SYRIO INTO PRODUCTION
     * 
     * ==================================================================
     */
    
    public function actionRoles()
    {
        $url = Yii::$app->urlManager->createUrl('/mdm_admin/role');
//        if (Yii::$app->user->can('sys_admin'))
        if (Yii::$app->user->can('ca_admin'))        
        {
            return $this->redirect($url);
        }
        else
        {
            echo '<script>alert("not allowed")</script>';
        }
    }

    public function actionPermissions()
    {
        $url = Yii::$app->urlManager->createUrl('/mdm_admin/permission');
        if (Yii::$app->user->can('sys_admin'))
        {
            return $this->redirect($url);
        }
        else
        {
            echo '<script>alert("not allowed")</script>';
        }
    }
    
    public function actionAssignments()
    {
        $url = Yii::$app->urlManager->createAbsoluteUrl('/mdm_admin/assignment');
        if (Yii::$app->user->can('sys_admin'))
        {
            return $this->redirect($url);
        }
        else
        {
            echo '<script>alert("not allowed")</script>';
        }
    }

    public function actionRules()
    {
        $url = Yii::$app->urlManager->createAbsoluteUrl('/mdm_admin/rule');
        if (Yii::$app->user->can('sys_admin'))
        {
            return $this->redirect($url);
        }
        else
        {
            echo '<script>alert("not allowed")</script>';
        }
    }

    /*
     * ===================== End of SECTION RBAC ==============================
     */ 
        
    
}
