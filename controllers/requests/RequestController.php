<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\controllers\requests;

use Yii;
use app\models\requests\Request;
use app\models\requests\RequestSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \app\models\requests\RequestTypes;
use app\models\email\EmailDispacher;
use app\models\common\ActionMessage;
use app\components\Errors;

/**
 * RequestController implements the CRUD actions for Request model.
 */
class RequestController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                //only authenticated users
                'rules' => [
                    [
                        'actions' => ['index', 
                            'user_new_operator', 
                            'user_new_installation', 
                            'user_new_competent_authority',
                            'new_organization',
                            'new_installation',
                            'captcha'],
                        'allow' => true,
                        'roles' => [ '?' ]
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    
    public function actionList()
    {
        if (!Yii::$app->user->can('process_request'))
        {
            return Yii::$app->error->ThrowError(Errors::RESTRICTED_AREA_ERROR);
        }
        
        $searchModel = new RequestSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('list', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    
    /**
     * Lists all Request models.
     * @return mixed
     */
    public function actionIndex()
    {
        //return $this->render('@app/views/shared_parts/not-implemented.php', ['action' => $this->id . '/' . $this->action->id]);        
        
        $searchModel = new RequestSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    
    public function actionUser_new_operator()
    {
 
        //he model must be forms/UserRegistrationOperators
        $actionMessage = new ActionMessage();
        
        $model=new \app\models\forms\UserRegistrationOperators();
        $roles = [
            'op_admin' => \Yii::t('app', 'Administrator'),
            'op_raporteur' => \Yii::t('app', 'Rapporteur'),
            'op_user' => \Yii::t('app', 'Member'),
        ];
        
        if ($model->load(Yii::$app->request->post()))
        {
            if (!$model->validate())
            {
                return $this->render('display-form', ['model' => $model, 'request_type'=>  RequestTypes::NEW_OO_USER, 'extra' => $roles]);
            }
            

            //create the request model
            $request = new Request();
            $request->request_type = RequestTypes::NEW_OO_USER;
            $request->id = time();
            $request->from = \Yii::$app->user->isGuest ? -1 : \Yii::$app->user->id;
            $request->content = json_encode($model);
            
            $transaction = \Yii::$app->db->beginTransaction();
            
            if ($request->validate())
            {
                if (!$request->save())
                {
                    $transaction->rollBack();
                    $actionMessage->SetError(
                            Yii::t('app', 'Unable to process your request!', []), 
                            Yii::t('app', 'Please retry later.'));
                    Yii::$app->session->set('finished_action_result', $actionMessage);
                
                    return $this->refresh();
                }
                else
                {
                    //send request submitted confirmation
                    if (!EmailDispacher::sendUserRegistrationRequest($request))
                    {
                        $transaction->rollBack();
                        $transaction->rollBack();
                        $actionMessage->SetError(
                                Yii::t('app', 'Unable to process your request!', []), 
                                Yii::t('app', 'Please retry later.'));
                        Yii::$app->session->set('finished_action_result', $actionMessage);

                        return $this->refresh();
                    }
                    
                    $transaction->commit();
                    $actionMessage->SetSuccess(
                            Yii::t('app', 'Your request has been submitted!', []), 
                            Yii::t('app', 'Please check your mail regularly.'));
                    Yii::$app->session->set('finished_action_result', $actionMessage);
                    
                    return $this->render('index', []);  
                }
            }
        }
        
        return $this->render('display-form', ['model' => $model, 'request_type'=>  RequestTypes::NEW_OO_USER, 'extra' => $roles]);
        
        return $this->render('@app/views/shared_parts/not-implemented.php', ['action' => $this->id . '/' . $this->action->id]);        
        
    }
    
    public function actionUser_new_installation()
    {
        //he model must be forms/UserRegistrationInstallation
        $actionMessage = new ActionMessage();
        
        $model=new \app\models\forms\UserRegistrationInstallation();
        $roles = [
            'inst_admin' => \Yii::t('app', 'Administrator'),
            'inst_rapporteur' => \Yii::t('app', 'Rapporteur'),
            'inst_user' => \Yii::t('app', 'Member'),
        ];
        
        if ($model->load(Yii::$app->request->post()))
        {
            if (!$model->validate())
            {
                return $this->render('display-form', ['model' => $model, 'request_type'=>  RequestTypes::NEW_INST_USER, 'extra' => $roles]);
            }
            

            //create the request model
            $request = new Request();
            $request->request_type = RequestTypes::NEW_INST_USER;
            $request->id = time();
            $request->from = \Yii::$app->user->isGuest ? -1 : \Yii::$app->user->id;
            $request->content = json_encode($model);
            
            $transaction = \Yii::$app->db->beginTransaction();
            
            if ($request->validate())
            {
                if (!$request->save())
                {
                    $transaction->rollBack();
                    $actionMessage->SetError(
                            Yii::t('app', 'Unable to process your request!', []), 
                            Yii::t('app', 'Please retry later.'));
                    Yii::$app->session->set('finished_action_result', $actionMessage);
                
                    return $this->refresh();
                }
                else
                {
                    //send request submitted confirmation
                    if (!EmailDispacher::sendUserRegistrationRequest($request))
                    {
                        $transaction->rollBack();
                        $transaction->rollBack();
                        $actionMessage->SetError(
                                Yii::t('app', 'Unable to process your request!', []), 
                                Yii::t('app', 'Please retry later.'));
                        Yii::$app->session->set('finished_action_result', $actionMessage);

                        return $this->refresh();
                    }
                    
                    $transaction->commit();
                    $actionMessage->SetSuccess(
                            Yii::t('app', 'Your request has been submitted!', []), 
                            Yii::t('app', 'Please check your mail regularly.'));
                    Yii::$app->session->set('finished_action_result', $actionMessage);
                    
                    return $this->render('index', []);  
                }
            }
        }
        
        return $this->render('display-form', ['model' => $model, 'request_type'=>  RequestTypes::NEW_INST_USER, 'extra' => $roles]);
        return $this->render('@app/views/shared_parts/not-implemented.php', ['action' => $this->id . '/' . $this->action->id]);        
        
    }

    public function actionUser_new_competent_authority()
    {
        //he model must be forms/UserRegistrationCa
        $actionMessage = new ActionMessage();
        
        $model=new \app\models\forms\UserRegistrationCa();
        $roles = [
            'ca_admin' => \Yii::t('app', 'Administrator'),
            'ca_assessor' => \Yii::t('app', 'Assessor'),
            'ca_user' => \Yii::t('app', 'Member'),
        ];
        
        if ($model->load(Yii::$app->request->post()))
        {
            if (!$model->validate())
            {
                return $this->render('display-form', ['model' => $model, 'request_type'=>  RequestTypes::NEW_CA_USER, 'extra' => $roles]);
            }
            

            //create the request model
            $request = new Request();
            $request->request_type = RequestTypes::NEW_CA_USER;
            $request->id = time();
            $request->from = \Yii::$app->user->isGuest ? -1 : \Yii::$app->user->id;
            $request->content = json_encode($model);
            
            $transaction = \Yii::$app->db->beginTransaction();
            
            if ($request->validate())
            {
                if (!$request->save())
                {
                    $transaction->rollBack();
                    $actionMessage->SetError(
                            Yii::t('app', 'Unable to process your request!', []), 
                            Yii::t('app', 'Please retry later.'));
                    Yii::$app->session->set('finished_action_result', $actionMessage);
                
                    return $this->refresh();
                }
                else
                {
                    //send request submitted confirmation
                    if (!EmailDispacher::sendUserRegistrationRequest($request))
                    {
                        $transaction->rollBack();
                        $transaction->rollBack();
                        $actionMessage->SetError(
                                Yii::t('app', 'Unable to process your request!', []), 
                                Yii::t('app', 'Please retry later.'));
                        Yii::$app->session->set('finished_action_result', $actionMessage);

                        return $this->refresh();
                    }
                    
                    $transaction->commit();
                    $actionMessage->SetSuccess(
                            Yii::t('app', 'Your request has been submitted!', []), 
                            Yii::t('app', 'Please check your mail regularly.'));
                    Yii::$app->session->set('finished_action_result', $actionMessage);
                    
                    return $this->render('index', []);  
                }
            }
        }
        
        return $this->render('display-form', ['model' => $model, 'request_type'=>  RequestTypes::NEW_CA_USER, 'extra' => $roles]);
                return $this->render('@app/views/shared_parts/not-implemented.php', ['action' => $this->id . '/' . $this->action->id]);        
        
    }

    
    /**
     * 
     * @param int $type 200 - CA; 201 - OO
     * @return type
     */
    public function actionNew_organization($type)
    {
        //the model must be forms/NewOrganizationRequest
        $actionMessage = new ActionMessage();
        
        $model=new \app\models\forms\NewOrganizationRequest();
        
        if ($model->load(Yii::$app->request->post()))
        {
            if (!$model->validate())
            {
                return $this->render('display-form', ['model' => $model, 'request_type'=>  $type, 'extra' => null]);
            }
            

            //create the request model
            $request = new Request();
            $request->request_type = $type;
            $request->id = time();
            $request->from = \Yii::$app->user->isGuest ? -1 : \Yii::$app->user->id;
            $request->content = json_encode($model);
            
            $transaction = \Yii::$app->db->beginTransaction();
            
            if ($request->validate())
            {
                if (!$request->save())
                {
                    $transaction->rollBack();
                    $actionMessage->SetError(
                            Yii::t('app', 'Unable to process your request!', []), 
                            Yii::t('app', 'Please retry later.'));
                    Yii::$app->session->set('finished_action_result', $actionMessage);
                
                    return $this->render('display-form', ['model' => $model, 'request_type'=>  $type, 'extra' => null]);
                }
                else
                {
                    //send request submitted confirmation
                    if (!EmailDispacher::sendOrganizationRegistrationRequest($request))
                    {
                        $transaction->rollBack();
                        $transaction->rollBack();
                        $actionMessage->SetError(
                                Yii::t('app', 'Unable to process your request!', []), 
                                Yii::t('app', 'Please retry later.'));
                        Yii::$app->session->set('finished_action_result', $actionMessage);

                    return $this->render('display-form', ['model' => $model, 'request_type'=>  $type, 'extra' => null]);
                    }
                    
                    $transaction->commit();
                    
                    $actionMessage->SetSuccess(
                            Yii::t('app', 'Your request has been submitted!', []), 
                            Yii::t('app', 'Please check your mail regularly.'));
                    Yii::$app->session->set('finished_action_result', $actionMessage);
                    
                    return $this->render('index', []);  
                    
                    //return $this->render('display-form', ['model' => $model, 'request_type'=>  $type, 'extra' => null]);
                }
            }
        }
 
        return $this->render('display-form', ['model' => $model, 'request_type'=>  $type, 'extra' => null]);
        
        return $this->render('@app/views/shared_parts/not-implemented.php', ['action' => $this->id . '/' . $this->action->id]);        
        
    }    
    
    
    /**
     * 
     * @param int $type 200 - CA; 201 - OO
     * @return type
     */
    public function actionNew_installation()
    {
        //the model must be forms/NewInstallationRequest
        $actionMessage = new ActionMessage();
        
        $model=new \app\models\forms\NewInstallationRequest();
        
        $installation_types = \yii\helpers\ArrayHelper::map(\app\models\InstallationTypes::find()->all(), 'type', 
                function($data) { 
                    return $data->type . ' - ' . $data->name; 
                }); 
        $countries = \yii\helpers\ArrayHelper::map(\app\modules\management\models\Country::find()->all(), 'iso2', 'short_name');
        $op_types = $model->operationalStatuses;
        
        $extra = [];
        $extra['inst_types'] = $installation_types;
        $extra['countries'] = $countries;
        $extra['op_types'] = $op_types;
        $extra['anonymous'] = Yii::$app->user->isGuest ? 1 : 0;
        
        if ($model->load(Yii::$app->request->post()))
        {
            if (!Yii::$app->user->isGuest) {
                $theUser = Yii::$app->user->identity;
                $model->first_name = $theUser->first_names;
                $model->second_name = $theUser->last_name;
                $model->email = $theUser->email;
                $model->email_repeat = $theUser->email;
                
                $model->phone = isset($theUser->phone) ? $theUser->phone : '---';
                $model->role_in_organization = isset($theUser->role_in_organization) ? $theUser->role_in_organization : '?';

                if (isset($theUser->organization)) {
                    $model->organization = "{$theUser->organization->organization_acronym} - {$theUser->organization->organization_name}";
                } else {
                    $model->organization = '?';
                }
            }
            
            if (!$model->validate())
            {
                return $this->render('display-form', ['model' => $model, 'request_type'=> RequestTypes::NEW_INST, 'extra' => $extra]);
            }
            

            //create the request model
            $request = new Request();
            $request->request_type = RequestTypes::NEW_INST;
            $request->id = time();
            $request->from = \Yii::$app->user->isGuest ? -1 : \Yii::$app->user->id;
            $request->content = json_encode($model);
            
            $transaction = \Yii::$app->db->beginTransaction();
            
            if ($request->validate())
            {
                if (!$request->save())
                {
                    $transaction->rollBack();
                    $actionMessage->SetError(
                            Yii::t('app', 'Unable to process your request!', []), 
                            Yii::t('app', 'Please retry later.'));
                    Yii::$app->session->set('finished_action_result', $actionMessage);
                
                    return $this->render('display-form', ['model' => $model, 'request_type'=>  RequestTypes::NEW_INST, 'extra' => $extra]);
                }
                else
                {
                    //send request submitted confirmation
                    if (!EmailDispacher::sendInstallationRegistrationRequest($request))
                    {
                        $transaction->rollBack();
                        $transaction->rollBack();
                        $actionMessage->SetError(
                                Yii::t('app', 'Unable to process your request!', []), 
                                Yii::t('app', 'Please retry later.'));
                        Yii::$app->session->set('finished_action_result', $actionMessage);

                    return $this->render('display-form', ['model' => $model, 'request_type'=>  RequestTypes::NEW_INST, 'extra' => $extra]);
                    }
                    
                    $transaction->commit();
                    
                    $actionMessage->SetSuccess(
                            Yii::t('app', 'Your request has been submitted!', []), 
                            Yii::t('app', 'Please check your mail regularly.'));
                    Yii::$app->session->set('finished_action_result', $actionMessage);
                    
                    return $this->render('index', []);  
                }
            }
        }
 
        if (!Yii::$app->user->isGuest) {
            $theUser = Yii::$app->user->identity;
            $model->first_name = $theUser->first_names;
            $model->second_name = $theUser->last_name;
            $model->email = $theUser->email;
            $model->email_repeat = $theUser->email;
            
            $model->phone = isset($theUser->phone) ? $theUser->phone : '---';
            $model->role_in_organization = isset($theUser->role_in_organization) ? $theUser->role_in_organization : '?';
            
            if (isset($theUser->organization)) {
                $model->organization = "{$theUser->organization->organization_acronym} - {$theUser->organization->organization_name}";
            } else {
                $model->organization = '?';
            }
        }
        
        return $this->render('display-form', ['model' => $model, 'request_type'=>  RequestTypes::NEW_INST, 'extra' => $extra]);
        
    }    
        
    
    /**
     * Displays a single Request model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        //$model is request
        if (!\Yii::$app->user->can('process_request'))
        {
            return Yii::$app->error->ThrowError(Errors::RESTRICTED_AREA_ERROR);
        }
        
        $actionMessage = new ActionMessage();
        
        $model=$this->findModel($id);
        
        if ($model->load(\Yii::$app->request->post()))
        {
            $model->processed_by_id = \Yii::$app->user->id;
            $model->processed_at = time();
            
            if ($model->validate())
            {
                $transaction = \Yii::$app->db->beginTransaction();
                
                if (!$model->save())
                {
                    $transaction->rollBack();
                    $actionMessage->SetError(
                        Yii::t('app', 'Unable to close request #{evt}!', ['evt'=>$model->id]), 
                        Yii::t('app', 'Please retry later'));
                        Yii::$app->session->set('finished_action_result', $actionMessage);               
                }
                
                $transaction->commit();
                
                if (isset($model->send_justification) && $model->send_justification)
                {
                    EmailDispacher::sendRequestProcessed($model);
                }
                
                $msgcontent = $model->send_justification ? \Yii::t('app', 'An email has been sent to the requester.') : '';
                $actionMessage->SetSuccess(
                    Yii::t('app', 'Request #{evt} has been closed!', ['evt'=>$model->id]), 
                    $msgcontent);
                    Yii::$app->session->set('finished_action_result', $actionMessage);                
            }
            else
            {
                $actionMessage->SetError(
                    Yii::t('app', 'Unable to close request #{evt}!', ['evt'=>$model->id]), 
                    Yii::t('app', 'Missing information.'));
                Yii::$app->session->set('finished_action_result', $actionMessage);               
            }
        }
        
        return $this->render('view', [
            'model' => $model,
        ]);        
    }

    public function actionReopen($id)
    {
        if (!\Yii::$app->user->can('process_request'))
        {
            return Yii::$app->error->ThrowError(Errors::RESTRICTED_AREA_ERROR);
        }
        
        $actionMessage = new ActionMessage();
        
        $model=$this->findModel($id);
        $model->status = Request::STATUS_OPEN;
        $model->processed_by_id = \Yii::$app->user->id;
        $model->processed_at = time();
        $model->justification = '';
        
        if ($model->save())
        {
            EmailDispacher::sendRequestReopened($model);
            
            $actionMessage->SetSuccess(
            Yii::t('app', 'Request #{evt} has been reopened!', ['evt'=>$model->id]), 
            Yii::t('app', 'An email has been sent to the requester.'));
            Yii::$app->session->set('finished_action_result', $actionMessage);
        }
        else {
            $actionMessage->SetFailure(
            Yii::t('app', 'Unable to reopen the request!', []), 
            Yii::t('app', 'Please retry later.'));
            Yii::$app->session->set('finished_action_result', $actionMessage);
        }
        
        return $this->render('view', [
            'model' => $model,
        ]);       
    }
    

    /**
     * Deletes an existing Request model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if (!\Yii::$app->user->can('process_request'))
        {
            return Yii::$app->error->ThrowError(Errors::RESTRICTED_AREA_ERROR);
        }
        
        $model = $this->findModel($id);
        $model->delete();
        
        $actionMessage = new ActionMessage();
        $actionMessage->SetSuccess(
        Yii::t('app', 'Delete request'), 
        Yii::t('app', 'Request #{evt} has been deleted.', ['evt'=>$model->id]));
        Yii::$app->session->set('finished_action_result', $actionMessage);

        $searchModel = new RequestSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->redirect(['list']);
        
        //return $this->redirect(['index']);
        //return $this->refresh();
    }

    /**
     * Finds the Request model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Request the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Request::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
