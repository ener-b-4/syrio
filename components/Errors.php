<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\components;

use Yii;
use yii\base\Component;

/**
 * Description of ErrorsController
 *
 * @author vamanbo
 */
class Errors extends Component{
    //put your code here
   
    const RESTRICTED_AREA_ERROR = 1;
    const RESTRICTED_OPERATION_ERROR = 2;

    const WARN_LAST_CAO = 1;

    public function ThrowError($error) {
        switch ($error)
        {
            case self::RESTRICTED_AREA_ERROR:
                $url = \Yii::$app->urlManager->createUrl(['site/restricted', 'code'=>$error]);
                \Yii::$app->controller->redirect($url);
                break;
            case self::RESTRICTED_OPERATION_ERROR:
                $url = \Yii::$app->urlManager->createUrl(['site/restricted', 'code'=>$error]);
                \Yii::$app->controller->redirect($url);
                break;
        }
    }  
    
    public function ThrowWarning($code) {
        return \Yii::$app->controller->render('@app/views/site/warning', ['code'=>$code]);
    }
    
    
    public static function WarnText($code) {
        switch ($code) {
            case self::WARN_LAST_CAO:
                $s = Yii::t('app', 'The Organization cannot be deleted.');
                $s .= Yii::t('app', 'There must be at least one Competent Authority Organization left.');
                
                return $s;
                break;
        }
    }
    
}
