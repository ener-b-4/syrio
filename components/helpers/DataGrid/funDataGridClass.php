<?php
/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\components\helpers\DataGrid;

/**
 * Description of funDataGridClass
 *
 * @author cavesje
 */
class funDataGridClass {
  
  public function getOurPdfHeader ($title_h_l, $title_h_c, $title_h_r)
  {
      // Header options for PDF format
      $ourPdfHeader = [
        'L' => [
            'content'   => $title_h_l,
            'font-size' => 8,
            'color'     => '#333333'
        ],
        'C' => [
            'content'   => $title_h_c,
            'font-size' => 16,
            'color'     => '#333333'
        ],
        'R' => [
            'content'   => $title_h_r,
            'font-size' => 8,
            'color'     => '#333333'
        ]
      ];
      return $ourPdfHeader;
  }
  
  public function getOurPdfFooter ($title_f)
  {
    // Footer options for PDF format
    $ourPdfFooter = [
      'L'    => [
          'content'    => $title_f,
          'font-size'  => 8,
          'font-style' => 'B',
          'color'      => '#999999'
      ],
      'R'    => [
          'content'     => '[ {PAGENO} ]',
          'font-size'   => 10,
          'font-style'  => 'B',
          'font-family' => 'serif',
          'color'       => '#333333'
      ],
      'line' => TRUE,
    ];
    return $ourPdfFooter;
  }
  
  
  public function getExportConfiguration($ourPdfHeader, $ourPdfFooter) 
  {
    // Cumtom export configuration
    $exportConfig = [
        
//        CustomGridView::HTML  => [
//              'label'           => 'HTML',
//              'icon'            => 'file-text',
//              'iconOptions'     => ['class' => 'text-info'],
//              'showHeader'      => TRUE,
//              'showPageSummary' => TRUE,
//              'showFooter'      => TRUE,
//              'showCaption'     => TRUE,
//              'alertMsg'        => 'The HTML export file will be generated for download.',
//              'options'         => ['title' => 'Hyper Text Markup Language'],
//              'mime'            => 'text/html',
//              'config'          => [
//                  'cssFile' => 'http://netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css'
//              ]
//          ],
          CustomGridView::CSV   => [
              'label'           => 'CSV',
              'icon'            => 'file-code-o',
              'iconOptions'     => ['class' => 'text-primary'],
              'showHeader'      => TRUE,
              'showPageSummary' => TRUE,
              'showFooter'      => TRUE,
              'showCaption'     => TRUE,
              'alertMsg'        => \Yii::t('app', 'The {evt} file will be generated for download.', ['evt'=>'CSV']),
              'options'         => ['title' => 'Comma Separated Values'],
              'mime'            => 'application/csv',
              'config'          => [
                  'colDelimiter' => ",",
                  'rowDelimiter' => "\r\n",
              ]
          ],
          CustomGridView::EXCEL => [
              'label'           => 'Excel',
              'icon'            => 'file-excel-o',
              'iconOptions'     => ['class' => 'text-success'],
              'showHeader'      => TRUE,
              'showPageSummary' => TRUE,
              'showFooter'      => TRUE,
              'showCaption'     => TRUE,
              'alertMsg'        => \Yii::t('app', 'The {evt} file will be generated for download.', ['evt'=>'EXCEL']),
              'options'         => ['title' => 'Microsoft Excel 95+'],
              'mime'            => 'application/vnd.ms-excel',
              'config'          => [
                  'worksheet' => 'Worksheet',
                  'cssFile'   => ''
              ]
          ],
          CustomGridView::PDF   => [
              'label'           => 'PDF',
              'icon'            => 'file-pdf-o',
              'iconOptions'     => ['class' => 'text-danger'],
              'showHeader'      => TRUE,
              'showPageSummary' => TRUE,
              'showFooter'      => TRUE,
              'showCaption'     => TRUE,
              'alertMsg'        => \Yii::t('app', 'The {evt} file will be generated for download.', ['evt'=>'PDF']),
              'options'         => ['title' => 'Portable Document Format'],
              'mime'            => 'application/pdf',
              'config'          => [
                  //'mode'          => 'c',
                  'mode' => \kartik\mpdf\Pdf::MODE_UTF8,                  
                  'format'        => 'A4-L',
                  'destination'   => 'D',
                  'marginTop'     => 20,
                  'marginBottom'  => 20,
                  'cssInline'     => '.kv-wrap{padding:20px;}' .
                      '.kv-align-center{text-align:center;}' .
                      '.kv-align-left{text-align:left;}' .
                      '.kv-align-right{text-align:right;}' .
                      '.kv-align-top{vertical-align:top!important;}' .
                      '.kv-align-bottom{vertical-align:bottom!important;}' .
                      '.kv-align-middle{vertical-align:middle!important;}' .
                      '.kv-page-summary{border-top:4px double #ddd;font-weight: bold;}' .
                      '.kv-table-footer{border-top:4px double #ddd;font-weight: bold;}' .
                      '.kv-table-caption{font-size:1.5em;padding:8px;border:1px solid #ddd;border-bottom:none;}',
                  'methods'       => [
                      'SetHeader' => [
                          ['odd' => $ourPdfHeader, 'even' => $ourPdfHeader]
                      ],
                      'SetFooter' => [
                          ['odd' => $ourPdfFooter, 'even' => $ourPdfFooter]
                      ],
                  ],
                  'options'       => [
                      'title'    => 'Custom Title',
                      'subject'  => 'PDF export',
                      'keywords' => 'pdf'
                  ],
                  'contentBefore' => '',
                  'contentAfter'  => ''
              ]
          ]
      ];
      return $exportConfig;
  }
  
}
