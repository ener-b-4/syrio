<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\components\helpers;

/**
 * Extend the ArrayHelper with a tMap function that translates the values
 *
 * @author vamanbo
 */
class TArrayHelper extends \yii\helpers\ArrayHelper {
    //put your code here
    
    
    /*
     * Builds a map (key-TRANSLATED(value) pairs) from a multidimensional array or an array of objects.
     */
    public static function tMap($array, $from, $to, $tclass, $group = null, $except = null)
    {
        $result = [];
        foreach ($array as $element) {
            $key = static::getValue($element, $from);
            $value = static::getValue($element, $to);
            
            if ($group !== null) {
                if (isset($except) && key_exists($value, $except)) {
                    $result[static::getValue($element, $group)][$key] = $except[$value];
                }
                else {
                    $result[static::getValue($element, $group)][$key] = \Yii::t($tclass, $value);
                }
            } else {
                if (isset($except) && key_exists($value, $except)) {
                    $result[$key] = $except[$value];
                }
                else {
                    $result[$key] = \Yii::t($tclass, $value);
                }
            }
        }

        return $result;
    }

    /**
     * 
     * @param type $array
     * @param string $group optional
     */
    public static function tIndex($array, $group = null) {
        $return = [];
        
        if (is_null($group)) {
            return $array;
        } else {
            /* build by group */
            if (count($array)>0) {
                $first_elem = array_values($array)[0];
                if (is_object($first_elem)) {
                    $return = self::groupFromObject($array, $group);
                } elseif (is_array($first_elem)) {
                    $return = self::groupFromArray($array, $group);
                } else {
                    return $array;
                }
            }
            return $return;
        }
    }

    
    private static function groupFromArray($array, $group) {
        $return  = [];
        foreach($array as $item) {
            $key = $item[$group];
            if (!key_exists($key, $return)) {
                $return[$key] = [];
            }
            $return[$key][] = $item;
        }
        return $return;
    }

    private static function groupFromObject($array, $group) {
        $return  = [];
        foreach($array as $item) {
            $key = $item->$group;
            if (!key_exists($key, $return)) {
                $return[$key] = [];
            }
            $return[$key][] = $item;
        }
        return $return;
    }
    
}
