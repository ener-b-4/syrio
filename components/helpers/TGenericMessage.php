<?php

namespace app\components\helpers;

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

/**
 * Description of TGenericMessage
 *
 * @author vamanbo
 */
class TGenericMessage {
    
    
    public static function Error($br = false) {
        $msg = \Yii::t('app/messages/errors', 'There\'s been an error while processing your request.');
        $msg .= '\n' . \Yii::t('app/messages/errors', 'Sorry for any inconvenience.');
        $msg .= '\n' . \Yii::t('app/messages/errors', 'Please retry later.');

        if ($br) {
            return str_replace('\n', '<br/>', $msg);
            //return nl2br($msg);
        }
        else
        {
            return $msg;
        }
    }
    
    public static function Validation($br = false) {
        $msg = \Yii::t('app/messages/errors', 'The data provided has validation errors.');
        $msg .= '\n' . \Yii::t('app/messages/errors', 'Please revise and retry.');

        if ($br) {
            return nl2br($msg);
        }
        else
        {
            return $msg;
        }
    }
    
    public static function Purged($evt = null, $br = false) {
        if (!isset($evt)) {
            $evt = ucfirst(\Yii::t('app/messages', 'the item'));
        }

        $msg = \Yii::t('app/messages', '{evt} has been purged (completely removed)!', [
            'evt'=>$evt
        ]);
        
        if ($br) {
            return nl2br($msg);
        }
        else
        {
            return $msg;
        }
    }
    

    public static function Removed($evt = null, $br = false) {
        if (!isset($evt)) {
            $evt = ucfirst(\Yii::t('app/messages', 'the item'));
        }

        $msg = \Yii::t('app/messages', '{evt} has been successfully removed.', [
            'evt'=>$evt
        ]);

        $msg .= '\n' . \Yii::t('app', 'Operation CAN be undone.', [
            'evt'=>$evt
        ]);
        
        if ($br) {
            return nl2br($msg);
        }
        else
        {
            return $msg;
        }
    }

    public static function Reactivated($evt = null, $br = false) {
        if (!isset($evt)) {
            $evt = ucfirst(\Yii::t('app/messages', 'the item'));
        }

        $msg = \Yii::t('app/messages', '{evt} has been successfully reactivated.', [
            'evt'=>$evt
        ]);

        if ($br) {
            return nl2br($msg);
        }
        else
        {
            return $msg;
        }
    }

    
    public static function IncidentSigned($evt = null, $br = false) {
        if (!isset($evt)) {
            $evt = ucfirst(\Yii::t('app', 'the report'));
        }

        $msg = \Yii::t('app/messages', '{evt} has been SIGNED.', [
            'evt'=>$evt
        ]);

        $msg .= '\n' . \Yii::t('app/messages', 'You may now proceed with submitting the Report to the Competent Authority.', [
            'evt'=>$evt
        ]);
        
        if ($br) {
            return nl2br($msg);
        }
        else
        {
            return $msg;
        }
    }
    
}
