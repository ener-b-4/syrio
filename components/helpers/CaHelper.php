<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\components\helpers;

/**
 * Description of CaHelper
 *
 * @author Bogdan
 */
class CaHelper {
    
    /**
     * returns the CpfSession for the current year or null otherwise
     * @param type $year
     */
    public static function getCpfSession($year) {
        return \app\modules\cpfbuilder\models\CpfSessions::findOne(['year'=>$year]);
    }
    
}
