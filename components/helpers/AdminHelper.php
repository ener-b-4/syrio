<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\components\helpers;

use Yii;

/**
 * AdminHelper
 *
 * @author Bogdan
 */

class AdminHelper
{
    public static function getRoles() {
        return [
            'sys_admin' => [
                'grade' => 0,
                'short' => Yii::t('app','SyRIO administrator'),
                'description' => Yii::t('app', 'full controll over SyRIO')
            ],
            'ca_admin' => [
                'grade' => 1,
                'short' => Yii::t('app','Administrator {evt}', ['evt'=>Yii::t('app', 'Competent Authority')]),
                'description' => Yii::t('app', 'administrator rights over the Competent Authority section of SyRIO')
            ],
            'ca_assessor' => [
                'grade' => 1,
                'short' => Yii::t('app','Assessor'),
                'description' => Yii::t('app', 'allows the assessment of the incident reports submitted by the Operators/Owners')
            ],
            'ca_user' => [
                'grade' => 1,
                'short' => Yii::t('app','User ({evt})', ['evt'=>Yii::t('app', 'Competent Authority')]),
                'description' => Yii::t('app', 'allows the access to the data in your Competent Authority')
            ],
            'op_admin' => [
                'grade' => 2,
                'short' => Yii::t('app','Administrator {evt}', ['evt'=>Yii::t('app', 'Operators/Owners')]),
                'description' => Yii::t('app', 'administrator rights over your Organization and installations data')
            ],
            'op_raporteur' => [
                'grade' => 2,
                'short' => Yii::t('app','Rapporteur'),
                'description' => Yii::t('app', 'allows the incident reporting and management for your Organization and installations')
            ],
            'op_user' => [
                'grade' => 2,
                'short' => Yii::t('app','User ({evt})', ['evt'=>Yii::t('app', 'Operators/Owners')]),
                'description' => Yii::t('app', 'allows the access to the reported incidents by your Organization as well as some reporting rights')
            ],
            'inst_admin' => [
                'grade' => 3,
                'short' => Yii::t('app','Administrator {evt}', ['evt'=>Yii::t('app', 'installation')]),
                'description' => Yii::t('app', 'administrator rights over your installation data')
            ],
            'inst_rapporteur' => [
                'grade' => 3,
                'short' => Yii::t('app','Rapporteur ({evt})', ['evt'=>Yii::t('app', 'installation')]),
                'description' => Yii::t('app', 'allows the incident reporting and management for your installation')
            ],
            'inst_user' => [
                'grade' => 3,
                'short' => Yii::t('app','User ({evt})', ['evt'=>Yii::t('app', 'installation')]),
                'description' => Yii::t('app', 'allows the access to the reported incidents by your Installation as well as some reporting rights')
            ],

        ];
    }
    
    
    public static function buildUserRoles($id) {
        $user = \app\models\User::find()->where(['id'=>$id]);
        $auth = Yii::$app->authManager;
        
        $item_template = '<li>%s</li>';
        $items = [];
        foreach($auth->getRolesByUser($id) as $role) {
            $items[] = sprintf($item_template, self::getRoles()[$role->name]['short']);
        }
        
        $template = '<div style="background-color: #080808; padding:6px; margin-top:-6px; margin-bottom:6px; margin-left:-18px; margin-right:-18px; color: lightgray">
                <small style="color: #5391af"><strong>'. Yii::t('app', 'Logged in as:').'</strong></small><br/>'                
                . '<div>'
                . ' <ul class="list list-unstyled">'
                . implode('', $items)
                . ' </ul>'
                . ' </div>'
                . '</div>';
        
        return $template;
    }
    
    
    public static function getUserRolesDescription($id) {
        $user = \app\models\User::find()->where(['id'=>$id]);
        $auth = Yii::$app->authManager;
        
        $items = [];
        foreach($auth->getRolesByUser($id) as $role) {
            $items[] = self::getRoles()[$role->name];
        }
        
        return $items;
    }
    
}
