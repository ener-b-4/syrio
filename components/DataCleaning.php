<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\components;

/**
 * The class implements the business logic for cleaning up the user input.
 * The role of the class is to provide a cascade-type of cleaning information.
 * 
 * Given an O and an array of attribute dependencies the class allows performing the cleaning up of the object (i.e. nullify dependencies based on trigger values)
 * 
 *
 * @author vamanbo
 */
class DataCleaning {
    //put your code here
    
 
        /**
         * Resets all the attributes of a model to NULL
         * 
         * @param Model $model
         * @param array[string] $exceptions the list of the attribute names that should be kept untouched
         * @return boolean
         */
        public static function ResetSection($model, $exceptions) {
            try {
                if (isset($exceptions) && is_array($exceptions)) {
                    foreach($model->attributes() as $attr) {
                        if (!in_array($attr, $exceptions)) {
                            $model->setAttribute($attr, null);
                            $model->markAttributeDirty($attr);
                        }
                    }
                }
                
                return true;
            } catch (Exception $ex) {
                return false;
            }
            //$x->refresh();
        }
        
        /**
         * Resets only a given set of attributes of a model to NULL
         * 
         * @param Model $model
         * @param array[string] $attrlist the name of the attributes that SHOULD be reset
         * @return boolean
         */
        public static function ResetAttributes($model, $attrlist) {
            try {
                if (isset($attrlist) && is_array($attrlist)) {
                    $attributes = $model->attributes($attrlist);
                    foreach($attributes as $attr) {
                        if (in_array($attr, $attrlist)) {
                            $model->setAttribute($attr, null);
                            $model->markAttributeDirty($attr);
                        }
                    }
                }
                return true;
            } catch (Exception $ex) {
                return false;
            }
        }

        
        public function refreshAttributes($model, $sitrep = null) {
            
            if (!isset($sitrep)) {
                //try to get it from Model's ResetLogicAttributes
                
                if (!method_exists($model, 'ResetLogicAttributes')) {
                    throw new Exception('no sitrep array and the class does not implement ResetLogicAttributes method', 1000, null);
                }
            }
            
            //go through the dirtyAttributes
            
            foreach($model->dirtyAttributes as $key=>$value) {
                //check if in sitrep
                foreach ($sitrep as $item) {
                    if (!is_array($item[0])) {
                        $attr = $item[0];
                        
                        if (count($item) === 4) {
                            //for now is just dif
                            if ($item[0] === $key && $model->$attr != $item[1]) {
                                self::ResetAttributes($model,$item[2]);
                                $model->markAttributeDirty($key);
                                break;
                            }
                        }
                        else
                        {
                            //($this->oldAttributes[$item[0]] !== $this->$item[0])
                            if ($item[0] === $key && $model->$attr == $item[1]) {
                                self::ResetAttributes($model, $item[2]);
                                $model->markAttributeDirty($key);
                                break;
                            }
                        }
                    }
                    else {
                        $bool = true;
                        foreach($item[0] as $operator) {
                            $bool = $bool && ($model->$operator == $item[1]);
                        }

                        if ($bool) {
                            self::ResetAttributes($model, $item[2]);
                        }
                    }
                }
            }
        }       //end function refreshAttributes
    
}
