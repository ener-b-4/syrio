<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\components\rules;

use yii\rbac\Rule;
use \app\models\User;
use app\models\events\EventDeclaration;

class UserInTheSameOORule extends Rule {
    
    public $name = 'user_in_the_same_oo';
    
    /**
     * @param string|integer $user the user ID.
     * @param Item $item the role or permission that this rule is associated with; actually is what you're asking on user 'can'
     * @param array $params parameters passed to ManagerInterface::checkAccess().
     * @return boolean a value indicating whether the rule permits the role or permission it is associated with.
     */    
    public function execute($user, $item, $params) {
        
        //$user_to_be_edited = key_exists('user', $params) ? $params['user'] : null;
        
        
        //echo '<pre>';
        //echo var_dump($user);
        //echo var_dump($item);
        //echo var_dump($params);
        //echo $item->name;
        //echo '</pre>';
        //die();
        
        
        switch ($item->name)
        {
            case 'oo-user-list':
                break;
            //(start) Author: vamanbo;  Date: 31/08/2015; On: allow installation rapporteurs to register a new event
            case 'incident_register_operators':
            case 'incident_view_operators':
                
                /*
                 * EXPECTED PARAMETERS:
                 * - event as FILLED_IN event_declaration
                 * 
                 * to register a new incident the user must:
                 * - be an organization_rapporteur
                 *      - be in the same organization as the installation he wants to register the incident for
                 * - be an installation_rapporteur
                 *      - be member of the same installation as the one reported
                 * 
                 * NOTES:
                 * 
                 * - this rule must be checked when SUBMITTING a NEW incident 
                 * - the access the new incident page one must pass the isOperator/isInstallation rapporteur
                 * 
                 */
                
                $event = isset($params['event']) ? $params['event'] : NULL;
                if (!isset($event)) {
                    throw new \yii\base\InvalidParamException('event missing');
                }

                $cUser = \Yii::$app->user->identity;
                
                if ($cUser->isInstallationUser)
                {
                    return ($cUser->inst_id == $event->installation_id && $cUser->installation->operator_id == $event->operator_id);
                }
                else
                {
                    //echo $cUser->org_id . ' - ' . $event->operator_id . '<br/>';
                    //echo $cUser->org_id == $event->operator_id;
                    //die();
                    return $cUser->org_id == $event->operator_id;
                }
                
                
                break;
            //(end) Author: vamanbo;  Date: 31/08/2015; On: allow installation rapporteurs to register a new event
            case 'incident_update_operators':
                //                        $event->status == \app\models\events\EventDeclaration::INCIDENT_REPORT_REJECTED ||
                /*
                 * EXPECTED PARAMETERS:
                 * - event as FILLED_IN event_declaration
                 * 
                 * to modify a new incident the user must:
                 * - be an organization_rapporteur
                 *      - be in the same organization as the installation he wants to register the incident for
                 * - be an installation_rapporteur
                 *      - be member of the same installation as the one reported
                 * - the incident status should NOT be submitted
                 * 
                 */
                
                /* @var $event \app\models\events\EventDeclaration */
                
                
                $event = isset($params['event']) ? $params['event'] : NULL;
                if (!isset($event)) {
                    throw new \yii\base\InvalidParamException('event missing');
                }

                if (
                        $event->status == \app\models\events\EventDeclaration::INCIDENT_REPORT_REPORTED ||
                        $event->status == \app\models\events\EventDeclaration::INCIDENT_REPORT_ACCEPTED ||
                        $event->status == \app\models\events\EventDeclaration::INCIDENT_REPORT_RESUBMITTED
                   )
                {
                    return false;
                }
                
                $cUser = \Yii::$app->user->identity;
                
                if ($cUser->isInstallationUser)
                {
                    return ($cUser->inst_id == $event->installation_id && $cUser->installation->operator_id == $event->operator_id);
                }
                else
                {
                    //echo $cUser->org_id . ' - ' . $event->operator_id . '<br/>';
                    //echo $cUser->org_id == $event->operator_id;
                    //die();
                    return $cUser->org_id == $event->operator_id;
                }
                
                break;
            case 'incident_delete_operators':
            case 'incident_purge_operators':
                /*
                 * EXPECTED PARAMETERS:
                 * - event as FILLED_IN event_declaration
                 * 
                 * to modify a new incident the user must:
                 * - be an organization_rapporteur
                 *      - be in the same organization as the installation he wants to register the incident for
                 * - be an installation_rapporteur
                 *      - be member of the same installation as the one reported
                 * - the incident status should NOT be submitted
                 * 
                 */
                
                /* @var $event \app\models\events\EventDeclaration */
                
                
                $event = isset($params['event']) ? $params['event'] : NULL;
                if (!isset($event)) {
                    throw new \yii\base\InvalidParamException('event missing');
                }

                if (    $event->status == \app\models\events\EventDeclaration::INCIDENT_REPORT_REJECTED ||
                        $event->status == \app\models\events\EventDeclaration::INCIDENT_REPORT_REPORTED ||
                        $event->status == \app\models\events\EventDeclaration::INCIDENT_REPORT_ACCEPTED ||
                        $event->status == \app\models\events\EventDeclaration::INCIDENT_REPORT_RESUBMITTED
                   )
                {
                    return false;
                }
                
                $cUser = \Yii::$app->user->identity;
                
                if ($cUser->isInstallationUser)
                {
                    return ($cUser->inst_id == $event->installation_id && $cUser->installation->operator_id == $event->operator_id);
                }
                else
                {
                    //echo $cUser->org_id . ' - ' . $event->operator_id . '<br/>';
                    //echo $cUser->org_id == $event->operator_id;
                    //die();
                    return $cUser->org_id == $event->operator_id;
                }
                
                break;
            case 'incident_draft_create_operators':
                /*
                 * to create a drafts of an incident the user must:
                 * - be ACTIVE (NO because an non-active user CANNOT be logged in)
                 * - be in the same OO organization as the one defined in the event-declaration of the draft
                 */

                /*
                 * this one expects params['event'] to be set as the current $event
                 */
                $event = isset($params['event']) ? $params['event'] : NULL;
                if (!isset($event)) {
                    throw new \yii\base\InvalidParamException();
                }

                $cUser = \Yii::$app->user->identity;
                
                if ($cUser->isInstallationUser)
                {
                    return ($cUser->installation->id == $event->installation_id && $cUser->installation->operator_id == $event->operator_id);
                }
                else
                {
                    return $cUser->org_id == $event->operator_id;
                }
                
                
                break;
            case 'incident_draft_list_operators':
                /*
                 * to list the drafts of an incident the user must:
                 * - be ACTIVE (NO because an non-active user CANNOT be logged in)
                 * - be in the same OO organization as the one defined in the event-declaration of the draft
                 */

                /*
                 * this one expects params['event'] to be set as the current $event
                 */
                
                $event = isset($params['event']) ? $params['event'] : NULL;
                if (!isset($event)) {
                    throw new \yii\base\InvalidParamException();
                }

                $cUser = \Yii::$app->user->identity;
                
                if ($cUser->isInstallationUser)
                {
                    return ($cUser->installation->id == $event->installation_id && $cUser->installation->operator_id == $event->operator_id);
                }
                else
                {
                    return $cUser->org_id == $event->operator_id;
                }
                
                
                break;
            case 'incident_draft_view_operators':
                /*
                 * to view the incident draft the user must:
                 * - be ACTIVE (NO because an non-active user CANNOT be logged in)
                 * - be in the same OO organization as the one defined in the event-declaration of the draft
                 */

                /*
                 * this one expects params['draft'] to be set as the current $event_draft
                 */
                
                $draft = isset($params['draft']) ? $params['draft'] : NULL;
                if (!isset($draft)) {
                    throw new \yii\base\InvalidParamException();
                }

                $cUser = \Yii::$app->user->identity;
                
                if ($cUser->isInstallationUser)
                {
                    return ($cUser->installation->id == $draft->event->installation_id && $cUser->installation->operator_id == $draft->event->operator_id);
                }
                else
                {
                    return $cUser->org_id == $draft->event->operator_id;
                }
                
                break;
            case 'incident_draft_delete_operators':
                /* @var $draft \app\models\events\drafts\EventDraft */
                
                $draft = isset($params['draft']) ? $params['draft'] : NULL;
                if (!isset($draft)) {
                    throw new \yii\base\InvalidParamException();
                }

                /* first check the ca_status:
                 * > a draft cannot be deleted as soon as the CA is aware of it
                 */
                if ($draft->ca_status == EventDeclaration::INCIDENT_REPORT_REJECTED ||
                        $draft->ca_status == EventDeclaration::INCIDENT_REPORT_REPORTED ||
                        $draft->ca_status == EventDeclaration::INCIDENT_REPORT_RESUBMITTED ||
                        $draft->ca_status == EventDeclaration::INCIDENT_REPORT_ACCEPTED)
                {
                    return null;
                }

                $cUser = \Yii::$app->user->identity;
                
                if ($cUser->isInstallationUser)
                {
                    if ($draft->ca_status == EventDeclaration::INCIDENT_REPORT_SIGNED)
                    {
                        return false;
                    }
                    $ismine = ($cUser->installation->id == $draft->event->installation_id && $cUser->installation->operator_id == $draft->event->operator_id);
                    $ca_stat = !isset($model->ca_status);
                    
                    return $ismine && $ca_stat;
                }
                else
                {
                    $is_mine = $cUser->org_id == $draft->event->operator_id;
                }
                break;
            case 'incident_draft_purge_operators':
                /* @var $draft \app\models\events\drafts\EventDraft */
                
                $draft = isset($params['draft']) ? $params['draft'] : NULL;
                if (!isset($draft)) {
                    throw new \yii\base\InvalidParamException();
                }

                /* 
                 * > a draft cannot be purged if not first deleted
                 */
                if ($draft->del_status != EventDeclaration::STATUS_DELETED)
                {
                    return false;
                }

                $cUser = \Yii::$app->user->identity;
                
                if ($cUser->isInstallationUser)
                {
                    if ($draft->ca_status == EventDeclaration::INCIDENT_REPORT_SIGNED)
                    {
                        return false;
                    }
                    return ($cUser->installation->id == $draft->event->installation_id && $cUser->installation->operator_id == $draft->event->operator_id);
                }
                else
                {
                    return $cUser->org_id == $draft->event->operator_id;
                }
                break;
            case 'incident_draft_sign_operators':
            {
                /* @var $draft \app\models\events\drafts\EventDraft */
                
                $draft = isset($params['draft']) ? $params['draft'] : NULL;
                if (!isset($draft)) {
                    throw new \yii\base\InvalidParamException('draft must be provided');
                }

                /* 
                 * > a draft cannot be signed if it is deleted
                 * > a draft can be signed:
                 *      - only by int_rap and oo_raps;
                 *      - only if it's finalized;
                 *      - only if it belongs to an event the user has access to
                 */
                if ( $draft->del_status == EventDeclaration::STATUS_DELETED ||
                     $draft->status != EventDeclaration::INCIDENT_REPORT_FINALIZED
                   ) { return false; }

                $cUser = \Yii::$app->user->identity;
                
                if ($cUser->isInstallationUser && \Yii::$app->user->can('inst_rapporteur'))
                {                    
                    return ($cUser->installation->id == $draft->event->installation_id && $cUser->installation->operator_id == $draft->event->operator_id);
                }
                elseif ($cUser->isOperatorUser && \Yii::$app->user->can('op_raporteur'))
                {
                    return $cUser->org_id == $draft->event->operator_id;
                }
                
            }
            case 'incident_draft_reopen_operators':
            {
                /* @var $draft \app\models\events\drafts\EventDraft */
                
                $draft = isset($params['draft']) ? $params['draft'] : NULL;
                if (!isset($draft)) {
                    throw new \yii\base\InvalidParamException('draft must be provided');
                }

                /* 
                 * > a draft cannot be signed if it is deleted
                 * > a draft can be signed:
                 *      - only by int_rap and oo_raps;
                 *      - only if it's finalized;
                 *      - only if it belongs to an event the user has access to
                 */
                if ( $draft->del_status == EventDeclaration::STATUS_DELETED ||
                     ($draft->status != EventDeclaration::INCIDENT_REPORT_FINALIZED && $draft->status != EventDeclaration::INCIDENT_REPORT_SIGNED)
                   ) { return false; }

                /* first check the ca_status:
                 * > a draft cannot be reoped unless the CA is aware of it
                 */
                if (    $draft->ca_status == EventDeclaration::INCIDENT_REPORT_REPORTED ||
                        $draft->ca_status == EventDeclaration::INCIDENT_REPORT_RESUBMITTED ||
                        $draft->ca_status == EventDeclaration::INCIDENT_REPORT_ACCEPTED)
                {
                    return null;
                }
                   
                   
                   
                $cUser = \Yii::$app->user->identity;
                
                if ($cUser->isInstallationUser && \Yii::$app->user->can('inst_rapporteur'))
                {                    
                    return ($cUser->installation->id == $draft->event->installation_id && $cUser->installation->operator_id == $draft->event->operator_id);
                }
                elseif ($cUser->isOperatorUser && \Yii::$app->user->can('op_raporteur'))
                {
                    return $cUser->org_id == $draft->event->operator_id;
                }
                
                break;
            }
            case 'incident_draft_section_edit_operators': {
                /* @var $draft Section */
                $section = isset($params['section']) ? $params['section'] : NULL;
                if (!isset($section)) {
                    throw new \yii\base\InvalidParamException('section must be provided');
                }
                
                $draft = $section->eventDraft;
                
                /* 
                 * > a section cannot be reopened if
                 *      - only by int_rap and oo_raps;
                 *      - only if it's finalized;
                 *      - only if it belongs to an event the user has access to
                 */
                if ( $section->status >= EventDeclaration::INCIDENT_REPORT_FINALIZED) { return false; }

                /* first check the ca_status:
                 * > a draft cannot be reoped unless the CA is aware of it
                 */
                if (    $draft->ca_status == EventDeclaration::INCIDENT_REPORT_REPORTED ||
                        $draft->ca_status == EventDeclaration::INCIDENT_REPORT_RESUBMITTED ||
                        $draft->ca_status == EventDeclaration::INCIDENT_REPORT_ACCEPTED)
                {
                    return null;
                }
                   
                   
                   
                $cUser = \Yii::$app->user->identity;
                
                if ($cUser->isInstallationUser && \Yii::$app->user->can('inst_rapporteur'))
                {                    
                    return ($cUser->installation->id == $draft->event->installation_id && $cUser->installation->operator_id == $draft->event->operator_id);
                }
                elseif ($cUser->isOperatorUser && \Yii::$app->user->can('op_raporteur'))
                {
                    return $cUser->org_id == $draft->event->operator_id;
                }
                break;
            }
            case 'incident_draft_section_reopen_operators':
            {
                /* @var $draft Section */
                $section = isset($params['section']) ? $params['section'] : NULL;
                if (!isset($section)) {
                    throw new \yii\base\InvalidParamException('section must be provided');
                }
                
                $draft = $section->eventDraft;
                
                /* 
                 * > a section cannot be reopened if
                 *      - only by int_rap and oo_raps;
                 *      - only if it's finalized;
                 *      - only if it belongs to an event the user has access to
                 */
                if ( $section->status < EventDeclaration::INCIDENT_REPORT_FINALIZED) { return false; }

                /* first check the ca_status:
                 * > a draft cannot be reoped unless the CA is aware of it
                 */
                if (    $draft->ca_status == EventDeclaration::INCIDENT_REPORT_REPORTED ||
                        $draft->ca_status == EventDeclaration::INCIDENT_REPORT_RESUBMITTED ||
                        $draft->ca_status == EventDeclaration::INCIDENT_REPORT_ACCEPTED)
                {
                    return null;
                }
                   
                   
                   
                $cUser = \Yii::$app->user->identity;
                
                if ($cUser->isInstallationUser && \Yii::$app->user->can('inst_rapporteur'))
                {                    
                    return ($cUser->installation->id == $draft->event->installation_id && $cUser->installation->operator_id == $draft->event->operator_id);
                }
                elseif ($cUser->isOperatorUser && \Yii::$app->user->can('op_raporteur'))
                {
                    return $cUser->org_id == $draft->event->operator_id;
                }
                break;
            }
            
            case 'incident_submit_operators':
            {

                /* @var $draft \app\models\events\drafts\EventDraft */
                $draft = isset($params['draft']) ? $params['draft'] : NULL;
                if (!isset($draft)) {
                    throw new \yii\base\InvalidParamException('draft must be provided');
                }

                /* 
                 * > an incident can be submitted to the ca if
                 *      - it is not deleted
                 *      - only by int_rap and oo_raps;
                 *      - only if it's signed;
                 *      - only if it belongs to an event the user has access to
                 *      - only if it hasn't been submitted before
                 */
                if ( $draft->del_status == EventDeclaration::STATUS_DELETED ||
                     $draft->status != EventDeclaration::INCIDENT_REPORT_SIGNED
                   ) { return false; }


                /* first check the ca_status:
                 * > a draft cannot be reoped unless the CA is aware of it
                 */
                if (    $draft->ca_status == EventDeclaration::INCIDENT_REPORT_REPORTED ||
                        $draft->ca_status == EventDeclaration::INCIDENT_REPORT_RESUBMITTED ||
                        $draft->ca_status == EventDeclaration::INCIDENT_REPORT_ACCEPTED)
                {
                    return null;
                }                   
                   
                $cUser = \Yii::$app->user->identity;
                
                if ($cUser->isInstallationUser && \Yii::$app->user->can('inst_rapporteur'))
                {                    
                    return ($cUser->installation->id == $draft->event->installation_id && $cUser->installation->operator_id == $draft->event->operator_id);
                }
                elseif ($cUser->isOperatorUser && \Yii::$app->user->can('op_raporteur'))
                {
                    return $cUser->org_id == $draft->event->operator_id;
                }
                
                /* check if not submitted before */
                if (isset($draft->event->report) && $draft->event->status !== EventDeclaration::INCIDENT_REPORT_REJECTED)
                {
                    return false;
                }
                
                break;
            }
            
            case 'clear_history_operators':
                $event = isset($params['event']) ? $params['event'] : NULL;
                if (!isset($event)) {
                    throw new \yii\base\InvalidParamException('event missing');
                }
                
                $cUser = \Yii::$app->user->identity;
                
                if ($cUser->isInstallationUser && \Yii::$app->user->can('inst_rapporteur'))
                {                    
                    return ($cUser->installation->id == $event->installation_id && $cUser->installation->operator_id == $event->operator_id);
                }
                elseif ($cUser->isOperatorUser && \Yii::$app->user->can('op_raporteur'))
                {
                    return $cUser->org_id == $event->operator_id;
                }
                
                break;
            default:
                return false;
        }
                
        if ($item->name = 'oo-user-list')
        {
            /*
             * CHECK: current user is OO
             */
            return \Yii::$app->user->identity->isOperatorUser;
        }
        
        /*
         * CHECKS: 
         */
        
        //check if the user to be edited is CaUser;
        //if (!isset($user_to_be_edited)) { return false; }
        //if (!$user_to_be_edited->isOperatorUser) { return false; }
        //if (!\Yii::$app->user->getIdentity()->isOperatorUser) {return false; }
        
        //check if the user to be edited AND the current user belong to the same Organization
        
        //get the country of the current user
        //return \Yii::$app->user->identity->organization->id == $user_to_be_edited->organization->id;
    }
}

/*20150920*/