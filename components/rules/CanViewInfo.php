<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\components\rules;

use yii\rbac\Rule;
use \app\models\User;

class CanViewInfo extends Rule {
    
    public $name = 'can_view_info';
    
    /**
     * @param string|integer $user the user ID.
     * @param Item $item the role or permission that this rule is associated with; actually is what you're asking on user 'can'
     * @param array $params parameters passed to ManagerInterface::checkAccess().
     * @return boolean a value indicating whether the rule permits the role or permission it is associated with.
     */    
    public function execute($user, $item, $params) {
        
        //get the permission name

        switch($item->name) {
            case 'display_info':
                if (\Yii::$app->user->can('sys_admin')) {
                    return true;
                }
                
                $user_id = isset($params['id']) ? $params['id'] : NULL;
                if (!isset($user_id)) {
                    throw new \yii\base\InvalidParamException('user to be seen id missing');
                }
                
                $current_user = \Yii::$app->user->identity;
                
                /*
                 * the ca users can see all the users in his jurisdiction
                 * the operator users can see only the users in their organization and installations
                 * the installation users can only see the users in their organization and installation
                 */
                
                if ($current_user->isCaUser) {
                    return true;
                } elseif ($current_user->isOperatorUser) {
                    /* check if requested user is in the same organization as he is */
                    $to_be_displayed = User::findOne($user_id);
                    return $to_be_displayed->org_id === $current_user->org_id;
                } elseif ($current_user->isInstallationUser) {
                    /* check if requested user is in the same installation and organization as he is */
                    $to_be_displayed = User::findOne($user_id);
                    return 
                        $to_be_displayed->org_id === $current_user->org_id && 
                        ($to_be_displayed->inst_id === $current_user->inst_id || !isset($to_be_displayed->inst_id));
                }
                
                break;
        }
        
        
        /*
         * SECOND CHECKS: 
         */
        
        $user_to_be_edited = User::findOne($user_to_be_edited_id);

        //check if the user to be edited is CaUser;
        if (!isset($user_to_be_edited)) { return false; }
        if (!isset($user_to_be_edited->organization)) { return false; }
        if (!$user_to_be_edited->isCaUser) { return false; }
        if (!\Yii::$app->user->getIdentity()->isCaUser) {return false; }
        
        //check if the user to be edited AND the current user belong to the same CA
        
        //get the country of the current user
        $c_user_country = \Yii::$app->user->getIdentity()->organization->country;

        //get user's to be edited country
        $user_to_be_edited_country = $user_to_be_edited->organization->country;
        
        return $c_user_country === $user_to_be_edited_country;
    }
}