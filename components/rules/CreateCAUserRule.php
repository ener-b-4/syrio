<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\components\rules;

use yii\rbac\Rule;
use \app\models\User;

class CreateCAUserRule extends Rule {
    
    public $name = 'user_is_ca';
    
    /**
     * @param string|integer $user the user ID.
     * @param Item $item the role or permission that this rule is associated with; actually is what you're asking on user 'can'
     * @param array $params parameters passed to ManagerInterface::checkAccess().
     * @return boolean a value indicating whether the rule permits the role or permission it is associated with.
     */    
    public function execute($user, $item, $params) {
        /*
         * Only a Competent Authority user can create a CA user
         */
        
        return \Yii::$app->user->can('sys_admin') || \Yii::$app->user->identity->isCaUser;
    }
}