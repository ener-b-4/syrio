<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\components\rules;

use yii\rbac\Rule;
use \app\models\User;

class UserInTheSameCARule extends Rule {
    
    public $name = 'user_in_the_same_ca';
    
    /**
     * @param string|integer $user the user ID.
     * @param Item $item the role or permission that this rule is associated with; actually is what you're asking on user 'can'
     * @param array $params parameters passed to ManagerInterface::checkAccess().
     * @return boolean a value indicating whether the rule permits the role or permission it is associated with.
     */    
    public function execute($user, $item, $params) {
        
        //get the permission name
        $p_name = $item->name;
        
        $user_to_be_edited_id = key_exists('id', $params) ? $params['id'] : -1;
        
        
//        if ($item->name = 'ca-user-create')
//        {
//            /*
//             * FIRST CHECK: both user_id and organization_id are null
//             * 
//             * This corresponds to a new user situation, so it should return True
//             */
//            if ($user_to_be_edited_id == -1 && $user_organization_id == -1)
//            {
//                //echo 'here';
//                //die();
//                return true;
//            }
//        }
//        else
//        {
//            if ($user_to_be_edited_id == -1 || $user_organization_id == -1)
//            {
//                return false;
//            }
//        }

        if ($user_to_be_edited_id == -1)
        {
            return false;
        }
        
        
        /* regardless of the permission name, do the rest
         * 
         */
        
        /*
         * SECOND CHECKS: 
         */
        
        $user_to_be_edited = User::findOne($user_to_be_edited_id);

        //check if the user to be edited is CaUser;
        if (!isset($user_to_be_edited)) { return false; }
        if (!isset($user_to_be_edited->organization)) { return false; }
        if (!$user_to_be_edited->isCaUser) { return false; }
        if (!\Yii::$app->user->getIdentity()->isCaUser) {return false; }
        
        //check if the user to be edited AND the current user belong to the same CA
        
        //get the country of the current user
        $c_user_country = \Yii::$app->user->getIdentity()->organization->country;

        //get user's to be edited country
        $user_to_be_edited_country = $user_to_be_edited->organization->country;
        
        return $c_user_country === $user_to_be_edited_country;
    }
}