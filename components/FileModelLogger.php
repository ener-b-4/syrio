<?php

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

namespace app\components;

/**
 * Description of FileModelLogger
 *
 * @author vamanbo
 */
class FileModelLogger {
    //put your code here
    
    public static function LogAttributes($model, $file_name=null) {
        
        if (!YII_DEBUG) {
        //open / create the file
        
        $url = isset($file_name) ? \Yii::getAlias(@web).$file_name.'.csv' : \Yii::getAlias(@web).'test.csv';
        //$url = \Yii::getAlias(@web).'test.csv';
        
        $myfile = fopen($url, 'w');
        
        //get all the properties (inclusing the old ones) of the model
        $line = ['$key', '$value', '$model->oldAttributes[$key]'];
        fputcsv($myfile, $line);

        foreach ($model->attributes as $key=>$value) {
            if (!is_array($value)) { $line = [$key, $value, isset($model->oldAttributes[$key]) ? $model->oldAttributes[$key] : '']; }
            
            fputcsv($myfile, $line);
            
            //$line = $key.'\t'.$value.'\t'.$model->oldAttributes[$key].'\n';
            //fwrite($myfile, $line);
        }
        
        //close the file
        fclose($myfile);
        };
        
        return true;
    }

    
    
    public static function LogDirtyAttributes($model, $file_name=null) {
        //open / create the file
        
        $url = isset($file_name) ? \Yii::getAlias(@web).$file_name.'.csv' : \Yii::getAlias(@web).'dirty_attr.csv';
        //$url = \Yii::getAlias(@web).'test.csv';
        
        $myfile = fopen($url, 'w');
        
        //get all the properties (inclusing the old ones) of the model
        $line = ['key', 'value'];
        fputcsv($myfile, $line);

        foreach ($model->dirtyAttributes as $key=>$value) {
            if (!is_array($value)) { $line = [$key, $value]; }
            
            fputcsv($myfile, $line);
            
            //$line = $key.'\t'.$value.'\t'.$model->oldAttributes[$key].'\n';
            //fwrite($myfile, $line);
        }
        
        //close the file
        fclose($myfile);
        
        return true;
    }
    
}
