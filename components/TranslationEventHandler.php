<?php
namespace app\components;

/**
 * @copyright Copyright (c) 2016 DG ENER
 * @license https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11
 */

class TranslationEventHandler
{
    public static function handleMissingTranslation(\yii\i18n\MissingTranslationEvent $event) {
        if (YII_ENV_DEV) {
            $event->translatedMessage = "@MISSING: {$event->category}.{$event->message} FOR $event->language@";
            
            //salveaza in messages noul mesaj
            
            //check if already registered as source message
            if (!\app\modules\translations\models\TSourceMessage::findOne(['message' => $event->message])) {
                $new_t_message = new \app\modules\translations\models\TSourceMessage();
                $new_t_message->category = $event->category;
                $new_t_message->message = $event->message;
                $new_t_message->save();
            }
        }
        else
        {
            //$event->translatedMessage = "@MISSING: {$event->category}.{$event->message} FOR $event->language@";
        }
    }
}
