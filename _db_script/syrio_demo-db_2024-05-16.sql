-- phpMyAdmin SQL Dump
-- version 4.4.15.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1:3307
-- Generation Time: May 16, 2024 at 09:57 AM
-- Server version: 5.5.46
-- PHP Version: 5.5.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `syrio_demo`
--
CREATE DATABASE IF NOT EXISTS `syrio_demo` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `syrio_demo`;

DELIMITER $$
--
-- Procedures
--
DROP PROCEDURE IF EXISTS `sp_utl_CreateIndex`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_utl_CreateIndex`(

    IN given_database 		VARCHAR(64),

    IN given_table    		VARCHAR(64),

    IN given_index    		VARCHAR(64),

    IN given_columns  		VARCHAR(64),

	OUT take_create_index	VARCHAR(255)

	

)
BEGIN

 

    DECLARE IndexIsThere INTEGER;

    SELECT COUNT(1) INTO IndexIsThere

    FROM INFORMATION_SCHEMA.STATISTICS

    WHERE table_schema  = given_database

    AND   table_name    = given_table

    AND   index_name    = given_index;

 

   IF IndexIsThere = 0 THEN

        SET @sqlstmt = CONCAT('CREATE INDEX ',given_index,' ON ',

        given_database,'.',given_table,' (',given_columns,')');

        PREPARE st FROM @sqlstmt;

        EXECUTE st;

        DEALLOCATE PREPARE st;

        SELECT CONCAT('Created index ', given_table,'.', given_index, ' on columns ', given_columns)

                -- AS 'CreateIndex status' ;

				INTO take_create_index;

    ELSE

        SELECT CONCAT('Index ',given_index,' Already Exists on Table ', given_database,'.',given_table)

                AS 'CreateIndex status';

		SET take_create_index = null;

    END IF;

 

END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `a1_release_causes`
--

DROP TABLE IF EXISTS `a1_release_causes`;
CREATE TABLE `a1_release_causes` (
  `a1_id` int(11) NOT NULL,
  `leak_cause_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='junction table for release causes';

-- --------------------------------------------------------

--
-- Table structure for table `auth_assignment`
--

DROP TABLE IF EXISTS `auth_assignment`;
CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auth_assignment`
--

INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('ca_admin', 90, 1454683377),
('ca_assessor', 90, 1454683377),
('ca_user', 91, 1454683424),
('inst_rapporteur', 93, 1454955823),
('inst_rapporteur', 95, 1454684449),
('op_admin', 92, 1454684318),
('op_admin', 94, 1454684384),
('op_raporteur', 92, 1454684318),
('op_raporteur', 94, 1454684384),
('sys_admin', 89, 1454604715);

-- --------------------------------------------------------

--
-- Table structure for table `auth_item`
--

DROP TABLE IF EXISTS `auth_item`;
CREATE TABLE `auth_item` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `rule_name` varchar(64) DEFAULT NULL,
  `data` text,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auth_item`
--

INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('ca-delete', 2, 'Allows the user to delete a Competent Authority', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('ca-list', 2, 'Allows the user to list the Competent Authorities', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('ca-org-create', 2, 'Allows the user to create a Competent Authority Organization', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('ca-org-delete', 2, 'Allows the user to delete a Competent Authority Organization', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('ca-org-purge', 2, 'Allows the user to purge (permanently delete) a Competent Authority Organization', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('ca-org-update', 2, 'Allows the user to modify a Competent Authority Organization', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('ca-purge', 2, 'Allows the user to purge (permanently delete) a Competent Authority', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('ca-register', 2, 'Allows registering a new Competent Authority', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('ca-user-create', 2, 'allows creation of a new Competent Authority user', 'user_is_ca', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('ca-user-delete', 2, 'allows deletion of a Competent Authority user', 'user_in_the_same_ca', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('ca-user-edit', 2, 'allows updating the details of a Competent Authority User', 'user_in_the_same_ca', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('ca-user-list', 2, 'allows the listing of Competent Authority Users', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('ca-user-purge', 2, 'allows the PERMANENT deletion of a CA user', 'user_in_the_same_ca', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('ca-user-view', 2, 'allows the display of the Competent Authority Users', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('ca_', 1, 'Generic role that signals that the user is Competent Authority user', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('ca_admin', 1, 'Competent Authority Administrator', NULL, NULL, '2015-02-02 11:24:44', '0000-00-00 00:00:00'),
('ca_assessor', 1, 'User that can assess the reports sent by the operators/owners', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('ca_generate_annual_report', 2, 'allows the user to generate the Annual Report', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('ca_user', 1, 'Competent Authority user', NULL, NULL, '2015-01-28 13:14:37', NULL),
('clear_history', 2, 'allows clearing the reporting history', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('clear_history_operators', 2, 'allows (enforced by rule) the operators to clear the reporting history', 'user_in_the_same_oo', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('display_info', 2, 'checks if a particular user can view the information on some asset', 'can_view_info', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('edit-user', 2, 'allows the admin to edit any user in the SyRIO system', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('edit-user-self', 2, 'allows the user to edit (some) of his data', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('events_assess', 2, 'assess an event and declare it as serious', NULL, NULL, '2014-12-10 14:18:24', NULL),
('full_access', 2, 'adminaccess', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('holiday_crud', 2, 'Allows changes in the holidays', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('incident_delete', 2, 'allows deleting an event (by the operator)', NULL, NULL, '2014-12-10 14:18:24', NULL),
('incident_delete_operators', 2, 'allows the user to delete (can be undone, enforced by the rule) the event', 'user_in_the_same_oo', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('incident_description_modify', 2, 'allows modifying incident description', NULL, NULL, '2014-12-10 14:18:24', NULL),
('incident_description_view', 2, 'allows viewing incident description', NULL, NULL, '2014-12-10 14:18:24', NULL),
('incident_draft_create', 2, 'allows creating a new incident report draft', NULL, NULL, '2014-12-10 14:18:24', NULL),
('incident_draft_create_operators', 2, 'allows creation of a new incident draft (enforced by rule)', 'user_in_the_same_oo', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('incident_draft_delete', 2, 'allows deleting a draft', NULL, NULL, '2014-12-10 14:18:24', NULL),
('incident_draft_delete_operators', 2, 'allows (by enforcing the rule) deleting a draft', 'user_in_the_same_oo', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('incident_draft_list', 2, 'allows the user to list the drafts of a given Event', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('incident_draft_list_operators', 2, 'allows (by enforcing the rule) the operators to list the drafts of a given event', 'user_in_the_same_oo', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('incident_draft_purge', 2, 'allows the user to purge (permanently delete) a draft', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('incident_draft_purge_operators', 2, 'allows the user to purge (enforced by rule) an incident report', 'user_in_the_same_oo', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('incident_draft_reopen', 2, 'allows the user to reopen a draft', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('incident_draft_reopen_operators', 2, 'allows the user to reopen (rule enforced) a draft', 'user_in_the_same_oo', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('incident_draft_section_edit', 2, 'allows editing a draft section', NULL, NULL, '2014-12-10 14:18:24', NULL),
('incident_draft_section_edit_operators', 2, 'allows editing (rule enforced) a draft section', 'user_in_the_same_oo', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('incident_draft_section_reopen', 2, 'allows a user to reopen a draft section', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('incident_draft_section_reopen_operators', 2, 'allows an operator (reinforced by rule) to reopen a draft section', 'user_in_the_same_oo', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('incident_draft_sign', 2, 'allows signing the incident report', NULL, NULL, '2014-12-10 14:18:24', NULL),
('incident_draft_sign_operators', 2, 'allows (by enforcing the rule) signing the incident report', 'user_in_the_same_oo', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('incident_draft_view', 2, 'allows displaying the incident draft', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('incident_draft_view_ca', 2, 'allows the CA users to view an incident report ONLY when it is submitted and or assessed by them', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('incident_draft_view_operators', 2, 'allows (enforces by the rule) the operators to view ONLY their incidents', 'user_in_the_same_oo', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('incident_list', 2, 'allows the user to list the incidents', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('incident_purge', 2, 'allows the user to purge (permanently delete) an event', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('incident_purge_operators', 2, 'allows the user to purge (permanently delete) an event', 'user_in_the_same_oo', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('incident_register', 2, 'register a new event', NULL, NULL, '2014-12-10 14:18:24', NULL),
('incident_register_operators', 2, 'allows (restricts by rule) registering a new incident', 'user_in_the_same_oo', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('incident_submit', 2, 'allows submitting a signed incident to the CA', NULL, NULL, '2014-12-10 14:18:24', NULL),
('incident_submit_operators', 2, 'allows submitting an incident (enforced by rule) to the CA', 'user_in_the_same_oo', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('incident_update', 2, 'allows the user to make changes to the event', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('incident_update_operators', 2, 'allows the user (enforced by rule) to edit the incident', 'user_in_the_same_oo', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('incident_view', 2, 'allows viewing an incident', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('incident_view_operators', 2, 'allows viewing (enforced by rule) an incident', 'user_in_the_same_oo', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('inst_admin', 1, 'offshore installation administrator', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('inst_rapporteur', 1, 'offshore installation rapporteur', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('inst_user', 1, 'offshore installation user', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('oo-inst-create', 2, 'Allows the user to create a new installation', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('oo-inst-delete', 2, 'Allows the user to delete an installation', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('oo-inst-list', 2, 'allows the listing of the installations', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('oo-inst-purge', 2, 'Allows a user to purge (permanently deleting) an installation', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('oo-inst-update', 2, 'Allows the user to update an installation', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('oo-org-create', 2, 'Allows the user to create a Operator/Owners Organization (Company)', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('oo-org-delete', 2, 'Allows the user to delete an Operators / Owners Organization (Company)', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('oo-org-purge', 2, 'Allows the purge (permanent deletion) of an Operators / Owners Organization', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('oo-org-update', 2, 'Allows the user to modify an Operator / Owners Organization (Company)', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('oo-user-create', 2, 'allows creation of a new Operators - Owners user', 'user_is_oo', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('oo-user-edit', 2, 'allows updating the details of a Operators / Owners User', 'user_in_the_same_oo', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('oo-user-list', 2, 'allows displaying the list of OO Organization', 'user_in_the_same_oo', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('op_admin', 1, 'operator administrator', NULL, NULL, '2015-01-28 13:15:23', NULL),
('op_raporteur', 1, 'offshore operator Raporteur', NULL, NULL, '2015-02-02 10:57:24', NULL),
('op_user', 1, 'off-shore operator user', NULL, NULL, '2014-12-10 13:17:57', NULL),
('organization-create', 2, 'allows creation of an organization', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('organization-delete', 2, 'allows the user to delete an organization', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('organization-list', 2, 'allows listing the organizations', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('process_request', 2, 'allows the user to process a request', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('process_request_operators', 2, 'allows a user (enforced by rule) to process a request', 'user_in_the_same_oo', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('sys_admin', 1, 'The administrator of the SyRIO system', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('translate_app', 2, 'allows user to create/modify application translations', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('units-create', 2, 'allows the user to create a new unit', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('units-edit', 2, 'allows the user to modify a unit', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('units-list', 2, 'allows the user to list the units registered with SyRIO', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('user-create', 2, 'allows creation of a new user', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('user-delete', 2, 'allows deletion of a user', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('user-edit', 2, 'allows user information to be edited', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('user-list', 2, 'allows listing the users', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('user-purge', 2, 'allows the PERMANENT deletion of an user', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('user-view', 2, 'allows displaying the user information', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `auth_item_child`
--

DROP TABLE IF EXISTS `auth_item_child`;
CREATE TABLE `auth_item_child` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auth_item_child`
--

INSERT INTO `auth_item_child` (`parent`, `child`) VALUES
('full_access', 'ca-delete'),
('sys_admin', 'ca-delete'),
('full_access', 'ca-list'),
('sys_admin', 'ca-list'),
('ca_admin', 'ca-org-create'),
('full_access', 'ca-org-create'),
('sys_admin', 'ca-org-create'),
('ca_admin', 'ca-org-delete'),
('full_access', 'ca-org-delete'),
('sys_admin', 'ca-org-delete'),
('ca_admin', 'ca-org-purge'),
('full_access', 'ca-org-purge'),
('sys_admin', 'ca-org-purge'),
('ca_admin', 'ca-org-update'),
('full_access', 'ca-org-update'),
('sys_admin', 'ca-org-update'),
('full_access', 'ca-purge'),
('sys_admin', 'ca-purge'),
('full_access', 'ca-register'),
('sys_admin', 'ca-register'),
('ca_admin', 'ca-user-create'),
('ca_admin', 'ca-user-delete'),
('ca_admin', 'ca-user-edit'),
('sys_admin', 'ca-user-edit'),
('ca_', 'ca-user-list'),
('ca_admin', 'ca-user-purge'),
('ca_', 'ca-user-view'),
('ca_assessor', 'ca_'),
('ca_user', 'ca_'),
('sys_admin', 'ca_admin'),
('ca_assessor', 'ca_generate_annual_report'),
('ca_admin', 'ca_user'),
('ca_assessor', 'ca_user'),
('clear_history_operators', 'clear_history'),
('inst_rapporteur', 'clear_history_operators'),
('ca_user', 'display_info'),
('inst_user', 'display_info'),
('ca_admin', 'events_assess'),
('sys_admin', 'full_access'),
('ca_admin', 'holiday_crud'),
('sys_admin', 'holiday_crud'),
('incident_delete_operators', 'incident_delete'),
('sys_admin', 'incident_delete'),
('inst_rapporteur', 'incident_delete_operators'),
('op_user', 'incident_description_view'),
('incident_draft_create_operators', 'incident_draft_create'),
('op_user', 'incident_draft_create'),
('inst_rapporteur', 'incident_draft_create_operators'),
('incident_draft_delete_operators', 'incident_draft_delete'),
('inst_rapporteur', 'incident_draft_delete_operators'),
('incident_draft_list_operators', 'incident_draft_list'),
('sys_admin', 'incident_draft_list'),
('inst_user', 'incident_draft_list_operators'),
('incident_draft_purge_operators', 'incident_draft_purge'),
('inst_rapporteur', 'incident_draft_purge_operators'),
('incident_draft_reopen_operators', 'incident_draft_reopen'),
('inst_rapporteur', 'incident_draft_reopen_operators'),
('incident_draft_section_edit_operators', 'incident_draft_section_edit'),
('inst_user', 'incident_draft_section_edit_operators'),
('incident_draft_section_reopen_operators', 'incident_draft_section_reopen'),
('inst_rapporteur', 'incident_draft_section_reopen_operators'),
('incident_draft_sign_operators', 'incident_draft_sign'),
('inst_rapporteur', 'incident_draft_sign_operators'),
('incident_draft_view_operators', 'incident_draft_view'),
('sys_admin', 'incident_draft_view'),
('inst_user', 'incident_draft_view_operators'),
('inst_user', 'incident_list'),
('incident_purge_operators', 'incident_purge'),
('sys_admin', 'incident_purge'),
('inst_rapporteur', 'incident_purge_operators'),
('incident_register_operators', 'incident_register'),
('inst_rapporteur', 'incident_register_operators'),
('incident_submit_operators', 'incident_submit'),
('inst_rapporteur', 'incident_submit_operators'),
('incident_update_operators', 'incident_update'),
('sys_admin', 'incident_update'),
('inst_rapporteur', 'incident_update_operators'),
('incident_view_operators', 'incident_view'),
('sys_admin', 'incident_view'),
('inst_user', 'incident_view_operators'),
('op_admin', 'inst_admin'),
('op_raporteur', 'inst_rapporteur'),
('inst_rapporteur', 'inst_user'),
('op_user', 'inst_user'),
('op_admin', 'oo-inst-create'),
('sys_admin', 'oo-inst-create'),
('op_admin', 'oo-inst-delete'),
('sys_admin', 'oo-inst-delete'),
('ca_user', 'oo-inst-list'),
('inst_user', 'oo-inst-list'),
('op_admin', 'oo-inst-list'),
('sys_admin', 'oo-inst-list'),
('sys_admin', 'oo-inst-purge'),
('op_admin', 'oo-inst-update'),
('sys_admin', 'oo-inst-update'),
('ca_admin', 'oo-org-create'),
('sys_admin', 'oo-org-create'),
('sys_admin', 'oo-org-delete'),
('sys_admin', 'oo-org-purge'),
('ca_admin', 'oo-org-update'),
('sys_admin', 'oo-org-update'),
('ca-user-create', 'oo-user-create'),
('ca_admin', 'oo-user-create'),
('ca_user', 'oo-user-create'),
('op_admin', 'oo-user-create'),
('sys_admin', 'oo-user-create'),
('ca_user', 'oo-user-edit'),
('op_admin', 'oo-user-edit'),
('sys_admin', 'oo-user-edit'),
('op_admin', 'oo-user-list'),
('op_user', 'oo-user-list'),
('sys_admin', 'oo-user-list'),
('sys_admin', 'op_admin'),
('op_admin', 'op_raporteur'),
('ca-org-create', 'organization-create'),
('oo-org-create', 'organization-create'),
('ca_admin', 'organization-delete'),
('sys_admin', 'organization-delete'),
('ca_admin', 'organization-list'),
('ca_user', 'organization-list'),
('inst_user', 'organization-list'),
('op_user', 'organization-list'),
('process_request_operators', 'process_request'),
('sys_admin', 'process_request'),
('ca_admin', 'translate_app'),
('sys_admin', 'translate_app'),
('ca_admin', 'units-create'),
('ca_assessor', 'units-create'),
('ca_admin', 'units-edit'),
('ca_assessor', 'units-edit'),
('inst_user', 'units-list'),
('ca-user-create', 'user-create'),
('ca_admin', 'user-create'),
('oo-user-create', 'user-create'),
('sys_admin', 'user-create'),
('ca-user-delete', 'user-delete'),
('sys_admin', 'user-delete'),
('ca-user-edit', 'user-edit'),
('oo-user-edit', 'user-edit'),
('sys_admin', 'user-edit'),
('ca-user-list', 'user-list'),
('inst_user', 'user-list'),
('oo-user-list', 'user-list'),
('sys_admin', 'user-list'),
('ca-user-purge', 'user-purge'),
('sys_admin', 'user-purge'),
('display_info', 'user-view');

-- --------------------------------------------------------

--
-- Table structure for table `auth_rule`
--

DROP TABLE IF EXISTS `auth_rule`;
CREATE TABLE `auth_rule` (
  `name` varchar(64) NOT NULL,
  `data` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auth_rule`
--

INSERT INTO `auth_rule` (`name`, `data`, `created_at`, `updated_at`) VALUES
('can_view_info', 'O:32:"app\\components\\rules\\CanViewInfo":3:{s:4:"name";s:13:"can_view_info";s:9:"createdAt";i:1465578307;s:9:"updatedAt";i:1465578411;}', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('user_in_the_same_ca', 'O:40:"app\\components\\rules\\UserInTheSameCARule":3:{s:4:"name";s:19:"user_in_the_same_ca";s:9:"createdAt";i:1436974315;s:9:"updatedAt";i:1436974315;}', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('user_in_the_same_oo', 'O:40:"app\\components\\rules\\UserInTheSameOORule":3:{s:4:"name";s:19:"user_in_the_same_oo";s:9:"createdAt";i:1437040933;s:9:"updatedAt";i:1437040933;}', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('user_is_ca', 'O:37:"app\\components\\rules\\CreateCAUserRule":3:{s:4:"name";s:10:"user_is_ca";s:9:"createdAt";i:1437054446;s:9:"updatedAt";i:1437054446;}', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('user_is_installation', 'O:47:"app\\components\\rules\\CreateInstallationUserRule":3:{s:4:"name";s:20:"user_is_installation";s:9:"createdAt";i:1437054447;s:9:"updatedAt";i:1437054447;}', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('user_is_oo', 'O:37:"app\\components\\rules\\CreateOOUserRule":3:{s:4:"name";s:10:"user_is_oo";s:9:"createdAt";i:1437054447;s:9:"updatedAt";i:1437054447;}', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `auth_users`
--

DROP TABLE IF EXISTS `auth_users`;
CREATE TABLE `auth_users` (
  `id` int(11) NOT NULL,
  `username` varchar(45) NOT NULL,
  `email` varchar(128) NOT NULL,
  `org_id` char(12) DEFAULT NULL,
  `inst_id` char(12) DEFAULT NULL,
  `authKey` varchar(255) DEFAULT NULL,
  `accessToken` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT '10',
  `created_by` varchar(45) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `role_in_organization` varchar(128) DEFAULT NULL,
  `last_name` varchar(45) NOT NULL,
  `first_names` varchar(128) DEFAULT NULL,
  `login_timestamp` timestamp NULL DEFAULT NULL,
  `logout_timestamp` timestamp NULL DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `password_hash` varchar(255) NOT NULL,
  `password_reset_token` varchar(255) DEFAULT NULL,
  `auth_key` varchar(32) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=96 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auth_users`
--

INSERT INTO `auth_users` (`id`, `username`, `email`, `org_id`, `inst_id`, `authKey`, `accessToken`, `status`, `created_by`, `created_at`, `modified_at`, `role_in_organization`, `last_name`, `first_names`, `login_timestamp`, `logout_timestamp`, `phone`, `password_hash`, `password_reset_token`, `auth_key`) VALUES
(89, 'admin', 'noone@nomail.com', NULL, NULL, NULL, NULL, 10, NULL, '2016-02-04 16:51:55', '0000-00-00 00:00:00', 'default administrator user', 'admin', 'admin', NULL, NULL, NULL, '$2y$13$eGYrNX7jV3ZMumG8WxRt/O8HdUyr2.J0fe2X7tq5VnfyQ9zIGW79q', NULL, ''),
(90, 'cao.assessor', 'cao.assessor@nomail.com', 'CA1454683148', NULL, NULL, NULL, 10, NULL, '2016-02-05 14:42:57', '2016-02-05 15:27:20', 'Offshore incidents assessor', 'Assessor', 'Cao', NULL, NULL, '', '$2y$13$kF8/UV.m4Vb3MaDgD9rOu.fOAw.3fByHpRCQOem7.XB5qGjqTduR.', NULL, '76yJDk4JL996NMuAtxNSGAXy90uWgIoi'),
(91, 'cao.user', 'cao.user@nomail.com', 'CA1454683148', NULL, NULL, NULL, 10, NULL, '2016-02-05 14:43:44', '2016-02-05 15:26:35', 'regular CA user', 'User', 'Cao', NULL, NULL, '', '$2y$13$mqZHuQaBvb6DdN.Gb/DwmuysP5U8TwRFOd08DD5DIFf4.9PzhiLXC', NULL, 'M4BPqV0XczdfM6Hv68UK6vxeSQzQZEcr'),
(92, 'foc.rapp.1', 'foc.rapporteur1@nomail.com', 'OO1454683642', NULL, NULL, NULL, 10, NULL, '2016-02-05 14:57:35', '2016-02-05 15:25:40', 'offshore incident inspector', 'Rapporteur 1', 'Foc', NULL, NULL, '', '$2y$13$1/4ub5zlPrLqCE24fYZZtuLilthFnNFlfbWHQS4F9mlJqChfEQSR2', NULL, 'HyhMKgDtipCAi5EqVNjeq9cAWuXde5Yi'),
(93, 'foc.rapp.2', 'foc.rapporteur2@nomail.com', 'OO1454683642', 'IN1454683947', NULL, NULL, 10, NULL, '2016-02-05 14:58:30', '2016-02-05 15:24:33', 'offshore safety inspector', 'Rapporteur 2', 'Foc', NULL, NULL, '', '$2y$13$wXSAdgWC//0HpU8hGv6wPOxlll53p0xcCSRq/15KNpXjU7IIkVRvi', NULL, '27zejrlBMznMpJ83neyfyGiAV77QhOXk'),
(94, 'soc.rapp.1', 'soc.rapporteur1@nomail.com', 'OO1454683755', NULL, NULL, NULL, 10, NULL, '2016-02-05 14:59:44', '2016-02-05 15:20:58', 'safety inspector', 'Rapporteur 1', 'Soc', NULL, NULL, '', '$2y$13$R51pHxfuaFybqC1LFszAHe/2ZyztE8LXW195mwWFHB3rdguKEQ69W', NULL, 'w6lx4b4_ZMifFjY4EmuVDeVoN1Fic8KI'),
(95, 'soc.rapp.2', 'soc.rapporteur2@nomail.com', 'OO1454683755', 'IN1454684037', NULL, NULL, 10, NULL, '2016-02-05 15:00:49', '2016-02-05 14:02:28', 'installation safety inspector', 'Rapporteur 2', 'Soc', NULL, NULL, '', '$2y$13$4AryaFCpTeoC675KQ5LJieKzGBYZgMRVhb4NzggfFbc5MVtVR7OJK', NULL, 'b-ODUdisgmKvF3yQ_wNSUlsvADvUHZVj');

-- --------------------------------------------------------

--
-- Table structure for table `ca_incident_cat`
--

DROP TABLE IF EXISTS `ca_incident_cat`;
CREATE TABLE `ca_incident_cat` (
  `id` int(11) NOT NULL,
  `draft_id` int(11) NOT NULL COMMENT 'the id of draft that is the winner',
  `op_submitted_at` datetime DEFAULT NULL,
  `is_major_a` tinyint(1) DEFAULT NULL COMMENT 'True if report SectionA is categorized as major',
  `major_a_just` longtext COMMENT 'Justification if SectionA is categorized as major',
  `a_assessed_by` int(11) DEFAULT NULL,
  `a_assessed_at` timestamp NULL DEFAULT NULL,
  `is_major_b` tinyint(1) DEFAULT NULL COMMENT 'True if report SectionA is categorized as major',
  `major_b_just` longtext COMMENT 'Justification if SectionA is categorized as major',
  `b_assessed_by` int(11) DEFAULT NULL,
  `b_assessed_at` timestamp NULL DEFAULT NULL,
  `is_major_c` tinyint(1) DEFAULT NULL COMMENT 'True if report SectionA is categorized as major',
  `major_c_just` longtext COMMENT 'Justification if SectionA is categorized as major',
  `c_assessed_by` int(11) DEFAULT NULL,
  `c_assessed_at` timestamp NULL DEFAULT NULL,
  `is_major_d` tinyint(1) DEFAULT NULL COMMENT 'True if report SectionA is categorized as major',
  `major_d_just` longtext COMMENT 'Justification if SectionA is categorized as major',
  `d_assessed_by` int(11) DEFAULT NULL,
  `d_assessed_at` timestamp NULL DEFAULT NULL,
  `is_major_e` tinyint(1) DEFAULT NULL COMMENT 'True if report SectionA is categorized as major',
  `major_e_just` longtext COMMENT 'Justification if SectionA is categorized as major',
  `e_assessed_by` int(11) DEFAULT NULL,
  `e_assessed_at` timestamp NULL DEFAULT NULL,
  `is_major_f` tinyint(1) DEFAULT NULL COMMENT 'True if report SectionA is categorized as major',
  `major_f_just` longtext COMMENT 'Justification if SectionA is categorized as major',
  `f_assessed_by` int(11) DEFAULT NULL,
  `f_assessed_at` timestamp NULL DEFAULT NULL,
  `is_major_g` tinyint(1) DEFAULT NULL COMMENT 'True if report SectionA is categorized as major',
  `major_g_just` longtext COMMENT 'Justification if SectionA is categorized as major',
  `g_assessed_by` int(11) DEFAULT NULL,
  `g_assessed_at` timestamp NULL DEFAULT NULL,
  `is_major_h` tinyint(1) DEFAULT NULL COMMENT 'True if report SectionA is categorized as major',
  `major_h_just` longtext COMMENT 'Justification if SectionA is categorized as major',
  `h_assessed_by` int(11) DEFAULT NULL,
  `h_assessed_at` timestamp NULL DEFAULT NULL,
  `is_major_i` tinyint(1) DEFAULT NULL COMMENT 'True if report SectionA is categorized as major',
  `major_i_just` longtext COMMENT 'Justification if SectionA is categorized as major',
  `i_assessed_by` int(11) DEFAULT NULL,
  `i_assessed_at` timestamp NULL DEFAULT NULL,
  `is_major_j` tinyint(1) DEFAULT NULL COMMENT 'True if report SectionA is categorized as major',
  `major_j_just` longtext COMMENT 'Justification if SectionA is categorized as major',
  `j_assessed_by` int(11) DEFAULT NULL,
  `j_assessed_at` timestamp NULL DEFAULT NULL,
  `is_major` tinyint(1) DEFAULT NULL,
  `is_done` tinyint(1) NOT NULL DEFAULT '0' COMMENT '-1 if rejected; 2 if re-submitted',
  `rejection_desc` mediumtext,
  `cpf_status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table holding the data for Section A.3';

-- --------------------------------------------------------

--
-- Table structure for table `constants`
--

DROP TABLE IF EXISTS `constants`;
CREATE TABLE `constants` (
  `num_code` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `type` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='enumerations';

--
-- Dumping data for table `constants`
--

INSERT INTO `constants` (`num_code`, `name`, `type`) VALUES
(1, 'not-set', 0),
(2, 'not-applicable', 0),
(100, 'non process', 1),
(101, 'Oil', 1),
(102, 'Condensate', 1),
(103, 'Gas', 1),
(104, '2-Phase', 1),
(200, 'Hazardous area 1', 2),
(201, 'Hazardous area 2', 2),
(202, 'Non hazardous area ', 2),
(300, 'Normal', 3),
(301, 'Forced', 3),
(400, 'Fire', 4),
(401, 'Gas', 4),
(402, 'Smoke', 4),
(403, 'Other', 4),
(501, 'Automatic', 5),
(502, 'Manual', 5),
(503, 'At stations', 6),
(504, 'At lifeboats', 6),
(505, 'Other', 5),
(600, 'Drilling', 7),
(601, 'Well operations', 7),
(602, 'Production', 7),
(603, 'Maintenance', 7),
(604, 'Construction', 7),
(605, 'Pipeline operations including pigging', 7),
(700, 'Other', 8),
(701, 'normal production', 8),
(702, 'drilling', 8),
(703, 'work over', 8),
(704, 'well services', 8),
(800, 'wire line', 9),
(801, 'coiled tubing', 9),
(802, 'snubbing', 9),
(900, 'brine', 10),
(901, 'oil', 10),
(902, 'gas', 10),
(1000, 'surface', 11),
(1001, 'subsea', 11);

-- --------------------------------------------------------

--
-- Table structure for table `cpf_crf_assessments`
--

DROP TABLE IF EXISTS `cpf_crf_assessments`;
CREATE TABLE `cpf_crf_assessments` (
  `year` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `report_id` int(11) NOT NULL,
  `s_s4_2_a_total` int(11) DEFAULT NULL COMMENT 'Ignited oil/gas releases - Total - syrio',
  `s_s4_2_a_if` int(11) DEFAULT NULL COMMENT 'Ignited oil/gas releases - Fires - syrio',
  `s_s4_2_a_ix` int(11) DEFAULT NULL COMMENT 'Ignited oil/gas releases - Explosions - syrio',
  `s_s4_2_a_nig` int(11) DEFAULT NULL COMMENT 'Not ignited gas releases - syrio',
  `s_s4_2_a_nio` int(11) DEFAULT NULL COMMENT 'Not ignited oil releases - syrio',
  `s_s4_2_a_haz` int(11) DEFAULT NULL COMMENT 'Hazardous substances releases - syrio',
  `s_s4_2_b_total` int(11) DEFAULT NULL COMMENT 'Loss of well - Total - syrio',
  `s_s4_2_b_bo` int(11) DEFAULT NULL COMMENT 'Blowouts - syrio',
  `s_s4_2_b_bda` int(11) DEFAULT NULL COMMENT 'Blowout / diverter activation - syrio',
  `s_s4_2_b_wbf` int(11) DEFAULT NULL COMMENT 'Well barrier failure - syrio',
  `s_s4_2_c_total` int(11) DEFAULT NULL COMMENT 'Failures of SECE - syrio',
  `s_s4_2_d_total` int(11) DEFAULT NULL COMMENT 'Loss of structural integrity - total - syrio',
  `s_s4_2_d_si` int(11) DEFAULT NULL COMMENT 'Loss of structural integrity - syrio',
  `s_s4_2_d_sb` int(11) DEFAULT NULL COMMENT 'Loss of stablity/buoyancy - syrio',
  `s_s4_2_d_sk` int(11) DEFAULT NULL COMMENT 'Loss of station keeping - syrio',
  `s_s4_2_e_total` int(11) DEFAULT NULL COMMENT 'Vessel collisions - total - syrio',
  `s_s4_2_f_total` int(11) DEFAULT NULL COMMENT 'Helicopter accidents - syrio',
  `s_s4_2_g_total` int(11) DEFAULT NULL COMMENT 'Fatal accidents - syrio',
  `s_s4_2_h_total` int(11) DEFAULT NULL COMMENT 'Serious injuries of 5 or more persons in the same accident - syrio',
  `s_s4_2_i_total` int(11) DEFAULT NULL COMMENT 'Evacuation of personnel - syrio',
  `s_s4_2_j_total` int(11) DEFAULT NULL COMMENT 'Environmental accidents - syrio',
  `s_s4_3_total` int(11) DEFAULT NULL COMMENT 'Total number of fatalities and injuries - syrio',
  `s_s4_3_fatalities` int(11) DEFAULT NULL COMMENT 'Number of fatalities - syrio',
  `s_s4_3_inj` int(11) DEFAULT NULL COMMENT 'Total number of injuries - syrio',
  `s_s4_3_sinj` int(11) DEFAULT NULL COMMENT 'Total number of serious injuries - syrio',
  `s_s4_4_nsis` int(11) DEFAULT NULL COMMENT 'Structural integrity systems - syrio',
  `s_s4_4_npcs` int(11) DEFAULT NULL COMMENT 'Process containment systems - syrio',
  `s_s4_4_nics` int(11) DEFAULT NULL COMMENT 'Ignition control systems - syrio',
  `s_s4_4_nds` int(11) DEFAULT NULL COMMENT 'Detection control systems - syrio',
  `s_s4_4_npcrs` int(11) DEFAULT NULL COMMENT 'Process containment relief systems - syrio',
  `s_s4_4_nps` int(11) DEFAULT NULL COMMENT 'Protection systems - syrio',
  `s_s4_4_nsds` int(11) DEFAULT NULL COMMENT 'Shutdown systems - syrio',
  `s_s4_4_nnavaids` int(11) DEFAULT NULL COMMENT 'Navigational aids - syrio',
  `s_s4_4_roteq` int(11) DEFAULT NULL COMMENT 'Rotation equipment - syrio',
  `s_s4_4_eere` int(11) DEFAULT NULL COMMENT 'Escape, evacuation and rescue equipment - syrio',
  `s_s4_4_ncoms` int(11) DEFAULT NULL COMMENT 'Communication systems - syrio',
  `s_s4_4_nother` int(11) DEFAULT NULL COMMENT 'other - syrio',
  `s_s4_5_a_total` int(11) DEFAULT NULL COMMENT 'Incidents caused by Equipment failure - Total - syrio',
  `s_s4_5_a_df` int(11) DEFAULT NULL COMMENT 'Design failures - syrio',
  `s_s4_5_a_co__int` int(11) DEFAULT NULL COMMENT 'Internal corrosion - syrio',
  `s_s4_5_a_co__ext` int(11) DEFAULT NULL COMMENT 'External corrosion - syrio',
  `s_s4_5_a_mf__f` int(11) DEFAULT NULL COMMENT 'Mechanical failures due to fatigue - syrio',
  `s_s4_5_a_mf__wo` int(11) DEFAULT NULL COMMENT 'Mechanical failures due to wear-out - syrio',
  `s_s4_5_a_mf__dm` int(11) DEFAULT NULL COMMENT 'Mechanical failures due to defected material - syrio',
  `s_s4_5_a_mf__vh` int(11) DEFAULT NULL COMMENT 'Mechanical failures (vessel/helicopter) - syrio',
  `s_s4_5_a_if` int(11) DEFAULT NULL COMMENT 'Instrument failures - syrio',
  `s_s4_5_a_csf` int(11) DEFAULT NULL COMMENT 'Control system failures - syrio',
  `s_s4_5_a_other` int(11) DEFAULT NULL COMMENT 'other - syrio',
  `s_s4_5_b_total` int(11) DEFAULT NULL COMMENT 'Human error-operational causes - total - syrio',
  `s_s4_5_b_err__op` int(11) DEFAULT NULL COMMENT 'Operation error - syrio',
  `s_s4_5_b_err__mnt` int(11) DEFAULT NULL COMMENT 'Maintenance error - syrio',
  `s_s4_5_b_err__tst` int(11) DEFAULT NULL COMMENT 'Testing error - syrio',
  `s_s4_5_b_err__insp` int(11) DEFAULT NULL COMMENT 'Inspection error - syrio',
  `s_s4_5_b_err__dsgn` int(11) DEFAULT NULL COMMENT 'Design error - syrio',
  `s_s4_5_b_other` int(11) DEFAULT NULL COMMENT 'other - syrio',
  `s_s4_5_c_total` int(11) DEFAULT NULL COMMENT 'Procedural/organizational error - syrio',
  `s_s4_5_c_inq__rap` int(11) DEFAULT NULL COMMENT 'Inadequate risk assessment/perception - syrio',
  `s_s4_5_c_inq__instp` int(11) DEFAULT NULL COMMENT 'Inadequate instruction/procedure - syrio',
  `s_s4_5_c_nc__prc` int(11) DEFAULT NULL COMMENT 'Non-compliance with procedure - syrio',
  `s_s4_5_c_nc__ptw` int(11) DEFAULT NULL COMMENT 'Non-compliance with permit-to-work - syrio',
  `s_s4_5_c_inq__com` int(11) DEFAULT NULL COMMENT 'Inadequate communication - syrio',
  `s_s4_5_c_inq__pc` int(11) DEFAULT NULL COMMENT 'Inadequate personnel competence - syrio',
  `s_s4_5_c_inq__sup` int(11) DEFAULT NULL COMMENT 'Inadequate supervision - syrio',
  `s_s4_5_c_inq__safelead` int(11) DEFAULT NULL COMMENT 'Inadequate safety leadership - syrio',
  `s_s4_5_c_other` int(11) DEFAULT NULL COMMENT 'other - syrio',
  `s_s4_6` text COMMENT 'Most important lessons learned - syrio',
  `u_s4_2_a_total` int(11) DEFAULT NULL COMMENT 'Ignited oil/gas releases - Total - user',
  `u_s4_2_a_if` int(11) DEFAULT NULL COMMENT 'Ignited oil/gas releases - Fires - user',
  `u_s4_2_a_ix` int(11) DEFAULT NULL COMMENT 'Ignited oil/gas releases - Explosions - user',
  `u_s4_2_a_nig` int(11) DEFAULT NULL COMMENT 'Not ignited gas releases - user',
  `u_s4_2_a_nio` int(11) DEFAULT NULL COMMENT 'Not ignited oil releases - user',
  `u_s4_2_a_haz` int(11) DEFAULT NULL COMMENT 'Hazardous substances releases - user',
  `u_s4_2_b_total` int(11) DEFAULT NULL COMMENT 'Loss of well - Total - user',
  `u_s4_2_b_bo` int(11) DEFAULT NULL COMMENT 'Blowouts - user',
  `u_s4_2_b_bda` int(11) DEFAULT NULL COMMENT 'Blowout / diverter activation - user',
  `u_s4_2_b_wbf` int(11) DEFAULT NULL COMMENT 'Well barrier failure - user',
  `u_s4_2_c_total` int(11) DEFAULT NULL COMMENT 'Failures of SECE - user',
  `u_s4_2_d_total` int(11) DEFAULT NULL COMMENT 'Loss of structural integrity - total - user',
  `u_s4_2_d_si` int(11) DEFAULT NULL COMMENT 'Loss of structural integrity - user',
  `u_s4_2_d_sb` int(11) DEFAULT NULL COMMENT 'Loss of stablity/buoyancy - user',
  `u_s4_2_d_sk` int(11) DEFAULT NULL COMMENT 'Loss of station keeping - user',
  `u_s4_2_e_total` int(11) DEFAULT NULL COMMENT 'Vessel collisions - total - user',
  `u_s4_2_f_total` int(11) DEFAULT NULL COMMENT 'Helicopter accidents - user',
  `u_s4_2_g_total` int(11) DEFAULT NULL COMMENT 'Fatal accidents - user',
  `u_s4_2_h_total` int(11) DEFAULT NULL COMMENT 'Serious injuries of 5 or more persons in the same accident - user',
  `u_s4_2_i_total` int(11) DEFAULT NULL COMMENT 'Evacuation of personnel - user',
  `u_s4_2_j_total` int(11) DEFAULT NULL COMMENT 'Environmental accidents - user',
  `u_s4_3_total` int(11) DEFAULT NULL COMMENT 'Total number of fatalities and injuries - user',
  `u_s4_3_fatalities` int(11) DEFAULT NULL COMMENT 'Number of fatalities - user',
  `u_s4_3_inj` int(11) DEFAULT NULL COMMENT 'Total number of injuries - user',
  `u_s4_3_sinj` int(11) DEFAULT NULL COMMENT 'Total number of serious injuries - user',
  `u_s4_4_nsis` int(11) DEFAULT NULL COMMENT 'Structural integrity systems - user',
  `u_s4_4_npcs` int(11) DEFAULT NULL COMMENT 'Process containment systems - user',
  `u_s4_4_nics` int(11) DEFAULT NULL COMMENT 'Ignition control systems - user',
  `u_s4_4_nds` int(11) DEFAULT NULL COMMENT 'Detection control systems - user',
  `u_s4_4_npcrs` int(11) DEFAULT NULL COMMENT 'Process containment relief systems - user',
  `u_s4_4_nps` int(11) DEFAULT NULL COMMENT 'Protection systems - user',
  `u_s4_4_nsds` int(11) DEFAULT NULL COMMENT 'Shutdown systems - user',
  `u_s4_4_nnavaids` int(11) DEFAULT NULL COMMENT 'Navigational aids - user',
  `u_s4_4_roteq` int(11) DEFAULT NULL COMMENT 'Rotation equipment - user',
  `u_s4_4_eere` int(11) DEFAULT NULL COMMENT 'Escape, evacuation and rescue equipment - user',
  `u_s4_4_ncoms` int(11) DEFAULT NULL COMMENT 'Communication systems - user',
  `u_s4_4_nother` int(11) DEFAULT NULL COMMENT 'other - user',
  `u_s4_5_a_total` int(11) DEFAULT NULL COMMENT 'Incidents caused by Equipment failure - Total - user',
  `u_s4_5_a_df` int(11) DEFAULT NULL COMMENT 'Design failures - user',
  `u_s4_5_a_co__int` int(11) DEFAULT NULL COMMENT 'Internal corrosion - user',
  `u_s4_5_a_co__ext` int(11) DEFAULT NULL COMMENT 'External corrosion - user',
  `u_s4_5_a_mf__f` int(11) DEFAULT NULL COMMENT 'Mechanical failures due to fatigue - user',
  `u_s4_5_a_mf__wo` int(11) DEFAULT NULL COMMENT 'Mechanical failures due to wear-out - user',
  `u_s4_5_a_mf__dm` int(11) DEFAULT NULL COMMENT 'Mechanical failures due to defected material - user',
  `u_s4_5_a_mf__vh` int(11) DEFAULT NULL COMMENT 'Mechanical failures (vessel/helicopter) - user',
  `u_s4_5_a_if` int(11) DEFAULT NULL COMMENT 'Instrument failures - user',
  `u_s4_5_a_csf` int(11) DEFAULT NULL COMMENT 'Control system failures - user',
  `u_s4_5_a_other` int(11) DEFAULT NULL COMMENT 'other - user',
  `u_s4_5_b_total` int(11) DEFAULT NULL COMMENT 'Human error-operational causes - total - user',
  `u_s4_5_b_err__op` int(11) DEFAULT NULL COMMENT 'Operation error - user',
  `u_s4_5_b_err__mnt` int(11) DEFAULT NULL COMMENT 'Maintenance error - user',
  `u_s4_5_b_err__tst` int(11) DEFAULT NULL COMMENT 'Testing error - user',
  `u_s4_5_b_err__insp` int(11) DEFAULT NULL COMMENT 'Inspection error - user',
  `u_s4_5_b_err__dsgn` int(11) DEFAULT NULL COMMENT 'Design error - user',
  `u_s4_5_b_other` int(11) DEFAULT NULL COMMENT 'other - user',
  `u_s4_5_c_total` int(11) DEFAULT NULL COMMENT 'Procedural/organizational error - user',
  `u_s4_5_c_inq__rap` int(11) DEFAULT NULL COMMENT 'Inadequate risk assessment/perception - user',
  `u_s4_5_c_inq__instp` int(11) DEFAULT NULL COMMENT 'Inadequate instruction/procedure - user',
  `u_s4_5_c_nc__prc` int(11) DEFAULT NULL COMMENT 'Non-compliance with procedure - user',
  `u_s4_5_c_nc__ptw` int(11) DEFAULT NULL COMMENT 'Non-compliance with permit-to-work - user',
  `u_s4_5_c_inq__com` int(11) DEFAULT NULL COMMENT 'Inadequate communication - user',
  `u_s4_5_c_inq__pc` int(11) DEFAULT NULL COMMENT 'Inadequate personnel competence - user',
  `u_s4_5_c_inq__sup` int(11) DEFAULT NULL COMMENT 'Inadequate supervision - user',
  `u_s4_5_c_inq__safelead` int(11) DEFAULT NULL COMMENT 'Inadequate safety leadership - user',
  `u_s4_5_c_other` int(11) DEFAULT NULL COMMENT 'other - user',
  `u_s4_6` text COMMENT 'Most important lessons learned - user',
  `s_s4_5_d_total` int(11) DEFAULT NULL COMMENT 'Weather-related causes - total - user',
  `s_s4_5_d_exc__design__wind` int(11) DEFAULT NULL COMMENT 'Wind in excess of limits of design - user',
  `s_s4_5_d_exc__design__wave` int(11) DEFAULT NULL COMMENT 'Waves in excess of limits of design - user',
  `s_s4_5_d_exc__design__lowvis` int(11) DEFAULT NULL COMMENT 'Extremely low visibility in excess of limits of design - user',
  `s_s4_5_d_ice__icebergs` int(11) DEFAULT NULL COMMENT 'Presence of ice or icebergs - user',
  `s_s4_5_d_other` int(11) DEFAULT NULL COMMENT 'other - user',
  `u_s4_5_d_total` int(11) DEFAULT NULL COMMENT 'Weather-related causes - total - SyRIO',
  `u_s4_5_d_exc__design__wind` int(11) DEFAULT NULL COMMENT 'Wind in excess of limits of design - SyRIO',
  `u_s4_5_d_exc__design__wave` int(11) DEFAULT NULL COMMENT 'Waves in excess of limits of design - SyRIO',
  `u_s4_5_d_exc__design__lowvis` int(11) DEFAULT NULL COMMENT 'Extremely low visibility in excess of limits of design - SyRIO',
  `u_s4_5_d_ice__icebergs` int(11) DEFAULT NULL COMMENT 'Presence of ice or icebergs - SyRIO',
  `u_s4_5_d_other` int(11) DEFAULT NULL COMMENT 'other - SyRIO',
  `status` int(11) NOT NULL,
  `assessment_status` int(11) NOT NULL,
  `created_at` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified_at` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cpf_installations`
--

DROP TABLE IF EXISTS `cpf_installations`;
CREATE TABLE `cpf_installations` (
  `id` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `type` varchar(8) NOT NULL,
  `ident` varchar(128) NOT NULL,
  `installation_year` int(11) NOT NULL,
  `tof` varchar(16) NOT NULL,
  `n_beds` int(11) DEFAULT NULL,
  `lat` double DEFAULT NULL,
  `lon` double DEFAULT NULL,
  `decom_type` int(11) DEFAULT NULL,
  `syrio_id` char(12) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cpf_inst_op_area`
--

DROP TABLE IF EXISTS `cpf_inst_op_area`;
CREATE TABLE `cpf_inst_op_area` (
  `id` int(11) NOT NULL,
  `installation_id` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `duration` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cpf_references`
--

DROP TABLE IF EXISTS `cpf_references`;
CREATE TABLE `cpf_references` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `url` varchar(512) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cpf_report_refs`
--

DROP TABLE IF EXISTS `cpf_report_refs`;
CREATE TABLE `cpf_report_refs` (
  `year` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `ref_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cpf_section1`
--

DROP TABLE IF EXISTS `cpf_section1`;
CREATE TABLE `cpf_section1` (
  `year` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `ms` char(2) NOT NULL,
  `ca` varchar(128) NOT NULL,
  `dra` varchar(128) NOT NULL,
  `phone` varchar(16) NOT NULL,
  `email` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cpf_section2`
--

DROP TABLE IF EXISTS `cpf_section2`;
CREATE TABLE `cpf_section2` (
  `year` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `working_hours` double DEFAULT NULL,
  `norm_ktoe` double DEFAULT NULL,
  `norm_oil_q` double DEFAULT NULL,
  `norm_oil_u` int(11) DEFAULT NULL,
  `norm_oil_n` int(11) DEFAULT NULL,
  `norm_gas_q` double DEFAULT NULL,
  `norm_gas_u` int(11) DEFAULT NULL,
  `norm_gas_n` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cpf_section3`
--

DROP TABLE IF EXISTS `cpf_section3`;
CREATE TABLE `cpf_section3` (
  `year` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `n_offshore_inspections` int(11) DEFAULT NULL,
  `man_days` double DEFAULT NULL,
  `n_inpeceted` int(11) DEFAULT NULL,
  `n_major_accidents` int(11) DEFAULT NULL,
  `n_env_concerns` int(11) DEFAULT NULL,
  `s3_3_text` text,
  `description` text,
  `rationale` text,
  `expected_outcome` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cpf_section4`
--

DROP TABLE IF EXISTS `cpf_section4`;
CREATE TABLE `cpf_section4` (
  `year` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `s4_1_n_events_total` int(11) DEFAULT NULL COMMENT 'Number of reportable events pursuant to Annex IX',
  `s4_1_n_major_accidents` int(11) DEFAULT NULL COMMENT 'Number of major accidents',
  `s4_2_a_total` int(11) DEFAULT NULL COMMENT 'Ignited oil/gas releases - Total',
  `s4_2_a_if` int(11) DEFAULT NULL COMMENT 'Ignited oil/gas releases - Fires',
  `s4_2_a_ix` int(11) DEFAULT NULL COMMENT 'Ignited oil/gas releases - Explosions',
  `s4_2_a_nig` int(11) DEFAULT NULL COMMENT 'Not ignited gas releases',
  `s4_2_a_nio` int(11) DEFAULT NULL COMMENT 'Not ignited oil releases',
  `s4_2_a_haz` int(11) DEFAULT NULL COMMENT 'Hazardous substances releases',
  `s4_2_b_total` int(11) DEFAULT NULL COMMENT 'Loss of well - Total',
  `s4_2_b_bo` int(11) DEFAULT NULL COMMENT 'Blowouts',
  `s4_2_b_bda` int(11) DEFAULT NULL COMMENT 'Blowout / diverter activation',
  `s4_2_b_wbf` int(11) DEFAULT NULL COMMENT 'Well barrier failure',
  `s4_2_c_total` int(11) DEFAULT NULL COMMENT 'Failures of SECE',
  `s4_2_d_total` int(11) DEFAULT NULL COMMENT 'Loss of structural integrity - total',
  `s4_2_d_si` int(11) DEFAULT NULL COMMENT 'Loss of structural integrity',
  `s4_2_d_sb` int(11) DEFAULT NULL COMMENT 'Loss of stablity/buoyancy',
  `s4_2_d_sk` int(11) DEFAULT NULL COMMENT 'Loss of station keeping',
  `s4_2_e_total` int(11) DEFAULT NULL COMMENT 'Vessel collisions - total',
  `s4_2_f_total` int(11) DEFAULT NULL COMMENT 'Helicopter accidents',
  `s4_2_g_total` int(11) DEFAULT NULL COMMENT 'Fatal accidents',
  `s4_2_h_total` int(11) DEFAULT NULL COMMENT 'Serious injuries of 5 or more persons in the same accident',
  `s4_2_i_total` int(11) DEFAULT NULL COMMENT 'Evacuation of personnel',
  `s4_2_j_total` int(11) DEFAULT NULL COMMENT 'Environmental accidents',
  `s4_3_total` int(11) DEFAULT NULL COMMENT 'Total number of fatalities and injuries',
  `s4_3_fatalities` int(11) DEFAULT NULL COMMENT 'Number of fatalities',
  `s4_3_inj` int(11) DEFAULT NULL COMMENT 'Total number of injuries',
  `s4_3_sinj` int(11) DEFAULT NULL COMMENT 'Total number of serious injuries',
  `s4_4_nsis` int(11) DEFAULT NULL COMMENT 'Structural integrity systems',
  `s4_4_npcs` int(11) DEFAULT NULL COMMENT 'Process containment systems',
  `s4_4_nics` int(11) DEFAULT NULL COMMENT 'Ignition control systems',
  `s4_4_nds` int(11) DEFAULT NULL COMMENT 'Detection control systems',
  `s4_4_npcrs` int(11) DEFAULT NULL COMMENT 'Process containment relief systems',
  `s4_4_nps` int(11) DEFAULT NULL COMMENT 'Protection systems',
  `s4_4_nsds` int(11) DEFAULT NULL COMMENT 'Shutdown systems',
  `s4_4_nnavaids` int(11) DEFAULT NULL COMMENT 'Navigational aids',
  `s4_4_roteq` int(11) DEFAULT NULL COMMENT 'Rotation equipment',
  `s4_4_eere` int(11) DEFAULT NULL COMMENT 'Escape, evacuation and rescue equipment',
  `s4_4_ncoms` int(11) DEFAULT NULL COMMENT 'Communication systems',
  `s4_4_nother` int(11) DEFAULT NULL COMMENT 'other',
  `s4_5_a_total` int(11) DEFAULT NULL COMMENT 'Incidents caused by Equipment failure - Total',
  `s4_5_a_df` int(11) DEFAULT NULL COMMENT 'Design failures',
  `s4_5_a_co__int` int(11) DEFAULT NULL COMMENT 'Internal corrosion',
  `s4_5_a_co__ext` int(11) DEFAULT NULL COMMENT 'External corrosion',
  `s4_5_a_mf__f` int(11) DEFAULT NULL COMMENT 'Mechanical failures due to fatigue',
  `s4_5_a_mf__wo` int(11) DEFAULT NULL COMMENT 'Mechanical failures due to wear-out',
  `s4_5_a_mf__dm` int(11) DEFAULT NULL COMMENT 'Mechanical failures due to defected material',
  `s4_5_a_mf__vh` int(11) DEFAULT NULL COMMENT 'Mechanical failures (vessel/helicopter)',
  `s4_5_a_if` int(11) DEFAULT NULL COMMENT 'Instrument failures',
  `s4_5_a_csf` int(11) DEFAULT NULL COMMENT 'Control system failures',
  `s4_5_a_other` int(11) DEFAULT NULL COMMENT 'other',
  `s4_5_b_total` int(11) DEFAULT NULL COMMENT 'Human error-operational causes - total',
  `s4_5_b_err__op` int(11) DEFAULT NULL COMMENT 'Operation error',
  `s4_5_b_err__mnt` int(11) DEFAULT NULL COMMENT 'Maintenance error',
  `s4_5_b_err__tst` int(11) DEFAULT NULL COMMENT 'Testing error',
  `s4_5_b_err__insp` int(11) DEFAULT NULL COMMENT 'Inspection error',
  `s4_5_b_err__dsgn` int(11) DEFAULT NULL COMMENT 'Design error',
  `s4_5_b_other` int(11) DEFAULT NULL COMMENT 'other',
  `s4_5_c_total` int(11) DEFAULT NULL COMMENT 'Procedural/organizational error',
  `s4_5_c_inq__rap` int(11) DEFAULT NULL COMMENT 'Inadequate risk assessment/perception',
  `s4_5_c_inq__instp` int(11) DEFAULT NULL COMMENT 'Inadequate instruction/procedure',
  `s4_5_c_nc__prc` int(11) DEFAULT NULL COMMENT 'Non-compliance with procedure',
  `s4_5_c_nc__ptw` int(11) DEFAULT NULL COMMENT 'Non-compliance with permit-to-work',
  `s4_5_c_inq__com` int(11) DEFAULT NULL COMMENT 'Inadequate communication',
  `s4_5_c_inq__pc` int(11) DEFAULT NULL COMMENT 'Inadequate personnel competence',
  `s4_5_c_inq__sup` int(11) DEFAULT NULL COMMENT 'Inadequate supervision',
  `s4_5_c_inq__safelead` int(11) DEFAULT NULL COMMENT 'Inadequate safety leadership',
  `s4_5_c_other` int(11) DEFAULT NULL COMMENT 'other',
  `s4_6` text COMMENT 'Most important lessons learned',
  `s4_5_d_total` int(11) DEFAULT NULL COMMENT 'Weather-related causes - total',
  `s4_5_d_exc__design__wind` int(11) DEFAULT NULL COMMENT 'Wind in excess of limits of design',
  `s4_5_d_exc__design__wave` int(11) DEFAULT NULL COMMENT 'Waves in excess of limits of design',
  `s4_5_d_exc__design__lowvis` int(11) DEFAULT NULL COMMENT 'Extremely low visibility in excess of limits of design',
  `s4_5_d_ice__icebergs` int(11) DEFAULT NULL COMMENT 'Presence of ice or icebergs',
  `s4_5_d_other` int(11) DEFAULT NULL COMMENT 'other'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cpf_section_4`
--

DROP TABLE IF EXISTS `cpf_section_4`;
CREATE TABLE `cpf_section_4` (
  `year` int(11) NOT NULL,
  `s4_1_n_events_total` int(11) DEFAULT '0' COMMENT 'Number of reportable events pursuant to Annex IX',
  `s4_1_n_major_accidents` int(11) DEFAULT '0' COMMENT 'Major accidents',
  `s4_2_a_total` int(11) DEFAULT '0' COMMENT 'Ignited oil/gas releases - Total',
  `s4_2_a_if` int(11) DEFAULT '0' COMMENT 'Ignited oil/gas releases - Fires',
  `s4_2_a_ix` int(11) DEFAULT '0' COMMENT 'Ignited oil/gas releases - Explosions',
  `s4_2_a_nig` int(11) DEFAULT '0' COMMENT 'Non ignited gas releases',
  `s4_2_a_nio` int(11) DEFAULT '0' COMMENT 'Non-ignited oil releases',
  `s4_2_a_haz` int(11) DEFAULT '0' COMMENT 'Hazardous substances released',
  `s4_2_b_total` int(11) DEFAULT '0' COMMENT 'Loss of well control - Total',
  `s4_2_b_bo` int(11) DEFAULT '0' COMMENT 'Blowouts',
  `s4_2_b_bda` int(11) DEFAULT '0' COMMENT 'Blowout / diverter activation',
  `s4_2_b_wbf` int(11) DEFAULT '0' COMMENT 'Well barrier failures',
  `s4_2_c_total` int(11) DEFAULT '0' COMMENT 'Failres of SECE''s',
  `s4_2_d_total` int(11) DEFAULT '0' COMMENT 'Loss of structural integrity - total',
  `s4_2_d_si` int(11) DEFAULT '0' COMMENT 'Loss of structural integrity',
  `s4_2_d_sb` int(11) DEFAULT '0' COMMENT 'Loss of stability/ buoyancy',
  `s4_2_d_sk` int(11) DEFAULT '0' COMMENT 'Loss of station keeping',
  `s4_2_e_total` int(11) DEFAULT '0' COMMENT 'Vesel collisions - total',
  `s4_2_f_total` int(11) DEFAULT '0' COMMENT 'Helicopter accidents',
  `s4_2_g_total` int(11) DEFAULT '0' COMMENT 'Fatal accidents',
  `s4_2_h_total` int(11) DEFAULT '0' COMMENT 'Serious injuries of 5 or more persons in the same accident',
  `s4_2_i_total` int(11) DEFAULT '0' COMMENT 'Evacuation of personnel',
  `s4_2_j_total` int(11) DEFAULT '0' COMMENT 'Environmental accidents',
  `s4_3_total` int(11) DEFAULT '0' COMMENT 'Total number of fatalities and injuries',
  `s4_3_fatalities` int(11) DEFAULT '0' COMMENT 'Number of fatalities',
  `s4_3_inj` int(11) DEFAULT '0' COMMENT 'Total number of injuries',
  `s4_3_sinj` int(11) DEFAULT '0' COMMENT 'Total number of serous injuries',
  `s4_4_nsis` int(11) DEFAULT '0' COMMENT 'Structural integrity systems',
  `s4_4_npcs` int(11) DEFAULT '0' COMMENT 'Process containment systems',
  `s4_4_nics` int(11) DEFAULT '0' COMMENT 'Ignition control systems',
  `s4_4_nds` int(11) DEFAULT '0' COMMENT 'Detection control systems',
  `s4_4_npcrs` int(11) DEFAULT '0' COMMENT 'Process containment relief systems',
  `s4_4_nps` int(11) DEFAULT '0' COMMENT 'Protection systems',
  `s4_4_nsds` int(11) DEFAULT '0' COMMENT 'Shutdown systems',
  `s4_4_nnavaids` int(11) DEFAULT '0' COMMENT 'Navigational aids',
  `s4_4_roteq` int(11) DEFAULT '0' COMMENT 'Rotation equipment',
  `s4_4_eere` int(11) DEFAULT '0' COMMENT 'Escape, evacuation and rescue equipment',
  `s4_4_ncoms` int(11) DEFAULT '0' COMMENT 'Communication systems',
  `s4_4_nother` int(11) DEFAULT '0' COMMENT 'other',
  `s4_5_a_total` int(11) DEFAULT '0' COMMENT 'Incidents caused by Equipment failure - Total',
  `s4_5_a_df` int(11) DEFAULT '0' COMMENT 'Design failures',
  `s4_5_a_co__int` int(11) DEFAULT '0' COMMENT 'Internal corrosion',
  `s4_5_a_co__ext` int(11) DEFAULT '0' COMMENT 'External corrosion',
  `s4_5_a_mf__f` int(11) DEFAULT '0' COMMENT 'Mechanical failure due to fatigue',
  `s4_5_a_mf__wo` int(11) DEFAULT '0' COMMENT 'Mechanical failures due to wear-out',
  `s4_5_a_mf__dm` int(11) DEFAULT '0' COMMENT 'Mechanical failures due to defected material',
  `s4_5_a_mf__vh` int(11) DEFAULT '0' COMMENT 'Mechanical failures (vessel/helicopter)',
  `s4_5_a_if` int(11) DEFAULT '0' COMMENT 'Instrument failures',
  `s4_5_a_csf` int(11) DEFAULT '0' COMMENT 'Control system failures',
  `s4_5_a_other` int(11) DEFAULT '0' COMMENT 'other',
  `s4_5_b_total` int(11) DEFAULT '0' COMMENT 'Human error-operational causes - total',
  `s4_5_b_err__op` int(11) DEFAULT '0' COMMENT 'Operation error',
  `s4_5_b_err__mnt` int(11) DEFAULT '0' COMMENT 'Maintenance error',
  `s4_5_b_err__tst` int(11) DEFAULT '0' COMMENT 'Testing error',
  `s4_5_b_err__insp` int(11) DEFAULT '0' COMMENT 'Inspection error',
  `s4_5_b_err__dsgn` int(11) DEFAULT '0' COMMENT 'Design error',
  `s4_5_b_other` int(11) DEFAULT '0' COMMENT 'Other',
  `s4_5_c_total` int(11) DEFAULT '0' COMMENT 'Procedural / organizational error',
  `s4_5_c_inq__rap` int(11) DEFAULT '0' COMMENT ' Inadequate risk assessment/perception',
  `s4_5_c_inq__instp` int(11) DEFAULT '0' COMMENT 'Inadequate instruction/procedure',
  `s4_5_c_nc__prc` int(11) DEFAULT '0' COMMENT 'Non-compliance with procedure',
  `s4_5_c_nc__ptw` int(11) DEFAULT '0' COMMENT 'Non-compliance with permit-to-work',
  `s4_5_c_inq__com` int(11) DEFAULT '0' COMMENT 'Inadequate communication',
  `s4_5_c_inq__pc` int(11) DEFAULT '0' COMMENT ' Inadequate personnel competence',
  `s4_5_c_inq__sup` int(11) DEFAULT '0' COMMENT ' Inadequate supervision',
  `s4_5_c_inq__safelead` int(11) DEFAULT '0' COMMENT 'Inadequate safety leadership',
  `s4_5_c_other` int(11) DEFAULT '0' COMMENT 'other',
  `s4_5_d_total` int(11) DEFAULT '0' COMMENT 'Weather-related causes - total',
  `s4_5_d_exc__design__wind` int(11) DEFAULT '0' COMMENT 'Wind in excess of limits of design',
  `s4_5_d_exc__design__wave` int(11) DEFAULT '0' COMMENT 'Waves in excess of limits of design',
  `s4_5_d_exc__design__lowvis` int(11) DEFAULT '0' COMMENT ' Extremely low visibility in excess of limits of design',
  `s4_5_d_ice__icebergs` int(11) DEFAULT '0' COMMENT 'Presence of ice or icebergs',
  `s4_5_d_other` int(11) DEFAULT '0' COMMENT 'other',
  `s4_6` longtext COMMENT 'Most important lessons learned'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='this table holds the data inferred for the CPF';

-- --------------------------------------------------------

--
-- Table structure for table `cpf_sessions`
--

DROP TABLE IF EXISTS `cpf_sessions`;
CREATE TABLE `cpf_sessions` (
  `year` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `refresh_installations` int(1) NOT NULL DEFAULT '1',
  `refresh_data` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cpf_tof`
--

DROP TABLE IF EXISTS `cpf_tof`;
CREATE TABLE `cpf_tof` (
  `type` varchar(16) NOT NULL,
  `name` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='types of fluids in cpf';

--
-- Dumping data for table `cpf_tof`
--

INSERT INTO `cpf_tof` (`type`, `name`) VALUES
('condensate', 'Condensate'),
('gas', 'Gas'),
('na', 'Not applicable'),
('oil', 'Oil'),
('oilcondensate', 'Oil/Condensate'),
('oilgas', 'Oil/Gas');

-- --------------------------------------------------------

--
-- Table structure for table `c_address`
--

DROP TABLE IF EXISTS `c_address`;
CREATE TABLE `c_address` (
  `id` int(11) NOT NULL,
  `street1` varchar(128) NOT NULL,
  `street2` varchar(128) DEFAULT NULL,
  `city` varchar(45) NOT NULL,
  `county` varchar(45) DEFAULT NULL,
  `country` char(2) NOT NULL,
  `zip` varchar(16) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='table holding addresses';

--
-- Dumping data for table `c_address`
--

INSERT INTO `c_address` (`id`, `street1`, `street2`, `city`, `county`, `country`, `zip`) VALUES
(1, 'CAO Street 1', '', 'Atlantis', '', 'AA', '111111'),
(2, 'Foc Street 1', '', 'Boton', '', 'AW', '11111'),
(3, 'Soc Street 1', 'Bl. C38', 'Dhaka', '', 'BD', '222222');

-- --------------------------------------------------------

--
-- Table structure for table `c_country`
--

DROP TABLE IF EXISTS `c_country`;
CREATE TABLE `c_country` (
  `country_id` int(5) NOT NULL,
  `short_name` varchar(80) NOT NULL DEFAULT '',
  `long_name` varchar(80) NOT NULL DEFAULT '',
  `iso2` char(2) NOT NULL,
  `iso3` char(3) NOT NULL,
  `numcode` varchar(6) DEFAULT NULL,
  `un_member` smallint(1) DEFAULT NULL,
  `calling_code` varchar(8) DEFAULT NULL,
  `cctld` varchar(5) DEFAULT NULL,
  `eu_member` smallint(2) NOT NULL DEFAULT '0',
  `has_shoreline` smallint(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=252 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `c_country`
--

INSERT INTO `c_country` (`country_id`, `short_name`, `long_name`, `iso2`, `iso3`, `numcode`, `un_member`, `calling_code`, `cctld`, `eu_member`, `has_shoreline`) VALUES
(1, 'Afghanistan', 'Islamic Republic of Afghanistan', 'AF', 'AFG', '004', 0, '93', '.af', 0, 0),
(2, 'Aland Islands', '&Aring;land Islands', 'AX', 'ALA', '248', 1, '358', '.ax', 0, 0),
(3, 'Albania', 'Republic of Albania', 'AL', 'ALB', '008', 0, '355', '.al', 0, 0),
(4, 'Algeria', 'People''s Democratic Republic of Algeria', 'DZ', 'DZA', '012', 0, '213', '.dz', 0, 0),
(5, 'American Samoa', 'American Samoa', 'AS', 'ASM', '016', 1, '1+684', '.as', 0, 0),
(6, 'Andorra', 'Principality of Andorra', 'AD', 'AND', '020', 0, '376', '.ad', 0, 0),
(7, 'Angola', 'Republic of Angola', 'AO', 'AGO', '024', 0, '244', '.ao', 0, 0),
(8, 'Anguilla', 'Anguilla', 'AI', 'AIA', '660', 1, '1+264', '.ai', 0, 0),
(9, 'Antarctica', 'Antarctica', 'AQ', 'ATA', '010', 1, '672', '.aq', 0, 0),
(10, 'Antigua and Barbuda', 'Antigua and Barbuda', 'AG', 'ATG', '028', 0, '1+268', '.ag', 0, 0),
(11, 'Argentina', 'Argentine Republic', 'AR', 'ARG', '032', 0, '54', '.ar', 0, 0),
(12, 'Armenia', 'Republic of Armenia', 'AM', 'ARM', '051', 0, '374', '.am', 0, 0),
(13, 'Aruba', 'Aruba', 'AW', 'ABW', '533', 1, '297', '.aw', 0, 0),
(14, 'Australia', 'Commonwealth of Australia', 'AU', 'AUS', '036', 0, '61', '.au', 0, 0),
(15, 'Austria', 'Republic of Austria', 'AT', 'AUT', '040', 0, '43', '.at', 1, 0),
(16, 'Azerbaijan', 'Republic of Azerbaijan', 'AZ', 'AZE', '031', 0, '994', '.az', 0, 0),
(17, 'Bahamas', 'Commonwealth of The Bahamas', 'BS', 'BHS', '044', 0, '1+242', '.bs', 0, 0),
(18, 'Bahrain', 'Kingdom of Bahrain', 'BH', 'BHR', '048', 0, '973', '.bh', 0, 0),
(19, 'Bangladesh', 'People''s Republic of Bangladesh', 'BD', 'BGD', '050', 0, '880', '.bd', 0, 0),
(20, 'Barbados', 'Barbados', 'BB', 'BRB', '052', 0, '1+246', '.bb', 0, 0),
(21, 'Belarus', 'Republic of Belarus', 'BY', 'BLR', '112', 0, '375', '.by', 0, 0),
(22, 'Belgium', 'Kingdom of Belgium', 'BE', 'BEL', '056', 0, '32', '.be', 1, 1),
(23, 'Belize', 'Belize', 'BZ', 'BLZ', '084', 0, '501', '.bz', 0, 0),
(24, 'Benin', 'Republic of Benin', 'BJ', 'BEN', '204', 0, '229', '.bj', 0, 0),
(25, 'Bermuda', 'Bermuda Islands', 'BM', 'BMU', '060', 1, '1+441', '.bm', 0, 0),
(26, 'Bhutan', 'Kingdom of Bhutan', 'BT', 'BTN', '064', 0, '975', '.bt', 0, 0),
(27, 'Bolivia', 'Plurinational State of Bolivia', 'BO', 'BOL', '068', 0, '591', '.bo', 0, 0),
(28, 'Bonaire, Sint Eustatius and Saba', 'Bonaire, Sint Eustatius and Saba', 'BQ', 'BES', '535', 1, '599', '.bq', 0, 0),
(29, 'Bosnia and Herzegovina', 'Bosnia and Herzegovina', 'BA', 'BIH', '070', 0, '387', '.ba', 0, 0),
(30, 'Botswana', 'Republic of Botswana', 'BW', 'BWA', '072', 0, '267', '.bw', 0, 0),
(31, 'Bouvet Island', 'Bouvet Island', 'BV', 'BVT', '074', 1, 'NONE', '.bv', 0, 0),
(32, 'Brazil', 'Federative Republic of Brazil', 'BR', 'BRA', '076', 0, '55', '.br', 0, 0),
(33, 'British Indian Ocean Territory', 'British Indian Ocean Territory', 'IO', 'IOT', '086', 1, '246', '.io', 0, 0),
(34, 'Brunei', 'Brunei Darussalam', 'BN', 'BRN', '096', 0, '673', '.bn', 0, 0),
(35, 'Bulgaria', 'Republic of Bulgaria', 'BG', 'BGR', '100', 0, '359', '.bg', 1, 1),
(36, 'Burkina Faso', 'Burkina Faso', 'BF', 'BFA', '854', 0, '226', '.bf', 0, 0),
(37, 'Burundi', 'Republic of Burundi', 'BI', 'BDI', '108', 0, '257', '.bi', 0, 0),
(38, 'Cambodia', 'Kingdom of Cambodia', 'KH', 'KHM', '116', 0, '855', '.kh', 0, 0),
(39, 'Cameroon', 'Republic of Cameroon', 'CM', 'CMR', '120', 0, '237', '.cm', 0, 0),
(40, 'Canada', 'Canada', 'CA', 'CAN', '124', 0, '1', '.ca', 0, 0),
(41, 'Cape Verde', 'Republic of Cape Verde', 'CV', 'CPV', '132', 0, '238', '.cv', 0, 0),
(42, 'Cayman Islands', 'The Cayman Islands', 'KY', 'CYM', '136', 1, '1+345', '.ky', 0, 0),
(43, 'Central African Republic', 'Central African Republic', 'CF', 'CAF', '140', 0, '236', '.cf', 0, 0),
(44, 'Chad', 'Republic of Chad', 'TD', 'TCD', '148', 0, '235', '.td', 0, 0),
(45, 'Chile', 'Republic of Chile', 'CL', 'CHL', '152', 0, '56', '.cl', 0, 0),
(46, 'China', 'People''s Republic of China', 'CN', 'CHN', '156', 0, '86', '.cn', 0, 0),
(47, 'Christmas Island', 'Christmas Island', 'CX', 'CXR', '162', 1, '61', '.cx', 0, 0),
(48, 'Cocos (Keeling) Islands', 'Cocos (Keeling) Islands', 'CC', 'CCK', '166', 1, '61', '.cc', 0, 0),
(49, 'Colombia', 'Republic of Colombia', 'CO', 'COL', '170', 0, '57', '.co', 0, 0),
(50, 'Comoros', 'Union of the Comoros', 'KM', 'COM', '174', 0, '269', '.km', 0, 0),
(51, 'Congo', 'Republic of the Congo', 'CG', 'COG', '178', 0, '242', '.cg', 0, 0),
(52, 'Cook Islands', 'Cook Islands', 'CK', 'COK', '184', 1, '682', '.ck', 0, 0),
(53, 'Costa Rica', 'Republic of Costa Rica', 'CR', 'CRI', '188', 0, '506', '.cr', 0, 0),
(54, 'Cote d''ivoire (Ivory Coast)', 'Republic of C&ocirc;te D''Ivoire (Ivory Coast)', 'CI', 'CIV', '384', 0, '225', '.ci', 0, 0),
(55, 'Croatia', 'Republic of Croatia', 'HR', 'HRV', '191', 0, '385', '.hr', 1, 1),
(56, 'Cuba', 'Republic of Cuba', 'CU', 'CUB', '192', 0, '53', '.cu', 0, 0),
(57, 'Curacao', 'Cura&ccedil;ao', 'CW', 'CUW', '531', 1, '599', '.cw', 0, 0),
(58, 'Cyprus', 'Republic of Cyprus', 'CY', 'CYP', '196', 0, '357', '.cy', 1, 1),
(59, 'Czech Republic', 'Czech Republic', 'CZ', 'CZE', '203', 0, '420', '.cz', 1, 0),
(60, 'Democratic Republic of the Congo', 'Democratic Republic of the Congo', 'CD', 'COD', '180', 0, '243', '.cd', 0, 0),
(61, 'Denmark', 'Kingdom of Denmark', 'DK', 'DNK', '208', 0, '45', '.dk', 1, 1),
(62, 'Djibouti', 'Republic of Djibouti', 'DJ', 'DJI', '262', 0, '253', '.dj', 0, 0),
(63, 'Dominica', 'Commonwealth of Dominica', 'DM', 'DMA', '212', 0, '1+767', '.dm', 0, 0),
(64, 'Dominican Republic', 'Dominican Republic', 'DO', 'DOM', '214', 0, '1+809, 8', '.do', 0, 0),
(65, 'Ecuador', 'Republic of Ecuador', 'EC', 'ECU', '218', 0, '593', '.ec', 0, 0),
(66, 'Egypt', 'Arab Republic of Egypt', 'EG', 'EGY', '818', 0, '20', '.eg', 0, 0),
(67, 'El Salvador', 'Republic of El Salvador', 'SV', 'SLV', '222', 0, '503', '.sv', 0, 0),
(68, 'Equatorial Guinea', 'Republic of Equatorial Guinea', 'GQ', 'GNQ', '226', 0, '240', '.gq', 0, 0),
(69, 'Eritrea', 'State of Eritrea', 'ER', 'ERI', '232', 0, '291', '.er', 0, 0),
(70, 'Estonia', 'Republic of Estonia', 'EE', 'EST', '233', 0, '372', '.ee', 1, 1),
(71, 'Ethiopia', 'Federal Democratic Republic of Ethiopia', 'ET', 'ETH', '231', 0, '251', '.et', 0, 0),
(72, 'Falkland Islands (Malvinas)', 'The Falkland Islands (Malvinas)', 'FK', 'FLK', '238', 1, '500', '.fk', 0, 0),
(73, 'Faroe Islands', 'The Faroe Islands', 'FO', 'FRO', '234', 1, '298', '.fo', 0, 0),
(74, 'Fiji', 'Republic of Fiji', 'FJ', 'FJI', '242', 0, '679', '.fj', 0, 0),
(75, 'Finland', 'Republic of Finland', 'FI', 'FIN', '246', 0, '358', '.fi', 1, 1),
(76, 'France', 'French Republic', 'FR', 'FRA', '250', 0, '33', '.fr', 1, 1),
(77, 'French Guiana', 'French Guiana', 'GF', 'GUF', '254', 1, '594', '.gf', 0, 0),
(78, 'French Polynesia', 'French Polynesia', 'PF', 'PYF', '258', 1, '689', '.pf', 0, 0),
(79, 'French Southern Territories', 'French Southern Territories', 'TF', 'ATF', '260', 1, NULL, '.tf', 0, 0),
(80, 'Gabon', 'Gabonese Republic', 'GA', 'GAB', '266', 0, '241', '.ga', 0, 0),
(81, 'Gambia', 'Republic of The Gambia', 'GM', 'GMB', '270', 0, '220', '.gm', 0, 0),
(82, 'Georgia', 'Georgia', 'GE', 'GEO', '268', 0, '995', '.ge', 0, 0),
(83, 'Germany', 'Federal Republic of Germany', 'DE', 'DEU', '276', 0, '49', '.de', 1, 1),
(84, 'Ghana', 'Republic of Ghana', 'GH', 'GHA', '288', 0, '233', '.gh', 0, 0),
(85, 'Gibraltar', 'Gibraltar', 'GI', 'GIB', '292', 1, '350', '.gi', 0, 0),
(86, 'Greece', 'Hellenic Republic', 'GR', 'GRC', '300', 0, '30', '.gr', 1, 1),
(87, 'Greenland', 'Greenland', 'GL', 'GRL', '304', 1, '299', '.gl', 0, 0),
(88, 'Grenada', 'Grenada', 'GD', 'GRD', '308', 0, '1+473', '.gd', 0, 0),
(89, 'Guadaloupe', 'Guadeloupe', 'GP', 'GLP', '312', 1, '590', '.gp', 0, 0),
(90, 'Guam', 'Guam', 'GU', 'GUM', '316', 1, '1+671', '.gu', 0, 0),
(91, 'Guatemala', 'Republic of Guatemala', 'GT', 'GTM', '320', 0, '502', '.gt', 0, 0),
(92, 'Guernsey', 'Guernsey', 'GG', 'GGY', '831', 1, '44', '.gg', 0, 0),
(93, 'Guinea', 'Republic of Guinea', 'GN', 'GIN', '324', 0, '224', '.gn', 0, 0),
(94, 'Guinea-Bissau', 'Republic of Guinea-Bissau', 'GW', 'GNB', '624', 0, '245', '.gw', 0, 0),
(95, 'Guyana', 'Co-operative Republic of Guyana', 'GY', 'GUY', '328', 0, '592', '.gy', 0, 0),
(96, 'Haiti', 'Republic of Haiti', 'HT', 'HTI', '332', 0, '509', '.ht', 0, 0),
(97, 'Heard Island and McDonald Islands', 'Heard Island and McDonald Islands', 'HM', 'HMD', '334', 1, 'NONE', '.hm', 0, 0),
(98, 'Honduras', 'Republic of Honduras', 'HN', 'HND', '340', 0, '504', '.hn', 0, 0),
(99, 'Hong Kong', 'Hong Kong', 'HK', 'HKG', '344', 1, '852', '.hk', 0, 0),
(100, 'Hungary', 'Hungary', 'HU', 'HUN', '348', 0, '36', '.hu', 1, 0),
(101, 'Iceland', 'Republic of Iceland', 'IS', 'ISL', '352', 0, '354', '.is', 0, 0),
(102, 'India', 'Republic of India', 'IN', 'IND', '356', 0, '91', '.in', 0, 0),
(103, 'Indonesia', 'Republic of Indonesia', 'ID', 'IDN', '360', 0, '62', '.id', 0, 0),
(104, 'Iran', 'Islamic Republic of Iran', 'IR', 'IRN', '364', 0, '98', '.ir', 0, 0),
(105, 'Iraq', 'Republic of Iraq', 'IQ', 'IRQ', '368', 0, '964', '.iq', 0, 0),
(106, 'Ireland', 'Ireland', 'IE', 'IRL', '372', 0, '353', '.ie', 1, 1),
(107, 'Isle of Man', 'Isle of Man', 'IM', 'IMN', '833', 1, '44', '.im', 0, 0),
(108, 'Israel', 'State of Israel', 'IL', 'ISR', '376', 0, '972', '.il', 0, 0),
(109, 'Italy', 'Italian Republic', 'IT', 'ITA', '380', 0, '39', '.jm', 1, 1),
(110, 'Jamaica', 'Jamaica', 'JM', 'JAM', '388', 0, '1+876', '.jm', 0, 0),
(111, 'Japan', 'Japan', 'JP', 'JPN', '392', 0, '81', '.jp', 0, 0),
(112, 'Jersey', 'The Bailiwick of Jersey', 'JE', 'JEY', '832', 1, '44', '.je', 0, 0),
(113, 'Jordan', 'Hashemite Kingdom of Jordan', 'JO', 'JOR', '400', 0, '962', '.jo', 0, 0),
(114, 'Kazakhstan', 'Republic of Kazakhstan', 'KZ', 'KAZ', '398', 0, '7', '.kz', 0, 0),
(115, 'Kenya', 'Republic of Kenya', 'KE', 'KEN', '404', 0, '254', '.ke', 0, 0),
(116, 'Kiribati', 'Republic of Kiribati', 'KI', 'KIR', '296', 0, '686', '.ki', 0, 0),
(117, 'Kosovo', 'Republic of Kosovo', 'XK', '---', '---', 1, '381', '', 0, 0),
(118, 'Kuwait', 'State of Kuwait', 'KW', 'KWT', '414', 0, '965', '.kw', 0, 0),
(119, 'Kyrgyzstan', 'Kyrgyz Republic', 'KG', 'KGZ', '417', 0, '996', '.kg', 0, 0),
(120, 'Laos', 'Lao People''s Democratic Republic', 'LA', 'LAO', '418', 0, '856', '.la', 0, 0),
(121, 'Latvia', 'Republic of Latvia', 'LV', 'LVA', '428', 0, '371', '.lv', 1, 1),
(122, 'Lebanon', 'Republic of Lebanon', 'LB', 'LBN', '422', 0, '961', '.lb', 0, 0),
(123, 'Lesotho', 'Kingdom of Lesotho', 'LS', 'LSO', '426', 0, '266', '.ls', 0, 0),
(124, 'Liberia', 'Republic of Liberia', 'LR', 'LBR', '430', 0, '231', '.lr', 0, 0),
(125, 'Libya', 'Libya', 'LY', 'LBY', '434', 0, '218', '.ly', 0, 0),
(126, 'Liechtenstein', 'Principality of Liechtenstein', 'LI', 'LIE', '438', 0, '423', '.li', 0, 0),
(127, 'Lithuania', 'Republic of Lithuania', 'LT', 'LTU', '440', 0, '370', '.lt', 1, 1),
(128, 'Luxembourg', 'Grand Duchy of Luxembourg', 'LU', 'LUX', '442', 0, '352', '.lu', 1, 0),
(129, 'Macao', 'The Macao Special Administrative Region', 'MO', 'MAC', '446', 1, '853', '.mo', 0, 0),
(130, 'Macedonia', 'The Former Yugoslav Republic of Macedonia', 'MK', 'MKD', '807', 0, '389', '.mk', 0, 0),
(131, 'Madagascar', 'Republic of Madagascar', 'MG', 'MDG', '450', 0, '261', '.mg', 0, 0),
(132, 'Malawi', 'Republic of Malawi', 'MW', 'MWI', '454', 0, '265', '.mw', 0, 0),
(133, 'Malaysia', 'Malaysia', 'MY', 'MYS', '458', 0, '60', '.my', 0, 0),
(134, 'Maldives', 'Republic of Maldives', 'MV', 'MDV', '462', 0, '960', '.mv', 0, 0),
(135, 'Mali', 'Republic of Mali', 'ML', 'MLI', '466', 0, '223', '.ml', 0, 0),
(136, 'Malta', 'Republic of Malta', 'MT', 'MLT', '470', 0, '356', '.mt', 1, 1),
(137, 'Marshall Islands', 'Republic of the Marshall Islands', 'MH', 'MHL', '584', 0, '692', '.mh', 0, 0),
(138, 'Martinique', 'Martinique', 'MQ', 'MTQ', '474', 1, '596', '.mq', 0, 0),
(139, 'Mauritania', 'Islamic Republic of Mauritania', 'MR', 'MRT', '478', 0, '222', '.mr', 0, 0),
(140, 'Mauritius', 'Republic of Mauritius', 'MU', 'MUS', '480', 0, '230', '.mu', 0, 0),
(141, 'Mayotte', 'Mayotte', 'YT', 'MYT', '175', 1, '262', '.yt', 0, 0),
(142, 'Mexico', 'United Mexican States', 'MX', 'MEX', '484', 0, '52', '.mx', 0, 0),
(143, 'Micronesia', 'Federated States of Micronesia', 'FM', 'FSM', '583', 0, '691', '.fm', 0, 0),
(144, 'Moldava', 'Republic of Moldova', 'MD', 'MDA', '498', 0, '373', '.md', 0, 0),
(145, 'Monaco', 'Principality of Monaco', 'MC', 'MCO', '492', 0, '377', '.mc', 0, 0),
(146, 'Mongolia', 'Mongolia', 'MN', 'MNG', '496', 0, '976', '.mn', 0, 0),
(147, 'Montenegro', 'Montenegro', 'ME', 'MNE', '499', 0, '382', '.me', 0, 0),
(148, 'Montserrat', 'Montserrat', 'MS', 'MSR', '500', 1, '1+664', '.ms', 0, 0),
(149, 'Morocco', 'Kingdom of Morocco', 'MA', 'MAR', '504', 0, '212', '.ma', 0, 0),
(150, 'Mozambique', 'Republic of Mozambique', 'MZ', 'MOZ', '508', 0, '258', '.mz', 0, 0),
(151, 'Myanmar (Burma)', 'Republic of the Union of Myanmar', 'MM', 'MMR', '104', 0, '95', '.mm', 0, 0),
(152, 'Namibia', 'Republic of Namibia', 'NA', 'NAM', '516', 0, '264', '.na', 0, 0),
(153, 'Nauru', 'Republic of Nauru', 'NR', 'NRU', '520', 0, '674', '.nr', 0, 0),
(154, 'Nepal', 'Federal Democratic Republic of Nepal', 'NP', 'NPL', '524', 0, '977', '.np', 0, 0),
(155, 'Netherlands', 'Kingdom of the Netherlands', 'NL', 'NLD', '528', 0, '31', '.nl', 1, 1),
(156, 'New Caledonia', 'New Caledonia', 'NC', 'NCL', '540', 1, '687', '.nc', 0, 0),
(157, 'New Zealand', 'New Zealand', 'NZ', 'NZL', '554', 0, '64', '.nz', 0, 0),
(158, 'Nicaragua', 'Republic of Nicaragua', 'NI', 'NIC', '558', 0, '505', '.ni', 0, 0),
(159, 'Niger', 'Republic of Niger', 'NE', 'NER', '562', 0, '227', '.ne', 0, 0),
(160, 'Nigeria', 'Federal Republic of Nigeria', 'NG', 'NGA', '566', 0, '234', '.ng', 0, 0),
(161, 'Niue', 'Niue', 'NU', 'NIU', '570', 1, '683', '.nu', 0, 0),
(162, 'Norfolk Island', 'Norfolk Island', 'NF', 'NFK', '574', 1, '672', '.nf', 0, 0),
(163, 'North Korea', 'Democratic People''s Republic of Korea', 'KP', 'PRK', '408', 0, '850', '.kp', 0, 0),
(164, 'Northern Mariana Islands', 'Northern Mariana Islands', 'MP', 'MNP', '580', 1, '1+670', '.mp', 0, 0),
(165, 'Norway', 'Kingdom of Norway', 'NO', 'NOR', '578', 0, '47', '.no', 0, 0),
(166, 'Oman', 'Sultanate of Oman', 'OM', 'OMN', '512', 0, '968', '.om', 0, 0),
(167, 'Pakistan', 'Islamic Republic of Pakistan', 'PK', 'PAK', '586', 0, '92', '.pk', 0, 0),
(168, 'Palau', 'Republic of Palau', 'PW', 'PLW', '585', 0, '680', '.pw', 0, 0),
(169, 'Palestine', 'State of Palestine (or Occupied Palestinian Territory)', 'PS', 'PSE', '275', 1, '970', '.ps', 0, 0),
(170, 'Panama', 'Republic of Panama', 'PA', 'PAN', '591', 0, '507', '.pa', 0, 0),
(171, 'Papua New Guinea', 'Independent State of Papua New Guinea', 'PG', 'PNG', '598', 0, '675', '.pg', 0, 0),
(172, 'Paraguay', 'Republic of Paraguay', 'PY', 'PRY', '600', 0, '595', '.py', 0, 0),
(173, 'Peru', 'Republic of Peru', 'PE', 'PER', '604', 0, '51', '.pe', 0, 0),
(174, 'Phillipines', 'Republic of the Philippines', 'PH', 'PHL', '608', 0, '63', '.ph', 0, 0),
(175, 'Pitcairn', 'Pitcairn', 'PN', 'PCN', '612', 1, 'NONE', '.pn', 0, 0),
(176, 'Poland', 'Republic of Poland', 'PL', 'POL', '616', 0, '48', '.pl', 1, 1),
(177, 'Portugal', 'Portuguese Republic', 'PT', 'PRT', '620', 0, '351', '.pt', 1, 1),
(178, 'Puerto Rico', 'Commonwealth of Puerto Rico', 'PR', 'PRI', '630', 1, '1+939', '.pr', 0, 0),
(179, 'Qatar', 'State of Qatar', 'QA', 'QAT', '634', 0, '974', '.qa', 0, 0),
(180, 'Reunion', 'R&eacute;union', 'RE', 'REU', '638', 1, '262', '.re', 0, 0),
(181, 'Romania', 'Romania', 'RO', 'ROU', '642', 0, '40', '.ro', 1, 1),
(182, 'Russia', 'Russian Federation', 'RU', 'RUS', '643', 0, '7', '.ru', 0, 0),
(183, 'Rwanda', 'Republic of Rwanda', 'RW', 'RWA', '646', 0, '250', '.rw', 0, 0),
(184, 'Saint Barthelemy', 'Saint Barth&eacute;lemy', 'BL', 'BLM', '652', 1, '590', '.bl', 0, 0),
(185, 'Saint Helena', 'Saint Helena, Ascension and Tristan da Cunha', 'SH', 'SHN', '654', 1, '290', '.sh', 0, 0),
(186, 'Saint Kitts and Nevis', 'Federation of Saint Christopher and Nevis', 'KN', 'KNA', '659', 0, '1+869', '.kn', 0, 0),
(187, 'Saint Lucia', 'Saint Lucia', 'LC', 'LCA', '662', 0, '1+758', '.lc', 0, 0),
(188, 'Saint Martin', 'Saint Martin', 'MF', 'MAF', '663', 1, '590', '.mf', 0, 0),
(189, 'Saint Pierre and Miquelon', 'Saint Pierre and Miquelon', 'PM', 'SPM', '666', 1, '508', '.pm', 0, 0),
(190, 'Saint Vincent and the Grenadines', 'Saint Vincent and the Grenadines', 'VC', 'VCT', '670', 0, '1+784', '.vc', 0, 0),
(191, 'Samoa', 'Independent State of Samoa', 'WS', 'WSM', '882', 0, '685', '.ws', 0, 0),
(192, 'San Marino', 'Republic of San Marino', 'SM', 'SMR', '674', 0, '378', '.sm', 0, 0),
(193, 'Sao Tome and Principe', 'Democratic Republic of S&atilde;o Tom&eacute; and Pr&iacute;ncipe', 'ST', 'STP', '678', 0, '239', '.st', 0, 0),
(194, 'Saudi Arabia', 'Kingdom of Saudi Arabia', 'SA', 'SAU', '682', 0, '966', '.sa', 0, 0),
(195, 'Senegal', 'Republic of Senegal', 'SN', 'SEN', '686', 0, '221', '.sn', 0, 0),
(196, 'Serbia', 'Republic of Serbia', 'RS', 'SRB', '688', 0, '381', '.rs', 0, 0),
(197, 'Seychelles', 'Republic of Seychelles', 'SC', 'SYC', '690', 0, '248', '.sc', 0, 0),
(198, 'Sierra Leone', 'Republic of Sierra Leone', 'SL', 'SLE', '694', 0, '232', '.sl', 0, 0),
(199, 'Singapore', 'Republic of Singapore', 'SG', 'SGP', '702', 0, '65', '.sg', 0, 0),
(200, 'Sint Maarten', 'Sint Maarten', 'SX', 'SXM', '534', 1, '1+721', '.sx', 0, 0),
(201, 'Slovakia', 'Slovak Republic', 'SK', 'SVK', '703', 0, '421', '.sk', 1, 0),
(202, 'Slovenia', 'Republic of Slovenia', 'SI', 'SVN', '705', 0, '386', '.si', 1, 1),
(203, 'Solomon Islands', 'Solomon Islands', 'SB', 'SLB', '090', 0, '677', '.sb', 0, 0),
(204, 'Somalia', 'Somali Republic', 'SO', 'SOM', '706', 0, '252', '.so', 0, 0),
(205, 'South Africa', 'Republic of South Africa', 'ZA', 'ZAF', '710', 0, '27', '.za', 0, 0),
(206, 'South Georgia and the South Sandwich Islands', 'South Georgia and the South Sandwich Islands', 'GS', 'SGS', '239', 1, '500', '.gs', 0, 0),
(207, 'South Korea', 'Republic of Korea', 'KR', 'KOR', '410', 0, '82', '.kr', 0, 0),
(208, 'South Sudan', 'Republic of South Sudan', 'SS', 'SSD', '728', 0, '211', '.ss', 0, 0),
(209, 'Spain', 'Kingdom of Spain', 'ES', 'ESP', '724', 0, '34', '.es', 1, 1),
(210, 'Sri Lanka', 'Democratic Socialist Republic of Sri Lanka', 'LK', 'LKA', '144', 0, '94', '.lk', 0, 0),
(211, 'Sudan', 'Republic of the Sudan', 'SD', 'SDN', '729', 0, '249', '.sd', 0, 0),
(212, 'Suriname', 'Republic of Suriname', 'SR', 'SUR', '740', 0, '597', '.sr', 0, 0),
(213, 'Svalbard and Jan Mayen', 'Svalbard and Jan Mayen', 'SJ', 'SJM', '744', 1, '47', '.sj', 0, 0),
(214, 'Swaziland', 'Kingdom of Swaziland', 'SZ', 'SWZ', '748', 0, '268', '.sz', 0, 0),
(215, 'Sweden', 'Kingdom of Sweden', 'SE', 'SWE', '752', 0, '46', '.se', 1, 1),
(216, 'Switzerland', 'Swiss Confederation', 'CH', 'CHE', '756', 0, '41', '.ch', 0, 0),
(217, 'Syria', 'Syrian Arab Republic', 'SY', 'SYR', '760', 0, '963', '.sy', 0, 0),
(218, 'Taiwan', 'Republic of China (Taiwan)', 'TW', 'TWN', '158', 1, '886', '.tw', 0, 0),
(219, 'Tajikistan', 'Republic of Tajikistan', 'TJ', 'TJK', '762', 0, '992', '.tj', 0, 0),
(220, 'Tanzania', 'United Republic of Tanzania', 'TZ', 'TZA', '834', 0, '255', '.tz', 0, 0),
(221, 'Thailand', 'Kingdom of Thailand', 'TH', 'THA', '764', 0, '66', '.th', 0, 0),
(222, 'Timor-Leste (East Timor)', 'Democratic Republic of Timor-Leste', 'TL', 'TLS', '626', 0, '670', '.tl', 0, 0),
(223, 'Togo', 'Togolese Republic', 'TG', 'TGO', '768', 0, '228', '.tg', 0, 0),
(224, 'Tokelau', 'Tokelau', 'TK', 'TKL', '772', 1, '690', '.tk', 0, 0),
(225, 'Tonga', 'Kingdom of Tonga', 'TO', 'TON', '776', 0, '676', '.to', 0, 0),
(226, 'Trinidad and Tobago', 'Republic of Trinidad and Tobago', 'TT', 'TTO', '780', 0, '1+868', '.tt', 0, 0),
(227, 'Tunisia', 'Republic of Tunisia', 'TN', 'TUN', '788', 0, '216', '.tn', 0, 0),
(228, 'Turkey', 'Republic of Turkey', 'TR', 'TUR', '792', 0, '90', '.tr', 0, 0),
(229, 'Turkmenistan', 'Turkmenistan', 'TM', 'TKM', '795', 0, '993', '.tm', 0, 0),
(230, 'Turks and Caicos Islands', 'Turks and Caicos Islands', 'TC', 'TCA', '796', 1, '1+649', '.tc', 0, 0),
(231, 'Tuvalu', 'Tuvalu', 'TV', 'TUV', '798', 0, '688', '.tv', 0, 0),
(232, 'Uganda', 'Republic of Uganda', 'UG', 'UGA', '800', 0, '256', '.ug', 0, 0),
(233, 'Ukraine', 'Ukraine', 'UA', 'UKR', '804', 0, '380', '.ua', 0, 0),
(234, 'United Arab Emirates', 'United Arab Emirates', 'AE', 'ARE', '784', 0, '971', '.ae', 0, 0),
(235, 'United Kingdom', 'United Kingdom of Great Britain and Nothern Ireland', 'GB', 'GBR', '826', 0, '44', '.uk', 1, 1),
(236, 'United States', 'United States of America', 'US', 'USA', '840', 0, '1', '.us', 0, 0),
(237, 'United States Minor Outlying Islands', 'United States Minor Outlying Islands', 'UM', 'UMI', '581', 1, 'NONE', 'NONE', 0, 0),
(238, 'Uruguay', 'Eastern Republic of Uruguay', 'UY', 'URY', '858', 0, '598', '.uy', 0, 0),
(239, 'Uzbekistan', 'Republic of Uzbekistan', 'UZ', 'UZB', '860', 0, '998', '.uz', 0, 0),
(240, 'Vanuatu', 'Republic of Vanuatu', 'VU', 'VUT', '548', 0, '678', '.vu', 0, 0),
(241, 'Vatican City', 'State of the Vatican City', 'VA', 'VAT', '336', 1, '39', '.va', 0, 0),
(242, 'Venezuela', 'Bolivarian Republic of Venezuela', 'VE', 'VEN', '862', 0, '58', '.ve', 0, 0),
(243, 'Vietnam', 'Socialist Republic of Vietnam', 'VN', 'VNM', '704', 0, '84', '.vn', 0, 0),
(244, 'Virgin Islands, British', 'British Virgin Islands', 'VG', 'VGB', '092', 1, '1+284', '.vg', 0, 0),
(245, 'Virgin Islands, US', 'Virgin Islands of the United States', 'VI', 'VIR', '850', 1, '1+340', '.vi', 0, 0),
(246, 'Wallis and Futuna', 'Wallis and Futuna', 'WF', 'WLF', '876', 1, '681', '.wf', 0, 0),
(247, 'Western Sahara', 'Western Sahara', 'EH', 'ESH', '732', 1, '212', '.eh', 0, 0),
(248, 'Yemen', 'Republic of Yemen', 'YE', 'YEM', '887', 0, '967', '.ye', 0, 0),
(249, 'Zambia', 'Republic of Zambia', 'ZM', 'ZMB', '894', 0, '260', '.zm', 0, 0),
(250, 'Zimbabwe', 'Republic of Zimbabwe', 'ZW', 'ZWE', '716', 0, '263', '.zw', 0, 0),
(251, 'Atlantida', 'Kingdom of Atlantida', 'AA', 'AAA', '999', 0, '999', '.aa', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `event_classifications`
--

DROP TABLE IF EXISTS `event_classifications`;
CREATE TABLE `event_classifications` (
  `id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `is_a` tinyint(1) NOT NULL DEFAULT '0',
  `is_b` tinyint(1) NOT NULL DEFAULT '0',
  `is_c` tinyint(1) NOT NULL DEFAULT '0',
  `is_d` tinyint(1) NOT NULL DEFAULT '0',
  `is_e` tinyint(1) NOT NULL DEFAULT '0',
  `is_f` tinyint(1) NOT NULL DEFAULT '0',
  `is_g` tinyint(1) NOT NULL DEFAULT '0',
  `is_h` tinyint(1) NOT NULL DEFAULT '0',
  `is_i` tinyint(1) NOT NULL DEFAULT '0',
  `is_j` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified_at` int(11) DEFAULT NULL,
  `modified_by` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `session_desc` varchar(45) NOT NULL,
  `ca_rejection_desc` mediumtext,
  `ca_status` tinyint(1) DEFAULT NULL COMMENT 'this field is changed if the ca rejects/accepts the report',
  `del_status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='the table holds the event classifications; it is the entry point of a draft';

--
-- Triggers `event_classifications`
--
DROP TRIGGER IF EXISTS `event_classifications_AFTER_INSERT`;
DELIMITER $$
CREATE TRIGGER `event_classifications_AFTER_INSERT` AFTER INSERT ON `event_classifications`
 FOR EACH ROW begin



if (NEW.is_a = 1) then

	-- insert a new record in s_a and set its [event_classification_id] to NEW.id

	INSERT INTO `s_a`

	(

		`event_classification_id`

	)

	VALUES

	(

		NEW.id

	);

    

end if;



if (NEW.is_b = 1) then

	-- insert a new record in s_b and set its [event_classification_id] to NEW.id

	INSERT INTO `s_b`

	(

		`event_classification_id`

	)

	VALUES

	(

		NEW.id

	);

    

end if;



if (NEW.is_c = 1) then

	-- insert a new record in s_c and set its [event_classification_id] to NEW.id

	INSERT INTO `s_c`

	(

		`event_classification_id`

	)

	VALUES

	(

		NEW.id

	);

    

end if;



if (NEW.is_d = 1) then

	-- insert a new record in s_d and set its [event_classification_id] to NEW.id

	INSERT INTO `s_d`

	(

		`event_classification_id`

	)

	VALUES

	(

		NEW.id

	);

    

end if;





if (NEW.is_e = 1) then

	-- insert a new record in s_e and set its [event_classification_id] to NEW.id

	INSERT INTO `s_e`

	(

		`event_classification_id`

	)

	VALUES

	(

		NEW.id

	);

    

end if;





if (NEW.is_f = 1) then

	-- insert a new record in s_f and set its [event_classification_id] to NEW.id

	INSERT INTO `s_f`

	(

		`event_classification_id`

	)

	VALUES

	(

		NEW.id

	);

    

end if;







if (NEW.is_i = 1) then

	-- insert a new record in s_i and set its [event_classification_id] to NEW.id

	INSERT INTO `s_i`

	(

		`event_classification_id`

	)

	VALUES

	(

		NEW.id

	);

    

end if;



if (NEW.is_j = 1) then

	-- insert a new record in s_j and set its [event_classification_id] to NEW.id

	INSERT INTO `s_j`

	(

		`event_classification_id`

	)

	VALUES

	(

		NEW.id

	);

    

end if;



end
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `event_classifications_AFTER_UPDATE`;
DELIMITER $$
CREATE TRIGGER `event_classifications_AFTER_UPDATE` AFTER UPDATE ON `event_classifications`
 FOR EACH ROW begin



if NEW.is_a = 1 then

	-- create the new s_a record if not existing yet

	IF NOT EXISTS (select id from    `s_a` where event_classification_id = NEW.id) THEN

		insert into  `s_a` (`event_classification_id`) values (NEW.id);

	END IF;

else

	-- delete the s_a record

    DELETE FROM  `s_a` WHERE `event_classification_id`=NEW.id;

end if;



if NEW.is_b = 1 then

	-- create the new s_b record if not existing yet

	IF NOT EXISTS (select id from   `s_b` where event_classification_id = NEW.id) THEN

		insert into  `s_b` (`event_classification_id`) values (NEW.id);

	END IF;

else

	-- delete the s_b record

    DELETE FROM  `s_b` WHERE `event_classification_id`=NEW.id;

end if;



if NEW.is_c = 1 then

	-- create the new s_c record if not existing yet

	IF NOT EXISTS (select id from   `s_c` where event_classification_id = NEW.id) THEN

		insert into  `s_c` (`event_classification_id`) values (NEW.id);

	END IF;

else

	-- delete the s_c record

    DELETE FROM  `s_c` WHERE `event_classification_id`=NEW.id;

end if;



if NEW.is_d = 1 then

	-- create the new s_d record if not existing yet

	IF NOT EXISTS (select id from   `s_d` where event_classification_id = NEW.id) THEN

		insert into  `s_d` (`event_classification_id`) values (NEW.id);

	END IF;

else

	-- delete the s_d record

    DELETE FROM  `s_d` WHERE `event_classification_id`=NEW.id;

end if;



if NEW.is_e = 1 then

	-- create the new s_e record if not existing yet

	IF NOT EXISTS (select id from   `s_e` where event_classification_id = NEW.id) THEN

		insert into  `s_e` (`event_classification_id`) values (NEW.id);

	END IF;

else

	-- delete the s_e record

    DELETE FROM  `s_e` WHERE `event_classification_id`=NEW.id;

end if;



if NEW.is_f = 1 then

	-- create the new s_f record if not existing yet

	IF NOT EXISTS (select id from   `s_f` where event_classification_id = NEW.id) THEN

		insert into  `s_f` (`event_classification_id`) values (NEW.id);

	END IF;

else

	-- delete the s_e record

    DELETE FROM  `s_f` WHERE `event_classification_id`=NEW.id;

end if;





if NEW.is_i = 1 then

	-- create the new s_i record if not existing yet

	IF NOT EXISTS (select id from   `s_i` where event_classification_id = NEW.id) THEN

		insert into  `s_i` (`event_classification_id`) values (NEW.id);

	END IF;

else

	-- delete the s_i record

    DELETE FROM  `s_i` WHERE `event_classification_id`=NEW.id;

end if;



if NEW.is_j = 1 then

	-- create the new s_j record if not existing yet

	IF NOT EXISTS (select id from   `s_j` where event_classification_id = NEW.id) THEN

		insert into  `s_j` (`event_classification_id`) values (NEW.id);

	END IF;

else

	-- delete the s_j record

    DELETE FROM  `s_j` WHERE `event_classification_id`=NEW.id;

end if;





end
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `event_classifications_BEFORE_UPDATE`;
DELIMITER $$
CREATE TRIGGER `event_classifications_BEFORE_UPDATE` BEFORE UPDATE ON `event_classifications`
 FOR EACH ROW begin

end
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `event_declaration`
--

DROP TABLE IF EXISTS `event_declaration`;
CREATE TABLE `event_declaration` (
  `id` int(11) NOT NULL,
  `event_date_time` datetime DEFAULT NULL,
  `event_deadline` datetime DEFAULT NULL,
  `operator_id` char(12) NOT NULL,
  `installation_id` char(12) NOT NULL,
  `operator` varchar(128) DEFAULT NULL COMMENT 'operator/owner of the installation',
  `field_name` varchar(128) DEFAULT NULL COMMENT 'operation field name / code (if relevant)',
  `raporteur_name` varchar(64) NOT NULL COMMENT 'full name of the person reporting the event',
  `raporteur_role` varchar(128) DEFAULT NULL COMMENT 'the role (in the organization) of the raporteur',
  `contact_tel` varchar(32) DEFAULT NULL,
  `contact_email` varchar(128) NOT NULL,
  `raporteur_id` int(11) DEFAULT NULL,
  `created` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated` timestamp NULL DEFAULT NULL,
  `modified_by_id` int(11) DEFAULT NULL,
  `event_name` varchar(64) NOT NULL,
  `status` int(11) DEFAULT NULL,
  `locked_by` int(11) DEFAULT NULL COMMENT 'the id of the user currently locking (editing) the record',
  `locked_at` timestamp NULL DEFAULT NULL,
  `signed_by` int(11) DEFAULT NULL,
  `signed_at` timestamp NULL DEFAULT NULL,
  `del_status` int(11) NOT NULL DEFAULT '0' COMMENT 'delete status',
  `modified_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table holding the event declarations';

--
-- Triggers `event_declaration`
--
DROP TRIGGER IF EXISTS `event_declaration_BEFORE_UPDATE`;
DELIMITER $$
CREATE TRIGGER `event_declaration_BEFORE_UPDATE` BEFORE UPDATE ON `event_declaration`
 FOR EACH ROW SET NEW.updated =NOW()
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `holidays`
--

DROP TABLE IF EXISTS `holidays`;
CREATE TABLE `holidays` (
  `id` int(11) NOT NULL,
  `day` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) DEFAULT '0',
  `name` varchar(45) NOT NULL,
  `repeat` int(1) DEFAULT '0',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='this table holds the annual holidays';

-- --------------------------------------------------------

--
-- Table structure for table `installations`
--

DROP TABLE IF EXISTS `installations`;
CREATE TABLE `installations` (
  `id` char(12) NOT NULL,
  `operator_id` char(12) NOT NULL,
  `name` varchar(64) NOT NULL COMMENT 'Name of installation',
  `type` varchar(8) NOT NULL COMMENT 'Type of installation',
  `type_other` varchar(45) DEFAULT NULL COMMENT 'Type (if other)',
  `code` varchar(32) NOT NULL,
  `flag` varchar(45) DEFAULT NULL COMMENT 'Flag (if MODU)',
  `year_of_construction` int(11) DEFAULT NULL COMMENT 'Year of construction',
  `number_of_beds` int(11) DEFAULT NULL COMMENT 'Number of beds',
  `op_status` int(11) NOT NULL COMMENT 'operational status of the installation',
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_at` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='installations table';

--
-- Dumping data for table `installations`
--

INSERT INTO `installations` (`id`, `operator_id`, `name`, `type`, `type_other`, `code`, `flag`, `year_of_construction`, `number_of_beds`, `op_status`, `status`, `created_at`, `created_by`, `modified_at`, `modified_by`) VALUES
('IN1454683860', 'OO1454683642', 'FOC Mobile 1', 'MODU', NULL, 'FM1', 'JM', 1992, 53, 10, 10, 1454683860, 89, 1454683979, 89),
('IN1454683947', 'OO1454683642', 'FOC Fixed 1', 'FMI', NULL, 'FF1', '', 2002, 75, 10, 10, 1454683947, 89, NULL, NULL),
('IN1454684037', 'OO1454683755', 'SOC Mobile 1', 'MODU', '', 'SM1', 'BN', 2005, 35, 10, 10, 1454684037, 89, 1477412849, 89);

-- --------------------------------------------------------

--
-- Table structure for table `installation_operations`
--

DROP TABLE IF EXISTS `installation_operations`;
CREATE TABLE `installation_operations` (
  `inst_code` varchar(32) NOT NULL,
  `ca_country` char(2) NOT NULL,
  `field_code` varchar(45) DEFAULT NULL,
  `op_start` datetime DEFAULT NULL,
  `op_end` datetime DEFAULT NULL,
  `status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='installation operations table';

-- --------------------------------------------------------

--
-- Table structure for table `installation_types`
--

DROP TABLE IF EXISTS `installation_types`;
CREATE TABLE `installation_types` (
  `type` varchar(8) NOT NULL,
  `name` varchar(45) NOT NULL,
  `mobile` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `installation_types`
--

INSERT INTO `installation_types` (`type`, `name`, `mobile`) VALUES
('FMI', 'Fixed Manned Installation', 0),
('FUI', 'Fixed Unmanned Installation', 0),
('MOA', 'Mobile, non-production', 1),
('MODU', 'Mobile Drilling Unit', 1),
('NUI', 'Normally Unattended Installation', 0);

-- --------------------------------------------------------

--
-- Table structure for table `leak_cause`
--

DROP TABLE IF EXISTS `leak_cause`;
CREATE TABLE `leak_cause` (
  `num_code` int(11) NOT NULL,
  `cause` varchar(128) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table holding the leak causes';

--
-- Dumping data for table `leak_cause`
--

INSERT INTO `leak_cause` (`num_code`, `cause`, `category_id`) VALUES
(1, 'none', 0),
(2, 'other', 0),
(3, 'not applicable', 0),
(1001, 'Failure related to design', 1),
(1101, 'Internal corrosion', 2),
(1102, 'External corrosion', 2),
(1103, 'Mechanical failure due to fatigue', 2),
(1104, 'Mechanical failure due to wear out', 2),
(1105, 'Erosion', 2),
(1106, 'Material defect', 2),
(1201, 'Incorrectly fitted', 3),
(1202, 'Left open', 3),
(1203, 'Improper inspection', 3),
(1204, 'Improper testing', 3),
(1205, 'Improper operation', 3),
(1206, 'Improper maintenance', 3),
(1207, 'Dropped object', 3),
(1208, 'Other impact', 3),
(1209, 'Opened when containing HC', 3),
(1301, 'Non-compliance with procedures', 4),
(1302, 'Non-compliance with permit of work', 4),
(1303, 'Deficient procedure', 4);

-- --------------------------------------------------------

--
-- Table structure for table `leak_cause_categories`
--

DROP TABLE IF EXISTS `leak_cause_categories`;
CREATE TABLE `leak_cause_categories` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `leak_cause_categories`
--

INSERT INTO `leak_cause_categories` (`id`, `name`) VALUES
(0, 'Shared'),
(1, 'Design'),
(2, 'Equipment'),
(3, 'Operation'),
(4, 'Procedural');

-- --------------------------------------------------------

--
-- Table structure for table `organizations`
--

DROP TABLE IF EXISTS `organizations`;
CREATE TABLE `organizations` (
  `id` char(12) NOT NULL,
  `organization_name` varchar(128) NOT NULL,
  `organization_acronym` varchar(8) NOT NULL,
  `organization_address` int(11) NOT NULL,
  `organization_phone` varchar(16) NOT NULL,
  `organization_email` varchar(45) NOT NULL,
  `organization_fax` varchar(16) DEFAULT NULL,
  `organization_phone2` varchar(16) DEFAULT NULL,
  `organization_web` varchar(128) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `modified_at` timestamp NULL DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `organizations`
--

INSERT INTO `organizations` (`id`, `organization_name`, `organization_acronym`, `organization_address`, `organization_phone`, `organization_email`, `organization_fax`, `organization_phone2`, `organization_web`, `created_at`, `created_by`, `modified_at`, `modified_by`, `status`) VALUES
('CA1454683148', 'The Competent Authority Organization', 'CAO', 1, '+111 11 11 11', 'contact@cao.nomail', '', '', 'http://www.cao.noaddres.com', '2016-02-02 13:39:08', 89, '0000-00-00 00:00:00', 89, 10),
('OO1454683642', 'First Offshore Company', 'FOC', 2, '+111 11 11', 'contact@foc.nomail', '', '', 'http://foc.nodomain', '2016-02-02 13:47:22', 89, '0000-00-00 00:00:00', 89, 10),
('OO1454683755', 'Second Offshore Company', 'SOC', 3, '+999 99 99 99', 'soc@nomail.com', '', '', 'http://www.soc.nodomain', '2016-02-02 13:49:15', 89, '2016-02-01 12:52:24', 89, 10);

-- --------------------------------------------------------

--
-- Table structure for table `reporting_history`
--

DROP TABLE IF EXISTS `reporting_history`;
CREATE TABLE `reporting_history` (
  `id` int(11) NOT NULL,
  `evt_id` int(11) NOT NULL,
  `draft_id` int(11) DEFAULT NULL,
  `event` int(11) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `visibility` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `request`
--

DROP TABLE IF EXISTS `request`;
CREATE TABLE `request` (
  `id` int(11) NOT NULL,
  `request_type` int(11) NOT NULL,
  `from` int(11) NOT NULL DEFAULT '-1',
  `content` text,
  `status` int(11) DEFAULT NULL,
  `processed_by_id` int(11) DEFAULT NULL,
  `processed_at` int(11) DEFAULT NULL,
  `justification` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `request_types`
--

DROP TABLE IF EXISTS `request_types`;
CREATE TABLE `request_types` (
  `code` int(11) NOT NULL,
  `description` varchar(128) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=203 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `request_types`
--

INSERT INTO `request_types` (`code`, `description`) VALUES
(100, 'create new Competent Authority user'),
(101, 'create new Operator/Owner user'),
(102, 'create new Installation user'),
(200, 'register new Competent Authority Organization'),
(201, 'register new Operator/Owners Organization'),
(202, 'register new Installation');

-- --------------------------------------------------------

--
-- Table structure for table `sece`
--

DROP TABLE IF EXISTS `sece`;
CREATE TABLE `sece` (
  `id` int(11) NOT NULL,
  `name` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sece`
--

INSERT INTO `sece` (`id`, `name`) VALUES
(801, 'Aircraft navigational aids'),
(303, 'ATEX certified equipment'),
(707, 'Blowdown'),
(402, 'Chemical injection monitor'),
(607, 'CO2/Halon fire-fighting system'),
(103, 'Cranes & lifting equipment'),
(601, 'Deluge'),
(305, 'Earthing / bonding equipment'),
(304, 'Electrical tripping equipment'),
(703, 'Emergency shutdown system (ESD)'),
(401, 'Fire & gas detection'),
(603, 'Fire water pumps'),
(604, 'Fire water system'),
(606, 'Fire/blast walls'),
(503, 'Gas tight floors'),
(301, 'Hazardous area ventilation'),
(602, 'Helideck foam system'),
(306, 'Inert Gas system'),
(1002, 'Lifeboats / TEMPSC'),
(701, 'Local shutdown system (LSD)'),
(104, 'Mooring systems (anchorline, dynamic positioning)'),
(204, 'Mud processing'),
(302, 'Non-hazardous area ventilation'),
(605, 'Passive fire protection system'),
(1001, 'Personal safety equipment'),
(206, 'Pipelines & risers'),
(207, 'Piping system'),
(208, 'Pressure vessels'),
(201, 'Primary well barrier'),
(702, 'Process shutdown system (PSD)'),
(1102, 'Public address'),
(1101, 'Radios / telephones'),
(502, 'Relief systems'),
(705, 'Riser ESD valve'),
(403, 'Sand'),
(205, 'Sand filters'),
(802, 'Seacraft navigational aids'),
(1005, 'Search & rescue facilities'),
(202, 'Secondary well barier'),
(704, 'Subsea isolation valve (SSIV)'),
(102, 'Subsea structures'),
(1004, 'Temporary refuge / Muster area'),
(1003, 'Tertiary escape means (lifecraft)'),
(706, 'Topsides ESD valve'),
(101, 'Topside_structures'),
(901, 'Turbine P.M. for compressor'),
(902, 'Turbine P.M. for generator'),
(209, 'Well control process equipment - BOP'),
(501, 'Well control process equipment - diverter'),
(203, 'Wireline equipment');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings` (
  `setting_name` varchar(64) NOT NULL,
  `setting_value` varchar(512) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='the settings table of the Application';

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`setting_name`, `setting_value`) VALUES
('terms_of_use', '0');

-- --------------------------------------------------------

--
-- Table structure for table `s_a`
--

DROP TABLE IF EXISTS `s_a`;
CREATE TABLE `s_a` (
  `id` int(11) NOT NULL,
  `event_classification_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `locked_by` int(11) DEFAULT NULL,
  `locked_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='this table holds the section A''s of the drafts';

--
-- Triggers `s_a`
--
DROP TRIGGER IF EXISTS `s_a_AFTER_INSERT`;
DELIMITER $$
CREATE TRIGGER `s_a_AFTER_INSERT` AFTER INSERT ON `s_a`
 FOR EACH ROW begin

INSERT INTO  `s_a_1`

(

	`sa_id`

)

VALUES

(

	NEW.id

);



INSERT INTO  `s_a_2`

(

	`sa_id`

)

VALUES

(

	NEW.id

);



INSERT INTO  `s_a_3`

(

	`sa_id`

)

VALUES

(

	NEW.id

);



INSERT INTO  `s_a_4`

(

	`sa_id`

)

VALUES

(

	NEW.id

);



end
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `s_a_1`
--

DROP TABLE IF EXISTS `s_a_1`;
CREATE TABLE `s_a_1` (
  `id` int(11) NOT NULL,
  `a1` tinyint(1) DEFAULT NULL,
  `a1i_np` tinyint(1) DEFAULT NULL,
  `a1i_np_specify` varchar(255) DEFAULT NULL,
  `a1i_2p_h2s_q` double DEFAULT NULL,
  `a1i_2p_h2s_u` int(11) DEFAULT NULL,
  `a1ii_q` double DEFAULT NULL,
  `a1ii_u` int(11) DEFAULT NULL,
  `a1iii_q` double DEFAULT NULL,
  `a1iii_u` int(11) DEFAULT NULL,
  `a1iv_q` double DEFAULT NULL,
  `a1iv_u` int(11) DEFAULT NULL,
  `a1v` varchar(512) DEFAULT NULL,
  `a1vi_code` int(11) DEFAULT NULL,
  `a1vii_code` int(11) DEFAULT NULL,
  `a1viia` int(11) DEFAULT NULL,
  `a1viib` double DEFAULT NULL,
  `a1viic` double DEFAULT NULL,
  `a1viid` double DEFAULT NULL COMMENT 'Specify hourly rate',
  `a1viii_uw_q` double DEFAULT NULL,
  `a1viii_uw_u` int(11) DEFAULT NULL,
  `a1viii_dw_q` double DEFAULT NULL,
  `a1viii_dw_u` int(11) DEFAULT NULL,
  `a1viii_c` mediumtext,
  `a1ix_a_q` double DEFAULT NULL,
  `a1ix_a_u` int(11) DEFAULT NULL,
  `a1ix_b_q` double DEFAULT NULL,
  `a1ix_b_u` int(11) DEFAULT NULL,
  `a1x_a` tinyint(1) DEFAULT NULL,
  `a1x_b` tinyint(1) DEFAULT NULL,
  `a1x_c` tinyint(1) DEFAULT NULL,
  `a1x_d` tinyint(1) DEFAULT NULL,
  `a1x_d_specify` varchar(255) DEFAULT NULL,
  `axi_description` varchar(512) DEFAULT NULL,
  `axii` tinyint(1) DEFAULT NULL,
  `axii_code` int(11) DEFAULT NULL,
  `axii_code_time` double DEFAULT NULL,
  `axii_flash_index` varchar(64) DEFAULT NULL,
  `axii_jet_index` varchar(64) DEFAULT NULL,
  `axii_exp_index` varchar(64) DEFAULT NULL,
  `axii_pool_index` varchar(64) DEFAULT NULL,
  `axiii` varchar(512) DEFAULT NULL,
  `axiv_sd` int(11) DEFAULT NULL,
  `axiv_bd` int(11) DEFAULT NULL,
  `axiv_d` int(11) DEFAULT NULL,
  `axiv_co2` int(11) DEFAULT NULL,
  `axiv_ctm` int(11) DEFAULT NULL,
  `axiv_other` tinyint(1) DEFAULT NULL,
  `axiv_specify` varchar(45) DEFAULT NULL,
  `a1i_o` tinyint(1) DEFAULT NULL,
  `a1i_co` tinyint(1) DEFAULT NULL,
  `a1i_g` tinyint(1) DEFAULT NULL,
  `a1i_2p` tinyint(1) DEFAULT NULL,
  `a1_op` int(11) DEFAULT NULL,
  `a1_op_specify` varchar(64) DEFAULT NULL,
  `a1_fail_design_code` int(11) DEFAULT NULL,
  `a1_fail_equipment_code` int(11) DEFAULT NULL,
  `a1_fail_equipment_specify` varchar(128) DEFAULT NULL,
  `a1_fail_operation_code` int(11) DEFAULT NULL,
  `a1_fail_operation_specify` varchar(128) DEFAULT NULL,
  `a1_fail_procedural_code` int(11) DEFAULT NULL,
  `a1_fail_procedural_specify` varchar(45) DEFAULT NULL,
  `a1xv` mediumtext,
  `sa_id` int(11) NOT NULL,
  `a1_vi` mediumtext,
  `a1i_2p_h2s_n` double DEFAULT NULL,
  `a1ii_n` double DEFAULT NULL,
  `a1iii_n` double DEFAULT NULL,
  `a1iv_n` double DEFAULT NULL,
  `a1viii_uw_n` double DEFAULT NULL,
  `a1viii_dw_n` double DEFAULT NULL,
  `a1ix_a_n` double DEFAULT NULL,
  `a1ix_b_n` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table holding Section A of reports';

-- --------------------------------------------------------

--
-- Table structure for table `s_a_2`
--

DROP TABLE IF EXISTS `s_a_2`;
CREATE TABLE `s_a_2` (
  `id` int(11) NOT NULL,
  `a2_1` tinyint(1) DEFAULT NULL COMMENT 'existence of a release of non-hydrocarbon hazardous substance',
  `a2_1_q` double DEFAULT NULL COMMENT 'released quantity',
  `a2_1_u` int(11) DEFAULT NULL COMMENT 'released unit id',
  `a2_1_t` varchar(45) DEFAULT NULL COMMENT 'released substance type',
  `a2_2` tinyint(1) DEFAULT NULL COMMENT 'presence of non-hydrocarbon fire',
  `a2_2_desc` mediumtext COMMENT 'circumstances of the fire',
  `a2_3` tinyint(1) DEFAULT NULL COMMENT 'likeness of degradation of marine environment',
  `a2_3_desc` mediumtext COMMENT 'observed and foreseen environmental impacts',
  `sa_id` int(11) DEFAULT NULL COMMENT 'the id of the parent SectionA',
  `a2_desc` mediumtext COMMENT 'the general description of the consequences',
  `a2_1_n` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table holding the data for Section A.2';

-- --------------------------------------------------------

--
-- Table structure for table `s_a_3`
--

DROP TABLE IF EXISTS `s_a_3`;
CREATE TABLE `s_a_3` (
  `id` int(11) NOT NULL,
  `a3_1` mediumtext COMMENT 'Preliminary direct and underlying causes',
  `sa_id` int(11) DEFAULT NULL COMMENT 'the id of the parent SectionA',
  `is_direct` tinyint(1) DEFAULT NULL COMMENT 'if True this is a direct cause; underlying otherwise'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table holding the data for Section A.3';

-- --------------------------------------------------------

--
-- Table structure for table `s_a_4`
--

DROP TABLE IF EXISTS `s_a_4`;
CREATE TABLE `s_a_4` (
  `id` int(11) NOT NULL,
  `a4_1` mediumtext COMMENT 'Initial lessons learned and preliminary recommendations',
  `is_lesson_learn` tinyint(1) DEFAULT NULL COMMENT 'if True this is classified as lesson learned; recommendation otherwise',
  `sa_id` int(11) DEFAULT NULL COMMENT 'the id of the parent SectionA'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table holding the data for Section A.4';

-- --------------------------------------------------------

--
-- Table structure for table `s_b`
--

DROP TABLE IF EXISTS `s_b`;
CREATE TABLE `s_b` (
  `id` int(11) NOT NULL,
  `event_classification_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `locked_by` int(11) DEFAULT NULL,
  `locked_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='this table holds the section B''s of the drafts';

--
-- Triggers `s_b`
--
DROP TRIGGER IF EXISTS `s_b_AFTER_INSERT`;
DELIMITER $$
CREATE TRIGGER `s_b_AFTER_INSERT` AFTER INSERT ON `s_b`
 FOR EACH ROW begin

INSERT INTO  `s_b_1`

(

	`sb_id`

)

VALUES

(

	NEW.id

);



INSERT INTO  `s_b_2`

(

	`sb_id`

)

VALUES

(

	NEW.id

);



INSERT INTO  `s_b_3`

(

	`sb_id`

)

VALUES

(

	NEW.id

);



INSERT INTO  `s_b_4`

(

	`sb_id`

)

VALUES

(

	NEW.id

);



end
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `s_b_1`
--

DROP TABLE IF EXISTS `s_b_1`;
CREATE TABLE `s_b_1` (
  `id` int(11) NOT NULL,
  `b1_1` varchar(64) DEFAULT NULL COMMENT 'Name/code of the well',
  `b1_2` varchar(64) DEFAULT NULL COMMENT 'Name of drilling contractor (if relevant)',
  `b1_3` varchar(64) DEFAULT NULL COMMENT 'Name/type of drilling rig (if relevant)',
  `b1_4_s` datetime DEFAULT NULL COMMENT 'Start date/time of loss of well control',
  `b1_4_e` datetime DEFAULT NULL COMMENT 'End date/time of loss of well control',
  `b1_5` int(11) DEFAULT NULL COMMENT 'Type of fluid: brine / oil / gas',
  `b1_6` int(11) DEFAULT NULL COMMENT 'Well head completion: surface/subsea',
  `b1_7` double DEFAULT NULL COMMENT 'Water depth (m)',
  `b1_8_1_q` double DEFAULT NULL COMMENT 'Reservoir pressure',
  `b1_8_1_u` int(11) DEFAULT NULL COMMENT 'Reservoir pressure unit',
  `b1_8_2_q` double DEFAULT NULL COMMENT 'Reservoir temperature',
  `b1_8_2_u` int(11) DEFAULT NULL COMMENT 'Reservoir temperature unit',
  `b1_8_3_q` double DEFAULT NULL COMMENT 'Reservoir depth',
  `b1_8_3_u` int(11) DEFAULT NULL COMMENT 'Reservoir depth unit',
  `b1_9` int(11) DEFAULT NULL COMMENT 'Type of activity',
  `b1_10` int(11) DEFAULT NULL COMMENT 'Type of well services (if applicable)',
  `sb_id` int(11) DEFAULT NULL COMMENT 'the id of the parent SectionA',
  `b1_8_1_n` double DEFAULT NULL,
  `b1_8_2_n` double DEFAULT NULL,
  `b1_8_3_n` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table holding the data for Section B.1';

-- --------------------------------------------------------

--
-- Table structure for table `s_b_2`
--

DROP TABLE IF EXISTS `s_b_2`;
CREATE TABLE `s_b_2` (
  `id` int(11) NOT NULL,
  `b2_desc` mediumtext COMMENT 'General description of circumstances',
  `b2_1` tinyint(1) DEFAULT NULL COMMENT 'Blowout prevention equipment activated',
  `b2_2` tinyint(1) DEFAULT NULL COMMENT 'Diverter system in operation',
  `b2_3` tinyint(1) DEFAULT NULL COMMENT 'Pressure build-up and/or positive flow check',
  `b2_4_a` tinyint(1) DEFAULT NULL COMMENT 'Failing well barriers (a)',
  `b2_4_a_desc` varchar(64) DEFAULT NULL COMMENT 'Failing well barriers (a) description',
  `b2_4_b` tinyint(1) DEFAULT NULL COMMENT 'Failing well barriers (b)',
  `b2_4_b_desc` varchar(64) DEFAULT NULL COMMENT 'Failing well barriers (b) description',
  `b2_4_c` tinyint(1) DEFAULT NULL COMMENT 'Failing well barriers (b)',
  `b2_4_c_desc` varchar(64) DEFAULT NULL COMMENT 'Failing well barriers (b) description',
  `b2_5` mediumtext COMMENT 'Description of circumstances',
  `b2_6_a_q` double DEFAULT NULL COMMENT 'Duration of uncontrolled flow of well fluids',
  `b2_6_a_u` int(11) DEFAULT NULL COMMENT 'Duration of uncontrolled flow of well fluids unit',
  `b2_6_b_q` double DEFAULT NULL COMMENT 'Flowrate',
  `b2_6_b_u` int(11) DEFAULT NULL COMMENT 'Flowrate unit',
  `b2_6_c_q` double DEFAULT NULL COMMENT 'Liquid volume',
  `b2_6_c_u` int(11) DEFAULT NULL COMMENT 'Liquid volume unit',
  `b2_6_d_q` double DEFAULT NULL COMMENT 'Gas volume unit',
  `b2_6_d_u` int(11) DEFAULT NULL COMMENT 'Gas volume unit',
  `b2_7` mediumtext COMMENT 'Consequences of event and emergency response',
  `sb_id` int(11) NOT NULL COMMENT 'the id of the parent SectionB',
  `b2_6_a_n` double DEFAULT NULL,
  `b2_6_b_n` double DEFAULT NULL,
  `b2_6_c_n` double DEFAULT NULL,
  `b2_6_d_n` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table holding the data for Section B.1';

-- --------------------------------------------------------

--
-- Table structure for table `s_b_3`
--

DROP TABLE IF EXISTS `s_b_3`;
CREATE TABLE `s_b_3` (
  `id` int(11) NOT NULL,
  `b3_1` mediumtext COMMENT 'Preliminary direct and underlying causes',
  `sb_id` int(11) DEFAULT NULL COMMENT 'the id of the parent SectionA',
  `is_direct` tinyint(1) DEFAULT NULL COMMENT 'if True this is a direct cause; underlying otherwise'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table holding the data for Section B.3';

-- --------------------------------------------------------

--
-- Table structure for table `s_b_4`
--

DROP TABLE IF EXISTS `s_b_4`;
CREATE TABLE `s_b_4` (
  `id` int(11) NOT NULL,
  `b4_1` mediumtext COMMENT 'Initial lessons learned and preliminary recommendations',
  `is_lesson_learn` tinyint(1) DEFAULT NULL COMMENT 'if True this is classified as lesson learned; recommendation otherwise',
  `sb_id` int(11) DEFAULT NULL COMMENT 'the id of the parent SectionA'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table holding the data for Section A.4';

-- --------------------------------------------------------

--
-- Table structure for table `s_c`
--

DROP TABLE IF EXISTS `s_c`;
CREATE TABLE `s_c` (
  `id` int(11) NOT NULL,
  `event_classification_id` int(11) NOT NULL,
  `locked_by` int(11) DEFAULT NULL,
  `locked_at` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Triggers `s_c`
--
DROP TRIGGER IF EXISTS `s_c_AFTER_INSERT`;
DELIMITER $$
CREATE TRIGGER `s_c_AFTER_INSERT` AFTER INSERT ON `s_c`
 FOR EACH ROW begin

INSERT INTO  `s_c_1`

(

	`sc_id`

)

VALUES

(

	NEW.id

);



INSERT INTO  `s_c_2`

(

	`sc_id`

)

VALUES

(

	NEW.id

);



INSERT INTO  `s_c_3`

(

	`sc_id`

)

VALUES

(

	NEW.id

);



INSERT INTO  `s_c_4`

(

	`sc_id`

)

VALUES

(

	NEW.id

);



end
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `s_c_1`
--

DROP TABLE IF EXISTS `s_c_1`;
CREATE TABLE `s_c_1` (
  `id` int(11) NOT NULL,
  `c1_1` varchar(128) DEFAULT NULL,
  `sc_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `s_c_2`
--

DROP TABLE IF EXISTS `s_c_2`;
CREATE TABLE `s_c_2` (
  `id` int(11) NOT NULL,
  `c2_desc` mediumtext COMMENT 'General description of circumstances',
  `sc_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Triggers `s_c_2`
--
DROP TRIGGER IF EXISTS `s_c_2_AFTER_INSERT`;
DELIMITER $$
CREATE TRIGGER `s_c_2_AFTER_INSERT` AFTER INSERT ON `s_c_2`
 FOR EACH ROW BEGIN

	INSERT INTO  `s_c_2_1`

	(	`sc_2_id`	)

	VALUES

	(	NEW.id	);





	INSERT INTO  `s_c_2_2`

	(	`sc_2_id`	)

	VALUES

	(	NEW.id	);



END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `s_c_2_1`
--

DROP TABLE IF EXISTS `s_c_2_1`;
CREATE TABLE `s_c_2_1` (
  `id` int(11) NOT NULL,
  `c21_1` varchar(255) DEFAULT NULL,
  `c21_2` mediumtext,
  `sc_2_id` int(11) NOT NULL,
  `c21_3_a` tinyint(1) DEFAULT NULL,
  `c21_3_a_desc` varchar(255) DEFAULT NULL,
  `c21_3_b` tinyint(1) DEFAULT NULL,
  `c21_3_b_desc` varchar(255) DEFAULT NULL,
  `c21_3_c` tinyint(1) DEFAULT NULL,
  `c21_3_c_desc` varchar(255) DEFAULT NULL,
  `c21_3_d` tinyint(1) DEFAULT NULL,
  `c21_3_d_desc` varchar(255) DEFAULT NULL,
  `c21_3_e` tinyint(1) DEFAULT NULL,
  `c21_3_e_desc` varchar(255) DEFAULT NULL,
  `c21_3_f` tinyint(1) DEFAULT NULL,
  `c21_3_f_desc` varchar(255) DEFAULT NULL,
  `c21_3_g` tinyint(1) DEFAULT NULL,
  `c21_3_g_desc` varchar(255) DEFAULT NULL,
  `c21_3_h` tinyint(1) DEFAULT NULL,
  `c21_3_h_desc` varchar(255) DEFAULT NULL,
  `c21_3_i` tinyint(1) DEFAULT NULL,
  `c21_3_i_desc` varchar(255) DEFAULT NULL,
  `c21_3_j` tinyint(1) DEFAULT NULL,
  `c21_3_j_desc` varchar(255) DEFAULT NULL,
  `c21_3_k` tinyint(1) DEFAULT NULL,
  `c21_3_k_desc` varchar(255) DEFAULT NULL,
  `c21_3_l` tinyint(1) DEFAULT NULL,
  `c21_3_l_desc` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `s_c_2_1_sece`
--

DROP TABLE IF EXISTS `s_c_2_1_sece`;
CREATE TABLE `s_c_2_1_sece` (
  `s_c_2_1_id` int(11) NOT NULL,
  `sece_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `s_c_2_2`
--

DROP TABLE IF EXISTS `s_c_2_2`;
CREATE TABLE `s_c_2_2` (
  `id` int(11) NOT NULL,
  `c22_1` tinyint(1) DEFAULT NULL,
  `c22_desc` mediumtext,
  `sc_2_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `s_c_3`
--

DROP TABLE IF EXISTS `s_c_3`;
CREATE TABLE `s_c_3` (
  `id` int(11) NOT NULL,
  `sc3_1` mediumtext,
  `sc_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `s_c_4`
--

DROP TABLE IF EXISTS `s_c_4`;
CREATE TABLE `s_c_4` (
  `id` int(11) NOT NULL,
  `c4_1` mediumtext,
  `sc_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `s_d`
--

DROP TABLE IF EXISTS `s_d`;
CREATE TABLE `s_d` (
  `id` int(11) NOT NULL,
  `event_classification_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `locked_by` int(11) DEFAULT NULL,
  `locked_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='this table holds the section D''s of the drafts';

--
-- Triggers `s_d`
--
DROP TRIGGER IF EXISTS `s_d_AFTER_INSERT`;
DELIMITER $$
CREATE TRIGGER `s_d_AFTER_INSERT` AFTER INSERT ON `s_d`
 FOR EACH ROW BEGIN

	INSERT INTO  `s_d_1`

	(

		`sd_id`

	)

	VALUES

	(

		NEW.id

	);



INSERT INTO  `s_d_2`

	(

		`sd_id`

	)

	VALUES

	(

		NEW.id

	);



INSERT INTO  `s_d_3`

	(

		`sd_id`

	)

	VALUES

	(

		NEW.id

	);



INSERT INTO  `s_d_4`

	(

		`sd_id`

	)

	VALUES

	(

		NEW.id

	);



END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `s_d_1`
--

DROP TABLE IF EXISTS `s_d_1`;
CREATE TABLE `s_d_1` (
  `id` int(11) NOT NULL,
  `d1_1` varchar(64) DEFAULT NULL COMMENT 'Name/code of the well',
  `sd_id` int(11) DEFAULT NULL COMMENT 'the id of the parent Section D'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table holding the data for Section D.1';

-- --------------------------------------------------------

--
-- Table structure for table `s_d_2`
--

DROP TABLE IF EXISTS `s_d_2`;
CREATE TABLE `s_d_2` (
  `id` int(11) NOT NULL,
  `d2_1` mediumtext COMMENT 'Description of circumstances',
  `sd_id` int(11) DEFAULT NULL COMMENT 'the id of the parent Section D'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table holding the data for Section D.1';

-- --------------------------------------------------------

--
-- Table structure for table `s_d_3`
--

DROP TABLE IF EXISTS `s_d_3`;
CREATE TABLE `s_d_3` (
  `id` int(11) NOT NULL,
  `d3_1` mediumtext COMMENT 'Preliminary direct and underlying causes',
  `sd_id` int(11) DEFAULT NULL COMMENT 'the id of the parent Section D',
  `is_direct` tinyint(1) DEFAULT NULL COMMENT 'if True this is a direct cause; underlying otherwise'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table holding the data for Section D.3';

-- --------------------------------------------------------

--
-- Table structure for table `s_d_4`
--

DROP TABLE IF EXISTS `s_d_4`;
CREATE TABLE `s_d_4` (
  `id` int(11) NOT NULL,
  `d4_1` mediumtext COMMENT 'Initial lessons learned and preliminary recommendations',
  `is_lesson_learn` tinyint(1) DEFAULT NULL COMMENT 'if True this is classified as lesson learned; recommendation otherwise',
  `sd_id` int(11) DEFAULT NULL COMMENT 'the id of the parent SectionA'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table holding the data for Section D.4';

-- --------------------------------------------------------

--
-- Table structure for table `s_e`
--

DROP TABLE IF EXISTS `s_e`;
CREATE TABLE `s_e` (
  `id` int(11) NOT NULL,
  `event_classification_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `locked_by` int(11) DEFAULT NULL,
  `locked_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='this table holds the section E''s of the drafts';

--
-- Triggers `s_e`
--
DROP TRIGGER IF EXISTS `s_e_AFTER_INSERT`;
DELIMITER $$
CREATE TRIGGER `s_e_AFTER_INSERT` AFTER INSERT ON `s_e`
 FOR EACH ROW BEGIN

	INSERT INTO  `s_e_1`

	(

		`se_id`

	)

	VALUES

	(

		NEW.id

	);



INSERT INTO  `s_e_2`

	(

		`se_id`

	)

	VALUES

	(

		NEW.id

	);



INSERT INTO  `s_e_3`

	(

		`se_id`

	)

	VALUES

	(

		NEW.id

	);



INSERT INTO  `s_e_4`

	(

		`se_id`

	)

	VALUES

	(

		NEW.id

	);



END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `s_e_1`
--

DROP TABLE IF EXISTS `s_e_1`;
CREATE TABLE `s_e_1` (
  `id` int(11) NOT NULL,
  `e1_1` varchar(64) DEFAULT NULL COMMENT 'Name/code of the vessel',
  `e1_2` varchar(64) DEFAULT NULL COMMENT 'Type/tonnage of the vessel',
  `e1_3` varchar(64) DEFAULT NULL COMMENT 'Contact via AIS?',
  `se_id` int(11) DEFAULT NULL COMMENT 'the id of the parent Section E'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table holding the data for Section E.1';

-- --------------------------------------------------------

--
-- Table structure for table `s_e_2`
--

DROP TABLE IF EXISTS `s_e_2`;
CREATE TABLE `s_e_2` (
  `id` int(11) NOT NULL,
  `e2_1` mediumtext COMMENT 'Description of circumstances',
  `se_id` int(11) DEFAULT NULL COMMENT 'the id of the parent Section E'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table holding the data for Section E.1';

-- --------------------------------------------------------

--
-- Table structure for table `s_e_3`
--

DROP TABLE IF EXISTS `s_e_3`;
CREATE TABLE `s_e_3` (
  `id` int(11) NOT NULL,
  `e3_1` mediumtext COMMENT 'Preliminary direct and underlying causes',
  `se_id` int(11) DEFAULT NULL COMMENT 'the id of the parent Section E',
  `is_direct` tinyint(1) DEFAULT NULL COMMENT 'if True this is a direct cause; underlying otherwise'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table holding the data for Section E.3';

-- --------------------------------------------------------

--
-- Table structure for table `s_e_4`
--

DROP TABLE IF EXISTS `s_e_4`;
CREATE TABLE `s_e_4` (
  `id` int(11) NOT NULL,
  `e4_1` mediumtext COMMENT 'Initial lessons learned and preliminary recommendations',
  `is_lesson_learn` tinyint(1) DEFAULT NULL COMMENT 'if True this is classified as lesson learned; recommendation otherwise',
  `se_id` int(11) DEFAULT NULL COMMENT 'the id of the parent Section E'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table holding the data for Section E.4';

-- --------------------------------------------------------

--
-- Table structure for table `s_f`
--

DROP TABLE IF EXISTS `s_f`;
CREATE TABLE `s_f` (
  `id` int(11) NOT NULL,
  `event_classification_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `locked_by` int(11) DEFAULT NULL,
  `locked_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='this table holds the section F''s of the drafts';

--
-- Triggers `s_f`
--
DROP TRIGGER IF EXISTS `s_f_AFTER_INSERT`;
DELIMITER $$
CREATE TRIGGER `s_f_AFTER_INSERT` AFTER INSERT ON `s_f`
 FOR EACH ROW BEGIN

	INSERT INTO  `s_f_1`

	(

		`sf_id`

	)

	VALUES

	(

		NEW.id

	);



INSERT INTO  `s_f_2`

	(

		`sf_id`

	)

	VALUES

	(

		NEW.id

	);



INSERT INTO  `s_f_3`

	(

		`sf_id`

	)

	VALUES

	(

		NEW.id

	);



INSERT INTO  `s_f_4`

	(

		`sf_id`

	)

	VALUES

	(

		NEW.id

	);



END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `s_f_1`
--

DROP TABLE IF EXISTS `s_f_1`;
CREATE TABLE `s_f_1` (
  `id` int(11) NOT NULL,
  `f1_1` varchar(64) DEFAULT NULL COMMENT 'Name of helicopter contractor',
  `f1_2` varchar(64) DEFAULT NULL COMMENT 'Helicopter type',
  `f1_3` varchar(64) DEFAULT NULL COMMENT 'Number of persons on board',
  `sf_id` int(11) DEFAULT NULL COMMENT 'the id of the parent Section F'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table holding the data for Section F.1';

-- --------------------------------------------------------

--
-- Table structure for table `s_f_2`
--

DROP TABLE IF EXISTS `s_f_2`;
CREATE TABLE `s_f_2` (
  `id` int(11) NOT NULL,
  `f2_1` mediumtext COMMENT 'Description of circumstances',
  `sf_id` int(11) DEFAULT NULL COMMENT 'the id of the parent Section F'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table holding the data for Section F.2';

-- --------------------------------------------------------

--
-- Table structure for table `s_f_3`
--

DROP TABLE IF EXISTS `s_f_3`;
CREATE TABLE `s_f_3` (
  `id` int(11) NOT NULL,
  `f3_1` mediumtext COMMENT 'Description of circumstances',
  `sf_id` int(11) DEFAULT NULL COMMENT 'the id of the parent sfction F',
  `is_direct` tinyint(1) DEFAULT NULL COMMENT 'if True this is a direct cause; underlying otherwise'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table holding the data for sfction F.3';

-- --------------------------------------------------------

--
-- Table structure for table `s_f_4`
--

DROP TABLE IF EXISTS `s_f_4`;
CREATE TABLE `s_f_4` (
  `id` int(11) NOT NULL,
  `f4_1` mediumtext COMMENT 'Description of circumstances',
  `is_lesson_learn` tinyint(1) DEFAULT NULL COMMENT 'if True this is classified as lesson learned; recommendation otherwise',
  `sf_id` int(11) DEFAULT NULL COMMENT 'the id of the parent sfction F'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table holding the data for sfction F.4';

-- --------------------------------------------------------

--
-- Table structure for table `s_i`
--

DROP TABLE IF EXISTS `s_i`;
CREATE TABLE `s_i` (
  `id` int(11) NOT NULL,
  `event_classification_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `locked_by` int(11) DEFAULT NULL,
  `locked_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='this table holds the section I''s of the drafts';

--
-- Triggers `s_i`
--
DROP TRIGGER IF EXISTS `s_i_AFTER_INSERT`;
DELIMITER $$
CREATE TRIGGER `s_i_AFTER_INSERT` AFTER INSERT ON `s_i`
 FOR EACH ROW begin

INSERT INTO  `s_i_1`

(

	`si_id`

)

VALUES

(

	NEW.id

);



INSERT INTO  `s_i_2`

(

	`si_id`

)

VALUES

(

	NEW.id

);



INSERT INTO  `s_i_3`

(

	`si_id`

)

VALUES

(

	NEW.id

);



INSERT INTO  `s_i_4`

(

	`si_id`

)

VALUES

(

	NEW.id

);



end
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `s_i_1`
--

DROP TABLE IF EXISTS `s_i_1`;
CREATE TABLE `s_i_1` (
  `id` int(11) NOT NULL,
  `i1_1_s` datetime DEFAULT NULL COMMENT 'Start date/time of evacuation',
  `i1_1_e` datetime DEFAULT NULL COMMENT 'End date/time of evacuation',
  `si_id` int(11) DEFAULT NULL COMMENT 'the id of the parent SectionI'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table holding the data for Section I.1';

-- --------------------------------------------------------

--
-- Table structure for table `s_i_2`
--

DROP TABLE IF EXISTS `s_i_2`;
CREATE TABLE `s_i_2` (
  `id` int(11) NOT NULL,
  `i2_1` tinyint(1) DEFAULT NULL COMMENT 'Evacuation type (null, 1 - precautionary, 2 - emergency, 3 - both',
  `i2_2` int(11) DEFAULT NULL COMMENT 'Number of persons evacuated',
  `i2_3` varchar(255) DEFAULT NULL COMMENT 'Means of evacuation',
  `i2_4` mediumtext NOT NULL,
  `si_id` int(11) DEFAULT NULL COMMENT 'the id of the parent SectionI'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table holding the data for Section I.2';

-- --------------------------------------------------------

--
-- Table structure for table `s_i_3`
--

DROP TABLE IF EXISTS `s_i_3`;
CREATE TABLE `s_i_3` (
  `id` int(11) NOT NULL,
  `i3_1` mediumtext,
  `si_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `s_i_4`
--

DROP TABLE IF EXISTS `s_i_4`;
CREATE TABLE `s_i_4` (
  `id` int(11) NOT NULL,
  `i4_1` mediumtext COMMENT 'Initial lessons learned and preliminary recommendations',
  `si_id` int(11) DEFAULT NULL COMMENT 'the id of the parent SectionI'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table holding the data for Section I.4';

-- --------------------------------------------------------

--
-- Table structure for table `s_j`
--

DROP TABLE IF EXISTS `s_j`;
CREATE TABLE `s_j` (
  `id` int(11) NOT NULL,
  `event_classification_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `locked_by` int(11) DEFAULT NULL,
  `locked_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='this table holds the section J''s of the drafts';

--
-- Triggers `s_j`
--
DROP TRIGGER IF EXISTS `s_j_AFTER_INSERT`;
DELIMITER $$
CREATE TRIGGER `s_j_AFTER_INSERT` AFTER INSERT ON `s_j`
 FOR EACH ROW begin

INSERT INTO  `s_j_1`

(

	`sj_id`

)

VALUES

(

	NEW.id

);



INSERT INTO  `s_j_2`

(

	`sj_id`

)

VALUES

(

	NEW.id

);



INSERT INTO  `s_j_3`

(

	`sj_id`

)

VALUES

(

	NEW.id

);



INSERT INTO `s_j_4`

(

	`sj_id`

)

VALUES

(

	NEW.id

);



end
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `s_j_1`
--

DROP TABLE IF EXISTS `s_j_1`;
CREATE TABLE `s_j_1` (
  `id` int(11) NOT NULL,
  `j1_1` varchar(128) DEFAULT NULL COMMENT 'Name of contractor',
  `sj_id` int(11) DEFAULT NULL COMMENT 'the id of the parent SectionJ'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table holding the data for Section J.1';

-- --------------------------------------------------------

--
-- Table structure for table `s_j_2`
--

DROP TABLE IF EXISTS `s_j_2`;
CREATE TABLE `s_j_2` (
  `id` int(11) NOT NULL,
  `j2_1` mediumtext COMMENT 'Description of circumstances, consequences of event and emergency response',
  `sj_id` int(11) DEFAULT NULL COMMENT 'the id of the parent SectionJ'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table holding the data for Section J.2';

-- --------------------------------------------------------

--
-- Table structure for table `s_j_3`
--

DROP TABLE IF EXISTS `s_j_3`;
CREATE TABLE `s_j_3` (
  `id` int(11) NOT NULL,
  `j3_1` mediumtext,
  `sj_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `s_j_4`
--

DROP TABLE IF EXISTS `s_j_4`;
CREATE TABLE `s_j_4` (
  `id` int(11) NOT NULL,
  `j4_1` mediumtext COMMENT 'Initial lessons learned and preliminary recommendations',
  `sj_id` int(11) DEFAULT NULL COMMENT 'the id of the parent SectionJ'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table holding the data for Section J.4';

-- --------------------------------------------------------

--
-- Table structure for table `t_message`
--

DROP TABLE IF EXISTS `t_message`;
CREATE TABLE `t_message` (
  `id` int(11) NOT NULL DEFAULT '0',
  `language` varchar(16) NOT NULL DEFAULT '',
  `translation` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `t_message`
--

INSERT INTO `t_message` (`id`, `language`, `translation`) VALUES
(2, 'it', 'Pagina iniziale'),
(2, 'ro', 'acasă'),
(3, 'it', 'richieste'),
(3, 'ro', 'solicitări'),
(4, 'it', 'Unità'),
(4, 'ro', 'unităţi de măsură'),
(5, 'it', 'Administrazione'),
(5, 'ro', 'administrare'),
(6, 'it', 'categoria'),
(6, 'ro', 'categorie'),
(7, 'it', 'messaggio'),
(7, 'ro', 'mesaj'),
(8, 'it', 'traduzione'),
(8, 'ro', 'traducere'),
(9, 'it', 'tutti'),
(9, 'ro', 'toate'),
(10, 'it', 'esistente'),
(10, 'ro', 'existent(ă)'),
(11, 'it', 'mancante'),
(11, 'ro', 'lipsă'),
(12, 'it', 'I messaggi di origine sono quelli che devono essere tradotti in altre lingue. I messaggi sono raggruppati in categorie e mirato ai messaggi dell''applicazione (UI) ed il Common Reporting Format (app/crf) e il formato comune di pubblicazione (app/cpf) ed i messaggi di testo.'),
(12, 'ro', '<em>Mesajele referinţă</em> sunt cele ce trebuiesc traduse. Mesajele sunt grupate ȋn Categorii şi acoperă atât mesajele aplicaţiei (IU) cât şi textul Raportului Comun de Raportare (app/crf) şi al Formatului Comun de Publicare (app/cpf).'),
(13, 'it', 'messaggi originali'),
(13, 'ro', 'mesaje referinţă'),
(14, 'it', 'traduzioni'),
(14, 'ro', 'traduceri'),
(15, 'it', 'Nazione'),
(15, 'ro', 'Ţară'),
(16, 'it', 'codice'),
(16, 'ro', 'cod'),
(17, 'it', 'completamento'),
(17, 'ro', 'nivel de completare'),
(19, 'it', 'tradotto'),
(19, 'ro', 'traduse'),
(20, 'it', 'totale'),
(20, 'ro', 'total'),
(21, 'it', 'lingue registrate'),
(21, 'ro', 'limbi ȋnregistrate'),
(22, 'it', 'Registrare nuovo {evt}'),
(22, 'ro', 'Înregistrare nouă ({evt})'),
(23, 'it', 'lingua'),
(23, 'ro', 'limba'),
(25, 'it', 'organizzazioni'),
(25, 'ro', 'organizaţii'),
(26, 'it', 'installazione'),
(26, 'ro', 'instalaţie'),
(27, 'it', 'impianti'),
(27, 'ro', 'instalaţii'),
(28, 'it', 'utente'),
(28, 'ro', 'utilizator'),
(29, 'it', 'utenti'),
(29, 'ro', 'utilizatori'),
(30, 'it', 'Attualmente, ci sono {evt} messaggi registrati Syrio.'),
(30, 'ro', 'În acest moment {evt} mesaje sunt ȋnregistrate ȋn SyRIO.'),
(31, 'it', 'messaggio originali'),
(31, 'ro', 'mesaj referinţă'),
(32, 'it', 'Almeno un messaggio di origine deve essere tradotto per registrare una nuova lingua.'),
(32, 'ro', 'Cel puţin un mesaj de referinţă trebuie tradus pentru a înregistra o noua limbă.'),
(33, 'it', 'Sottomettere'),
(33, 'ro', 'Trimite'),
(34, 'it', 'azioni'),
(34, 'ro', 'acţiuni'),
(36, 'it', 'Azzerare'),
(36, 'ro', 'resetează'),
(39, 'it', 'Testo'),
(39, 'ro', 'Text'),
(43, 'it', 'esportare come ...'),
(43, 'ro', 'exportă ca...'),
(44, 'it', 'traduzione messaggio'),
(44, 'ro', 'Traducere mesaj'),
(45, 'it', 'Si prega di fornire la traduzione per il messaggio di origine nella lingua scelta ({evt}).'),
(45, 'ro', 'Traduceţi mesajul referinţă în limba selectată ({evt}).'),
(46, 'it', 'Prestare particolare attenzione a (preservare!) gli eventuali segnaposto nel messaggio di origine (tra parentesi graffe).'),
(46, 'ro', 'Atenţie la eventualele elemente substituent (acolade); acestea trebuiesc păstrate în traducerea finală.'),
(47, 'it', 'Resetta {evt}'),
(47, 'ro', 'Resetare {evt}'),
(48, 'it', 'griglia'),
(48, 'ro', 'tabel'),
(49, 'it', 'modifica {evt}'),
(49, 'ro', 'modifică {evt}'),
(50, 'it', 'Cancellare'),
(50, 'ro', 'Renunță'),
(51, 'it', 'piú'),
(51, 'ro', 'mai multe'),
(52, 'it', 'Organizazzioni'),
(52, 'ro', 'Organizaţii'),
(53, 'it', '''{evt}'' e'' stato con successo {evt2}.'),
(53, 'ro', '''{evt}'' a fost {evt2} cu succes.'''),
(54, 'it', 'acronimo'),
(54, 'ro', 'acronim'),
(56, 'it', 'indirizzo'),
(56, 'ro', 'adresa'),
(57, 'it', 'Installazioni attive'),
(57, 'ro', 'Instalații active'),
(58, 'it', 'Aggiungere {evt}'),
(58, 'ro', 'Adaugă {evt}'),
(59, 'it', 'qualsiasi'),
(59, 'ro', 'oricare'),
(60, 'it', 'tipo'),
(60, 'ro', 'tip'),
(61, 'it', 'Elimina {evt}'),
(61, 'ro', 'Șterge {evt}'),
(62, 'it', 'Operazione può essere annullata.'),
(62, 'ro', 'Operaţia ESTE reversibilă.'),
(64, 'it', 'Elimina definitivamente {evt}'),
(64, 'ro', 'Şterge DEFINITIV?'),
(65, 'it', 'Operazione non può essere annullata.'),
(65, 'ro', 'Operaţia NU ESTE reversibilă'),
(66, 'it', 'purge'),
(66, 'ro', 'elimină'),
(67, 'it', 'Questo verrá  riattivato {evt}'),
(67, 'ro', 'Se va reactiva {evt}.'),
(68, 'it', 'Sei sicuro?'),
(68, 'ro', 'Sunteţi sigur?'),
(69, 'it', 'reattivare'),
(69, 'ro', 'reactivare'),
(70, 'it', 'Utente AC'),
(70, 'ro', 'utilizator AC'),
(71, 'it', 'Utente OO'),
(71, 'ro', 'utilizator OO'),
(72, 'it', 'Elenco {evt}'),
(72, 'ro', 'Listă {evt}'),
(73, 'it', 'Autoritá Competente'),
(73, 'ro', 'Autoritate Competentă'),
(74, 'it', 'Operatori / Proprietari'),
(74, 'ro', 'Operatori / Proprietari'),
(75, 'it', 'Nome'),
(75, 'ro', 'Nume'),
(76, 'it', 'Organizzazione'),
(76, 'ro', 'Organizaţie'),
(77, 'it', 'Ruolo  nell''Organizzazione'),
(77, 'ro', 'Rol în organizaţie'),
(78, 'it', 'Membro dal'),
(78, 'ro', 'Membru de la'),
(79, 'it', 'e-mail'),
(79, 'ro', 'Email'),
(80, 'it', 'Telefono'),
(80, 'ro', 'Telefon'),
(81, 'it', 'Nome utente'),
(81, 'ro', 'Nume utilizator'),
(82, 'it', 'Tipo utente'),
(82, 'ro', 'Tip utilizator'),
(83, 'it', 'Ruolo(i)'),
(83, 'ro', 'Rol / roluri'),
(84, 'it', 'Sei umano?'),
(84, 'ro', 'Sunteţi o persoană?'),
(85, 'it', 'Si prega di fornire il testo visualizzato nell''immagine'),
(85, 'ro', 'Introduceţi textul din imagine'),
(86, 'it', 'Cambiare {evt}'),
(86, 'ro', 'Modifică {evt}'),
(87, 'it', 'Impossibile {evt}! Si prega di leggere le informazioni qui di seguito e riprovare.'),
(87, 'ro', '{evt} eșuat! Verificaţi informaţiile de mai jos şi încercaţi din nou.'),
(88, 'it', 'password'),
(88, 'ro', 'parola'),
(89, 'it', '{evt} è stato modificato con successo.'),
(89, 'ro', '{evt} a fost modificat cu succes.'),
(92, 'it', 'Questo sarà verificato dopo la presentazione della domanda'),
(92, 'ro', 'Va fi verificată după înregistrarea cererii'),
(93, 'it', 'cambia i dettagli'),
(93, 'ro', 'modifică detalii'),
(95, 'it', 'Il nuovo nome utente che si intende utilizzare per il login'),
(95, 'ro', 'Noul nume utilizator pe care îl veţi utiliza la autentificare'),
(96, 'it', '{evt} sono stati modificati con successo.'),
(96, 'ro', '{evt} a fost modificat cu succes.'),
(97, 'it', '{evt} utente '),
(97, 'ro', 'Utilizator {evt}'),
(98, 'it', 'Creazione'),
(98, 'ro', 'Crează'),
(99, 'it', 'Creazione utente'),
(99, 'ro', 'crează utilizator'),
(100, 'it', 'Utente {evt} é stato creato.'),
(100, 'ro', 'Utilizatorul {evt} a fost generat.'),
(101, 'it', 'Registrato {evt}'),
(101, 'ro', 'Înregistrează {evt}'),
(102, 'it', 'nuovo utente'),
(102, 'ro', 'Utilizator nou'),
(103, 'it', 'Utenti Registrati'),
(103, 'ro', 'Utilizatori înregistraţi'),
(104, 'it', 'Si prega di selezionare uno dei tipi di richiesta nelle categorie inferiori.'),
(104, 'ro', 'Alegeţi tipul solicitării dintre opţiunile de mai jos.'),
(105, 'it', 'Utilizzare questi moduli per la richiesta di registrazione del vostro impianto.'),
(105, 'ro', 'Utilizaţi aceste formulare pentru a solicita înregistrarea instalaţiei dumneavoastră.'),
(106, 'it', 'Utilizzare questi moduli per la richiesta di registrazione della vostra organizzazione nel Syrio.'),
(106, 'ro', 'Utilizaţi aceste formulare pentru a solicita înregistrarea organzaţiei dumneavoastră în SyRIO.'),
(107, 'it', 'Organizzazione Autoritá Competente'),
(107, 'ro', 'Organizaţie Autoritate Competentă'),
(108, 'it', 'Organizzazione Operatori / Proprietari'),
(108, 'ro', 'Organizaţie Operatori / Proprietari'),
(109, 'it', 'Utilizzare questi moduli per l''invio di registrazione utente.'),
(109, 'ro', 'Utilizaţi aceste formulare pentru a solicita înregistrarea ca utilizator.'),
(110, 'it', 'Registrato come {evt}'),
(110, 'ro', 'Înregistrare ca {evt}'),
(111, 'it', 'view  {evt}'),
(111, 'ro', 'Afişează {evt}'),
(112, 'it', 'Assegnazioni'),
(112, 'ro', 'Sarcini'),
(113, 'it', 'Assegnazione'),
(113, 'ro', 'Sarcină'),
(115, 'it', 'Sezione {evt}'),
(115, 'ro', 'Secţiunea {evt}'),
(116, 'it', 'Sezione {id}'),
(116, 'ro', 'Secţiunea {id}'),
(117, 'it', 'Clicca su ciascuna delle sezioni qui sotto per i dettagli.'),
(117, 'ro', 'Selectaţi una din secţiunile de mai jos pentru detalii.'),
(118, 'it', 'Sezione'),
(118, 'ro', 'Secţiune'),
(119, 'it', 'bozza relazione'),
(119, 'ro', 'ciornă raport'),
(120, 'it', 'Evento'),
(120, 'ro', 'Eveniment'),
(121, 'it', 'data'),
(121, 'ro', 'data'),
(122, 'it', 'ora'),
(122, 'ro', 'ora'),
(123, 'it', '(se rilevante)'),
(123, 'ro', '(dacă este relevant)'),
(124, 'it', '(se applicabile)'),
(124, 'ro', '(dacă este aplicabil)'),
(126, 'it', 'Secondo l''allegato IX della direttiva 2013/30/UE'),
(126, 'ro', 'Conform Anexei IX a Directivei 2013/30/EU'),
(127, 'it', 'Data ed ora dell''evento'),
(127, 'ro', 'Data şi ora evenimentului'),
(128, 'it', 'Data dell''evento'),
(128, 'ro', 'Data evenimentului'),
(129, 'it', 'Ora dell''evento'),
(129, 'ro', 'Ora evenimentului'),
(130, 'it', 'Dettagli del luogo e della persona che ha comunicato l\\''evento'),
(130, 'ro', 'Detalii referitoare la localizare şi la persoana care raportează  evenimentul'),
(131, 'it', 'Operatore/proprietario'),
(131, 'ro', 'Operator/proprietar'),
(132, 'it', 'Nome/tipo dell''installazione'),
(132, 'ro', 'Numele/tipul  instalaţiei'),
(133, 'it', 'Nome/Codice del campo'),
(133, 'ro', 'Denumire câmp/cod'),
(134, 'it', 'Nome della persona che ha fatto la relazione'),
(134, 'ro', 'Numele persoanei  care raportează'),
(135, 'it', 'Ruolo della persona che ha fatto la relazione'),
(135, 'ro', 'Funcţia persoanei care raportează'),
(136, 'it', 'Recapiti'),
(136, 'ro', 'Date de contact'),
(137, 'it', 'Numero di telefono'),
(137, 'ro', 'Număr de telefon'),
(138, 'it', 'Indirizzo e-mail'),
(138, 'ro', 'Adresa de e-mail'),
(139, 'it', 'Rilascio accientale di olio, gas o altre sostanze pericolose, accesi o non acceso.'),
(139, 'ro', 'Scurgerile neintenţionate de petrol, gaze sau alte substanţe periculoase, în stare de ardere sau nu'),
(140, 'it', 'Qualsiasi rilascio accidentale di gas acceso o olio o da un impianto off-shore;'),
(140, 'ro', 'orice scurgere neintenţionată de gaze sau petrol în stare de ardere pe sau de la o instalaţie offshore;'),
(141, 'it', 'Il rilascio accidentale o non accidentale da un impianto off-shore di:'),
(141, 'ro', 'scurgerea neintenţionată pe sau de la o instalaţie offshore de:'),
(144, 'it', 'Il rilascio non intenzionale o la fuga di sostanze pericolose, per la quale il rischio di incidente è stato valutato nella relazione sui grandi rischi, o da un impianto offshore, compresi pozzi e ritorni di additivi di perforazione.'),
(144, 'ro', 'scurgerea sau pierderea neintenţionată a oricărei substanţe periculoase, pentru care a fost evaluat riscul de accident major în raportul privind accidentele majore, pe sau de la o instalaţie offshore, inclusiv sonde şi rezultate ale aditivilor de foraj.'),
(145, 'it', 'La perdita di controllo dei pozzi che richiede l''azionamento di attrezzature di controllo del pozzo, o il fallimento di un pozzo di sbarramento che richiede la sua sostituzione o la riparazione'),
(145, 'ro', 'Pierderea controlului asupra sondei, situaţie care necesită declanşarea echipamentelor de control ale sondei, sau defecţiunea unei bariere la sondă, necesitând înlocuirea sau reparaţia acesteia'),
(146, 'it', 'Qualsiasi scoppio , indipendentemente dalla durata;'),
(146, 'ro', 'orice erupţie, indiferent de durata acesteia'),
(147, 'it', 'Il guasto meccanico di qualsiasi parte di un pozzo, il cui scopo è quello di prevenire o limitare l''effetto del rilascio accidentale di fluidi da un pozzo o un serbatoio essendo disegnato su da un pozzo , o il cui guasto causerebbe o contribuirebbe a tale rilascio.'),
(147, 'ro', 'dereglarea mecanică a oricărei părţi a unei sonde, al cărei scop este acela de a preveni sau limita efectul scurgerii neintenţionate de fluide dintr-o sondă sau un rezervor utilizat de o sondă sau a cărei dereglare ar cauza sau contribui la o astfel de scurgere'),
(148, 'it', 'L''adozione di provvedimenti cautelari supplementari a qualsiasi già  contenuta nel programma di perforazione originale in cui una distanza minima prevista tra pozzetti adiacenti non è stata mantenuta.'),
(148, 'ro', 'luarea de măsuri de precauţie adiţionale în plus faţă de orice alte măsuri deja conţinute în programul de foraj iniţial unde nu a fost menţinută o distanţă proiectată minimă de separare între sondele adiacente.'),
(149, 'it', 'Fallimento di una sicurezza e di elemento critico ambientale'),
(149, 'ro', 'Defecţiunea unui element critic de siguranţă şi de mediu'),
(150, 'it', 'Qualsiasi perdita o mancata disponibilità  di un SECE richiede un''azione correttiva immediata.'),
(150, 'ro', 'Orice pierdere sau indisponibilitate a unui element critic de siguranţă şi de mediu (SECE) ce necesită măsuri imediate de remediere.'),
(152, 'it', 'Qualsiasi condizione rilevata che riduce l''integrità  strutturale progettata dell''impianto, compresa la stabilità , la galleggiabilità  e la stazione di tenuta, nella misura in cui si richiede un''azione correttiva immediata.'),
(152, 'ro', 'Orice condiţie detectată care reduce integritatea structurală proiectată a instalaţiei, inclusiv stabilitatea, flotabilitatea şi menţinerea poziţiei, în măsura în care aceasta necesită măsuri imediate de remediere.'),
(153, 'it', 'Le navi in rotta di collisione e le collisioni reali imbarcazioni, con un impianto off-shore'),
(153, 'ro', 'Navele aflate pe o traiectorie de coliziune şi coliziunile efective ale navelor cu o instalaţie offshore'),
(154, 'it', 'Qualsiasi collisione, o potenziale collisione tra una nave e un impianto off-shore che abbia o possa avere l''energia sufficiente per causare danni sufficienti per l''installazione e / o di una nave , di compromettere l''integrità  strutturale o processo globale.'),
(154, 'ro', 'Orice coliziune sau posibilă coliziune între o navă şi o instalaţie offshore care are sau ar avea destulă energie pentru a cauza suficiente daune instalaţiei şi/sau navei, pentru a pune în pericol integritatea generală structurală sau integritatea procesului.'),
(155, 'it', 'Incidenti in elicottero, sopra o vicino a impianti off-shore'),
(155, 'ro', 'Accidente implicând elicoptere care se produc pe instalaţiile offshore sau în apropierea acestora'),
(156, 'it', 'Qualsiasi collisione, o potenziale scontro tra un elicottero e un impianto off-shore.'),
(156, 'ro', 'Orice coliziune sau posibilă coliziune între un elicopter şi o instalaţie offshore.'),
(157, 'it', 'Qualsiasi incidente mortale da segnalare sotto i requisiti della direttiva 92/91/EEC'),
(157, 'ro', 'Orice accident mortal care va fi raportat în conformitate cu cerinţele Directivei 92/91/CEE'),
(158, 'it', 'Eventuali ferimento grave di cinque o pià¹ persone nello stesso incidente che é stato riportato sotto i requisiti della direttiva 92/91/EEC'),
(158, 'ro', 'Orice vătămare gravă a cinci sau mai multe persoane afectate de acelaşi accident care va fi raportat în conformitate cu cerinţele Directivei 92/91/CEE\r\n'),
(159, 'it', 'Qualsiasi evacuazione del personale'),
(159, 'ro', 'Orice evacuare a personalului'),
(160, 'it', 'Qualsiasi evacuazione di emergenza imprevista di una parte o tutto il personale come risultato, o quando vi é un rischio significativo di un incidente rilevante.'),
(160, 'ro', 'Orice evacuare de urgenţă neplanificată a unei părţi sau a întregului personal ca rezultat al -sau în cazurile în care există riscul - producerii unui accident major.'),
(161, 'it', 'Un grave incidente ambientale'),
(161, 'ro', 'Un incident de mediu major'),
(162, 'it', 'Qualsiasi grave incidente ambientale quale definita all''articolo 2.1.d e l''articolo 2.37 della direttiva 2013/30/UE.'),
(162, 'ro', 'Orice incident de mediu major, astfel cum este definit la articolul 2 punctul 1 litera (d) şi articolul 2 punctul 37 din Directiva 2013/30/UE.'),
(163, 'it', 'Informazioni generali'),
(163, 'ro', 'Informaţii generale'),
(165, 'it', 'Descrizione delle circostanze, delle conseguenza dell''evento e la risposta all''emergenza.'),
(165, 'ro', 'Descrierea circumstanţelor, a consecinţelor evenimentului şi a intervenţiei în caz de urgenţă'),
(166, 'it', 'Cause dirette e sottostanti preliminari.'),
(166, 'ro', 'Cauze preliminarii directe şi de bază.'),
(167, 'it', 'Cause dirette e sottostanti preliminari'),
(167, 'ro', 'Cauze preliminarii directe şi de bază'),
(168, 'it', '( entro 10 giorni dell''evento )'),
(168, 'ro', '(în termen de 10 zile lucrătoare de la producerea evenimentului)'),
(169, 'it', 'Iniziale lezioni apprese e le preliminari raccomandazioni per prevenire il ripetersi di eventi simili.'),
(169, 'ro', 'Lecţii iniţiale învăţate şi recomandări preliminarii pentru prevenirea reapariţiei evenimentelor similare'),
(170, 'it', 'Nome della nave'),
(170, 'ro', 'Numele navei'),
(171, 'it', 'Indicare il sistema che non ha fallito e fornisce una descrizione delle circostanze dell''evento / descrivendo quanto é successo'),
(171, 'ro', 'Indicaţi sistemul care s-a defectat şi oferiţi o descriere a circumstanţelor în care s-a produs evenimentul'),
(172, 'it', 'tra cui le condizioni meteo e stato del mare'),
(172, 'ro', 'inclusiv condiţii meteorologice şi starea mării.'),
(173, 'it', 'L''autorità  competente completerà  ulteriormente questa sezione'),
(173, 'ro', 'Această secţiune va fi completată în continuare de către autoritatea competentă.'),
(174, 'it', 'Questo é considerato un grave incidente?'),
(174, 'ro', 'Este considerat acesta un incident major?'),
(175, 'it', 'Dare una giustificazione'),
(175, 'ro', 'Justificaţi'),
(176, 'it', 'Accedere'),
(176, 'ro', 'Autentificare'),
(179, 'it', 'Significativa perdita di integrità  strutturale, o perdita di protezione contro gli effetti di incendio o di esplosione, o perdita della stazione di tenuta in relazione ad un impianto mobile, '),
(179, 'ro', 'pierderea semnificativă a integrităţii structurale sau pierderea protecţiei împotriva efectelor incendiului sau exploziei ori pierderea menţinerii poziţiei în cazul unei instalaţii mobile'),
(180, 'it', 'Data/ora dell\\''evento'),
(180, 'ro', 'Data şi ora evenimentului'),
(181, 'it', 'Nome dell''evento'),
(181, 'ro', 'Numele evenimentului'),
(182, 'it', 'Descrizione di circostanze'),
(182, 'ro', 'Descrierea circumstanţelor'),
(183, 'it', 'Conseguenze di eventi e interventi di emergenza'),
(183, 'ro', 'Consecinţele evenimentului şi intervenţia în caz de urgenţă'),
(184, 'it', 'Classificazione evento'),
(184, 'ro', 'Clasificare eveniment'),
(186, 'it', 'Tipo di evento comunicato?'),
(186, 'ro', 'Ce tip de eveniment este raportat?'),
(187, 'it', '(E'' possibile effettuare piú di una scelta)'),
(187, 'ro', '(Pot fi selectate mai multe opţiuni)'),
(188, 'it', 'EMISSIONE ACCIDENTALE DI PETROLIO, GAS O ALTRE SOSTANZE'),
(188, 'ro', 'SCURGERILE NEINTENŢIONATE DE PETROL, GAZE SAU ALTE SUBSTANŢE PERICULOASE, '),
(189, 'it', 'PERICOLOSE, INFIAMMATE O NON INFIAMMATE'),
(189, 'ro', 'ÎN STARE DE ARDERE SAU NU'),
(190, 'it', 'A.1. Si é verificata un''emissione di sostanze idrocarburi?'),
(190, 'ro', 'A.1. A existat o scurgere de hidrocarburi?'),
(191, 'it', 'Si'),
(191, 'ro', 'Da'),
(192, 'it', 'No'),
(192, 'ro', 'Nu'),
(193, 'it', 'Idrocarburi (HC) emessi'),
(193, 'ro', 'Hidrocarbură (HC) deversată'),
(195, 'it', '(Specificare)'),
(195, 'ro', '(Specificaţi)'),
(196, 'it', 'NON PROCESSO'),
(196, 'ro', 'ÎN AFARA PROCESULUI'),
(197, 'it', 'PROCESSO'),
(197, 'ro', 'ÎN CADRUL PROCESULUI'),
(198, 'it', 'Petrolio'),
(198, 'ro', 'Petrol'),
(199, 'it', 'Condensato'),
(199, 'ro', 'Condensat'),
(200, 'it', 'Gas'),
(200, 'ro', 'Gaz'),
(201, 'it', '2-Phase'),
(201, 'ro', '2 faze'),
(202, 'it', 'Per il gas o 2-Phase, livello dello stato di H'),
(202, 'ro', 'Pentru gaz sau 2 faze, menţionaţi nivelul de H'),
(203, 'it', '(ppm stimato)'),
(203, 'ro', '(ppm estimat)'),
(204, 'it', 'Quantità  stimata emessa'),
(204, 'ro', 'Cantitate deversată estimată'),
(205, 'it', 'Specificare unità  di misura, ad esempio tonnellate, kg, Nm'),
(205, 'ro', 'Specificaţi unităţile, de exemplu, tone, kg, Nm'),
(206, 'it', 'Stima dell''iniziale tasso di emissione'),
(206, 'ro', 'Rata iniţială de deversare estimată'),
(207, 'it', 'Specificare unità , ad esempio tonnellate/giorno , kg/s , Nm'),
(207, 'ro', 'Specificaţi unităţile, de exemplu, tone/zi, kg/s, Nm'),
(208, 'it', 'Durata della perdita'),
(208, 'ro', 'Durata scurgerii'),
(209, 'it', 'Tempo stimato dalla scoperta  per esempio allarmi, registrazione in formato elettronico, fino alla fine della perdita'),
(209, 'ro', 'Interval estimat de la descoperire, de exemplu, alarmă, jurnal electronic, până la terminarea scurgerii'),
(210, 'it', 'Luogo della perdita'),
(210, 'ro', 'Localizarea scurgerii'),
(211, 'it', 'Classificazione dell''area pericolosa'),
(211, 'ro', 'Clasificarea zonei periculoase'),
(212, 'it', 'ad esempio la zona di localizzazione dell''incidente'),
(212, 'ro', 'şi anume zona de la locul incidentului'),
(213, 'it', 'Non classificati'),
(213, 'ro', 'Neclasificat'),
(215, 'it', 'Non applicabile'),
(215, 'ro', 'nu se aplică'),
(216, 'it', 'Naturale'),
(216, 'ro', 'Naturală'),
(217, 'it', 'Forzato'),
(217, 'ro', 'Forţată'),
(218, 'it', 'Quanti lati chiuso?'),
(218, 'ro', 'Câte laturi sunt cuprinse?'),
(219, 'it', '(Inserire il numero di pareti, tra cui pavimento e soffitto)'),
(219, 'ro', '(Introduceţi numărul de pereţi, inclusiv podea şi plafon)'),
(220, 'it', 'Modulo volume'),
(220, 'ro', 'Volum al modulului'),
(221, 'it', 'Stima del numero di ricambi d''aria'),
(221, 'ro', 'Număr de schimburi de aer estimat'),
(222, 'it', 'conosciuto'),
(222, 'ro', 'este cunoscut'),
(223, 'it', 'Specificare tariffa oraria'),
(223, 'ro', 'Specificaţi tariful orar'),
(224, 'it', 'Condizioni meteo'),
(224, 'ro', 'Condiţii meteorologice'),
(225, 'it', 'Velocità  del vento'),
(225, 'ro', 'Viteza vântului'),
(226, 'it', 'Direzione del vento'),
(226, 'ro', 'Direcţia vântului'),
(227, 'it', '(Specificare le unità  , ad esempio mph, m/s , ft/s)'),
(227, 'ro', '(Specificaţi unităţile, de exemplu, mph, m/s, ft/s)'),
(228, 'it', '(Specificare voce in gradi),'),
(228, 'ro', '(Specificaţi direcţia în grade)'),
(229, 'it', 'Fornire una descrizione di altre rilevanti condizioni meteorologiche'),
(229, 'ro', 'Oferiţi o descriere a altor condiţii meteorologice relevante'),
(230, 'it', 'Pressione del sistema'),
(230, 'ro', 'Presiune sistem'),
(231, 'it', 'Pressione di progetto'),
(231, 'ro', 'Presiune proiectată'),
(232, 'it', 'Pressione reale'),
(232, 'ro', 'Presiune reală'),
(233, 'it', '(Precisare le unità , per es. bar, psi o altro)'),
(233, 'ro', '(Specificaţi unităţile, de exemplu, bar, psi sau altele)'),
(234, 'it', '(ad esempio al momento dell''emissione)'),
(234, 'ro', '(şi anume la momentul deversării)'),
(235, 'it', 'Mezzi di rilevamento'),
(235, 'ro', 'Mijloace de detectare'),
(236, 'it', 'Incendio'),
(236, 'ro', 'Foc'),
(237, 'it', 'Fumo'),
(237, 'ro', 'Fum'),
(238, 'it', 'Altro'),
(238, 'ro', 'Altele'),
(239, 'it', '(Per favore contrassegnare il tipo di rilevatore o specificare)'),
(239, 'ro', '(Vă rugăm bifaţi tipul de detector sau specificaţi, după caz)'),
(240, 'it', '(Per favore specificare)'),
(240, 'ro', '(Specificaţi)'),
(241, 'it', 'Causa della perdita'),
(241, 'ro', 'Cauza scurgerii'),
(242, 'it', '(Si prega di dare una breve descrizione e compilare la lista di controllo "Cause" di seguito)'),
(242, 'ro', '(Vă rugăm oferiţi o scurtă descriere şi completaţi lista de verificare "Cauza" de mai jos)'),
(243, 'it', 'Si é verificata una combustione?'),
(243, 'ro', 'A avut loc aprinderea?'),
(245, 'it', 'é stata:'),
(245, 'ro', 'aprinderea s-a produs'),
(246, 'it', 'Immediata'),
(246, 'ro', 'Imediat'),
(247, 'it', 'Differita'),
(247, 'ro', 'Cu întârziere'),
(248, 'it', 'Ritardo'),
(248, 'ro', 'Timp de întârziere'),
(249, 'it', '(sec)'),
(249, 'ro', '(sec)'),
(250, 'it', 'Si é verificato'),
(250, 'ro', 'A existat'),
(251, 'it', '(aggiungere la sequenza di eventi numerando le apposite caselle in ordine cronologico)'),
(251, 'ro', '(adăugaţi ordinea evenimentelor prin numerotarea căsuţelor adecvate, în ordinea apariţiei acestora)'),
(252, 'it', 'Fiammata'),
(252, 'ro', 'Un incendiu fulger (Flash Fire)'),
(253, 'it', 'Esplosione'),
(253, 'ro', 'O explozie'),
(254, 'it', 'Dardo di fuoco'),
(254, 'ro', 'Un incendiu tip jet de foc (Jet Fire)\r\n'),
(255, 'it', 'Incendio da pozza'),
(255, 'ro', 'O incendiere a unor bălţi de lichid (Pool fire)'),
(256, 'it', 'Origine della combustione'),
(256, 'ro', 'Sursa de aprindere'),
(257, 'it', 'Fornire una descrizione della fonte di combustione'),
(257, 'ro', 'Oferiţi o descriere a sursei de aprindere'),
(258, 'it', 'Quali misure di emergenza sono state adottate?'),
(258, 'ro', 'Ce măsură de urgenţă a fost luată?'),
(259, 'it', 'Chiusura'),
(259, 'ro', 'Oprire'),
(260, 'it', 'Automatica'),
(260, 'ro', 'automată'),
(261, 'it', 'Manuale'),
(261, 'ro', 'manuală'),
(262, 'it', 'Depressurizzazione'),
(262, 'ro', 'Evacuare'),
(263, 'it', 'Inondazione'),
(263, 'ro', 'Inundare'),
(264, 'it', 'halon/inerti'),
(264, 'ro', 'halon/gaze inerte'),
(265, 'it', 'Chiamata a raccolta'),
(265, 'ro', 'Apel de adunare'),
(266, 'it', 'Altro, precisare'),
(266, 'ro', 'Altele, specificaţi'),
(267, 'it', 'Eventuali aggiuntivi commenti'),
(267, 'ro', 'Orice observaţii suplimentare'),
(268, 'it', 'ELENCO CAUSE DELLA PERDITA ( Vedi punto A.1.XI "Cause di perdita" )'),
(268, 'ro', 'LISTĂ DE VERIFICARE PENTRU CAUZA SCURGERII (a se vedea punctul A.1.XI "Cauza scurgerii")'),
(269, 'it', '(Indicare gli elementi che identificano pià¹ accuratamente la causa o le cause della perdita)'),
(269, 'ro', '(Vă rugăm indicaţi acele elemente care sunt cele mai apropiate de identificarea cauzei scurgerii)'),
(270, 'it', 'Indicare la causa(e) dell''emissione'),
(270, 'ro', 'Indicaţi cauza (cauzele) deversării'),
(271, 'it', 'In ciascuna delle seguenti categorie'),
(271, 'ro', 'La fiecare dintre categoriile de mai jos '),
(272, 'it', 'barrare le caselle corrispondenti'),
(272, 'ro', 'bifaţi căsuţele relevante'),
(273, 'it', 'Progettazione'),
(273, 'ro', 'Proiectare'),
(274, 'it', 'Guasto connesso alla progettazione'),
(274, 'ro', 'Defecţiune legată de proiectare'),
(275, 'it', 'Attrezzatura'),
(275, 'ro', 'Echipament'),
(276, 'it', 'Corrosione interna'),
(276, 'ro', 'Coroziune internă'),
(277, 'it', 'Corrosione esterna'),
(277, 'ro', 'Coroziune externă'),
(278, 'it', 'Guasto meccanico da fatica'),
(278, 'ro', 'Dereglare mecanică datorată oboselii'),
(279, 'it', 'Guasto meccanico da usura'),
(279, 'ro', 'Dereglare mecanică datorată uzurii'),
(280, 'it', 'Erosione'),
(280, 'ro', 'Eroziune'),
(281, 'it', 'Difetto materiale'),
(281, 'ro', 'Defect material'),
(282, 'it', 'Funzionamento'),
(282, 'ro', 'Operare'),
(283, 'it', 'Montaggio scorretto'),
(283, 'ro', 'Echipare incorectă'),
(284, 'it', 'Lasciato aperto'),
(284, 'ro', 'Lăsată deschisă'),
(285, 'it', 'Ispezione scorretta'),
(285, 'ro', 'Inspecţie necorespunzătoare'),
(286, 'it', 'Collaudo scorretto'),
(286, 'ro', 'Testare necorespunzătoare'),
(287, 'it', 'Funzionamento scorretto'),
(287, 'ro', 'Operare necorespunzătoare'),
(288, 'it', 'Manutenzione scorretta'),
(288, 'ro', 'Întreţinere necorespunzătoare'),
(289, 'it', 'Oggetto caduto'),
(289, 'ro', 'Obiect scăpat'),
(290, 'it', 'Altri impatti'),
(290, 'ro', 'Altă sursă de impact'),
(291, 'it', 'Apertura con contenuto di idrocarburi'),
(291, 'ro', 'Deschisă atunci când conţinea HC'),
(292, 'it', 'Procedura'),
(292, 'ro', 'Procedural'),
(293, 'it', 'Mancata conformità  alla procedura'),
(293, 'ro', 'Nerespectarea procedurii'),
(294, 'it', 'Mancata conformità  alla licenza'),
(294, 'ro', 'Nerespectarea permisului de lucru'),
(295, 'it', 'Procedura carente'),
(295, 'ro', 'Procedură deficitară'),
(296, 'it', 'Indicare la modalità  operativa nella zona al momento dell''emissione'),
(296, 'ro', 'Indicaţi modul operaţional în zonă, la momentul deversării'),
(297, 'it', 'Selezionare'),
(297, 'ro', 'Selectaţi'),
(298, 'it', 'un'),
(298, 'ro', 'un'),
(299, 'it', 'parametro fra le seguenti categorie e barrare le caselle corrispondenti'),
(299, 'ro', 'parametru dintre următoarele categorii şi bifaţi căsuţele relevante'),
(301, 'it', 'Perforazione'),
(301, 'ro', 'Foraj'),
(302, 'it', 'Operazioni di pozzo'),
(302, 'ro', 'Operaţiuni la sondă'),
(303, 'it', '(Precisare il tipo di operazione, per es. carotaggio a fune, prova di pozzo ecc.)'),
(303, 'ro', '(specificaţi operaţiunea reală, de exemplu cablu de oţel, test sondă etc.)'),
(304, 'it', 'Produzione'),
(304, 'ro', 'Producţie'),
(305, 'it', 'Manutenzione'),
(305, 'ro', 'Întreţinere'),
(306, 'it', 'Costruzione'),
(306, 'ro', 'Construcţie'),
(307, 'it', 'Operazioni sugli oleodotti, inclusa la pulitura'),
(307, 'ro', 'Operaţiuni la conducte, inclusiv pigging'),
(308, 'it', 'A.2. Descrizione delle circostanze, delle conseguenze dell''evento e risposta all''emergenza'),
(308, 'ro', 'A.2. Descrierea circumstanţelor, a consecinţelor evenimentului şi a intervenţiei în caz de urgenţă'),
(309, 'it', 'Si é verificata un''emissione di sostanze pericolose diverse dagli idrocarburi?'),
(309, 'ro', 'A existat o deversare a unei substanţe periculoase, alta decât o hidrocarbură?'),
(310, 'it', 'precisare il tipo e il quantitativo di sostanza emessa'),
(310, 'ro', 'specificaţi tipul şi cantitatea substanţei deversate'),
(311, 'it', '(Tipo)'),
(311, 'ro', '(Tip)'),
(312, 'it', '(Quantitativo, precisare le unità )'),
(312, 'ro', '(Cantitate, specificaţi unităţile)'),
(313, 'it', 'Si é verificato un incendio non dovuto a idrocarburi (per es. elettrico) con significativamente in grado di provocare un grave incidente?'),
(313, 'ro', 'A existat un foc declanşat din altă cauză decât scurgerea unei hidrocarburi (de exemplu, electrică) cu un potenţial semnificativ pentru declanşarea unui accident major?'),
(314, 'it', 'Descrivere le circostanze'),
(314, 'ro', 'Descrieţi circumstanţele'),
(315, 'it', 'L''incidente puó degradare l''ambiente marino circostante?'),
(315, 'ro', 'Este incidentul susceptibil de a cauza degradarea mediului marin înconjurător?'),
(316, 'it', 'illustrare gli impatti ambientali già  osservati o che sono conseguenza dall''incidente'),
(316, 'ro', 'precizaţi impactul asupra mediului observat deja sau care este susceptibil de a rezulta în urma incidentului'),
(317, 'it', 'Cause dirette e sottostanti preliminari. (entro 10 giorni lavorativi dall''evento)'),
(317, 'ro', 'Cauze preliminarii directe şi de bază (în termen de 10 zile lucrătoare de la producerea evenimentului)'),
(318, 'it', 'Iniziale esperienza acquisita e raccomandazioni preliminari per evitare la ripetizione di eventi analoghi (entro 10 giorni lavorativi dall''evento)'),
(318, 'ro', 'Lecţii iniţiale învăţate şi recomandări preliminarii pentru prevenirea reapariţiei evenimentelor similare (în termen de 10 zile lucrătoare de la producerea evenimentului)'),
(322, 'it', 'FINE DELLA RELAZIONE'),
(322, 'ro', 'FINALUL RAPORTULUI'),
(325, 'it', 'PERDITA DI CONTROLLO DEI POZZI CHE'),
(325, 'ro', 'DEFECŢIUNEA UNUl ELEMENT CRITIC DE SIGURANŢĂ ŞI DE MEDIU'),
(326, 'it', 'RICHIEDA L''ATTIVAZIONE DI APPARECCHIATURE DI CONTROLLO DEGLI STESSI, O'),
(326, 'ro', 'PIERDEREA CONTROLULUI ASUPRA SONDEI, SITUAŢIE CARE NECESITĂ DECLANŞAREA ECHIPAMENTELOR DE CONTROL ALE SONDEI, SAU '),
(327, 'it', 'GUASTO DELLA BARRIERA DI UN POZZO CHE RICHIEDA LA SUA SOSTITUZIONE O RIPARAZIONE'),
(327, 'ro', 'DEFECŢIUNEA UNEI BARIERE LA SONDĂ, NECESITÂND ÎNLOCUIREA SAU REPARAŢIA ACESTEIA'),
(328, 'it', 'Nome/codice del pozzo'),
(328, 'ro', 'Numele/codul sondei'),
(329, 'it', 'Nome dell''appaltatore incaricato della trivellazione (se pertinente)'),
(329, 'ro', 'Numele contractantului care desfăşoară activităţi de foraj (dacă este cazul)'),
(330, 'it', 'Nome/tipo della piattaforma di trivellazione (se pertinente)'),
(330, 'ro', 'Numele/tipul platformei de foraj (dacă este cazul)'),
(331, 'it', 'Data di inizio e fine/durata della perdita di controllo del pozzo'),
(331, 'ro', 'Data de început şi finalizare/momentul pierderii controlului asupra sondei'),
(332, 'it', 'Tipo di fluido: salamoia / petrolio / gas'),
(332, 'ro', 'Tipul de lichid: saramură / petrol / gaz / ... (dacă este cazul)'),
(333, 'it', 'Completamento della testa di pozzo: in superficie / sottomarina'),
(333, 'ro', 'Finalizare capăt sondă: suprafaţă / submarin'),
(335, 'it', 'Serbatoio: pressione / temperatura / profondità '),
(335, 'ro', 'Rezervor: presiune / temperatură / adâncime'),
(336, 'it', 'Tipo di attività : produzione normale / trivellazione / operazioni di ripresa / operazioni di manutenzione'),
(336, 'ro', 'Tip de activitate: lucrări normale de producţie / forare / lucrări de intervenţie / servicii sonde'),
(337, 'it', 'Tipo di manutenzione del pozzo (se pertinente): carotaggio a fune / coiled tibong / manovra in pressione'),
(337, 'ro', 'Tipul de servicii la sondă (dacă este cazul): cablu de oţel / tubing flexibil / introducerea ţevilor sub presiune'),
(338, 'it', 'Attivazione dei dispositivi antieruzione'),
(338, 'ro', 'Echipament de prevenire a erupţiei activat'),
(339, 'it', 'Sistema deviatore in funzione'),
(339, 'ro', 'Sistem de deviere în funcţiune'),
(340, 'it', 'Aumento della pressione e/o controllo del flusso positivo'),
(340, 'ro', 'Formare de presiune şi/sau verificare a debitului cu rezultat pozitiv'),
(341, 'it', 'Guasti delle barriere del pozzo'),
(341, 'ro', 'Bariere sondă defecte'),
(343, 'it', '(precisare le unità )'),
(343, 'ro', '(specificaţi unităţile)'),
(344, 'it', 'Durata del deflusso non controllato di fluidi del pozzo'),
(344, 'ro', 'Durata scurgerii necontrolate a fluidelor din sondă'),
(345, 'it', 'Portata'),
(345, 'ro', 'Debitul sondei'),
(346, 'it', 'Volume del liquido'),
(346, 'ro', 'Volum de lichid'),
(347, 'it', 'Volume del gas'),
(347, 'ro', 'Volum de gaz'),
(349, 'it', '((Ad esempio: 1. dardo di fuoco / 2. prima esplosione / 3. seconda esplosione, ecc.)'),
(349, 'ro', '(De exemplu, 1. jet fire/2. prima exp/ozie/3. a doua explozie etc.)'),
(351, 'it', 'Fallimento di una sicurezza e di elemento critico ambientale'),
(351, 'ro', 'DEFECŢIUNEA UNUl ELEMENT CRITIC DE SIGURANŢĂ ŞI DE MEDIU'),
(352, 'it', 'Nome del verificatore indipendente (se applicabile)'),
(352, 'ro', 'Numele verificatorului independent (dacă este cazul)'),
(353, 'it', 'Descrizione del SECE e delle circostanze'),
(353, 'ro', 'Descrierea elementelor critice de siguranţă şi de mediu şi a circumstanţelor'),
(354, 'it', 'Quali sistemi critici per la sicurezza e l''ambiente sono stati segnalati dal verificatore indipendente come perduti o non disponibili, con necessità  di un''azione correttiva immediata o non hanno funzionato durante un incidente?'),
(354, 'ro', 'Ce sisteme critice de siguranţă şi de mediu au fost raportate de către verificatorul independent ca fiind pierdute sau indisponibile, care necesită o acţiune de remediere imediată sau s-au defectat în timpul incidentului?'),
(355, 'it', 'Relazione del verificatore indipendente: dettagli (n. della relazione/data/verificatore / ...)'),
(355, 'ro', 'Raportul verificatorului independent: detalii (raport nr. /data / verificator / ...)'),
(356, 'it', 'Guasto durante un incidente grave: dettagli (data/descrizione dell''incidente / ...)'),
(356, 'ro', 'Defecţiune în timpul unui accident major: detalii (data / descrierea accidentului / ... )'),
(358, 'it', 'Sistemi di integrità  strutturale'),
(358, 'ro', 'Sisteme de integritate structurală'),
(359, 'it', 'Strutture di superficie'),
(359, 'ro', 'Structuri pe puntea superioară'),
(360, 'it', 'Strutture sottomarine'),
(360, 'ro', 'Structuri submarine'),
(361, 'it', 'Gru e apparecchi di sollevamento'),
(361, 'ro', 'Macarale & echipamente de ridicare'),
(362, 'it', 'Sistemi di ormeggio (cime d''ormeggio, posizionamento dinamico)'),
(362, 'ro', 'Sisteme de acostare (support de ancorare, poziţionare dinamică)'),
(364, 'it', 'Barriera primaria del pozzo'),
(364, 'ro', 'Barieră principală sondă'),
(365, 'it', 'Barriera secondaria del pozzo'),
(365, 'ro', 'Barieră secundară sondă'),
(366, 'it', 'Attrezzatura di carotaggio'),
(366, 'ro', 'Echipament cablu de oţel'),
(367, 'it', 'Lavorazione dei fanghi'),
(367, 'ro', 'Procesare nămol'),
(368, 'it', 'Filtri a sabbia'),
(368, 'ro', 'Filtre de nisip'),
(369, 'it', 'Condotte e colonne'),
(369, 'ro', 'Conducte & coloane ascendente'),
(370, 'it', 'Sistema di tubature'),
(370, 'ro', 'Sistem de conducte'),
(371, 'it', 'Recipienti a pressione'),
(371, 'ro', 'Vase de presiune'),
(372, 'it', 'Apparecchiature per il controllo dei processi dei pozzi â€” BOP'),
(372, 'ro', 'Echipament de control al sondei - BOP'),
(373, 'it', 'Sistemi di controllo della combustione'),
(373, 'ro', 'Sisteme de control al aprinderii'),
(374, 'it', 'Ventilazione della zona a rischio'),
(374, 'ro', 'Ventilaţie zonă periculoasă'),
(375, 'it', 'Ventilazione della zona non a rischio'),
(375, 'ro', 'Ventilaţie zonă nepericuloasă'),
(376, 'it', 'Attrezzature certificate ATEX'),
(376, 'ro', 'Echipament certificat ATEX'),
(377, 'it', 'Attrezzature intervento elettrico'),
(377, 'ro', 'Echipament de declanşare electrică'),
(378, 'it', 'Messa a terra / incollaggio attrezzature'),
(378, 'ro', 'Echipament de legare la pamânt / îmbinare'),
(379, 'it', 'Impianto di gas inerte'),
(379, 'ro', 'Sistem gaze inerte'),
(380, 'it', 'Sistemi di rilevamento'),
(380, 'ro', 'Sisteme de detectare'),
(381, 'it', 'Rilevamento di incendio e gas'),
(381, 'ro', 'Detectare foc şi gaze'),
(382, 'it', 'Dispositivo di controllo delle iniezioni chimiche'),
(382, 'ro', 'Monitorizare injectare chimică'),
(383, 'it', 'Sabbia'),
(383, 'ro', 'Nisip'),
(384, 'it', 'Sistemi di attenuazione per il contenimento del processo'),
(384, 'ro', 'Sisteme de siguranţă pentru izolarea proceselor'),
(385, 'it', 'WApparecchiature per il controllo dei processi dei pozzi'),
(385, 'ro', 'Echipament de control al sondei - deviator'),
(386, 'it', 'Sistemi di attenuazione divertore'),
(386, 'ro', 'Sisteme de siguranţă'),
(387, 'it', 'Pavimenti impermeabili ai gas'),
(387, 'ro', 'Podele etanşe la gaze'),
(388, 'it', 'Sistemi di protezione'),
(388, 'ro', 'Sisteme de protecţie'),
(390, 'it', 'Sistema a schiuma sulla piattaforma per elicotteri'),
(390, 'ro', 'Sistem cu spumă pentru puntea de aterizare pentru elicoptere'),
(391, 'it', 'Pompe antincendio ad acqua'),
(391, 'ro', 'Pompe de stingere a incendiului cu apă'),
(392, 'it', 'Sistema antincendio ad acqua'),
(392, 'ro', 'Sistem de stingere a incendiului cu apă'),
(393, 'it', 'Sistema antincendio passivo'),
(393, 'ro', 'Sistem pasiv de protecţie împotriva incendiilor'),
(394, 'it', 'Barriere antifuoco/antideflagrazione'),
(394, 'ro', 'Pereţi antifoc/antiexplozie'),
(395, 'it', 'Impianto antincendio / Halon CO2'),
(395, 'ro', 'Sistem de stingere a incendiilor cu CO<sub>2</sub>/halon'),
(396, 'it', 'Sistemi di blocco'),
(396, 'ro', 'Sisteme de oprire'),
(397, 'it', 'Blocco di una singola apparecchiatura (LSD)'),
(397, 'ro', 'Sistem local de oprire (LSD)'),
(398, 'it', 'Blocco del processo (PSD)'),
(398, 'ro', 'Sistem de oprire a proceselor (PSD)'),
(399, 'it', 'Blocco di emergenza (ESD)'),
(399, 'ro', 'Sistem de oprire în caz de urgenţă (ESD)'),
(400, 'it', 'Valvola di isolamento sottomarina (SSIV)'),
(400, 'ro', 'Vană de izolare submarină (SSIV)'),
(401, 'it', 'Valvola ESD della colonna montante'),
(401, 'ro', 'Vană ESD conductă ascendentă'),
(402, 'it', 'Valvola ESD della colonna montante'),
(402, 'ro', 'Vană ESD pe puntea superioară'),
(404, 'it', 'Ausili alla navigazione'),
(404, 'ro', 'Ajutoare navigare'),
(405, 'it', 'Ausili alla navigazione aerea'),
(405, 'ro', 'Ajutoare navig. aeronave'),
(406, 'it', 'Ausili alla navigazione marittima'),
(406, 'ro', 'Ajutoare navig. nave'),
(407, 'it', 'Ausili alla navigazione marittima'),
(407, 'ro', 'Echipament rotativ - alimentare cu energie electrică'),
(408, 'it', 'Ausili alla navigazione marittima'),
(408, 'ro', 'Turbină P.M. pentru compresor'),
(409, 'it', 'Turbina P.M. per generatore'),
(409, 'ro', 'Turbină P.M. pentru generator'),
(410, 'it', 'Attrezzature di evacuazione e salvataggio'),
(410, 'ro', 'Echipament de ieşire în caz de urgenţe, evacuare şi salvare'),
(411, 'it', 'Dispositivi di sicurezza personale'),
(411, 'ro', 'Echipament de siguranţă personal'),
(412, 'it', 'Scialuppe di salvataggio / TEMPSC'),
(412, 'ro', 'Bărci de salvare / bărci TEMPSC'),
(413, 'it', 'Mezzi di evacuazione terziari (lance di salvataggio)'),
(413, 'ro', 'Mijloace de evacuare terţiare (aeronave)'),
(414, 'it', 'Rifugio temporaneo / punto di raccolta'),
(414, 'ro', 'Zonă temporară de refugiu / Adunare a echipajului'),
(415, 'it', 'Rifugio temporaneo / punto di raccolta'),
(415, 'ro', 'Unităţi de căutare şi salvare'),
(416, 'it', 'Sistemi di comunicazione'),
(416, 'ro', 'Sisteme de comunicare'),
(417, 'it', 'Radio / telefoni'),
(417, 'ro', 'Radiouri / telefoane'),
(418, 'it', 'Appello pubblico'),
(418, 'ro', 'Adresă publică'),
(421, 'it', 'Descrivete le importanti lezioni apprese dall''evento. Elencare le raccomandazioni per evitare il ripetersi di eventi simili.'),
(421, 'ro', 'Descrieţi orice lecţie importantă învăţată urmare a evenimentului. Menţionaţi recomandări pentru prevenirea reapariţiei evenimentelor similare.'),
(424, 'it', 'Indicare il sistema guasto e descrivere le circostanze dell''evento / descrivere quanto accaduto, comprese le condizioni meteorologiche e lo stato del mare.'),
(424, 'ro', 'Indicaţi sistemul care s-a detectat şi oferiţi o descriere a circumstanţelor în care s-a produs evenimentul / descrieţi ce s-a întâmplat, inclusiv condiţii meteorologice şi starea mării.\r\n'),
(425, 'it', 'SIGNIFICATIVA PERDITA DI INTEGRITà€ STRUTTURALE, O PERDITA DI PROTEZIONE CONTRO'),
(425, 'ro', 'PIERDEREA SEMNIFICATIVĂ A INTEGRITĂTII STRUCTURALE SAU PIERDEREA PROTECTIEI ÎMPOTRIVA '),
(426, 'it', 'GLI EFFETTI DI UN INCENDIO O UN''ESPLOSIONE, O PERDITA DI STAZIONARIETà€ IN RELAZIONE'),
(426, 'ro', 'EFECTELOR INCENDIULUI SAU EXPLOZIEI ORI PIERDEREA MENŢINERII POZIŢIEI '),
(427, 'it', 'A UN IMPIANTO MOBILE'),
(427, 'ro', 'ÎN CAZUL UNEI INSTALAŢII MOBILE'),
(429, 'it', 'INCIDENTI CHE COINVOLGONO ELICOTTERI, SULL''IMPIANTO IN MARE'),
(429, 'ro', 'NAVELE AFLATE PE O TRAIECTORIE DE COLIZIUNE ŞI COLIZIUNILE EFECTIVE ALE NAVELOR '),
(430, 'it', 'O NELLE SUE VICINANZE'),
(430, 'ro', 'CU O INSTALAŢIE OFFSHORE'),
(431, 'it', 'Nome/ stato di bandiera della nave (Se applicabile)'),
(431, 'ro', 'Nume/Stat de pavilion navă (dacă este cazul)'),
(432, 'it', 'Tipo /stazza della nave (Se applicabile)'),
(432, 'ro', 'Tip/tonaj navă (dacă este cazul)'),
(433, 'it', 'Contatti via AIS?'),
(433, 'ro', 'Contact prin AIS?'),
(435, 'it', 'Indicare il sistema guasto e descrivere le circostanze dell''evento/descrivere quanto accaduto (distanza minima fra la nave e l''impianto, rotta e velocità  della nave, e le condizioni meteorologiche)'),
(435, 'ro', 'Indicaţi sistemul care s-a defectat şi oferiţi o descriere a circumstanţelor în care s-a produs evenimentul / descrieţi ce s-a întâmplat (distanţa minimă între navă şi instalaţie, cursul şi viteza navei, condiţii meteorologice)'),
(438, 'it', 'Nome dell''appaltatore dell''elicottero'),
(438, 'ro', 'Numele contractantului elicopterului'),
(439, 'it', 'Tipo dell''elicottero'),
(439, 'ro', 'Tipul elicopterului'),
(440, 'it', 'Numero di persone a bordo'),
(440, 'ro', 'Număr de persoane la bord'),
(441, 'it', 'Gli incidenti che coinvolgono elicotteri sono comunicati a norma dei regolamenti CAA. Se si verifica un incidente che coinvolge un elicottero in relazione alla direttiva 2013/30/EU, si compila la sezione F.'),
(441, 'ro', 'Incidentele ce implică elicoptere sunt raportate în conformitate cu reglementările CAA. Dacă are loc un accident de elicopter în raport cu Directiva 2013/30/UE, se va completa secţiunea F.'),
(442, 'it', 'Indicare il sistema guasto e descrivere le circostanze dell''evento/descrivere quanto accaduto (condizioni meteorologiche)'),
(442, 'ro', 'Indicaţi sistemul care s-a defectat şi oferiţi o descriere a circumstanţelor în care s-a produs evenimentul / descrieţi ce s-a întâmplat (condiţii meteorologice)'),
(443, 'it', 'La sezione G deve essere comunicati ai sensi della direttiva 92/91/EEC'),
(443, 'ro', 'Secţiunea G este raportată în conformitate cu cerinţele Directivei 92/91/CEE'),
(444, 'it', 'La sezione H deve essere comunicati ai sensi della direttiva 92/91/EEC'),
(444, 'ro', 'Secţiunea H este raportată în conformitate cu cerinţele Directivei 92/91/CEE'),
(446, 'it', 'Qualsiasi evacuazione del personale'),
(446, 'ro', 'ORICE EVACUARE A PERSONALULUI'),
(447, 'it', 'Data/ora di inizio e fine dell''evacuazione'),
(447, 'ro', 'Data de început şi finalizare/momentul evacuării '),
(448, 'it', 'Era un''evacuazione a titolo precauzionale o di emergenza?'),
(448, 'ro', 'A reprezentat evacuarea o măsură de precauţie sau o urgenţă?'),
(449, 'it', 'Precauzionale'),
(449, 'ro', 'Măsură de precauţie'),
(450, 'it', 'Emergenza'),
(450, 'ro', 'Urgenţă'),
(451, 'it', 'Emergenza'),
(451, 'ro', 'Ambele'),
(452, 'it', 'Numero di persone evacuate'),
(452, 'ro', 'Numărul de persoane evacuate'),
(453, 'it', 'Mezzi di evacuazionen'),
(453, 'ro', 'Mijloace de evacuare'),
(454, 'it', '(ad esempio elicottero)'),
(454, 'ro', '(e.g. elicopter)'),
(455, 'it', 'Indicare il sistema guasto e descrivere le circostanze dell''evento / descrivere quanto accaduto, tranne se già  riportato in una precedente sezione della relazione.'),
(455, 'ro', 'Indicaţi sistemul care s-a defectat şi oferiţi o descriere a circumstanţelor în care s-a produs evenimentul / descrieţi ce s-a întâmplat, dacă acest lucru nu a fost deja raportat într-o secţiune anterioară a acestui raport.'),
(457, 'it', 'Un grave incidente ambientale'),
(457, 'ro', 'UN INCIDENT DE MEDIU MAJOR'),
(458, 'it', 'Nome dell''appaltatore'),
(458, 'ro', 'Numele contractantului'),
(459, 'it', 'Indicare il sistema guasto e descrivere le circostanze dell''evento / descrivere quanto accaduto. Quali sono o possono essere gli effetti negativi significativi sull''ambiente ?'),
(459, 'ro', 'Indicaţi sistemul care s-a defectat şi oferiţi o descriere a circumstanţelor în care s-a produs evenimentul / descrieţi ce s-a întâmplat. Care sunt sau sunt posibile să fie efectele adverse semnificative asupra mediului?'),
(460, 'it', 'Relazione in PDF'),
(460, 'ro', 'Generează PDF'),
(461, 'it', 'Osservazioni'),
(461, 'ro', 'Observaţii'),
(462, 'it', 'Se l''episodio rientra in una delle categorie di cui sopra, l''operatore / proprietario deve procedere alla relativa sezione/i, quindi un solo incidente potrebbe comportare il completamento pià¹ sezioni . L''operatore / proprietario deve presentare la compilato sezioni per l''autorità  competente entro 15 giorni lavorativi dalla manifestazione , con le migliori informazioni disponibili in quel momento. Se l''evento segnalato é un incidente , lo Stato membro avvia un''indagine approfondita ai sensi dell''articolo 26 della direttiva 2013/30/EU'),
(462, 'ro', 'Dacă incidentul se încadrează în una dintre categoriile menţionate mai sus, operatorul/proprietarul va continua cu secţiunea (secţiunile) relevantă (relevante) şi, în consecinţă, un singur incident ar putea duce la completarea mai multor secţiuni. Operatorul/proprietarul va transmite secţiunile completate autorităţii competente în termen de 10 zile lucrătoare de la producerea evenimentului, utilizând cele mai bune informatii disponibile la momentul respectiv. În cazul în care evenimentul raportat este un accident major, statul membru va iniţia o investigaţie minuţioasă în conformitate cu articolul 26 din Directiva 2013/30/UE.'),
(463, 'it', 'Decessi e lesioni gravi sono riportati sotto i requisiti della Direttiva 92/91/EEC'),
(463, 'ro', 'Accidentele mortale sau vătămările grave sunt raportate în conformitate cu cerinţele Directivei 92/91/CEE'),
(465, 'it', 'Si é verificata un''emissione di sostanze idrocarburi?'),
(465, 'ro', 'A existat o scurgere de hidrocarburi?'),
(466, 'it', 'Descrizione delle circostanze , le conseguenze di un evento e le azioni di risposta di emergenza adottate.'),
(466, 'ro', 'Descrierea circumstanţelor, a consecinţelor evenimentului şi a acţiunilor de intervenţie în caz de urgenţă întreprinse.'),
(467, 'it', 'La perdita di controllo dei pozzi che richiede l''azionamento di attrezzature di controllo del pozzo'),
(467, 'ro', 'Pierderea controlului asupra sondei, situaţie care necesită declanşarea echipamentelor de control ale sondei'),
(468, 'it', 'o il fallimento di un pozzo di sbarramento che richiede la sua sostituzione o la riparazione'),
(468, 'ro', 'sau defecţiunea unei bariere la sondă, necesitând înlocuirea sau reparaţia acesteia'),
(469, 'it', 'Ulteriori dettagli'),
(469, 'ro', 'Detalii suplimentare'),
(471, 'it', 'come perduti o non disponibili, con necessità  di un''azione correttiva immediata o non hanno funzionato durante un incidente?'),
(471, 'ro', 'ca fiind pierdute sau indisponibile, care necesită o acţiune de remediere imediată sau s-au defectat în timpul incidentului?'),
(472, 'it', 'Sicurezza e elementi critici ambientali interessato'),
(472, 'ro', 'Elemente critice de siguranţă şi de mediu implicate'),
(473, 'it', 'PROFILO'),
(473, 'ro', 'PROFIL'),
(474, 'it', 'Informazioni  sullo Stato membro e l''autorità che trasmette la relazione'),
(474, 'ro', 'Informaţii referitoare la statul membru şi autoritatea de raportare'),
(475, 'it', 'Stato Membro'),
(475, 'ro', 'Stat membru'),
(476, 'it', 'Periodo di riferimento'),
(476, 'ro', 'Perioada de raportare'),
(477, 'it', 'Anno di calendario'),
(477, 'ro', 'an calendaristic'),
(478, 'it', 'Autorità competente per la relazione'),
(478, 'ro', 'Autoritatea de raportare desemnată'),
(479, 'it', 'Aggiorna gli impianti'),
(479, 'ro', 'Actualizaţi instalaţiile'),
(480, 'it', 'Impianti fissi'),
(480, 'ro', 'Instalaţii fixe'),
(481, 'it', 'elencare  nei dettagli gli impianti in mare per le attività nel settore degli idrocarburi  nello Stato (al 1° gennaio  dell''anno  oggetto  della relazione),  compresi  il tipo (ossia  fisso con personale,  fisso di norma senza  personale,   galleggiante   destinato  alla  produzione,   fisso  non  destinato  alla  produzione),   l''anno  di installazione  e l''ubicazione'),
(481, 'ro', 'Vă rugăm să transmiteţi o listă detaliată a instalaţiilor pentru operaţii petroliere şi gaziere offshore în ţara dumneavoastră (la data de 1 ianuarie a anului pentru care se face raportarea), inclusiv tipul acestora (şi anume instalaţie fixă cu echipaj, instalaţie fixă fără echipaj în mod normal, mijloc plutitor de producţie, instalaţie fixă neproductivă), anul instalaţiei şi amplasamentul.'),
(482, 'it', 'Tabella {evt}'),
(482, 'ro', 'Tabelul {evt}'),
(483, 'it', 'Impianti nella giurisdizione al 1° gennaio dell'' anno oggetto della relazione'),
(483, 'ro', 'Instalaţii în jurisdicţie, la data de 1 ianuarie a perioadei de raportare'),
(484, 'it', 'Nome o ID'),
(484, 'ro', 'Numele sau codul de identificare'),
(485, 'it', 'Tipo d''impianto'),
(485, 'ro', 'Tipul instalaţiei'),
(486, 'it', 'Anno di installazione'),
(486, 'ro', 'Anul instalaţiei'),
(487, 'it', 'Tipo di fluido'),
(487, 'ro', 'Tipul de lichid'),
(488, 'it', 'Numero di letti'),
(488, 'ro', 'Număr de paturi'),
(489, 'it', 'Coordinate'),
(489, 'ro', 'Coordonate'),
(490, 'it', 'ad esempio'),
(490, 'ro', 'şi anume'),
(491, 'it', 'impianto fisso con personale  (FMI)'),
(491, 'ro', 'instalaţie fixă cu echipaj (FMI)'),
(492, 'it', 'impianto (fisso) di norma senza personale  (NUI)'),
(492, 'ro', 'instalaţie (fixă) fără echipaj în mod normal (NUI)'),
(493, 'it', 'impianto galleggiante  destinato alla produzione (FPI)'),
(493, 'ro', 'mijloc plutitor de producţie (FPI)'),
(494, 'it', 'impianto fisso non destinato alla produzione (FNP).'),
(494, 'ro', 'instalaţie fixă neproductivă (FNP)'),
(495, 'it', 'longitudine'),
(495, 'ro', 'longitudine'),
(496, 'it', 'latitudine'),
(496, 'ro', 'Latitudine'),
(497, 'it', 'Sei sicuro di voler eliminare questa voce?');
INSERT INTO `t_message` (`id`, `language`, `translation`) VALUES
(497, 'ro', 'Sunteţi sigur că doriţi ştergerea acestui item?'),
(498, 'it', 'Nessun impianto riportato'),
(498, 'ro', 'nicio instalaţie înregistrată'),
(499, 'it', '(non impostato)'),
(499, 'ro', '(gol)'),
(500, 'it', 'Cambiamenti  rispetto al precedente  anno di riferimento'),
(500, 'ro', 'Modificări faţă de anul de raportare precedent'),
(501, 'it', 'Nuovi impianti  fissi'),
(501, 'ro', 'Instalaţii fixe noi'),
(502, 'it', 'comunicare  gli impianti  fissi nuovi  entrati  in funzione  durante  il periodo  oggetto della relazione'),
(502, 'ro', 'Vă rugăm raportaţi noile instalaţii fixe care au fost puse în funcţiune în timpul perioadei de raportare'),
(503, 'it', 'Impianti fissi nuovi entrati in funzione durante il periodo oggetto di comunicazione'),
(503, 'ro', 'Noile instalaţii fixe care au fost puse în funcţiune în timpul perioadei de raportare'),
(504, 'it', 'Impianti fissi non in funzione'),
(504, 'ro', 'Instalaţii fixe scoase din funcţiune'),
(505, 'it', 'comunicare gli impianti non più in funzione nelle operazioni  del settore degli idrocarburi  in mare durante il periodo oggetto della relazione'),
(505, 'ro', 'Vă rugăm raportaţi instalaţiile fixe care au fost retrase din operaţiunile petroliere şi gaziere offshore în timpul perioadei de raportare'),
(506, 'it', 'Impianti smantellati  durante il periodo oggetto della relazione'),
(506, 'ro', 'Instalaţiile fixe care au fost dezafectate în timpul perioadei de raportare'),
(507, 'it', 'Temporaneo'),
(507, 'ro', 'Temporar'),
(508, 'it', 'Permanente'),
(508, 'ro', 'Permanent'),
(509, 'it', 'Impianti mobili'),
(509, 'ro', 'Instalaţii mobile'),
(510, 'it', 'comunicare  gli impianti mobili in funzione durante il periodo oggetto della relazione (MODU e altri impianti non destinati alla produzione)'),
(510, 'ro', 'Vă rugăm raportaţi instalaţiile mobile care desfăşoară operaţiuni în timpul perioadei de raportare (MODU şi alte instalaţii neproductive)'),
(511, 'it', 'fisso con personale'),
(511, 'ro', 'instalaţie fixă cu echipaj'),
(512, 'it', 'fisso di norma senza personale'),
(512, 'ro', 'instalaţie fixă fără echipaj în mod normal'),
(513, 'it', 'impianto galleggiante  destinato  alla produzione'),
(513, 'ro', 'mijloc plutitor de producţie'),
(514, 'it', 'impianto fisso non destinato  alla produzione'),
(514, 'ro', 'instalaţie fixă neproductivă'),
(515, 'it', 'impianto mobile di trivellazione  in mare'),
(515, 'ro', 'instalaţie mobilă de foraj offshore'),
(516, 'it', 'altro impianto mobile non destinato alla produzione'),
(516, 'ro', 'alte instalaţii mobile neproductive'),
(517, 'it', 'Anno di costruzione'),
(517, 'ro', 'Anul construcţiei'),
(518, 'it', 'Area geografica  delle operazioni '),
(518, 'ro', 'Zona geografică în care se desfăşoară operaţiunile'),
(519, 'it', 'durata'),
(519, 'ro', 'Durată'),
(520, 'it', 'ad es. Mare del Nord meridionale,  Alto Adriatico'),
(520, 'ro', 'de exemplu, zona sudică a Mării Nordului, zona nordică a Mării Adriatice'),
(521, 'it', 'Zona'),
(521, 'ro', 'Zona'),
(522, 'it', 'mesi'),
(522, 'ro', 'luni'),
(523, 'it', 'Informazioni  a fini di normalizzazione dei dati'),
(523, 'ro', 'Informaţii pentru standardizarea datelor'),
(524, 'it', 'Comunicare  il numero  totale  di ore lavorative  reali  in mare e la produzione  totale del periodo oggetto della relazione'),
(524, 'ro', 'Vă rugăm menţionaţi numărul total de ore reale de muncă offshore, precum şi producţia totală în perioada de raportare'),
(525, 'it', 'Numero totale di ore lavorative reali in mare per tutti gli impianti'),
(525, 'ro', 'Numărul total de ore reale de muncă offshore pentru toate instalaţiile'),
(526, 'it', 'Produzione totale, in kTEP'),
(526, 'ro', 'Producţie totală, în kTOE'),
(527, 'it', 'Produzione di petrolio'),
(527, 'ro', 'Producţie de petrol'),
(528, 'it', 'Produzione di gas'),
(528, 'ro', 'Producţie de gaz'),
(529, 'it', 'UNITA'' DI MISURA NON RICONSCIUTA'),
(529, 'ro', 'UNITATE DE MĂSURĂ A ENERGIEI NECUNOSCUTĂ'),
(530, 'it', 'FUNZIONI E QUADRO DI RIFERIMENTO NORMATIVI\r\n'),
(530, 'ro', 'FUNCŢII ŞI CADRU DE REGLEMENTARE'),
(531, 'it', 'Ispezioni'),
(531, 'ro', 'Inspecţii'),
(532, 'it', 'Numero di ispezioni in mare effettuate durante il periodo oggetto della relazione'),
(532, 'ro', 'Numărul de inspecţii offshore'),
(533, 'it', 'Numero di ispezioni  in mare'),
(533, 'ro', 'Numărul de inspecţii offshore'),
(534, 'it', 'Giorni-uomo  sugli impianti (spostamenti non compresi)\r\n'),
(534, 'ro', 'Zile-om petrecute la bordul instalaţiei (nu este inclusă perioada de deplasare)'),
(535, 'it', 'Numero di impianti ispezionati'),
(535, 'ro', 'Numărul de instalaţii verificate'),
(536, 'it', 'Indagini'),
(536, 'ro', 'lnvestigări'),
(537, 'it', 'Numero e tipo di ispezioni effettuate durante il periodo oggetto della relazione'),
(537, 'ro', 'Numărul şi tipul investigărilor efectuate în timpul perioadei de raportare'),
(538, 'it', 'Incidenti gravi'),
(538, 'ro', 'Accidente majore'),
(539, 'it', 'a norma dell''articolo  26 della direttiva 2013/30/UE'),
(539, 'ro', 'în conformitate cu articolul 26 din Directiva 2013/30/UE'),
(540, 'it', 'a norma dell''articolo  22 della direttiva 2013/30/UE'),
(540, 'ro', 'în conformitate cu articolul 22 din Directiva 2013/30/UE'),
(541, 'it', 'Problemi di sicurezza e ambientali'),
(541, 'ro', 'Preocupări în materie de siguranţă şi de mediu'),
(542, 'it', 'Interventi di applicazione  delle norme'),
(542, 'ro', 'Măsuri de asigurare a respectării legii'),
(543, 'it', 'Principali   interventi   di  applicazione   delle  norme  o  condanne   durante  il  periodo   oggetto  della relazione (articolo 18 della direttiva 2013/30/UE)'),
(543, 'ro', 'Principalele măsuri de asigurare a respectării legii sau condamnări care au avut loc în perioada de raportare în conformitate cu articolul 18 din Directiva 2013/30/UE:'),
(544, 'it', 'Descrizione'),
(544, 'ro', 'Conţinut'),
(545, 'it', 'Modifiche significative  del quadro normativa  sulle attività in mare'),
(545, 'ro', 'Modificări majore aduse cadrului de reglementare offshore'),
(546, 'it', 'Descrivere  eventuali  cambiamenti  di  rilievo  del  quadro  normativa  sulle  attività  in  mare  durante  il  periodo oggetto della relazione'),
(546, 'ro', 'Vă rugăm descrieţi orice modificări majore aduse cadrului de reglementare offshore în timpul perioadei de raportare'),
(547, 'it', 'includere ad es. motivo, descrizione,  risultato previsto, riferimenti'),
(547, 'ro', 'includ, de exemplu, conţinutul, descrierea, rezultatul aşteptat, referinţe'),
(548, 'it', 'Motivo'),
(548, 'ro', 'Motivare'),
(549, 'it', 'Descrizione'),
(549, 'ro', 'Descriere'),
(550, 'it', 'Risultato previsto'),
(550, 'ro', 'Rezultat aşteptat'),
(551, 'it', 'Riferimenti'),
(551, 'ro', 'Referinţe'),
(552, 'it', '(nessuno)'),
(552, 'ro', '(niciunul / una)'),
(553, 'it', 'DATI RELATIVI  ALL''INCIDENTE E PRESTAZIONI DELLE OPERAZIONI  IN MARE'),
(553, 'ro', 'DATE REFERITOARE LA INCIDENTE ŞI EXECUTAREA OPERAŢIUNILOR OFFSHORE'),
(554, 'it', 'Essere consapevoli del fatto che questo processo potrebbe richiedere un po ''di tempo, a seconda del numero di eventi registrati e preparati per essere riportati nel periodo di riferimento. Procedere?'),
(554, 'ro', 'Atenţie! Durata acestui proces depinde de numărul de rapoarte ce trebuiesc procesate. Continuaţi?'),
(555, 'it', 'Aggiornare i dati'),
(555, 'ro', 'Actualizaţi datele'),
(556, 'it', 'Dati relativi all''incidente'),
(556, 'ro', 'Date referitoare la incidente'),
(557, 'it', 'Numero di eventi da comunicare  ai sensi dell''allegato  IX'),
(557, 'ro', 'Număr de evenimente raportabile în conformitate cu anexa IX'),
(558, 'it', 'dei quali identificati come incidenti gravi'),
(558, 'ro', 'dintre care, identificate ca accidente majore'),
(559, 'it', 'Categorie ex allegato IX'),
(559, 'ro', 'Anexa IX Categorii'),
(560, 'it', 'Numero di eventi'),
(560, 'ro', 'Număr de evenimente'),
(561, 'it', 'Numero di eventi normalizzati'),
(561, 'ro', 'Număr de evenimente standardizat'),
(562, 'it', 'Categorie di incidenti ex allegato IX'),
(562, 'ro', 'Anexa IX Categorii de incidente'),
(563, 'it', 'numero riportato'),
(563, 'ro', 'număr raportat'),
(564, 'it', 'eventi'),
(564, 'ro', 'evenimente'),
(565, 'it', 'ore lavorate'),
(565, 'ro', 'ore lucrate'),
(566, 'it', 'kTEP'),
(566, 'ro', 'kTOE'),
(567, 'it', 'Emissioni accidentali'),
(567, 'ro', 'Scurgeri neintenţionate'),
(568, 'it', 'Emissioni di petrolio/gas  infiammati - Incendi'),
(568, 'ro', 'Scurgeri de petrol/gaze în stare de ardere - focuri'),
(569, 'it', 'Emissioni di petrolio/gas  infiammati - Esplosioni'),
(569, 'ro', 'Scurgeri de petrol/gaze în stare de ardere - explozii'),
(570, 'it', 'Emissioni di gas non infiammato'),
(570, 'ro', 'Scurgeri de gaze care nu se află în stare de ardere'),
(571, 'it', 'Emissioni di petrolio non infiammato'),
(571, 'ro', 'Scurgeri de petrol fără ardere'),
(572, 'it', 'Emissione  di sostanze pericolose'),
(572, 'ro', 'Scurgeri de substanţe periculoase'),
(573, 'it', 'Perdita di controllo del pozzo'),
(573, 'ro', 'Pierderea controlului asupra sondei'),
(574, 'it', 'Eruzioni'),
(574, 'ro', 'Erupţii'),
(575, 'it', 'Attivazione  di otturatori di sicurezza (BOP)/divertori\r\n'),
(575, 'ro', 'Activarea sistemului BOP / de deviere'),
(576, 'it', 'Guasto di una barriera del pozzo'),
(576, 'ro', 'Defectarea barierei sondei'),
(577, 'it', 'Guasti di SECE'),
(577, 'ro', 'Defectarea elementelor critice de siguranţă şi de mediu'),
(578, 'it', 'Perdita di integrità strutturale'),
(578, 'ro', 'Pierderea integrităţii structurale'),
(580, 'it', 'Perdita di stabilità/galleggiamento'),
(580, 'ro', 'Pierderea stabilităţii / flotabilităţii'),
(581, 'it', 'Perdita di stazionarietà'),
(581, 'ro', 'Pierderea poziţiei'),
(582, 'it', 'Collisione  con una nave'),
(582, 'ro', 'Coliziuni de nave'),
(583, 'it', 'Incidenti di elicottero'),
(583, 'ro', 'Accidente de elicopter'),
(584, 'it', 'Incidenti mortali'),
(584, 'ro', 'Accidente mortale'),
(586, 'it', 'Evacuazioni di personale'),
(586, 'ro', 'Evacuarea personalului'),
(587, 'it', 'Incidenti ambientali'),
(587, 'ro', 'Accidente de mediu'),
(588, 'it', 'Evacuazioni di personale'),
(588, 'ro', 'Evacuarea personalului'),
(589, 'it', 'Solo se in relazione  a un incidente grave'),
(589, 'ro', 'Doar dacă au legătură cu un accident major'),
(590, 'it', 'I valori utilizzati per la normalizzazione'),
(590, 'ro', 'Valori utilizate în standardizarea datelor'),
(592, 'it', 'E'' stato fornito dalla'),
(592, 'ro', 'Conform'),
(593, 'it', 'di questo documento'),
(593, 'ro', 'a acestui document'),
(594, 'it', 'Numero totale di decessi e lesioni'),
(594, 'ro', 'Număr total de accidente mortale şi vătămări grave'),
(595, 'it', 'Numero'),
(595, 'ro', 'Număr'),
(596, 'it', 'Valore normalizzato'),
(596, 'ro', 'Valoare standardizată'),
(597, 'it', 'Numero totale di decessi'),
(597, 'ro', 'Număr total de accidente mortale'),
(598, 'it', 'Numero totale di lesioni'),
(598, 'ro', 'Număr total de vătămări'),
(599, 'it', 'Numero totale di lesioni gravi'),
(599, 'ro', 'Număr total de vătămări grave'),
(601, 'it', 'SECE'),
(601, 'ro', 'Elemente critice de siguranţă şi de mediu'),
(602, 'it', 'Numero in relazione a incidenti gravi'),
(602, 'ro', 'Număr referitor la accidente majore'),
(603, 'it', 'Sistemi di contenimento del processo'),
(603, 'ro', 'Sisteme de izolare procese'),
(604, 'it', 'Sistemi di controllo della combustione'),
(604, 'ro', 'Sisteme de control al aprinderii'),
(605, 'it', 'Sistemi di attenuazione  per il contenimento del processo'),
(605, 'ro', 'Sisteme de descărcare pentru izolarea proceselor'),
(606, 'it', 'Sistemi di protezione'),
(606, 'ro', 'Sisteme de protecţie'),
(607, 'it', 'Ausili alla navigazione'),
(607, 'ro', 'Ajutoare navigare'),
(608, 'it', 'Sistemi di contenimento del processo'),
(608, 'ro', 'Sisteme de izolare procese'),
(609, 'it', 'Cause dirette e profonde di incidenti gravi'),
(609, 'ro', 'Cauze directe şi de bază ale incidentelor majore'),
(610, 'it', 'Cause connesse alle attrezzature'),
(610, 'ro', 'Număr de incidente'),
(611, 'it', 'Errore umano- Errore operativo'),
(611, 'ro', 'Eroare umană - defecţiune operaţională'),
(612, 'it', 'Errore procedurale/organizzativo\r\n'),
(612, 'ro', 'Eroare procedurală / organizaţională'),
(613, 'it', 'Cause meteorologiche'),
(613, 'ro', 'Cauze ce ţin de condiţiile meteorologice'),
(614, 'it', 'Progettazioni errate'),
(614, 'ro', 'Defecţiune legată de proiectare'),
(615, 'it', 'Guasti meccanici di fatica'),
(615, 'ro', 'Dereglare mecanică datorată oboselii'),
(616, 'it', 'Guasti meccanici da usura'),
(616, 'ro', 'Dereglare mecanică datorată uzurii'),
(617, 'it', 'Guasti meccanici da materiale difettoso'),
(617, 'ro', 'Dereglare mecanică datorată materialului defect'),
(618, 'it', 'Guasto meccanico (nave/elicottero)\r\n'),
(618, 'ro', 'Dereglare mecanică (navă / elicopter)'),
(621, 'it', 'Errore umano-Errore operativo'),
(621, 'ro', 'Eroare umană - defecţiune operaţională'),
(622, 'it', 'Errore operativo'),
(622, 'ro', 'Eroare de operare'),
(623, 'it', 'Errore di manutenzione'),
(623, 'ro', 'Eroare de întreţinere'),
(624, 'it', 'Testing error'),
(624, 'ro', 'Eroare de testare'),
(625, 'it', 'Inspection error'),
(625, 'ro', 'Eroare de inspecţie'),
(626, 'it', 'Errore di progettazione'),
(626, 'ro', 'Eroare de proiectare'),
(627, 'it', 'Errore umano-Errore operativo'),
(627, 'ro', 'Eroare procedurală / organizaţională'),
(628, 'it', 'Valutazione/percezione del rischio inadeguata'),
(628, 'ro', 'Evaluare / percepţie necorespunzătoare cu privire la riscuri'),
(629, 'it', 'Istruzioni/procedure inadeguate\r\n'),
(629, 'ro', 'Instrucţiuni / proceduri necorespunzătoare'),
(630, 'it', 'Comunicazione inadeguata'),
(630, 'ro', 'Comunicare necorespunzătoare'),
(631, 'it', 'Competenze  personali inadeguate'),
(631, 'ro', 'Competenţă necorespunzătoare a personalului'),
(632, 'it', 'Supervisione  inadeguata'),
(632, 'ro', 'Supervizare necorespunzătoare'),
(633, 'it', 'Organizzazione della sicurezza inadeguata'),
(633, 'ro', 'Conducere în ceea ce priveşte chestiunile de siguranţă necorespunzătoare'),
(634, 'it', 'Vento superiore alle specifiche di progettazione'),
(634, 'ro', 'Vânt ce depăşeşte limitele prevăzute prin proiectare'),
(635, 'it', 'Moto ondoso superiore alle specifiche di progettazione'),
(635, 'ro', 'Val ce depăşeşte limitele prevăzute prin proiectare'),
(636, 'it', 'Visibilità estremamente ridotta superiore alle specifiche di progettazione'),
(636, 'ro', 'Vizibilitate extrem de redusă, ce depăşeşte limitele prevăzute prin proiectare'),
(637, 'it', 'Presenza di ghiaccio/iceberg'),
(637, 'ro', 'Prezenţa gheţii / gheţari'),
(638, 'it', 'Principali esperienze acquisite in seguito agliincidenti degne di essere condivise'),
(638, 'ro', 'Care sunt cele mai importante lecţii învăţate în urma incidentelor, care merită să fie împărtăşite?'),
(639, 'it', 'incidente'),
(639, 'ro', 'incident'),
(640, 'it', 'incidenti'),
(640, 'ro', 'incidente'),
(641, 'it', 'Lanciare il fulcro degli eventi '),
(641, 'ro', 'Accesaţi secţiunea Evenimente'),
(643, 'it', 'Gestione eventi'),
(643, 'ro', 'Gestiune evenimente'),
(646, 'it', 'nuovo evento'),
(646, 'ro', 'eveniment nou'),
(647, 'it', 'Fulcro degli incidenti'),
(647, 'ro', 'secțiunea incidente'),
(649, 'it', 'Uscita'),
(649, 'ro', 'Ieşire'),
(650, 'it', 'Account'),
(650, 'ro', 'Cont'),
(652, 'it', 'OK'),
(652, 'ro', 'OK'),
(655, 'it', 'creato alle'),
(655, 'ro', 'Înregistrat la'),
(656, 'it', 'modificato alle'),
(656, 'ro', 'modificat la'),
(659, 'it', 'modificato da'),
(659, 'ro', 'modificat de'),
(660, 'it', 'Eventi registrati'),
(660, 'ro', 'Evenimente înregistrate'),
(661, 'it', 'nome incidente'),
(661, 'ro', 'nume incident'),
(662, 'it', 'Data e Ora'),
(662, 'ro', 'Data şi ora'),
(663, 'it', 'Classificazione'),
(663, 'ro', 'Clasificare'),
(664, 'it', 'lettere delimitati da virgole (a - j)'),
(664, 'ro', 'litere separate prin virgulă (a-j)'),
(666, 'it', 'impianto/tipo/campo'),
(666, 'ro', 'inst/tip/câmp'),
(667, 'it', 'Date'),
(667, 'ro', 'Date'),
(668, 'it', 'a tempo debito'),
(668, 'ro', 'în termen'),
(669, 'it', 'in ritardo'),
(669, 'ro', 'în întârziere'),
(670, 'it', 'in ritardo'),
(670, 'ro', 'timp depăşit'),
(671, 'it', 'presentato'),
(671, 'ro', 'transmis'),
(672, 'it', 'presentato in ritardo'),
(672, 'ro', 'transmis cu întârziere'),
(673, 'it', 'presentato a tempo debito'),
(673, 'ro', 'transmis în termen'),
(674, 'it', 'Registra un nuovo evento'),
(674, 'ro', 'Înregistrare eveniment nou'),
(675, 'it', 'Ripristina Griglia'),
(675, 'ro', 'Resetare tabel'),
(676, 'it', 'Elenco degli eventi'),
(676, 'ro', 'Listă evenimente'),
(677, 'it', 'Eventi eliminati. Fare clic per gestirli.'),
(677, 'ro', 'Evenimente înlăturate. Apasă pentru a gestiona.'),
(678, 'it', 'Attivo'),
(678, 'ro', 'Activ'),
(679, 'it', 'Inattivo'),
(679, 'ro', 'Inactiv'),
(680, 'it', 'Disattivare il blocco popup nel browser per garantire il corretto download.'),
(680, 'ro', 'Dezactivați orice program de blocare a ferestrelor de tip popup în browserul dumneavoastră pentru a asigura descărcarea corectă a fișierului solicitat.'),
(681, 'it', 'Ok per procedere?'),
(681, 'ro', 'Continuaţi?'),
(682, 'it', 'Generazione del file di esportazione. Attendere prego...'),
(682, 'ro', 'Generare fişier export. Vă rugăm aşteptaţi...'),
(683, 'it', 'Richiesta inviata! Si può tranquillamente chiudere la finestra di dialogo dopo aver salvato il file scaricato.'),
(683, 'ro', 'Solicitare transmisă! Puteți închide această fereastră după salvarea fișierului descărcat.'),
(684, 'it', 'Esportare'),
(684, 'ro', 'Export'),
(685, 'it', 'Esportare tutti i dati'),
(685, 'ro', 'Exportă toate datele'),
(686, 'it', 'Export della Griglia'),
(686, 'ro', 'Export tabel'),
(687, 'it', 'Yii2 Grid Export (PDF)'),
(687, 'ro', 'Yii2 Grid Export (PDF)'),
(688, 'it', 'Generato'),
(688, 'ro', 'Creat'),
(689, 'it', '© Krajee Yii2 Extensions'),
(689, 'ro', '© Krajee Yii2 Extensions'),
(690, 'it', 'HTML'),
(690, 'ro', 'HTML'),
(691, 'it', 'Il file di esportazione HTML verrà generato per il download.'),
(691, 'ro', 'Fișierul de export de tip HTML va fi generat în vederea descărcării.'),
(692, 'it', 'Linguaggio a marcatori per ipertesti'),
(692, 'ro', 'Hyper Text Markup Language'),
(693, 'it', 'CSV'),
(693, 'ro', 'CSV'),
(694, 'it', 'Il file di esportazione CVS verrà generato per il download.'),
(694, 'ro', 'Fișierul de export de tip CSV va fi generat în vederea descărcării.'),
(695, 'it', 'Valori separati da virgola'),
(695, 'ro', 'valori separate prin virgulă'),
(696, 'it', 'Il file di esportazione TEXT verrà generato per il download.'),
(696, 'ro', 'Fișierul de export de tip TEXT va fi generat în vederea descărcării.'),
(697, 'it', 'Testo delimitato da tab'),
(697, 'ro', 'text separat prin tab'),
(698, 'it', 'Excel'),
(698, 'ro', 'Excel'),
(699, 'it', 'Il file di esportazione EXCEL verrà generato per il download.'),
(699, 'ro', 'Fișierul de export de tip EXCEL va fi generat în vederea descărcării.'),
(700, 'it', 'Microsoft Excel 95+'),
(700, 'ro', 'Microsoft Excel 95+'),
(701, 'it', 'Esporta foglio di lavoro'),
(701, 'ro', 'Exportă foaia de calcul'),
(702, 'it', 'PDF'),
(702, 'ro', 'PDF'),
(703, 'it', 'Il file di esportazione PDF verrà generato per il download.'),
(703, 'ro', 'Fișierul de export de tip PDF va fi generat în vederea descărcării.'),
(704, 'it', 'Portable Document Format'),
(704, 'ro', 'Portable Document Format'),
(705, 'it', 'PDF generato da kartik-v/yii2-grid estensione'),
(705, 'ro', 'Export PDF generat cu extensia kartik-v/yii2-grid'),
(706, 'it', 'krajee, grid, export, yii2-grid, pdf'),
(706, 'ro', 'krajee, grid, export, yii2-grid, pdf '),
(707, 'it', 'JSON'),
(707, 'ro', 'JSON'),
(708, 'it', 'Il file di esportazione JSON verrà generato per il download.'),
(708, 'ro', 'Fișierul de export de tip JSON va fi generat în vederea descărcării.'),
(709, 'it', 'JavaScript Object Notation'),
(709, 'ro', 'JavaScript Object Notation'),
(710, 'it', 'Ispezionare'),
(710, 'ro', 'Audit'),
(711, 'it', 'Dichiarazione Evento'),
(711, 'ro', 'Declarare Eveniment'),
(712, 'it', '{evt} Ispezionare'),
(712, 'ro', 'Audit {evt}'),
(713, 'it', 'Perdita di controllo del pozzo/barriera'),
(713, 'ro', 'Pierderea controlului asupra sondei / a unei bariere de protecţie'),
(714, 'it', 'Guasto di un SECE'),
(714, 'ro', 'Pierdere SECE'),
(715, 'it', 'Collisione con una nave'),
(715, 'ro', 'Coliziune cu o navă'),
(716, 'it', 'Lesioni gravi a cinque o più persone'),
(716, 'ro', 'Vătămarea gravă a cinci sau mai multe persoane'),
(717, 'it', 'Incidente ambientale grave'),
(717, 'ro', 'Incident de mediu major'),
(718, 'it', 'Visualizza relazione nel CRF'),
(718, 'ro', 'Afişează raport în FCR'),
(719, 'it', '{evt, plural, =1{# mese} other{# mesi}} '),
(719, 'ro', '{evt, plural, =1{# lună} other{# luni}} '),
(720, 'it', 'e'),
(720, 'ro', 'şi'),
(721, 'it', '{evt, plural, =1{# giorno} other{# giorni}} '),
(721, 'ro', '{evt, plural, =1{# zi} other{# zile}}'),
(722, 'it', 'Tempo di presentazione'),
(722, 'ro', 'Timp rămas până la expirarea termenului limită '),
(723, 'it', 'Tempo dalla presentazione'),
(723, 'ro', 'Timp de la momentul raportării'),
(725, 'it', 'Date di presentazione'),
(725, 'ro', 'Date raportare'),
(726, 'it', 'prima della scadenza'),
(726, 'ro', 'înainte de expirarea termenului limită'),
(727, 'it', 'Questa relazione è stata presentata prima della scadenza.'),
(727, 'ro', 'Acest raport a fost transmis înainte de expirarea termenului limită.'),
(728, 'it', 'Stato CA'),
(728, 'ro', 'Status AC'),
(729, 'it', 'ACCETTATO E VALUTATA DALLA CA.'),
(729, 'ro', 'ACCEPTAT ŞI EVALUAT DE CĂTRE AC.'),
(730, 'it', 'Visualizzare la sezione della relazione/bozze'),
(730, 'ro', 'Afişează secţiunea raport/ciorne.'),
(731, 'it', 'Visualizzazione dei dettagli'),
(731, 'ro', 'Afişează detalii'),
(732, 'it', 'Sei sicuro di voler eliminare {data} ?'),
(732, 'ro', 'Sunteţi sigur că vreţi să ştergeti {data}?'),
(733, 'it', 'L''incidente deve essere comunicato prima di avere queste informazioni a disposizione.'),
(733, 'ro', 'Pentru a avea această informaţie disponibilă evenimentul trebuie în prealabil raportat.'),
(736, 'it', 'campo'),
(736, 'ro', 'câmp'),
(737, 'it', 'relazioni'),
(737, 'ro', 'rapoarte'),
(738, 'it', 'Communicare'),
(738, 'ro', 'Raportare'),
(739, 'it', 'Modificare'),
(739, 'ro', 'Modifică '),
(740, 'it', 'Rimuovere'),
(740, 'ro', 'Elimină'),
(741, 'it', 'creato da'),
(741, 'ro', 'Înregistrat de'),
(742, 'it', 'Stato'),
(742, 'ro', 'Status'),
(743, 'it', 'Date di presentazione'),
(743, 'ro', 'Data raportării'),
(744, 'it', 'Scadenza'),
(744, 'ro', 'Termen limită'),
(745, 'it', 'Periodo di scadenza'),
(745, 'ro', 'Interval termen limită'),
(746, 'it', 'Relazione presentata'),
(746, 'ro', 'Raport transmis'),
(747, 'it', 'Tempistica'),
(747, 'ro', 'Timp'),
(748, 'it', '{evt, plural, =1{# anno} other{# anni} } '),
(748, 'ro', '{evt, plural, =1{# an} other{# ani} }'),
(749, 'it', '{evt, plural, =1{# ora} other{# ore}} '),
(749, 'ro', '{evt, plural, =1{# oră} other{# ore}}'),
(750, 'it', '{evt, plural, =1{# minuto} other{# minuti}} '),
(750, 'ro', '{evt, plural, =1{# minut} other{# minute}}'),
(752, 'it', 'Modifica i recapiti'),
(752, 'ro', 'Modifică datele de contact'),
(753, 'it', 'SyRIO Admin '),
(753, 'ro', 'Administrator SyRIO'),
(754, 'it', 'Amministratore di sistema'),
(754, 'ro', 'Administrator sistem'),
(755, 'it', 'pieno controllo su SyRIO'),
(755, 'ro', 'control total asupra SyRIO'),
(756, 'it', 'Amministratore ({evt})'),
(756, 'ro', 'Administrator ({evt})'),
(757, 'it', 'i diritti di amministratore sulla sezione Competente Autorità di SyRIO'),
(757, 'ro', 'drepturi de administrator asupra secţiunii Autoritatea Competentă a SyRIO'),
(758, 'it', 'Perito degli incidenti'),
(758, 'ro', 'Evaluator incidente'),
(759, 'it', 'consente la valutazione dei rapporti sugli incidenti trasmessi dagli Operatori/Proprietari'),
(759, 'ro', 'permite evaluarea rapoartelor trimise de către Operatori / Proprietari'),
(760, 'it', 'Utenti registrati'),
(760, 'ro', 'Înregistrează utilizator'),
(761, 'it', 'consente l''accesso ai dati nella tua Autorità Competente'),
(761, 'ro', 'permite accesul la informaţiile referitoare la Autoritatea Competentă'),
(762, 'it', 'diritti di amministratore sui tuoi dati di installazione'),
(762, 'ro', 'drepturi de administrator asupra datelor legate de instalaţia dumneavoastră'),
(763, 'it', 'Relatore ({evt})'),
(763, 'ro', 'Raportor ({evt})'),
(764, 'it', 'consente la segnalazione degli incidenti e la gestione del tuo impianto'),
(764, 'ro', 'permite raportarea incidentelor şi gestiunea rapoartelor în cazul instalaţiei dumneavoastră'),
(765, 'it', 'Utenti registrati ({evt}) '),
(765, 'ro', 'Înregistrează utilizator ({evt})'),
(766, 'it', 'consente l''accesso agli incidenti comunicati dal vostro impianto, nonché alcuni diritti della relazione'),
(766, 'ro', 'permite accesul la incidentele raportate de către instalaţia dumneavoastră, precum şi un set restrâns de drepturi de raportare'),
(767, 'it', 'diritti di amministratore sulla vostra Organizzazione e sui dati delle installazioni'),
(767, 'ro', 'drepturi de administrator asupra datelor legate de organizaţia si instalaţiile dumneavoastră'),
(768, 'it', 'consente la relazione degli incidenti e la gestione per la tua Organizzazione ed impianti'),
(768, 'ro', 'permite raportarea incidentelor şi gestiunea rapoartelor în cazul organizaţiei şi a instalaţiilor dumneavoastră'),
(769, 'it', 'consente l'' accesso agli incidenti comunicati dei vostri impianti, nonché alcuni diritti di segnalazione'),
(769, 'ro', 'permite accesul la incidentele raportate de către toate instalaţiile dumneavoastră, precum şi un set restrâns de drepturi de raportare'),
(773, 'it', 'Membro da:'),
(773, 'ro', 'membru din:'),
(779, 'it', 'Si prega di log-in per iniziare a utilizzare l''applicazione'),
(779, 'ro', 'Vă rugăm să vă autentificaţi pentru a utiliza aplicaţia'),
(780, 'it', 'Si prega di compilare i seguenti campi per effettuare il login'),
(780, 'ro', 'Vă rugăm să completaţi câmpurile următoare pentru a vă autentifica'),
(781, 'it', 'Ha dimenticato la password?'),
(781, 'ro', 'Aţi uitat parola?'),
(782, 'it', 'RESPINTI DELLA CA.  CONTROLLARE IL MOTIVO (nella sezione bozze) E INOLTRARE NUOVAMENTE.'),
(782, 'ro', 'RESPINS DE CĂTRE AC. VERIFICAŢI MOTIVUL (în secţiunea ciorne) APOI RETRANSMITEŢI RAPORTUL.'),
(783, 'it', 'Incidenti che coinvolgono elicotteri'),
(783, 'ro', 'Accidente implicând elicoptere'),
(784, 'it', 'Vista'),
(784, 'ro', 'Afişare'),
(785, 'it', 'Eliminare'),
(785, 'ro', 'Şterge'),
(786, 'it', 'Questo rimuoverà la lingua ''{evt} '' dal SyRIO.'),
(786, 'ro', 'Elimină limba ''{evt}'' din SyRIO.'),
(787, 'it', 'L''operazione non può essere annullata!'),
(787, 'ro', 'NU se poate reveni asupra operaţiei!'),
(788, 'it', 'Gestione Hub'),
(788, 'ro', 'Centru Gestiune'),
(789, 'it', 'Gestione utenti'),
(789, 'ro', 'Gestiune Utilizatori'),
(790, 'it', 'Registrazione utente'),
(790, 'ro', 'Înregistrare utilizator'),
(791, 'it', 'Gestione delle Organizzazioni'),
(791, 'ro', 'Gestiune Organizaţii'),
(792, 'it', 'Elenco dei messaggi tradotti per {evt}'),
(792, 'ro', 'Lista mesajelor traduse pentru {evt}'),
(793, 'it', 'Esportazione PDF generato da SyRIO'),
(793, 'ro', 'Export PDF generat de SyRIO'),
(794, 'it', 'Tradurre'),
(794, 'ro', 'Efectuează traducere'),
(796, 'it', 'Statistiche'),
(796, 'ro', 'Statistici'),
(797, 'it', 'Relazioni di incidente'),
(797, 'ro', 'Rapoarte de incident'),
(798, 'it', 'Visualizza l''elenco degli incidenti segnalati come previsto dalla Direttiva 2013/30/EU.'),
(798, 'ro', 'Afisează lista incidentelor raportate în conformitate cu Directiva 2013/30/UE'),
(799, 'it', 'Rapporto Annuale'),
(799, 'ro', 'Raport anual'),
(800, 'it', 'Strumenti per la stesura della relazione annuale della CA da presentare alla Commissione , come previsto dalla Direttiva 2013/30 / UE .'),
(800, 'ro', 'Uneltele de pregătire a raportului anual al AC pentru a fi transmis către Comisie, în conformitate cu cerinţele Directivei 2013/30/UE.'),
(801, 'it', 'La valutazione degli incidenti presentati, caso per caso , in termini della sezione 4 del Formato Comune di Pubblicazione.'),
(801, 'ro', 'Evaluarea individuală a incidentelor raportate, în raport cu Secţiunea 4 a formatului comun de publicare (CPF)'),
(802, 'it', 'Strumenti per la redazione della relazione annuale della CA. Esportare la relazione annuale come  XML.'),
(802, 'ro', 'Uneltele de editare a raportului annual al AC. Exportă raport anual ca XML.'),
(803, 'it', 'Si prega di fare in modo che tutti gli incidenti denunciati sono stati valutati in termini della Sezione 4 del CPF (vale a dire la preparazione del Rapporto Annuale è fatto e aggiornato) .'),
(803, 'ro', 'Asiguraţi-vă că toate evenimentele au fost evaluate în raport cu Secţiunea 4 a CPF (i.e. Pregătirea raportului anual este actualizată si finalizată)'),
(804, 'it', 'La tua sessione è scaduta!'),
(804, 'ro', 'Sesiunea a expirat!'),
(805, 'it', 'È necessario effettuare il login per continuare.'),
(805, 'ro', 'Pentru a continua, trebuie să vă autentificaţi din nou.'),
(806, 'it', 'Incidenti registrati in {evt}:'),
(806, 'ro', 'Incidente înregistrate în {evt}:'),
(807, 'it', 'dei quali classificati come incidenti gravi :'),
(807, 'ro', 'dintre care clasificate ca Accident Major:'),
(808, 'it', 'relazioni di incidente'),
(808, 'ro', 'rapoartele de incident'),
(809, 'it', 'preparazione della valutazione annuale'),
(809, 'ro', 'pregătirea raportului anual'),
(810, 'it', 'bozza della relazione annuale'),
(810, 'ro', 'editarea raportului anual'),
(818, 'it', '{section} Valutato da'),
(818, 'ro', '{section} evaluată de'),
(819, 'it', '{section} Valutato alle'),
(819, 'ro', '{section} evaluată la'),
(821, 'it', 'Giustificazione di rifiuto'),
(821, 'ro', 'Justificarea respingerii'),
(822, 'it', 'Incidenti comunicati'),
(822, 'ro', 'Incidente raportate'),
(823, 'it', 'MA / CPF'),
(823, 'ro', 'AM / CPF'),
(824, 'it', 'Incidente Grave'),
(824, 'ro', 'Accident Major'),
(825, 'it', 'Sezione 4 del CPF'),
(825, 'ro', 'Secţiunea 4 a CPF'),
(826, 'it', 'Valutazione MA'),
(826, 'ro', 'Evaluare AM'),
(827, 'it', 'Valutazione CPF'),
(827, 'ro', 'Evaluare CPF'),
(828, 'it', 'Operatore'),
(828, 'ro', 'Operator'),
(829, 'it', 'nome o tipo'),
(829, 'ro', 'nume sau tip'),
(830, 'it', 'Ripristina filtri'),
(830, 'ro', 'Resetare filtre'),
(832, 'it', 'Valutazione come Incidente Grave'),
(832, 'ro', 'Evaluare ca Accident Major'),
(833, 'it', 'Relazione {evt}.'),
(833, 'ro', 'Raport {evt}.'),
(834, 'it', 'Evento categorizzato come {evt}'),
(834, 'ro', 'Eveniment clasificat ca {evt}'),
(835, 'it', 'La relazione non è inclusa nella lista per essere valutata in termini della Sezione 4 del CPF.'),
(835, 'ro', 'Acest raport nu este inclus în lista pentru evaluare în raport cu Secţiunea 4 a CPF. '),
(836, 'it', '<br/>Si prega di <strong>Aggiornare </strong> la lista della <em>preparazione della Valutazione Annuale </em>'),
(836, 'ro', '<br/><strong>Actualizaţi</strong>  lista în <em>Pregătirea raportului anual</em>'),
(837, 'it', 'Valutazione in termini della Sezione a del CPF '),
(837, 'ro', 'Evaluare din punct de vedere al Secţiunii 4 a CPF'),
(838, 'it', 'minore'),
(838, 'ro', 'minor'),
(839, 'it', 'not incluso nella lista delle valutazioni'),
(839, 'ro', 'nu este inclus(ă) în lista de evaluare'),
(840, 'it', 'Date dell''oggetto della relazione '),
(840, 'ro', 'Date de raportare'),
(841, 'it', 'La relazione {evt} in termini di CPF Sezione 4.'),
(841, 'ro', 'Raportul {evt} în termenii Secţiunii 4 a CPF.'),
(842, 'it', 'grave'),
(842, 'ro', 'major'),
(843, 'it', 'valutato'),
(843, 'ro', 'evaluat'),
(844, 'it', 'Questa relazione è respinta. Non c''è bisogno di essere valutata per CPF.'),
(844, 'ro', 'Acest raport este respins. Evaluarea în raport cu CPF nu este necesară.'),
(845, 'it', 'rifiutato'),
(845, 'ro', 'respins'),
(846, 'it', 'non c''è bisogno di valutare. relazione respinta'),
(846, 'ro', 'evaluarea nu este necesară. raport respins'),
(847, 'it', 'Relazione accettata'),
(847, 'ro', 'Raport acceptat'),
(848, 'it', 'Vero se la Sezione {evt } della relazione è classificata come grave'),
(848, 'ro', 'Adevărat dacă Secţiunea {evt} a raportului este clasificată ca accident major'),
(849, 'it', 'Giustificazione se la Sezione {evt } è classificata come grave'),
(849, 'ro', 'Justificarea în cazul în care Secţiunea {evt} a raportului este clasificată ca accident major.'),
(850, 'it', 'Relazione presentata alle'),
(850, 'ro', 'Raport re-transmis la'),
(851, 'it', 'Incidenti minori'),
(851, 'ro', 'Incident Minor'),
(852, 'it', 'Direttiva 2013/30/EU'),
(852, 'ro', 'Directiva 2013/30/UE'),
(853, 'it', 'Regolamento (EU) No 1112/2014 del 13 Ottobre 2014'),
(853, 'ro', 'Regulamentul (UE) Nr. 1112/2014 din 13 octombrie 2014'),
(854, 'it', 'Quello che segue è l''elenco di tutti gli eventi off-shore nella vostra giurisdizione, dichiarato dall''Operatore/Proprietari in base a {evt1} e {evt2}.'),
(854, 'ro', 'Mai jos găsiţi lista evenimentelor offshore din jurisdicţia dumneavoastră, raportate de către Operatori/Proprietari în conformitate cu {evt1} si {evt2}.'),
(855, 'it', 'valutazione'),
(855, 'ro', 'Evaluare'),
(856, 'it', 'About'),
(856, 'ro', 'Despre'),
(857, 'it', 'Contatti'),
(857, 'ro', 'Contact'),
(859, 'it', 'Rivalutare'),
(859, 'ro', 'Re-evaluează'),
(860, 'it', '<i class="fa fa-file-pdf-o"></i> PDF'),
(860, 'ro', '<i class="fa fa-file-pdf-o"></i> PDF'),
(861, 'it', 'Entrata in funzione di un dispositivo antieruzione o di un divertore per controllare il flusso di fluidi del pozzo'),
(861, 'ro', 'intrarea în funcţiune a sistemului de prevenire sau deviere a erupţiei pentru a controla fluxul de fluide din sondă;'),
(864, 'it', 'Pagina intenzionalmente vuota'),
(864, 'ro', 'Pagină lasată goală intenţionat'),
(865, 'it', 'da'),
(865, 'ro', 'de'),
(868, 'it', 'Incidenti mortali'),
(868, 'ro', 'Accidente mortale'),
(869, 'it', 'Emissione Accidentale'),
(869, 'ro', 'Scurgeri neintenţionate'),
(870, 'it', 'Perdita di integrità strutturale'),
(870, 'ro', 'Pierderea integrităţii structurale'),
(871, 'it', 'Evacuazioni di personale'),
(871, 'ro', 'Evacuarea personalului '),
(873, 'it', 'entro {evt} giorni lavorativi dall''evento'),
(873, 'ro', 'în termen de {evt} zile lucrătoare de la producerea evenimentului'),
(874, 'it', 'Modulo di ventilazione'),
(874, 'ro', 'Ventilaţie modul'),
(875, 'it', 'Si è verificato un incendio non dovuto a idrocarburi (per es. elettrico) con significativamente in grado di provocare un incidente grave?'),
(875, 'ro', 'A existat un foc declanşat din altă cauză decât scurgerea unei hidrocarburi (de exemplu, electrică) cu un potenţial semnificativ pentru declanşarea unui accident major?'),
(877, 'it', 'Descrizione delle conseguenze'),
(877, 'ro', 'Descrierea consecinţelor'),
(878, 'it', 'Imbarcazioni in rotta di collisione e collisioni effettive di navi con un impianto in mare'),
(878, 'ro', 'Navele aflate pe o traiectorie de coliziune şi coliziunile efective ale navelor cu o instalaţie offshore'),
(882, 'it', 'Se noto'),
(882, 'ro', 'dacă este cunoscut'),
(885, 'it', 'Modalità operativa nella zona al momento dell’emissione'),
(885, 'ro', 'Modul operaţional în zonă, la momentul deversării'),
(886, 'it', 'Combustione era:'),
(886, 'ro', 'Aprinderea s-a produs:'),
(890, 'it', '<u>se</u> conosciuto'),
(890, 'ro', '<u>dacă</u> este cunoscut(ă)'),
(895, 'it', 'Sistemi di contenimento del processo'),
(895, 'ro', 'Sisteme de siguranţă pentru izolarea proceselor'),
(899, 'it', 'Relazione del verificatore indipendente: (n. della relazione/data/verificatore)'),
(899, 'ro', 'Raportul verificatorului independent: detalii (raport nr. /data / verificator)'),
(902, 'it', 'specificare'),
(902, 'ro', 'specificaţi'),
(903, 'it', 'C.2.2. Descrizione delle conseguenze'),
(903, 'ro', 'C.2.2. Descrierea consecinţelor'),
(909, 'it', 'Guasto del sistema e descrizione degli eventi'),
(909, 'ro', 'Sistemul defect și descrierea evenimentului'),
(915, 'it', 'In caso affermativo, compilare le seguenti sezioni.'),
(915, 'ro', 'Dacă da, se completează secţiunile următoare'),
(916, 'it', 'Barrare la casella corrispondente'),
(916, 'ro', 'Bifaţi căsuţa relevantă'),
(917, 'it', 'livello di H'),
(917, 'ro', 'Nivelul de H'),
(920, 'it', 'Non preso'),
(920, 'ro', 'nu a fost luată'),
(921, 'it', 'Nelle stazioni'),
(921, 'ro', 'la staţii'),
(922, 'it', 'Nelle scialuppe di salvataggio'),
(922, 'ro', 'la bărcile de salvare'),
(924, 'it', 'quantità'),
(924, 'ro', 'cantitate'),
(925, 'it', 'In caso affermativo, illustrare gli impatti ambientali già osservati o suscettibili di derivare dall’incidente'),
(925, 'ro', 'Dacă da, precizaţi impactul asupra mediului observat deja sau care este susceptibil de a rezulta în urma incidentului.'),
(929, 'it', 'Completamento della testa di pozzo'),
(929, 'ro', 'Finalizare capăt sondă'),
(930, 'it', 'Profondità dell\\''acqua '),
(930, 'ro', 'Adâncimea apei'),
(931, 'it', 'Tipo di attività'),
(931, 'ro', 'Tip de activitate'),
(935, 'it', 'Origine'),
(935, 'ro', 'Origine'),
(936, 'it', 'Sistemi di rilevamento'),
(936, 'ro', 'Sisteme de control al aprinderii'),
(937, 'it', 'Sistemi di contenimento del processo'),
(937, 'ro', 'Sisteme de izolare procese'),
(962, 'it', ' gas naturale non infiammato o gas evaporato associato se la massa emessa >= 1 kg'),
(962, 'ro', 'gaze naturale care nu se află în stare de ardere sau de gaze asociate evaporate, dacă masa degajată este >= 1 kg;'),
(963, 'it', 'idrocarburo liquido di petrolio non infiammato se la massa emessa > 60 kg'),
(963, 'ro', 'hidrocarburi lichide din petrol care nu se află în stare de ardere, dacă masa degajată este >= 60 kg'),
(967, 'it', 'se'),
(967, 'ro', 'Dacă'),
(975, 'it', 'm'),
(975, 'ro', 'm'),
(977, 'it', 'CO'),
(977, 'ro', 'CO'),
(983, 'it', 'Relazione di incidente'),
(983, 'ro', 'raport de incident'),
(984, 'it', 'Valutazione del {modelClass} '),
(984, 'ro', 'Evaluare {modelClass}'),
(985, 'it', 'Relazione degli incidenti rilevanti'),
(985, 'ro', 'Evaluare accident major'),
(986, 'it', 'Si prega di rivedere la sezione {section}.'),
(986, 'ro', 'Revedeți Secțiunea {section}.'),
(999, 'it', 'Salvare la bozza'),
(999, 'ro', 'Salvaţi ca ciornă'),
(1000, 'it', 'Finalizzare'),
(1000, 'ro', 'Finalizaţi'),
(1001, 'it', 'Bozza salvata per {evt}!'),
(1001, 'ro', 'Ciornă salvată pentru {evt}!'),
(1002, 'it', 'Contiua la valutazione '),
(1002, 'ro', 'Continuaţi evaluarea'),
(1006, 'it', 'Evento valutato con successo!'),
(1006, 'ro', 'Eveniment evaluat cu success!'),
(1007, 'it', 'Ora si può procedere con la valutazione del caso in termini della Sezione 4 della Relazione Annuale.'),
(1007, 'ro', 'Puteţi continua cu evaluarea evenimentului în raport cu Secţiunea 4 a CPF.'),
(1008, 'it', 'Per questo andare alla Pagina Iniziale > preparazione della Relazione annuale quindi fare click su Aggiorna.'),
(1008, 'ro', 'Pentru aceasta accesaţi Acasă > Pregătirea raportului anual, apoi apăsaţi Actualizare'),
(1009, 'it', 'Preparazione Incidenti CPF'),
(1009, 'ro', 'Pregătirea incidentelor în vederea FCP'),
(1010, 'it', 'Come'),
(1010, 'ro', 'Indicaţii'),
(1011, 'it', 'Ricarica'),
(1011, 'ro', 'Actualizaţi '),
(1012, 'it', 'Anno'),
(1012, 'ro', 'An'),
(1014, 'it', 'Relazione ID'),
(1014, 'ro', 'Raport'),
(1141, 'it', 'Stato Valutazione '),
(1141, 'ro', 'Status evaluare'),
(1143, 'it', 'Ultimo Aggiornamento Alle'),
(1143, 'ro', 'Ultima actualizare efectuată la'),
(1144, 'it', 'Ultimo Aggiornamento Da'),
(1144, 'ro', 'Ultima actualizare efectuată de'),
(1155, 'it', 'Emissione Accidentale - Totale - {evt}'),
(1155, 'ro', 'Scurgeri neintenţionate - Total - {evt}'),
(1156, 'it', 'Emissioni di petrolio/gas infiammati — Incendi - {evt} '),
(1156, 'ro', 'Scurgeri de petrol/gaze în stare de ardere - focuri - {evt}'),
(1157, 'it', 'Emissioni di petrolio/gas infiammati - Esplosioni - {evt} '),
(1157, 'ro', 'Scurgeri de petrol/gaze în stare de ardere - explozii - {evt}'),
(1158, 'it', 'Emissioni di gas non infiammato - {evt} '),
(1158, 'ro', 'Scurgeri de gaze care nu se află în stare de ardere - {evt}'),
(1159, 'it', 'Emissioni di petrolio non infiammato - {evt} '),
(1159, 'ro', 'Scurgeri de petrol fără ardere - {evt}'),
(1160, 'it', 'Emissione di sostanze pericolose - {evt}'),
(1160, 'ro', 'Scurgeri de substanţe periculoase - {evt}'),
(1161, 'it', 'Perdita di controllo del pozzo - Totale - {evt} '),
(1161, 'ro', 'Pierderea controlului asupra sondei - Total - {evt}'),
(1162, 'it', 'Eruzioni - {evt} '),
(1162, 'ro', 'Erupţii '),
(1163, 'it', 'Eruzioni / attivazione divertori - {evt}'),
(1163, 'ro', 'Activarea sistemului BOP / de deviere - {evt}'),
(1164, 'it', 'Guasto della barriera di un pozzo - {evt}'),
(1164, 'ro', 'Defectarea barierei sondei - {evt}'),
(1165, 'it', 'Guasti di SECE - {evt} '),
(1165, 'ro', 'Defectarea elementelor critice de siguranţă şi de mediu - {evt}'),
(1166, 'it', 'Perdita di integrità strutturale - totale - {evt} '),
(1166, 'ro', 'Pierderea integrităţii structurale - Total - {evt}'),
(1167, 'it', 'Perdita di integrità strutturale - {evt} '),
(1167, 'ro', 'Pierderea integrităţii structurale - {evt}'),
(1168, 'it', 'Perdita di stabilità/galleggiamento - {evt}'),
(1168, 'ro', 'Pierderea stabilităţii / flotabilităţii - {evt}'),
(1169, 'it', 'Perdita di stazionarietà - {evt} '),
(1169, 'ro', 'Pierderea poziţiei - {evt}'),
(1170, 'it', 'Collisione con una nave - totale - {evt} '),
(1170, 'ro', 'Coliziuni de nave - Total - {evt}'),
(1171, 'it', 'Incidenti che coinvolgono elicotteri - {evt}'),
(1171, 'ro', 'Accidente implicând elicoptere - {evt}'),
(1172, 'it', 'Incidenti mortali - {evt}'),
(1172, 'ro', 'Accidente mortale - {evt}'),
(1173, 'it', 'Lesioni gravi di 5 o più persone nello stesso incidente - {evt}'),
(1173, 'ro', 'Vătămare gravă a cinci sau mai multe persoane afectate de acelaşi accident - {evt}'),
(1174, 'it', 'Evacuazioni di personale - {evt}'),
(1174, 'ro', 'Evacuarea personalului - {evt}'),
(1175, 'it', 'Incidenti ambientali - {evt}'),
(1175, 'ro', 'Accidente de mediu - {evt}'),
(1176, 'it', 'Numero totale di decessi e lesioni - {evt}'),
(1176, 'ro', 'Număr total de accidente mortale şi vătămări grave - {evt}'),
(1177, 'it', 'Numero totale di decessi - {evt}'),
(1177, 'ro', 'Număr de accidente mortale - Total - {evt}'),
(1178, 'it', 'Numero totale di lesioni - {evt}'),
(1178, 'ro', 'Număr total de vătămări - {evt}'),
(1179, 'it', 'Numero totale di lesioni gravi - {evt}'),
(1179, 'ro', 'Număr total de vătămări grave - {evt}'),
(1180, 'it', 'Sistemi di integrità strutturale - {evt}'),
(1180, 'ro', 'Sisteme de integritate structural - {evt}'),
(1181, 'it', 'Sistemi di contenimento del processo - {evt}'),
(1181, 'ro', 'Sisteme de izolare procese'),
(1182, 'it', 'Sistemi di controllo della combustione - {evt}'),
(1182, 'ro', 'Sisteme de control al aprinderii - {evt}'),
(1183, 'it', 'Sistemi di rilevamento - {evt}'),
(1183, 'ro', 'Sisteme de detectare - {evt}'),
(1184, 'it', 'Sistemi di attenuazione per il contenimento del processo - {evt}'),
(1184, 'ro', 'Sisteme de descărcare pentru izolarea proceselor - {evt}'),
(1185, 'it', 'Sistemi di protezione - {evt}'),
(1185, 'ro', 'Sisteme de protecţie - {evt}'),
(1186, 'it', 'Sistemi di blocco - {evt}'),
(1186, 'ro', 'Sisteme de oprire - {evt}'),
(1187, 'it', 'Ausili alla navigazione - {evt}'),
(1187, 'ro', 'Ajutoare navigare - {evt}'),
(1188, 'it', 'Rotating equipment - {evt}'),
(1188, 'ro', 'Echipament rotativ - {evt}'),
(1189, 'it', 'Attrezzature di evacuazione e salvataggio - {evt}'),
(1189, 'ro', 'Echipament de ieşire în caz de urgenţe, evacuare şi salvare - {evt}'),
(1190, 'it', 'Sistemi di comunicazione - {evt}'),
(1190, 'ro', 'Sisteme de comunicare - {evt}'),
(1191, 'it', 'altro - {evt} '),
(1191, 'ro', 'altele - {evt}'),
(1192, 'it', 'Incidenti causati dal malfunzionamento dell''equipaggiamento - Totale - {evt}'),
(1192, 'ro', 'Incidente cauzate de defecţiuni ale echipamentelor - Total - {evt}'),
(1193, 'it', 'Progettazioni errate - {evt}'),
(1193, 'ro', 'Defecțiune legată de proiectare - {evt}'),
(1194, 'it', 'Corrosione interna - {evt}'),
(1194, 'ro', 'Coroziune internă - {evt}'),
(1195, 'it', 'Corrosione esterna - {evt} '),
(1195, 'ro', 'Coroziune externă - {evt}'),
(1196, 'it', 'Guasti meccanici da fatica - {evt}'),
(1196, 'ro', 'Dereglare mecanică datorată oboselii - {evt}'),
(1197, 'it', 'Guasti meccanici da usura - {evt}'),
(1197, 'ro', 'Dereglare mecanică datorată uzurii- {evt}'),
(1198, 'it', 'Guasti meccanici da materiale difettoso - {evt}'),
(1198, 'ro', 'Dereglare mecanică datorată materialului defect - {evt}'),
(1199, 'it', 'Guasti meccanici (nave/elicottero) - {evt}'),
(1199, 'ro', 'Dereglare mecanică (navă / elicopter) - {evt}'),
(1200, 'it', 'Guasti strumentali - {evt}'),
(1200, 'ro', 'Defecţiune instrument - {evt}'),
(1201, 'it', 'Guasti del sistema di controllo - {evt}'),
(1201, 'ro', 'Defecţiune a sistemului de comandă - {evt}'),
(1202, 'it', 'Errore umano — Errore operativo - Totale - {evt}'),
(1202, 'ro', 'Eroare umană - defecţiune operaţională - Total - {evt}'),
(1203, 'it', 'Errore operativo - {evt}'),
(1203, 'ro', 'Eroare umană - defecțiune operațională - {evt}'),
(1204, 'it', 'Errore di manutenzione - {evt}'),
(1204, 'ro', 'Eroare de întreținere - {evt}'),
(1205, 'it', 'Errore di collaudo - {evt}'),
(1205, 'ro', 'Eroare de testare  - {evt}'),
(1206, 'it', 'Errore di ispezione - {evt}'),
(1206, 'ro', 'Eroare de inspecție - {evt}'),
(1207, 'it', 'Errore di progettazione - {evt}'),
(1207, 'ro', 'Defecțiune legată de proiectare - {evt}'),
(1209, 'it', 'Valutazione/percezione del rischio inadeguata - {evt} '),
(1209, 'ro', 'Evaluare / percepţie necorespunzătoare cu privire la riscuri - {evt}'),
(1210, 'it', 'Istruzioni/procedure inadeguate  - {evt} '),
(1210, 'ro', 'Instrucțiuni/proceduri necorespunzătoare - {evt}'),
(1211, 'it', 'Mancata conformità alla procedura - {evt}'),
(1211, 'ro', 'Nerespectarea procedurii - {evt}'),
(1212, 'it', 'Mancata conformità alla licenza - {evt}'),
(1212, 'ro', 'Nerespectarea permisului de lucru - {evt}'),
(1213, 'it', 'Comunicazione inadeguata - {evt} '),
(1213, 'ro', 'Comunicare necorespunzătoare - {evt}'),
(1214, 'it', 'Competenze personali inadeguate - {evt} '),
(1214, 'ro', 'Competență necorespunzătoare a personalului - {evt}'),
(1215, 'it', 'Supervisione inadeguata - {evt} '),
(1215, 'ro', 'Supervizare necorespunzătoare - {evt}'),
(1216, 'it', 'Organizzazione della sicurezza inadeguata - {evt} '),
(1216, 'ro', 'Conducere în ceea ce privește chestiunile de siguranță necorespunzătoare - {evt}'),
(1217, 'it', 'Principali esperienze acquisite - {evt}'),
(1217, 'ro', 'Cele mai importante lecții învățate - {evt}'),
(1218, 'it', 'Cause metereologiche - Totale - {evt}'),
(1218, 'ro', 'Cauze ce ţin de condiţiile meteorologice - Total - {evt}'),
(1219, 'it', 'Vento superiore alle specifiche di progettazione - {evt} '),
(1219, 'ro', 'Vânt ce depășește limitele prevăzute prin proiectare - {evt}'),
(1220, 'it', 'Moto ondoso superiore alle specifiche di progettazione - {evt} '),
(1220, 'ro', 'Valuri ce depăşeşc limitele prevăzute prin proiectare - {evt}'),
(1221, 'it', 'Visibilità estremamente ridotta superiore alle specifiche di progettazione - {evt} '),
(1221, 'ro', 'Vizibilitate extrem de redusă, ce depășește limitele prevăzute prin proiectare - {evt}'),
(1222, 'it', 'Presenza di ghiaccio or iceberg - {evt} '),
(1222, 'ro', 'Prezenţa gheţii / gheţari - {evt}'),
(1223, 'it', 'nuovo'),
(1223, 'ro', 'nou'),
(1224, 'it', 'in corso'),
(1224, 'ro', 'În procesare'),
(1225, 'it', 'finalizzato'),
(1225, 'ro', 'Finalizat'),
(1226, 'it', 'Per essere contabilizzato nel'),
(1226, 'ro', 'Pentru a fi luat în considerare în faza de '),
(1227, 'it', 'stesura della bozza della relazione annuale del CPF '),
(1227, 'ro', 'editare a raportului anual (CPF)'),
(1228, 'it', 'ogni incidente (precedentemente valutato in termini di Incidente Grave) deve essere analizzato in termini di qualificazione in categorie uno o più guasti che fanno parte della Sezione 4 del CPF.'),
(1228, 'ro', 'fiecare incident (clasificat în prealabil din punct de vedere al accidentului major) trebuie analizat şi clasificat în termenii definiţi de Secţiunea 4 a CPF.'),
(1229, 'it', 'In SYRIO , questo processo viene indicato come il <b> {evt} </b> di un incidente.'),
(1229, 'ro', 'În SYRIO, acest proces poartă denumirea de <b>{evt}</b>.'),
(1230, 'it', 'Una volta fatto, l\\''incidente deve essere contrassegnato come pronto, cioè <kbd>Finalizzata</kbd>.'),
(1230, 'ro', 'Odată încheiat acest proces, incidentul trebuie marcat ca <kbd>Finalizat</kbd>.'),
(1231, 'it', 'Solo gli incidenti, i quali sono in fase di preparazione di <kbd> Finalizzata </kbd> sono presi in considerazione nella fase di stesura della bozza del CPF!'),
(1231, 'ro', 'Doar incidentele marcate - în această fază - ca <kbd>Finalizat</kbd> sunt luate în considerare în faza de editare a CPF!'),
(1232, 'it', 'Sul lato destro è possibile trovare l\\''elenco degli incidenti e il loro stato in relazione alla Valutazione CPF sezione 4 .'),
(1232, 'ro', 'În partea dreaptă este afisată lista incidentelor, împreuna cu starea acestora din punct de vedere al evaluarii în termenii Secţiunii 4 a CPF.'),
(1233, 'it', 'I tuoi compiti'),
(1233, 'ro', 'Sarcinile dumneavoastră'),
(1234, 'it', 'Notare che'),
(1234, 'ro', 'Note'),
(1235, 'it', 'Riaprire'),
(1235, 'ro', 'Redeschide'),
(1236, 'it', 'Valutazione da riaprire?'),
(1236, 'ro', 'Redeschide evaluare?'),
(1237, 'it', 'Valutazione dell''Incidente Sezione 4 del CPF'),
(1237, 'ro', 'Evaluare în termenii Secţiunii 4 a CPF'),
(1238, 'it', 'L''errata progettazione è stata segnalata come una delle cause del rilascio (vedere Sezione A.1.Elenco di cause della perdita (a))'),
(1238, 'ro', 'Defecțiune legată de proiectare a fost raportat ca una din cauzele emisiei (v. Secțiunea A.1. Listă de verificare pentru cauza scurgerii (a))'),
(1239, 'it', 'Corrosione interna è stata segnalata come una delle cause del rilascio (vedere Sezione A.1.Elenco di cause della perdita (b))'),
(1239, 'ro', 'Coroziunea internă a fost raportată ca una din cauzele emisiei (v. Secțiunea A.1. Listă de verificare pentru cauza scurgerii (b))'),
(1240, 'it', 'Corrosione esterna  è stata segnalata come una delle cause del rilascio (vedere Sezione A.1.Elenco di cause della perdita (b))'),
(1240, 'ro', 'Coroziunea externă a fost raportată ca una din cauzele emisiei (v. Secțiunea A.1. Listă de verificare pentru cauza scurgerii (b))'),
(1241, 'it', 'Guasto meccanico da fatica è stato segnalato come una delle cause del rilascio (vedere Sezione A.1.Elenco di cause della perdita (b))'),
(1241, 'ro', 'Dereglarea mecanică datorată oboselii a fost raportată ca una din cauzele emisiei (v. Secțiunea A.1. Listă de verificare pentru cauza scurgerii (b))'),
(1242, 'it', 'Guasto meccanico da usura è stato segnalato come una delle cause del rilascio (vedere Sezione A.1.Elenco di cause della perdita (b))'),
(1242, 'ro', 'Dereglarea mecanică datorată uzurii a fost raportată ca una din cauzele emisiei (v. Secțiunea A.1. Listă de verificare pentru cauza scurgerii (b))'),
(1243, 'it', 'Difetto materiale  è stata segnalata come una delle cause del rilascio (vedere Sezione A.1.Elenco di cause della perdita (b))'),
(1243, 'ro', 'Defectul material a fost raportat ca una din cauzele emisiei (v. Secțiunea A.1. Listă de verificare pentru cauza scurgerii (b))'),
(1244, 'it', 'Almeno uno di: Funzionamento scorretto, Lasciato aperto, Apertura con contenuto di idrocarburi, Oggetto caduto, è stato segnalato come una delle cause del rilascio (vedere Sezione A.1.Elenco di cause della perdita (c))'),
(1244, 'ro', 'xxxxxxxxxxxxxxx'),
(1245, 'it', 'Manutenzione scorretta è stata segnalata come una delle cause del rilascio (vedere Sezione A.1.Elenco di cause della perdita (c))'),
(1245, 'ro', 'Întreținerea necorespunzătoare a fost raportată ca una din cauzele emisiei (v. Secțiunea A.1. Listă de verificare pentru cauza scurgerii (c))');
INSERT INTO `t_message` (`id`, `language`, `translation`) VALUES
(1246, 'it', 'Collaudo scorretto  è stata segnalata come una delle cause del rilascio (vedere Sezione A.1.Elenco di cause della perdita (c))'),
(1246, 'ro', 'Testarea necorespunzătoare a fost raportată ca una din cauzele emisiei (v. Secțiunea A.1. Listă de verificare pentru cauza scurgerii (c))'),
(1247, 'it', 'Ispezione scorretta è stata segnalata come una delle cause del rilascio (vedere Sezione A.1.Elenco di cause della perdita (c))'),
(1247, 'ro', 'Inspecția necorespunzătoare a fost raportată ca una din cauzele emisiei (v. Secțiunea A.1. Listă de verificare pentru cauza scurgerii (c))'),
(1248, 'it', 'Altro o Erosione è stato segnalato come uno delle (attrezzature) cause del rilascio (vedere Sezione A.1 Elenco delle cause della perdita (b))'),
(1248, 'ro', 'Altele și/sau Eroziunea au fost raportate între cauzele legate de echipament a emisiei (v. Secțiunea A.1. Listă de verificare pentru cauza scurgerii (b))'),
(1249, 'it', 'Procedura carente è stata segnalata come una delle cause del rilascio (vedere Sezione A.1.Elenco di cause della perdita (d))'),
(1249, 'ro', 'Procedură deficitară a fost raportată ca una din cauzele emisiei (v. Secțiunea A.1. Listă de verificare pentru cauza scurgerii (d))'),
(1250, 'it', 'Mancata conformità alla procedura è stata segnalata come una delle cause del rilascio (vedere Sezione A.1.Elenco di cause della perdita (d))'),
(1250, 'ro', 'Nerespectarea procedurii a fost raportată ca una din cauzele emisiei (v. Secțiunea A.1. Listă de verificare pentru cauza scurgerii (d))'),
(1251, 'it', 'Mancata conformità alla licenza è stata segnalata come una delle cause del rilascio (vedere Sezione A.1.Elenco di cause della perdita (d))'),
(1251, 'ro', 'Nerespectarea permisului de lucru a fost raportată ca una din cauzele emisiei (v. Secțiunea A.1. Listă de verificare pentru cauza scurgerii (d))'),
(1252, 'it', 'Altro è stato segnalato come una delle (procedurale) cause di rilascio (vedere Sezione A.1 Elenco delle cause della perdita (d))'),
(1252, 'ro', 'Altele a fost raportată ca una din cauzele procedurale a emisiei (v. Secțiunea A.1. Listă de verificare pentru cauza scurgerii (d))'),
(1253, 'it', 'Guasti di elementi critici per la sicurezza e l’ambiente'),
(1253, 'ro', 'Defecţiuni ale elementelor critice de siguranţă şi de mediu'),
(1255, 'it', 'Numero di incidenti gravi'),
(1255, 'ro', 'Număr accidente majore'),
(1256, 'it', 'Emissione Accidentale - Totale'),
(1256, 'ro', 'Scurgeri neintenţionate - Total'),
(1257, 'it', 'Emissione di sostanze pericolose'),
(1257, 'ro', 'Scurgeri de substanţe periculoase'),
(1258, 'it', 'Perdita di controllo del pozzo - Totale'),
(1258, 'ro', 'Pierderea controlului asupra sondei - Total'),
(1259, 'it', 'Eruzioni / attivazione divertori'),
(1259, 'ro', 'Activarea sistemului BOP / de deviere'),
(1260, 'it', 'Guasto della barriera di un pozzo'),
(1260, 'ro', 'Defectarea barierei sondei'),
(1261, 'it', 'Perdita di integrità strutturale - totale'),
(1261, 'ro', 'Pierderea integrităţii structurale - Total'),
(1263, 'it', 'Collisione con una nave - totale'),
(1263, 'ro', 'Coliziuni de nave - Total'),
(1264, 'it', 'Lesioni gravi di 5 o più persone nello stesso incidente'),
(1264, 'ro', 'Vătămare gravă a cinci sau mai multe persoane afectate de acelaşi accident'),
(1265, 'it', 'Numero di decessi'),
(1265, 'ro', 'Număr de accidente mortale - Total'),
(1266, 'it', 'Sistemi di rilevamento'),
(1266, 'ro', 'Sisteme de detectare'),
(1267, 'it', 'Attrezzature rotanti'),
(1267, 'ro', 'Echipament rotativ'),
(1268, 'it', 'Sistemi di comunicazione'),
(1268, 'ro', 'Sisteme de comunicare'),
(1269, 'it', 'Guasti alle attrezzature - Totale'),
(1269, 'ro', 'Defecţiune echipament - Total'),
(1270, 'it', 'Guasti meccanici (nave/elicottero)'),
(1270, 'ro', 'Dereglare mecanică (navă / elicopter)'),
(1271, 'it', 'Guasti strumentali'),
(1271, 'ro', 'Defecţiune instrument'),
(1272, 'it', 'Guasti del sistema di controllo'),
(1272, 'ro', 'Defecţiune a sistemului de comandă'),
(1273, 'it', 'Errore umano — Errore operativo - Totale'),
(1273, 'ro', 'Eroare umană - defecţiune operaţională - Total'),
(1275, 'it', 'Mancata conformità alla procedura'),
(1275, 'ro', 'Nerespectarea procedurii'),
(1276, 'it', 'Principali esperienze acquisite'),
(1276, 'ro', 'Cele mai importante lecții învățate'),
(1277, 'it', 'Cause metereologiche - Totale'),
(1277, 'ro', 'Cauze ce ţin de condiţiile meteorologice - Total'),
(1278, 'it', 'Moto ondoso superiore alle specifiche di progettazione'),
(1278, 'ro', 'Valuri ce depăşeşc limitele prevăzute prin proiectare'),
(1279, 'it', 'Presenza di ghiaccio or iceberg'),
(1279, 'ro', 'Prezenţa gheţii / gheţari'),
(1280, 'it', 'non si può dire'),
(1280, 'ro', 'nu se poate spune'),
(1281, 'it', 'Sistemi di integrità strutturale'),
(1281, 'ro', 'Sisteme de integritate structurală'),
(1282, 'it', 'Sistemi di blocco'),
(1282, 'ro', 'Sisteme de oprire'),
(1283, 'it', 'Sistemi di protezione'),
(1283, 'ro', 'Sisteme de protecţie '),
(1285, 'it', 'Attrezzature di evacuazione e salvataggio'),
(1285, 'ro', 'Echipament de ieşire în caz de urgenţe, evacuare şi salvare'),
(1286, 'it', 'altro'),
(1286, 'ro', 'altele'),
(1287, 'it', 'Corrosione interna'),
(1287, 'ro', 'Coroziune internă'),
(1288, 'it', 'Corrosione esterna'),
(1288, 'ro', 'Coroziune externă '),
(1290, 'it', 'Mancata conformità alla licenza'),
(1290, 'ro', 'Nerespectarea permisului de lucru'),
(1291, 'it', 'tu'),
(1291, 'ro', 'utilizator'),
(1292, 'it', 'calcolata'),
(1292, 'ro', 'identificat'),
(1293, 'it', 'evento della relazione'),
(1293, 'ro', 'raport eveniment'),
(1294, 'it', 'tipo di evento'),
(1294, 'ro', 'clasificare eveniment'),
(1295, 'it', 'Errore procedurale / organizzativo - {evt}'),
(1295, 'ro', 'Eroare procedurală / organizational - {evt}'),
(1296, 'it', 'Modifica'),
(1296, 'ro', 'Modificaţi'),
(1297, 'it', 'Successo'),
(1297, 'ro', 'Operaţie reusită'),
(1298, 'it', 'Hai riaperto ''{evt}''. L''evento NON SARA'' contabilizzata nel Generatore della Relazione Annuale.'),
(1298, 'ro', '''{evt}'' a fost redeschisă. Evenimentul NU va fi luat în considerare în cadrul Raportului Annual.'),
(1299, 'it', 'Hai già una relazione registrata per quest''anno.'),
(1299, 'ro', 'Există deja un raport înregistrat pentru acest an.'),
(1301, 'it', 'Unità di produzione del petrolio'),
(1301, 'ro', 'Unitate de măsură producție petrol'),
(1302, 'it', 'Unità di produzione del gas'),
(1302, 'ro', 'Unitatea de măsură a producției de gaz'),
(1303, 'it', 'Giorni-uomo sugli impianti'),
(1303, 'ro', 'Zile-om petrecute la bordul instalaţiei'),
(1304, 'it', '(numero di) Incidenti gravi'),
(1304, 'ro', 'Accidente majore (număr de investigaţii)'),
(1305, 'it', '(numero di) Problemi ambientali'),
(1305, 'ro', 'Preocupări în materie de siguranţă şi de mediu (număr de investigaţii)'),
(1306, 'it', 'Hai {evt} nuovi record trovato.'),
(1306, 'ro', 'Au fost găsite {evt} cazuri noi.'),
(1307, 'it', 'Valutazione da finalizzare?'),
(1307, 'ro', 'Finalizaţi evaluarea?'),
(1308, 'it', 'Hai segnato ''{evt}'' come finalizzata. La relazione sarà contabilizzata nel Generatore Relazione Annuale.'),
(1308, 'ro', '''{evt}'' a fost marcat ca Finalizat. Evenimentul VA FI LUAT ÎN CONSIDERARE în faza de editare a Raportului Anual.'),
(1309, 'it', 'Relazioni annuali'),
(1309, 'ro', 'Rapoarte anuale'),
(1311, 'it', 'Solo una Relazione Annuale per anno è disponibile.'),
(1311, 'ro', 'Un singur raport anual este permis pentru fiecare an.'),
(1312, 'it', 'Note'),
(1312, 'ro', 'Note'),
(1313, 'it', 'Creare la Relazione Annuale'),
(1313, 'ro', 'Crează raport anual'),
(1314, 'it', 'La valutazione {evt} in termini di Incidente grave.'),
(1314, 'ro', 'Raportul {evt} din punct de vedere al Accident Major.'),
(1315, 'it', 'non sottoposte a valutazione'),
(1315, 'ro', 'neevaluat'),
(1316, 'it', 'Creare {evt}'),
(1316, 'ro', 'Crează {evt}'),
(1317, 'it', 'Si prega di fornire l''anno per la relazione.'),
(1317, 'ro', 'Introduceți anul de raportare.'),
(1319, 'it', 'IMPIANTI'),
(1319, 'ro', 'INSTALAŢII'),
(1320, 'it', 'almeno un incidente'),
(1320, 'ro', 'cel puţin un incident'),
(1321, 'it', 'Solo gli impianti che hanno riportato {evt} nel periodo di riferimento li stanno prendendo in considerazione.'),
(1321, 'ro', 'Doar instalaţiile care au avut {evt} în perioada anului de raportare sunt luate în considerare.'),
(1322, 'it', 'Aggiungendo il resto degli impianti è possibile dopo il caricamento di questa valutazione (file XML) sulla piattaforma SPIROS.'),
(1322, 'ro', 'Adăugarea celorlalte instalaţii este posibilă odată cu încărcarea acestui raport anual (ca fisier XML) în platforma online SPIROS.'),
(1324, 'it', 'Si prega di fornire l''elenco dettagliato di impianti per operazione di petrolio e gas in mare aperto nel tuo paese (il primo Gennaio dell''anno di riferimento).'),
(1324, 'ro', 'Vă rugăm să transmiteţi o listă detaliată a instalaţiilor pentru operaţii petroliere şi gaziere offshore în ţara dumneavoastră (la data de 1 ianuarie a anului pentru care se face raportarea).'),
(1326, 'it', '(lon - lat)'),
(1326, 'ro', '(lon-lat)'),
(1329, 'it', 'Area geografica delle operazioni: e Durata'),
(1329, 'ro', 'Zona geografică în care se desfăşoară operaţiunile; şi durata'),
(1330, 'it', 'Produzione di petrolio (norma)'),
(1330, 'ro', 'Unitate de măsură producție petrol (norm)'),
(1332, 'it', 'Produzione di gas (norma)'),
(1332, 'ro', 'Producția de gaz (norm)'),
(1334, 'it', 'Principali interventi di applicazione delle norme'),
(1334, 'ro', 'Măsurile principale de asigurare a respectării legii'),
(1342, 'it', '(numero segnalazione)'),
(1342, 'ro', '(număr raportat)'),
(1343, 'it', '(Eventi/ore lavorate)'),
(1343, 'ro', 'evenimente/ore lucrate'),
(1344, 'it', '(evento/kTOE)'),
(1344, 'ro', 'evenimente/kTOE'),
(1345, 'it', 'Fornito in {evt} di questo documento'),
(1345, 'ro', 'Conform {evt} a acestui document'),
(1347, 'it', 'Impossibile eseguire la richiesta!'),
(1347, 'ro', 'Acţiunea nu a putut fi finalizată!'),
(1348, 'it', 'Controllare le informazioni qui sotto e riprovare.'),
(1348, 'ro', 'Verificaţi informaţiile de mai jos şi încercaţi din nou.'),
(1349, 'it', 'Almeno una zona operativa deve essere fornita per ogni impianto mobile'),
(1349, 'ro', 'Cel puțin o zonă de operare trebuie specificată pentru fiecare dintre instalațiile mobile'),
(1350, 'it', 'Si prega di fornire le coordinate di ogni impianto fisso'),
(1350, 'ro', 'Vă rugăm introduceți coordonatele geografice ale fiecărei instalații fixe.'),
(1351, 'it', 'Lista degli impianti è stata aggiornata con successo dalla lista incidenti'),
(1351, 'ro', 'Lista instalaţiilor a fost actualizată cu succes.'),
(1352, 'it', 'Di seguito si sono verificati:'),
(1352, 'ro', 'Au fost aduse următoarele modificări:'),
(1353, 'it', 'Nuovi impianti'),
(1353, 'ro', 'Instalaţii adăugate'),
(1354, 'it', 'Saltato'),
(1354, 'ro', 'Omise'),
(1355, 'it', 'Rimosso'),
(1355, 'ro', 'Retrase'),
(1356, 'it', '{evt} modificato'),
(1356, 'ro', '{evt} modificat(ă)'),
(1357, 'it', 'Lista degli impianti'),
(1357, 'ro', 'Lista instalaţiilor '),
(1358, 'it', '{modelClass}:'),
(1358, 'ro', '{modelClass}:'),
(1360, 'it', 'area operativa'),
(1360, 'ro', 'zona geografică de operare'),
(1361, 'it', 'Nome dell''area'),
(1361, 'ro', 'numele zonei'),
(1362, 'it', 'durata in mesi'),
(1362, 'ro', 'durata în luni'),
(1363, 'it', 'Dati di produzione'),
(1363, 'ro', 'Date referitoare la producţie'),
(1364, 'it', '{evt} produzione'),
(1364, 'ro', 'Producţie {evt}'),
(1365, 'it', 'ore'),
(1365, 'ro', 'ore'),
(1366, 'it', 'Generare SPIROS XML?'),
(1366, 'ro', 'Generaţi fişierul de tip SPIROS xml?'),
(1367, 'it', 'Costruire SPIROS XML'),
(1367, 'ro', 'Generare SPIROS XML'),
(1368, 'it', 'Impossibile creare il file XML! Si prega di leggere le informazioni qui di seguito e riprovare.'),
(1368, 'ro', 'Fisierul XML nu a putut fi generat. Verificaţi informaţiile de mai jos şi încercaţi din nou.'),
(1369, 'it', 'La password è stata modificata con successo.'),
(1369, 'ro', 'Parola dumneavoastră a fost modificată cu succes.'),
(1370, 'it', 'È necessario fornire un valore per {evt} ({sec})'),
(1370, 'ro', 'Trebuie să introduceţi o valoare pentru {evt} ({sec})'),
(1371, 'it', '{modelClass}: Modifica'),
(1371, 'ro', '{modelClass}: Modifică'),
(1372, 'it', 'Url'),
(1372, 'ro', 'Url'),
(1373, 'it', 'riferimento'),
(1373, 'ro', 'referinţă'),
(1376, 'it', 'titolo'),
(1376, 'ro', 'titlu'),
(1378, 'it', '{evt, plural, =1{# riferimento} other{# riferimenti} }'),
(1378, 'ro', '{evt, plural, =1{# referinţă} other{# referinţe} }'),
(1380, 'it', 'Si prega di aggiungere qualsiasi riferimento relativo al informazione nella Sezione 3.4. qui sopra (se presente)'),
(1380, 'ro', 'Adăugati orice referinţa la informaţia din Secţiunea 3.4.'),
(1381, 'it', 'Pagina non trovata'),
(1381, 'ro', 'Pagină inexistentă'),
(1382, 'it', 'Dalla lista dei titoli attualmente disponibili (*) spuntare quelli che si desidera utilizzare come riferimento nella sezione 3.4. della CRF attuale.'),
(1382, 'ro', 'Selectaţi din lista de titluri disponibile pe cele pe care doriţi să le utilizaţi ca referinţe în Sectiunea 3.4 a raportului CPF curent.'),
(1383, 'it', 'Se non riuscite a trovare il titolo all''interno della lista fornita, usare {evt} per registrare un nuovo documento.'),
(1383, 'ro', 'Dacă nu regăsiţi un titlu în lista de mai jos utilizaţi {evt} pentru înregistrarea unui nou document.'),
(1384, 'it', 'titolo di riferimento'),
(1384, 'ro', 'titlu referinţă'),
(1385, 'it', 'url di riferimento (se presente)'),
(1385, 'ro', 'url referinţă (dacă există)'),
(1386, 'it', 'Titoli disponibili'),
(1386, 'ro', 'Titluri disponibile'),
(1387, 'it', 'qui'),
(1387, 'ro', 'aici'),
(1388, 'it', 'Clicca {evt} per gestire i documenti registrati.'),
(1388, 'ro', 'Pentru a gestiona documentele înregistrate, apăsaţi {evt}.'),
(1389, 'it', '{evt, plural,=0{non riferenziato} =1{# Valutazione Annuale} other{# Valutazioni Annuale}}'),
(1389, 'ro', '{evt, plural,=0{nu este utilizat ca referinţă} =1{# Raport anual} other{# Rapoarte anuale}}'),
(1390, 'it', 'Modifica {modelClass}:'),
(1390, 'ro', 'Modifică {modelClass}:'),
(1391, 'it', 'si fa riferimento in'),
(1391, 'ro', 'Referinţă în'),
(1392, 'it', 'Funzionamento limitato'),
(1392, 'ro', 'Operaţie interzisă'),
(1393, 'it', 'Non si dispone di un numero sufficiente di credenziali per eseguire questa operazione.'),
(1393, 'ro', 'Nu aveţi suficiente drepturi utilizator pentru a executa această operaţie.'),
(1394, 'it', 'Effettua il login con un account diverso; o'),
(1394, 'ro', 'Folosiţi un alt cont pentru autentificare; sau'),
(1395, 'it', 'Contattare l''amministratore di SyRIO per richiedere di elevare i propri diritti.'),
(1395, 'ro', 'Contactaţi administratorii SyRIO pentru a cere ridicarea nivelului de acces pentru acest cont.'),
(1396, 'it', 'Per favore fai prima il log in.'),
(1396, 'ro', 'Vă rugăm să vă autentificați.'),
(1397, 'it', 'non grave'),
(1397, 'ro', 'minor'),
(1398, 'it', 'non richiesto'),
(1398, 'ro', 'nu este necesar'),
(1399, 'it', 'non disponibile'),
(1399, 'ro', 'indisponibil'),
(1400, 'it', 'Lista dei messaggi di origine'),
(1400, 'ro', 'Lista mesajelor referință'),
(1401, 'it', 'unità'),
(1401, 'ro', 'unitate de măsură'),
(1402, 'it', '''{evt}'' è stato con successo {evt2}.'),
(1402, 'ro', '{evt} a fost {evt2} cu succes.'),
(1403, 'it', 'Nome utente o password errati.'),
(1403, 'ro', 'Nume utilizator şi/sau parola gresite.'),
(1407, 'it', 'Utilizzare questi moduli per ottenere la registrazione di installazione in SyRIO.'),
(1407, 'ro', 'Utilizați aceste formulare pentru înregistrarea unei noi instalații în SyRIO.'),
(1409, 'it', 'Utilizzare questi moduli per la registrazione in SyRIO.'),
(1409, 'ro', 'Utilizați aceste formulare pentru înregistrarea ca utilizator SyRIO.'),
(1413, 'it', 'Utilizzare una delle opzioni qui sotto per inviare un messaggio di testo libero.'),
(1413, 'ro', 'Utilizați una dintre opțiunile de mai jos pentru a contacta:'),
(1414, 'it', 'Amministratori di SyRIO'),
(1414, 'ro', 'Administratorul de sistem SyRIO'),
(1416, 'it', 'Registra una nuova {evt} Organizzazione'),
(1416, 'ro', 'Cerere înregistrare Organizație {evt}'),
(1418, 'it', 'Registrati come {evt} utente'),
(1418, 'ro', 'Cerere înregistrare ca utilizator de tip {evt}'),
(1419, 'it', 'Nuova organizzazione Autorità Competente'),
(1419, 'ro', 'Organizație de tip Autoritate Competentă nouă'),
(1420, 'it', 'Modulo di registrazione dell''organizzazione'),
(1420, 'ro', 'Formular de înregistrare Organizație'),
(1421, 'it', 'Compilare questo modulo per inviare una {evt} richiesta di registrazione dell''Organizzazione'),
(1421, 'ro', 'Completați acest formular pentru a înainta o cerere de înregistrate a unei noi organizații de tip {evt}.'),
(1423, 'it', 'Generale'),
(1423, 'ro', 'Informaţii generale'),
(1424, 'it', 'Informazioni sulla tua organizzazione'),
(1424, 'ro', 'Informații referitoare la organizație'),
(1425, 'it', 'Il nome completo dell''organizzazione'),
(1425, 'ro', 'Numele complet al organizației'),
(1426, 'it', 'Nome dell''organizzazione'),
(1426, 'ro', 'Numele organizației'),
(1427, 'it', 'Acronimo dell''organizzazione'),
(1427, 'ro', 'Acronimul organizației'),
(1428, 'it', 'Telefono dell''organizzazione'),
(1428, 'ro', 'Telefon organizație'),
(1429, 'it', 'Email dell''organizzazione'),
(1429, 'ro', 'Adresă email organizație'),
(1430, 'it', 'Fax dell''organizzazione'),
(1430, 'ro', 'Fax organizație'),
(1431, 'it', 'Telefono (2) dell''organizzazione'),
(1431, 'ro', 'Telefon organizație (alt.)'),
(1432, 'it', 'Website dell''organizzazione'),
(1432, 'ro', 'Adresă web organizație'),
(1435, 'it', 'Città'),
(1435, 'ro', 'Localitatea'),
(1436, 'it', 'Nazione'),
(1436, 'ro', 'Județ / Sector'),
(1437, 'it', 'C.A.P.'),
(1437, 'ro', 'Cod poștal'),
(1438, 'it', 'Codice di verifica'),
(1438, 'ro', 'Cod de verificare'),
(1439, 'it', 'Nome'),
(1439, 'ro', 'Prenume'),
(1440, 'it', 'Cognome'),
(1440, 'ro', 'Nume'),
(1441, 'it', 'Conferma email'),
(1441, 'ro', 'Email (repetă)'),
(1442, 'it', 'L''acronimo dell''organizzazione'),
(1442, 'ro', 'Acronimul organizației'),
(1443, 'it', 'Indirizzo dell''organizzazione'),
(1443, 'ro', 'Adresa organizației'),
(1444, 'it', 'Dettagli del richiedente'),
(1444, 'ro', 'Date referitoare la solicitant'),
(1445, 'it', 'Stato dei tuoi dati personali qui di seguito.'),
(1445, 'ro', 'Completați câmpurile de mai jos cu informațiile referitoare la dumneavoastră.'),
(1446, 'it', 'Si prega di notare che le informazioni qui di seguito possono essere utilizzate con l''Organizzazione da registrare ai fini della verifica.'),
(1446, 'ro', 'Vă informăm că informațiile de mai jos pot fi comunicate Organizației pentru care se solicită înregistrarea, din motive de verificare.'),
(1447, 'it', 'Il responsabile (essendo informato) lo stato della richiesta sei tu.'),
(1447, 'ro', 'Persoana responsabilă (persoana de contact) pentru această solicitare sunteți dumneavoastră.'),
(1448, 'it', 'In entrambi i casi il nome completo o l''acronimo della vostra organizzazione'),
(1448, 'ro', 'Numele complet sau acronimul organizației dumneavoastră'),
(1449, 'it', 'Il vostro ruolo all''interno dell''organizzazione'),
(1449, 'ro', 'Funcția dumneavoastră în cadrul organizației.'),
(1451, 'it', 'Una volta inviata, la richiesta sarà valutata dagli amministratori di sistema . Sarete informati via email una volta che la richiesta sarà stata elaborata.'),
(1451, 'ro', 'Odată transmisă, cererea dumneavoastră va fi analizată de către administratorii sistemului. Veți primi o notificare prin poștă electronică în momentul în care solicitarea dumneavoastră este procesată.'),
(1452, 'it', 'Via'),
(1452, 'ro', 'Strada'),
(1453, 'it', 'Via (riga 2)'),
(1453, 'ro', 'Strada (rândul 2)'),
(1455, 'it', 'Non operante'),
(1455, 'ro', 'nu funcționează'),
(1456, 'it', 'Smantellati '),
(1456, 'ro', 'dezafectat'),
(1457, 'it', 'Operativa'),
(1457, 'ro', 'operațional'),
(1458, 'it', 'Modulo di registrazione dell''impianto '),
(1458, 'ro', 'Formular de înregistrare al unei instalații'),
(1459, 'it', 'Compilare questo modulo per inviare una richiesta di registrazione di {evt}.'),
(1459, 'ro', 'Completați acest formular pentru a înainta o cerere de înregistrate a unei {evt}.'),
(1461, 'it', 'Informazioni sull''impianto'),
(1461, 'ro', 'Informații referitoare la instalație'),
(1462, 'it', 'Il nome o acronimo dell''Operatore/Proprietario dell''organizzazione dell''impianto'),
(1462, 'ro', 'Numele sau acronimul Operatorului / Proprietarului instalației'),
(1463, 'it', 'Nome dell''impianto'),
(1463, 'ro', 'Numele instalației'),
(1464, 'it', 'Tipo (se altro)'),
(1464, 'ro', 'Tip (altul)'),
(1465, 'it', 'Bandiera (se MODU)'),
(1465, 'ro', 'Pavilion (dacă tipul instalației este MODU)'),
(1466, 'it', 'Stato operativo'),
(1466, 'ro', 'Status operațional'),
(1467, 'it', 'Contatto telefonico (impianto)'),
(1467, 'ro', 'Telefon de contact (instalație)'),
(1468, 'it', 'Contatto email (impianto)'),
(1468, 'ro', 'Email de contact (instalație)'),
(1469, 'it', 'Contatto fax (impianto)'),
(1469, 'ro', 'Fax de contact (instalație)'),
(1470, 'it', 'Il nome delll''impianto'),
(1470, 'ro', 'Precizați numele instalației'),
(1471, 'it', 'tipo d''impianto'),
(1471, 'ro', 'tipul instalației'),
(1472, 'it', 'Il tipo d''impianto'),
(1472, 'ro', 'Specificați tipul instalației'),
(1473, 'it', 'Nazione di registrazione'),
(1473, 'ro', 'țara în care este înregistrat vasul'),
(1474, 'it', 'Bandiera dell''impianto(se MODU)'),
(1474, 'ro', 'Pavilionul vasului'),
(1475, 'it', 'esempio: FPSO'),
(1475, 'ro', 'de ex. FPSO'),
(1476, 'it', 'Codice di registrazione dell''impianto (Numero IMO se esiste)'),
(1476, 'ro', 'Codul de înregistrare a instalației (Numărul IMO dacă este disponibil)'),
(1477, 'it', 'al momento della richiesta'),
(1477, 'ro', 'la momentul acestei solicitări'),
(1478, 'it', 'impianto'),
(1478, 'ro', 'instalații'),
(1481, 'it', 'Amministratore'),
(1481, 'ro', 'Administrator'),
(1482, 'it', 'Ispettore'),
(1482, 'ro', 'Evaluator'),
(1483, 'it', 'Membro'),
(1483, 'ro', 'Membru'),
(1484, 'it', 'Modulo di registrazione utente'),
(1484, 'ro', 'Formular de înregistrare al unui utilizator nou'),
(1485, 'it', 'Compilare questo modulo per inviare una {evt} richiesta di registrazione utente.'),
(1485, 'ro', 'Completați acest formular pentru a înainta o cerere de înregistrate a unui utilizator membru al {evt}.'),
(1487, 'it', 'Almeno un ruolo deve essere selezionato.'),
(1487, 'ro', 'Cel puțin un rol trebuie selectat.'),
(1488, 'it', 'Un ruolo valida deve essere selezionato.'),
(1488, 'ro', 'Rolul selectat nu există.'),
(1489, 'it', 'Personale'),
(1489, 'ro', 'Personal'),
(1490, 'it', 'Informazioni e dati personali di contatto'),
(1490, 'ro', 'Informații cu caracter personal și date de contact'),
(1491, 'it', 'Ruoli'),
(1491, 'ro', 'Roluri'),
(1492, 'it', 'Quale ruolo ti piacerebbe avere nel SyRIO'),
(1492, 'ro', 'Ce rol / roluri doriți să aveți în SyRIO'),
(1493, 'it', 'Tutti i campi sono obbligatori'),
(1493, 'ro', 'toate câmpurile sunt obligatorii'),
(1494, 'it', 'Autorità Competente'),
(1494, 'ro', 'Autorității Competente'),
(1496, 'it', 'Relatore'),
(1496, 'ro', 'Raportor'),
(1498, 'it', 'Nome del tuo impianto'),
(1498, 'ro', 'Numele instalației dumneavoastră'),
(1499, 'it', 'Nome del tuo impianto'),
(1499, 'ro', 'Numele instalației'),
(1500, 'it', 'In entrambi i casi il nome completo o l''acronimo del proprietario/gestore dell''impianto'),
(1500, 'ro', 'Numele complet sau acronimul proprietarului / operatorului instalației'),
(1501, 'it', 'Il tuo ruolo all''interno dell''impianto'),
(1501, 'ro', 'Funcția dumneavoastră în cadrul instalației.'),
(1502, 'it', 'Se avete richieste commerciali o altre domande, si prega di compilare il seguente modulo per contattarci. Grazie.'),
(1502, 'ro', 'Completați formularul de mai jos pentru a ne contacta.'),
(1503, 'it', 'Soggetto'),
(1503, 'ro', 'Subiect'),
(1504, 'it', 'Corpo'),
(1504, 'ro', 'Conținut'),
(1505, 'it', 'Grazie per averci contattato. Vi risponderemo al più presto possibile.'),
(1505, 'ro', 'Vă mulțumim că ne-ați contactat. Vă vom răspunde cât de curând posibil.'),
(1507, 'it', 'Le operazioni in questa sezione consente di controllare gli eventi registrati della vostra organizzazione.'),
(1507, 'ro', 'Operaţiile din această secţiune vă permit gestionarea evenimentelor înregistrate de către organizaţia dumneavoastră.'),
(1508, 'it', 'Modifica'),
(1508, 'ro', 'Modifică'),
(1510, 'it', 'Impianto nome/tipo'),
(1510, 'ro', 'Numele / tipul instalației'),
(1511, 'it', 'Ulteriori informazioni obbligatorie'),
(1511, 'ro', 'Informații suplimentare obligatorii'),
(1512, 'it', 'Le informazioni qui di seguito sono richieste solo per SyRIO.'),
(1512, 'ro', 'Informațiile de mai jos sunt necesare strict pentru funcționarea SyRIO.'),
(1513, 'it', 'Nessuna delle informazioni sarà parte della relazione CRF.'),
(1513, 'ro', 'Informația din această secțiune nu va face parte din conținutul raportului.'),
(1514, 'it', 'tratto dalle credenziali utente'),
(1514, 'ro', 'date preluate din informațiile de contact ale utilizatorului current'),
(1515, 'it', 'Si prega di indicare il nome e/o il tipo dell''impianto'),
(1515, 'ro', 'Vă rugăm selectați numele și/sau tipul instalației'),
(1516, 'it', 'Il nome o il codice del campo operazione dell''impianto (se pertinente):'),
(1516, 'ro', 'Numele sau codul câmpului în care operează instalația (dacă este relevant)'),
(1517, 'it', 'Devono essere fornito la data e ora dell\\''incidente!'),
(1517, 'ro', 'Data și ora producerii incidentului trebuie raportate.'),
(1518, 'it', 'Qualsiasi incidente deve essere collegato ad un impianto!'),
(1518, 'ro', 'Trebuie selectată instalația de care este legat incidentul!'),
(1519, 'it', 'Salvata come bozza'),
(1519, 'ro', 'Salvat ca ciornă'),
(1520, 'it', 'stesura della bozza della relazione dell\\''indicente'),
(1520, 'ro', 'completarea raportului de incident'),
(1521, 'it', 'Incidente ''{evt}'' è stato registrato con successo. \\n Per favore procedere con {evt2} .'),
(1521, 'ro', 'Incidentul ''{evt}'' a fost înregistrat cu succes.\\nPuteți începe {evt2}.'),
(1522, 'it', 'Annulla eliminazione'),
(1522, 'ro', 'Reactivează'),
(1523, 'it', 'Sei sicuro di voler eliminare DEFINITIVAMENTE {data}?'),
(1523, 'ro', 'Sunteţi sigur că vreţi să ELIMINAȚI {data}?'),
(1524, 'it', 'Eventi eliminati'),
(1524, 'ro', 'Evenimente înlăturate'),
(1525, 'it', 'Cestino'),
(1525, 'ro', 'Coș de reciclare'),
(1526, 'it', 'Lista degli eventi (cestino)'),
(1526, 'ro', 'Listă evenimente (coș de reciclare)'),
(1529, 'it', 'C''è stato un errore durante l''elaborazione della richiesta.'),
(1529, 'ro', 'S-a produs o eroare în procesarea cererii dumneavoastră.'),
(1530, 'it', 'Si prega di riprovare più tardi.'),
(1530, 'ro', 'Vă rugăm să încercați mai târziu.'),
(1532, 'it', 'aggiunto'),
(1532, 'ro', 'adăugat'),
(1534, 'it', 'eliminati'),
(1534, 'ro', 'șters / ștearsă'),
(1536, 'it', 'Ci scusiamo per eventuali disagi.'),
(1536, 'ro', 'Ne cerem scuze pentru neplăcere.'),
(1537, 'it', 'I dati forniti hanno errori di convalida.'),
(1537, 'ro', 'Datele introduse conțin erori de validare.'),
(1538, 'it', 'Si prega di rivedere e riprovare.'),
(1538, 'ro', 'Vă rugăm să le corectați și să încercați din nou.'),
(1539, 'it', 'la voce'),
(1539, 'ro', 'itemul'),
(1540, 'it', 'l''incidente'),
(1540, 'ro', 'incidentul'),
(1541, 'it', '{evt} è stata eliminata (completamente rimosso)!'),
(1541, 'ro', '{evt} a fost eliminat definitiv!'),
(1542, 'it', 'Segnalazione di evento'),
(1542, 'ro', 'Raportare eveniment'),
(1543, 'it', 'Lista delle bozze di relazione '),
(1543, 'ro', 'Lista schițelor de raport'),
(1545, 'it', 'Bozze eliminate. Fare click per gestirle.'),
(1545, 'ro', 'Copii șterse. Apăsați pentru a gestiona.'),
(1557, 'it', 'Motivo del rifiuto'),
(1557, 'ro', 'Motivația respingerii'),
(1558, 'it', 'Stato sottomissione'),
(1558, 'ro', 'Status transmitere'),
(1559, 'it', 'Liberare'),
(1559, 'ro', 'Elimină'),
(1560, 'it', 'ancora in bozza'),
(1560, 'ro', 'în curs de completare'),
(1561, 'it', '{evt} è stato con successo rimosso.'),
(1561, 'ro', '{evt} a fost șters.'),
(1562, 'it', '{evt} è stato con successo riattivato.'),
(1562, 'ro', '{evt} a fost reactivat cu succes.'),
(1565, 'it', 'Si prega di fornire una breve descrizione per questa bozza.'),
(1565, 'ro', 'Adăugați o scurtă descriere.'),
(1566, 'it', 'Più di una opzione potrebbe essere scelta'),
(1566, 'ro', 'Pot fi selectate mai multe opţiuni'),
(1567, 'it', 'Guida di selezione'),
(1567, 'ro', 'Ajutor selecție'),
(1568, 'it', 'bozza'),
(1568, 'ro', 'ciornă'),
(1569, 'it', 'Rivedere tutto'),
(1569, 'ro', 'Verifică integral'),
(1570, 'it', 'Vai a {evt}'),
(1570, 'ro', 'Deschide {evt}'),
(1576, 'it', 'Altre sezioni di questa relazione'),
(1576, 'ro', 'Alte secțiuni în acest raport'),
(1577, 'it', 'Finalizzare {evt}'),
(1577, 'ro', 'Finalizează {evt}'),
(1640, 'it', 'BLOCCATO'),
(1640, 'ro', 'BLOCAT'),
(1641, 'it', 'da {evt1} quando {evt2}'),
(1641, 'ro', 'de {evt1} începând cu {evt2}'),
(1642, 'it', 'firmato'),
(1642, 'ro', 'semnat'),
(1643, 'it', 'Modifica {evt}'),
(1643, 'ro', 'Modifică {evt}'),
(1644, 'it', 'Bozza di Eventi'),
(1644, 'ro', 'Ciorne eveniment'),
(1646, 'it', 'PARZIALMENTE FINALIZZATO'),
(1646, 'ro', 'PARȚIAL FINALIZAT'),
(1647, 'it', 'FINALIZZATO. PRONTO PER ESSERE FIRMATO'),
(1647, 'ro', 'FINALIZAT. GATA DE A FI SEMNAT.'),
(1648, 'it', 'FIRMATO. Puo'' essere inviato.'),
(1648, 'ro', 'SEMNAT. Poate fi trimis către AC.'),
(1649, 'it', 'SEGNALATO DA CA. IN ATTESA DI ACCETTAZIONE.'),
(1649, 'ro', 'RAPORTAT CĂTRE AC. ÎN AȘTEPTAREA CONFIRMĂRII DE PRIMIRE.'),
(1650, 'it', 'RIFIUTATO DALLA CA. CONTROLLA LA RAGIONE E QUINDI INVIA.'),
(1650, 'ro', 'RESPINS DE CĂTRE AC. VERIFICAȚI JUSTIFICAREA, APOI RETRANSMITEȚI.'),
(1652, 'it', 'NON INVIATO.'),
(1652, 'ro', 'NETRANSMIS'),
(1653, 'it', 'La tua relazione è stata respinta dalla Autorità Competente, con la seguente motivazione.'),
(1653, 'ro', 'Raportul dumneavoastră a fost respins de către Autoritatea Competentă, cu următoarea justificare'),
(1654, 'it', 'alle'),
(1654, 'ro', 'la'),
(1655, 'it', 'Bozze eliminate'),
(1655, 'ro', 'Copii șterse'),
(1656, 'it', 'Forza lo sblocco delle sezioni '),
(1656, 'ro', 'Deblocare forțată a secțiunilor'),
(1657, 'it', 'Le sezioni di bazza ''{ EVT }'' sono stati sbloccati . Si può procedere a modificare.'),
(1657, 'ro', 'Secțiunile draftului {evt} au fost deblocate. Puteți începe editarea.'),
(1660, 'it', 'Bloccato. Quanlcuno sta attualmente lavorando su questo.'),
(1660, 'ro', 'Blocat. Deschis pentru editare de către altcineva.'),
(1661, 'it', 'Sblocco'),
(1661, 'ro', 'Deblochează'),
(1662, 'it', 'Forza lo sblocco?'),
(1662, 'ro', 'Deblochează forțat?'),
(1664, 'it', 'Descrizione generale'),
(1664, 'ro', 'Descriere generală'),
(1665, 'it', 'esistenza di un rilascio di non-idrocarburi sostanza pericolosa'),
(1665, 'ro', 'existența unei eliberări de substanță periculoasă, alta decât hidrocarbură'),
(1666, 'it', 'quantità di rilascio'),
(1666, 'ro', 'cantitate eliberată'),
(1667, 'it', 'unità della quantità rilasciata'),
(1667, 'ro', 'cantitatea eliberată (unitate de măsură)'),
(1668, 'it', 'presenza del fuoco non idrocarburico'),
(1668, 'ro', 'prezența unui foc ce nu este cauzat de hidrocarburi'),
(1669, 'it', 'somiglianza di degrado dell''ambiente marino'),
(1669, 'ro', 'posibilitatea degradării mediului marin'),
(1670, 'it', 'impatti ambientali osservati e previsti'),
(1670, 'ro', 'impactul de mediu observat și prevăzut'),
(1671, 'it', 'Prima esperienza acquisita e raccomandazioni preliminari '),
(1671, 'ro', 'Lecţii iniţiale învăţate şi recomandări preliminarii'),
(1673, 'it', 'Finalizzare la Sezione {evt}'),
(1673, 'ro', 'Finalizare Secțiunea {evt}'),
(1674, 'it', 'Sezione {evt} non puo'' essere finalizzata! Si prega di leggere le informazioni qui di seguito e riprovare.'),
(1674, 'ro', 'Secțiunea {evt} nu poate fi finalizată! Vezi informațiile de mai jos.'),
(1676, 'it', 'Validazione della Sezione {evt} non è riuscita. Si prega di rivedere.'),
(1676, 'ro', 'Secțiunea {evt} conține erori de validare. Vezi mai jos.'),
(1677, 'it', 'Numero di lati racchiuso'),
(1677, 'ro', 'Numărul de laturi'),
(1678, 'it', 'Descrizione di altre condizioni climatiche rilevanti'),
(1678, 'ro', 'Alte condiţii meteorologice relevante'),
(1679, 'it', 'Altri mezzi di rilevamento'),
(1679, 'ro', 'Alte modalități de detecție'),
(1680, 'it', 'Descrizione della cause della perdita '),
(1680, 'ro', 'Descrierea cauzelor scurgerii'),
(1681, 'it', 'Tipo della combustione'),
(1681, 'ro', 'Tipul aprinderii'),
(1686, 'it', 'Nome dell’appaltatore incaricato della trivellazione'),
(1686, 'ro', 'Numele contractantului care desfăşoară activităţi de foraj'),
(1688, 'it', 'Sezione {evt} - Stesura della bozza'),
(1688, 'ro', 'Secțiunea {evt} - Editare'),
(1689, 'it', 'Si prega di compilare le seguenti sezioni'),
(1689, 'ro', 'Completați următoarele secțiuni'),
(1690, 'it', 'Sezione è stata con successo modificata.'),
(1690, 'ro', 'Secțiunea a fost modificată cu succes.'),
(1691, 'it', '(voce, gradi decimali)'),
(1691, 'ro', '(direcţia în grade zecimale)'),
(1694, 'it', 'riapri'),
(1694, 'ro', 'redeschide'),
(1695, 'it', 'Riaprire sezione?'),
(1695, 'ro', 'Redeschide secțiune?'),
(1697, 'it', 'Sezione {evt} è stata ''finalizzata'' con successo.'),
(1697, 'ro', 'Secțiunea {evt} a fost ''finalizată'' cu succes.'),
(1698, 'it', 'Si può procedere con la stesura restanti sezioni del relazione dell\\''incidente'),
(1698, 'ro', 'Continuați editarea celorlalte secțiuni ale raportului de incident.'),
(1699, 'it', 'La relazione dell\\''incidente ''{evt}'' è pronto per essere firmato.'),
(1699, 'ro', 'Raportul de incident ''{evt}'' este gata de a fi semnat.'),
(1700, 'it', 'Nome/codice del pozzo'),
(1700, 'ro', 'Numele/codul sondei'),
(1701, 'it', 'Data di inizio e fine/durata della perdita di controllo del pozzo'),
(1701, 'ro', 'Data de început a pierderii controlului asupra sondei'),
(1702, 'it', 'Data di inizio e fine/durata della perdita di controllo del pozzo'),
(1702, 'ro', 'Data de finalizare a pierderii controlului asupra sondei'),
(1703, 'it', 'Tipo di fluido: salamoia / petrolio / gas'),
(1703, 'ro', 'Tipul de lichid: saramură / petrol / gaz'),
(1705, 'it', 'Pressione del serbatoio'),
(1705, 'ro', 'Presiunea la nivelul rezervorului'),
(1706, 'it', 'Unità di misura della pressione del serbatoio'),
(1706, 'ro', 'Presiunea la nivelul rezervorului (unitate de măsură)'),
(1707, 'it', 'Temperatura del serbatoio'),
(1707, 'ro', 'Temperatura rezervorului'),
(1708, 'it', 'Unità di misura della temperatura del serbatoio'),
(1708, 'ro', 'Temperatura rezervorului (unitate de măsură)'),
(1709, 'it', 'Profondità del serbatoio'),
(1709, 'ro', 'Adâncimea rezervorului'),
(1710, 'it', 'Unità di misura della profondità del serbatoio'),
(1710, 'ro', 'Adâncimea rezervorului (unitate de măsură)'),
(1711, 'it', 'Tipo di manutenzione del pozzo '),
(1711, 'ro', 'Tipul de servicii la sondă'),
(1712, 'it', 'Pressione'),
(1712, 'ro', 'Presiunea'),
(1713, 'it', 'Temperatura'),
(1713, 'ro', 'Temperatura'),
(1714, 'it', 'Profondità '),
(1714, 'ro', 'Adâncime'),
(1715, 'it', 'usare Evento data ora come data di inizio'),
(1715, 'ro', 'Folosește data evenimentului ca moment de început'),
(1722, 'it', 'Unità della portata'),
(1722, 'ro', 'Debitul sondei (unitate de măsură)'),
(1726, 'it', 'Strutture di superficie'),
(1726, 'ro', 'Structuri pe puntea superioară'),
(1727, 'it', 'Fornire un valore per l''altro sistema che non è fallito.'),
(1727, 'ro', 'Oferiți o valoare pentru sistemul care a cedat.'),
(1730, 'it', 'È necessario definire gli impatti ambientali osservati.'),
(1730, 'ro', 'Impactul asupra mediului trebuie precizat.'),
(1733, 'it', 'Nome/codice della nave'),
(1733, 'ro', 'Nume/Stat de pavilion navă'),
(1734, 'it', 'Tipo/stazza della nave'),
(1734, 'ro', 'Tip/tonaj navă'),
(1735, 'it', 'Sezione modificata'),
(1735, 'ro', 'Modificare secțiune'),
(1736, 'it', 'Impossibile salvare le modifiche'),
(1736, 'ro', 'Modificările nu au putut fi salvate'),
(1737, 'it', 'La pagina richiesta non esiste.'),
(1737, 'ro', 'Pagina căutată nu există.'),
(1739, 'it', 'Data/ora di inizio e fine dell\\’evacuazione'),
(1739, 'ro', 'Data de început a evacuării '),
(1740, 'it', 'Data/ora fine dell’evacuazione'),
(1740, 'ro', 'Data de finalizare a evacuării'),
(1742, 'it', 'intero non negativo'),
(1742, 'ro', 'întreg pozitiv'),
(1743, 'it', 'per esempio: elicottero'),
(1743, 'ro', 'e.g. elicopter'),
(1744, 'it', 'aggiungi testo'),
(1744, 'ro', 'text'),
(1745, 'it', 'intero positivo'),
(1745, 'ro', 'număr întreg pozitiv'),
(1746, 'it', 'Un incidente ambientale grave'),
(1746, 'ro', 'Incident de mediu major'),
(1749, 'it', 'Sei sicuro di voler riaprire questa voce?'),
(1749, 'ro', 'Redeschideți acest item?'),
(1750, 'it', 'Firma'),
(1750, 'ro', 'Semnează'),
(1751, 'it', 'Si sta per firmare la relazione dell''incidente. Questo è l''ultimo passo prima di inviarlo alla CA.'),
(1751, 'ro', 'Sunteți pe cale să semnați raportul de incident. Acesta este ultimul pas înaintea transmiterii raportului către AC.'),
(1753, 'it', 'Modificato'),
(1753, 'ro', 'Modifică'),
(1754, 'it', 'Tenuto conto degli obblighi facenti capo agli Stati Membri di mantenere o conseguire un buono stato ecologico ai sensi della direttiva 2008/56/CE, qualora un\\’emissione accidentale di petrolio, gas o altra sostanza pericolosa o il guasto di un elemento critico per la sicurezza e l\\’ambiente degradi o sia suscettibile di degradare l\\’ambiente, tali impatti devono essere comunicati alle autorità competenti.'),
(1754, 'ro', 'Luând în considerare obligaţiile statelor membre de a menţine sau de a atinge o stare ecologică bună în conformitate cu Directiva 2008/56/CE a Parlamentului European şi a Consiliului (dacă o scurgere neintenţionată de petrol sau gaze sau alte substanţe periculoase sau defecţiunea unui element critic de siguranţă şi de mediu conduc sau pot conduce la degradarea mediului), astfel de impacturi trebuie raportate autorităţilor competente'),
(1755, 'it', 'Guasti delle barriere del pozzo ({evt})'),
(1755, 'ro', 'Barieră sondă defectă ({evt})'),
(1756, 'it', 'Ausili alla navigazione'),
(1756, 'ro', 'Ajutoare navigare'),
(1757, 'it', 'Si prega di fornire una giustificazione per la scelta per la Sezione {evt}'),
(1757, 'ro', 'Vă rugăm să oferiți o justificare pentru alegerea în cazul Secțiunii {evt}.'),
(1758, 'it', 'Firmato da'),
(1758, 'ro', 'Semnat de'),
(1759, 'it', 'Firmato alle'),
(1759, 'ro', 'Semnat la'),
(1760, 'it', 'Giustificazione di rifiuto CA'),
(1760, 'ro', 'Justificare respingere'),
(1762, 'it', 'Tutte le lesioni gravi a cinque o più	persone	nello stesso incidente da	comunicare ai sensi della Direttiva 92/91/EEC\r\n'),
(1762, 'ro', 'Orice vătămare gravă a cinci sau mai multe persoane afectate de același accident care va fi raportat în conformitate cu cerințele Directive 92/91/CEE'),
(1765, 'it', 'nessuno'),
(1765, 'ro', 'niciunul / una'),
(1766, 'it', 'Mancata conformità alla licenza'),
(1766, 'ro', 'Nerespectarea permisului de lucru'),
(1769, 'it', 'Tipo di evacuazione'),
(1769, 'ro', 'Tipul evacuării'),
(1771, 'it', 'Finalizzazione della valutazione'),
(1771, 'ro', 'Finalizare raport'),
(1772, 'it', 'La relazione dell''incidente non ha potuto essere completato! Si prega di leggere le informazioni qui di seguito e riprovare.'),
(1772, 'ro', 'Raportul de incident nu a putut fi finalizat. Vedeți informația de mai jos, apoi încercați din nou.'),
(1773, 'it', 'La relazione dell''incidente è stato finalizzato. Ora si può procedere con la firma del rapporto.'),
(1773, 'ro', 'Raportul de incident a fost finalizat. Puteți continua cu semnarea raportului.'),
(1774, 'it', 'Firmare la relazione di incidente'),
(1774, 'ro', 'Semnează raportul de incident'),
(1775, 'it', 'Segnalato da'),
(1775, 'ro', 'Raportat de'),
(1776, 'it', 'Un nuovo incidente è stato comunicato.'),
(1776, 'ro', 'Un nou incident a fost raportat.'),
(1777, 'it', 'Stato relazione'),
(1777, 'ro', 'Status raportare'),
(1778, 'it', 'Scadenza prevista'),
(1778, 'ro', 'Termen limită'),
(1779, 'it', 'Dettagli dell''incidente'),
(1779, 'ro', 'Detalii incident'),
(1780, 'it', 'Questo è un messaggio automatico. Si prega di non rispondere.'),
(1780, 'ro', 'Acesta este un mesaj automat. Vă rugăm nu răspundeți.'),
(1781, 'it', 'La relazione è stata precedentemente respinta con la seguente giustificazione'),
(1781, 'ro', 'Acest raport a fost în prealabil respins cu următoarea justificare'),
(1782, 'it', 'Nuovo incidente Offshore è stato comunicato'),
(1782, 'ro', 'Un nou incident offshore a fost raportat'),
(1783, 'it', 'Relazione {evt} è stato ripresentato.'),
(1783, 'ro', 'Incidentul {evt} a fost retransmis.'),
(1784, 'it', '(della prima presentazione)'),
(1784, 'ro', '(a transmisiei inițiale)'),
(1785, 'it', '(del primo termine di presentazione)'),
(1785, 'ro', '(în relație cu termenul limită inițial)'),
(1787, 'it', 'Aperto in modalità di sola lettura.'),
(1787, 'ro', 'Deschis în mod citire.'),
(1788, 'it', 'Bloccato da {evt1} dal {evt2}.'),
(1788, 'ro', 'Blocat de {evt1} începând cu {evt2}.'),
(1792, 'it', 'salamoia'),
(1792, 'ro', 'saramură'),
(1793, 'it', 'petrolio'),
(1793, 'ro', 'petrol'),
(1794, 'it', 'gas'),
(1794, 'ro', 'gaz'),
(1796, 'it', 'in superficie'),
(1796, 'ro', 'suprafaţă'),
(1797, 'it', 'sottomarina'),
(1797, 'ro', 'submarin'),
(1798, 'it', 'produzione normale'),
(1798, 'ro', 'lucrări normale de producţie'),
(1799, 'it', 'operazioni di ripresa'),
(1799, 'ro', 'lucrări de intervenţie'),
(1800, 'it', 'operazioni di manutenzione '),
(1800, 'ro', 'servicii la sondă'),
(1801, 'it', 'perforazione'),
(1801, 'ro', 'forare'),
(1802, 'it', 'carotaggio a fune'),
(1802, 'ro', 'cablu de oţel'),
(1803, 'it', 'coiled tubing'),
(1803, 'ro', 'tubing flexibil'),
(1804, 'it', 'manovra in pressione\r\n'),
(1804, 'ro', 'introducerea ţevilor sub presiune'),
(1806, 'it', 'SEZIONE {evt}'),
(1806, 'ro', 'SECŢIUNEA {evt}'),
(1808, 'it', 'Per uscire da questa pagina si prega di utilizzare i pulsanti nella parte inferiore della pagina.'),
(1808, 'ro', 'Pentru a părăsi această pagină utilizați butoanele de la capătul acesteia.'),
(1809, 'it', 'Inviare la Relazione dell''Incidente alla CA'),
(1809, 'ro', 'Trimite raport de incident către AC'),
(1810, 'it', 'revisionare'),
(1810, 'ro', 'vezi'),
(1812, 'it', 'Inviare la relazione di incidente'),
(1812, 'ro', 'Trimite Raportul de Incident'),
(1813, 'it', 'Si sta per presentare la Relazione dell''Incidente all''Autorità Competente'),
(1813, 'ro', 'Sunteți pe cale să trimiteți raportul de incident către Autoritatea Competentă'),
(1814, 'it', 'Si prega di leggere attentamente le informazioni qui sotto e scegliere Invia, se siete d''accordo , altrimenti Annullare.'),
(1814, 'ro', 'Citiți cu atenție informațiile din această pagină. Alegeți Trimite dacă sunteți de acord sau Renunță, în caz contrar.'),
(1815, 'it', 'Cosa succede dopo'),
(1815, 'ro', 'Ce se va întâmpla'),
(1816, 'it', 'Una volta che la relazione dell''incidente è stata presentata:'),
(1816, 'ro', 'Odată ce raportul de incident este transmis:'),
(1817, 'it', 'L''Autorità Competente è informata circa l''evento;'),
(1817, 'ro', 'Autoritatea Competentă este informată despre producerea evenimentului;'),
(1818, 'it', 'È (o qualsiasi membro della vostra organizzazione):'),
(1818, 'ro', 'Orice membru al organizației dumneavoastră:'),
(1819, 'it', '<b>potrà</b> essere visualizzato l''incidente comunicato'),
(1819, 'ro', '<b>va avea acces</b> la raportul de incident'),
(1823, 'it', 'Informazioni da presentare'),
(1823, 'ro', 'Informația ce va fi trimisă'),
(1824, 'it', 'Seleziona bozza da inviare'),
(1824, 'ro', 'Varianta de raport selectată'),
(1825, 'it', '<b>non sarà </b> in grado di modificare le informazioni comunicate\r\n\r\n'),
(1825, 'ro', '<b>nu</b> va putea modifica informația raportată'),
(1826, 'it', '<b>non sarà </b> in grado di eliminare le informazioni comunicate\r\n'),
(1826, 'ro', '<b>nu</b> va putea șterge informația raportată'),
(1827, 'it', 'a) contattare l''amministratore di sistema (tramite SyRIO)'),
(1827, 'ro', 'a) contactați administratorul de sistem (utilizând SyRIO)'),
(1828, 'it', 'non sarà inviato.'),
(1828, 'ro', 'nu va fi transmis'),
(1829, 'it', 'Dettagli della persona che comunica l''evento saranno aggiornati dalle tue informazioni del tuo account!'),
(1829, 'ro', 'Detaliile referitoare la persoana care raportează evenimentul vor fi actualizate cu datele provenite din contul dumneavoastră de utilizator!'),
(1832, 'it', 'alla scadenza'),
(1832, 'ro', 'până la expirarea termenului limită'),
(1834, 'it', '{evt} E'' STATO PRESENTATO ALLA AUTORITA'' COMPETENTE.'),
(1834, 'ro', '{evt} A FOST TRANSMIS CĂTRE AUTORITATEA COMPETENTĂ.'),
(1835, 'it', 'non c''è bisogno di valutare. Relazione non sottoposte a valutazione come MA.'),
(1835, 'ro', 'evaluarea nu este necesară; raportul nu este classificat din punct de vedere al Accidentului Major.'),
(1836, 'it', 'deve essere valutata prima'),
(1836, 'ro', 'trebuie să fie evaluat în prealabil'),
(1837, 'it', 'Gestione'),
(1837, 'ro', 'Gestiune'),
(1839, 'it', 'categorizzazione dell''evento'),
(1839, 'ro', 'clasificarea evenimentului'),
(1840, 'it', 'Modificato'),
(1840, 'ro', 'Modificat'),
(1841, 'it', 'Fax'),
(1841, 'ro', 'Fax'),
(1842, 'it', 'Telefono (alt.)'),
(1842, 'ro', 'Telefon (alt.)'),
(1843, 'it', 'Sito internet'),
(1843, 'ro', 'Adresă internet'),
(1844, 'it', 'Nome completo'),
(1844, 'ro', 'Nume complet'),
(1847, 'it', 'Inserire il numero di telefono'),
(1847, 'ro', 'Introduceți numărul de telefon'),
(1848, 'it', 'Numero di telefono alternativo (opzionale)'),
(1848, 'ro', 'Număr de telefon alternativ (opțional)'),
(1849, 'it', 'Inserire il numero di fax (opzionale)'),
(1849, 'ro', 'Introduceți numărul de fax (opțional)'),
(1850, 'it', 'Inserire l''indirizzo email'),
(1850, 'ro', 'Introduceți adresa de email'),
(1851, 'it', 'Inserisci la Homepage dell''organizzazione (opzionale)'),
(1851, 'ro', 'Introduceți adresa de internet a organizației (opțional)'),
(1852, 'it', 'Inserire il nome della via'),
(1852, 'ro', 'Introduceți strada'),
(1853, 'it', 'Via (1)'),
(1853, 'ro', 'Strada1'),
(1854, 'it', 'Via (2)'),
(1854, 'ro', 'Strada2'),
(1855, 'it', 'Inserire il nome della via (riga 2, opzionale)'),
(1855, 'ro', 'Introduceți strada (linia 2, optional)'),
(1856, 'it', 'Inserire la Città'),
(1856, 'ro', 'Introduceți localitatea'),
(1857, 'it', 'Inserire la nazione (opzionale)'),
(1857, 'ro', 'Introduceți regiune/județ/sector (opțional)'),
(1859, 'it', 'Inserire il C.A.P. (opzionale)'),
(1859, 'ro', 'Introduceți codul poștal (opțional)'),
(1860, 'it', 'inserisci nome della via, nr ...'),
(1860, 'ro', 'introduceți numele și numărul străzii, etc.'),
(1861, 'it', 'Ricerca'),
(1861, 'ro', 'Caută'),
(1862, 'it', 'Impianti in giurisdizione'),
(1862, 'ro', 'Instalații în jurisdicție'),
(1867, 'it', 'Mobile'),
(1867, 'ro', 'Instalație mobilă'),
(1868, 'it', 'Nome regola'),
(1868, 'ro', 'Nume regulă'),
(1869, 'it', 'Tipo di smantellamento'),
(1869, 'ro', 'Tip dezafectare'),
(1870, 'it', 'Indirizzi'),
(1870, 'ro', 'Adrese'),
(1871, 'it', 'Valutare'),
(1871, 'ro', 'Evaluează'),
(1872, 'it', 'Rifiutare'),
(1872, 'ro', 'Respinge'),
(1874, 'it', 'inserisci testo...'),
(1874, 'ro', 'introduceți textul...'),
(1875, 'it', 'Si prega di fornire una giustificazione per respingere la relazione dell''incidente .'),
(1875, 'ro', 'Oferiți o justificare pentru respingerea acestui raport.'),
(1876, 'it', 'La vostra giustificazione verrà inviata al Operatore/proprietario che ha presentato la relazione.'),
(1876, 'ro', 'Justificarea va fi transmisă Operatorului/proprietarului care a înaintat acest raport.'),
(1877, 'it', 'Respingere la relazione di incidente!'),
(1877, 'ro', 'Respinge raport de incident'),
(1878, 'it', 'La giustificazione deve essere fornita quando viene rifiutata una relazione dell''incidente.'),
(1878, 'ro', 'Trebuie oferită o justificare în momentul respingerii unui raport.'),
(1879, 'it', 'Questa relazione è stata respinta con la seguente giustificazione:'),
(1879, 'ro', 'Acest raport a fost respins cu următoarea justificare:'),
(1880, 'it', 'Relazione di incidente respinto!'),
(1880, 'ro', 'Raport de incident respins!'),
(1881, 'it', 'Hai respinto la relazione dell''incidente.'),
(1881, 'ro', 'Raportul de incident a fost respins.'),
(1882, 'it', 'aperto'),
(1882, 'ro', 'deschis'),
(1883, 'it', 'accettato'),
(1883, 'ro', 'acceptat'),
(1884, 'it', 'NON è valutata'),
(1884, 'ro', 'NU este evaluat'),
(1885, 'it', 'E'' STATA VALUTATA'),
(1885, 'ro', 'A FOST EVALUAT'),
(1892, 'it', 'Vecchia password'),
(1892, 'ro', 'Parola veche'),
(1893, 'it', 'Nuova password'),
(1893, 'ro', 'Noua parolă'),
(1894, 'it', 'Ripeti password'),
(1894, 'ro', 'Repetați parola'),
(1895, 'it', 'Nuovo nome utente'),
(1895, 'ro', 'Nume utilizator nou'),
(1899, 'it', 'Finalizzato e firmato automaticamente.'),
(1899, 'ro', 'Finalizat și semnat prin definiție.'),
(1901, 'it', 'Bozza della relazione di indicente ''{ EVT }'' è stato riaperto. Ora si può procedere con la modifica di esso.'),
(1901, 'ro', 'Raportul de incident {evt} a fost REDESCHIS. Puteți trece la modificarea acestuia.'),
(1903, 'it', 'Sezione {evt1} di ''{evt2}'' è stato RIAPERTO. Ora si può procedere con la sua modifica.'),
(1903, 'ro', 'Secțiunea {evt1} a ciornei ''{evt2}'' a fost REDESCHISĂ. Puteți trece la modificarea acesteia.'),
(1904, 'it', 'Incidente Offshore ripresentato '),
(1904, 'ro', 'Incident offshore retransmis'),
(1905, 'it', 'Questa relazione è stata ri-presentata. Giustificazione di rifiuto precedente:'),
(1905, 'ro', 'Acest raport a fost re-transmis. Justificarea respingerii a fost:'),
(1906, 'it', 'Creato'),
(1906, 'ro', 'Generat'),
(1907, 'it', 'Area riservata'),
(1907, 'ro', 'Zonă restricționată'),
(1908, 'it', 'Non si dispone di sufficienti credenziali per accedere a questa sezione.'),
(1908, 'ro', 'Nu aveţi suficiente drepturi utilizator pentru a accesa această secțiune.'),
(1910, 'it', '{evt1} per {evt2}'),
(1910, 'ro', '{evt1} pentru {evt2}'),
(1911, 'it', 'Quali sono le principali esperienze acquisite in seguito agli incidenti degne di essere condivise?'),
(1911, 'ro', 'Care sunt cele mai importante lecţii învăţate în urma incidentelor, care merită să fie împărtăşite?'),
(1912, 'it', 'Perdita di stabilità/galleggiamento'),
(1912, 'ro', 'Pierderea stabilităţii / flotabilităţii'),
(1913, 'it', 'Richiesta di registrazione dell''utente'),
(1913, 'ro', 'Cerere de înregistrare utilizator'),
(1914, 'it', 'Hai una richiesta in attesa.'),
(1914, 'ro', 'O nouă cerere a fost înaintată.'),
(1915, 'it', 'Si prega di notare che è necessario effettuare l''accesso per visualizzare la richiesta.'),
(1915, 'ro', 'Pentru a vizualiza solicitarea trebuie să fiți autentificat.'),
(1918, 'it', 'Grazie.'),
(1918, 'ro', 'Mulțumim.'),
(1919, 'it', 'La tua richiesta è stata inviata!'),
(1919, 'ro', 'Cerea dumneavoastră a fost transmisă!'),
(1920, 'it', 'Si prega di controllare regolarmente la tua email.'),
(1920, 'ro', 'Vă rugăm să vă verificați poșta electronică (inclusiv directorul spam) în mod frecvent.'),
(1921, 'it', 'Richiesta di registrazione degli Operatori/Proprietari utente'),
(1921, 'ro', 'Solicitare înregistrare utilizator de tip Operatori/Proprietari'),
(1922, 'it', 'Richiesta di registrazione dell''Autorità Competente utente '),
(1922, 'ro', 'Solicitare înregistrare utilizator de tip Autoritate Competentă'),
(1923, 'it', 'Richiesta di registrazione dell''impianto utente'),
(1923, 'ro', 'Solicitare înregistrare utilizator de tip Instalație'),
(1924, 'it', '[SyRIO] - Richiesta #{evt1} - {evt2}  richiesta di registrazione utente'),
(1924, 'ro', '[SyRIO] Cerere #{evt1} - solicitare înregistrare utilizator {evt2}'),
(1925, 'it', 'reimpostazione della password'),
(1925, 'ro', 'resetare parolă'),
(1927, 'it', '[SyRIO] - Richiesta #{evt1} - {evt2} richiesta di registrazione');
INSERT INTO `t_message` (`id`, `language`, `translation`) VALUES
(1927, 'ro', '[SyRIO] Cerere #{evt1} - solicitare înregistrare {evt2}'),
(1928, 'it', '[SyRIO] - Registrazione utente confermato'),
(1928, 'ro', '[SyRIO] - Înregistrare utilizator confirmată'),
(1929, 'it', '[SyRIO] - INCIDENTE OFFSHORE SEGNALATO - {evt1}/{evt2}/{evt3} '),
(1929, 'ro', '[SyRIO] INCIDENT OFFSHORE RAPORTAT - {evt1}/{evt2}/{evt3}'),
(1930, 'it', '[SyRIO] - INCIDENTE OFFSHORE RIPRESENTATO - {evt1}/{evt2}/{evt3} '),
(1930, 'ro', '[SyRIO] INCIDENT OFFSHORE RETRANSMIS - {evt1}/{evt2}/{evt3}'),
(1931, 'it', 'Richiesta #{evt} è stata eliminata.'),
(1931, 'ro', 'Solicitarea #{evt} a fost ștearsă.'),
(1932, 'it', 'Elimina richiesta'),
(1932, 'ro', 'Șterge solicitare'),
(1933, 'it', 'Lista delle richieste'),
(1933, 'ro', 'Listă solicitări'),
(1934, 'it', 'Tipo della richiesta'),
(1934, 'ro', 'Tip solicitare'),
(1935, 'it', 'Da'),
(1935, 'ro', 'De la'),
(1936, 'it', 'Contenuto'),
(1936, 'ro', 'Conținut'),
(1937, 'it', 'Elaborato da ID'),
(1937, 'ro', 'Procesat de ID'),
(1938, 'it', 'Elaborato alle'),
(1938, 'ro', 'Procesat la'),
(1939, 'it', 'Giustificazione'),
(1939, 'ro', 'Justificare'),
(1941, 'it', 'Questo riattiverà {data}.'),
(1941, 'ro', 'Această acțiune va reactiva {data}.'),
(1944, 'it', 'Ambito'),
(1944, 'ro', 'Domeniu'),
(1946, 'it', 'metrico'),
(1946, 'ro', 'metric'),
(1947, 'it', 'imperiale'),
(1947, 'ro', 'Imperial'),
(1948, 'it', 'massa'),
(1948, 'ro', 'masă'),
(1949, 'it', 'tasso (massa)'),
(1949, 'ro', 'debit (masă)'),
(1950, 'it', 'velocità'),
(1950, 'ro', 'viteză'),
(1951, 'it', 'lunghezza'),
(1951, 'ro', 'lungime'),
(1952, 'it', 'volume di metrica'),
(1952, 'ro', 'volum (metric)'),
(1953, 'it', 'Unità dell''energia'),
(1953, 'ro', 'unitate de energie'),
(1954, 'it', 'Fattore di moltiplicazione'),
(1954, 'ro', 'Factor multiplicativ'),
(1956, 'it', 'Espressione'),
(1956, 'ro', 'Expresie'),
(1957, 'it', 'Classe di conversione'),
(1957, 'ro', 'Clasă de tip conversie'),
(1958, 'it', 'Unità'),
(1958, 'ro', 'Unitate de măsură'),
(1959, 'it', 'Fattore di somma'),
(1959, 'ro', 'Factor aditiv'),
(1960, 'it', 'tempo'),
(1960, 'ro', 'timp'),
(1961, 'it', 'pressione'),
(1961, 'ro', 'presiune'),
(1962, 'it', 'temperatura'),
(1962, 'ro', 'temperatură'),
(1963, 'it', 'Per motivi di stabilità del sistema questa opzione è disabilitata nella versione corrente di SyRIO.'),
(1963, 'ro', 'Din motive de stabilitate a aplicației această opțiune este dezafectată în versiunea curentă de SyRIO.'),
(1964, 'it', 'Impossibile eliminare (evt)!'),
(1964, 'ro', '{evt} nu a putut fi ștearsă!'),
(1972, 'it', 'Finalizzare la registrazione'),
(1972, 'ro', 'Finalizați înregistrarea'),
(1973, 'it', 'Benvenuti in SyRIO. Poiché questa è la prima volta che si accede, si prega di fornire una password a vostro piacimento per attivare l''account .'),
(1973, 'ro', 'Bine ați venit în SyRIO. Cum aceasta este prima dumneavoastră autentificare, vă rugăm să alegeți o nouă parolă de acces; aceasta va însemna și activarea contului dumneavoastră.'),
(1974, 'it', 'Attiva account'),
(1974, 'ro', 'Activați contul'),
(1975, 'it', 'Aggiungi utente'),
(1975, 'ro', 'Adaugă utilizator'),
(1980, 'it', 'Letti'),
(1980, 'ro', 'Paturi'),
(1982, 'it', 'Fisso'),
(1982, 'ro', 'Fixă'),
(1983, 'it', 'Tipo operazione'),
(1983, 'ro', 'Tipul operației'),
(1990, 'it', '{action} {evt} utente'),
(1990, 'ro', '{action} utilizator {evt}'),
(1991, 'it', 'Modifica Utente'),
(1991, 'ro', 'Modifică utilizator'),
(1995, 'it', 'selezionare il tipo dell''utente'),
(1995, 'ro', 'selectați tipul'),
(1996, 'it', 'Si prega di selezionare il tipo di utente che si desidera creare.'),
(1996, 'ro', 'Selectați tipul de utilizator pe care doriți sa-l înregistrați.'),
(1997, 'it', 'Seguente'),
(1997, 'ro', 'Următorul'),
(2002, 'it', 'Registra nuovo utente'),
(2002, 'ro', 'Înregistrare utilizator nou'),
(2004, 'it', 'Utente {user} è stato creato e registrato con {org}.'),
(2004, 'ro', 'Utilizatorul {user} a fost creat și înregistrat ca membru al {org}.'),
(2006, 'it', 'L''utente è stato notificato tramite e-mail.'),
(2006, 'ro', 'Utilizatorul a fost notificat prin email.'),
(2015, 'it', 'Visibilità'),
(2015, 'ro', 'Vizibilitate'),
(2016, 'it', 'evento registrato in SyRIO'),
(2016, 'ro', 'eveniment înregistrat'),
(2017, 'it', 'evento finalizzato'),
(2017, 'ro', 'eveniment finalizat'),
(2018, 'it', 'evento firmato'),
(2018, 'ro', 'eveniment semnat'),
(2019, 'it', 'evento riaperto'),
(2019, 'ro', 'eveniment redeschis'),
(2020, 'it', 'evento inviato'),
(2020, 'ro', 'eveniment transmis'),
(2021, 'it', 'evento rinviato'),
(2021, 'ro', 'eveniment re-transmis'),
(2022, 'it', 'evento rifiutato'),
(2022, 'ro', 'eveniment respins'),
(2023, 'it', 'evento accettato'),
(2023, 'ro', 'eveniment acceptat'),
(2024, 'it', 'evento valutato'),
(2024, 'ro', 'eveniment evaluat'),
(2025, 'it', 'evento valutato per CPF'),
(2025, 'ro', 'eveniment evaluat pentru CPF'),
(2026, 'it', 'sezione riaperta'),
(2026, 'ro', 'secțiune redeschisă'),
(2027, 'it', '{n, plural, =1{Vacanza} other{Vacanze}}'),
(2027, 'ro', '{n, plural, =1{Zi nelucrătoare} other{Zile nelucrătoare}}'),
(2028, 'it', 'Giorno'),
(2028, 'ro', 'Ziua'),
(2029, 'it', 'Mese'),
(2029, 'ro', 'Luna'),
(2030, 'it', 'Ripetere ogni anno'),
(2030, 'ro', 'Recurentă în fiecare an'),
(2031, 'it', 'Individuale'),
(2031, 'ro', 'O singură dată'),
(2032, 'it', 'Istoric raportare'),
(2032, 'ro', 'Istoric raportare'),
(2033, 'it', 'Evento rivalutato con successo!'),
(2033, 'ro', 'Eveniment reevaluat cu succes!'),
(2034, 'it', 'Salvare questo come bozza porterà alla rimozione del report dall'' elenco di preparazione CPF . Procedere?'),
(2034, 'ro', 'Salvarea ca ciornă va conduce la ELIMINAREA ACESTUI CAZ din lista rapoartelor pregătite pentru FCP. Continuați?'),
(2035, 'it', 'CPF Valutati'),
(2035, 'ro', 'CPF evaluat'),
(2036, 'it', 'Le modifiche implicano l''aggiornamento della Valutazione Annuale CPF (per {evt}).'),
(2036, 'ro', 'Modificările implică actualizarea raportului anual (pentru {evt}).'),
(2037, 'it', 'Procedere di conseguenza.'),
(2037, 'ro', 'Vă rugăm să acționați în consecință.'),
(2038, 'it', 'sospeso'),
(2038, 'ro', 'suspendat'),
(2039, 'it', 'Aggiornare dati della Sezione 4'),
(2039, 'ro', 'Actualizați datele Secțiunii 4'),
(2040, 'it', 'Relazione Annuale è sincronizzato con gli incidenti comunicati.'),
(2040, 'ro', 'Raportul anual este sincornizat cu lista incidentelor raportate.'),
(2041, 'it', 'Relazione Annuale non è sincronizzato con gli incidenti comunicati.'),
(2041, 'ro', 'Raportul anual NU ESTE sincornizat cu lista incidentelor raportate.'),
(2042, 'it', 'Redattore {evt} '),
(2042, 'ro', 'Editor {evt}'),
(2043, 'it', 'I cambiamenti sono stati rilevati nella lista delle valutazioni per l''anno in corso.'),
(2043, 'ro', 'Au fost detectate modificări în lista rapoartelor din acest an.'),
(2044, 'it', 'Per favore:'),
(2044, 'ro', 'Vă rugăm:'),
(2045, 'it', 'per mantenere la Relazione Annuale aggiornata.'),
(2045, 'ro', 'pentru a menține Raportul Anual actualizat.'),
(2046, 'it', 'I dati nella Sezione 4 sono stati aggiornati con successo dalla lista incidenti'),
(2046, 'ro', 'Actualizarea datelor din Secțiunea 4 cu informațiile din lista de incidente a fost efectuată cu succes '),
(2047, 'it', 'Dati'),
(2047, 'ro', 'Informație'),
(2048, 'it', 'Tu devi specificare il fallimento OPERATIVO'),
(2048, 'ro', 'Eroarea OPERAȚIONALĂ trebuie specificată'),
(2049, 'it', 'Tu devi specificare il fallimento delle ATTREZZATURE'),
(2049, 'ro', 'Eroarea DE ECHIPAMENT trebuie specificată'),
(2050, 'it', 'Tu devi specificare il fallimento della PROCEDURA'),
(2050, 'ro', 'Eroarea PROCEDURALĂ trebuie specificată'),
(2051, 'it', 'Tu devi specificare il fallimento delle emissioni NON DI PROCESSO'),
(2051, 'ro', 'Specificați tipul deversării ÎN AFARA PROCESULUI'),
(2052, 'it', 'Tu devi specificare il fallimento di altri  MEZZI DI RILEVAMENTO'),
(2052, 'ro', 'Trebuie specificată MODALITATEA DE DETECȚIE'),
(2053, 'it', 'Numero di incidenti'),
(2053, 'ro', 'Număr de incidente'),
(2054, 'it', '(numero/ore lavorate)'),
(2054, 'ro', '(număr/ore lucrate)'),
(2055, 'it', '(numero/kTOE)'),
(2055, 'ro', '(număr/kTOE)'),
(2056, 'it', 'Questo cancellerà tutti i dati nella valutazione corrente.'),
(2056, 'ro', 'Această acțiune va reseta întreaga informație conținută în raport.'),
(2057, 'it', 'Le singole valutazioni del CRF in termini di sezione 4 SARANNO CONSERVATE.'),
(2057, 'ro', 'Evaluările individuale ale rapoartelor în termenii Secțiunii 4 VOR FI PĂSTRATE.'),
(2058, 'it', 'avvertimento'),
(2058, 'ro', 'ATENȚIE'),
(2059, 'it', 'La valutazione corrente verrà COMPLETAMENTE eliminata.'),
(2059, 'ro', 'Această acțiune va ELIMINA COMPLET raportul curent.'),
(2060, 'it', 'Le singole valutazioni del CRF in termini di sezione 4 VERRANNO ANCHE ELIMINATE!'),
(2060, 'ro', 'Evaluările individuale ale rapoartelor în termenii Secțiunii 4 VOR FI, DE ASEMENEA, ȘTERSE!'),
(2061, 'it', 'Azzerrare la Valutazione Annuale'),
(2061, 'ro', 'Resetarea raportului anual'),
(2062, 'it', 'La Relazione Annuale {evt}  è stata azzerata (tutti i dati sono stati cancellati)'),
(2062, 'ro', 'Raportul anual pentru anul {evt} a fost resetat (întreaga informație a fost anulată)'),
(2063, 'it', 'La Relazione Annuale {evt}  è stata eliminata (i file di preparazione della relazione e degli incidenti individuali sono stati cancellati)'),
(2063, 'ro', 'Raportul anual pentru {evt} a fost eliminat (raportul propriu-zis și evaluarea individuală a rapoartelor CRF)'),
(2064, 'it', 'Riapertura dela valutazione può avere effetti a cascata sulla Relazione Annuale.'),
(2064, 'ro', 'Redeschiderea raportului de evaluare poate avea consecințe asupra Raportului Anual'),
(2065, 'it', 'Modificare {evt}'),
(2065, 'ro', 'Editează {evt}'),
(2066, 'it', 'Anno di installazione'),
(2066, 'ro', 'Anul instalației'),
(2069, 'it', 'installazione comunicate'),
(2069, 'ro', 'instalație raportată'),
(2071, 'it', 'Questa installazione ha almeno un incidente registrato nel periodo di riferimento.'),
(2071, 'ro', 'Există cel puțin un incident raportat de aceată instalație în perioada de raportare.'),
(2073, 'it', 'Lasciare vuoto nel caso di un impianto operativo'),
(2073, 'ro', 'Lăsați gol în cazul unei instalații operaționale'),
(2074, 'it', 'Petrolio/Condensato'),
(2074, 'ro', 'Petrol/Condensat'),
(2075, 'it', 'Petrolio/Gas'),
(2075, 'ro', 'Petrol/Gaz'),
(2076, 'it', 'Petrolio'),
(2076, 'ro', 'Petrol'),
(2077, 'it', 'Gas'),
(2077, 'ro', 'Gaz'),
(2078, 'it', 'Condensato'),
(2078, 'ro', 'Condensat'),
(2079, 'it', 'Non applicabile'),
(2079, 'ro', 'nu se aplică'),
(2082, 'it', '(Barrare la casella corrispondente)'),
(2082, 'ro', '(Bifați căsuța relevantă)'),
(2083, 'it', 'per favore specificare'),
(2083, 'ro', 'specificați'),
(2085, 'it', 'ppm stimato'),
(2085, 'ro', 'ppm estimat'),
(2087, 'it', 'Modulo di ventilazione?'),
(2087, 'ro', 'Ventilație modul?'),
(2088, 'it', 'Immediata/Differita'),
(2088, 'ro', 'Imediat/Cu întârziere'),
(2089, 'it', 'Quali misure di emergenza sono state adottate'),
(2089, 'ro', 'Ce măsură de siguranță a fost luată'),
(2090, 'it', 'C02/Halon/inerti'),
(2090, 'ro', 'CO2/Halon/Gaze inerte'),
(2091, 'it', '(a) Progettazione:'),
(2091, 'ro', '(a) Proiectare:'),
(2092, 'it', '(b) Attrezzatura:'),
(2092, 'ro', '(b) Echipament:'),
(2093, 'it', '(c) Funzionamento:'),
(2093, 'ro', '(c) Operare:'),
(2094, 'it', '(d) Procedura:'),
(2094, 'ro', '(d) Procedural:'),
(2095, 'it', 'Precisare il tipo di operazione, per es. carotaggio a fune, prova di pozzo ecc.'),
(2095, 'ro', 'specificați operațiunea reală, de exemplu cablu de oțel, test sondă, etc.'),
(2096, 'it', 'Precisare le unità'),
(2096, 'ro', 'specificați unitățile'),
(2097, 'it', 'tonnellate, kg, Nm3'),
(2097, 'ro', 'tone, kg, Nm3'),
(2099, 'it', 'tonnellate/giorno , kg/s , Nm3/s'),
(2099, 'ro', 'tone/zi, kg/s, Nm3/s'),
(2101, 'it', '(Tempo stimato dalla scoperta  per esempio allarmi, registrazione in formato elettronico, fino alla fine della perdita)'),
(2101, 'ro', '(Interval estimat de la descoperire, de exemplu, alarmă, jurnal electronic, până la terminarea scurgerii)'),
(2104, 'it', '(ossia la zona nella posizione di incidente)'),
(2104, 'ro', 'și anume zona de la locul incidentului'),
(2106, 'it', 'Inserire il numero di pareti, tra pavimento e soffitto'),
(2106, 'ro', 'Introduceți numărul de pereți, inclusive podea și plafon'),
(2109, 'it', 'Specificare voce in gradi'),
(2109, 'ro', 'Specificați direcția in grade'),
(2113, 'it', 'Si prega di contrassegnare il tipo di rilevatore o specificare'),
(2113, 'ro', 'Vă rugăm bifați tipul de detector sau specificați, după caz'),
(2115, 'it', 'Cause'),
(2115, 'ro', 'Cauza'),
(2116, 'it', 'Si prega di dare una breve descrizione e compilare la lista di controllo ''{evt}'' di seguito'),
(2116, 'ro', 'Vă rugăm oferiți o scurtă descriere și completați lista de verificare "Cauza" de mai jos'),
(2117, 'it', 'elenco cause della perdita'),
(2117, 'ro', 'LISTĂ DE VERIFICARE PENTRU CAUZA SCURGERII'),
(2120, 'it', 'Selezionare un parametro fra le seguenti categorie e barrare le caselle corrispondenti'),
(2120, 'ro', 'Selectați un parametru dintre următoarele categorii și selectați căsuțele relevante'),
(2121, 'it', 'Modalità operativa nella zona al momento dell’emissione'),
(2121, 'ro', 'Modul operational în zonă, la momentul deversării'),
(2123, 'it', 'Si prega di barrare la casella corrispondente'),
(2123, 'ro', 'Vă rugăm bifați căsuța relevantă'),
(2132, 'it', 'In caso affermativo, si prega di precisare il tipo e il quantitativo di sostanza emessa:'),
(2132, 'ro', 'Dacă da, specificați tipul și cantitatea substanței deversate:'),
(2138, 'it', 'entro 10 giorni lavorativi dall’evento'),
(2138, 'ro', 'în termen de 10 zile lucrătoare de la producerea evenimentului'),
(2150, 'it', 'se applicabile'),
(2150, 'ro', 'dacă este aplicabil'),
(2168, 'it', '(distanza minima fra la nave e l’impianto, rotta e velocità della nave, e le condizioni meteorologiche)'),
(2168, 'ro', '(distanța minima între navă și instalație, cursul și viteza navei, condiții meteorologice)'),
(2179, 'it', 'tranne se già riportato in una precedente sezione della relazione'),
(2179, 'ro', ', dacă acest lucru nu a fost deja raportat într-o secţiune anterioară a acestui raport'),
(2184, 'it', 'Illustrare gli effetti negativi reali o potenziali sull’ambiente.'),
(2184, 'ro', 'Care sunt sau sunt posibile să fie efectele adverse semnificative asupra mediului'),
(2187, 'it', 'Relazione SyRIO'),
(2187, 'ro', 'raport SyRIO'),
(2189, 'it', 'A'),
(2189, 'ro', 'A'),
(2190, 'it', 'B'),
(2190, 'ro', 'B'),
(2191, 'it', 'C'),
(2191, 'ro', 'C'),
(2192, 'it', 'D'),
(2192, 'ro', 'D'),
(2193, 'it', 'E'),
(2193, 'ro', 'E'),
(2194, 'it', 'F'),
(2194, 'ro', 'F'),
(2195, 'it', 'G'),
(2195, 'ro', 'G'),
(2196, 'it', 'H'),
(2196, 'ro', 'H'),
(2197, 'it', 'I'),
(2197, 'ro', 'I'),
(2198, 'it', 'J'),
(2198, 'ro', 'J'),
(2200, 'it', 'Barrare la casella corrispondente'),
(2200, 'ro', 'Bifați căsuța relevantă'),
(2201, 'it', 'ad esempio'),
(2201, 'ro', 'de exemplu'),
(2204, 'it', 'o altro'),
(2204, 'ro', 'sau altele'),
(2205, 'it', 'In caso affermativo, illustrare gli impatti ambientali già osservati o che sono conseguenza dall’incidente'),
(2205, 'ro', 'Dacă da, precizaţi impactul asupra mediului observat deja sau care este susceptibil de a rezulta în urma incidentului'),
(2206, 'it', 'Clicca le sezioni di seguito per la redazione o restringendo le informazioni visualizzate.'),
(2206, 'ro', 'Selectați secțiunile de mai jos pentru a începe editarea sau pentru a limita informația afișată.'),
(2208, 'it', '(a)'),
(2208, 'ro', '(a)'),
(2209, 'it', '(b)'),
(2209, 'ro', '(b)'),
(2210, 'it', '(c)'),
(2210, 'ro', '(c)'),
(2211, 'it', '(d)'),
(2211, 'ro', '(d)'),
(2212, 'it', '(e)'),
(2212, 'ro', '(e)'),
(2213, 'it', '(f)'),
(2213, 'ro', '(f)'),
(2214, 'it', '(g)'),
(2214, 'ro', '(g)'),
(2215, 'it', '(h)'),
(2215, 'ro', '(h)'),
(2216, 'it', '(i)'),
(2216, 'ro', '(i)'),
(2217, 'it', '(j)'),
(2217, 'ro', '(j)'),
(2218, 'it', '(k)'),
(2218, 'ro', '(k)'),
(2219, 'it', '(l)'),
(2219, 'ro', '(l)'),
(2220, 'it', 'INCIDENTI CHE COINVOLGONO ELICOTTERI, SULL’IMPIANTO IN MARE O NELLE SUE VICINANZE'),
(2220, 'ro', 'ACCIDENTE IMPLICÂND ELICOPTERE CARE SE PRODUC PE INSTALAȚIILE PETROLIERE SAU ÎN APROPRIEREA ACESTORA'),
(2222, 'it', 'Incidenti aperti Hub'),
(2222, 'ro', 'Deschide secțiunea incidente'),
(2223, 'it', 'bozze'),
(2223, 'ro', 'ciorne'),
(2224, 'it', 'CA accettato'),
(2224, 'ro', 'acceptat de către AC'),
(2225, 'it', 'CA respinto'),
(2225, 'ro', 'respins de către AC'),
(2226, 'it', 'CA in sospeso'),
(2226, 'ro', 'în așteptarea răspunsului de la AC'),
(2232, 'it', 'Questo corrisponde a {evt1} e {evt2} sezioni del Formato Comune per Comunicare i dati relativi agli incidenti (EU IR 1112/2014)'),
(2232, 'ro', 'Infomațiile de mai jos corespund secțiunilor {evt1} și {evt2} ale Formatului Comun de Raportare (EU IR 1112/2014)'),
(2233, 'it', 'In caso affermativo, è stata:'),
(2233, 'ro', 'Dacă da, aceasta s-a produs:'),
(2239, 'it', 'In caso affermativo, illustrare gli impatti ambientali già osservati o che sono conseguenza dall’incidente.'),
(2239, 'ro', 'Dacă da, precizaţi impactul asupra mediului observat deja sau care este susceptibil de a rezulta în urma incidentului.'),
(2242, 'it', 'Cancellare'),
(2242, 'ro', 'Curățare'),
(2243, 'it', 'Cancellare la cronologia?'),
(2243, 'ro', 'Curăță istoric?'),
(2244, 'it', 'sezione finalizzata'),
(2244, 'ro', 'secțiune finalizată'),
(2245, 'it', 'bozza creata'),
(2245, 'ro', 'ciornă creată'),
(2246, 'it', 'bozza eliminata'),
(2246, 'ro', 'ciornă ștearsă'),
(2247, 'it', 'bozza ripristinata'),
(2247, 'ro', 'ciornă recuperată'),
(2248, 'it', 'Valutazione CPF della relazione riaperta'),
(2248, 'ro', 'evaluarea FCP a raportului redeschisă'),
(2249, 'it', 'Valutazione CPF della relazione sospesa'),
(2249, 'ro', 'evaluarea FCP a raportului suspendată'),
(2250, 'it', 'Valutazione CPF della relazione ripresa'),
(2250, 'ro', 'evaluarea FCP a raportului reluată'),
(2251, 'it', 'valutazione della Relazione degli Incidenti Gravi riaperto'),
(2251, 'ro', 'evaluarea AM a raportului redeschisă'),
(2252, 'it', 'a'),
(2252, 'ro', 'la'),
(2253, 'it', 'Bozza'),
(2253, 'ro', 'Ciornă'),
(2255, 'it', 'Azione'),
(2255, 'ro', 'Acțiune'),
(2256, 'it', 'Sezione'),
(2256, 'ro', 'Secțiunea'),
(2260, 'it', 'Incidenti segnalati'),
(2260, 'ro', 'Incidente în curs de raportare'),
(2262, 'it', 'Stato degli incidenti'),
(2262, 'ro', 'Status incident'),
(2264, 'it', 'La valutazione non può essere finalizzata!'),
(2264, 'ro', 'Evaluarea nu a putut fi finalizată!'),
(2265, 'it', 'Esaminare le informazioni qui di seguito e riprovare.'),
(2265, 'ro', 'Soluționați problemele de mai jos și încercați din nou.'),
(2266, 'it', 'Panoramica dei incidenti registrati della vostra organizzazione.'),
(2266, 'ro', 'Incidente înregistrate de organizația dumneavoastră - prezentare generală.'),
(2267, 'it', 'Panoramica degli incidenti'),
(2267, 'ro', 'Incidente'),
(2268, 'it', 'Incidenti registrati'),
(2268, 'ro', 'Incident înregistrate'),
(2269, 'it', 'il numero di incidenti riportati in SyRIO'),
(2269, 'ro', 'numărul total de incidente înregistrate în SyRIO'),
(2271, 'it', 'Anno corrente ({evt}):'),
(2271, 'ro', 'Anul current ({evt}):'),
(2272, 'it', 'Valutazione presentata'),
(2272, 'ro', 'Rapoarte transmise'),
(2274, 'it', 'In sospeso per accettazione'),
(2274, 'ro', 'În așteptarea confirmării de primire'),
(2275, 'it', 'In sospeso'),
(2275, 'ro', 'În așteptare'),
(2276, 'it', 'gli incidenti registrati in SyRIO, non ancora comunicati alla CA'),
(2276, 'ro', 'incidentele înregistrate în SyRIO, care nu au fost încă raportate'),
(2277, 'it', 'Finalizzata (parzialmente)'),
(2277, 'ro', 'Finalizate (partial)'),
(2278, 'it', 'Panoramica dei incidenti registrati della tua installazione.'),
(2278, 'ro', 'Incidente înregistrate de către instalația dumneavoastră.'),
(2279, 'it', 'Le operazioni in questa sezione consente di controllare gli eventi registrati della vostra installazione.'),
(2279, 'ro', 'Operaţiile din această secţiune vă permit gestionarea evenimentelor înregistrate de către instalația dumneavoastră.'),
(2280, 'it', 'relazioni sugli incidenti che sono stati presentati alla Autorità Competente'),
(2280, 'ro', 'rapoarte de incident care au fost transmise către Autoritatea Competentă'),
(2281, 'it', 'Finalizzata'),
(2281, 'ro', 'Finalizate'),
(2282, 'it', 'Rigettata'),
(2282, 'ro', 'Respinse'),
(2283, 'it', 'Accettata'),
(2283, 'ro', 'Acceptate'),
(2284, 'it', 'Sottoscritta'),
(2284, 'ro', 'Semnate'),
(2287, 'it', 'Creare {evt} Organizazzione'),
(2287, 'ro', 'Crează Organizatie {evt}'),
(2288, 'it', '{evt} Organizzazione'),
(2288, 'ro', 'Organizație de tip {evt}'),
(2289, 'it', 'Operatori/Proprietari'),
(2289, 'ro', 'Operatori/Proprietari'),
(2290, 'it', 'Organizzazioni della Autorità Competente'),
(2290, 'ro', 'Organizații de tip Autoritate Competentă'),
(2291, 'it', 'Tipo di Organizzazione'),
(2291, 'ro', 'Tipul organizației'),
(2294, 'it', 'selezionare il paese'),
(2294, 'ro', 'selectați țara'),
(2295, 'it', 'Installazione {evt} è stata eliminata!'),
(2295, 'ro', 'Instalația {evt} a fost ștearsă din SyRIO!'),
(2296, 'it', 'È possibile annullare l''operazione o eliminare se si vuole completamente la rimozione da SyRIO.'),
(2296, 'ro', 'Puteți reveni asupra operației sau alege ''elimină'' pentru înlăturarea completă a itemului din SyRIO.'),
(2297, 'it', 'Iinstallazione {evt} è stata riattivata con successo.'),
(2297, 'ro', 'Instalația {evt} a fost reactivată cu succes.'),
(2298, 'it', 'Riattivare l''installazione'),
(2298, 'ro', 'Reactivați instalația'),
(2299, 'it', 'Organizzazione {evt} è stata eliminata!'),
(2299, 'ro', 'Organizația {evt} a fost ștearsă!'),
(2300, 'it', 'Organizzazione {evt} è stata riattivata con successo.'),
(2300, 'ro', 'Organizația {evt} a fost reactivată cu succes.'),
(2301, 'it', 'Organizzazione riattivata'),
(2301, 'ro', 'Reactivați organizația'),
(2302, 'it', 'L''Organizzazione non può essere eliminata.'),
(2302, 'ro', 'Această organizație nu poate fi ștearsă.'),
(2303, 'it', 'Ci deve essere almeno una Organizzazione per l'' Autorità Competente.'),
(2303, 'ro', 'Trebuie să existe cel puțin o organizație de tip Autoritate Competentă activă.'),
(2304, 'it', 'Organizzazione {evt} è stata rimossa (cancellata in modo permanente)!'),
(2304, 'ro', 'Organizația {evt} a fost eliminată (ștearsă definitiv)!'),
(2305, 'it', 'Utente {evt} è stata cancellato con successo.'),
(2305, 'ro', 'Utilizatorul {user} a fost șters cu succes.'),
(2306, 'it', 'Si può riattivare l''utente piu'' tardi o eliminarlo per la cancellazione definitiva.'),
(2306, 'ro', 'Puteți reveni asupra operației sau alege ''elimină'' pentru înlăturarea completă a itemului din SyRIO.'),
(2307, 'it', 'Eliminare utente'),
(2307, 'ro', 'Șterge utilizator'),
(2308, 'it', 'Utente {evt} è stata riattivato con successo.'),
(2308, 'ro', 'Utilizatorul {user} a fost reactivat cu succes.'),
(2309, 'it', 'Riattivare utente'),
(2309, 'ro', 'Reactivează utilizator'),
(2310, 'it', 'SyRIO Sviluppatori'),
(2310, 'ro', 'Dezvoltatori SyRIO'),
(2311, 'it', 'Utilizzare questo modulo per contattare gli sviluppatori di SyRIO. Si prega di fornire qualsiasi richiesta e/o segnalare eventuali bug riscontrati. Grazie.'),
(2311, 'ro', 'Utilizați aceast formular pentru a contacta dezvoltatorii aplicației. Vă rugam să raportați orice problemă / eroare întâmpinată. Mulțumim.'),
(2312, 'it', 'Contatto degli Sviluppatori'),
(2312, 'ro', 'Contactați dezvoltatorii'),
(2313, 'it', 'Collegato come:'),
(2313, 'ro', 'Autentificat ca:'),
(2314, 'it', 'SyRIO amministratore'),
(2314, 'ro', 'Administrator SyRIO'),
(2315, 'it', 'Amministratore {evt}'),
(2315, 'ro', 'Administrator {evt}'),
(2316, 'it', 'Utente ({evt})'),
(2316, 'ro', 'Utilizator ({evt})'),
(2317, 'it', 'consente l''accesso agli incidenti segnalati della vostra Organizzazione, nonché alcuni permessi di segnalazione'),
(2317, 'ro', 'permite accesul la evenimentele înregistrate de Organizația dumneavoastră. precum și un set de drepturi de raportare'),
(2321, 'it', 'Questo sito utilizza cookies per offrire la migliore esperienza online. Continuando ad utilizzare questo sito, l''utente accetta di utilizzare i cookie. In alternativa, è possibile gestirli nelle impostazioni del browser.'),
(2321, 'ro', 'Acest site utilizează cookie-uri pentru a oferi o experiență de navigare optimă. Continuând utilizarea acestui site sunteți de acord cu folosirea cookie-urilor. Alternativ, puteți gestiona politica de utilizare a acestora în setările browserului dumneavoastră.'),
(2322, 'it', 'Lista degli utenti'),
(2322, 'ro', 'Listă utilizatori'),
(2323, 'it', 'Si prega di utilizzare i numeri delimitati della virgola per separare gli eventi dello stesso tipo (ad esempio, due esplosioni)'),
(2323, 'ro', 'Pentru a raporta evenimente de același tip (de exemplu două explozii) utilizați numere delimitate prin virgulă.'),
(2324, 'it', 'Una esplosione'),
(2324, 'ro', 'O explozie'),
(2340, 'it', 'mai'),
(2340, 'ro', 'niciodată'),
(2341, 'it', 'Si prega di fornire un nome univoco per riferimento interno di questo evento'),
(2341, 'ro', 'Introduceți un nume unic pentru ulterioare referințe ale acestui eveniment.'),
(2342, 'it', 'Il file {evt} verra'' generato per il download'),
(2342, 'ro', 'Fișierul {evt} va fi generat în vederea descărcării.'),
(2343, 'it', 'Stato della bozza'),
(2343, 'ro', 'Stare versiune de lucru'),
(2345, 'it', 'Finalizzato. Pronto per la firma.'),
(2345, 'ro', 'Finalizat. Poate fi semnat.'),
(2348, 'it', 'Dettagli dell''utente'),
(2348, 'ro', 'Detalii utilizator'),
(2352, 'it', 'aggiungi il nome'),
(2352, 'ro', 'adaugă nume'),
(2353, 'it', 'aggiungi il codice dell''installazione'),
(2353, 'ro', 'adaugă codul instalației'),
(2354, 'it', 'Si prega di usare il codice IMO (se disponibile)'),
(2354, 'ro', 'Utilizați codul IMO (dacă este disponibil)'),
(2360, 'it', 'Tipo (altro)'),
(2360, 'ro', 'Tip (altul)'),
(2361, 'it', 'per esempio jack-up'),
(2361, 'ro', 'de exemplu jack-up'),
(2362, 'it', 'opzionale'),
(2362, 'ro', 'opțional'),
(2363, 'it', 'Livello di H2S deve essere specificata se il rilascio di Gas o 2-Phase sono stati dichiarati'),
(2363, 'ro', 'Nivelul de H2S trebuie specificat în cazul declarării unei emisii de Gaz sau 2-Faze'),
(2364, 'it', 'Devi specificare l'' ALTRA AZIONE D''EMERGENZA'),
(2364, 'ro', 'Trebuie specificată MĂSURA DE URGENȚĂ luată'),
(2367, 'it', 'Intenzionalmente lasciato in bianco'),
(2367, 'ro', 'Spațiu lăsat gol în mod deliberat'),
(2368, 'it', 'Bozza del rapporto firmata'),
(2368, 'ro', 'Ciornă raport semnată'),
(2369, 'it', 'La bozza del rapporto incidentale ''{evt}'' é stata firmata.'),
(2369, 'ro', 'Ciorna ''{evt}'' a raportului de incident a fost semnată.'),
(2370, 'it', 'E'' possibile utilizzare questa bozza per riportare l''evento incidentale (''{evt}'') all''autorità competente (scegliendo il comando ''Inviare'', a meno che l''evento ''{evt}'' non sia stato precedentemente riportato)'),
(2370, 'ro', 'Puteți utiliza această ciornă pentru a raporta evenimentul (''{evt}'') autorității competente (alegeți Trimite, doar dacă evenimentul ''{evt}'' nu a fost raportat în prealabil).'),
(2371, 'it', 'utente amministratore di default'),
(2371, 'ro', 'utilizator administrator implicit');

-- --------------------------------------------------------

--
-- Table structure for table `t_source_message`
--

DROP TABLE IF EXISTS `t_source_message`;
CREATE TABLE `t_source_message` (
  `id` int(11) NOT NULL,
  `category` varchar(32) CHARACTER SET utf8 DEFAULT NULL,
  `message` varchar(1024) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2372 DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=COMPACT;

--
-- Dumping data for table `t_source_message`
--

INSERT INTO `t_source_message` (`id`, `category`, `message`) VALUES
(2, 'app', 'home'),
(3, 'app', 'requests'),
(4, 'app', 'units'),
(5, 'app', 'admin'),
(6, 'app/translations', 'category'),
(7, 'app/translations', 'message'),
(8, 'app/translations', 'translation'),
(9, 'app', 'all'),
(10, 'app', 'existing'),
(11, 'app', 'missing'),
(12, 'app', 'The source messages are the ones that must be translated in other languages. The messages are grouped in Categories and address the application messages (UI) and the Common Reporting Format (app/crf) and Common Publication Format (app/cpf) messages and text.'),
(13, 'app/translations', 'source messages'),
(14, 'app/translations', 'translations'),
(15, 'app', 'country'),
(16, 'app', 'code'),
(17, 'app', 'completion'),
(19, 'app', 'translated'),
(20, 'app', 'total'),
(21, 'app/translations', 'registered languages'),
(22, 'app', 'register new {evt}'),
(23, 'app', 'language'),
(25, 'app', 'organizations'),
(26, 'app', 'installation'),
(27, 'app', 'installations'),
(28, 'app', 'user'),
(29, 'app', 'users'),
(30, 'app', 'Currently, there are {evt} messages registered with SyRIO.'),
(31, 'app/translations', 'source message'),
(32, 'app/translations', 'At least one source message must be translated to register a new language.'),
(33, 'app/commands', 'Submit'),
(34, 'app', 'actions'),
(36, 'app', 'reset'),
(39, 'app', 'Text'),
(43, 'app', 'export as...'),
(44, 'app/translations', 'message translation'),
(45, 'app/translations', 'Please provide the translation for the source message in the language of choice ({evt}).'),
(46, 'app/translations', 'Pay special attention to (preserve!) the eventual placeholders in the source message (curly bracelets).'),
(47, 'app/commands', 'Reset {evt}'),
(48, 'app', 'grid'),
(49, 'app', 'modify {evt}'),
(50, 'app/commands', 'Cancel'),
(51, 'app', 'more'),
(52, 'app', 'Organizations'),
(53, 'app', '\\''{evt}\\'' has been successfully {evt2}.'),
(54, 'app', 'acronym'),
(56, 'app', 'address'),
(57, 'app', 'Active Installations'),
(58, 'app', 'Add {evt}'),
(59, 'app', 'any'),
(60, 'app', 'type'),
(61, 'app', 'Delete {evt}'),
(62, 'app', 'Operation CAN be undone.'),
(64, 'app', 'PERMANENTLY delete {evt}'),
(65, 'app', 'Operation CAN NOT be undone.'),
(66, 'app', 'purge'),
(67, 'app', 'This will reactivate {evt}.'),
(68, 'app', 'Are you sure?'),
(69, 'app', 'reactivate'),
(70, 'app', 'CA user'),
(71, 'app', 'OO user'),
(72, 'app', 'List {evt}'),
(73, 'app', 'Competent Authority'),
(74, 'app', 'Operators / Owners'),
(75, 'app', 'Name'),
(76, 'app', 'Organization'),
(77, 'app', 'Role in organization'),
(78, 'app', 'Member since'),
(79, 'app', 'Email'),
(80, 'app', 'Phone'),
(81, 'app', 'Username'),
(82, 'app', 'User type'),
(83, 'app', 'Role(s)'),
(84, 'app', 'Are you human?'),
(85, 'app', 'Please provide the verification text from the image'),
(86, 'app', 'Change {evt}'),
(87, 'app', 'Unable to {evt}! Please review the information below and try again.'),
(88, 'app', 'password'),
(89, 'app', '{evt} has been successfully modified.'),
(92, 'app', 'This will be verified after submitting the request'),
(93, 'app', 'change details'),
(95, 'app', 'The new username you will use for login'),
(96, 'app', '{evt} have been successfully modified.'),
(97, 'app', '{evt} user'),
(98, 'app', 'Create'),
(99, 'app', 'create user'),
(100, 'app', 'User {evt} has been created.'),
(101, 'app', 'Register {evt}'),
(102, 'app', 'new user'),
(103, 'app', 'Registered Users'),
(104, 'app', 'Please select one of the request types in the categories below.'),
(105, 'app', 'Use these forms for requesting the registration of your installation.'),
(106, 'app', 'Use these forms for requesting the registration of your organization in SyRIO.'),
(107, 'app', 'Competent Authority Organization'),
(108, 'app', 'Operators / Owners Organization'),
(109, 'app', 'Use these forms for submitting user registration.'),
(110, 'app', 'Register as {evt}'),
(111, 'app', 'view {evt}'),
(112, 'app', 'Assignments'),
(113, 'app', 'Assignment'),
(115, 'app/crf', 'Section {evt}'),
(116, 'app/crf', 'Section {id}'),
(117, 'app/crf', 'Click on each of the sections below for details'),
(118, 'app/crf', 'Section'),
(119, 'app/crf', 'report draft'),
(120, 'app/crf', 'Event'),
(121, 'app/crf', 'date'),
(122, 'app/crf', 'time'),
(123, 'app/crf', '(if relevant)'),
(124, 'app/crf', '(if applicable)'),
(126, 'app/crf', 'According to Annex IX of Directive 2013/30/EU'),
(127, 'app/crf', 'Event date and time'),
(128, 'app/crf', 'Event date'),
(129, 'app/crf', 'Event time'),
(130, 'app/crf', 'Details of the location and of the person reporting the event'),
(131, 'app/crf', 'Operator/owner'),
(132, 'app/crf', 'Name/type of the installation'),
(133, 'app/crf', 'Field name/code'),
(134, 'app/crf', 'Name of the reporting person'),
(135, 'app/crf', 'Role of the reporting person'),
(136, 'app/crf', 'Contact details'),
(137, 'app/crf', 'Telephone number'),
(138, 'app/crf', 'E-mail address'),
(139, 'app/crf', 'Unintended release of oil, gas or other hazardous substances, whether or not ignited'),
(140, 'app/crf', 'Any unintentional release of ignited gas or oil on or from an offshore installation;'),
(141, 'app/crf', 'The unintentional release on or from an offshore installation of:'),
(144, 'app/crf', 'The unintentional	release	or escape of any hazardous substance, for which the major accident risk has been assessed in the report on major hazards, on or from an offshore installation, including wells and returns of drilling additives.'),
(145, 'app/crf', 'Loss of well control requiring actuation of well control equipment, or failure of a well barrier requiring its replacement or repair'),
(146, 'app/crf', 'Any blowout, regardless of the duration;'),
(147, 'app/crf', 'The mechanical failure of any part of a well, whose purpose is to prevent or limit the effect of the unintentional release of fluids from a well or a reservoir being drawn on by a well, or whose failure would cause or contribute to such a release'),
(148, 'app/crf', 'The taking of precautionary measures additional to any already contained in the original drilling programme where a planned minimum separation distance between adjacent wells was not maintained.'),
(149, 'app/crf', 'Failure of a safety and environmental critical element'),
(150, 'app/crf', 'Any loss or non-availability of a SECE requiring immediate remedial action.'),
(152, 'app/crf', 'Any detected condition that reduces the designed structural integrity of the installation, including stability, buoyancy and station keeping, to the extent that it requires immediate remedial action.'),
(153, 'app/crf', 'Vessels on collision course and actual vessel collisions with an offshore installation'),
(154, 'app/crf', 'Any collision, or potential collision, between a vessel and an offshore installation which has, or would have, enough energy to cause sufficient damage to the installation and/or vessel, to jeopardise the overall structural or process integrity.'),
(155, 'app/crf', 'Helicopter accidents, on or near offshore installations'),
(156, 'app/crf', 'Any collision, or potential collision, between a helicopter and an offshore installation.'),
(157, 'app/crf', 'Any fatal accident to be reported under the requirements of Directive 92/91/EEC'),
(158, 'app/crf', 'Any serious injuries to five or more persons in the same accident to be reported under the requirements of Directive 92/91/EEC'),
(159, 'app/crf', 'Any evacuation of personnel'),
(160, 'app/crf', 'Any unplanned emergency evacuation of part of or all personnel as a result of, or where there is a significant risk of a major accident.'),
(161, 'app/crf', 'A major environmental incident'),
(162, 'app/crf', 'Any major environmental incident as defined in Article 2.1.d and Article 2.37 of Directive 2013/30/EU.'),
(163, 'app/crf', 'General information'),
(165, 'app/crf', 'Description of circumstances, consequences of event and emergency response'),
(166, 'app/crf', 'Preliminary direct and underlying causes.'),
(167, 'app/crf', 'Preliminary direct and underlying causes'),
(168, 'app/crf', '(within 10 days of the event)'),
(169, 'app/crf', 'Initial lessons learned and preliminary recommendations to prevent recurrence of similar events'),
(170, 'app/crf', 'Name of the vessel'),
(171, 'app/crf', 'Indicate the system that failed and provide a description of the circumstances of the event / describe what has happened '),
(172, 'app/crf', 'including weather conditions and sea state.'),
(173, 'app/crf', 'The competent authority shall further complete this section'),
(174, 'app/crf', 'Is this considered to be a major incident?'),
(175, 'app/crf', 'Give justification'),
(176, 'app', 'Login'),
(179, 'app/crf', 'significant loss of structural integrity, or loss of protection against the effects of fire or explosion, or loss of station keeping in relation to a mobile installation'),
(180, 'app/crf', 'Event date/time'),
(181, 'app/crf', 'Event Name'),
(182, 'app/crf', 'Description of circumstances'),
(183, 'app/crf', 'Consequences of event and emergency response'),
(184, 'app/crf', 'Event categorization'),
(186, 'app/crf', 'What type of event is being reported?'),
(187, 'app/crf', '(More than one option might be chosen)'),
(188, 'app/crf', 'UNINTENDED RELEASE OF OIL, GAS OR OTHER HAZARDOUS'),
(189, 'app/crf', 'SUBSTANCES, WHETHER OR NOT IGNITED'),
(190, 'app/crf', 'A.1. Was there a release of hydrocarbon substances?'),
(191, 'app', 'Yes'),
(192, 'app', 'No'),
(193, 'app/crf', 'Hydrocarbon (HC) released'),
(195, 'app/crf', '(Specify)'),
(196, 'app/crf', 'NON-PROCESS'),
(197, 'app/crf', 'PROCESS'),
(198, 'app/crf', 'Oil'),
(199, 'app/crf', 'Condensate'),
(200, 'app/crf', 'Gas'),
(201, 'app/crf', '2-Phase'),
(202, 'app/crf', 'For gas or 2-Phase, state level of H'),
(203, 'app/crf', '(estimated ppm)'),
(204, 'app/crf', 'Estimated quantity released'),
(205, 'app/crf', 'Specify units, e.g. tonnes, kg, Nm'),
(206, 'app/crf', 'Estimated initial release rate'),
(207, 'app/crf', 'Specify units, e.g. tonnes/day, kg/s, Nm'),
(208, 'app/crf', 'Duration of leak'),
(209, 'app/crf', 'Estimated time from discovery, e.g. alarm, electronic log, to termination of leak'),
(210, 'app/crf', 'Location of leak'),
(211, 'app/crf', 'Hazardous area classification'),
(212, 'app/crf', 'i.e. zone at location of incident'),
(213, 'app/crf', 'Unclassified'),
(215, 'app/crf', 'Not applicable'),
(216, 'app/crf', 'Natural'),
(217, 'app/crf', 'Forced'),
(218, 'app/crf', 'How many sides enclosed?'),
(219, 'app/crf', '(Insert the number of walls, including floor and ceiling)'),
(220, 'app/crf', 'Module volume'),
(221, 'app/crf', 'Estimated number of air changes'),
(222, 'app/crf', 'known'),
(223, 'app/crf', 'Specify hourly rate'),
(224, 'app/crf', 'Weather conditions'),
(225, 'app/crf', 'Wind speed'),
(226, 'app/crf', 'Wind direction'),
(227, 'app/crf', '(Specify units, e.g. mph, m/s, ft/s)'),
(228, 'app/crf', '(Specify heading in degrees)'),
(229, 'app/crf', 'Provide a description of other relevant weather conditions'),
(230, 'app/crf', 'System pressure'),
(231, 'app/crf', 'Design pressure'),
(232, 'app/crf', 'Actual pressure'),
(233, 'app/crf', '(Specify units, e.g. bar psi or other)'),
(234, 'app/crf', '(i.e. at time of release)'),
(235, 'app/crf', 'Means of detection'),
(236, 'app/crf', 'Fire'),
(237, 'app/crf', 'Smoke'),
(238, 'app/crf', 'Other'),
(239, 'app/crf', '(Please tick type of detector or specify as appropriate)'),
(240, 'app/crf', '(Please specify)'),
(241, 'app/crf', 'Cause of leak'),
(242, 'app/crf', '(Please give a short description and complete the "Cause" checklist below)'),
(243, 'app/crf', 'Did ignition occur?'),
(245, 'app/crf', 'was it'),
(246, 'app/crf', 'Immediate'),
(247, 'app/crf', 'Delayed'),
(248, 'app/crf', 'Delay time'),
(249, 'app/crf', '(sec)'),
(250, 'app/crf', 'Was there'),
(251, 'app/crf', '(add sequence of events by numbering appropriate boxes in order of occurrence)'),
(252, 'app/crf', 'A flash fire'),
(253, 'app/crf', 'An explosion'),
(254, 'app/crf', 'A jet fire'),
(255, 'app/crf', 'A pool fire'),
(256, 'app/crf', 'Ignition source'),
(257, 'app/crf', 'Provide a description of the ignition source'),
(258, 'app/crf', 'What emergency action was taken?'),
(259, 'app/crf', 'Shutdown'),
(260, 'app/crf', 'Automatic'),
(261, 'app/crf', 'Manual'),
(262, 'app/crf', 'Blowdown'),
(263, 'app/crf', 'Deluge'),
(264, 'app/crf', 'Halon/inerts'),
(265, 'app/crf', 'Call to muster'),
(266, 'app/crf', 'Other, specify'),
(267, 'app/crf', 'Any additional comments'),
(268, 'app/crf', 'CAUSE OF LEAK CHECKLIST (See point A.1.XI "Cause of leak")'),
(269, 'app/crf', '(Please indicate those items which come nearest to pinpointing the cause of the leak)'),
(270, 'app/crf', 'Indicate the cause(s) of the release'),
(271, 'app/crf', 'From each of the following categories'),
(272, 'app/crf', 'tick the appropriate boxes'),
(273, 'app/crf', 'Design'),
(274, 'app/crf', 'Failure related to design'),
(275, 'app/crf', 'Equipment'),
(276, 'app/crf', 'Internal corrosion'),
(277, 'app/crf', 'External corrosion'),
(278, 'app/crf', 'Mechanical failure due to fatigue'),
(279, 'app/crf', 'Mechanical failure due to wear out'),
(280, 'app/crf', 'Erosion'),
(281, 'app/crf', 'Material defect'),
(282, 'app/crf', 'Operation'),
(283, 'app/crf', 'Incorrectly fitted'),
(284, 'app/crf', 'Left open'),
(285, 'app/crf', 'Improper inspection'),
(286, 'app/crf', 'Improper testing'),
(287, 'app/crf', 'Improper operation'),
(288, 'app/crf', 'Improper maintenance'),
(289, 'app/crf', 'Dropped object'),
(290, 'app/crf', 'Other impact'),
(291, 'app/crf', 'Opened when containing HC'),
(292, 'app/crf', 'Procedural'),
(293, 'app/crf', 'Non-compliance with procedures'),
(294, 'app/crf', 'Non-compliance with permit-to-work'),
(295, 'app/crf', 'Deficient procedure'),
(296, 'app/crf', 'Indicate the operational mode in the area at the time of release'),
(297, 'app', 'Choose'),
(298, 'app', 'one'),
(299, 'app', 'parameter from the following categories, and tick the appropriate boxes'),
(301, 'app/crf', 'Drilling'),
(302, 'app/crf', 'Well operations'),
(303, 'app/crf', '(Specify actual operation, e.g. wire line, well test, etc.)'),
(304, 'app/crf', 'Production'),
(305, 'app/crf', 'Maintenance'),
(306, 'app/crf', 'Construction'),
(307, 'app/crf', 'Pipeline operations including pigging'),
(308, 'app/crf', 'A.2. Description of circumstances, consequences of event and emergency response'),
(309, 'app/crf', 'Was there a release of a non-hydrocarbon hazardous substance?'),
(310, 'app/crf', 'specify the type and quantity of released substance'),
(311, 'app/crf', '(Type)'),
(312, 'app/crf', '(Quantity, specify units)'),
(313, 'app/crf', 'Was there a non-hydrocarbon fire (e.g. electrical) with a significant potential to cause a major accident?'),
(314, 'app/crf', 'Describe circumstances'),
(315, 'app/crf', 'Is the incident likely to cause degradation to the surrounding marine environment?'),
(316, 'app/crf', 'outline the environmental impacts which have already been observed or are likely to result from the incident'),
(317, 'app/crf', 'Preliminary direct and underlying causes (within 10 working days of the event)'),
(318, 'app/crf', 'Initial lessons learned and preliminary recommendations to prevent recurrence of similar events (within 10 working days of the event)'),
(322, 'app/crf', 'END OF THE REPORT'),
(325, 'app/crf', 'FAILURE OF A SAFETY AND ENVIRONMENT CRITICAL ELEMENT'),
(326, 'app/crf', 'LOSS OF WELL CONTROL REQUIRING ACTUATION OF WELL CONTROL EQUIPMENT, OR'),
(327, 'app/crf', 'FAILURE OF A WELL BARRIER REQUIRING ITS REPLACEMENT OR REPAIR'),
(328, 'app/crf', 'Name/code of well'),
(329, 'app/crf', 'Name of drilling contractor (if relevant)'),
(330, 'app/crf', 'Name/type of drilling rig (if relevant)'),
(331, 'app/crf', 'Start and end date/time of loss of well control'),
(332, 'app/crf', 'Type of fluid: brine / oil / gas / ... (if relevant)'),
(333, 'app/crf', 'Well head completion: surface / subsea'),
(335, 'app/crf', 'Reservoir: pressure / temperature / depth'),
(336, 'app/crf', 'Type of activity: normal production / drilling / work over / well services'),
(337, 'app/crf', 'Type of well services (if applicable): wire line / coiled tubing / snubbing'),
(338, 'app/crf', 'Blowout prevention equipment activated'),
(339, 'app/crf', 'Diverter system in operation'),
(340, 'app/crf', 'Pressure build-up and/or positive flow check'),
(341, 'app/crf', 'Failing well barriers'),
(343, 'app/crf', '(specify units)'),
(344, 'app/crf', 'Duration of uncontrolled flow of well-fluids'),
(345, 'app/crf', 'Flowrate'),
(346, 'app/crf', 'Liquid volume'),
(347, 'app/crf', 'Gas volume'),
(349, 'app/crf', '(E.g.; 1. jet fire / 2. first explosion / 3. second explosion, etc.)'),
(351, 'app/crf', 'FAILURE OF A SAFETY AND ENVIRONMENTAL CRITICAL ELEMENT'),
(352, 'app/crf', 'Name of the independent verifier (if applicable)'),
(353, 'app/crf', 'Description of SECE and circumstances'),
(354, 'app/crf', 'Which Safety and Environmental Critical systems were reported by the independent verifier as lost or unavailable, requiring immediate remedial action, or have failed during an incident?'),
(355, 'app/crf', 'Report Independent verifier: details (report nr. / date / verifier / ...) '),
(356, 'app/crf', 'Failure during major accident: details (date /accident description / ...)'),
(358, 'app/crf', 'Structural integrity systems'),
(359, 'app/crf', 'Topside Structures'),
(360, 'app/crf', 'Subsea structures'),
(361, 'app/crf', 'Cranes & lifting equipment'),
(362, 'app/crf', 'Mooring systems (anchorline, dynamic positioning)'),
(364, 'app/crf', 'Primary well barrier'),
(365, 'app/crf', 'Secondary well barier'),
(366, 'app/crf', 'Wireline equipment'),
(367, 'app/crf', 'Mud processing'),
(368, 'app/crf', 'Sand filters'),
(369, 'app/crf', 'Pipelines & risers'),
(370, 'app/crf', 'Piping system'),
(371, 'app/crf', 'Pressure vessels'),
(372, 'app/crf', 'Well control process equipment - BOP'),
(373, 'app/crf', 'Ignition control system'),
(374, 'app/crf', 'Hazardous area ventilation'),
(375, 'app/crf', 'Non-hazardous area ventilation'),
(376, 'app/crf', 'ATEX certified equipment'),
(377, 'app/crf', 'Electrical tripping equipment'),
(378, 'app/crf', 'Earthing / bonding equipment'),
(379, 'app/crf', 'Inert Gas system'),
(380, 'app/crf', 'Detection systems'),
(381, 'app/crf', 'Fire & gas detection'),
(382, 'app/crf', 'Chemical injection monitor'),
(383, 'app/crf', 'Sand'),
(384, 'app/crf', 'Process containment relief systems'),
(385, 'app/crf', 'Well control process equipment - diverter'),
(386, 'app/crf', 'Relief systems'),
(387, 'app/crf', 'Gas tight floors'),
(388, 'app/crf', 'Protection systems'),
(390, 'app/crf', 'Helideck foam system'),
(391, 'app/crf', 'Fire water pumps'),
(392, 'app/crf', 'Fire water system'),
(393, 'app/crf', 'Passive fire protection system'),
(394, 'app/crf', 'Fire/blast walls'),
(395, 'app/crf', 'CO2/Halon fire-fighting system'),
(396, 'app/crf', 'Shutdown systems'),
(397, 'app/crf', 'Local shutdown system (LSD)'),
(398, 'app/crf', 'Process shutdown system (PSD)'),
(399, 'app/crf', 'Emergency shutdown system (ESD)'),
(400, 'app/crf', 'Subsea isolation valve (SSIV)'),
(401, 'app/crf', 'Riser ESD valve'),
(402, 'app/crf', 'Topsides ESD valve'),
(404, 'app/crf', 'Navigational aids'),
(405, 'app/crf', 'Aircraft navigational aids'),
(406, 'app/crf', 'Seacraft navigational aids'),
(407, 'app/crf', 'Rotating equipment - power supply'),
(408, 'app/crf', 'Turbine P.M. for compressor'),
(409, 'app/crf', 'Turbine P.M. for generator'),
(410, 'app/crf', 'Escape, evacuation and rescue equipment'),
(411, 'app/crf', 'Personal safety equipment'),
(412, 'app/crf', 'Lifeboats / TEMPSC'),
(413, 'app/crf', 'Tertiary escape means (lifecraft)'),
(414, 'app/crf', 'Temporary refuge / Muster area'),
(415, 'app/crf', 'Search & rescue facilities'),
(416, 'app/crf', 'Communication system'),
(417, 'app/crf', 'Radios / telephones'),
(418, 'app/crf', 'Public address'),
(421, 'app/crf', 'Describe any important lessons learned from the event. List recommendations to prevent the recurrence of similar events.'),
(424, 'app/crf', 'Indicate the system that failed and provide a description of the circumstances of the event / describe what has happened including weather conditions and sea state.'),
(425, 'app/crf', 'SIGNIFICANT LOSS OF STRUCTURAL INTEGRITY, OR LOSS OF PROTECTION AGAINST '),
(426, 'app/crf', 'THE EFFECTS OF FIRE OR EXPLOSION, OR LOSS OF STATION KEEPING IN RELATION TO '),
(427, 'app/crf', 'A MOBILE INSTALLATION '),
(429, 'app/crf', 'VESSELS ON COLLISION COURSE AND ACTUAL VESSEL COLLISIONS WITH '),
(430, 'app/crf', 'AN OFFSHORE INSTALLATION'),
(431, 'app/crf', 'Name / Flag State of vessel (If applicable)'),
(432, 'app/crf', 'Type / Tonnage of vessel (If applicable)'),
(433, 'app/crf', 'Contact via AIS?'),
(435, 'app/crf', 'Indicate the system that failed and provide a description of the circumstances of the event / describe what has happened (minimum distance between vessel and installation, course and speed of vessel, weather condition)'),
(438, 'app/crf', 'Name of helicopter contractor'),
(439, 'app/crf', 'Helicopter type'),
(440, 'app/crf', 'Number of persons on board'),
(441, 'app/crf', 'Helicopter incidents are reported under CAA regulations. If a helicopter accident occurs in relation to Directive 2013/30/EU, section F shall be completed'),
(442, 'app/crf', 'Indicate the system that failed and provide a description of the circumstances of the event / describe what has happened (weather conditions)'),
(443, 'app/crf', 'Section G shall be reported under the requirements of Directive 92/91/EEC'),
(444, 'app/crf', 'Section H shall be reported under the requirements of Directive 92/91/EEC'),
(446, 'app/crf', 'ANY EVACUATION OF PERSONNEL'),
(447, 'app/crf', 'Start and end date/time of evacuation'),
(448, 'app/crf', 'Was the evacuation precautionary or emergency?'),
(449, 'app/crf', 'Precautionary'),
(450, 'app/crf', 'Emergency'),
(451, 'app/crf', 'Both'),
(452, 'app/crf', 'Number of persons evacuated'),
(453, 'app/crf', 'Means of evacuation'),
(454, 'app/crf', '(e.g. helicopter)'),
(455, 'app/crf', 'Indicate the system that failed and provide a description of the circumstances of the event / describe what has happened, unless already reported in a previous section of this report'),
(457, 'app/crf', 'A MAJOR ENVIRONMENTAL INCIDENT'),
(458, 'app/crf', 'Name of contractor'),
(459, 'app/crf', 'Indicate the system that failed and provide a description of the circumstances of the event / describe what has happened. What are or are likely to be the significant adverse effects on the environment?'),
(460, 'app/crf', 'Generate PDF'),
(461, 'app/crf', 'Remarks'),
(462, 'app/crf', 'If the incident falls into one of the above mentioned categories, the operator/owner shall proceed to the relevant section(s), hence a single incident could result in completing multiple sections.  The operator/owner shall submit the filled in sections to the Competent Authority within 15 working days of the event, using the best information available at that time. If the event reported is a major accident, the Member State shall initiate a thorough investigation in accordance with Article 26 of Directive 2013/30/EU'),
(463, 'app/crf', 'Fatalities and serious injuries are reported under the requirements of Directive 92/91/EEC'),
(465, 'app/crf', 'Was there a release of hydrocarbon substances?'),
(466, 'app/crf', 'Description of circumstances, consequences of an event and emergency response actions taken.'),
(467, 'app/crf', 'Loss of well control requiring actuation of well control equipment'),
(468, 'app/crf', 'or failure of a well barrier requiring its replacement or repair'),
(469, 'app/crf', 'Further details'),
(471, 'app/crf', 'as lost or unavailable, requiring immediate remedial action, or have failed during an incident?'),
(472, 'app/crf', 'Safety and Environmental Critical Elements concerned'),
(473, 'app/cpf', 'PROFILE'),
(474, 'app/cpf', 'Information on Member State and Reporting Authority'),
(475, 'app/cpf', 'Member State'),
(476, 'app/cpf', 'Reporting period'),
(477, 'app/cpf', 'Calendar Year'),
(478, 'app/cpf', 'Designated Reporting Authority'),
(479, 'app/cpf', 'Refresh installations'),
(480, 'app/cpf', 'Fixed installations'),
(481, 'app/cpf', 'Please provide detailed list of installations for offshore oil and gas operations in your country (on first of January of the reported year), including their type (i.e. fixed manned, fixed normally unmanned, floating production, fixed non-production), year of installation and location'),
(482, 'app/cpf', 'Table {evt}'),
(483, 'app/cpf', 'Installations within jurisdiction on 1 January of the reporting year'),
(484, 'app/cpf', 'Name or ID'),
(485, 'app/cpf', 'Type of installation'),
(486, 'app/cpf', 'Year of installation'),
(487, 'app/cpf', 'Type of fluid'),
(488, 'app/cpf', 'Number of beds'),
(489, 'app/cpf', 'Coordinates'),
(490, 'app/cpf', 'i.e.'),
(491, 'app/cpf', 'Fixed manned installation (FMI)'),
(492, 'app/cpf', '(Fixed) normally unmanned (NUl)'),
(493, 'app/cpf', 'Floating production install. (FPI)'),
(494, 'app/cpf', 'Fixed non-production install. (FNP)'),
(495, 'app/cpf', 'longitude'),
(496, 'app/cpf', 'latitude'),
(497, 'app', 'Are you sure you want to delete this item?'),
(498, 'app', 'no installation to report'),
(499, 'app', '(not set)'),
(500, 'app/cpf', 'Changes since the previous reporting year'),
(501, 'app/cpf', 'New fixed installations'),
(502, 'app/cpf', 'Please report the new fixed installations, entered in operation during the reporting period.'),
(503, 'app/cpf', 'New fixed installations entered in operation during the reporting period'),
(504, 'app/cpf', 'Fixed installations out of operation'),
(505, 'app/cpf', 'Please report the installations that went out of offshore oil and gas operations during the reporting period'),
(506, 'app/cpf', 'Installations that were decommissioned during the reporting period'),
(507, 'app', 'Temporary'),
(508, 'app', 'Permanent'),
(509, 'app/cpf', 'Mobile installations'),
(510, 'app/cpf', 'Please report the mobile installations carrying out operations during the reporting period (MODUs and other non-production installations)'),
(511, 'app/cpf', 'Fixed attended'),
(512, 'app/cpf', 'Fixed normally unattended'),
(513, 'app/cpf', 'Floating production installation'),
(514, 'app/cpf', 'Fixed non-production installation'),
(515, 'app/cpf', 'Mobile offshore drilling'),
(516, 'app/cpf', 'Other mobile non-production'),
(517, 'app/cpf', 'Year of construction'),
(518, 'app/cpf', 'Geographical area of operations'),
(519, 'app/cpf', 'Duration'),
(520, 'app/cpf', 'e.g. South North Sea, North Adriatic'),
(521, 'app/cpf', 'Area'),
(522, 'app/cpf', 'months'),
(523, 'app/cpf', 'Information for data normalization purposes'),
(524, 'app/cpf', 'Please provide the total number of actual offshore working hours and the total production in the reporting period'),
(525, 'app/cpf', 'Total number of actual offshore working hours for all installations'),
(526, 'app/cpf', 'Total production, in kTOE'),
(527, 'app/cpf', 'Oil production'),
(528, 'app/cpf', 'Gas production'),
(529, 'app/cpf', 'UNRECOGNIZED ENERGY UNIT'),
(530, 'app/cpf', 'REGULATORY FUNCTIONS AND FRAMEWORK'),
(531, 'app/cpf', 'Inspections'),
(532, 'app/cpf', 'Number and offshore inspections performed during the reporting year'),
(533, 'app/cpf', 'Number of offshore inspections'),
(534, 'app/cpf', 'Man-days spent on installation (travel time not included)'),
(535, 'app/cpf', 'Number of inspected installations'),
(536, 'app/cpf', 'Investigations'),
(537, 'app/cpf', 'Number and type of investigations performed during the reporting year'),
(538, 'app/cpf', 'Major accidents'),
(539, 'app/cpf', 'pursuant to Article 26 of Directive 2013/30/EU'),
(540, 'app/cpf', 'pursuant to Article 22 of Directive 2013/30/EU'),
(541, 'app/cpf', 'Environmental concerns'),
(542, 'app/cpf', 'Enforcement actions'),
(543, 'app/cpf', 'Main enforcement actions or convictions performed in the reporting period pursuant to Article 18 of Directive 2013/30/EU'),
(544, 'app/cpf', 'Narrative'),
(545, 'app/cpf', 'Major changes in the offshore regulatory framework'),
(546, 'app/cpf', 'Please describe any major changes in the offshore regulatory framework during the reporting period'),
(547, 'app/cpf', 'include e.g. rationale, description, expected outcome, references'),
(548, 'app/cpf', 'Rationale'),
(549, 'app', 'Description'),
(550, 'app/cpf', 'Expected outcome'),
(551, 'app/cpf', 'References'),
(552, 'app', '(none)'),
(553, 'app/cpf', 'INCIDENT DATA AND PERFORMANCE OF OFFSHORE OPERATIONS'),
(554, 'app/cpf', 'Be aware that this process may take quite a while, depending on the number of events registered and prepared for being reported over the reporting period. Proceed?'),
(555, 'app/cpf', 'Refresh data'),
(556, 'app/cpf', 'Incident data'),
(557, 'app/cpf', 'Number of reportable events pursuant to Annex IX'),
(558, 'app/cpf', 'of which identified as being major accidents'),
(559, 'app/cpf', 'Annex IX categories'),
(560, 'app/cpf', 'Number of events'),
(561, 'app/cpf', 'Normalized number of events'),
(562, 'app/cpf', 'Annex IX Incident Categories'),
(563, 'app/cpf', 'reported number'),
(564, 'app/cpf', 'events'),
(565, 'app/cpf', 'hours worked'),
(566, 'app/cpf', 'kTOE'),
(567, 'app/cpf', 'Unintended releases'),
(568, 'app/cpf', 'Ignited oil/gas releases - Fires'),
(569, 'app/cpf', 'Ignited oil/gas releases - Explosions'),
(570, 'app/cpf', 'Not ignited gas releases'),
(571, 'app/cpf', 'Not ignited oil releases'),
(572, 'app/cpf', 'Hazardous substances released'),
(573, 'app/cpf', 'Loss of well control'),
(574, 'app/cpf', 'Blowouts'),
(575, 'app/cpf', 'Activation of BOP / diverter system'),
(576, 'app/cpf', 'Failure of a well barrier'),
(577, 'app/cpf', 'Failures of SECE'),
(578, 'app/cpf', 'Loss of structural integrity'),
(580, 'app/cpf', 'Loss  of stability/buoyancy'),
(581, 'app/cpf', 'Loss of station keeping'),
(582, 'app/cpf', 'Vessel collisions'),
(583, 'app/cpf', 'Helicopter accidents'),
(584, 'app/cpf', 'Fatal accidents'),
(586, 'app/cpf', 'Evacuations  of personnel'),
(587, 'app/cpf', 'Environmental accidents'),
(588, 'app/cpf', 'Evacuation of personnel'),
(589, 'app/cpf', 'Only if related to a major accident'),
(590, 'app/cpf', 'Values used for normalization'),
(592, 'app/cpf', 'Provided in'),
(593, 'app/cpf', 'of this document'),
(594, 'app/cpf', 'Total number of fatalities and injuries'),
(595, 'app/cpf', 'Number'),
(596, 'app/cpf', 'Normalized value'),
(597, 'app/cpf', 'Total number of fatalities'),
(598, 'app/cpf', 'Total number of injuries'),
(599, 'app/cpf', 'Total number of serious injuries'),
(601, 'app/cpf', 'SECE'),
(602, 'app/cpf', 'Number related to major accidents'),
(603, 'app/cpf', 'Process containment  systems'),
(604, 'app/cpf', 'Ignition control systems'),
(605, 'app/cpf', 'Process containment relief systems'),
(606, 'app/cpf', 'Protection  systems'),
(607, 'app/cpf', 'Navigational  aids'),
(608, 'app/cpf', 'Process containment systems'),
(609, 'app/cpf', 'Direct and Underlying causes of major incidents'),
(610, 'app/cpf', 'Equipment-related causes'),
(611, 'app/cpf', 'Human error - operational causes'),
(612, 'app/cpf', 'Procedural/organisational error'),
(613, 'app/cpf', 'Weather-related causes'),
(614, 'app/cpf', 'Design failures'),
(615, 'app/cpf', 'Mechanical failures due to fatigue'),
(616, 'app/cpf', 'Mechanical failures due to wear-out'),
(617, 'app/cpf', 'Mechanical failures due to defected material'),
(618, 'app/cpf', 'Mechanical failure (vessel/helicopter)'),
(621, 'app/cpf', 'Human error-operational failure'),
(622, 'app/cpf', 'Operation error'),
(623, 'app/cpf', 'Maintenance error'),
(624, 'app/cpf', 'Testing error'),
(625, 'app/cpf', 'Inspection error'),
(626, 'app/cpf', 'Design error'),
(627, 'app/cpf', 'Procedural / Organizational error'),
(628, 'app/cpf', 'Inadequate risk assessment/perception'),
(629, 'app/cpf', 'Inadequate instruction/procedure'),
(630, 'app/cpf', 'Inadequate communication'),
(631, 'app/cpf', 'Inadequate personnel competence'),
(632, 'app/cpf', 'Inadequate supervision'),
(633, 'app/cpf', 'Inadequate safety leadership'),
(634, 'app/cpf', 'Wind in excess of limits of design'),
(635, 'app/cpf', 'Wave in excess of limits of design'),
(636, 'app/cpf', 'Extremely low visibility in excess of limits of design'),
(637, 'app/cpf', 'Presence of ice/icebergs'),
(638, 'app/cpf', 'Which are the most important lessons learned from the incidents that  deserve to be shared?'),
(639, 'app', 'incident'),
(640, 'app', 'incidents'),
(641, 'app', 'Launch Events Hub'),
(643, 'app', 'Events management'),
(646, 'app', 'new event'),
(647, 'app', 'incidents hub'),
(649, 'app', 'Logout'),
(650, 'app', 'Account'),
(652, 'app', 'OK'),
(655, 'app', 'created at'),
(656, 'app', 'modified at'),
(659, 'app', 'modified by'),
(660, 'app', 'Registered events'),
(661, 'app', 'incident name'),
(662, 'app', 'Date/Time'),
(663, 'app', 'Classification'),
(664, 'app', 'comma-delimited letters (a to j)'),
(666, 'app', 'inst/type/field'),
(667, 'app', 'Dates'),
(668, 'app', 'in due time'),
(669, 'app', 'late'),
(670, 'app', 'overdue'),
(671, 'app', 'submitted'),
(672, 'app', 'submitted late'),
(673, 'app', 'submitted in due time'),
(674, 'app', 'Register new event'),
(675, 'app', 'Reset Grid'),
(676, 'app', 'Events list'),
(677, 'app', 'Deleted events. Click to manage.'),
(678, 'app', 'Active'),
(679, 'app', 'Inactive'),
(680, 'app', 'Disable any popup blockers in your browser to ensure proper download.'),
(681, 'app', 'Ok to proceed?'),
(682, 'app', 'Generating the export file. Please wait...'),
(683, 'app', 'Request submitted! You may safely close this dialog after saving your downloaded file.'),
(684, 'app', 'Export'),
(685, 'app', 'Export All Data'),
(686, 'app', 'Grid Export'),
(687, 'app', 'Yii2 Grid Export (PDF)'),
(688, 'app', 'Generated'),
(689, 'app', '© Krajee Yii2 Extensions'),
(690, 'app', 'HTML'),
(691, 'app', 'The HTML export file will be generated for download.'),
(692, 'app', 'Hyper Text Markup Language'),
(693, 'app', 'CSV'),
(694, 'app', 'The CSV export file will be generated for download.'),
(695, 'app', 'Comma Separated Values'),
(696, 'app', 'The TEXT export file will be generated for download.'),
(697, 'app', 'Tab Delimited Text'),
(698, 'app', 'Excel'),
(699, 'app', 'The EXCEL export file will be generated for download.'),
(700, 'app', 'Microsoft Excel 95+'),
(701, 'app', 'ExportWorksheet'),
(702, 'app', 'PDF'),
(703, 'app', 'The PDF export file will be generated for download.'),
(704, 'app', 'Portable Document Format'),
(705, 'app', 'PDF export generated by kartik-v/yii2-grid extension'),
(706, 'app', 'krajee, grid, export, yii2-grid, pdf'),
(707, 'app', 'JSON'),
(708, 'app', 'The JSON export file will be generated for download.'),
(709, 'app', 'JavaScript Object Notation'),
(710, 'app', 'Audit'),
(711, 'app', 'Event Declaration'),
(712, 'app', '{evt} Audit'),
(713, 'app/crf', 'Loss of well control/barrier'),
(714, 'app/crf', 'Failure of SECE'),
(715, 'app/crf', 'Vessel collision'),
(716, 'app/crf', 'Serious injuries to five or more persons'),
(717, 'app/crf', 'Major environmental incident'),
(718, 'app', 'View report in CRF'),
(719, 'app', '{evt, plural, =1{# month} other{# months}} '),
(720, 'app', 'and'),
(721, 'app', '{evt, plural, =1{# day} other{# days}}'),
(722, 'app', 'Time to submission'),
(723, 'app', 'Time from submission'),
(725, 'app', 'Submission dates'),
(726, 'app', 'before the deadline'),
(727, 'app', 'This report has been submitted before the deadline.'),
(728, 'app', 'CA status'),
(729, 'app', 'ACCEPTED AND ASSESSED BY THE CA.'),
(730, 'app', 'Display the report/drafts section'),
(731, 'app', 'View details'),
(732, 'app', 'Are you sure you want to delete {data}?'),
(733, 'app', 'The incident must to be reported before having this information available.'),
(736, 'app', 'field'),
(737, 'app', 'reports'),
(738, 'app', 'Reporting'),
(739, 'app', 'Edit'),
(740, 'app', 'Remove'),
(741, 'app', 'created by'),
(742, 'app', 'Status'),
(743, 'app', 'Date of submission'),
(744, 'app', 'Deadline'),
(745, 'app', 'Deadline interval'),
(746, 'app', 'Report submitted'),
(747, 'app', 'Timing'),
(748, 'app', '{evt, plural, =1{# year} other{# years} }'),
(749, 'app', '{evt, plural, =1{# hour} other{# hours}}'),
(750, 'app', '{evt, plural, =1{# minute} other{# minutes}}'),
(752, 'app', 'Change contact details'),
(753, 'app', 'SyRIO Admin'),
(754, 'app', 'System Administrator'),
(755, 'app', 'full controll over SyRIO'),
(756, 'app', 'Administrator ({evt})'),
(757, 'app', 'administrator rights over the Competent Authority section of SyRIO'),
(758, 'app', 'Incidents Assessor'),
(759, 'app', 'allows the assessment of the incident reports submitted by the Operators/Owners'),
(760, 'app', 'Registers user'),
(761, 'app', 'allows the access to the data in your Competent Authority'),
(762, 'app', 'administrator rights over your installation data'),
(763, 'app', 'Rapporteur ({evt})'),
(764, 'app', 'allows the incident reporting and management for your installation'),
(765, 'app', 'Registers user ({evt})'),
(766, 'app', 'allows the access to the reported incidents by your Installation as well as some reporting rights'),
(767, 'app', 'administrator rights over your Organization and installations data'),
(768, 'app', 'allows the incident reporting and management for your Organization and installations'),
(769, 'app', 'allows the access to the reported incidents by your Installations as well as some reporting rights'),
(773, 'app', 'Member since: '),
(779, 'app', 'Please log-in to start using the application'),
(780, 'app', 'Please fill out the following fields to login'),
(781, 'app', 'Forget password?'),
(782, 'app', 'REJECTED BY THE CA. CHECK THE REASON (in the drafts section) THEN RESUBMIT.'),
(783, 'app/crf', 'Helicopter accidents'),
(784, 'app', 'View'),
(785, 'app', 'Delete'),
(786, 'app/translation', 'This will remove language ''{evt}'' from SyRIO.'),
(787, 'app', 'The operation CANNOT be undone!'),
(788, 'app', 'Management Hub'),
(789, 'app', 'Manage users'),
(790, 'app', 'Register user'),
(791, 'app', 'Manage Organizations'),
(792, 'app', 'List of translated messages for {evt}'),
(793, 'app', 'PDF export generated by SyRIO'),
(794, 'app/translations', 'Translate'),
(796, 'app', 'Statistics'),
(797, 'app', 'Incident reports'),
(798, 'app', 'Displays the list of the incidents reported as required by Directive 2013/30/EU.'),
(799, 'app', 'Annual report'),
(800, 'app', 'Tools for drafting the CA Annual Report to be submitted to the Commission, as required by Directive 2013/30/EU.'),
(801, 'app', 'Assessment of the submitted incidents, case by case, in terms of Section 4 of the Common Publication Format.'),
(802, 'app', 'The tools for drafting the CA Annual Report. Export the Annual report as XML.'),
(803, 'app', 'Please make sure that all the reported incidents have been assessed in terms of CPF Section 4 (i.e. Annual report preparation is done and up-to-date).'),
(804, 'app', 'Your session has timed-out!'),
(805, 'app', 'You must login again to continue.'),
(806, 'app', 'Incidents recorded in {evt}:'),
(807, 'app', 'of which classified as Major Incidents:'),
(808, 'app', 'incident reports'),
(809, 'app', 'annual report preparation'),
(810, 'app', 'annual report drafting'),
(818, 'app', '{section} Assessed By'),
(819, 'app', '{section} Assessed At'),
(821, 'app', 'Rejection justification'),
(822, 'app', 'Reported incidents'),
(823, 'app', 'MA / CPF'),
(824, 'app', 'Major Accident'),
(825, 'app', 'CPF Section 4'),
(826, 'app', 'MA Assessment'),
(827, 'app', 'CPF Assessment'),
(828, 'app', 'Operator'),
(829, 'app', 'name or type'),
(830, 'app', 'Reset filters'),
(832, 'app', 'Assessment as Major Accident'),
(833, 'app', 'Report {evt}.'),
(834, 'app', 'Event categorized as {evt}'),
(835, 'app', 'The report is not included in the list to be assessed in terms of CPF Section 4.'),
(836, 'app', '<br/>Please <strong>Refresh</strong> the list in <em>Annual report preparation</em>'),
(837, 'app', 'Assessment in terms of CPF Section 4'),
(838, 'app', 'minor'),
(839, 'app', 'not included in the assessment list'),
(840, 'app', 'Reporting dates'),
(841, 'app', 'The report {evt} in terms of CPF Section 4.'),
(842, 'app', 'major'),
(843, 'app', 'assessed'),
(844, 'app', 'This report is rejected. No need to be assessed for CPF.'),
(845, 'app', 'rejected'),
(846, 'app', 'no need to assess. report rejected'),
(847, 'app', 'Report accepted'),
(848, 'app', 'True if report Section {evt} is categorized as major'),
(849, 'app', 'Justification if Section {evt} is categorized as major'),
(850, 'app', 'Report Submitted At'),
(851, 'app', 'Minor Incident'),
(852, 'app', 'Directive 2013/30/EU'),
(853, 'app', 'Regulation (EU) No 1112/2014 of 13 October 2014'),
(854, 'app', 'The following is the list of all the off-shore events in your jurisdiction, reported by the Operator/Owners according to {evt1} and {evt2}.'),
(855, 'app', 'assessment'),
(856, 'app', 'About'),
(857, 'app', 'Contact'),
(859, 'app', 'Re-assess'),
(860, 'app', '<i class="fa fa-file-pdf-o"></i> PDF'),
(861, 'app/crf', 'The coming into operation of a blowout prevention or diverter system to control flow of well-fluids;'),
(864, 'app', 'Page intentionally left blank'),
(865, 'app', 'by'),
(868, 'app/crf', 'Fatal accidents'),
(869, 'app/crf', 'Unintended releases'),
(870, 'app/crf', 'Loss of structural integrity'),
(871, 'app/crf', 'Evacuation of personnel'),
(873, 'app/crf', 'within {evt} working days from the event'),
(874, 'app/crf', 'Module ventilation'),
(875, 'app/crf', 'Was there a non-carbon fire (e.g. electrical) with a significant potential to cause a major accident?'),
(877, 'app/crf', 'Description of consequences'),
(878, 'app/crf', 'Vessels on collision course and actual vessels collisions with an offshore installation'),
(882, 'app', 'if known'),
(885, 'app/crf', 'Operational mode in the area at the time of release'),
(886, 'app/crf', 'Ignition was:'),
(890, 'app/crf', '<u>if</u> known'),
(895, 'app/crf', 'Process containment release systems'),
(899, 'app/crf', 'Report independent verifier (report nr. / date / verifier)'),
(902, 'app', 'specify'),
(903, 'app/crf', 'C.2.2. Description of consequences'),
(909, 'app/crf', 'Failing system and events description'),
(915, 'app/crf', 'If yes, fill in the following sections'),
(916, 'app', 'Tick appropriate box'),
(917, 'app/crf', 'Level of H'),
(920, 'app/crf', 'Not taken'),
(921, 'app/crf', 'At stations'),
(922, 'app/crf', 'At lifeboats'),
(924, 'app', 'quantity'),
(925, 'app/crf', 'If yes, outline the environmental impacts which already have been observed or are likely to result from the incident'),
(929, 'app/crf', 'Well head completion'),
(930, 'app/crf', 'Water depth'),
(931, 'app/crf', 'Type of activity'),
(935, 'app/crf', 'Origin'),
(936, 'app/crf', 'Ignition control systems'),
(937, 'app/crf', 'Process containment systems'),
(962, 'app/crf', 'not ignited natural gas or evaporated associated gas if mass released >= 1 kg'),
(963, 'app/crf', 'not ignited liquid of petroleum hydrocarbon if mass released >= 60 kg'),
(967, 'app/crf', 'If'),
(975, 'app/crf', 'm'),
(977, 'app/crf', 'CO'),
(983, 'app', 'Incident Report'),
(984, 'app', 'Assessment of {modelClass}'),
(985, 'app', 'Major Accident assessment'),
(986, 'app', 'Please revise Section {section}.'),
(999, 'app', 'Save draft'),
(1000, 'app', 'Finalize'),
(1001, 'app', 'Draft saved for {evt}!'),
(1002, 'app', 'Continue assessment'),
(1006, 'app', 'Event assessed successfully!'),
(1007, 'app', 'You may now proceed with assessing the event in terms of Section 4 of the Annual Report.'),
(1008, 'app', 'For this go to Home > Annual report preparation then click Refresh.'),
(1009, 'app/cpf', 'Incidents CPF Preparation'),
(1010, 'app', 'How to'),
(1011, 'app/cpf', 'Refresh'),
(1012, 'app/cpf', 'Year'),
(1014, 'app/cpf', 'Report ID'),
(1141, 'app', 'Assessment Status'),
(1143, 'app/cpf', 'Last Refresh At'),
(1144, 'app/cpf', 'Last Refresh By'),
(1155, 'app/cpf', 'Unintended releases - Total - {evt}'),
(1156, 'app/cpf', 'Ignited oil/gas releases - Fires - {evt}'),
(1157, 'app/cpf', 'Ignited oil/gas releases - Explosions - {evt}'),
(1158, 'app/cpf', 'Not ignited gas releases - {evt}'),
(1159, 'app/cpf', 'Not ignited oil releases - {evt}'),
(1160, 'app/cpf', 'Hazardous substances releases - {evt}'),
(1161, 'app/cpf', 'Loss of well - Total - {evt}'),
(1162, 'app/cpf', 'Blowouts - {evt}'),
(1163, 'app/cpf', 'Blowout / diverter activation - {evt}'),
(1164, 'app/cpf', 'Well barrier failure - {evt}'),
(1165, 'app/cpf', 'Failures of SECE - {evt}'),
(1166, 'app/cpf', 'Loss of structural integrity - total - {evt}'),
(1167, 'app/cpf', 'Loss of structural integrity - {evt}'),
(1168, 'app/cpf', 'Loss of stability/buoyancy - {evt}'),
(1169, 'app/cpf', 'Loss of station keeping - {evt}'),
(1170, 'app/cpf', 'Vessel collisions - total - {evt}'),
(1171, 'app/cpf', 'Helicopter accidents - {evt}'),
(1172, 'app/cpf', 'Fatal accidents - {evt}'),
(1173, 'app/cpf', 'Serious injuries of 5 or more persons in the same accident - {evt}'),
(1174, 'app/cpf', 'Evacuation of personnel - {evt}'),
(1175, 'app/cpf', 'Environmental accidents - {evt}'),
(1176, 'app/cpf', 'Total number of fatalities and injuries - {evt}'),
(1177, 'app/cpf', 'Number of fatalities - {evt}'),
(1178, 'app/cpf', 'Total number of injuries - {evt}'),
(1179, 'app/cpf', 'Total number of serious injuries - {evt}'),
(1180, 'app/cpf', 'Structural integrity systems - {evt}'),
(1181, 'app/cpf', 'Process containment systems - {evt}'),
(1182, 'app/cpf', 'Ignition control systems - {evt}'),
(1183, 'app/cpf', 'Detection control systems - {evt}'),
(1184, 'app/cpf', 'Process containment relief systems - {evt}'),
(1185, 'app/cpf', 'Protection systems - {evt}'),
(1186, 'app/cpf', 'Shutdown systems - {evt}'),
(1187, 'app/cpf', 'Navigational aids - {evt}'),
(1188, 'app/cpf', 'Rotating equipment - {evt}'),
(1189, 'app/cpf', 'Escape, evacuation and rescue equipment - {evt}'),
(1190, 'app/cpf', 'Communication systems - {evt}'),
(1191, 'app/cpf', 'other - {evt}'),
(1192, 'app/cpf', 'Incidents caused by Equipment failure - Total - {evt}'),
(1193, 'app/cpf', 'Design failures - {evt}'),
(1194, 'app/cpf', 'Internal corrosion - {evt}'),
(1195, 'app/cpf', 'External corrosion - {evt}'),
(1196, 'app/cpf', 'Mechanical failures due to fatigue - {evt}'),
(1197, 'app/cpf', 'Mechanical failures due to wear-out - {evt}'),
(1198, 'app/cpf', 'Mechanical failures due to defected material - {evt}'),
(1199, 'app/cpf', 'Mechanical failures (vessel/helicopter) - {evt}'),
(1200, 'app/cpf', 'Instrument failures - {evt}'),
(1201, 'app/cpf', 'Control system failures - {evt}'),
(1202, 'app/cpf', 'Human error-operational causes - total - {evt}'),
(1203, 'app/cpf', 'Operation error - {evt}'),
(1204, 'app/cpf', 'Maintenance error - {evt}'),
(1205, 'app/cpf', 'Testing error - {evt}'),
(1206, 'app/cpf', 'Inspection error - {evt}'),
(1207, 'app/cpf', 'Design error - {evt}'),
(1209, 'app/cpf', 'Inadequate risk assessment/perception - {evt}'),
(1210, 'app/cpf', 'Inadequate instruction/procedure - {evt}'),
(1211, 'app/cpf', 'Non-compliance with procedure - {evt}'),
(1212, 'app/cpf', 'Non-compliance with permit-to-work - {evt}'),
(1213, 'app/cpf', 'Inadequate communication - {evt}'),
(1214, 'app/cpf', 'Inadequate personnel competence - {evt}'),
(1215, 'app/cpf', 'Inadequate supervision - {evt}'),
(1216, 'app/cpf', 'Inadequate safety leadership - {evt}'),
(1217, 'app/cpf', 'Most important lessons learned - {evt}'),
(1218, 'app/cpf', 'Weather-related causes - total - {evt}'),
(1219, 'app/cpf', 'Wind in excess of limits of design - {evt}'),
(1220, 'app/cpf', 'Waves in excess of limits of design - {evt}'),
(1221, 'app/cpf', 'Extremely low visibility in excess of limits of design - {evt}'),
(1222, 'app/cpf', 'Presence of ice or icebergs - {evt}'),
(1223, 'app', 'new'),
(1224, 'app', 'in progress'),
(1225, 'app', 'finalized'),
(1226, 'app/cpf/help', 'For being accounted in the '),
(1227, 'app/cpf/help', 'drafting of the CPF annual report'),
(1228, 'app/cpf/help', 'each incident (previously assessed in terms of Major Incident) must be analysed in terms of qualifying in one or more failure categories that are part of the CPF''s Section 4.'),
(1229, 'app/cpf/help', 'In SYRIO, this process is referred to as the <b>{evt}</b> of an incident.'),
(1230, 'app/cpf/help', 'Once done, the incident must be marked as ready, i.e. <kbd>Finalized</kbd>.'),
(1231, 'app/cpf/help', 'Only the incidents who''s CPF preparation phase is <kbd>Finalized</kbd> are taken into account in the CPF drafting phase!'),
(1232, 'app/cpf/help', 'On the right-hand side you may find the list of incidents and their status in respect to the CPF Section 4 assessment.'),
(1233, 'app', 'Your tasks'),
(1234, 'app', 'Please note'),
(1235, 'app', 'Re-open'),
(1236, 'app/cpf', 'Reopen assessment?'),
(1237, 'app/cpf', 'CPF Section 4 Incident Assessment'),
(1238, 'app/cpf', 'Design failure has been reported as one of the causes of the release (see Section A.1 Cause of leak checklist (a))'),
(1239, 'app/cpf', 'Internal corrosion has been reported as one of the causes of the release (see Section A.1 Cause of leak checklist (b))'),
(1240, 'app/cpf', 'External corrosion has been reported as one of the causes of the release (see Section A.1 Cause of leak checklist (b))'),
(1241, 'app/cpf', 'Mechanical failure due to fatigue has been reported as one of the causes of the release (see Section A.1 Cause of leak checklist (b))'),
(1242, 'app/cpf', 'Mechanical fatigue due to wear out has been reported as one of the causes of the release (see Section A.1 Cause of leak checklist (b))'),
(1243, 'app/cpf', 'Material defect has been reported as one of the causes of the release (see Section A.1 Cause of leak checklist (b))'),
(1244, 'app/cpf', 'At least one of: Improper operation, Left open, Opened when containing HC, Dropped object, has been reported as one of the causes of the release (see Section A.1 Cause of leak checklist (c))'),
(1245, 'app/cpf', 'Improper maintenance has been reported as one of the causes of the release (see Section A.1 Cause of leak checklist (c))'),
(1246, 'app/cpf', 'Improper testing has been reported as one of the causes of the release (see Section A.1 Cause of leak checklist (c))'),
(1247, 'app/cpf', 'Improper inspection has been reported as one of the causes of the release (see Section A.1 Cause of leak checklist (c))'),
(1248, 'app/cpf', 'Other or Erosion has been reported as one of the (equipment) causes of the release (see Section A.1 Cause of leak checklist (b))'),
(1249, 'app/cpf', 'Deficient procedure has been reported as one of the causes of the release (see Section A.1 Cause of leak checklist (d))'),
(1250, 'app/cpf', 'Non-compliance with procedure has been reported as one of the causes of the release (see Section A.1 Cause of leak checklist (d))'),
(1251, 'app/cpf', 'Non-compliance with permit-to-work has been reported as one of the causes of the release (see Section A.1 Cause of leak checklist (d))'),
(1252, 'app/cpf', 'Other has been reported as one of the (procedural) causes of the release (see Section A.1 Cause of leak checklist (d))'),
(1253, 'app/cpf', 'Failures of Safety and Environmental Critical Elements'),
(1255, 'app/cpf', 'Number of major accidents'),
(1256, 'app/cpf', 'Unintended releases - Total'),
(1257, 'app/cpf', 'Hazardous substances releases'),
(1258, 'app/cpf', 'Loss of well - Total'),
(1259, 'app/cpf', 'Blowout / diverter activation'),
(1260, 'app/cpf', 'Well barrier failure'),
(1261, 'app/cpf', 'Loss of structural integrity - total'),
(1263, 'app/cpf', 'Vessel collisions - total'),
(1264, 'app/cpf', 'Serious injuries of 5 or more persons in the same accident'),
(1265, 'app/cpf', 'Number of fatalities'),
(1266, 'app/cpf', 'Detection control systems');
INSERT INTO `t_source_message` (`id`, `category`, `message`) VALUES
(1267, 'app/cpf', 'Rotating equipment'),
(1268, 'app/cpf', 'Communication systems'),
(1269, 'app/cpf', 'Equipment failures - Total'),
(1270, 'app/cpf', 'Mechanical failures (vessel/helicopter)'),
(1271, 'app/cpf', 'Instrument failures'),
(1272, 'app/cpf', 'Control system failures'),
(1273, 'app/cpf', 'Human error-operational causes - total'),
(1275, 'app/cpf', 'Non-compliance with procedure'),
(1276, 'app/cpf', 'Most important lessons learned'),
(1277, 'app/cpf', 'Weather-related causes - total'),
(1278, 'app/cpf', 'Waves in excess of limits of design'),
(1279, 'app/cpf', 'Presence of ice or icebergs'),
(1280, 'app', 'can''t tell'),
(1281, 'app/cpf', 'Structural integrity systems'),
(1282, 'app/cpf', 'Shutdown systems'),
(1283, 'app/cpf', 'Protection systems'),
(1285, 'app/cpf', 'Escape, evacuation and rescue equipment'),
(1286, 'app/cpf', 'other'),
(1287, 'app/cpf', 'Internal corrosion'),
(1288, 'app/cpf', 'External corrosion'),
(1290, 'app/cpf', 'Non-compliance with permit-to-work'),
(1291, 'app', 'you'),
(1292, 'app', 'computed'),
(1293, 'app', 'event report'),
(1294, 'app', 'event type'),
(1295, 'app/cpf', 'Procedural / organizational error - {evt}'),
(1296, 'app', 'Update'),
(1297, 'app', 'Success'),
(1298, 'app', 'You have reopened ''{evt}''. The event WILL NOT be accounted in the Annual Report Builder.'),
(1299, 'app/cpf', 'You already have a report registered for this year.'),
(1301, 'app/cpf', 'Oil production unit'),
(1302, 'app/cpf', 'Gas production unit'),
(1303, 'app/cpf', 'Man-days spent on installation'),
(1304, 'app/cpf', '(number of) Major accidents'),
(1305, 'app/cpf', '(number of) Environmental concerns'),
(1306, 'app', 'You have {evt} new records found.'),
(1307, 'app/cpf', 'Finalize assessment?'),
(1308, 'app', 'You have marked ''{evt}'' as finalized. The report will be accounted in the Annual Report Builder'),
(1309, 'app', 'Annual reports'),
(1311, 'app', 'Only one Annual Report per year is available.'),
(1312, 'app', 'Notes'),
(1313, 'app', 'Create Annual Report'),
(1314, 'app', 'The report {evt} in terms of Major accident.'),
(1315, 'app', 'unassessed'),
(1316, 'app', 'Create {evt}'),
(1317, 'app/cpf', 'Please provide the year for the report.'),
(1319, 'app', 'INSTALLATIONS'),
(1320, 'app', 'at least one incident'),
(1321, 'app', 'Only the installations that have reported {evt} over the reporting period are taking into account.'),
(1322, 'app', 'Adding the rest of the installations is possible after uploading this report (XML file) on the SPIROS platform.'),
(1324, 'app/cpf', 'Please provide detailed list of installations for offshore oil and gas operation in your country (on first of January of the reported year).'),
(1326, 'app/cpf', '(lon - lat)'),
(1329, 'app/cpf', 'Geographical area of operations; and Duration'),
(1330, 'app/cpf', 'Oil production (norm)'),
(1332, 'app/cpf', 'Gas production (norm)'),
(1334, 'app/cpf', 'Main enforcement actions'),
(1342, 'app/cpf', '(reported number)'),
(1343, 'app/cpf', '(events/hours worked)'),
(1344, 'app/cpf', '(events/kTOE)'),
(1345, 'app/cpf', 'Provided in {evt} of this document'),
(1347, 'app/messages/errors', 'Unable to execute request!'),
(1348, 'app/messages/errors', 'Check the information below and try again.'),
(1349, 'app/messages/errors', 'At least one operational area must be provided for each mobile installation'),
(1350, 'app/messages/errors', 'Please provide the coordinates for each fixed installation'),
(1351, 'app/messages', 'Installations list has been successfuly refreshed from the incidents list'),
(1352, 'app/messages', 'The following have occured:'),
(1353, 'app/messages', 'New installations'),
(1354, 'app/messages', 'Skipped'),
(1355, 'app/messages', 'Removed'),
(1356, 'app', '{evt} updated'),
(1357, 'app/messages', 'Installations list'),
(1358, 'app', '{modelClass}: '),
(1360, 'app/cpf', 'operation area'),
(1361, 'app', 'area name'),
(1362, 'app', 'duration in months'),
(1363, 'app/cpf', 'Production data'),
(1364, 'app/cpf', '{evt} production'),
(1365, 'app', 'hours'),
(1366, 'app/cpf', 'Generate SPIROS Xml?'),
(1367, 'app', 'Build SPIROS XML'),
(1368, 'app', 'Unable to build the xml! Please review the information below and try again.'),
(1369, 'app', 'Your password has been successfully modified.'),
(1370, 'app/cpf', 'You must provide a value for {evt} ({sec})'),
(1371, 'app', '{modelClass}: Update'),
(1372, 'app/cpf', 'Url'),
(1373, 'app', 'reference'),
(1376, 'app', 'title'),
(1378, 'app', '{evt, plural, =1{# reference} other{# references} }'),
(1380, 'app/cpf', 'Please add any reference related to the information in Section 3.4. above (if any)'),
(1381, 'app', 'Page not found'),
(1382, 'app', 'From the list of titles currently available(*) tick the ones you want to use as reference in Section 3.4. of the current CRF.'),
(1383, 'app', 'If you cannot find the title within the provided list, use {evt} to register a new document.'),
(1384, 'app', 'reference title'),
(1385, 'app', 'reference url (if any)'),
(1386, 'app', 'Available titles'),
(1387, 'app', 'here'),
(1388, 'app', 'Click {evt} to manage the registered documents.'),
(1389, 'app', '{evt, plural,=0{not referenced} =1{# Annual Report} other{# Annual Reports}}'),
(1390, 'app/cpf', 'Update {modelClass}: '),
(1391, 'app', 'referenced in'),
(1392, 'app/error', 'Restricted operation'),
(1393, 'app/error', 'You do not have enough credentials to perform this operation.'),
(1394, 'app/error', 'Please login with a different account; or'),
(1395, 'app/error', 'Contact the SyRIO administrator to request elevating your rights.'),
(1396, 'app/error', 'Please login first.'),
(1397, 'app', 'non-major'),
(1398, 'app', 'not required'),
(1399, 'app', 'not available'),
(1400, 'app', 'List of source messages'),
(1401, 'app', 'unit'),
(1402, 'app', '''{evt}'' has been successfully {evt2}.'),
(1403, 'app', 'Incorrect username or password.'),
(1407, 'app', 'Use these forms for requesting the registration of installation in SyRIO.'),
(1409, 'app', 'Use these forms for registering to SyRIO.'),
(1413, 'app', 'Use one of the options below to send a free text message.'),
(1414, 'app', 'SyRIO administrators'),
(1416, 'app', 'Register new {evt} Organization'),
(1418, 'app', 'Register as {evt} user'),
(1419, 'app', 'New Competent Authority organization'),
(1420, 'app', 'Organization registration form'),
(1421, 'app', 'Fill-in this form to submit a {evt} Organization registration request.'),
(1423, 'app', 'General'),
(1424, 'app', 'Information about your organization'),
(1425, 'app', 'The full name of the organization'),
(1426, 'app', 'Organization Name'),
(1427, 'app', 'Organization Acronym'),
(1428, 'app', 'Organization Phone'),
(1429, 'app', 'Organization Email'),
(1430, 'app', 'Organization Fax'),
(1431, 'app', 'Organization Phone2'),
(1432, 'app', 'Organization Web'),
(1435, 'app', 'City'),
(1436, 'app', 'County'),
(1437, 'app', 'Zip'),
(1438, 'app', 'Verification code'),
(1439, 'app', 'First name'),
(1440, 'app', 'Last name'),
(1441, 'app', 'Repeat email'),
(1442, 'app', 'The acronym of the organization'),
(1443, 'app', 'Organization address'),
(1444, 'app', 'Requester details'),
(1445, 'app', 'State your personal details below.'),
(1446, 'app', 'Please be adviced that the information below might be used with the Organization to be registered for verification purposes.'),
(1447, 'app', 'The person responsible for (being informed about) the request status is you.'),
(1448, 'app', 'Either the full name or the acronym of your organization'),
(1449, 'app', 'Your role within the organization'),
(1451, 'app', 'Once submitted, your request will be assessed by the system administrators. You will be informed by email once your request is processed.'),
(1452, 'app', 'Street'),
(1453, 'app', 'Street (line 2)'),
(1455, 'app', 'Inoperative'),
(1456, 'app', 'Decommissioned'),
(1457, 'app', 'Operative'),
(1458, 'app', 'Installation registration form'),
(1459, 'app', 'Fill-in this form to submit an {evt} registration request.'),
(1461, 'app', 'Information about installation'),
(1462, 'app', 'The name or acronym of the Installation''s Operator/Owner organization'),
(1463, 'app', 'Name of installation'),
(1464, 'app', 'Type (if other)'),
(1465, 'app', 'Flag (if MODU)'),
(1466, 'app', 'Operational status'),
(1467, 'app', 'Contact phone (installation)'),
(1468, 'app', 'Contact email (installation)'),
(1469, 'app', 'Contact fax (installation)'),
(1470, 'app', 'The name of the installation'),
(1471, 'app', 'installation type'),
(1472, 'app', 'The type of the installation'),
(1473, 'app', 'registration country'),
(1474, 'app', 'Flag of the installation (if MODU)'),
(1475, 'app', 'e.g. FPSO'),
(1476, 'app', 'Installation registration code (IMO Number if existing)'),
(1477, 'app', 'at the time of this request'),
(1478, 'app/extras', 'installation'),
(1481, 'app', 'Administrator'),
(1482, 'app', 'Assessor'),
(1483, 'app', 'Member'),
(1484, 'app', 'User registration form'),
(1485, 'app', 'Fill-in this form to submit an {evt} user registration request.'),
(1487, 'app', 'At least one role must be selected.'),
(1488, 'app', 'A valid role must be selected.'),
(1489, 'app', 'Personal'),
(1490, 'app', 'Personal information and contact data'),
(1491, 'app', 'Roles'),
(1492, 'app', 'Which roles would you like to have in SyRIO'),
(1493, 'app', 'all fields are compulsory'),
(1494, 'app/extras', 'Competent Authority'),
(1496, 'app', 'Rapporteur'),
(1498, 'app', 'Name of your installation'),
(1499, 'app', 'Installation name'),
(1500, 'app', 'Either the full name or the acronym of the owner/operator of the installation'),
(1501, 'app', 'Your role within the installation'),
(1502, 'app', 'If you have business inquiries or other questions, please fill out the following form to contact us. Thank you.'),
(1503, 'app', 'Subject'),
(1504, 'app', 'Body'),
(1505, 'app', 'Thank you for contacting us. We will respond to you as soon as possible.'),
(1507, 'app', 'Operations in this section allows you to control the events registered by your organization.'),
(1508, 'app', 'Modify'),
(1510, 'app/crf', 'Installation name/type'),
(1511, 'app/crf', 'Additional compulsory information'),
(1512, 'app/crf', 'The information below is required for SyRIO only.'),
(1513, 'app/crf', 'None of the information will be part of the CRF report.'),
(1514, 'app', 'taken from user credentials'),
(1515, 'app/crf', 'Please state the name and/or type of the installation'),
(1516, 'app/crf', 'The name or code of the installation operation field (if relevant)'),
(1517, 'app/crf', 'Incident date and time must be provided!'),
(1518, 'app/crf', 'Any incident must be linked to an installation!'),
(1519, 'app/messages', 'Saved as draft'),
(1520, 'app/messages', 'drafting the incident report'),
(1521, 'app/messages', 'Incident ''{evt}'' has been successfully registered.\\nPlease proceed with {evt2}.'),
(1522, 'app', 'Undo delete'),
(1523, 'app', 'Are you sure you want to PERMANENTLY delete {data}?'),
(1524, 'app', 'Deleted events'),
(1525, 'app', 'Trash'),
(1526, 'app', 'Events list (trash)'),
(1529, 'app/messages/errors', 'There''s been an error while processing your request.'),
(1530, 'app/messages/errors', 'Please retry later.'),
(1532, 'app', 'added'),
(1534, 'app', 'deleted'),
(1536, 'app/messages/errors', 'Sorry for any inconvenience.'),
(1537, 'app/messages/errors', 'The data provided has validation errors.'),
(1538, 'app/messages/errors', 'Please revise and retry.'),
(1539, 'app/messages', 'the item'),
(1540, 'app', 'the incident'),
(1541, 'app/messages', '{evt} has been purged (completely removed)!'),
(1542, 'app', 'Event reporting'),
(1543, 'app', 'Report drafts list'),
(1545, 'app', 'Deleted drafts. Click to manage.'),
(1557, 'app', 'Reason for rejection'),
(1558, 'app', 'Submission status'),
(1559, 'app/commands', 'Purge'),
(1560, 'app', 'still drafting'),
(1561, 'app/messages', '{evt} has been successfully removed.'),
(1562, 'app/messages', '{evt} has been successfully reactivated.'),
(1565, 'app', 'Please provide a short description of this draft.'),
(1566, 'app', 'More than one option might be chosen'),
(1567, 'app', 'Selection guidance'),
(1568, 'app', 'draft'),
(1569, 'app', 'Review all'),
(1570, 'app', 'Goto {evt}'),
(1576, 'app', 'Other sections in this report'),
(1577, 'app', 'Finalize {evt}'),
(1640, 'app', 'LOCKED'),
(1641, 'app', 'by {evt1} since {evt2}'),
(1642, 'app', 'signed'),
(1643, 'app', 'Update {evt}'),
(1644, 'app', 'Event Drafts'),
(1646, 'app', 'PARTIALLY FINALIZED'),
(1647, 'app', 'FINALIZED. READY TO BE SIGNED'),
(1648, 'app', 'SIGNED. Can be submitted.'),
(1649, 'app', 'REPORTED TO THE CA. PENDING FOR ACCEPTANCE.'),
(1650, 'app', 'REJECTED BY THE CA. CHECK THE REASON THEN RESUBMIT.'),
(1652, 'app', 'NOT SUBMITTED.'),
(1653, 'app', 'Your report has been rejected by the Competent Authority, with the following justification.'),
(1654, 'app', ' at'),
(1655, 'app', 'Deleted drafts'),
(1656, 'app', 'Force unlock sections'),
(1657, 'app', 'Sections of draft ''{evt}'' have been unlocked. You may proceed to edit.'),
(1660, 'app', 'Locked. Someone is currently working on it.'),
(1661, 'app', 'Unlock'),
(1662, 'app', 'Force unlock?'),
(1664, 'app', 'General description'),
(1665, 'app', 'existence of a release of non-hydrocarbon hazardous substance'),
(1666, 'app', 'released quantity'),
(1667, 'app', 'released quantity unit'),
(1668, 'app', 'presence of non-hydrocarbon fire'),
(1669, 'app', 'likeness of degradation of marine environment'),
(1670, 'app', 'observed and foreseen environmental impacts'),
(1671, 'app', 'Initial lessons learned and preliminary recommendations'),
(1673, 'app', 'Finalize Section {evt}'),
(1674, 'app', 'Section {evt} could not be finalized! Please review the information below and try again.'),
(1676, 'app/messages/errors', 'Section {evt} failed validation. Please revise.'),
(1677, 'app/crf', 'Number of sides enclosed'),
(1678, 'app/crf', 'Description of other relevant weather conditions'),
(1679, 'app/crf', 'Other means of detection'),
(1680, 'app/crf', 'Cause of leak Description'),
(1681, 'app/crf', 'Ignition type'),
(1686, 'app/crf', 'Name of drilling contractor'),
(1688, 'app', 'Section {evt} - Drafting'),
(1689, 'app', 'Please fill in the following sections'),
(1690, 'app', 'Section has been successfully modified.'),
(1691, 'app', '(heading, decimal degrees)'),
(1694, 'app', 'reopen'),
(1695, 'app', 'Reopen section?'),
(1697, 'app/crf', 'Section {evt} has been successfully ''finalized''.'),
(1698, 'app/crf', 'You may proceed with drafting the remaining sections of the Incident Report'),
(1699, 'app/crf', 'The Incident Report ''{evt}'' is ready to be signed.'),
(1700, 'app', 'Name/code of the well'),
(1701, 'app', 'Start date/time of loss of well control'),
(1702, 'app', 'End date/time of loss of well control'),
(1703, 'app', 'Type of fluid: brine / oil / gas'),
(1705, 'app', 'Reservoir pressure'),
(1706, 'app', 'Reservoir pressure unit'),
(1707, 'app', 'Reservoir temperature'),
(1708, 'app', 'Reservoir temperature unit'),
(1709, 'app', 'Reservoir depth'),
(1710, 'app', 'Reservoir depth unit'),
(1711, 'app', 'Type of well services'),
(1712, 'app', 'Pressure'),
(1713, 'app', 'Temperature'),
(1714, 'app', 'Depth'),
(1715, 'app', 'use Event date time as start date'),
(1722, 'app', 'Flowrate unit'),
(1726, 'app/crf', 'Topside_structures'),
(1727, 'app', 'Provide a value for the other system that failed.'),
(1730, 'app', 'You must outline the observed environmental impacts.'),
(1733, 'app/crf', 'Name/code of the vessel'),
(1734, 'app/crf', 'Type/tonnage of the vessel'),
(1735, 'app', 'Section modified'),
(1736, 'app', 'Unable to save changes'),
(1737, 'app', 'The requested page does not exist.'),
(1739, 'app', 'Start date/time of evacuation'),
(1740, 'app', 'End date/time of evacuation'),
(1742, 'app', 'non-negative integer'),
(1743, 'app', 'e.g. helicopter'),
(1744, 'app', 'add text'),
(1745, 'app', 'positive integer'),
(1746, 'app/crf', 'A major environmental accident'),
(1749, 'app', 'Are you sure you want to re-open this item?'),
(1750, 'app', 'Sign'),
(1751, 'app', 'You are about to sign the incident report. This is the last step before submitting to the CA.'),
(1753, 'app/crf', 'Updated'),
(1754, 'app/crf', 'Taking into account Member States'' obligations to maintain or achieve Good Environmental Status under Directive 2008/56/EC , if an unintended release of oil, gas or other hazardous substance, or the failure of a safety and environmental critical element results in or is likely to result in degradation of the environment, such impacts should be reported to the competent authorities'),
(1755, 'app', 'Failing well barrier ({evt})'),
(1756, 'app/cpf', 'Navigational aids'),
(1757, 'app', 'Please provide a justification for the choice for Section {evt}.'),
(1758, 'app/crf', 'Signed by'),
(1759, 'app/crf', 'Signed at'),
(1760, 'app/crf', 'CA rejection justification'),
(1762, 'app/crf', 'Any serious injuries to five or more persons in the same accident to be reported under the requirements of Directive 92/21/EEC'),
(1765, 'app/crf', 'none'),
(1766, 'app/crf', 'Non-compliance with permit of work'),
(1769, 'app/crf', 'Evacuation type'),
(1771, 'app', 'Finalizing Report'),
(1772, 'app', 'The incident report could not be finalized! Please review the information below and try again.'),
(1773, 'app', 'The incident report has been finalized. You may now proceed with signing the report.'),
(1774, 'app', 'Sign Incident Report'),
(1775, 'app/crf', 'Reported by'),
(1776, 'app', 'A new incident has been reported.'),
(1777, 'app', 'Reporting status'),
(1778, 'app', 'Expected deadline'),
(1779, 'app', 'Incident details'),
(1780, 'app', 'This is an automatic message. Please do not reply.'),
(1781, 'app', 'The report has been previously rejected with the following justification'),
(1782, 'app', 'New Offshore Incident Reported'),
(1783, 'app', 'Incident {evt} has been resubmitted.'),
(1784, 'app', '(of the first submission)'),
(1785, 'app', '(from the first submission deadline)'),
(1787, 'app', 'Opened in read-only mode.'),
(1788, 'app', 'Locked by {evt1} since {evt2}.'),
(1792, 'app/data', 'brine'),
(1793, 'app/data', 'oil'),
(1794, 'app/data', 'gas'),
(1796, 'app/data', 'surface'),
(1797, 'app/data', 'subsea'),
(1798, 'app/data', 'normal production'),
(1799, 'app/data', 'work over'),
(1800, 'app/data', 'well services'),
(1801, 'app/data', 'drilling'),
(1802, 'app/data', 'wire line'),
(1803, 'app/data', 'coiled tubing'),
(1804, 'app/data', 'snubbing'),
(1806, 'app', 'SECTION {evt}'),
(1808, 'app/messages', 'To leave this page please use the buttons at the bottom the page.'),
(1809, 'app', 'Submit Incident Report to the CA'),
(1810, 'app', 'review'),
(1812, 'app', 'Submit Incident Report'),
(1813, 'app', 'You are about to submit the Incident Report to the Competent Authority'),
(1814, 'app', 'Please read carefully the information below and choose Submit if agree, Cancel otherwise.'),
(1815, 'app', 'What happens next'),
(1816, 'app', 'Once the Incident report is submitted:'),
(1817, 'app', 'The Competent Authority is informed about the event;'),
(1818, 'app', 'You (or any member of your Organization):'),
(1819, 'app', '<b>will</b> be able to view the reported incident'),
(1823, 'app', 'Information to be submitted'),
(1824, 'app', 'Selected draft to submit'),
(1825, 'app', '<b>will not</b> be able to modify the information reported'),
(1826, 'app', '<b>will not</b> be able to delete the information reported'),
(1827, 'app', 'a) contact the system administrator (through SyRIO)'),
(1828, 'app', 'will not be submitted'),
(1829, 'app', 'Details of the person reporting the event will be updated from your account information!'),
(1832, 'app', 'to deadline'),
(1834, 'app', '{evt} HAS BEEN SUBMITTED TO THE COMPETENT AUTHORITY.'),
(1835, 'app', 'no need to assess. report unassessed as MA.'),
(1836, 'app', 'must be assessed first'),
(1837, 'app', 'Management'),
(1839, 'app', 'catgorization of the event'),
(1840, 'app', 'Modified'),
(1841, 'app', 'Fax'),
(1842, 'app', 'Phone (alt.)'),
(1843, 'app', 'Web'),
(1844, 'app', 'Full name'),
(1847, 'app', 'Enter phone number'),
(1848, 'app', 'Alternate phone number (optional)'),
(1849, 'app', 'Enter fax number (optional)'),
(1850, 'app', 'Enter email address'),
(1851, 'app', 'Enter Organization homepage (optional)'),
(1852, 'app', 'Enter street name'),
(1853, 'app', 'Street1'),
(1854, 'app', 'Street2'),
(1855, 'app', 'Enter street name (line 2, optional)'),
(1856, 'app', 'Enter city'),
(1857, 'app', 'Enter county (optional)'),
(1859, 'app', 'Enter zip (optional)'),
(1860, 'app', 'please enter street name, nr...'),
(1861, 'app', 'Search'),
(1862, 'app', 'Installations in jurisdiction'),
(1867, 'app', 'Mobile'),
(1868, 'app', 'Rule Name'),
(1869, 'app/cpf', 'Decommissioning type'),
(1870, 'app', 'Addresses'),
(1871, 'app', 'Assess'),
(1872, 'app', 'Reject'),
(1874, 'app', 'enter text...'),
(1875, 'app', 'Please provide justification for rejecting the incident report.'),
(1876, 'app', 'Your justification will be sent to the Operator/Owner who submitted the report.'),
(1877, 'app', 'Reject incident report'),
(1878, 'app', 'Justification must be provided when rejecting an incident report.'),
(1879, 'app', 'This report has been rejected with the following justification:'),
(1880, 'app', 'Incident report rejected!'),
(1881, 'app', 'You have rejected the incident report.'),
(1882, 'app', 'open'),
(1883, 'app', 'accepted'),
(1884, 'app', 'is NOT assessed'),
(1885, 'app', 'HAS BEEN ASSESSED'),
(1892, 'app', 'Old password'),
(1893, 'app', 'New password'),
(1894, 'app', 'Repeat password'),
(1895, 'app', 'New username'),
(1899, 'app', 'Finalized and Signed by default.'),
(1901, 'app', 'Incident Report Draft ''{evt}'' has been RE-OPENED. You may now proceed with modifying it.'),
(1903, 'app', 'Section {evt1} of ''{evt2}'' has been RE-OPENED. You may now proceed with modifying it.'),
(1904, 'app', 'Offshore Incident Resubmitted'),
(1905, 'app', 'This report has been re-submitted. Previous rejection justification:'),
(1906, 'app/crf', 'Created'),
(1907, 'app/error', 'Restricted area'),
(1908, 'app/error', 'You do not have enough credentials to access this section.'),
(1910, 'app', '{evt1} for {evt2}'),
(1911, 'app/cpf', 'Which are the most important lessons learned from the incidents that deserve to be shared?'),
(1912, 'app/cpf', 'Loss of stability/buoyancy'),
(1913, 'app', 'User registration request'),
(1914, 'app', 'You have a request waiting.'),
(1915, 'app', 'Please note that you must be logged in to view the request.'),
(1918, 'app', 'Thank you.'),
(1919, 'app', 'Your request has been submitted!'),
(1920, 'app', 'Please check your mail regularly.'),
(1921, 'app', 'Operators/Owners user registration request'),
(1922, 'app', 'Competent Authority user registration request'),
(1923, 'app', 'Installation user registration request'),
(1924, 'app', '[SyRIO] Request #{evt1} - {evt2} user registration request'),
(1925, 'app', 'password reset'),
(1927, 'app', '[SyRIO] Request #{evt1} - {evt2} registration request'),
(1928, 'app', '[SyRIO] - User registration confirmed'),
(1929, 'app', '[SyRIO] OFFSHORE INCIDENT REPORTED - {evt1}/{evt2}/{evt3}'),
(1930, 'app', '[SyRIO] OFFSHORE INCIDENT RESUBMITTED - {evt1}/{evt2}/{evt3}'),
(1931, 'app', 'Request #{evt} has been deleted.'),
(1932, 'app', 'Delete request'),
(1933, 'app', 'Requests list'),
(1934, 'app', 'Request Type'),
(1935, 'app', 'From'),
(1936, 'app', 'Content'),
(1937, 'app', 'Processed By ID'),
(1938, 'app', 'Processed At'),
(1939, 'app', 'Justification'),
(1941, 'app', 'This will reactivate {data}.'),
(1944, 'app/units', 'Scope'),
(1946, 'app/units', 'metric'),
(1947, 'app/units', 'imperial'),
(1948, 'app/units', 'mass'),
(1949, 'app/units', 'rate (mass)'),
(1950, 'app/units', 'speed'),
(1951, 'app/units', 'length'),
(1952, 'app/units', 'metric volume'),
(1953, 'app/units', 'unit of energy'),
(1954, 'app/units', 'Multiplication Factor'),
(1956, 'app/units', 'Expression'),
(1957, 'app/units', 'Conversion Class'),
(1958, 'app/units', 'Unit'),
(1959, 'app/units', 'Sum Factor'),
(1960, 'app/units', 'time'),
(1961, 'app/units', 'pressure'),
(1962, 'app/units', 'temperature'),
(1963, 'app/messages', 'For system stability reasons this option is disabled in the current version of SyRIO.'),
(1964, 'app', 'Unable to delete {evt}!'),
(1972, 'app', 'Finalize registration'),
(1973, 'app', 'Welcome to SyRIO. As this is the first time you log in, please provide a password at your convenience to activate your account.'),
(1974, 'app', 'Activate account'),
(1975, 'app', 'Add user'),
(1980, 'app', 'Beds'),
(1982, 'app', 'Fixed'),
(1983, 'app', 'Operation type'),
(1990, 'app', '{action} {evt} user'),
(1991, 'app', 'Edit user'),
(1995, 'app', 'select user type'),
(1996, 'app', 'Please select the type of user you would like to create.'),
(1997, 'app', 'Next'),
(2002, 'app', 'Register new user'),
(2004, 'app', 'User {user} has been created and registered with {org}.'),
(2006, 'app', 'The user has been notified through email.'),
(2015, 'app', 'Visibility'),
(2016, 'app', 'event registered to SyRIO'),
(2017, 'app', 'event finalized'),
(2018, 'app', 'event signed'),
(2019, 'app', 'event reopened'),
(2020, 'app', 'event submitted'),
(2021, 'app', 'event re-submitted'),
(2022, 'app', 'event rejected'),
(2023, 'app', 'event accepted'),
(2024, 'app', 'event assessed'),
(2025, 'app', 'event assessed for CPF'),
(2026, 'app', 'section reopened'),
(2027, 'app', '{n, plural, =1{Holiday} other{Holidays}}'),
(2028, 'app', 'Day'),
(2029, 'app', 'Month'),
(2030, 'app/calendar', 'Repeat every year'),
(2031, 'app/calendar', 'Individual'),
(2032, 'app', 'Reporting history'),
(2033, 'app', 'Event re-assessed successfully!'),
(2034, 'app', 'Saving this as draft will lead to the REMOVAL of the report from the CPF preparation list. Proceed?'),
(2035, 'app', 'CPF Assessed'),
(2036, 'app/cpf', 'The changes entail refreshing the CPF Annual Report (for {evt}).'),
(2037, 'app/cpf', 'Please proceed accordingly.'),
(2038, 'app', 'suspended'),
(2039, 'app/cpf', 'Refresh Section 4 data'),
(2040, 'app/crf', 'Annual report is synced with the reported incidents.'),
(2041, 'app/crf', 'Annual report IS NOT synced with the reported incidents.'),
(2042, 'app', 'Editor {evt}'),
(2043, 'app/crf', 'Changes have been detected in the reports list for the current year.'),
(2044, 'app/crf', 'Please:'),
(2045, 'app/crf', 'to keep the Annual Report up to date.'),
(2046, 'app/messages', 'Data in Section 4 has been successfuly refreshed from the incidents list'),
(2047, 'app/messages', 'Data'),
(2048, 'app/messages/errors', 'You must specify the OPERATION failure'),
(2049, 'app/messages/errors', 'You must specify the EQUIPMENT failure'),
(2050, 'app/messages/errors', 'You must specify the PROCEDURAL failure'),
(2051, 'app/messages/errors', 'You must specify the NON-PROCESS release'),
(2052, 'app/messages/errors', 'You must specify the OTHER MEANS OF DETECTION'),
(2053, 'app/cpf', 'Number of incidents'),
(2054, 'app/cpf', '(number/hours worked)'),
(2055, 'app/cpf', '(number/kTOE)'),
(2056, 'app', 'This will clear all the information in the current report.'),
(2057, 'app', 'The individual CRF Assessments in Terms of Section 4 WILL BE PRESERVED.'),
(2058, 'app', 'warning'),
(2059, 'app', 'This will COMPLETELY delete the current report.'),
(2060, 'app', 'The individual CRF Assessments in Terms of Section 4 WILL ALSO BE DELETED!'),
(2061, 'app', 'Annual Report Reset'),
(2062, 'app/messages', 'The {evt} Annual Report has been reset (all the information has been cleared)'),
(2063, 'app/messages', 'The {evt} Annual Report has been deleted (the report AND individual incident preparation files have been deleted)'),
(2064, 'app/messages', 'Reopening the assessment may have cascade effects on the Annual Report.'),
(2065, 'app', 'Edit {evt}'),
(2066, 'app/cpf', 'Installation Year'),
(2069, 'app', 'reported installation'),
(2071, 'app', 'This installation has at least one incident recorded over the reporting period.'),
(2073, 'app', 'Leave empty in case of an operational installation'),
(2074, 'app', 'Oil/Condensate'),
(2075, 'app', 'Oil/Gas'),
(2076, 'app', 'Oil'),
(2077, 'app', 'Gas'),
(2078, 'app', 'Condensate'),
(2079, 'app', 'Not applicable'),
(2082, 'app/crf', '(Tick appropriate box)'),
(2083, 'app/crf', 'please specify'),
(2085, 'app/crf', 'estimated ppm'),
(2087, 'app/crf', 'Module ventilation?'),
(2088, 'app/crf', 'Immediate/Delayed'),
(2089, 'app/crf', 'What emergency action was taken'),
(2090, 'app/crf', 'CO2/Halon/inerts'),
(2091, 'app/crf', '(a) Design:'),
(2092, 'app/crf', '(b) Equipment:'),
(2093, 'app/crf', '(c) Operation:'),
(2094, 'app/crf', '(d) Procedural:'),
(2095, 'app/crf', 'Specify actual operation. e.g. wire line, well test, etc.'),
(2096, 'app/crf', 'Specify units'),
(2097, 'app/crf', 'tonnes, kg, Nm3'),
(2099, 'app/crf', 'tonnes/day, kg/s, Nm3/s'),
(2101, 'app/crf', '(Estimated time from discovery, e.g. alarm, electronic log, to termination of leak)'),
(2104, 'app/crf', '(i.e. zone at location of incident)'),
(2106, 'app/crf', 'Insert the number of walls, including floor and ceiling'),
(2109, 'app/crf', 'Specify heading in degrees'),
(2113, 'app', 'Please tick type of detector ot specify as appropriate'),
(2115, 'app/crf', 'Cause'),
(2116, 'app/crf', 'Please give a short description and complete the ''{evt}'' checklist below'),
(2117, 'app/crf', 'cause of leak checklist'),
(2120, 'app/crf', 'Choose one parameter from the following categories, and tick the appropriate boxes'),
(2121, 'app/crf', 'Operation mode in the area at the time of release'),
(2123, 'app', 'Please tick appropriate box'),
(2132, 'app/crf', 'If yes, please specify the type and quantity of released substance:'),
(2138, 'app/crf', 'within 10 working days from the event'),
(2150, 'app/crf', 'if applicable'),
(2168, 'app/crf', '(minimum distance between vessel and installation, course and speed of vessel, weather condition)'),
(2179, 'app/crf', ', unless already reported in a previous section of this report'),
(2184, 'app/crf', 'What are or are likely to be the significant adverse effects on the environment'),
(2187, 'app', 'SyRIO report'),
(2189, 'app', 'A'),
(2190, 'app', 'B'),
(2191, 'app', 'C'),
(2192, 'app', 'D'),
(2193, 'app', 'E'),
(2194, 'app', 'F'),
(2195, 'app', 'G'),
(2196, 'app', 'H'),
(2197, 'app', 'I'),
(2198, 'app', 'J'),
(2200, 'app/crf', 'Tick appropriate box'),
(2201, 'app', 'e.g.'),
(2204, 'app/crf', 'or other'),
(2205, 'app', 'If yes, outline the environmental  impacts which have already been observed  or are likely to result from the incident'),
(2206, 'app/crf', 'Click the sections below for drafting or narrowing down the displayed information.'),
(2208, 'app', '(a)'),
(2209, 'app', '(b)'),
(2210, 'app', '(c)'),
(2211, 'app', '(d)'),
(2212, 'app', '(e)'),
(2213, 'app', '(f)'),
(2214, 'app', '(g)'),
(2215, 'app', '(h)'),
(2216, 'app', '(i)'),
(2217, 'app', '(j)'),
(2218, 'app', '(k)'),
(2219, 'app', '(l)'),
(2220, 'app/crf', 'HELICOPTER ACCIDENTS, ON OR NEAR OFFSHORE INSTALLATIONS'),
(2222, 'app', 'Open Incidents Hub'),
(2223, 'app', 'drafts'),
(2224, 'app', 'CA accepted'),
(2225, 'app', 'CA rejected'),
(2226, 'app', 'CA pending'),
(2232, 'app', 'This corresponds to {evt1} and {evt2} sections of the Common Reporting Format (EU IR 1112/2014)'),
(2233, 'app/crf', 'If yes, was it:'),
(2239, 'app', 'If yes, outline the environmental  impacts which have already been observed  or are likely to result from the incident.'),
(2242, 'app', 'Clear'),
(2243, 'app', 'Clear history?'),
(2244, 'app', 'section finalized'),
(2245, 'app', 'draft created'),
(2246, 'app', 'draft deleted'),
(2247, 'app', 'draft restored'),
(2248, 'app', 'report''s CPF Assessment reopened'),
(2249, 'app', 'report''s CPF Assessment suspended'),
(2250, 'app', 'report''s CPF Assessment resumed'),
(2251, 'app', 'report''s MA Assessment reopened'),
(2252, 'app', 'at'),
(2253, 'app', 'Draft'),
(2255, 'app', 'Action'),
(2256, 'app', 'Section'),
(2260, 'app', 'Incidents reporting'),
(2262, 'app', 'Incidents status'),
(2264, 'app', 'The Assessment could not be finalized!'),
(2265, 'app', 'Review the information below and try again.'),
(2266, 'app', 'Overview of the incidents registered by your organization.'),
(2267, 'app', 'Incidents overview'),
(2268, 'app', 'Registered incidents'),
(2269, 'app', 'the number of incidents registered in SyRIO'),
(2271, 'app', 'Current year ({evt}):'),
(2272, 'app', 'Submitted reports'),
(2274, 'app', 'Pending for acceptance'),
(2275, 'app', 'Pending'),
(2276, 'app', 'the incidents registered in SyRIO, yet not reported to the CA'),
(2277, 'app', 'Finalized (partially)'),
(2278, 'app', 'Overview of the incidents registered by your installation.'),
(2279, 'app', 'Operations in this section allows you to control the events registered by your installation.'),
(2280, 'app', 'incident reports that have been submitted to the Competent Authority'),
(2281, 'app', 'Finalized'),
(2282, 'app', 'Rejected'),
(2283, 'app', 'Accepted'),
(2284, 'app', 'Signed'),
(2287, 'app', 'Create {evt} Organization'),
(2288, 'app', '{evt} Organization'),
(2289, 'app', 'Operators/Owners'),
(2290, 'app', 'Competent Authority Organizations'),
(2291, 'app', 'Organization type'),
(2294, 'app', 'select country'),
(2295, 'app', 'Installation {evt} has been deleted!'),
(2296, 'app', 'You may undo the operation or purge if you want it completely removed from SyRIO.'),
(2297, 'app', 'Intallation {evt} has been successfully reactivated.'),
(2298, 'app', 'Reactivate installation'),
(2299, 'app', 'Organization {evt} has been deleted!'),
(2300, 'app', 'Organization {evt} has been successfully reactivated.'),
(2301, 'app', 'Reactivate organization'),
(2302, 'app', 'The Organization cannot be deleted.'),
(2303, 'app', 'There must be at least one Competent Authority Organization left.'),
(2304, 'app', 'Organization {evt} has been purged (permanently deleted)!'),
(2305, 'app', 'User {user} has been successfully deleted.'),
(2306, 'app', 'You may reactivate user later or purge it for permanent deletion.'),
(2307, 'app', 'Delete user'),
(2308, 'app', 'User {user} has been successfully reactivated.'),
(2309, 'app', 'Reactivate user'),
(2310, 'app', 'SyRIO Developers'),
(2311, 'app', 'Use this form to contact SyRIO developers. Please provide any inquiries and / or report any bugs encountered. Thank you.'),
(2312, 'app', 'Contact Developers'),
(2313, 'app', 'Logged in as:'),
(2314, 'app', 'SyRIO administrator'),
(2315, 'app', 'Administrator {evt}'),
(2316, 'app', 'User ({evt})'),
(2317, 'app', 'allows the access to the reported incidents by your Organization as well as some reporting rights'),
(2321, 'app', 'This site uses cookies to offer the best online experience. By continuing to use this website, you are agreeing to our use of cookies. Alternatively, you can manage them in your browser settings.'),
(2322, 'app', 'Users list'),
(2323, 'app', 'Please use comma delimited numbers for separating the events of the same type (e.g. two explosions)'),
(2324, 'app/crf', 'An expolsion'),
(2340, 'app', 'never'),
(2341, 'app', 'Please provide a unique name for internal reference of this event'),
(2342, 'app', 'The {evt} file will be generated for download.'),
(2343, 'app', 'Drafting status'),
(2345, 'app', 'Finalized. Ready to be signed.'),
(2348, 'app', 'User details'),
(2352, 'app', 'add name'),
(2353, 'app', 'add the installation code'),
(2354, 'app', 'Please use the IMO code (if available)'),
(2360, 'app', 'Type (other)'),
(2361, 'app', 'e.g. jack-up'),
(2362, 'app', 'optional'),
(2363, 'app/messages/errors', 'Level of H2S must be specified if Gas or 2-Phase releases have been declared'),
(2364, 'app/messages/errors', 'You must specify the OTHER EMERGENCY ACTION taken'),
(2367, 'app', 'Intentionally left blank'),
(2368, 'app', 'Report draft signed'),
(2369, 'app', 'The incident report draft ''{evt}'' has been signed.'),
(2370, 'app', 'You may use this draft for reporting the event (''{evt}'') to the CA (by choosing Submit, unless ''{evt}'' has not been previously reported).'),
(2371, 'app', 'default administrator user');

-- --------------------------------------------------------

--
-- Table structure for table `units`
--

DROP TABLE IF EXISTS `units`;
CREATE TABLE `units` (
  `id` int(11) NOT NULL,
  `unit` varchar(45) NOT NULL,
  `c_factor` double NOT NULL,
  `scope` int(11) NOT NULL,
  `us` tinyint(1) NOT NULL DEFAULT '0',
  `s_unit` double DEFAULT NULL,
  `expression` varchar(128) DEFAULT NULL,
  `conversion_class` varchar(64) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `units`
--

INSERT INTO `units` (`id`, `unit`, `c_factor`, `scope`, `us`, `s_unit`, `expression`, `conversion_class`) VALUES
(1, 'kg', 1, 1, 0, NULL, NULL, NULL),
(2, 'tonne', 1000, 1, 0, NULL, NULL, NULL),
(3, 'g', 0.001, 1, 0, NULL, NULL, NULL),
(4, 's', 1, 2, 0, NULL, NULL, NULL),
(5, 'm', 60, 2, 0, NULL, NULL, NULL),
(6, 'h', 3600, 2, 0, NULL, NULL, NULL),
(7, 'day', 86400, 2, 0, NULL, NULL, NULL),
(8, 'kg/s', 1, 3, 0, NULL, NULL, NULL),
(9, 'tonnes/hour', 0.2777777777777778, 3, 0, NULL, NULL, NULL),
(10, 'tonnes/day', 0.0115740740740741, 3, 0, NULL, NULL, NULL),
(11, 'm/s', 1, 4, 0, NULL, NULL, NULL),
(12, 'km/h', 0.2777777777777778, 4, 0, NULL, NULL, NULL),
(13, 'bar', 1, 5, 0, NULL, NULL, NULL),
(14, 'psi', 0.0689475728034313, 5, 1, NULL, NULL, NULL),
(15, 'Pa', 0.00001, 5, 0, NULL, NULL, NULL),
(16, 'meter', 1, 6, 0, NULL, NULL, NULL),
(17, 'kilometer', 1000, 6, 0, NULL, NULL, NULL),
(18, 'deg C', 1, 7, 0, NULL, NULL, NULL),
(19, 'm3', 1, 8, 0, NULL, NULL, NULL),
(20, 'K', 1, 7, 0, 273.15, NULL, NULL),
(21, 'F', 1, 7, 1, NULL, NULL, '\\app\\models\\units\\FarenheitToCelsiusConverter'),
(23, 'toe', 1, 9, 0, NULL, NULL, NULL),
(24, 'Mtoe', 1000000, 9, 0, NULL, NULL, NULL),
(25, 'Gtoe', 1000000000, 9, 0, NULL, NULL, NULL),
(26, 'Ktoe', 1000, 9, 0, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `a1_release_causes`
--
ALTER TABLE `a1_release_causes`
  ADD PRIMARY KEY (`a1_id`,`leak_cause_id`),
  ADD KEY `fk_release_cause_cause_idx` (`leak_cause_id`);

--
-- Indexes for table `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD PRIMARY KEY (`item_name`,`user_id`),
  ADD KEY `auth_assignment_user_idx` (`user_id`);

--
-- Indexes for table `auth_item`
--
ALTER TABLE `auth_item`
  ADD PRIMARY KEY (`name`),
  ADD KEY `rule_name` (`rule_name`),
  ADD KEY `type` (`type`);

--
-- Indexes for table `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD KEY `child` (`child`);

--
-- Indexes for table `auth_rule`
--
ALTER TABLE `auth_rule`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `auth_users`
--
ALTER TABLE `auth_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_user_organization_idx` (`org_id`),
  ADD KEY `fk_user_installation_idx` (`inst_id`);

--
-- Indexes for table `ca_incident_cat`
--
ALTER TABLE `ca_incident_cat`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_ca_incident_cat_event_categorization_idx` (`draft_id`);

--
-- Indexes for table `constants`
--
ALTER TABLE `constants`
  ADD PRIMARY KEY (`num_code`);

--
-- Indexes for table `cpf_crf_assessments`
--
ALTER TABLE `cpf_crf_assessments`
  ADD PRIMARY KEY (`year`,`user_id`,`report_id`);

--
-- Indexes for table `cpf_installations`
--
ALTER TABLE `cpf_installations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_cpf_installations_section2` (`year`,`user_id`),
  ADD KEY `fk_cpf_installations_type_of_inst` (`type`),
  ADD KEY `fk_cpf_installation_tof_idx` (`tof`);

--
-- Indexes for table `cpf_inst_op_area`
--
ALTER TABLE `cpf_inst_op_area`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_op_area_installation_idx` (`installation_id`);

--
-- Indexes for table `cpf_references`
--
ALTER TABLE `cpf_references`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cpf_report_refs`
--
ALTER TABLE `cpf_report_refs`
  ADD UNIQUE KEY `idx_unique` (`year`,`ref_id`),
  ADD KEY `fk_cpf_report_refs_cpf_idx` (`year`,`user_id`),
  ADD KEY `fk_cpf_report_refs_ref_idx` (`ref_id`);

--
-- Indexes for table `cpf_section1`
--
ALTER TABLE `cpf_section1`
  ADD PRIMARY KEY (`year`,`user_id`);

--
-- Indexes for table `cpf_section2`
--
ALTER TABLE `cpf_section2`
  ADD PRIMARY KEY (`year`,`user_id`);

--
-- Indexes for table `cpf_section3`
--
ALTER TABLE `cpf_section3`
  ADD PRIMARY KEY (`year`,`user_id`);

--
-- Indexes for table `cpf_section4`
--
ALTER TABLE `cpf_section4`
  ADD PRIMARY KEY (`year`,`user_id`);

--
-- Indexes for table `cpf_section_4`
--
ALTER TABLE `cpf_section_4`
  ADD PRIMARY KEY (`year`);

--
-- Indexes for table `cpf_sessions`
--
ALTER TABLE `cpf_sessions`
  ADD PRIMARY KEY (`year`,`user_id`);

--
-- Indexes for table `cpf_tof`
--
ALTER TABLE `cpf_tof`
  ADD PRIMARY KEY (`type`);

--
-- Indexes for table `c_address`
--
ALTER TABLE `c_address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `c_country`
--
ALTER TABLE `c_country`
  ADD PRIMARY KEY (`country_id`),
  ADD UNIQUE KEY `iso2_UNIQUE` (`iso2`),
  ADD UNIQUE KEY `iso3_UNIQUE` (`iso3`),
  ADD KEY `iso2_INDEX` (`iso2`);

--
-- Indexes for table `event_classifications`
--
ALTER TABLE `event_classifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_event_declaration_event_cat_idx` (`event_id`);

--
-- Indexes for table `event_declaration`
--
ALTER TABLE `event_declaration`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `event_name_UNIQUE` (`event_name`),
  ADD KEY `fk_event_declaration_reporting_person_idx` (`raporteur_id`),
  ADD KEY `fk_event_declaration_installation_idx` (`installation_id`),
  ADD KEY `fk_event_declaration_operator_idx` (`operator_id`);

--
-- Indexes for table `holidays`
--
ALTER TABLE `holidays`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQUE` (`day`,`month`,`year`,`repeat`);

--
-- Indexes for table `installations`
--
ALTER TABLE `installations`
  ADD PRIMARY KEY (`id`,`operator_id`),
  ADD KEY `code_idx` (`code`),
  ADD KEY `type_idx` (`type`),
  ADD KEY `fk_installation_operator_idx` (`operator_id`);

--
-- Indexes for table `installation_operations`
--
ALTER TABLE `installation_operations`
  ADD PRIMARY KEY (`inst_code`,`ca_country`);

--
-- Indexes for table `installation_types`
--
ALTER TABLE `installation_types`
  ADD PRIMARY KEY (`type`);

--
-- Indexes for table `leak_cause`
--
ALTER TABLE `leak_cause`
  ADD PRIMARY KEY (`num_code`),
  ADD KEY `fk_leak_cause_cause_category_idx` (`category_id`);

--
-- Indexes for table `leak_cause_categories`
--
ALTER TABLE `leak_cause_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `organizations`
--
ALTER TABLE `organizations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_accronym` (`organization_acronym`),
  ADD KEY `fk_ca_organization_address_idx` (`organization_address`);

--
-- Indexes for table `reporting_history`
--
ALTER TABLE `reporting_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_reporting_history_events` (`evt_id`),
  ADD KEY `fk_reporting_history_drafts` (`draft_id`);

--
-- Indexes for table `request`
--
ALTER TABLE `request`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_request_request_type` (`request_type`);

--
-- Indexes for table `request_types`
--
ALTER TABLE `request_types`
  ADD PRIMARY KEY (`code`);

--
-- Indexes for table `sece`
--
ALTER TABLE `sece`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name_UNIQUE` (`name`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`setting_name`),
  ADD UNIQUE KEY `setting_name_UNIQUE` (`setting_name`);

--
-- Indexes for table `s_a`
--
ALTER TABLE `s_a`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_sa_eventcat_idx` (`event_classification_id`);

--
-- Indexes for table `s_a_1`
--
ALTER TABLE `s_a_1`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_sa1_sa_idx` (`sa_id`);

--
-- Indexes for table `s_a_2`
--
ALTER TABLE `s_a_2`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_sa2_sa_idx` (`sa_id`);

--
-- Indexes for table `s_a_3`
--
ALTER TABLE `s_a_3`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_sa3_sa_idx` (`sa_id`);

--
-- Indexes for table `s_a_4`
--
ALTER TABLE `s_a_4`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_sa4_sa_idx` (`sa_id`);

--
-- Indexes for table `s_b`
--
ALTER TABLE `s_b`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_sb_eventcat_idx` (`event_classification_id`);

--
-- Indexes for table `s_b_1`
--
ALTER TABLE `s_b_1`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_sb1_sa_idx` (`sb_id`);

--
-- Indexes for table `s_b_2`
--
ALTER TABLE `s_b_2`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_sb2_sb_idx` (`sb_id`);

--
-- Indexes for table `s_b_3`
--
ALTER TABLE `s_b_3`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_sb3_sb_idx` (`sb_id`);

--
-- Indexes for table `s_b_4`
--
ALTER TABLE `s_b_4`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_sb4_sb_idx` (`sb_id`);

--
-- Indexes for table `s_c`
--
ALTER TABLE `s_c`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_sc_eventcat_idx` (`event_classification_id`);

--
-- Indexes for table `s_c_1`
--
ALTER TABLE `s_c_1`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `sc_id_UNIQUE` (`sc_id`);

--
-- Indexes for table `s_c_2`
--
ALTER TABLE `s_c_2`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_sc_sc2_idx` (`sc_id`);

--
-- Indexes for table `s_c_2_1`
--
ALTER TABLE `s_c_2_1`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_sc2_sc21_idx` (`sc_2_id`);

--
-- Indexes for table `s_c_2_1_sece`
--
ALTER TABLE `s_c_2_1_sece`
  ADD PRIMARY KEY (`s_c_2_1_id`,`sece_id`),
  ADD KEY `fk_sece_sc21sece_idx` (`sece_id`);

--
-- Indexes for table `s_c_2_2`
--
ALTER TABLE `s_c_2_2`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_sc22_sc2_idx` (`sc_2_id`);

--
-- Indexes for table `s_c_3`
--
ALTER TABLE `s_c_3`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_sc3_sc_idx` (`sc_id`);

--
-- Indexes for table `s_c_4`
--
ALTER TABLE `s_c_4`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_sc4_sc_idx` (`sc_id`);

--
-- Indexes for table `s_d`
--
ALTER TABLE `s_d`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_sd_eventcat_idx` (`event_classification_id`);

--
-- Indexes for table `s_d_1`
--
ALTER TABLE `s_d_1`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_sd1_sd_idx` (`sd_id`);

--
-- Indexes for table `s_d_2`
--
ALTER TABLE `s_d_2`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_sd2_sd_idx` (`sd_id`);

--
-- Indexes for table `s_d_3`
--
ALTER TABLE `s_d_3`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_sd3_sd_idx` (`sd_id`);

--
-- Indexes for table `s_d_4`
--
ALTER TABLE `s_d_4`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_sd4_sd_idx` (`sd_id`);

--
-- Indexes for table `s_e`
--
ALTER TABLE `s_e`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_se_eventcat_idx` (`event_classification_id`);

--
-- Indexes for table `s_e_1`
--
ALTER TABLE `s_e_1`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_se1_se_idx` (`se_id`);

--
-- Indexes for table `s_e_2`
--
ALTER TABLE `s_e_2`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_se2_se_idx` (`se_id`);

--
-- Indexes for table `s_e_3`
--
ALTER TABLE `s_e_3`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_se3_se_idx` (`se_id`);

--
-- Indexes for table `s_e_4`
--
ALTER TABLE `s_e_4`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_se4_se_idx` (`se_id`);

--
-- Indexes for table `s_f`
--
ALTER TABLE `s_f`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_sf_eventcat_idx` (`event_classification_id`);

--
-- Indexes for table `s_f_1`
--
ALTER TABLE `s_f_1`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_sf1_sf_idx` (`sf_id`);

--
-- Indexes for table `s_f_2`
--
ALTER TABLE `s_f_2`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_sf2_sf_idx` (`sf_id`);

--
-- Indexes for table `s_f_3`
--
ALTER TABLE `s_f_3`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_sf3_sf_idx` (`sf_id`);

--
-- Indexes for table `s_f_4`
--
ALTER TABLE `s_f_4`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_sf4_sf_idx` (`sf_id`);

--
-- Indexes for table `s_i`
--
ALTER TABLE `s_i`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_si_eventcat_idx` (`event_classification_id`);

--
-- Indexes for table `s_i_1`
--
ALTER TABLE `s_i_1`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_si1_si_idx` (`si_id`);

--
-- Indexes for table `s_i_2`
--
ALTER TABLE `s_i_2`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_si2_si_idx` (`si_id`);

--
-- Indexes for table `s_i_3`
--
ALTER TABLE `s_i_3`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_si3_si_idx` (`si_id`);

--
-- Indexes for table `s_i_4`
--
ALTER TABLE `s_i_4`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_si4_si_idx` (`si_id`);

--
-- Indexes for table `s_j`
--
ALTER TABLE `s_j`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_sj_eventcat_idx` (`event_classification_id`);

--
-- Indexes for table `s_j_1`
--
ALTER TABLE `s_j_1`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_sj1_sj_idx` (`sj_id`);

--
-- Indexes for table `s_j_2`
--
ALTER TABLE `s_j_2`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_sj2_sj_idx` (`sj_id`);

--
-- Indexes for table `s_j_3`
--
ALTER TABLE `s_j_3`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_sj3_sj_idx` (`sj_id`);

--
-- Indexes for table `s_j_4`
--
ALTER TABLE `s_j_4`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_sj4_sj_idx` (`sj_id`);

--
-- Indexes for table `t_message`
--
ALTER TABLE `t_message`
  ADD PRIMARY KEY (`id`,`language`);

--
-- Indexes for table `t_source_message`
--
ALTER TABLE `t_source_message`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_message` (`message`(255));

--
-- Indexes for table `units`
--
ALTER TABLE `units`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unit_UNIQUE` (`unit`),
  ADD UNIQUE KEY `scope_identity_UNIQUE` (`unit`,`scope`,`us`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `auth_users`
--
ALTER TABLE `auth_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=96;
--
-- AUTO_INCREMENT for table `ca_incident_cat`
--
ALTER TABLE `ca_incident_cat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cpf_installations`
--
ALTER TABLE `cpf_installations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cpf_inst_op_area`
--
ALTER TABLE `cpf_inst_op_area`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cpf_references`
--
ALTER TABLE `cpf_references`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `c_address`
--
ALTER TABLE `c_address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `c_country`
--
ALTER TABLE `c_country`
  MODIFY `country_id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=252;
--
-- AUTO_INCREMENT for table `event_classifications`
--
ALTER TABLE `event_classifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `holidays`
--
ALTER TABLE `holidays`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `reporting_history`
--
ALTER TABLE `reporting_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `request_types`
--
ALTER TABLE `request_types`
  MODIFY `code` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=203;
--
-- AUTO_INCREMENT for table `s_a`
--
ALTER TABLE `s_a`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `s_a_1`
--
ALTER TABLE `s_a_1`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `s_a_2`
--
ALTER TABLE `s_a_2`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `s_a_3`
--
ALTER TABLE `s_a_3`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `s_a_4`
--
ALTER TABLE `s_a_4`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `s_b`
--
ALTER TABLE `s_b`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `s_b_1`
--
ALTER TABLE `s_b_1`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `s_b_2`
--
ALTER TABLE `s_b_2`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `s_b_3`
--
ALTER TABLE `s_b_3`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `s_b_4`
--
ALTER TABLE `s_b_4`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `s_c`
--
ALTER TABLE `s_c`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `s_c_1`
--
ALTER TABLE `s_c_1`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `s_c_2`
--
ALTER TABLE `s_c_2`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `s_c_2_1`
--
ALTER TABLE `s_c_2_1`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `s_c_2_2`
--
ALTER TABLE `s_c_2_2`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `s_c_3`
--
ALTER TABLE `s_c_3`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `s_c_4`
--
ALTER TABLE `s_c_4`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `s_d`
--
ALTER TABLE `s_d`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `s_d_1`
--
ALTER TABLE `s_d_1`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `s_d_2`
--
ALTER TABLE `s_d_2`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `s_d_3`
--
ALTER TABLE `s_d_3`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `s_d_4`
--
ALTER TABLE `s_d_4`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `s_e`
--
ALTER TABLE `s_e`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `s_e_1`
--
ALTER TABLE `s_e_1`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `s_e_2`
--
ALTER TABLE `s_e_2`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `s_e_3`
--
ALTER TABLE `s_e_3`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `s_e_4`
--
ALTER TABLE `s_e_4`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `s_f`
--
ALTER TABLE `s_f`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `s_f_1`
--
ALTER TABLE `s_f_1`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `s_f_2`
--
ALTER TABLE `s_f_2`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `s_f_3`
--
ALTER TABLE `s_f_3`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `s_f_4`
--
ALTER TABLE `s_f_4`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `s_i`
--
ALTER TABLE `s_i`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `s_i_1`
--
ALTER TABLE `s_i_1`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `s_i_2`
--
ALTER TABLE `s_i_2`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `s_i_3`
--
ALTER TABLE `s_i_3`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `s_i_4`
--
ALTER TABLE `s_i_4`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `s_j`
--
ALTER TABLE `s_j`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `s_j_1`
--
ALTER TABLE `s_j_1`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `s_j_2`
--
ALTER TABLE `s_j_2`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `s_j_3`
--
ALTER TABLE `s_j_3`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `s_j_4`
--
ALTER TABLE `s_j_4`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_source_message`
--
ALTER TABLE `t_source_message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2372;
--
-- AUTO_INCREMENT for table `units`
--
ALTER TABLE `units`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=27;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `a1_release_causes`
--
ALTER TABLE `a1_release_causes`
  ADD CONSTRAINT `fk_a1_release_a1` FOREIGN KEY (`a1_id`) REFERENCES `s_a_1` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_a1_release_release` FOREIGN KEY (`leak_cause_id`) REFERENCES `leak_cause` (`num_code`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auth_assignment_user` FOREIGN KEY (`user_id`) REFERENCES `auth_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `auth_item`
--
ALTER TABLE `auth_item`
  ADD CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `auth_users`
--
ALTER TABLE `auth_users`
  ADD CONSTRAINT `fk_user_installation` FOREIGN KEY (`inst_id`) REFERENCES `installations` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_user_organization` FOREIGN KEY (`org_id`) REFERENCES `organizations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ca_incident_cat`
--
ALTER TABLE `ca_incident_cat`
  ADD CONSTRAINT `fk_ca_incident_cat_event_categorization` FOREIGN KEY (`draft_id`) REFERENCES `event_classifications` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `cpf_crf_assessments`
--
ALTER TABLE `cpf_crf_assessments`
  ADD CONSTRAINT `fk_cpf_crf_cpf_sessions` FOREIGN KEY (`year`) REFERENCES `cpf_sessions` (`year`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `cpf_installations`
--
ALTER TABLE `cpf_installations`
  ADD CONSTRAINT `fk_cpf_installations_section2` FOREIGN KEY (`year`, `user_id`) REFERENCES `cpf_section2` (`year`, `user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_cpf_installations_type_of_inst` FOREIGN KEY (`type`) REFERENCES `installation_types` (`type`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_cpf_installation_tof` FOREIGN KEY (`tof`) REFERENCES `cpf_tof` (`type`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `cpf_inst_op_area`
--
ALTER TABLE `cpf_inst_op_area`
  ADD CONSTRAINT `fk_op_area_inst` FOREIGN KEY (`installation_id`) REFERENCES `cpf_installations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `cpf_report_refs`
--
ALTER TABLE `cpf_report_refs`
  ADD CONSTRAINT `fk_cpf_report_refs_cpf` FOREIGN KEY (`year`, `user_id`) REFERENCES `cpf_sessions` (`year`, `user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_cpf_report_refs_ref` FOREIGN KEY (`ref_id`) REFERENCES `cpf_references` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `cpf_section1`
--
ALTER TABLE `cpf_section1`
  ADD CONSTRAINT `fk_cpf_sessions_section1` FOREIGN KEY (`year`, `user_id`) REFERENCES `cpf_sessions` (`year`, `user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `cpf_section2`
--
ALTER TABLE `cpf_section2`
  ADD CONSTRAINT `fk_cpf_cpf_section2` FOREIGN KEY (`year`, `user_id`) REFERENCES `cpf_sessions` (`year`, `user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `cpf_section3`
--
ALTER TABLE `cpf_section3`
  ADD CONSTRAINT `fk_cpf_sessions_section3` FOREIGN KEY (`year`, `user_id`) REFERENCES `cpf_sessions` (`year`, `user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `cpf_section4`
--
ALTER TABLE `cpf_section4`
  ADD CONSTRAINT `fk_cpf_sessions_section4` FOREIGN KEY (`year`, `user_id`) REFERENCES `cpf_sessions` (`year`, `user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `event_classifications`
--
ALTER TABLE `event_classifications`
  ADD CONSTRAINT `fk_event_declaration_event_cat` FOREIGN KEY (`event_id`) REFERENCES `event_declaration` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `event_declaration`
--
ALTER TABLE `event_declaration`
  ADD CONSTRAINT `fk_event_declaration_installation` FOREIGN KEY (`installation_id`) REFERENCES `installations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_event_declaration_operator` FOREIGN KEY (`operator_id`) REFERENCES `organizations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_event_declaration_reporting_person` FOREIGN KEY (`raporteur_id`) REFERENCES `auth_users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `installations`
--
ALTER TABLE `installations`
  ADD CONSTRAINT `fk_installation_operator` FOREIGN KEY (`operator_id`) REFERENCES `organizations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_installation_type` FOREIGN KEY (`type`) REFERENCES `installation_types` (`type`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `leak_cause`
--
ALTER TABLE `leak_cause`
  ADD CONSTRAINT `fk_leak_cause_cause_category` FOREIGN KEY (`category_id`) REFERENCES `leak_cause_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `organizations`
--
ALTER TABLE `organizations`
  ADD CONSTRAINT `fk_organization_address` FOREIGN KEY (`organization_address`) REFERENCES `c_address` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `reporting_history`
--
ALTER TABLE `reporting_history`
  ADD CONSTRAINT `fk_reporting_history_drafts` FOREIGN KEY (`draft_id`) REFERENCES `event_classifications` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_reporting_history_events` FOREIGN KEY (`evt_id`) REFERENCES `event_declaration` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `request`
--
ALTER TABLE `request`
  ADD CONSTRAINT `fk_request_request_type` FOREIGN KEY (`request_type`) REFERENCES `request_types` (`code`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `s_a`
--
ALTER TABLE `s_a`
  ADD CONSTRAINT `fk_sa_eventcat` FOREIGN KEY (`event_classification_id`) REFERENCES `event_classifications` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `s_a_1`
--
ALTER TABLE `s_a_1`
  ADD CONSTRAINT `fk_sa1_sa` FOREIGN KEY (`sa_id`) REFERENCES `s_a` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `s_a_2`
--
ALTER TABLE `s_a_2`
  ADD CONSTRAINT `fk_sa2_sa` FOREIGN KEY (`sa_id`) REFERENCES `s_a` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `s_a_3`
--
ALTER TABLE `s_a_3`
  ADD CONSTRAINT `fk_sa3_sa` FOREIGN KEY (`sa_id`) REFERENCES `s_a` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `s_a_4`
--
ALTER TABLE `s_a_4`
  ADD CONSTRAINT `fk_sa4_sa` FOREIGN KEY (`sa_id`) REFERENCES `s_a` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `s_b`
--
ALTER TABLE `s_b`
  ADD CONSTRAINT `fk_sb_eventcat` FOREIGN KEY (`event_classification_id`) REFERENCES `event_classifications` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `s_b_1`
--
ALTER TABLE `s_b_1`
  ADD CONSTRAINT `fk_sb1_sb` FOREIGN KEY (`sb_id`) REFERENCES `s_b` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `s_b_2`
--
ALTER TABLE `s_b_2`
  ADD CONSTRAINT `fk_sb2_sb` FOREIGN KEY (`sb_id`) REFERENCES `s_b` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `s_b_3`
--
ALTER TABLE `s_b_3`
  ADD CONSTRAINT `fk_sb3_sb` FOREIGN KEY (`sb_id`) REFERENCES `s_b` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `s_b_4`
--
ALTER TABLE `s_b_4`
  ADD CONSTRAINT `fk_sb4_sb` FOREIGN KEY (`sb_id`) REFERENCES `s_b` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `s_c`
--
ALTER TABLE `s_c`
  ADD CONSTRAINT `fk_sc_eventcat` FOREIGN KEY (`event_classification_id`) REFERENCES `event_classifications` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `s_c_1`
--
ALTER TABLE `s_c_1`
  ADD CONSTRAINT `fk_sc1_sc` FOREIGN KEY (`sc_id`) REFERENCES `s_c` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `s_c_2`
--
ALTER TABLE `s_c_2`
  ADD CONSTRAINT `fk_sc_sc2` FOREIGN KEY (`sc_id`) REFERENCES `s_c` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `s_c_2_1`
--
ALTER TABLE `s_c_2_1`
  ADD CONSTRAINT `fk_sc2_sc21` FOREIGN KEY (`sc_2_id`) REFERENCES `s_c_2` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `s_c_2_1_sece`
--
ALTER TABLE `s_c_2_1_sece`
  ADD CONSTRAINT `fk_sc21_sc21sece` FOREIGN KEY (`s_c_2_1_id`) REFERENCES `s_c_2_1` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_sece_sc21sece` FOREIGN KEY (`sece_id`) REFERENCES `sece` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `s_c_2_2`
--
ALTER TABLE `s_c_2_2`
  ADD CONSTRAINT `fk_sc22_sc2` FOREIGN KEY (`sc_2_id`) REFERENCES `s_c_2` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `s_c_3`
--
ALTER TABLE `s_c_3`
  ADD CONSTRAINT `fk_sc3_sc` FOREIGN KEY (`sc_id`) REFERENCES `s_c` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `s_c_4`
--
ALTER TABLE `s_c_4`
  ADD CONSTRAINT `fk_sc4_sc` FOREIGN KEY (`sc_id`) REFERENCES `s_c` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `s_d`
--
ALTER TABLE `s_d`
  ADD CONSTRAINT `fk_sd_eventcat` FOREIGN KEY (`event_classification_id`) REFERENCES `event_classifications` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `s_d_1`
--
ALTER TABLE `s_d_1`
  ADD CONSTRAINT `fk_sd1_sd` FOREIGN KEY (`sd_id`) REFERENCES `s_d` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `s_d_2`
--
ALTER TABLE `s_d_2`
  ADD CONSTRAINT `fk_sd2_sd` FOREIGN KEY (`sd_id`) REFERENCES `s_d` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `s_d_3`
--
ALTER TABLE `s_d_3`
  ADD CONSTRAINT `fk_sd3_sd` FOREIGN KEY (`sd_id`) REFERENCES `s_d` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `s_d_4`
--
ALTER TABLE `s_d_4`
  ADD CONSTRAINT `fk_sd4_sd` FOREIGN KEY (`sd_id`) REFERENCES `s_d` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `s_e`
--
ALTER TABLE `s_e`
  ADD CONSTRAINT `fk_se_eventcat` FOREIGN KEY (`event_classification_id`) REFERENCES `event_classifications` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `s_e_1`
--
ALTER TABLE `s_e_1`
  ADD CONSTRAINT `fk_se1_se` FOREIGN KEY (`se_id`) REFERENCES `s_e` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `s_e_2`
--
ALTER TABLE `s_e_2`
  ADD CONSTRAINT `fk_se2_se` FOREIGN KEY (`se_id`) REFERENCES `s_e` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `s_e_3`
--
ALTER TABLE `s_e_3`
  ADD CONSTRAINT `fk_se3_se` FOREIGN KEY (`se_id`) REFERENCES `s_e` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `s_e_4`
--
ALTER TABLE `s_e_4`
  ADD CONSTRAINT `fk_se4_se` FOREIGN KEY (`se_id`) REFERENCES `s_e` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `s_f`
--
ALTER TABLE `s_f`
  ADD CONSTRAINT `fk_sf_eventcat` FOREIGN KEY (`event_classification_id`) REFERENCES `event_classifications` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `s_f_1`
--
ALTER TABLE `s_f_1`
  ADD CONSTRAINT `fk_sf1_sf` FOREIGN KEY (`sf_id`) REFERENCES `s_f` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `s_f_2`
--
ALTER TABLE `s_f_2`
  ADD CONSTRAINT `fk_sf2_sf` FOREIGN KEY (`sf_id`) REFERENCES `s_f` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `s_f_3`
--
ALTER TABLE `s_f_3`
  ADD CONSTRAINT `fk_sf3_sf` FOREIGN KEY (`sf_id`) REFERENCES `s_f` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `s_f_4`
--
ALTER TABLE `s_f_4`
  ADD CONSTRAINT `fk_sf4_sf` FOREIGN KEY (`sf_id`) REFERENCES `s_f` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `s_i`
--
ALTER TABLE `s_i`
  ADD CONSTRAINT `fk_si_eventcat` FOREIGN KEY (`event_classification_id`) REFERENCES `event_classifications` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `s_i_1`
--
ALTER TABLE `s_i_1`
  ADD CONSTRAINT `fk_si1_si` FOREIGN KEY (`si_id`) REFERENCES `s_i` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `s_i_2`
--
ALTER TABLE `s_i_2`
  ADD CONSTRAINT `fk_si2_si` FOREIGN KEY (`si_id`) REFERENCES `s_i` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `s_i_3`
--
ALTER TABLE `s_i_3`
  ADD CONSTRAINT `fk_si3_si` FOREIGN KEY (`si_id`) REFERENCES `s_i` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `s_i_4`
--
ALTER TABLE `s_i_4`
  ADD CONSTRAINT `fk_si4_si` FOREIGN KEY (`si_id`) REFERENCES `s_i` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `s_j`
--
ALTER TABLE `s_j`
  ADD CONSTRAINT `fk_sj_eventcat` FOREIGN KEY (`event_classification_id`) REFERENCES `event_classifications` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `s_j_1`
--
ALTER TABLE `s_j_1`
  ADD CONSTRAINT `fk_sj1_sj` FOREIGN KEY (`sj_id`) REFERENCES `s_j` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `s_j_2`
--
ALTER TABLE `s_j_2`
  ADD CONSTRAINT `fk_sj2_sj` FOREIGN KEY (`sj_id`) REFERENCES `s_j` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `s_j_3`
--
ALTER TABLE `s_j_3`
  ADD CONSTRAINT `fk_sj3_sj` FOREIGN KEY (`sj_id`) REFERENCES `s_j` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `s_j_4`
--
ALTER TABLE `s_j_4`
  ADD CONSTRAINT `fk_sj4_sj` FOREIGN KEY (`sj_id`) REFERENCES `s_j` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `t_message`
--
ALTER TABLE `t_message`
  ADD CONSTRAINT `fk_message_source_message_2` FOREIGN KEY (`id`) REFERENCES `t_source_message` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
